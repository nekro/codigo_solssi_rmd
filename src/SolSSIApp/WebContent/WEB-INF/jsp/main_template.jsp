
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	response.setHeader( "Pragma", "no-cache" );
	response.setHeader( "Cache-Control", "no-cache" );
	response.setDateHeader( "Expires", 0 );
%>

<s:htmlEscape defaultHtmlEscape="true" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="es">

<!-- head: Start -->
<head>


<!-- T�tulo de la Ventana -->
<title>MetLife | SSI</title>
<!-- Icono -->
<link rel="shortcut icon" href="<s:url value='/resources/images/favicon.ico'/>" type="image/x-icon">
<!-- Meta -->

<!-- Estilos y Scripts -->
<link href="<s:url value="/resources/css/metlife/screen.css" />" rel="stylesheet" type="text/css" />
<link href="<s:url value="/resources/css/ui/estilos_ui.css" />" rel="stylesheet" type="text/css" />
<link href="<s:url value="/resources/css/solssi.css" />" rel="stylesheet" type="text/css" />
<link href="<s:url value="/resources/css/tablas.css" />" rel="stylesheet" type="text/css" />
<link href="<s:url value="/resources/css/dijit/themes/claro/claro.css" />" rel="stylesheet" type="text/css" />


<!--[if lt IE 7]>
   		<style type="text/css">@import url(<s:url value="/resources"/>/css/metlife/IEbugs.css);</style>
    <![endif]-->


<script type="text/javascript" src="<s:url value="/resources/js/ssi_library_1.0.0.js"/>"></script>
<script type="text/javascript" src="<s:url value="/resources/js/dojo/dojo.xd.js"/>"></script>
<script type="text/javascript" src="<s:url value="/resources/js/menu.js"/>"></script>

<script type="text/javascript">
	
		var cerrarSession = true;
		
		document.onkeydown=teclaPulsada;
		
		function teclaPulsada(){
		    var code = window.event.keyCode;
		    
		    if(code==8 && (event.srcElement.tagName != 'INPUT') && event.srcElement.tagName != 'TEXTAREA'){
		        return false;
		    }
		    if(code==17){
		        return false;
		    }
		    if(code==18){
		        return false;
		    }
		     
		     if(code==116){
		        window.event.keyCode = 0;
		        return false;
		    }
		}
	
		dojo.ready(function(){
			 
			  			  
			  dojo.byId("divLoading").style.display = "none";
			  
			  <c:if test="${empty enable_history}">
			  	window.history.forward();
			  </c:if>
			  
			  dojo.query("button").onclick(function(e){
			  	  cerrarSession = false;
			  });
			 
			  dojo.query("a").onclick(function(e){
			  	  cerrarSession = false;
			  });
			  
			  try{
			  	dojo.query("form").attr("autocomplete", "off");
			  }catch(e){}	
		  });

	                            
		<c:if test="${empty disabled_close_session}">
			
			window.onbeforeunload = function() {
				if(cerrarSession){
					document.forms["menuDirectionForm"].action = "<s:url value='/logout'/>";
					document.forms["menuDirectionForm"].submit();
				}	
			}
		
		</c:if>
</script>

<script>
  
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-5376212-35', 'metlife.com.mx');
  ga('send', 'pageview');
  
</script>



</head>
<!-- Head: Fin -->
<!-- Body: Inicio -->

<body class="claro">
	<!-- Contenedor: Inicio -->
	<div id="contenedor">
		<!-- #Encabezado: Inicio -->
		<div id="encabezado">
			<!-- .Encabezado: Inicio -->
				<t:insertAttribute name="header" />
				<c:if test="${ACCESO_BY_OPERACION eq 'true'}">
					<c:choose>
						<c:when test="${tipoMenu eq 'RESCATES'}">
							<ul class="menuDerecho">
								<li class="menuItem1-On"><a href="<s:url value='/operacion/rescates'/>" title="Rescates">Rescates</a></li>
								<li class="menuItem2"><a href="<s:url value='/operacion/esquemas'/>" title="Esquemas">Esquemas</a></li>
							</ul>
						</c:when>
						<c:when test="${tipoMenu eq 'ESQUEMAS'}">
							<ul class="menuDerecho">
								<li class="menuItem1"><a href="<s:url value='/operacion/rescates'/>" title="Rescates">Rescates</a></li>
								<li class="menuItem2-On"><a href="<s:url value='/operacion/esquemas'/>" title="Esquemas">Esquemas</a></li>
							</ul>
						</c:when>
					</c:choose>
				</c:if>
				
			<!-- #Encabezado: Fin -->
			<!-- #Contenido: Inicio -->
			<div id="contenido" class="servicioCliente">
				<!-- .Contenido: Inicio -->
				<div class="contenido">

					<!-- Men� Principal: Inicio -->
					<div id="menu" class="activo" style="height: 75px">

						<t:insertAttribute name="menu" />
						
					</div>
					<!-- Men� Principal: Fin -->
					<div style="width: 100%;min-height: 475px" align="center" >
						<div class="ui-widget-content ui-corner-all" align="center" style="width: 98%;min-height: 465px">
							<t:insertAttribute name="content" />
						</div>
						<div id="divLoading" class="ui-widget-content ui-corner-all">
							<div style="padding-top: 20%">	
								<h5>Cargando...</h5>
							</div>
							<div align="center" style="vertical-align: middle;">
							 	<img src="<s:url value="/resources/images/loading.gif"/>" alt="Loading..." class="loading"/>
							</div>
						</div>
					</div>
					<!-- Portlets: Fin -->
				</div>
				<!-- Contenedor P�gina Inicial: Fin -->
			</div>
			<!-- .Contenido: Fin -->
		</div>
		<!-- #Contenido: Fin -->
	</div>
	<!-- Contenedor: Fin -->
	<!-- .Pie: Inicio -->
	<div class="pie">
		<t:insertAttribute name="footer" />
	</div>
	<!-- .Pie: Fin -->
	
</body>
<!-- Body: Fin -->
</html>
