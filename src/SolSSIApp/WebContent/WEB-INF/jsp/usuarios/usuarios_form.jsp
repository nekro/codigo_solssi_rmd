<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
	
	function buscar() {
		document.forms["filtro"].action = "<s:url value='/usuarios/consulta'/>";
		submitForm(document.forms["filtro"]);
		//document.forms["filtro"].submit();
	}
	
	function save() {
		document.forms["usuario"].action = "<s:url value='/usuarios/save'/>";
		submitForm(document.forms["usuario"]);
		//document.forms["usuario"].submit();
	}
	
	function update() {
		document.forms["usuario"].action = "<s:url value='/usuarios/update'/>";
		submitForm(document.forms["usuario"]);
		//document.forms["usuario"].submit();
	}
	
	require(["dojo/ready"], function(ready){
		  ready(function(){
			  dojo.byId("submenuUsuarios").setAttribute("class", "activo");
		  });
		});
	
	

</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

#formulario td{
	padding-top: 3px !important;
	padding-bottom: 3px !important;
}

</style>

	<c:choose>
		<c:when test="${command eq 'update'}">
			<div class="moduleTitle"><span>Actualizaci&oacute;n de Usuario</span></div>					
		</c:when>
		<c:otherwise>
			<div class="moduleTitle"><span>Alta de Usuario</span></div>
		</c:otherwise>
	</c:choose>
	
	<div id="formulario" style="width: 75%" align="center" class="ui-widget-content ui-corner-all">
		<form:form modelAttribute="usuario">
			<table>
				<tr>
					<td colspan="2" align="left">
						<span class="subseccion">Capture la informaci&oacute;n</span>
					</td>
				</tr>
				<tr>	
					<td class="tag">
						<form:label path="clave">Clave:</form:label>
					</td>
					<c:choose>
							<c:when test="${command eq 'update'}">
								<td class="valueField">
									<c:out value="${usuario.clave}"/>
									<form:hidden path="clave" htmlEscape="true"/>
								</td>
							</c:when>
							<c:otherwise>
								<td class="control">
									<form:input path="clave" maxlength="10" htmlEscape="true"/>
									<s:hasBindErrors name="usuario"><br/><form:errors path="clave" cssClass="error"/></s:hasBindErrors>
								</td>
							</c:otherwise>
						</c:choose>
					</tr>
				<tr>	
					<td class="tag">
						<form:label path="nombre">Nombre:</form:label>
					</td>
					<td class="control">
						<form:input path="nombre" maxlength="64" size="50" htmlEscape="true"/>
						<s:hasBindErrors name="usuario"><br/><form:errors path="nombre" cssClass="error"/></s:hasBindErrors>
					</td>
				</tr>
				<tr>	
					<td class="tag">
						<form:label path="dependencia">Centro de Trabajo:</form:label>
					</td>
					<td class="control">
						<form:select path="dependencia" cssStyle="width:200px" htmlEscape="true">
							<form:option value="0" label="NINGUNA"/>
							<form:options items="${CATALOGO.DEPENDENCIAS}" itemLabel="descDependencia"/>
						</form:select>
					</td>
				</tr>
				<tr>	
					<td class="tag">
						<form:label path="dependencia">Estatus:</form:label>
					</td>
					<td class="control">
						<form:radiobutton path="activo" value="true" label="Activo"/>
						<form:radiobutton path="activo" value="false" label="Inactivo"/>
					</td>
				</tr>
				<tr>	
					<td class="tag">
						<form:label path="rol">Rol:</form:label>
					</td>
					<td class="control">
						<form:select path="rol.id" cssStyle="width:200px" htmlEscape="true">
							<form:options items="${roles}" itemLabel="descripcion" itemValue="id"/>
						</form:select>
					</td>
				</tr>
			</table>
			
			<input type="hidden" name="command" value="${command}"/>
			<form:hidden path="admin"/>
		</form:form>
		
		<div>
			<br/>
			<c:choose>
				<c:when test="${command eq 'update'}">
					<button type="button" class="btnEnviar" onclick="update()"><span><em>Guardar</em></span></button>					
				</c:when>
				<c:otherwise>
					<button type="button" class="btnEnviar" onclick="save()"><span><em>Guardar</em></span></button>
				</c:otherwise>
			</c:choose>
			<br/><br/>
		</div>		
		
	</div>
	
	

