<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
	
	function buscarUsuarios() {
		document.forms["filtro"].action = "<s:url value='/usuarios/consulta'/>";
		submitForm(document.forms["filtro"]);
		//document.forms["filtro"].submit();
	}
	
	function editarUsuario(clave) {
		document.forms["edit"].clave.value = clave;
		submitForm(document.forms["edit"]);
		//document.forms["edit"].submit();
	}
	
</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

</style>

	<div class="moduleTitle"><span>Administraci&oacute;n de Usuarios</span></div>
	<div id="filtro" style="width: 90%" align="center">
		<form:form modelAttribute="usuarioFilter" name="filtro">
		
			<table class="ui-widget-content ui-corner-all">
				<tr >
					<td align="center"><span class="titleSeccion">Filtro de B&uacute;squeda</span>
					</td>
				</tr>
				<tr>
					<td align="left">
						<ul>
							<li>
								<span>Clave de Usuario:</span> 
									<form:input path="clave" maxlength="10" style="width:90px" htmlEscape="true"/>
							</li>
							<li>
								<span>Rol:</span> 
								<form:select path="rol" cssStyle="width:200px" htmlEscape="true">
									<form:option value="0" label="TODOS"/>
									<form:options items="${roles}" itemLabel="descripcion" itemValue="id"/>
								</form:select>	
							</li>
							<li>
								<span>Dependencia:</span> 
									<form:select path="dependencia" cssStyle="width:200px" htmlEscape="true">
										<form:option value="0" label="TODAS"/>
										<form:options items="${CATALOGO.DEPENDENCIAS}" itemLabel="descDependencia"/>
									</form:select>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td align="center">
						<button type="button" class="btnEnviar" onclick="buscarUsuarios()"><span><em>Buscar</em></span></button>
					</td>
				</tr>
			</table>
			
			<s:hasBindErrors name="usuarioFilter">
				<br/><br/>
				<div class="ui-state-error ui-corner-all" style="width: 80%;padding-top: 10px;padding-bottom: 10px">
					<form:errors path="*"/>
				</div>
			</s:hasBindErrors>
		</form:form>
	</div>
	
	

<c:if test="${not empty usuarios}">
	
		<br/>
		<h4>Resultados</h4>
		<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 80%">
			<table  class="display" style="width: 100%;padding-top: 30px" align="center">
				<thead class="ui-widget-header">
					<tr>
						<th>CLAVE</th>
						<th>NOMBRE</th>
						<th>CENTRO DE TRABAJO</th>
						<th>ROL</th>
						<th>ESTATUS</th>
						<th>MODIFICAR</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
	
				<c:forEach items="${usuarios}" var="user" varStatus="st">
					
					<c:choose>
				    	<c:when test="${st.count % 2 == 1}">
				    		<c:set var="css" value="odd"/>
				    	</c:when>
				    	<c:otherwise>
				    		<c:set var="css" value="even"/>
				    	</c:otherwise>
				    </c:choose>
					
					<c:set var="centroTrabajo">${user.dependencia}</c:set>
					
					<tr class="${css}">
						<td><a href="javascript:editarUsuario('${user.clave}')">${user.clave}</a></td>
						<td>${user.nombre}</td>
						<td>${CATALOGO.DEPENDENCIAS[centroTrabajo].descripcion}</td>
						<td>${user.rol.descripcion}</td>
						<td>
							<c:choose>
								<c:when test="${user.activo}">
									Activo
								</c:when>
								<c:otherwise>
									Inactivo								
								</c:otherwise>
							</c:choose>
						</td>
						<td><a href='javascript:editarUsuario("${user.clave}")'><img src="<s:url value="/resources/images/edit-file-icon32.png" />" alt="Modificar" height="15px" width="15px"/></a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
</c:if>


<form name="edit" action="<s:url value='/usuarios/edit'/>" method="post">
	<input type="hidden" name="clave"/>
</form>