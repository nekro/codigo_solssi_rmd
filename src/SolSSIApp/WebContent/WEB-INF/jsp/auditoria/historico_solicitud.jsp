<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>

	dojo.require("dijit.Calendar");
	dojo.require("dijit.Dialog");
	
	var userDlg = null;
	
	
	dojo.ready(function(){
	  	userDlg = new dijit.Dialog({ title:"Datos de Usuario"}, "userDlg");
	  	userDlg.startup();
	});
	
	function detalleUser(usuario){
        dojo.xhrPost({
				   url: "<s:url value='/logs/user'/>",
				   content: {"user": usuario},
				   handleAs: "json",
				   load: function (result) {
					   if(result != null){
					   		dojo.byId("claveUsuario").innerHTML = result.clave;
					   		dojo.byId("nombreUsuario").innerHTML = result.nombre;
					   		dojo.byId("rol").innerHTML = result.rol.descripcion;
					   		dojo.byId("dependencia").innerHTML = result.dependencia;
					   		userDlg.show();
					   }		
				  }
			});
    }
	
	function cerrarDetalleUser(){
		userDlg.hide();
	}
</script>


<div class="moduleTitle"><span>Detalle Hist&oacute;ricos</span></div>

	<c:if test="${not empty movimientos}">
		<br/>
		
		<div class="ui-widget-content ui-corner-all" style="width: 80%">
			<table style="width:65%">
				<tr>
					<td class="tag">Folio:</td>
					<td class="valueField"><c:out value='${solicitud.userFolio}'/></td>
				</tr>
				<tr>
					<td class="tag">RFC:</td>
					<td class="valueField"><c:out value='${solicitud.rfc}'/></td>
					<td class="tag">Cuenta:</td>
					<td class="valueField"><c:out value='${solicitud.cuenta}'/></td>
				</tr>
			</table>
		</div>
		
		<br/>

		<div id="tablaMovimientos" align="center" class="ui-widget-content ui-corner-all" style="width: 80%">
			<form name="oficios" method="post">
				<table  class="display" style="width: 100%;padding-top: 30px" align="center">
					<thead class="ui-widget-header">
						<tr>
							<th>Estatus</th>
							<th>Fecha</th>
							<th>Usuario</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
		
					<c:forEach items="${movimientos}" var="control" varStatus="st">
						<c:set var="estatus">${control.estatus}</c:set>
						<c:choose>
					    	<c:when test="${st.count % 2 == 1}">
					    		<c:set var="css" value="odd"/>
					    	</c:when>
					    	<c:otherwise>
					    		<c:set var="css" value="even"/>
					    	</c:otherwise>
					    </c:choose>
		
						<tr class="${css}">
							<td><c:out value="${CATALOGO.ESTATUS_TRAMITE[estatus].valorCampo2}"/></td>
							<td><fmt:formatDate value="${control.fecha}" pattern="dd-MM-yyyy HH:mm:ss"/></td>
							<td align="center">
								<a href="javascript:detalleUser('<c:out value='${control.usuario}'/>')">
									<c:out value='${control.usuario}'/>
								</a>
							</td>
						</tr>
					</c:forEach>
				</table>
			</form>	
		</div>
	</c:if>
	
	<div>
		<br/>
		<button type="button" class="btnEnviar" onclick="history.back()" style="width: 140px"><span><em>Regresar</em></span></button>
	</div>
	
	<div id="userDlg" align="center">
		<div align="center" style="width: 300px">
			<table>
				<tr>
					<td><span>Clave:</span></td>
					<td><span id="claveUsuario" class="valueField"></span></td>
				</tr>	
				<tr>
					<td><span>Nombre:</span></td>
					<td><span id="nombreUsuario" class="valueField"></span></td>
				</tr>
				<tr>
					<td><span>Rol:</span></td>
					<td><span id="rol" class="valueField"></span></td>
				</tr>
				<tr>
					<td><span>Dependencia:</span></td>
					<td><span id="dependencia" class="valueField"></span></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><a href="javascript:cerrarDetalleUser()">Cerrar</a></td>
				</tr>
			</table>
		</div>
	</div>
		
