<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

<link href="<s:url value='/resources/css/dijit/themes/claro/Calendar.css'/>" rel="stylesheet" type="text/css" />

<style>

#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

</style>

<script type="text/javascript">

	dojo.require("dijit.Calendar");
	dojo.require("dijit.Dialog");
	
	var calendarIniDlg = null;
	var calendarFinDlg = null;
	var userDlg = null;
	
	
	dojo.ready(function(){
		calendarIniDlg = new dijit.Dialog({ title:"Seleccione la Fecha"}, "calendarIniDlg");
	  	calendarIniDlg.startup();
	  	
	  	calendarFinDlg = new dijit.Dialog({ title:"Seleccione la Fecha"}, "calendarFinDlg");
	  	calendarFinDlg.startup();
	  	
	  	userDlg = new dijit.Dialog({ title:"Datos de Usuario"}, "userDlg");
	  	userDlg.startup();
	  	
	  	<c:if test="${not empty warning}">
	  		alert("<c:out value='${warning}'/>");
	  	</c:if>
	    
	});
	
	function buscar() {
		document.forms["filtro"].action = "<s:url value='/logs/search'/>";
		submitForm(document.forms["filtro"]);
	}
	
	function showCalendarIni(){
		
		calendarIniDlg.show();
		
		var calIni = new dijit.Calendar(new Date(), dojo.byId("calFechaInicio"));
	    dojo.connect(calIni, "onValueSelected", function(date){
	    	var fmt = dojo.date.locale.format(date, {datePattern: "dd/MM/yyyy", selector: "date"});
	    	dojo.byId("fechaInicio").value = fmt;
	    	calendarIniDlg.hide();
	    });
	}
	
	function showCalendarFin(){
	
		calendarFinDlg.show();
		
		var calFin = new dijit.Calendar(new Date(), dojo.byId("calFechaFin"));
	    dojo.connect(calFin, "onValueSelected", function(date){
	    	var fmt = dojo.date.locale.format(date, {datePattern: "dd/MM/yyyy", selector: "date"});
	    	dojo.byId("fechaFin").value = fmt;
	    	calendarFinDlg.hide();
	    });
	}
	
	function cerrarIni(){
		calendarIniDlg.hide();
	}
	
	function cerrarFin(){
		calendarFinDlg.hide();
	}
	
	function detalleUser(usuario){
        dojo.xhrPost({
				   url: "<s:url value='/logs/user'/>",
				   content: {"user": usuario},
				   handleAs: "json",
				   load: function (result) {
					   dojo.byId("claveUsuario").innerHTML = result.clave;
					   dojo.byId("nombreUsuario").innerHTML = result.nombre;
					   dojo.byId("rol").innerHTML = result.rol.descripcion;
					   dojo.byId("dependencia").innerHTML = result.dependencia;
					   userDlg.show();
				  }
			});
    }
	
	function cerrarDetalleUser(){
		userDlg.hide();
	}
	
</script>


<div class="moduleTitle"><span>Consulta de Movimientos</span></div>
	<div id="filtro" style="width: 90%" align="center">
		<form name="filtro" method="post">
			<table class="ui-widget-content ui-corner-all">
				<tr >
					<td align="center"><span class="titleSeccion">Filtro de B&uacute;squeda</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<ul>
							<li>
								<span>Fecha Inicio: de </span> 
								<input name="fechaInicio" type="text" id="fechaInicio" onclick="showCalendarIni()" size="10" />
								a
								<input name="fechaFin" type="text" id="fechaFin" onclick="showCalendarFin()" size="10"/>
							</li>
							<li>
								Movimiento:
								<select name="movimiento" style="width:200px">
									<option value="0">TODOS</option>
									<c:forEach items="${CATALOGO.MOVIMIENTOS}" var="cat">
										<option value="${cat.value.claveItem}" ${selected}>${cat.value.descripcion}</option>
									</c:forEach>
								</select>
							</li>
							<li>
								<button type="button" class="btnEnviar" onclick="buscar()"><span><em>Buscar</em></span></button>
							</li>		
	
						</ul>
					</td>
				</tr>
			</table>
		</form>
	</div>
	
<c:if test="${not empty movimientos}">
	
		<br/>
		<h4>Resultados <c:out value="${fechaInicio}"/> - <c:out value="${fechaFin}"/></h4>
		<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 98%">
		
		<table class="display">
			<thead>
			<tr class="ui-widget-header  ui-corner-all">
				<th align="center">Fecha Movimiento</th>
				<th align="center">Movimiento</th>
				<th align="center">Usuario</th>
				<th align="center">Detalle</th>
			</tr>
			</thead>
			<tbody>
			<c:forEach items="${movimientos}" var="mov" varStatus="st">
				<c:set var="claveMovimiento"><c:out value="${mov.movimiento}"/></c:set>
				<c:choose>
					    	<c:when test="${st.count % 2 == 1}">
					    		<c:set var="css" value="odd"/>
					    	</c:when>
					    	<c:otherwise>
					    		<c:set var="css" value="even"/>
					    	</c:otherwise>
				</c:choose>
				<tr class="${css}">
					<td align="center"><fmt:formatDate value="${mov.fecha}" pattern="dd/MM/yyyy HH:mm:ss"/></td>
					<td align="center"><c:out value='${CATALOGO.MOVIMIENTOS[claveMovimiento].descripcion}'/></td>
					<td align="center">
						<a href="javascript:detalleUser('<c:out value='${mov.usuario}'/>')">
							<c:out value='${mov.usuario}'/>
						</a>
					</td>
					<td align="center"><c:out value='${mov.detalle}'/></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
			
		</div>
		<br/>
</c:if>

<c:if test="${not empty error}">
	<br/><br/>
	<div class="ui-state-error ui-corner-all" style="width: 50%">
		<img src="<s:url value='/resources/images/rechazar32.png'/>">
		<br/>
		<span class="error"><c:out value="${error}"/></span>	
	</div>
</c:if>

<form name="proceso" method="post">
	<input type="hidden" name="idProceso"/>
	<input type="hidden" name="estatus"/>
</form>

<div id="calendarIniDlg" align="center">
	<div id="calFechaInicio"></div>
	<a href="javascript:cerrarIni()">Cerrar</a>
</div>

<div id="calendarFinDlg" align="center">
	<div id="calFechaFin"></div>
	<a href="javascript:cerrarFin()">Cerrar</a>
</div>

<div id="userDlg" align="center">
	<div align="center" style="width: 300px">
		<table>
			<tr>
				<td><span>Clave:</span></td>
				<td><span id="claveUsuario" class="valueField"></span></td>
			</tr>	
			<tr>
				<td><span>Nombre:</span></td>
				<td><span id="nombreUsuario" class="valueField"></span></td>
			</tr>
			<tr>
				<td><span>Rol:</span></td>
				<td><span id="rol" class="valueField"></span></td>
			</tr>
			<tr>
				<td><span>Dependencia:</span></td>
				<td><span id="dependencia" class="valueField"></span></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><a href="javascript:cerrarDetalleUser()">Cerrar</a></td>
			</tr>
		</table>
	</div>
</div>


