<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Solicitud PDF</title>
</head>

<frameset rows="45,*" frameborder="0" border="0" framespacing="0">
	<frame name="menu" src="<s:url value='/solicitud/pdf/menu'/>" marginheight="0" marginwidth="0" scrolling="no" noresize>
	<frame name="content" src="<s:url value='/report/service'/>" marginheight="0" marginwidth="0" scrolling="auto" noresize>

<noframes>
<p>El navegador no soporta FRAMES</p>
</noframes>

</frameset>
</html>	