<%@ page contentType="text/html" isELIgnored="false"%>
<%@ page buffer="40kb" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<s:url value="/solicitud/procesa" var="urlProcesa"/>

<style>
input{
	text-transform: uppercase;
}

<s:hasBindErrors name="solicitud">
	#containerSolicitud td{
		vertical-align: top;
	}
	
	.error{
		position: relative;
		top: -3px; 
	}
</s:hasBindErrors>

}

</style>
		
<script>
		 
		var windowDependiente = null;
		var windowErrores = null;
		//var avisoDialog;
		//dojo.require("dijit.form.Button");
		//dojo.require("dijit.Dialog");
		
		
		dojo.ready(function(){
		
			  	//avisoDialog = new dijit.Dialog({ title:"Aviso de Privacidad", style: "width: 700px;heigth:500px;text-align:center" }, "divAvisoPrivacidad");
  				//avisoDialog.startup();
			    
			    <c:if test="${solicitud.contribuyenteEUA}">
		 			js_solicitud.showNSS(true);
		 	    </c:if>
		 	   
		 		<c:if test="${solicitud.idPaisNac gt 0}">
		 			js_solicitud.checkPaisNacimiento(<c:out value="${solicitud.idPaisNac}"/>, false);
		 		</c:if>
		 		
		 		<c:if test="${solicitud.idEstado gt 0}">
		 			loadMunicipios(dojo.byId("idEstado"), <c:out value="${solicitud.idMunicipio}"/>);
	 			</c:if>
	 			
	 			
		 		<c:if test="${solicitud.idPais gt 0}">
		 			checkPaisDomicilio(<c:out value="${solicitud.idPais}"/>);
				</c:if>
					
				dojo.byId("captcha").value = '';
				
				validaIdentificacion(dojo.byId("cveTipoIdentificacion"), false);
				
				<c:if test="${validCaptcha == 'false'}">
					dojo.byId("btnGenerar").focus();
				</c:if>
				
				<c:if test="${refresh == 'true'}">
					dojo.byId("btnGenerar").focus();
				</c:if>
				
				<c:if test="${not empty solicitud.correoPersonal}">
					js_solicitud.showCorreoPersonal(dojo.byId("correoPersonal"));
				</c:if>
				
				<c:if test="${not empty solicitud.correoTrabajo}">
					js_solicitud.showCorreoTrabajo(dojo.byId("correoTrabajo"));
				</c:if>
				
				<c:if test="${solicitud.aplicaFormato140}">
				    document.getElementById("divDependientesList").style.display = "";
				</c:if>
				
				<c:if test="${not solicitud.aplicaFormato140}">
			        document.getElementById("divDependientesList").style.display = "none";
			    </c:if>
			    
			    <c:if test="${solicitud.modoPago eq 2}">
		 			js_solicitud.otrasFormasPago(dojo.byId("modoPago2"));
	 			</c:if>
	 			
	 			<c:if test="${solicitud.cheque}">
		 			js_solicitud.modoPagoCheque(dojo.byId("cheque"));
	 			</c:if>
	 			
	 			<c:if test="${solicitud.estadoPromotoria gt 0}">
		 			loadPromotorias(dojo.byId("estadoPromotoria"), <c:out value="${solicitud.clavePromotoria}"/>);
	 			</c:if>
	 			
				
		});
		
		function verErrores(){
			    windowErrores = openWindowCenter("","errores",600,400);
		        document.forms["solicitud"].target = "errores";
			    document.forms["solicitud"].action = "<s:url value='/solicitud/error'/>";
			    document.forms["solicitud"].submit();
		}
		
		function actualizarCaptcha(){
			var d = new Date(); 
			dojo.byId("imgCaptcha").src = "<s:url value='/solicitud/captcha?d='/>"+d.getTime();
		}
		
		
		function loadMunicipios(estado, municipio){
			 loadSubCatalogo(dojo,"<s:url value='/catalogo/municipios/'/>"+estado.value, "idMunicipio", municipio);
		}
		
		function loadPromotorias(estado, promotoria){
  		    var msgError = "No se encontr� informacion para el estado seleccionado"; 
			loadSubCatalogo(dojo,"<s:url value='/catalogo/promotorias/'/>"+estado.value, "clavePromotoria", promotoria, msgError);
		}
		
		function dependientes() {
				
				if(document.getElementById('idNacionalidad').value!='1' 
						&& document.getElementById('idNacionalidad').value!='0'
						&& document.getElementById('politicamenteExpuestaSi').checked)
				{
					windowDependiente = openWindowCenterScroll("","dep",1000,600);
					if(windowDependiente != null){
						windowDependiente.focus();
						document.forms["solicitud"].action = "<s:url value='/solicitud/dependientes'/>";
						document.forms["solicitud"].target = "dep";
						document.forms["solicitud"].submit();
					}else{
						alert("<s:message code='window.popup.blocked' htmlEscape='true'/>");
						cerrarSession = false;
					}	
				}
				
		 };
		 
		 
		 function showAvisoPrivacidad(){
			avisoDialog.show();
		 }
		 
		 function enviarSolicitud(){
	 	 
		 	 if(windowDependiente != null && !windowDependiente.closed){
		 		windowDependiente.close();
		 	 }
		 	 
		 	 if(windowErrores != null && !windowErrores.closed){
		 		windowErrores.close();
		 	 }
			 
			 js_solicitud.showMessageAvisoPrivacidad(document.forms["solicitud"].avisoPrivacidad.checked);
			 
			 if( document.forms["solicitud"].avisoPrivacidad.checked){
				 document.getElementById("avisoPrivacidadRequired").innerHTML = "&nbsp;";
				 document.forms["solicitud"].action = "${urlProcesa}";
				 document.forms["solicitud"].target = "_top";
				 submitForm(document.forms["solicitud"]);
			 }
		 
	 	}
		 
</script>
<div class="moduleTitle"><span>Captura de Solicitud</span></div>
<div id="containerSolicitud" align="center">      
<form:form name="solicitud" id="solicitud" modelAttribute="solicitud" method="post" action="${urlProcesa}" autocomplete="on">
     
    <input type="hidden" name="tipoTramite" value="1">

    <c:set var="tabIndex" value="1" scope="request"/>

	<!--  Table esquema 1Header&1RowX5Columns -->
	<s:hasBindErrors name="solicitud">
		<div class="ui-widget-content ui-state-error ui-corner-all seccion-solicitud" id="divErrores">
			<br/>
			<table width="100%">
				<tr>
					<td align="center">
						<span><s:message code="solicitud.errores.captura" htmlEscape="true"/></span>
					</td>
				</tr>	
			</table>
			<br/>
		</div>
		<br/>
	</s:hasBindErrors>	
	
	<jsp:include page="./secciones/tipo_tramite.jsp"/>

	<jsp:include page="./secciones/datos_generales.jsp"/>
	
    <jsp:include page="./secciones/datos_contacto.jsp"/>
    
    <jsp:include page="./secciones/politicamente_expuesto.jsp"/>
    
    <jsp:include page="./secciones/modo_pago.jsp"/>
   
    <jsp:include page="./secciones/verificacion.jsp"/>
	
	<div class="ui-state-active ui-corner-all seccion-solicitud">
		<br/>
		<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
		<form:checkbox path="avisoPrivacidad" value="true" onchange="showMessageAvisoPrivaidad(this.value)" tabIndex="${tabIndex}" />
		<span>Acepto y autorizo que mis datos personales, patrimoniales o financieros y sensibles <br/> sean tratados conforme al </span> <a href="http://www.metlife.com.mx/wps/portal/seguros/QuienesSomos?Menu=7&tabT=1" target="_blank" style="text-decoration: underline;color: blue">aviso de privacidad</a>
		<div id="avisoPrivacidadRequired" class="msgAvisoPrivacidad">&nbsp;</div>
	</div>
	
	<br/>
	
	<div>
		<span style="color:#000;font-weight:bold"><s:message code="solicitud.responsabilidad.asegurado" htmlEscape="true"/></span>
		<br/><br/>
		<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
		<button type="button" class="btnEnviar" onclick="enviarSolicitud()" id="btnGenerar" style="width: 150px" tabindex="${tabIndex}"><span><em><s:message code="button.solicitud.generar" htmlEscape="true"/></em></span></button>
	</div>
	<br/>
	<br/>
	
</form:form>

</div>

