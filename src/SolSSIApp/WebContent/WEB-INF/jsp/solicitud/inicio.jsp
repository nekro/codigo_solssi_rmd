<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<style>
input{
	text-transform: uppercase;
}

<s:hasBindErrors name="solicitud">
	#containerSolicitud td{
		vertical-align: top;
	}
	
	.error{
		position: relative;
		top: -3px; 
	}
</s:hasBindErrors>

}

</style>

<script>
		
		function enviarDatos(){
			var result = doValidate('formInicioTramite');
			if(result == true){
				submitForm(document.forms["inicio"]);	
		    }
		 }
		
		 function cancelModificacion(){
			 document.forms["formConfirmar"].action = "<s:url value='/clientes/noModificar'/>";
			 submitForm(document.forms["formConfirmar"]);
		 }
		 
		 function confirmModificacion(){
			 document.forms["formConfirmar"].action = "<s:url value='/clientes/siModificar'/>";
			 submitForm(document.forms["formConfirmar"]);
		 }
		 
		 function acrobatReader(){
			openWindowCenter("http://get.adobe.com/es/reader/","Descargar",800,400);
		}
		
		function verErrores(){
			    windowErrores = openWindowCenter("","errores",600,400);
			    document.forms["errores"].action = "<s:url value='/solicitud/error'/>";
			    document.forms["errores"].submit();
		}
		
</script>

<s:url value="/clientes/busqueda" var="urlConsulta"/>

<div class="moduleTitle"><span>Inicio de Tr&aacute;mite</span></div>
        
<c:if test="${not cliente.reCaptura}">
		<div class="" align="center" style="width: 675px;height: 450px">
		
			<div class="ui-widget-content ui-corner-all ui-state-active" align="left">
				<p	style="padding: 15px 25px 15px 25px" align="justify">
					<s:message code="inicio.leyenda1" htmlEscape="true"/> 
					<a href="javascript:acrobatReader()">aqu&iacute;</a>
					<br/><br/>
					<s:message code="inicio.leyenda2" htmlEscape="true"/>
				</p>
			</div>  
			
			<br/>    
			
			<form:form name="inicio" id="inicio" modelAttribute="cliente" method="post" action="${urlConsulta} " autocomplete="on">
			
				<table cellspacing="0" class="ui-widget-content ui-corner-all" style="width: 100%;min-height: 175px" >
					<tbody>
						<tr>
						<td colspan="4" style="padding: 15px 5px 15px 5px">
				            <h5 style="padding: 5px 5px 15px 25px" align="left"><s:message code="inicio.leyenda4" htmlEscape="true"/></h5>
						</td>
						</tr>
						
						
						<tr>
							<td class="tag" width="25%"><label for="rfc">RFC-Homoclave:</label></td>
							<td class="control" width="25%">
							    <table> 
							    	<tbody>
							    		<tr>
							    			<td>
							    				<form:input title="Capture RFC" path="rfc" maxlength="10" onkeypress="return enableAlfaNumero(event);" onchange="validRFC(this);" cssStyle="width: 90px"/>
											</td>
						        			<td>
							    				<form:input path="homoclave" maxlength="3" onkeypress="return enableAlfaNumero(event)" onchange="validRFCHomoclave(this);" cssStyle="width: 40px"/>
											</td>
						                </tr>
						              </tbody>
						          </table>
						    </td>
						
							<td width="30%" align="right"><label for="cuenta">N&uacute;mero de Cuenta SSI:</label></td>
							<td width="20%" align="left"><form:input path="cuenta" maxlength="10" onkeypress="return enableNumero(event)" onchange="validCuenta(this);" cssStyle="width: 90px" />
							</td>
				
						</tr>
						
				        <!--  Fila error -->		
						<tr>
							<td colspan="2" align="center" height="15px"><span id="error_rfc" class="error"/></td>
							<td colspan="2" align="left"" height="15px"><span id="error_cuenta" class="error"/></td>
						</tr>
						<tr>
							<td colspan="2" align="center"" height="15px"><span id="error_homoclave" class="error"/></td>
						</tr>
										
				        <tr>
									 <td colspan="4" align="center" style="padding-bottom: 15px">
										 <br/>
										 <button type="button" class="btnEnviar" onclick="enviarDatos()"><span><em>Entrar</em></span></button>
										 <br/>
									 </td>
					     </tr>
					</tbody>
				</table>
				<s:hasBindErrors name="cliente">
					<br/>
						<div class="ui-widget-content ui-state-error ui-corner-all" style="width: 100%;padding-top: 5px;padding-bottom:  5px">
						<div>
							<form:errors path="*" />
							<c:if test="${(not empty errorCuenta)}">
                        		<br/>
			                  	<a href="javascript:verErrores()" style="text-decoration: underline;color: blue; position: relative;top: -3px">Imprimir error</a>
							</c:if>
						
						</div>
					</div>
				</s:hasBindErrors>
				
			</form:form>
		</div>
		
</c:if>

<c:if test="${cliente.reCaptura}">
	<div id="containerInicio" align="center" style="width: 675px">
		<br/>
		<br/>
	    <form name="formConfirmar" method="post" >
			<table class="ui-widget-content ui-corner-all" style="width: 100%" align="center">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td align="center">
						<span class="leyenda">
							<s:message code="solicitud.existe.registrada"/><br/>
						</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<button type="button" class="btnEnviar" onclick="confirmModificacion()"><span><em>Si</em></span></button>
						<button type="button" class="btnEnviar" onclick="cancelModificacion()"><span><em>No</em></span></button>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</form>
	</div>	
</c:if>

<form name="errores" target="errores" method="post"></form>
