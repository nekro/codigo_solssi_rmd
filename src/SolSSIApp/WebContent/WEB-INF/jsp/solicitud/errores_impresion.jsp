<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE link PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

		<link href="<s:url value="/resources/css/metlife/screen.css" />" rel="stylesheet" type="text/css" />
		<link href="<s:url value="/resources/css/ui/estilos_ui.css" />" rel="stylesheet" type="text/css" />
		<link href="<s:url value="/resources/css/solssi.css" />" rel="stylesheet" type="text/css" />
		<link href="<s:url value="/resources/css/tablas.css" />" rel="stylesheet"  type="text/css" />

		<script type="text/javascript" src="<s:url value="/resources/js/ssi_library_1.0.0.js"/>"></script>
		<script type="text/javascript" src="<s:url value="/resources/js/dojo/dojo.xd.js"/>"></script>
		
	<script>
		function imprimir(){
			try{
				window.print();
			}catch(e){}
		}
	</script>
	
	<style type="text/css">
		td{
			border:1px solid #ddd;
		}
		
		table{
			border:1px solid gray;
			font-size: 14px;
			font-family: sans-serif;
			color: black;
		}
	</style>
	
	<style type="text/css" media="print">
		.noImprimir{
			display: none;
		}
	
	</style>
	
</head>

<body style="background-image: none;background-color: #fff">	
	
	<div style="width: 100%" align="center">
		<div style="width: 90%" align="center">
		
			<div align="left">
				<img src="<s:url value="/resources"/>/images/logoblanco2.png"/>
			</div>
			
			<div>
				<span style="color:red;font-size:16px">
					<s:message code="solicitud.messagePrintWindow"/>
				</span>
			</div>
			<br/>
			<div align="left" style="width: 80%">
				RFC:<b><c:out value="${solicitud.rfc}"/></b><br/>
				Cuenta SSI:<b><c:out value="${solicitud.cuenta}"/></b>
			</div>
			
	
			<div align="center" class="popupWindow">
			   <br/>
			   <br/>
			   <table cellspacing="0" cellpadding="0" width="80%">
			   		<tr style="background-color: #eaeaea">
				   			<td width="15%" align="center">
				   				<b>Campo</b>
				   			</td>
				   			<td width="15%" align="center">
				   				<b>Valor Capturado</b> 
				   			</td>
				   			<td align="center">
				   				<b>Mensaje de error</b>
				   			</td>
				   	</tr>
			   		<c:if test="${not empty errorPorcentaje}">
				   		<tr>
				   			<td align="center">
				   				Porcentaje
				   			</td>
				   			<td align="center">
				   				<c:out value="${solicitud.porcentaje}"/>
				   			</td>
				   			<td align="left">
				   				<c:out value="${errorPorcentaje}"/>
				   			</td>
				   		</tr>
			   		</c:if>
			   		<c:if test="${not empty errorClabe}">
				   		<tr>
				   			<td align="center">
				   				CLABE
				   			</td>
				   			<td align="center">
				   				<c:out value="${solicitud.clabe}"/>
				   			</td>
				   			<td align="left">
				   				<c:out value="${errorClabe}"/>
				   			</td>
				   		</tr>
			   		</c:if>
			   </table>
			   
			   <div class="noImprimir">
			   		<br/><br/>
			   		<button type="button" class="btnEnviar" onclick="window.close()" id="btnGenerar"><span><em>Corregir</em></span></button>
			   		<button type="button" class="btnEnviar" onclick="window.close()" id="btnGenerar"><span><em>Cerrar</em></span></button>
			   		<button type="button" class="btnEnviar" onclick="imprimir()" id="btnGenerar"><span><em>Imprimir</em></span></button>
			   </div>
			</div>
		</div>	
	</div>
</body>				
</html>