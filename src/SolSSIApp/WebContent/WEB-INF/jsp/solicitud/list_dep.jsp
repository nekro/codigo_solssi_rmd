<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE link PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<link href="<s:url value="/resources/css/metlife/screen.css" />" rel="stylesheet" type="text/css" />
		<link href="<s:url value="/resources/css/ui/estilos_ui.css" />" rel="stylesheet" type="text/css" />
		<link href="<s:url value="/resources/css/solssi.css" />" rel="stylesheet" type="text/css" />
		<link href="<s:url value="/resources/css/tablas.css" />" rel="stylesheet"  type="text/css" />

		<script type="text/javascript" src="<s:url value="/resources/js/ssi_library_1.0.0.js"/>"></script>
		<script type="text/javascript" src="<s:url value="/resources/js/dojo/dojo.xd.js"/>"></script>
		
	<script>
		
	    <c:if test="${not empty DECLARACION_NO_DEPENDIENTES}">
	          window.close();
	    </c:if>
	
	    function insertDependiente(){
	    	document.forms["formOptions"].action = '<s:url value="/solicitud/dependientes/form"/>';
			document.forms["formOptions"].submit();
		}
		
		function declaracionNoDE(check){
			if(check.checked){
				document.getElementById("btnAgregar").style.display = "none";
				document.getElementById("btnAceptar").style.display = "";
			}else{
				document.getElementById("btnAgregar").style.display = "";
				document.getElementById("btnAceptar").style.display = "none";
			}
		}
		
		function submitDeclaracionNoDE(){
			document.forms["formOptions"].action = '<s:url value="/solicitud/dependientes/nodependientes"/>';
			document.forms["formOptions"].submit();
		}
		
		function submitFinCapturaDependientes(){
			window.opener.document.getElementById("divDependientesList").style.display = "";
			window.close();
		}
		
		
		function editarDependiente(numero){
			document.forms["formOptions"].action = '<s:url value="/solicitud/dependientes/edit"/>/'+numero;
			document.forms["formOptions"].submit();
		}
		
		function eliminarDependiente(numero){
			if(confirm("�Est� seguro que desea eliminar el dependiente?")){
				document.forms["formOptions"].action = '<s:url value="/solicitud/dependientes/remove"/>/'+numero;
				document.forms["formOptions"].submit();
			}
		}
		
		

	</script>
</head>
<body style="background-image: none;background-color: #fff">	

<div style="width: 100%" align="center">	
	<div align="center" class="popupWindow" style="width: 95%">
	   <div class="welcomeLeft">
		 <br/>
		 <table width="95%">
		 <tr>
		  <td width="15px">&nbsp;</td>
		  <td align="left" valign="bottom" width="50%">
		    <img src="<s:url value="/resources"/>/images/logoblanco2.png"/>
		  </td>
		  <td align="right" valign="bottom">
			   &nbsp;
		  </td>
		 </tr>  
		 </table> 
		</div>
	
		<div align="center" class="ui-widget-content ui-corner-all">      
	
			<div class="moduleTitle"><span>Captura de Dependientes Econ&oacute;micos</span></div>
				<table width="75%" align="center">
					<c:if test="${empty solicitud.dependientes}">
					    <tr>
							<td colspan="2" align="left" id="declaracionNoDE">
								<br/>
								<p style="text-align: justify;">
									<s:message code="dependientes.leyenda1" htmlEscape="true"/>
								</p>
							    <br/>
							</td>
						</tr>
						
					</c:if>
					
					<tr>
						<td colspan="2" align="center">
						    <form name="formOptions" action="${insertUrl}" method="post">
								<button type="button" class="btnEnviar" onclick="insertDependiente()" id="btnAgregar" style="width: 200px"><span><em>Agregar Dependientes</em></span></button>
								
								<c:if test="${not empty sessionScope.solicitud.dependientes}">
									<button id="btnCerrar" type="button" class="btnEnviar" onclick="submitFinCapturaDependientes()"><span><em>Terminar</em></span></button>
								</c:if>
							    <c:if test="${empty sessionScope.solicitud.dependientes}">
							    	<button type="button" class="btnEnviar" style="width:175px" onclick="submitDeclaracionNoDE()" id="btnAceptar"><span><em>Sin Dependientes</em></span></button>
							    </c:if>
							</form>
						    
						</td>
					    
					</tr>
			</table>
			
			<div align="center">
					<c:if test="${not empty sessionScope.solicitud.dependientes}">
						<span class="moduleTitle">Lista de Dependientes Econ&oacute;micos Capturados</span>
									<table class="ui-widget-content ui-corner-all display" style="width: 75%" align="center">
									  <thead class="ui-widget-header">
										<tr>
											<th>
												No.
											</th>
											<th>
												NOMBRE
											</th>
											<th>
												CAR&Aacute;CTER
											</th>
											<th>
												MODIFICAR
											</th>
											<th>
												ELIMINAR
											</th>
										</tr>
									</thead>	
										<c:forEach var="dep" items="${solicitud.dependientes}" varStatus="st">
										    <c:choose>
										    	<c:when test="${st.count % 2 == 1}">
										    		<c:set var="css" value="odd"/>
										    	</c:when>
										    	<c:otherwise>
										    		<c:set var="css" value="even"/>
										    	</c:otherwise>
										    </c:choose>
											<tr class="${css}">
												<td>
												    ${st.count}
												</td>
												<td>${dep.nombre} ${dep.apePaterno} ${dep.apeMaterno}</td>
												<td>
													<c:choose>
														<c:when test="${dep.caracter eq 1}">
															CONYUGE O CONCUBINO
														</c:when>
														<c:when test="${dep.caracter eq 2}">
															DEPENDIENTE ECON&Oacute;MICO
														</c:when>
													</c:choose>
												</td>
												
												<td>
												    <a href='javascript:editarDependiente(${dep.numero})'><img src="<s:url value="/resources/images/edit-file-icon32.png" />" alt="Modificar" height="15px" width="15px"/></a>
												</td>
												
												<td><a href='javascript:eliminarDependiente(${dep.numero})'><img src="<s:url value="/resources/images/rechazar32.png" />" alt="Eliminar" height="15px" width="15px"/></a></td>
											    
											</tr>
										</c:forEach>
									</table>
									<br/><br/><br/>
						</c:if>					
			</div>
			
		</div>
		
	</div>
</div>	
</body>				
</html>