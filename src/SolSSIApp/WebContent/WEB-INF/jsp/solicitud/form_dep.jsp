<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	    
	    <link href="<s:url value="/resources/css/metlife/screen.css" />" rel="stylesheet" type="text/css" />
		<link href="<s:url value="/resources/css/ui/estilos_ui.css" />" rel="stylesheet" type="text/css" />
		<link href="<s:url value="/resources/css/solssi.css" />" rel="stylesheet" type="text/css" />
		<link href="<s:url value="/resources/css/tablas.css" />" rel="stylesheet"  type="text/css" />
		
	    <style>
			input{
				text-transform: uppercase;
			}
			
			<s:hasBindErrors name="dependienteEconomico">
				#containerSolicitud td{
					vertical-align: top;
				}
				
				.error{
					position: relative;
					top: -3px; 
				}
			</s:hasBindErrors>
			
			}

        </style>

		<script type="text/javascript" src="<s:url value="/resources/js/ssi_library_1.0.0.js"/>"></script>
		<script type="text/javascript" src="<s:url value="/resources/js/dojo/dojo.xd.js"/>"></script>
		<script>
	
		dojo.ready(function(){
			  
		 		<c:if test="${dependienteEconomico.idPaisNac gt 0}">
		 			checkPaisNacimiento(<c:out value="${dependienteEconomico.idPaisNac}"/>);
		 		</c:if>
		 		
		 		<c:if test="${dependienteEconomico.idEstado gt 0}">
		 			loadMunicipios(dojo.byId("idEstado"), <c:out value="${dependienteEconomico.idMunicipio}"/>);
	 			</c:if>
	 			
	 	 		<c:if test="${dependienteEconomico.idPais gt 0}">
		 			checkPaisDomicilio(<c:out value="${dependienteEconomico.idPais}"/>);
				</c:if>
				
				<c:if test="${not empty dependienteEconomico.correoPersonal}">
				    js_solicitud.showCorreoPersonal(dojo.byId("correoPersonal"));
		   	    </c:if>
			
			    <c:if test="${not empty dependienteEconomico.correoTrabajo}">
				    js_solicitud.showCorreoTrabajo(dojo.byId("correoTrabajo"));
			    </c:if>
					
				validaIdentificacion(dojo.byId("cveTipoIdentificacion"), false);
					
		  });
	
	function loadMunicipios(estado, municipio){
		 
		 var select = dojo.byId("idMunicipio");
		 //var text = select.get(0).options[0].text;
		 
		 dojo.xhrGet({
			   url: "<s:url value='/catalogo/municipios/'/>"+estado.value,
			   handleAs: "json", ///
			   load: function (result) {
				   select.options.length = 0;
				   dojo.forEach(result, function(field, i){
					   select.options[select.options.length] = new Option(field.descripcion, field.claveItem);
				   });
				   
				   if(municipio > 0){
					   select.value = ''+municipio;
				   }
			   }
			});
	}
			
	
		function saveDependiente() {
			document.forms["dependiente"].action = '<s:url value="/solicitud/dependientes/insert"/>';
			document.forms["dependiente"].submit();
		}
	
		function cancelarDependiente() {
			document.forms["cancel"].action = '<s:url value="/solicitud/dependientes/list"/>';
			document.forms["cancel"].submit();
		}
		
		function setMunicipio(estado, municipio){
			 
			var select = dojo.byId("idMunicipio");
			 //var text = select.get(0).options[0].text;
			 
			 dojo.xhrGet({
				   url: "<s:url value='/catalogo/municipios/'/>"+estado.value,
				   handleAs: "json", ///
				   load: function (result) {
					   select.options.length = 0;
					   dojo.forEach(result, function(field, i){
						   select.options[select.options.length] = new Option(field.descripcion, field.clave.clave);
					   });
					   select.value = municipio;
				   }
				});
		 }
		
		function copyDomicilio(band){
			if(band){
				document.forms["dependiente"].calle.value = "<c:out value='${solicitud.calle}'/>";
				document.forms["dependiente"].codigoPostal.value = "<c:out value='${solicitud.codigoPostal}'/>";
				document.forms["dependiente"].ciudad.value = "<c:out value='${solicitud.ciudad}'/>";
				document.forms["dependiente"].idPais.value = <c:out value="${solicitud.idPais}"/>;
				document.forms["dependiente"].idEstado.value = <c:out value="${solicitud.idEstado}"/>;
				
				<c:if test="${not empty solicitud.numExt}">
					document.forms["dependiente"].numExt.value = "<c:out value='${solicitud.numExt}'/>";
				</c:if>
				<c:if test="${not empty solicitud.numInt}">
					document.forms["dependiente"].numInt.value = "<c:out value='${solicitud.numInt}'/>";
				</c:if>
				<c:if test="${not empty solicitud.telefonoCasa}">
					document.forms["dependiente"].telefonoCasa.value = "<c:out value='${solicitud.telefonoCasa}'/>";
				</c:if>
				
				<c:if test="${solicitud.idPais gt 1}">
					checkPaisDomicilio(<c:out value="${solicitud.idPais}"/>);
				</c:if>
				
				if(document.forms["dependiente"].idEstado.value > 0){
					setMunicipio(document.forms["dependiente"].idEstado , <c:out value="${solicitud.idMunicipio}"/>);
				}
				document.forms["dependiente"].idMunicipio.value = <c:out value="${solicitud.idMunicipio}"/>;
				document.forms["dependiente"].colonia.value = "<c:out value='${solicitud.colonia}'/>";
				document.forms["dependiente"].estadoExtranjero.value = "<c:out value='${solicitud.estadoExtranjero}'/>";
				document.forms["dependiente"].municipioExtranjero.value = "<c:out value='${solicitud.municipioExtranjero}'/>";
			}else{
				document.forms["dependiente"].calle.value = "";
				document.forms["dependiente"].codigoPostal.value = "";
				document.forms["dependiente"].ciudad.value = "";
				document.forms["dependiente"].idPais.value = 0;
				document.forms["dependiente"].idEstado.value = 0;
				document.forms["dependiente"].numExt.value = "";
				document.forms["dependiente"].numInt.value = "";
				document.forms["dependiente"].telefonoCasa.value = "";
				document.forms["dependiente"].idMunicipio.value = 0;
				document.forms["dependiente"].colonia.value = "";
				document.forms["dependiente"].estadoExtranjero.value = "";
				document.forms["dependiente"].municipioExtranjero.value = "";
			}
			
		}
		
		function checkPaisNacimiento(value){
			 if(value == 1){
				 document.getElementById("divEstadoNacExtranjero").style.display = "none";
				 document.getElementById("divEstadoNacMexico").style.display = "";
			 }else{
				 document.getElementById("divEstadoNacMexico").style.display = "none";
				 document.getElementById("divEstadoNacExtranjero").style.display = "";
			 }
			 
			 var comboNacionalidad = document.getElementById("idNacionalidad");
			 comboNacionalidad.value = document.getElementById("idPaisNac").value;
			 
		}
		
	</script>
</head>
<body style="background-image: none;background-color: #fff">
<div style="width:100%" align="center">
	<div class="popupWindow" align="center">
		<div align="center">
			
			<div class="welcomeLeft">
			 <br/>
			 <table width="95%">
			 <tr>
			  <td width="15px">&nbsp;</td>
			  <td align="left" valign="bottom" width="50%">
			    <img src="<s:url value="/resources"/>/images/layout/logo.gif"/>
			  </td>
			  <td align="right" valign="bottom">
				   &nbsp;
			  </td>
			 </tr>  
			 </table> 
			</div>
			
			<div align="center" id="containerSolicitud">
				
				<div class="moduleTitle"><span>Captura de Dependientes Econ&oacute;micos</span></div>
				
				<form:form modelAttribute="dependienteEconomico" name="dependiente">
				    <c:set var="tabIndex" value="1"/>
					<s:hasBindErrors name="dependienteEconomico">
						<div class="ui-widget-content ui-state-error ui-corner-all seccion-solicitud">
							<br/>
							<span>Se encontraron errores en la captura, favor de corregir para continuar.</span>
							<br/><br/>
						</div>
						<div style="display: none;">
							<form:errors path="*"/>
						</div>
						<br/>
					</s:hasBindErrors>
				
					<form:hidden path="numero"/>
		            <div align="left" class="seccion">Datos Generales de Asegurado</div>
					<table class="ui-widget-content ui-corner-all seccion-solicitud">
						<tbody>
							   
				            
				            <!-- Subseccion datos personales -->
					        <tr>
								<td colspan="2" align="left"><span class="subseccion">Datos Personales</span></td>
								<td colspan="2">&nbsp;</td>
							</tr>
				            <tr>
				            	<td class="tag"><label>Car&aacute;cter:</label>
								</td>
								<td class="control" colspan="3">
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
                                    <form:radiobutton path="caracter" value="1" tabindex="${tabIndex}" /><label  for="caracter1" style="font-size: 12px">C&oacute;nyuge o Concubino</label>
									<c:set var="tabIndex" value="${tabIndex+1}"/>
									<form:radiobutton path="caracter" value="2" tabindex="${tabIndex}" /><label  for="caracter2" style="font-size: 12px">Dependiente econ&oacute;mico</label>
									<s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="caracter" cssClass="error"/></s:hasBindErrors>
								</td>
				            </tr>
				            
				            <!-- Homoclave  y cuenta -->
							<tr>
								<td class="tag" ><span class="required">*</span><label for="rfc">RFC - Homoclave:</label></td>
							    <td class="control" >
							        <c:set var="tabIndex" value="${tabIndex+1}"/>
							        <form:input maxlength="10" path="rfc" cssStyle="width:90px" onkeypress="return enableAlfaNumero(event)" htmlEscape="true" tabindex="${tabIndex}"/>&nbsp;- 
							        <c:set var="tabIndex" value="${tabIndex+1}"/>
							        <form:input maxlength="3" path="homoclaveRfc" cssStyle="width:30px" onkeypress="return enableAlfaNumero(event)" htmlEscape="true" tabindex="${tabIndex}"/>
							        <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="rfc" cssClass="error"/></s:hasBindErrors>
							        <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="homoclaveRfc" cssClass="error"/></s:hasBindErrors>
							    </td>
							</tr>
				           
					        
					     <!-- Apellido paterno y materno -->  
					        <tr>
								<td class="tag" >
									<span class="required">*</span>
									<label for="apePaterno">Apellido Paterno:</label>
								</td>
								<td class="control" >
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
								    <form:input path="apePaterno" maxlength="30" cssStyle="width:95%" onkeypress="return enableAlfa(event)" htmlEscape="true" tabindex="${tabIndex}"/>
									<s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="apePaterno" cssClass="error"/></s:hasBindErrors>	
								</td>
								<td class="tag" >
								    <span class="required">*</span>
								    <label for="apeMaterno">Apellido Materno:</label>
								</td>
								<td class="control" >
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
								    <form:input path="apeMaterno" maxlength="30" cssStyle="width:95%" onkeypress="return enableAlfa(event)" htmlEscape="true" tabindex="${tabIndex}"/>
							   	    <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="apeMaterno" cssClass="error"/></s:hasBindErrors>
							     </td>
							</tr>
					       
					       <!-- Nombre y fecha de nacimiento -->
					       <tr>
								<td class="tag" >
								    <span class="required">*</span>
								    <label for="nombre">Nombre(s):</label>
								</td>
								<td class="control" >
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
								    <form:input path="nombre" maxlength="30" cssStyle="width:95%" onkeypress="return enableAlfa(event)" htmlEscape="true" tabindex="${tabIndex}"/>
									<s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="nombre" cssClass="error"/></s:hasBindErrors>
								</td>
								<td class="tag" >
								    <span class="required">*</span>
								    <label for="dia">Fecha de Nacimiento:</label>
								</td>
								<td class="control">
								                <c:set var="tabIndex" value="${tabIndex+1}"/>
								           		<form:select path="diaNacimiento" tabindex="${tabIndex}">
								           			  <form:option value="0" label="--"/>	
					                    			  <form:options items="${CATALOGO.DIAS}" itemLabel="descripcion"/>
				                           		</form:select>
				                                <c:set var="tabIndex" value="${tabIndex+1}"/>
				                                <form:select path="mesNacimiento" cssStyle="width:100px" tabindex="${tabIndex}">
				                                	<form:option value="0" label="--"/>
					                    			<form:options items="${CATALOGO.MESES}" itemLabel="descripcion"/>
				                                </form:select>
				                                <c:set var="tabIndex" value="${tabIndex+1}"/>
				                              	<form:select path="anioNacimiento" tabindex="${tabIndex}">
				                              		  <form:option value="0" label="--"/>
					                    			  <form:options items="${CATALOGO.ANIOS}" itemLabel="descripcion"/>
				                                </form:select>
				                        <s:hasBindErrors name="dependienteEconomico">
				                        <br/>
				                        <form:errors path="fechaNacimiento" cssClass="error"/>		
				                        </s:hasBindErrors>
								</td>
							</tr>
					       
					       <!-- Genero y estado civil -->
					       <tr>
								<td class="tag" >
								     <span class="required">*</span>
								     <label for="cveGenero">G&eacute;nero:</label>
								</td>
								<td align="left" >
								      <c:set var="tabIndex" value="${tabIndex+1}"/>
								      <form:radiobutton path="cveGenero" onclick="" value="1" tabindex="${tabIndex}"/> 
								      <label for="cveGenero1" style="font-size: 12px">Masculino</label>
								      <c:set var="tabIndex" value="${tabIndex+1}"/> 
									  <form:radiobutton path="cveGenero" onclick="" value="2" tabindex="${tabIndex}"/> 
									  <label for="cveGenero2" style="font-size: 12px">Femenino</label>
							          <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="cveGenero" cssClass="error"/></s:hasBindErrors>
							    </td>
								<td class="tag" >
								      <span class="required">*</span>
								      <form:label path="cveEdoCivil">Estado Civil:</form:label></td>
								<td class="control" >
							  	      <c:set var="tabIndex" value="${tabIndex+1}"/>
							  	      <form:select path="cveEdoCivil" tabindex="${tabIndex}">
					                    <form:option value="0" label="Seleccione"></form:option>
					                    <form:options items="${CATALOGO.ESTADO_CIVIL}" itemLabel="descripcion"/>
					                  </form:select> 
								      <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="cveEdoCivil" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
					       
					        <!-- Subseccion lugar de nacimiento -->
					        <tr>
								<td colspan="2" align="left"><span class="subseccion">Lugar de Nacimiento</span></td>
								<td colspan="2">&nbsp;</td>
							</tr>
					       
					       <!-- Pais y nacionalidad -->
					       <tr>
								<td class="tag" >
								    <span class="required">*</span>
								    <label for="idPaisNac">Pa&iacute;s:</label>
								</td>
								<td class="control" >
								        <c:set var="tabIndex" value="${tabIndex+1}"/>
									    <form:select path="idPaisNac" onchange="checkPaisNacimiento(this.value)" cssStyle="width:95%" tabindex="${tabIndex}">
						                    <form:option value="0" label="Seleccione"></form:option>
						                    <form:option value="1" label="M�XICO"></form:option>
						                    <form:options items="${CATALOGO.PAISES}" itemLabel="descripcion"/>
						                </form:select>
								    <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="idPaisNac" cssClass="error"/></s:hasBindErrors>
								</td>
								<td class="tag" >
								    <span class="required">*</span>
								    <label for="idNacionalidad">Nacionalidad:</label>
								</td>
								<td class="control" >
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
								    <form:select path="idNacionalidad" cssStyle="width:95%" tabindex="${tabIndex}">
					                    <form:option value="0" label="Seleccione"></form:option>
					                    <form:option value="1" label="MEXICANA"></form:option>
					                    <form:options items="${CATALOGO.PAISES}" itemLabel="descripcion"/>
					                </form:select>
					                <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="idNacionalidad" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
					       
					       <tr>
								<td class="tag" >
								    <span class="required">*</span>
								    <label for="idEdoNac">Estado:</label>
								</td>
								<td class="control" >
								     <div id="divEstadoNacMexico">
								         <c:set var="tabIndex" value="${tabIndex+1}"/>
									     <form:select path="idEdoNac" cssStyle="width:95%" tabindex="${tabIndex}">
										 	<form:option value="0" label="Seleccione"></form:option>
										 	<form:options items="${CATALOGO.ESTADOS}" itemLabel="descripcion"/>
									     </form:select>
								         <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="idEdoNac" cssClass="error"/></s:hasBindErrors>
							         </div>
							         <div id="divEstadoNacExtranjero" style="display:none">
									     <form:input path="estadoNacExtranjero" maxlength="40" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true"/>
								         <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="estadoNacExtranjero" cssClass="error"/></s:hasBindErrors>
							         </div>
						        </td>
								<td class="tag" >
								     <span class="required">*</span>
								     <label for="ciudadNacimiento">Ciudad/Poblaci&oacute;n:</label>
								</td>
								<td class="control" >
								     <c:set var="tabIndex" value="${tabIndex+1}"/>
								     <form:input path="ciudadNacimiento" cssStyle="width:95%" maxlength="40" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabindex="${tabIndex}"/>
								     <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="ciudadNacimiento" cssClass="error"/></s:hasBindErrors>
							    </td>
							</tr>
					       
					        <!-- Subseccion datos adicionales -->
					        <tr>
								<td colspan="2" align="left"><span class="subseccion">Datos Adicionales</span></td>
								<td colspan="2">&nbsp;</td>
							</tr>
					       
					       
					       
					       <!-- Curp y profesion -->
					       <tr>
								 <td class="tag" >
								    <label for="curp">CURP:</label>
								 </td>
								 <td class="control" >
								     <c:set var="tabIndex" value="${tabIndex+1}"/>
								     <form:input path="curp" maxlength="18" htmlEscape="true" tabindex="${tabIndex}"/>
								     <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="curp" cssClass="error"/></s:hasBindErrors>
							     </td>
							     <td class="tag" >
							          <span class="required">*</span>
							          <label for="profesion">Ocupaci&oacute;n:</label>
								</td>
							     <td class="control" >
							          <c:set var="tabIndex" value="${tabIndex+1}"/>
							          <form:select path="profesion" tabindex="${tabIndex}">
							          		<form:option value="0" label="Seleccione"></form:option>
										 	<form:options items="${CATALOGO.OCUPACIONES}" itemLabel="descripcion"/>
							          </form:select>
								          <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="profesion" cssClass="error"/></s:hasBindErrors>
								</td>
								
							</tr>
					       
					       
					       <!-- Dependencia -->
					       	<tr>
					       	    <td class="tag" >
					       	        <label for="centroTrabajo">Centro de Trabajo:</label>
					       	    </td>
								<td class="control" >
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
								    <form:input path="centroTrabajo" onkeypress="return enableAlfaEsp(event)" maxlength="40" cssStyle="width:95%" htmlEscape="true" tabindex="${tabIndex}"/>
									<s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="centroTrabajo" cssClass="error"/></s:hasBindErrors>
							    </td>
				                 <td class="tag" >
				                	<label for="detalleActividad">Detalle Ocupaci&oacute;n:</label>
				                </td>
				                <td class="control" >
				                    <c:set var="tabIndex" value="${tabIndex+1}"/>
				                	<form:input path="detalleActividad" onkeypress="return enableAlfaEsp2(event)" maxlength="60" cssStyle="width:95%" htmlEscape="true" tabindex="${tabIndex}"/>
									<s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="detalleActividad" cssClass="error"/></s:hasBindErrors>
				                </td>
							</tr>
					       
					       
					        <!-- Tipo y numero de identificacion -->
					       <tr>
								<td class="tag" >
								     <span class="required">*</span>
								     <label for="cveTipoIdentificacion">Identificaci&oacute;n Oficial:</label>
								</td>
							    <td class="control" >
							        <c:set var="tabIndex" value="${tabIndex+1}"/>
									<form:select path="cveTipoIdentificacion" cssStyle="width:95%" onchange="validaIdentificacion(this,true)" tabindex="${tabIndex}">
										<option value="0">Seleccione</option>
										<form:options items="${CATALOGO.IDENTIFICACIONES}" itemLabel="descripcion"/>
									</form:select>
									<s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="cveTipoIdentificacion" cssClass="error"/></s:hasBindErrors>
								</td>
								<td class="tag" >
								    <span class="required">*</span>
								    <label for="numeroIdentificacion">N&uacute;mero de Identificaci&oacute;n:</label>
								</td>
								<td class="control" >
								     <c:set var="tabIndex" value="${tabIndex+1}"/>
								     <form:input path="numeroIdentificacion" maxlength="20" htmlEscape="true" tabindex="${tabIndex}"/>
								     <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="numeroIdentificacion" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
					       
					      </tbody>
					      </table>
					      
					      <br/>
					      <div align="left"><span class="seccion">Datos de Contacto</span></div>
					      <table class="ui-widget-content ui-corner-all seccion-solicitud">
						     <tbody>
				            
				            <!-- Subseccion datos personales -->
					        <tr>
								<td colspan="2" align="left"><span class="subseccion">Domicilio Particular</span></td>
								<td colspan="2">&nbsp;</td>
							</tr>
							
							<tr>
								<td colspan="2" align="left">
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
									<input type="checkbox" id="mismoDomicilio" onclick="copyDomicilio(this.checked)" tabindex="${tabIndex}"/>
									<label for="mismoDomicilio" style="font-size: 12px">Mismo domicilio del asegurado</label>
									</td>
								<td colspan="2">&nbsp;</td>
							</tr>
				             
					       <!-- Calle -->
					       
					       <tr>
								<td class="tag" >
								    <span class="required">*</span>
								    <label for="calle">Calle/Avenida:</label>
								</td>
								<td class="control"  colspan="3">
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
								    <form:input path="calle" cssStyle="width:95%" maxlength="80" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabindex="${tabIndex}"/>
								    <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="calle" cssClass="error"/></s:hasBindErrors>
							    </td>
				   		  </tr>
				   		  <tr>
				   		  		<td class="tag" >
								    <span class="required">*</span>
								    <label for="numExt">No. Exterior:</label>
								</td>
								<td class="control">
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
									<form:input cssStyle="width:90px" path="numExt" maxlength="10" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabindex="${tabIndex}"/>
									<s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="numExt" cssClass="error"/></s:hasBindErrors>
							    </td>
							    <td class="tag" >
								    <label for="numInt">No. Interior:</label>
								</td>
								<td class="control">
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
								    <form:input cssStyle="width:90px" path="numInt" maxlength="10" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabindex="${tabIndex}"/>
								    <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="numInt" cssClass="error"/></s:hasBindErrors>
								</td>
				   		  </tr>
					       
					       <!-- Codigo postal y colonia -->
					       <tr>
								<td class="tag" >
								    <span class="required">*</span>
								    <label for="codigoPostal">C&oacute;digo Postal:</label>
								</td>
								<td class="control" >
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
								    <form:input path="codigoPostal" cssStyle="width:70px" maxlength="6" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabindex="${tabIndex}"/>
								    <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="codigoPostal" cssClass="error"/></s:hasBindErrors>
								</td>
								<td class="tag" >
									<span class="required">*</span>
								    <label for="colonia">Colonia/Barrio:</label>
								</td>
								<td class="control" >
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
								    <form:input path="colonia" cssStyle="width:95%" maxlength="40" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabindex="${tabIndex}"/>
								    <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="colonia" cssClass="error"/></s:hasBindErrors>
							    </td>
							</tr>
							
							
							<!-- Ciudad y pais -->
							<tr>
								<td class="tag" >
								    <label for="ciudad">Ciudad/Poblaci&oacute;n:</label>
								</td>
								<td class="control" >
								    <c:set var="tabIndex" value="${tabIndex+1}"/>
								    <form:input path="ciudad" cssStyle="width:95%" maxlength="40" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabindex="${tabIndex}"/>
								    <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="ciudad" cssClass="error"/></s:hasBindErrors>
								</td>
								<td class="tag" >
								    <span class="required">*</span>
								     <label for="idPais">Pa&iacute;s:</label>
								</td>
								<td class="control" >
									<c:set var="tabIndex" value="${tabIndex+1}"/>
									<form:select path="idPais" onchange="checkPaisDomicilio(this.value)" cssStyle="width:95%" tabindex="${tabIndex}">
										<form:option value="0">Seleccione</form:option>
										<form:option value="1" label="M�XICO"></form:option>
										<form:options items="${CATALOGO.PAISES}" itemLabel="descripcion"/>
								     </form:select>
								     <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="idPais" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
							
							
							<!-- Estado y delegacion-->
							<tr>
								<td class="tag" >
								     <span class="required">*</span>
								      <label for="idEstado">Estado:</label>
								</td>
								<td class="control" >
									<div id="divEstadoMexico">
									    <c:set var="tabIndex" value="${tabIndex+1}"/>
										<form:select path="idEstado" cssStyle="width:95%" onchange="loadMunicipios(this)" tabindex="${tabIndex}">
											<form:option value="0">Seleccione un estado</form:option>
											<form:options items="${CATALOGO.ESTADOS}" itemLabel="descripcion"/>
									      </form:select>
									      <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="idEstado" cssClass="error"/></s:hasBindErrors>
								    </div>  
								    <div id="divEstadoExtranjero" style="display: none">
								          <c:set var="tabIndex" value="${tabIndex+1}"/>
										  <form:input path="estadoExtranjero" onkeypress="return enableAlfa(event)" htmlEscape="true" tabindex="${tabIndex}"/>
									      <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="estadoExtranjero" cssClass="error"/></s:hasBindErrors>
								    </div>
								</td>
								<td class="tag" >
								      <span class="required">*</span>
								      <label for="idMunicipio">Delegaci&oacute;n/Municipio:</label>
								</td>
								<td class="control" >
								 	<div id="divMunicipioMexico">
								 	      <c:set var="tabIndex" value="${tabIndex+1}"/>
									      <form:select path="idMunicipio" cssStyle="width:95%" tabindex="${tabIndex}">
											<option value="0">Seleccione</option>
										</form:select>
										<s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="idMunicipio" cssClass="error"/></s:hasBindErrors>
									</div>
									<div id="divMunicipioExtranjero" style="display: none">
									      <c:set var="tabIndex" value="${tabIndex+1}"/>
										  <form:input path="municipioExtranjero" htmlEscape="true" tabindex="${tabIndex}"/>
									      <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="municipioExtranjero" cssClass="error"/></s:hasBindErrors>
								    </div>
								</td>
							</tr>
					       
					        <!-- Subseccion telefonos de contacto -->
					        <tr>
								<td colspan="2" align="left"><span class="subseccion">Tel&eacute;fonos de Contacto     (Capturar al menos uno)</span></td>
								<td colspan="2">&nbsp;</td>
							</tr>
					       
					       <tr>
								<td class="tag" >
								    <label for="telefonoCasa">Tel&eacute;fono Domicilio:</label>
								</td>
								<td class="control" >
								     <c:set var="tabIndex" value="${tabIndex+1}"/>
								     <form:input path="telefonoCasa" cssStyle="width:100px" maxlength="10" onkeypress="return enableNumero(event)" htmlEscape="true" tabindex="${tabIndex}"/>
								     <span>(*Incluir LADA)</span>
								     <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="telefonoCasa" cssClass="error"/></s:hasBindErrors>
							     </td>
								<td class="tag" >
								     <label for="telefonoCelular">Tel&eacute;fono Celular (044):</label>
								</td>
								<td class="control" >
								     <c:set var="tabIndex" value="${tabIndex+1}"/>
								     <form:input path="telefonoCelular" maxlength="10" cssStyle="width: 100px" onkeypress="return enableNumero(event)" htmlEscape="true" tabindex="${tabIndex}"/>
									 <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="telefonoCelular" cssClass="error"/></s:hasBindErrors>
							    </td>
							</tr>
				
							<tr>
								<td class="tag" >
								     <label for="telefonoTrabajo">Tel&eacute;fono Oficina/Trabajo:</label>
								</td>
								<td class="control" >
								     <c:set var="tabIndex" value="${tabIndex+1}"/>
								     <form:input path="telefonoTrabajo" maxlength="10" cssStyle="width:100px" onkeypress="return enableNumero(event)" htmlEscape="true" tabindex="${tabIndex}"/>
									 <span>(*Incluir LADA)</span>            
									 <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="telefonoTrabajo" cssClass="error"/></s:hasBindErrors>
									</td>
								<td class="tag" >
								     <label for="extension">N&uacute;m. Extensi&oacute;n:</label>
								</td>
								<td class="control" >
								     <c:set var="tabIndex" value="${tabIndex+1}"/>
								     <form:input path="extension" maxlength="6" cssStyle="width:90px" htmlEscape="true" tabindex="${tabIndex}"/>
									 <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="extension" cssClass="error"/></s:hasBindErrors>
							    </td>
							</tr>
				
							 <!-- Subseccion correo electronico -->
					        <tr>
								<td colspan="2" align="left"><span class="subseccion">Correo El&eacute;ctronico</span></td>
								<td colspan="2">&nbsp;</td>
							</tr>
				
							<tr>
								<td class="tag" >
								      <label for="correoPersonal">Correo Personal:</label>
								</td>
								<td class="control" >
								       <c:set var="tabIndex" value="${tabIndex+1}"/>
								       <c:set var="tabIndexBis" value="${tabIndex+2}"/>
							           <form:input path="correoPersonal" cssStyle="width:95%;text-transform:none;" onchange="js_solicitud.showCorreoPersonal(this);dojo.byId('correoPersonalBis').focus()" maxlength="50" htmlEscape="true" tabindex="${tabIndex}"/>
							           <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="correoPersonal" cssClass="error"/></s:hasBindErrors>
								</td>
								<td class="tag" >
								       <label for="correoTrabajo">Correo Trabajo:</label>
								</td>
								<td class="control" >
								        <form:input path="correoTrabajo" cssStyle="width:95%;text-transform: none;" onchange="js_solicitud.showCorreoTrabajo(this);dojo.byId('correoTrabajoBis').focus()" maxlength="50" htmlEscape="true" tabindex="${tabIndexBis}"/>
									    <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="correoTrabajo" cssClass="error"/></s:hasBindErrors>
								</td>
						 	</tr>
						     <tr>
				<td class="tag" >
				    <div id="divCorreoPersonalLabel" style="display:none">
				         <label for="correoPersonalBis">Confirmar Correo Personal:</label>
				    </div>
				</td>
				<td class="control" >
				    <div id="divCorreoPersonal" style="display:none">
				        <c:set var="tabIndex" value="${tabIndex+1}"/>
				        <c:set var="tabIndexBis" value="${tabIndex+2}"/>
			            <form:input type="text" path="correoPersonalConfirm" style="width:95%;text-transform: none;" onpaste="return false;" maxlength="50" tabindex="${tabIndex}"/>
			            <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="correoPersonalConfirm" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			        </div>
			    </td>
				<td class="tag" >
				    <div id="divCorreoTrabajoLabel" style="display:none">
				         <label for="correoTrabajoBis">Confirmar Correo Trabajo:</label>
                    </div>				    
				</td>
				<td class="control" >
				    <div id="divCorreoTrabajo" style="display:none">
				        <form:input type="text" path="correoTrabajoConfirm" style="width:95%;text-transform: none;" maxlength="50" onpaste="return false;" tabindex="${tabIndexBis}"/>
				        <s:hasBindErrors name="dependienteEconomico"><br/><form:errors path="correoTrabajoConfirm" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				    </div>
				</td>
			</tr>
		    <tr>
				<td class="tag">
				</td>
				<td class="control">
				    <span id="error_correoPersonal" class="error"/>
				</td>
				<td class="tag">
				</td>
				<td class="control">
				    <span id="error_correoTrabajo" class="error"/>    				    
				</td>
			</tr>
						</tbody>
					</table>      
				</form:form>
			</div>
			<br/><br/><br/>
			<form:errors path="*"/>
			<form name="cancel" action="${cancelUrl}" method="post"></form>
		</div>
		<div align="center">
			<button type="button" class="btnEnviar" onclick="cancelarDependiente()" ><span><em>Cancelar</em></span></button>
			<button type="button" class="btnEnviar" onclick="saveDependiente()" ><span><em>Guardar</em></span></button>
		</div>
		<br/>
		<br/>
	</div>
</div>
</body>
</html>