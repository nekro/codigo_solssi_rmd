<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

  <div id="divAvisoPrivacidad">
	<div align="center" style="width:100%">
		<div align="center" style="width: 90%">
		<br/>
		<p style="text-align: justify;width: 100%">
		        <s:escapeBody>
				Metlife M�xico, S.A., ubicada en Boulevard Manuel �vila Camacho No. 32, pisos SKL, 14 al 20 y
				PH, Colonia Lomas de Chapultepec, Deleg. Miguel Hidalgo, C.P. 11000, M�xico, D.F. II. FINALIDADES. Los datos personales que recabamos directamente de ti, de otras fuentes
				permitidas por la ley o los que se generen de estas o de la relaci�n que lleguemos a establecer, y que son necesarios para otorgarte servicios financieros relativos a la contrataci�n
				de seguros o los que se deriven o sean accesor�as de �sta, los utilizamos para evaluar tu solicitud de seguro, analizar riesgos, identificar, operar, administrar, dictaminar, tramitar tus
				siniestros, prevenir fraudes y cumplir obligaciones derivadas de cualquier relaci�n jur�dica que establezcamos conforme a la Legislaci�n aplicable en materia de Seguros; generar
				datos estad�sticos; evaluar la calidad del servicio; y para fines secundarios al promocionarte nuestros productos o servicios financieros o de nuestras filiales, subsidiarias y partes
				relacionadas o al realizar campa�as publicitarias o con fines de mercadotecnia. Para estas finalidades, requerimos tus datos personales de identificaci�n, laborales, acad�micos y
				migratorios; tus datos patrimoniales y financieros; y tus datos personales sensibles de salud y caracter�sticas f�sicas. III. MEDIOS PARA EJERCER TUS DERECHOS. Tienes
				derecho a acceder, rectificar, cancelar y oponerte al tratamiento de tus datos o puedes revocar el consentimiento que nos hayas otorgado solicit�ndolo personalmente en nuestro
				departamento de protecci�n de datos en la direcci�n arriba citada, en nuestros Centros de Servicios, que puedes ubicar en nuestro sitio de internet www.metlife.com.mx o enviando
				un correo a contacto@metlife.com.mx. El procedimiento, los requisitos y plazos, puedes consultarlos en nuestro sitio de internet www.metlife.com.mx en el v�nculo de pol�tica de
				privacidad. IV. LIMITAR EL USO O DIVULGACION DE TUS DATOS. Si deseas que tus datos no sean tratados con fines secundarios, al promocionarte productos o servicios
				financieros, puedes llenar nuestro formulario de preferencias de privacidad, en el v�nculo de pol�tica de privacidad en nuestro sitio de internet www.metlife.com.mx, inscribirte en el
				Registro P�blico de Usuarios o tachando esta casilla. V. TRANSFERENCIA DE DATOS. Podremos transferir tus datos a terceros nacionales o internacionales como
				dependencias, entidades o instancias gubernamentales para fines de Ley o por requerimiento de Autoridad; a prestadores de servicios de salud para tramitar tus siniestros; a
				organizaciones del sector asegurador para fines de prevenci�n de fraude y selecci�n de riesgos; a nuestras sociedades controladoras y casa matriz para la administraci�n de tu
				seguro; y a nuestras subsidiarias, filiales y partes relacionadas para fines de mercadotecnia, publicidad o prospecci�n comercial, si deseas oponerte a esta ultima transferencia
				tacha esta casilla. VI. CAMBIOS AL AVISO DE PRIVACIDAD. Los cambios o actualizaciones a este aviso de privacidad est�n disponibles y puedes consultarlos peri�dicamente en
				nuestro sitio de internet www.metlife.com.mx en el v�nculo de pol�tica de privacidad. Este aviso se fundamenta en lo dispuesto en la Ley Federal de Protecci�n de Datos Personales
				en Posesi�n de los Particulares y dem�s legislaci�n aplicable.
				</s:escapeBody>
		</p>
		<br/>
		<br/>
		<button type="button" class="btnEnviar" onclick="avisoDialog.hide()"><span><em>Cerrar</em></span></button>
	</div>
	</div>
</div>
