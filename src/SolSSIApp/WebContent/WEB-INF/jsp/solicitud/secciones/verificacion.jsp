<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


	    <div align="left"><span class="seccion">Verificaci&oacute;n de Usuario</span></div>  
	      <table class="ui-widget-content ui-corner-all seccion-solicitud">
		     <tbody>
	        <tr>
				<td colspan="4" align="center">
				   <div>
				   		<label for="captcha"><s:message code="NotEmpty.solicitud.captcha"/></label>
				   </div>
				   <div>
				        <button type="button" style="color:blue;font-size:8pt;text-decoration:underline" onclick="javascript:actualizarCaptcha()">Actualizar Imagen</button>
				   </div>
				   <div>
				   		<img src="<s:url value='/solicitud/captcha'/>" id="imgCaptcha"/>
				   </div>
				   <div>
				   		<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				   		<form:input path="captcha" maxlength="5" cssStyle="text-transform: none;width:50px" htmlEscape="true" onkeypress="return enableAlfa(event)" tabIndex="${tabIndex}"/>
				    	<c:if test="${validCaptcha == 'false'}">
				    		<br/>
							<span class="error"><s:message code="NotValidCaptcha.solicitud.captcha"/></span>
						</c:if>
				   </div>
				   
			    </td>
			</tr>
		</tbody>
	</table>
	<br/>
