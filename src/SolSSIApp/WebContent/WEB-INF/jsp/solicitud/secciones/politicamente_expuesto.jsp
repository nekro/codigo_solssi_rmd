<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

	<div align="left"><span class="seccion">Pol&iacute;ticamente Expuesto</span></div>
	<table class="ui-widget-content ui-corner-all seccion-solicitud">
		     <tbody>
	            <tr>
					<td colspan="4">
						<p style="text-align: justify;padding: 5px 20px 0px 20px" >
							&iquest;Desempe&ntilde;a o ha desempe&ntilde;ado usted, su c&oacute;nyuge o
							un familiar por consanguinidad o afinidad de hasta segundo grado,
							funciones p&uacute;blicas destacadas en territorio nacional o en el
							extranjero? Se considera Persona Pol&iacute;ticamente Expuesta , entre
							otros, a los jefes de estado o de gobierno, l&iacute;deres
							pol&iacute;ticos, funcionarios gubernamentales, judiciales o
							militares de alta jerarqu&iacute;a, altos ejecutivos de empresas estatales o
							funcionarios o miembros importantes de partidos pol&iacute;ticos.
						</p>	
					</td>
			        
			</tr>
			<tr>
			
			<td colspan="4" align="center">
			     <label for="politicamenteExpuestaSi">Si</label>
			     <form:radiobutton path="politicamenteExpuesta" value="1" onclick="javascript:dependientes();" id="politicamenteExpuestaSi" tabIndex="${tabIndex}"/>
				 <label for="politicamenteExpuesta">No</label>
				 <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				 <form:radiobutton path="politicamenteExpuesta" value="0" id="politicamenteExpuestaNo" tabIndex="${tabIndex}"/>
				 <s:hasBindErrors name="solicitud"><br/><form:errors path="politicamenteExpuesta" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
		    </td>
		    </tr>
		    <tr>
		    	<td colspan="4" align="center">
		    		<div syle="text-align: justify;padding: 5px 20px 0px 20px" >
		    			<s:message code="solicitud.articulo140.leyenda" htmlEscape="true"/>
		    		</div>
		    	</td>
		    </tr>
		    <tr>
		    	<td colspan="4" align="center">
		    		<div id="divDependientesList" style="display: none">
		    			<a href="javascript:dependientes()" style="text-decoration: underline;color: blue;">Ver Dependientes Econ&oacute;micos</a>
		    		</div>
		    	</td>
		    </tr>
	</tbody>
	
	</table>
	<br/>	