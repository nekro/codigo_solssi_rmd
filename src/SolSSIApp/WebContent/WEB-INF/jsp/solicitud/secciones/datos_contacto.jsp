<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

	<div align="left"><span class="seccion">Datos de Contacto</span></div>
	      <table class="ui-widget-content ui-corner-all seccion-solicitud">
		     <tbody>
            
            <!-- Subseccion datos personales -->
	        <tr>
				<td colspan="2" align="left"><span class="subseccion">Domicilio Particular</span></td>
				<td colspan="2">&nbsp;</td>
			</tr>
             
	       <tr>
				<td class="tag" >
				    <span class="required">*</span>
				    <label for="calle">Calle/Avenida:</label>
				</td>
				<td class="control"  colspan="3">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input path="calle" cssStyle="width:95%" maxlength="80" onkeypress="return enableAlfaNumeroEsp2(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
				    <s:hasBindErrors name="solicitud"><br/><form:errors path="calle" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			    </td>
   		  </tr>
   		  <tr>
   		  		<td class="tag" >
				    <span class="required">*</span>
				    <label for="numExt">No. Exterior:</label>
				</td>
				<td class="control">
					<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					<form:input cssStyle="width:90px" path="numExt" maxlength="10" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
					<s:hasBindErrors name="solicitud"><br/><form:errors path="numExt" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			    </td>
			    <td class="tag" >
				    <label for="numInt">No. Interior:</label>
				</td>
				<td class="control">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input cssStyle="width:90px" path="numInt" maxlength="10" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
				    <s:hasBindErrors name="solicitud"><br/><form:errors path="numInt" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
   		  </tr>
	       
	       <!-- Codigo postal y colonia -->
	       <tr>
				<td class="tag" >
				    <span class="required">*</span>
				    <label for="codigoPostal">C&oacute;digo Postal:</label>
				</td>
				<td class="control" >
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input path="codigoPostal" cssStyle="width:70px" maxlength="6" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
				    <s:hasBindErrors name="solicitud"><br/><form:errors path="codigoPostal" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				<td class="tag" >
				    <span class="required">*</span>
				    <label for="colonia">Colonia/Barrio:</label>
				</td>
				<td class="control" >
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input path="colonia" cssStyle="width:95%" maxlength="40" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
				    <s:hasBindErrors name="solicitud"><br/><form:errors path="colonia" cssClass="error"  htmlEscape="true"/></s:hasBindErrors>
			    </td>
			</tr>
			
			
			<!-- Ciudad y delegacion -->
			<tr>
				<td class="tag" >
				    <span class="required">*</span>
				    <label for="ciudad">Ciudad/Poblaci&oacute;n:</label>
				</td>
				<td class="control" >
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input path="ciudad" cssStyle="width:95%" maxlength="40" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
				    <s:hasBindErrors name="solicitud"><br/><form:errors path="ciudad" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				<td class="tag" >
				    <span class="required">*</span>
				     <label for="idPais">Pa&iacute;s:</label>
				</td>
				<td class="control" >
					<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					<form:select path="idPais" onchange="checkPaisDomicilio(this.value)" cssStyle="width:95%" tabIndex="${tabIndex}">
						<form:option value="0">Seleccione</form:option>
						<form:option value="1" label="M�XICO"></form:option>
						<form:options items="${CATALOGO.PAISES}" itemLabel="descripcion"/>
				     </form:select>
				     <s:hasBindErrors name="solicitud"><br/><form:errors path="idPais" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
					
					
				</td>
			</tr>
			
			
			<!-- Pais y estado -->
			<tr>
				<td class="tag" >
				     <span class="required">*</span>
				      <label for="idEstado">Estado:</label>
				</td>
				<td class="control" >
					<div id="divEstadoMexico" align="left" style="padding-left: 0px">
						<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
						<form:select path="idEstado" cssStyle="width:95%" onchange="loadMunicipios(this)" tabIndex="${tabIndex}">
							<form:option value="0">Seleccione un estado</form:option>
							<form:options items="${CATALOGO.ESTADOS}" itemLabel="descripcion"/>
					      </form:select>
					      <s:hasBindErrors name="solicitud"><br/><form:errors path="idEstado" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				    </div>  
				    <div id="divEstadoExtranjero" style="display: none;padding-left: 0px" align="left">
						  <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
						  <form:input path="estadoExtranjero" onkeypress="return enableAlfa(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
					      <s:hasBindErrors name="solicitud"><br/><form:errors path="estadoExtranjero" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				    </div>
				</td>
				<td class="tag" >
					  <span class="required">*</span>
				      <label for="idMunicipio">Delegaci&oacute;n/Municipio:</label>
				</td>
				<td class="control" >
				 	<div id="divMunicipioMexico">
					    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					    <form:select path="idMunicipio" cssStyle="width:95%" tabIndex="${tabIndex}">
							<option value="0">Seleccione</option>
						</form:select>
						<s:hasBindErrors name="solicitud"><br/><form:errors path="idMunicipio" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
					</div>
					<div id="divMunicipioExtranjero" style="display: none">
						  <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
						  <form:input path="municipioExtranjero" htmlEscape="true" tabIndex="${tabIndex}"/>
					      <s:hasBindErrors name="solicitud"><br/><form:errors path="municipioExtranjero" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				    </div>
				</td>
			</tr>
	       
	        <!-- Subseccion telefonos de contacto -->
	        <tr>
				<td colspan="2" align="left"><span class="subseccion">Tel&eacute;fonos de Contacto  (Capturar al menos uno)</span></td>
				<td colspan="2">&nbsp;</td>
			</tr>
	       
	       <tr>
				<td class="tag" >
				    <label for="telefonoCasa">Tel&eacute;fono Domicilio:</label>
				</td>
				<td class="control" >
				     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				     <form:input path="telefonoCasa" cssStyle="width:100px" maxlength="10" onkeypress="return enableNumero(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
				     <span>(*Incluir LADA)</span>
				     <s:hasBindErrors name="solicitud"><br/><form:errors path="telefonoCasa" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			     </td>
				<td class="tag" >
				     <label for="telefonoCelular">Tel&eacute;fono Celular (044):</label>
				</td>
				<td class="control" >
				     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				     <form:input path="telefonoCelular" maxlength="10" cssStyle="width: 100px"
					             onkeypress="return enableNumero(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
					 <s:hasBindErrors name="solicitud"><br/><form:errors path="telefonoCelular" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			    </td>
			</tr>

			<tr>
				<td class="tag" >
				     <label for="telefonoTrabajo">Tel&eacute;fono Oficina/Trabajo:</label>
				</td>
				<td class="control" >
				     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				     <form:input path="telefonoTrabajo" maxlength="10" cssStyle="width:100px"
					             onkeypress="return enableNumero(event)" 
					             onchange="js_solicitud.showExtension(this);dojo.byId('extension').focus()" htmlEscape="true" tabIndex="${tabIndex}"/>
					 <span>(*Incluir LADA)</span>            
					 <s:hasBindErrors name="solicitud"><br/><form:errors path="telefonoTrabajo" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
					</td>
				<td class="tag" >
				     <div id="divExtensionLabel" style="display:none">
				     	<label for="extension">N&uacute;m. Extensi&oacute;n:</label>
				     </div>
				</td>
				<td class="control" >
				     <div id="divExtension" style="display:none">
					     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					     <form:input path="extension" maxlength="6" cssStyle="width:90px" htmlEscape="true" tabIndex="${tabIndex}" />
						 <s:hasBindErrors name="solicitud"><br/><form:errors path="extension" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			         </div>
			    </td>
			</tr>

			 <!-- Subseccion correo electronico -->
	        <tr>
				<td colspan="2" align="left"><span class="subseccion">Correo Electr&oacute;nico  (Capturar al menos uno)</span></td>
				<td colspan="2">&nbsp;</td>
			</tr>

			<tr>
				<td class="tag" >
				      <label for="correoPersonal">Correo Personal:</label>
				</td>
				<td class="control" >
			           <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
			           <c:set var="tabIndexBis" value="${tabIndex+2}"/>
			           <form:input path="correoPersonal" cssStyle="width:95%;text-transform: none;" onchange="js_solicitud.showCorreoPersonal(this);dojo.byId('correoPersonalConfirm').focus()" maxlength="50" htmlEscape="true" tabIndex="${tabIndex}"/>
			           <s:hasBindErrors name="solicitud"><br/><form:errors path="correoPersonal" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				<td class="tag" >
				       <label for="correoTrabajo">Correo Trabajo:</label>
				</td>
				<td class="control" >
				        <form:input path="correoTrabajo" cssStyle="width:95%;text-transform: none;" onchange="js_solicitud.showCorreoTrabajo(this);dojo.byId('correoTrabajoConfirm').focus()" maxlength="50" htmlEscape="true" tabIndex="${tabIndexBis}"/>
					    <s:hasBindErrors name="solicitud"><br/><form:errors path="correoTrabajo" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
			</tr>
		    <tr style="height: 25px">
				<td class="tag" >
				    <div id="divCorreoPersonalLabel" style="display:none">
				         <label for="correoPersonalConfirm">Confirmar Correo Personal:</label>
				    </div>
				</td>
				<td class="control" >
				    <div id="divCorreoPersonal" style="display:none">
			           <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
			           <c:set var="tabIndexBis" value="${tabIndex+2}"/>
			           <form:input path="correoPersonalConfirm" cssStyle="width:95%;text-transform: none;" maxlength="50" htmlEscape="true" onpaste="return false;" tabIndex="${tabIndex}"/>
			           <s:hasBindErrors name="solicitud"><br/><form:errors path="correoPersonalConfirm" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			        </div>
			    </td>
				<td class="tag" >
				    <div id="divCorreoTrabajoLabel" style="display:none">
				         <label for="correoTrabajoConfirm">Confirmar Correo Trabajo:</label>
                    </div>				    
				</td>
				<td class="control" >
				    <div id="divCorreoTrabajo" style="display:none">
				        <form:input path="correoTrabajoConfirm" cssStyle="width:95%;text-transform: none;" maxlength="50" htmlEscape="true" onpaste="return false;" tabIndex="${tabIndexBis}"/>
				        <s:hasBindErrors name="solicitud"><br/><form:errors path="correoTrabajoConfirm" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				    </div>
				</td>
			</tr>
		    <tr style="height: 20px">
				<td class="tag">
				</td>
				<td class="control" style="position: relative;top: -5px">
				    <span id="error_correoPersonal" class="error"/>
				</td>
				<td class="tag">
				</td>
				<td class="control" style="position: relative;top: -5px">
				    <span id="error_correoTrabajo" class="error"/>    				    
				</td>
			</tr>
		</tbody>
	</table>
	<br/>	
	
	<!-- ok -->
