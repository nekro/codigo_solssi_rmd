<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

	
	<div align="left"><span class="seccion">Tipo de Tr&aacute;mite</span></div>
	<table class="ui-widget-content ui-corner-all seccion-solicitud">
		<tbody>
		     <!--  Tipo de tramite -->
		     <!-- Subseccion tipo de tramite  -->
		    <tr>
				<td align="left" colspan="2"><span class="subseccion">Rescate Parcial</span></td>
			</tr>	
			<!--  Porcentaje de retiro -->
			<tr>
				<td colspan="4">
					<table width="100%">
						<tr>
							<td width="30%" align="right" style="vertical-align: middle !important;">
								<label for="porcentaje"><span class="required">*</span>Porcentaje Rescate Parcial:</label>
								<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
			    				<form:input path="porcentaje" cssStyle="width:40px" maxlength="2" onkeypress="return enableNumero(event)" htmlEscape="true" tabindex="${tabIndex}"/>
								<span>%</span>
								<span id="msgPorcentajeRescateParcial" class="error">&nbsp;</span>
                        		<s:hasBindErrors name="solicitud"><br/><form:errors path="porcentaje" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
                        		<c:if test="${(not empty errorPorcentaje)}">
                        			<br/>
			                  		<a href="javascript:verErrores()" style="text-decoration: underline;color: blue; position: relative;top: -3px">Imprimir error</a>
			            		</c:if>
			    			</td>
			    			<td align="left">
				    			<p align="justify" style="padding-top: 2px;padding-left: 20px;padding-right: 20px">
									<s:message code="solicitud.porcentaje.leyenda1" htmlEscape="true" arguments="${cliente.maxPorcentaje}"/>
								</p>
						    </td>
						</tr>
					</table>	
				</td>
			</tr>	
			<!-- Lugar de elaboracion -->
			<tr>
				<td align="left" colspan="2"><span class="subseccion">Lugar de Elaboraci&oacute;n:</span></td>
			</tr>
			<tr>
	             <td class="tag">
				     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				     <span class="required">*</span> 
				     <label for="estadoElaboracion">Estado:</label>
				 </td>
				 <td class="control">    
	                 <form:select path="estadoElaboracion" cssClass="inputContent" tabIndex="${tabIndex}">
			               <form:option value="0">Seleccione</form:option>
			               <form:options items="${CATALOGO.ESTADOS}" itemLabel="descripcion"/>
		             </form:select>
		             <s:hasBindErrors name="solicitud"><br/><form:errors path="estadoElaboracion" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
	             </td>
	             <td class="tag">
	             	  <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
	                  <span class="required">*</span>
	                  <label for="ciudadElaboracion">Ciudad/Poblaci&oacute;n:</label>
	             </td>
	             <td class="control">
	                  <form:input path="ciudadElaboracion" cssStyle="width:250px" maxlength="40" htmlEscape="true" tabIndex="${tabIndex}"/>
	                  <s:hasBindErrors name="solicitud"><br/><form:errors path="ciudadElaboracion" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
	             </td> 
	            </tr>
        	</tbody>
	</table>
	
	<br/>