


<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

	<div align="left" class="seccion">Datos Generales de Asegurado</div>
	<table class="ui-widget-content ui-corner-all seccion-solicitud">
		<tbody>
			   
            
            <!-- Subseccion datos personales -->
	        <tr>
				<td colspan="2" align="left"><span class="subseccion">Datos Personales</span></td>
				<td colspan="2">&nbsp;</td>
			</tr>
            
            
            <!-- Homoclave  y cuenta -->
			<tr>
				<td class="tag" ><label for="rfc">RFC <c:if test="${not empty cliente.homoclave}"><c:out value="-"/></c:if>:</label></td>
			    <td class="valueField" >
			        <c:out value="${cliente.rfc}"/><c:if test="${not empty cliente.homoclave}"><c:out value="-${cliente.homoclave}"/></c:if>
			    </td>
			    <td class="tag" ><label for="cuenta">N&uacute;mero de Cuenta:</label>
			    </td>
			    <td class="valueField">
			        <c:out value="${cliente.cuenta}"/>
			    </td>    
			</tr>
			
	     <!-- Apellido paterno y materno -->  
	        <tr>
				<td class="tag">
					<span class="required">*</span>
					<label for="apePaterno">Apellido Paterno:</label>
				</td>
				<td class="control">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input path="apePaterno" maxlength="30" cssStyle="width:95%" onkeypress="return enableAlfa(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
					<s:hasBindErrors name="solicitud"><br/><form:errors path="apePaterno" cssClass="error" htmlEscape="true" /></s:hasBindErrors>	
				</td>
				<td class="tag">
				    <span class="required">*</span>
				    <label for="apeMaterno">Apellido Materno:</label>
				</td>
				<td class="control">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input path="apeMaterno" maxlength="30" cssStyle="width:95%" onkeypress="return enableAlfa(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
			   	    <s:hasBindErrors name="solicitud"><br/><form:errors path="apeMaterno" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			     </td>
			</tr>
	       
	       <!-- Nombre y fecha de nacimiento -->
	       <tr>
				<td class="tag">
				    <span class="required">*</span>
				    <label for="nombre">Nombre(s):</label>
				</td>
				<td class="control">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input path="nombre" maxlength="30" cssStyle="width:95%" onkeypress="return enableAlfa(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
					<s:hasBindErrors name="solicitud" ><br/><form:errors path="nombre" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				<td class="tag">
				    <span class="required">*</span>
				    <label for="dia">Fecha de Nacimiento:</label>
				</td>
				<td class="control">
	           			<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
	           			<form:select path="diaNacimiento" tabIndex="${tabIndex}">
                  			  <form:options items="${CATALOGO.DIAS}" itemLabel="descripcion"/>
                        </form:select>
                        <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
                        <form:select path="mesNacimiento" cssStyle="width:125px" tabIndex="${tabIndex}">
                  			<form:options items="${CATALOGO.MESES}" itemLabel="descripcion"/>
                        </form:select>
                        <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
                        <form:select path="anioNacimiento" tabIndex="${tabIndex}">
                  			  <form:options items="${CATALOGO.ANIOS}" itemLabel="descripcion"/>
                        </form:select>
				    <s:hasBindErrors name="solicitud"><br/><form:errors path="fechaNacimiento" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
			</tr>
	       
	       <!-- Genero y estado civil -->
	       <tr>
				<td class="tag">
				     <span class="required">*</span>
				     <label for="cveGenero">G&eacute;nero:</label>
				</td>
				<td class="control">
				      <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				      <form:radiobutton path="cveGenero" onclick="" value="1" label="Masculino" tabIndex="${tabIndex}"/> 
					  <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					  <form:radiobutton path="cveGenero" onclick="" value="2" label="Femenino" tabIndex="${tabIndex}"/>
			          <s:hasBindErrors name="solicitud"><br/><form:errors path="cveGenero" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			    </td>
				<td class="tag">
					  <span class="required">*</span>
				      <form:label path="cveEdoCivil">Estado Civil:</form:label>
				</td>
				<td class="control">
			  	      <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
			  	      <form:select path="cveEdoCivil" tabIndex="${tabIndex}">
	                    <form:option value="0" label="Seleccione"></form:option>
	                    <form:options items="${CATALOGO.ESTADO_CIVIL}" itemLabel="descripcion"/>
	                  </form:select> 
				      <s:hasBindErrors name="solicitud"><br/><form:errors path="cveEdoCivil" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
			</tr>
	       
	        <!-- Subseccion lugar de nacimiento -->
	        <tr>
				<td colspan="2" align="left"><span class="subseccion">Lugar de Nacimiento</span></td>
				<td colspan="2">&nbsp;</td>
			</tr>
	       
	       <!-- Pais y nacionalidad -->
	       <tr>
				<td class="tag">
				    <span class="required">*</span>
				    <label for="idPaisNac">Pa&iacute;s:</label>
				</td>
				<td class="control">
					    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					    <form:select path="idPaisNac" onchange="js_solicitud.checkPaisNacimiento(this.value, true)" cssStyle="width:95%" tabIndex="${tabIndex}">
		                    <form:option value="0" label="Seleccione"></form:option>
		                    <form:option value="1" label="M�XICO"></form:option>
		                    <form:options items="${CATALOGO.PAISES}" itemLabel="descripcion"/>
		                </form:select>
				    <s:hasBindErrors name="solicitud"><br/><form:errors path="idPaisNac" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				<td class="tag">
				    <span class="required">*</span>
				    <label for="idNacionalidad">Nacionalidad:</label>
				</td>
				<td class="control">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:select path="idNacionalidad" cssStyle="width:95%" onchange="dependientes();" tabIndex="${tabIndex}">
	                    <form:option value="0" label="Seleccione"></form:option>
	                    <form:option value="1" label="MEXICANA"></form:option>
	                    <form:options items="${CATALOGO.PAISES}" itemLabel="valorCampo2"/>
	                </form:select>
	                <s:hasBindErrors name="solicitud"><br/><form:errors path="idNacionalidad" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
			</tr>
	       
	       <tr>
				<td class="tag">
				    <span class="required">*</span>
				    <label for="idEdoNac">Estado:</label>
				</td>
				<td class="control">
				     <div id="divEstadoNacMexico" align="left">
					     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					     <form:select path="idEdoNac" cssStyle="width:95%" tabIndex="${tabIndex}">
						 	<form:option value="0" label="Seleccione"></form:option>
						 	<form:options items="${CATALOGO.ESTADOS}" itemLabel="descripcion"/>
					     </form:select>
				         <s:hasBindErrors name="solicitud"><br/><form:errors path="idEdoNac" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			         </div>
			         <div id="divEstadoNacExtranjero" style="display:none;padding-left: 0px" align="left">
					     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					     <form:input path="estadoNacExtranjero" maxlength="40" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
				         <s:hasBindErrors name="solicitud"><br/><form:errors path="estadoNacExtranjero" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			         </div>
		        </td>
				<td class="tag">
				     <span class="required">*</span>
				     <label for="ciudadNacimiento">Ciudad/Poblaci&oacute;n:</label>
				</td>
				<td class="control">
				     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				     <form:input path="ciudadNacimiento" cssStyle="width:95%" maxlength="40" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
				     <s:hasBindErrors name="solicitud"><br/><form:errors path="ciudadNacimiento" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			    </td>
			</tr>
	       
	        <!-- Subseccion datos adicionales -->
	        <tr>
				<td colspan="2" align="left"><span class="subseccion">Datos Adicionales</span></td>
				<td colspan="2">&nbsp;</td>
			</tr>
	       
	       
	       
	       <!-- Curp y profesion -->
	       <tr>
				 <td class="tag">
				    <label for="curp">CURP:</label>
				 </td>
				 <td class="control">
				     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				     <form:input path="curp" maxlength="18" htmlEscape="true" tabIndex="${tabIndex}"/>
				     <s:hasBindErrors name="solicitud"><br/><form:errors path="curp" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			     </td>
			     <td class="tag">
			     	<span class="required">*</span>
			          <label for="profesion">Profesi&oacute;n/Ocupaci&oacute;n:</label>
				</td>
			     <td class="control">
			          <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
			          <form:input path="profesion" maxlength="40" onkeypress="return enableAlfaNumeroEsp(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
			          <s:hasBindErrors name="solicitud"><br/><form:errors path="profesion" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				
			</tr>
	       
	       
	       <!-- Centro de trabajo y nivel de puesto -->
	      <tr>
                <td class="tag">
                	<span class="required">*</span>
	       	        <label for="nivelPuesto">Nivel de Puesto:</label>
                </td> 
			    <td class="valueField">
			        <c:out value="${cliente.nivelPuesto}"/>
			    </td>  
	             <td class="tag">
                	
                </td> 
                <td class="control">
                
                </td> 
	       </tr>  
	       <!-- Contribuyente en EU -->
	       
	       <tr>
				<td colspan="2" align="center" height="45px">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <label for="contribuyenteEUA">&iquest;Es usted contribuyente de impuestos en E.U.A?</label>
				    <form:radiobutton path="contribuyenteEUA" onclick="js_solicitud.showNSS(true);dojo.byId('numSsn').focus()" value="1" tabIndex="${tabIndex}"/>Si
					<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					<form:radiobutton path="contribuyenteEUA" onclick="js_solicitud.showNSS(false)" value="0" tabIndex="${tabIndex}"/>No
					<s:hasBindErrors name="solicitud"><br/><form:errors path="contribuyenteEUA" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				<td class="tag">
				     <div id="divNSSLabel" style="display:none">
				     <label for="numSsn">N&uacute;mero de Seguridad Social (NSS):</label>
				     </div>
				</td>
				<td class="control">
				     <div id="divNSS" style="display: none">
				          <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				          <form:input path="numSsn" maxlength="11" htmlEscape="true" onkeypress="return enableNumero(event)" tabIndex="${tabIndex}"/>
				          <s:hasBindErrors name="solicitud"><br/><form:errors path="numSsn" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				     </div>
				</td>

			</tr>
	       
	       <!-- Tipo y numero de identificacion -->
	       <tr>
				<td class="tag">
				     <span class="required">*</span>
				     <label for="cveTipoIdentificacion">Identificaci&oacute;n Oficial:</label>
				</td>
			    <td class="control">
					<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					<form:select path="cveTipoIdentificacion" cssStyle="width:95%" onchange="validaIdentificacion(this, true)" tabIndex="${tabIndex}">
						<option value="0">Seleccione</option>
						<form:options items="${CATALOGO.IDENTIFICACIONES}" itemLabel="descripcion"/>
					</form:select>
					<s:hasBindErrors name="solicitud"><br/><form:errors path="cveTipoIdentificacion" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				<td class="tag" >
				    <span class="required">*</span>
				    <label for="numeroIdentificacion">N&uacute;mero de Identificaci&oacute;n:</label>
				</td>
				<td class="control" >
				     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				     <form:input path="numeroIdentificacion" maxlength="20" htmlEscape="true" tabIndex="${tabIndex}"/>
				     <s:hasBindErrors name="solicitud"><br/><form:errors path="numeroIdentificacion" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
			</tr>
	       
	      </tbody>
      </table>
    <br/>  
