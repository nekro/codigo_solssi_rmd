<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>



	<div align="left"><span class="seccion">Modo de Pago</span></div>       
		<table class="ui-widget-content ui-corner-all seccion-solicitud">
			<tbody>
		       <!-- Banco y cuenta clave -->
		       <tr>
		       		<td colspan="3" align="left">
		       			  <c:choose>
		       			  	<c:when test="${not solicitud.bndUltimosDigitosClabe}">
		       			  		<strong><form:radiobutton path="modoPago" value="1" onclick="js_solicitud.otrasFormasPago(this)" label="Transferencia Electrónica" htmlEscape="true"/></strong>
		       			  	</c:when>
		       			  	<c:otherwise>
		       			  		<input type="hidden" name="modoPago" value="1"/>
		       			  	</c:otherwise>
		       			  </c:choose>
		       		     
					</td>
			   </tr>	
		       <tr>
				<td width="44%" align="right">
				      <span class="required">*</span>
				      <label for="banco">Banco:</label>
				      <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				      <form:select path="banco" tabIndex="${tabIndex}">
							<form:option value="0">Seleccione</form:option>
							<form:options items="${CATALOGO.BANCOS}" itemLabel="descripcion"/>
				      </form:select>
				      <s:hasBindErrors name="solicitud"><br/><form:errors path="banco" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				<td width="10px">&nbsp;</td>
				<td width="40%" align="left">
				      <span class="required">*</span>
				      <label for="clabe">CLABE:</label>
				      <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				      <form:input path="clabe" cssStyle="width:150px" onkeypress="return enableDecimal(event)" maxlength="18" htmlEscape="true" tabIndex="${tabIndex}"/>
				      <s:hasBindErrors name="solicitud"><br/><form:errors path="clabe" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				      <c:if test="${(not empty errorClabe)}">
			              <a href="javascript:verErrores()" style="text-decoration: underline;color: blue; position: relative;top: -3px">Imprimir error</a>
			          </c:if>
				</td>
				</tr>
				<c:if test="${not solicitud.bndUltimosDigitosClabe}">
					<tr>
						<td colspan="3" align="left">
							<strong><form:radiobutton path="modoPago" value="2" onclick="js_solicitud.otrasFormasPago(this)" label="Otras formas de pago" htmlEscape="true"/></strong> 
						</td>
					</tr>
					<tr>
						<td colspan="3" align="center" style="height: 130px">
							<div id="divOtraFormaPago" style="display: none;width: 100%" >
							     <table width="95%">
							     	<tr>
							     		<td colspan=3" align="left">
							     			<form:checkbox path="cheque" value="1" id="cheque" onclick="js_solicitud.modoPagoCheque(this)" label="Cheque"/>
							     			<s:hasBindErrors name="solicitud"><br/><form:errors path="cheque" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
							     		</td>
							     	</tr>	
							     	<tr>
							     		<td colspan="3" align="center" style="height: 100px">
							     		  <div id="divModoPagoCheque" style="display: none;" align="center">
							     		  	  <table>
							     		  	  	<tr>
							     		  	  		<td align="right"">
							     		  	  			<span class="required">*</span>
													    <label for="estadoPromotoria">Estado:</label>
													    <form:select path="estadoPromotoria" tabIndex="${tabIndex}" onchange="loadPromotorias(this)">
														      <form:option value="0">Seleccione</form:option>
														      <form:options items="${CATALOGO.ESTADOS}" itemLabel="descripcion" htmlEscape="true"/>
													    </form:select>
							     		  	  			<s:hasBindErrors name="solicitud"><br/><form:errors path="estadoPromotoria" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
							     		  	  		</td>
							     		  	  		<td>
									     		  	  	<span class="required" style="padding-left:25px">*</span>
													 	<label for="clavePromotoria">Oficina de entrega:</label>
												     	<form:select path="clavePromotoria" tabIndex="${tabIndex}" cssStyle="width:425px">
													     	<form:option value="0">Seleccione</form:option>
													     </form:select>
													     <s:hasBindErrors name="solicitud"><br/><form:errors path="clavePromotoria" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
							     		  	  		</td>
							     		  	  	</tr>
							     		  	  	<tr>
							     		  	  		<td align="center" colspan="4">
							     		  	  			<p align="justify"">
							     		  	  				<s:message code="solicitud.cheque.leyenda1" htmlEscape="true" arguments="${CATALOGO.PARAMETROS_NEGOCIO['DIASCHQ'].descripcion}"/>
							     		  	  				<br/>
							     		  	  				<s:message code="solicitud.cheque.leyenda2" htmlEscape="true"/>
							     		  	  			</p>
							     		  	  		</td>
							     		  	  	</tr>
							     		  	  </table> 	
							     		  </div>
							     		</td>
							     	</tr>	
							     </table>
							</div>
						</td>
					</tr>
				</c:if>
	      </tbody>
	     </table> 
       <br/>	     