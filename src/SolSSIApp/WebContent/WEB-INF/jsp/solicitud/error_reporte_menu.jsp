<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script>
		
		var windowPDF = null;
		
		function salir(){
		    top.window.close();
		}
		
		function imprimir(){
			top.content.print();
		}
		
		function descargar(){
			document.forms["form"].action = "<s:url value='/report/service/download/error'/>";
			document.forms["form"].submit();
		}
		
		function acrobatReader(){
			openWindowCenter("http://get.adobe.com/es/reader/","Descargar",800,400);
		}
		
		function ready(){
			<c:if test="${not empty errorDownload}">
				alert("<c:out value="${errorDownload}"/>");	
			</c:if>
		}
		
</script>
</head>
<body onload="ready()">
	<div id="containerInicio" align="center" style="width: 100%">
	    <form name="form" method="post">
			<table>
				<tr>
					<td align="center">
						<button type="button" onclick="descargar()"><span><em>Guardar</em></span></button>
		                 &nbsp;&nbsp;			
						<button type="button" onclick="imprimir()"><span><em>Imprimir</em></span></button>
						 &nbsp;&nbsp;
						<button type="button" onclick="salir()"><span><em>Regresar</em></span></button>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</form>
	</div>	
</body>
</html>
	