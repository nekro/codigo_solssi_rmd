<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
		
		var windowPDF = null;
		
		function salir(){
		    if(windowPDF != null && !windowPDF.closed){
		    	windowPDF.close();
		    }
			
		    document.forms["form"].target = "_top";
			document.forms["form"].action = "<s:url value='/solicitud/salir'/>";
			document.forms["form"].submit();
			
			
		}
		
		function verPDF(){
			windowPDF = openWindowCenter("","SSI_PDF",1000,600);
			document.forms["form"].target = "SSI_PDF";
			document.forms["form"].action = "<s:url value='/solicitud/pdf'/>";
			document.forms["form"].submit();
		}
		
		function descargar(){
			//openWindowCenter("","SSI_PDF",1000,600);
			//document.forms["form"].target = "SSI_PDF";
			document.forms["form"].action = "<s:url value='/report/service/download'/>";
			document.forms["form"].submit();
		}
		
		function acrobatReader(){
			openWindowCenter("http://get.adobe.com/es/reader/","Descargar",800,400);
		}
		
</script>

<s:url value="/clientes/busqueda" var="urlConsulta"/>

<div class="moduleTitle"><span>Solicitud Generada</span></div>        

	<div id="containerInicio" align="center" style="width: 800px">
	    <form name="form" method="post">
			<table class="ui-widget-content ui-corner-all" style="width: 100%" align="center">
				<tr><td align="center"><img src="<s:url value='/resources/images/sucess.png'/>" width="50px" height="50px" /></td></tr>
				<tr>
					<td align="center">
						<span class="leyenda">
							<s:message code="solicitud.result.generado" htmlEscape="true"/>
						</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table>
							<tr style="font-size: 16px;font-weight: bold;">
								<td><c:out value="${sessionScope.solicitud.userFolio}"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<div style="width: 70%">
						<p style="text-align:justify" >
							<s:message code="solicitud.result.leyenda1" htmlEscape="true"/>
						</p>
						</div>	
					</td>
				</tr>
				
				<tr>
					<td align="center">
						    <div style="width: 70%">
							<p style="text-align:justify;color: blue;font-weight: bold">
								<s:message code="solicitud.result.continuarTramite" htmlEscape="true"/>
							</p>
							<br/>
							</div>
					</td>
				</tr>
				
				<tr>
					<td align="center">
						<div style="width: 70%">
						<p style="text-align:justify">
							<s:message code="solicitud.result.leyenda2" htmlEscape="true"/>
						</p>
						</div>
					</td>
				</tr>
				
				<c:if test="${solicitud.pensionado}">
					<tr>
						<td align="center">
							<div style="width: 70%">
								<p style="text-align:justify">
									<s:message code="solicitud.result.pensionado" htmlEscape="true"/>
								</p>
							</div>	
						</td>
					</tr>
				</c:if>
				<c:if test="${solicitud.prestamo}">
					<tr>
						<td align="center">
						    <div style="width: 70%">
							<p style="text-align:justify">
								<s:message code="solicitud.result.prestamo" htmlEscape="true"/>
							</p>
							<br/>
							</div>
						</td>
					</tr>
				</c:if>
				
				<tr>
					<td align="center">
						<button type="button" class="btnEnviar" onclick="verPDF()" style="width:220px"><span><em><s:message code="button.ver.pdf" htmlEscape="true"/></em></span></button>
						<button type="button" class="btnEnviar" onclick="salir()"><span><em><s:message code="button.salir" htmlEscape="true"/></em></span></button>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</form>
	</div>	
	<br/>
	<div id="acrobatReader">
	  <table width="80%">   
	     <tr>
	         <td align="right">
			     <a href="#" onclick="acrobatReader()"><font size="1pt">Descargar Adobe Acrobat Reader</font> 
			     	<img src="<s:url value="/resources/images/adobe.jpg"/>" width="15px" height="15px"/>
			     </a>
	         </td>
	     </tr>
	  </table> 
	</div>	 

