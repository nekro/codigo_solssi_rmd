<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<%@ page isErrorPage="true" import="java.io.*" %>

<html>
<head>
		
<title>Metlife - Aviso del Sistema</title>
<link href="<s:url value="/resources/css/solssi.css" />" rel="stylesheet"  type="text/css" />
<link href="<s:url value="/resources/css/ui/estilos_ui.css" />" rel="stylesheet"  type="text/css" />

</head>

<body style="background-image: none">

<div align="center">

	<form id="form" name="form" method="${m}">
	</form>

  <br/><br/>
  <div class="ui-widget-content ui-corner-all" align="center" style="width: 90%">

   <table align="center">
      <tr>
         <td colspan="5" align="center">
         	<img src="<s:url value='/resources/images/warning_blue.png'/>" width="90px" height="90px" ondblclick="displayDetalle()">
         </td>
      </tr>


      <tr>
         <td colspan="5" align="center">
         	<h3>Favor de entrar desde el portal de Metlife</h3>
         </td>
      </tr>
      
   </table>
   
   </div>
</div>

</body>

</html>