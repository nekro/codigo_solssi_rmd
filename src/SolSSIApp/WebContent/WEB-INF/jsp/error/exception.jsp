<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ page isErrorPage="true" import="java.io.*" %>


<script type="text/javascript">
	
	dojo.ready(function(){
		cerrarSession = false;
	});
	
	
	function displayDetalle(){
		if(document.getElementById("detalle").style.display == ''){
			document.getElementById("detalle").style.display = 'none';
		}else{
			document.getElementById("detalle").style.display = '';			
		}
		
	}
</script>

<div align="center">

	<form id="form" name="form" method="post">
	</form>

  <br/><br/>
  <div class="ui-widget-content ui-corner-all" align="center" style="width: 90%">

   <table align="center">
      <tr>
         <td colspan="5" align="center">
         	<img src="<s:url value='/resources/images/warning_blue.png'/>" width="90px" height="90px" ondblclick="displayDetalle()">
         </td>
      </tr>


      <tr>
         <td colspan="5" align="center">
         	<h2><s:message code="error.app.general"/></h2>
         </td>
      </tr>
      
   </table>
   
   <table align="center" width="90%" >
      
	      <tr>
			  <td id = "error" align="center" width="100%" style="font-family:Arial; font-size:11pt" ondblclick="displayDetalle()">
			  	  <h5>ERROR: <%
			  	  					String message = exception.getMessage();
			  	                    exception.printStackTrace();
			  	                    out.print(message);
			  	  				%>
			  	  </h5>
		      </td>
	      </tr>
         
       </table>
   </div>
</div>

