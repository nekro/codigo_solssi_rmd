
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<%@ page isErrorPage="true" import="java.io.*" %>

<div align="center">
  <br/><br/>
  <div class="ui-widget-content ui-corner-all" align="center" style="width: 90%">

   <table align="center">
      <tr>
         <td colspan="5" align="center">
         	<img src="<s:url value='/resources/images/warning_blue.png'/>" width="90px" height="90px" ondblclick="displayDetalle()">
         </td>
      </tr>
      <tr>
         <td colspan="5" align="center">
         	<h3>Ha ocurrido un error con la base de datos</h3>
         	</br>
         	<%exception.printStackTrace(); %>
         	<h4>ERROR: <%=exception.getMessage()%></h4>
         </td>
      </tr>
      
   </table>
   
   </div>
</div>

