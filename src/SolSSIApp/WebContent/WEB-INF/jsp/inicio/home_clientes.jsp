<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<script type="text/javascript">
	
	function docManualUser(){
		openWindowCenterScroll("<s:url value='/resources/docs/manual_usuario.pdf'/>","manual",800,600);	
	}
	
	function docGuiaSimulador(){
		openWindowCenterScroll("<s:url value='/resources/docs/guia_simulador.pdf'/>","guiasimulador",800,600);
	}
	
	function docPreguntasFrecuentes(){
		openWindowCenterScroll("<s:url value='/resources/docs/preguntas_frecuentes.pdf'/>","preguntas",800,600);
	}
	
	function docDependenciasList(){
		openWindowCenterScroll("<s:url value='/resources/docs/dependencias.pdf'/>","dependencias",800,600);
	}
	
</script>

<style>
	.even{
		font-size: 12px !important;
		font-weight: bold !important;
		background-color: #EFFBEF !important;
	}
	
	.odd{
		font-size: 12px !important;
		font-weight: bold !important;
	}
</style>

<div align="center">
	<br/>
	
	<div style="width: 90%" align="left" id="welcome"> 
			<br/>
			<div>
				<span><s:escapeBody>�Para qui�n aplica este beneficio?</s:escapeBody></span>
			</div>
			<div style="padding-top: 5px">
					Este beneficio es aplicable para los asegurados que prestan sus servicios en las ENTIDADES Y DEPENDENCIAS DEL PODER EJECUTIVO FEDERAL 
					que se encuentran amparadas en el Seguro de Separaci&oacute;n Individualizado en la p&oacute;liza n&uacute;mero SS0001.
					<br/>
					Puedes verificar <a href="javascript:docDependenciasList()">aqu&iacute;</a> que tu dependencia se encuentre dentro de la colectividad.
			</div>
			<br/>
			<div><span><s:escapeBody>�Cu�ndo puedo hacerlo?</s:escapeBody></span></div>
			<div style="padding-top: 5px">
				<s:escapeBody>
					Dependiendo de las 2 primeras letras de tu RFC podr�s efectuar un primer rescate parcial en el periodo comprendido entre el 15 de diciembre de 2013 y el 30 de junio del 2014,  de acuerdo a la siguiente tabla:
				</s:escapeBody>
				<br/>
				<br/>
					<div align="center"> 	
						<div id="tablaRescate" align="center"  style="width: 60%">      
								<table colspan="0" cellspacing="0" style="font-family:Arial" class="ui-widget-content ui-corner-all display">
									 <tr height="30px" class="ui-widget-header">
									    <td width="10%">
											 &nbsp;
										 </td>
									 
										 <td width="30%" align="center">
											 Letras Iniciales del RFC
										 </td>
									 
										 <td width="30%" align="center">
											 Periodo de Solicitud
										 </td>
										 <td width="20%" align="center">
											 Mes de Pago
										 </td>
									</tr>	
									<tr height="30px" class="odd">
										<td>Grupo 1</td>
										<td>AA-CG</td>
										<td>15-31 diciembre 2013</td>
										<td>enero-2014</td>
									</tr>
									<tr height="30px" class="even">
										<td>Grupo 2</td>
										<td>CH-GD</td>
										<td>15-31 enero 2014</td>
										<td>febrero-2014</td>
									</tr>
									<tr height="30px" class="odd">
										<td>Grupo 3</td>
										<td>GE-LO</td>
										<td>15-28 febrero 2014</td>
										<td>marzo-2014</td>
									</tr>
									<tr height="30px" class="even">
										<td>Grupo 4</td>
										<td>LP-OT</td>
										<td>15-31 marzo 2014</td>
										<td>abril-2014</td>
									</tr>
									<tr height="30px" class="odd">
										<td>Grupo 5</td>
										<td>OU-RZ</td>
										<td>15-30 abril 2014</td>
										<td>mayo-2014</td>
									</tr>
									<tr height="30px" class="even">
										<td>Grupo 6</td>
										<td>SA-ZZ</td>
										<td>15-31 mayo 2014</td>
										<td>junio-2014</td>
									</tr>
								</table>	 
						</div>
					</div>
			</div>
			<br/><br/>
			<div><span><s:escapeBody>�C�mo puedo solicitar mi Rescate Parcial?</s:escapeBody></span></div>
			<div style="padding-top: 5px">
					Puedes solicitar tu rescate con estos sencillos pasos:
					<ul style="padding-left: 30px">
						<li style="list-style-type: decimal;">
							Registra e imprime 2 tantos de tu <a href="javascript:optionMenuOpen(2,'<s:url value="/clientes/captura"/>')">Solicitud de Rescate</a> 
						</li>
						<li style="list-style-type: decimal;">
							Acude al &Aacute;rea de Recursos Humanos de tu Dependencia para que se realice la <a href="<s:url value='/operacion'/>">Revisi&oacute;n Documental.</a>
								<ul style="padding-left: 30px">
									<li style="list-style-type: disc;">
										<s:escapeBody>
											Debes acompa�ar tu solicitud de rescate de original y copia de tu identificaci�n oficial vigente, 
											�ltimo tal�n de pago indicando tu aportaci�n al SSI 
											y el estado de cuenta bancario de tu cuenta de n�mina que muestre tu cuenta Clabe. 
										</s:escapeBody>
									<li>
								 </ul>
						</li>
					</ul>
				
			</div>
			
			<br/><br/>
			
			<div>
				<div style="padding-top: 5px">
					Si tienes alguna duda sobre c&oacute;mo llenar tu solicitud, consulta el <a href="javascript:docManualUser()">Manual de usuario</a> de la solicitud de rescate.
				</div>
				<div style="padding-top: 5px">		
						Descarga el <a href="<s:url value='/resources/docs/Simulador_Rescate_Parcial_SSI.xls'/>">Simulador de Rescate Parcial de SSI</a> para realizar una estimaci&oacute;n del porcentaje de retiro que puedes solicitar.
				</div>
				<div style="padding-top: 5px">		
						Consulta <a href="javascript:docGuiaSimulador()">aqu&iacute;</a> la gu&iacute;a para la ejecuci&oacute;n del simulador.
				</div>		
				<div style="padding-top: 5px">
						Consulta nuestra secci&oacute;n de <a href="javascript:docPreguntasFrecuentes()">preguntas frecuentes</a>, si tienes alguna duda sobre el proceso de Rescates del Seguro de Separaci&oacute;n Individualizado
				</div>
			</div>
			<br/>
			
			<div style="width: 100%;font-size: 14px;text-align: left;vertical-align: middle;padding: 10px 10px 10px 10px" class="ui-state-highlight ui-corner-all">
				<span style="color:#333">Ya puedes hacer la <a href="javascript:optionMenuOpen(4,'<s:url value="/esquemas/inicio/home"/>')" style="color: blue;text-decoration: underline;">Selecci&oacute;n de Esquema</a> de Rescates Parciales aplicable a tu Seguro de Separaci&oacute;n Individualizado.</span> 
			</div>
			
			<br/><br/>
	</div>
</div>
      
