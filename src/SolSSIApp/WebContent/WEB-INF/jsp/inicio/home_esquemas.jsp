<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<script type="text/javascript">
	
	function docManualUser(){
		openWindowCenterScroll("<s:url value='/resources/docs/manual_usuario_seleccion_esquemas.pdf'/>","manual",800,600);	
	}
	
	function docPreguntasFrecuentes(){
		openWindowCenterScroll("<s:url value='/resources/docs/preguntas_frecuentes_seleccion_esquemas.pdf'/>","preguntas",800,600);
	}
	
</script>

<style>
	.even{
		font-size: 12px !important;
		font-weight: bold !important;
		background-color: #EFFBEF !important;
	}
	
	.odd{
		font-size: 12px !important;
		font-weight: bold !important;
	}
	
	.esquemaName{
		text-decoration: underline;
		font-size: 12px !important;
		font-weight: bold !important;
		color: #333 !important;
	}
	
	.opciones li{
		
		list-style-type: disc;
		width: 90%;
		text-align: justify;
	}
	
	.opciones ul{
		padding-left:60px;
	}
	
</style>

<div align="center">
	<br/>
	
	<div style="width: 90%" align="left" id="welcome"> 
			<br/>
			<div>
				<span><s:escapeBody>�Para qui�n aplica este beneficio?</s:escapeBody></span>
			</div>
			<div style="padding-top: 5px">
					Este beneficio es aplicable para los asegurados que prestan sus servicios en las ENTIDADES Y DEPENDENCIAS DEL PODER EJECUTIVO FEDERAL 
					que se encuentran amparadas en el Seguro de Separaci&oacute;n Individualizado en la p&oacute;liza n&uacute;mero SS0001.
					<br/>
					Puedes verificar <a href="javascript:docDependenciasList()">aqu&iacute;</a> que tu dependencia se encuentre dentro de la colectividad.
			</div>
			<br/>
			<div><span><s:escapeBody>�Cu�ndo puedo hacerlo?</s:escapeBody></span></div>
			<div style="padding-top: 5px">
				<s:escapeBody>
					Tienes hasta el <c:out value="${CATALOGO.PARAMETROS_NEGOCIO['FECMAXESQ'].descripcionOrigen}"/> para seleccionar el Esquema de Rescates Parciales. 
				</s:escapeBody>
			</div>
			<br/><br/>
			<div><span><s:escapeBody>�Cu�les son las opciones que tengo?</s:escapeBody></span></div>
			<div style="padding-top: 5px" class="opciones">
					Podr&aacute;s elegir tu esquema de rescates parciales entre las siguientes opciones:
					<ul style="padding-left: 30px">
						<br/>
						<li>
							<span class='esquemaName'>Con opci&oacute;n a rescates de las aportaciones voluntarias:</span><s:escapeBody> Las condiciones no cambiaran.  Aprovecha el mayor rendimiento para cuando m�s lo necesites. Puedes rescatar el 100% de tus aportaciones voluntarias cada 6 meses </s:escapeBody><b>(Esquema Original)</b>.
						</li>
						<br/>
						<li>
							<span class='esquemaName'>Con opci&oacute;n a rescates parciales cada tres a&ntilde;os:</span><s:escapeBody> Un rendimiento competitivo con el que podr�s planear para tus metas. Inversi�n a mediano plazo. Te permite rescatar hasta el 50% de tu reserva cada 3 a�os </s:escapeBody><b>(Esquema de Mediano Plazo)</b>.
						</li>
						<br/>
						<li>
							<span class='esquemaName'>Con opci&oacute;n a rescates parciales cada seis meses:</span><s:escapeBody> Disponibilidad frecuente de las aportaciones para que  hagas uso de ellas sin preocupaciones. Cada 6 meses podr�s rescatar hasta el 50% de tu reserva </s:escapeBody><b>(Esquema de Corto Plazo)</b>.
						</li>
						
					</ul>
				
			</div>
			<br/><br/>
			
			<div><span><s:escapeBody>�C�mo puedo hacer la Selecci�n de Esquema de Rescate?</s:escapeBody></span></div>
			<div style="padding-top: 5px">
					Puedes realizar tu selecci&oacute;n de esquema con estos sencillos pasos:
					<ul style="padding-left: 30px">
						<li style="list-style-type: decimal;">
							Registra e imprime 2 tantos de tu <a href="javascript:optionSubMenuOpen(2,'<s:url value="/esquemas/inicio"/>')">Documento de Selecci&oacute;n</a> de Esquema de Rescates Parciales SSI.
							 
						</li>
						<li style="list-style-type: decimal;">
							Acude al &Aacute;rea de Recursos Humanos de tu Dependencia para que se realice la <a href="javascript:document.forms['loginForm'].submit()">Revisi&oacute;n Documental</a> y confirme el Esquema que seleccionaste.
								<ul style="padding-left: 30px">
									<li style="list-style-type: disc;">
										<s:escapeBody>
											Debes acompa�ar tu Documento de Selecci�n, del original y copia de tu identificaci�n oficial vigente.  
										</s:escapeBody>
									<li>
								 </ul>
						</li>
					</ul>
				
			</div>
			
			<br/><br/>
			
			<div>
				<div style="padding-top: 5px">
					Si tienes alguna duda sobre c&oacute;mo llenar tu Documento de Selecci&oacute;n, consulta el <a href="javascript:docManualUser()">Manual de usuario</a> de la p&aacute;gina de Selecci&oacute;n de Esquema de Rescates Parciales SSI.
				</div>
				<div style="padding-top: 5px">
						Consulta nuestra secci&oacute;n de <a href="javascript:docPreguntasFrecuentes()">preguntas frecuentes</a>, si tienes alguna duda sobre el proceso de Selecci&oacute;n de Esquemas de Rescates Parciales SSI. 
				</div>
			</div>
			<br/><br/><br/>
	</div>
	<form action="<s:url value='/login'/>" name="loginForm" method="post">
		<input type="hidden" name="esquemas" value="true">
	</form>
</div>
      
