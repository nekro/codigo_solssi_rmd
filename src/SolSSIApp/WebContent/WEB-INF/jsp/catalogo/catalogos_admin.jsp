<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
	function getItems(){
		document.forms["catalogoMaster"].action = "<s:url value='/admin/catalogos/items'/>";
		submitForm(document.forms["catalogoMaster"]);
	}
	
	function newItem(){
		
		document.forms["form"].action = "<s:url value='/admin/catalogos/alta/form'/>";
		
		document.forms["form"].id.value = document.forms["catalogoMaster"].id.value;
		submitForm(document.forms["form"]);
	}
	
	function editItem(id, clave){
		
		document.forms["form"].action = "<s:url value='/admin/catalogos/edit/form'/>";
		
		document.forms["form"].id.value = id;
		document.forms["form"].clave.value = clave;
		submitForm(document.forms["form"]);
	}

</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

</style>


<div class="moduleTitle"><span>Cat&aacute;logos</span></div>
	<div id="filtro" style="width: 90%" align="center">
		<form:form modelAttribute="catalogoMaster">
			<table class="ui-widget-content ui-corner-all" style="width: 75%">
				<tr >
					<td align="center"><span class="titleSeccion">Seleccione el cat&aacute;logo</span>
					</td>
				</tr>
				<tr>
					<td align="center">
							<span>Cat&aacute;logo: </span>
							<form:select path="id" cssStyle="width:200px" htmlEscape="true">
								<form:options items="${catalogos}" itemLabel="nombreCatalogo" itemValue="id"/>
							</form:select>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td align="center">
						<button type="button" class="btnEnviar" onclick="javascript:getItems()" style="width:175px"><span><em>Consultar Elementos</em></span></button>
						<button type="button" class="btnEnviar" onclick="newItem()" style="width:175px"><span><em>Nuevo Elemento</em></span></button>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
		</form:form>
	</div>
	
	
	<c:if test="${not empty items}">
		<br/><br/>
		<div>
				
				<table class="display ui-widget-content ui-corner-all" style="width: 75%">
				
					<thead class="ui-widget-header">
						<tr>	
							<th><c:out value="${catalogoMaster.nombreClave}"/></th>
							<th><c:out value="${catalogoMaster.nombreCampo1}"/></th>
							<c:if test="${not empty catalogoMaster.nombreCampo2}">
								<th><c:out value="${catalogoMaster.nombreCampo2}"/></th>
							</c:if>
							<c:if test="${not empty catalogoMaster.nombreCampo3}">
								<th><c:out value="${catalogoMaster.nombreCampo3}"/></th>
							</c:if>
							<c:if test="${not empty catalogoMaster.nombreCampo4}">
								<th><c:out value="${catalogoMaster.nombreCampo4}"/></th>
							</c:if>
							<c:if test="${not empty catalogoMaster.nombreCampo5}">
								<th><c:out value="${catalogoMaster.nombreCampo5}"/></th>
							</c:if>
							<c:if test="${not empty catalogoMaster.nombreCampo6}">
								<th><c:out value="${catalogoMaster.nombreCampo6}"/></th>
							</c:if>
							<c:if test="${not empty catalogoMaster.nombreCampo7}">
								<th><c:out value="${catalogoMaster.nombreCampo7}"/></th>
							</c:if>
							<th>Editar</th>
						</tr>
					</thead>
					<tbody>
					
					<c:forEach items="${items}" var="item" varStatus="st">
					
						<c:choose>
					    	<c:when test="${st.count % 2 == 1}">
					    		<c:set var="css" value="odd"/>
					    	</c:when>
					    	<c:otherwise>
					    		<c:set var="css" value="even"/>
					    	</c:otherwise>
				    	</c:choose>
					
					
						<tr class="${css}">
							
				    		<td width="15%">
								<c:out value="${item.claveItem}"/>
							</td>
							
							<td>
								<a href="javascript:editItem(<c:out value="${catalogoMaster.id}"/>,'<c:out value="${item.claveItem}"/>')">
									<c:out value="${item.descripcion}"/>
								</a>
							</td>
							<c:if test="${not empty catalogoMaster.nombreCampo2}">
								<td>
									<c:choose>
										<c:when test="${catalogoMaster.id eq 3}">
											<c:out value="${CATALOGO.ESTADOS[item.valorCampo2].descripcion}"/>
										</c:when>
										<c:when test="${catalogoMaster.id eq 12}">
											<c:out value="${CATALOGO.ESTADOS[item.valorCampo2].descripcion}"/>
										</c:when>
										<c:otherwise>
											<c:out value="${item.valorCampo2}"/>
										</c:otherwise>
									</c:choose>
								</td>
							</c:if>
							<c:if test="${not empty catalogoMaster.nombreCampo3}">
								<td>
									<c:out value="${item.valorCampo3}"/>
								</td>
							</c:if>
							<c:if test="${not empty catalogoMaster.nombreCampo4}">
								<td>
									<c:out value="${item.valorCampo4}"/>
								</td>
							</c:if>
							<c:if test="${not empty catalogoMaster.nombreCampo5}">
								<td>
									<c:out value="${item.valorCampo5}"/>
								</td>
							</c:if>
							<c:if test="${not empty catalogoMaster.nombreCampo6}">
								<td>
									<c:out value="${item.valorCampo6}"/>
								</td>
							</c:if>
							<c:if test="${not empty catalogoMaster.nombreCampo7}">
								<td>
									<c:out value="${item.valorCampo7}"/>
								</td>
							</c:if>
							
							<td>
								<a href="javascript:editItem(<c:out value="${catalogoMaster.id}"/>,'<c:out value="${item.claveItem}"/>')">
									<img src="<s:url value="/resources/images/edit-file-icon32.png" />" alt="Modificar" height="15px" width="15px"/>
								</a>
							</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
		</div>
		<br/><br/>
	</c:if>
	
	<form name="form" method="post">
		<input name="id" type="hidden"/>
		<input name="clave" type="hidden"/>
	</form>
	
