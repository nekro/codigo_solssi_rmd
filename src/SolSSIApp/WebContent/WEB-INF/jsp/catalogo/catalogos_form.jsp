<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>

	dojo.require("dijit.Calendar");
	dojo.require("dijit.Dialog");
	
	var calendarDlg = null;
	
	dojo.ready(function(){
		calendarDlg = new dijit.Dialog({ title:"Seleccione la Fecha"}, "calendarDlg");
	  	calendarDlg.startup();
	});
	
	function save() {
		document.forms["catalogoItem"].action = "<s:url value='/admin/catalogos/save'/>";
		submitForm(document.forms["catalogoItem"]);
		//document.forms["usuario"].submit();
	}
	
	function cancelar() {
		document.forms["master"].action = "<s:url value='/admin/catalogos/items'/>";
		submitForm(document.forms["master"]);
		//document.forms["usuario"].submit();
	}
	
	function update() {
		document.forms["catalogoItem"].action = "<s:url value='/admin/catalogos/update'/>";
		submitForm(document.forms["catalogoItem"]);
		//document.forms["usuario"].submit();
	}
	
	function showCalendar(input, number){
	
		try{
			cerrarDlg();
			
			var calFin = dijit.byId("calFecha"+number);
			
			calendarDlg.show();
			
			if (!calFin ) {		
				var da = new Date();
			
				if(input.value.length == 10){
					var numbers = input.value.match(/\d+/g);
				    da = new Date(numbers[2], numbers[1] -1 , numbers[0])
				}
				
				calFin = new dijit.Calendar(da, dojo.byId("calFecha"+number));
				calFin.set('value', da);
				var fechaSel = null;
				
			    dojo.connect(calFin, "onValueSelected", function(date){
			    	dojo.byId("calFecha"+number).style.display='';
			    	var fmt = dojo.date.locale.format(date, {datePattern: "dd/MM/yyyy", selector: "date"});
			    	input.value = fmt;
			    	calendarDlg.hide();
			    });	
			    
			}else{
				dojo.byId("calFecha"+number).style.display='';
			}
		}catch(e){
			console.log(e);
			calendarDlg.hide();
			dojo.byId("errorJS").innerHTML = "Capture la fecha en formato dd/MM/yyyy";
		}	
	}
	
	function cerrarDlg(){
	    dojo.byId("calFecha1").style.display='none';
	    dojo.byId("calFecha2").style.display='none';
	    dojo.byId("calFecha3").style.display='none';
	    dojo.byId("calFecha4").style.display='none';
	    dojo.byId("calFecha5").style.display='none';
	    dojo.byId("calFecha6").style.display='none';
	    dojo.byId("calFecha7").style.display='none';
		calendarDlg.hide();
	}
	
	
	
</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

#formulario td{
	padding-top: 3px !important;
	padding-bottom: 3px !important;
}

#formulario .ui-state-active {border: 0px !important;}

</style>

	<c:choose>
		<c:when test="${command eq 'update'}">
			<div class="moduleTitle"><span>Actualizaci&oacute;n de Cat&aacute;logo</span></div>					
		</c:when>
		<c:otherwise>
			<div class="moduleTitle"><span>Alta de Cat&aacute;logo</span></div>
		</c:otherwise>
	</c:choose>
	
	
	<div id="formulario" style="width: 75%" align="center" class="ui-widget-content ui-corner-all">
		<form:form modelAttribute="catalogoItem">
		
			<form:errors path="clave.clave" cssClass="error"/>
			
			<table>
				<thead class="ui-state-active">
				<tr>
					<td colspan="2" align="left">
						<span class="subseccion">Capture la informaci&oacute;n</span>
					</td>
				</tr>
				</thead>
				<tbody>
				<tr>	
					<td class="tag">
						<form:label path="clave.clave"><c:out value="${catalogoMaster.nombreClave}"/>:</form:label>
					</td>
					<c:choose>
							<c:when test="${command eq 'update'}">
								<td class="valueField">
									<c:out value="${catalogoItem.clave.clave}"/>
									<form:hidden path="clave.clave" htmlEscape="true"/>
								</td>
							</c:when>
							<c:otherwise>
								<td class="control">
									<form:input path="clave.clave" maxlength="10" htmlEscape="true"/>
									<s:hasBindErrors name="catalogoItem"><br/><form:errors path="clave.clave" cssClass="error"/></s:hasBindErrors>
								</td>
							</c:otherwise>
						</c:choose>
					</tr>
				
				<tr>	
					<td class="tag">
						<form:label path="descripcion"><c:out value="${catalogoMaster.nombreCampo1}"/>:</form:label>
					</td>
					<td class="control">
						<c:choose>
							<c:when test="${catalogoMaster.tipoCampo1 eq 'D'}">
								<form:input path="descripcion" maxlength="10" size="12" htmlEscape="true" onclick="showCalendar(this,1)"/>
								<img src="<s:url value='/resources/images/calendar.png'/>" onclick="showCalendar(document.getElementById('descripcion'),1)" style="position: relative;top: 5px;cursor: pointer;" width="21px" height="21px"/>
							</c:when>
							<c:otherwise>
								<form:input path="descripcion" maxlength="128" size="50" htmlEscape="true"/>	
							</c:otherwise>
						</c:choose>
						
						<s:hasBindErrors name="catalogoItem"><br/><form:errors path="descripcion" cssClass="error"/></s:hasBindErrors>
					</td>
				</tr>
				
				<c:if test="${not empty catalogoMaster.nombreCampo2}">
					<c:choose>
						<c:when test="${catalogoMaster.id eq 3 or catalogoMaster.id eq 12}">
							<tr>	
								<td class="tag">
									<form:label path="valorCampo2"><c:out value="${catalogoMaster.nombreCampo2}"/>:</form:label>
								</td>
								<td class="control">
									<form:select path="valorCampo2" cssStyle="width:50%" tabIndex="${tabIndex}">
						 				<form:options items="${CATALOGO.ESTADOS}" itemLabel="descripcion"/>
					     			</form:select>
									<s:hasBindErrors name="catalogoItem"><br/><form:errors path="valorCampo2" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
							
						</c:when>
						<c:otherwise>
							<tr>	
								<td class="tag">
									<form:label path="valorCampo2"><c:out value="${catalogoMaster.nombreCampo2}"/>:</form:label>
								</td>
								<td class="control">
									<c:choose>
										<c:when test="${catalogoMaster.tipoCampo2 eq 'D'}">
											<form:input path="valorCampo2" maxlength="10" size="12" htmlEscape="true" onclick="showCalendar(this,2)"/>
											<img src="<s:url value='/resources/images/calendar.png'/>" onclick="showCalendar(document.getElementById('valorCampo2'),2)" style="position: relative;top: 5px;cursor: pointer;" width="21px" height="21px"/>
										</c:when>
										<c:otherwise>
											<form:input path="valorCampo2" maxlength="100" size="70" htmlEscape="true"/>	
										</c:otherwise>
									</c:choose>
									
									<s:hasBindErrors name="catalogoItem"><br/><form:errors path="valorCampo2" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
						</c:otherwise>
					</c:choose>
					
				</c:if>
				
				<c:if test="${not empty catalogoMaster.nombreCampo3}">
					<tr>	
						<td class="tag">
							<form:label path="valorCampo3"><c:out value="${catalogoMaster.nombreCampo3}"/>:</form:label>
						</td>
						<td class="control">
							<c:choose>
								<c:when test="${catalogoMaster.tipoCampo3 eq 'D'}">
									<form:input path="valorCampo3" maxlength="10" size="12" htmlEscape="true" onclick="showCalendar(this,3)"/>
									<img src="<s:url value='/resources/images/calendar.png'/>" onclick="showCalendar(document.getElementById('valorCampo3'),3)" style="position: relative;top: 5px;cursor: pointer;" width="21px" height="21px"/>
								</c:when>
								<c:otherwise>
									<form:input path="valorCampo3" maxlength="100" size="70" htmlEscape="true"/>	
								</c:otherwise>
							</c:choose>
							<s:hasBindErrors name="catalogoItem"><br/><form:errors path="valorCampo3" cssClass="error"/></s:hasBindErrors>
						</td>
					</tr>
				</c:if>
				
				<c:if test="${not empty catalogoMaster.nombreCampo4}">
					<tr>	
						<td class="tag">
							<form:label path="valorCampo4"><c:out value="${catalogoMaster.nombreCampo4}"/>:</form:label>
						</td>
						<td class="control">
							<c:choose>
								<c:when test="${catalogoMaster.tipoCampo4 eq 'D'}">
									<form:input path="valorCampo4" maxlength="10" size="12" htmlEscape="true" onclick="showCalendar(this,4)"/>
									<img src="<s:url value='/resources/images/calendar.png'/>" onclick="showCalendar(document.getElementById('valorCampo4'),4)" style="position: relative;top: 5px;cursor: pointer;" width="21px" height="21px"/>
								</c:when>
								<c:otherwise>
									<form:input path="valorCampo4" maxlength="100" size="70" htmlEscape="true"/>	
								</c:otherwise>
							</c:choose>
							<s:hasBindErrors name="catalogoItem"><br/><form:errors path="valorCampo4" cssClass="error"/></s:hasBindErrors>
						</td>
					</tr>
				</c:if>
				
				<c:if test="${not empty catalogoMaster.nombreCampo5}">
					<tr>	
						<td class="tag">
							<form:label path="valorCampo5"><c:out value="${catalogoMaster.nombreCampo5}"/>:</form:label>
						</td>
						<td class="control">
							<c:choose>
								<c:when test="${catalogoMaster.tipoCampo5 eq 'D'}">
									<form:input path="valorCampo5" maxlength="10" size="12" htmlEscape="true" onclick="showCalendar(this,5)"/>
									<img src="<s:url value='/resources/images/calendar.png'/>" onclick="showCalendar(document.getElementById('valorCampo5'),5)" style="position: relative;top: 5px;cursor: pointer;" width="21px" height="21px"/>
								</c:when>
								<c:otherwise>
									<form:input path="valorCampo5" maxlength="100" size="70" htmlEscape="true"/>	
								</c:otherwise>
							</c:choose>
							<s:hasBindErrors name="catalogoItem"><br/><form:errors path="valorCampo5" cssClass="error"/></s:hasBindErrors>
						</td>
					</tr>
				</c:if>
				
				<c:if test="${not empty catalogoMaster.nombreCampo6}">
					<tr>	
						<td class="tag">
							<form:label path="valorCampo6"><c:out value="${catalogoMaster.nombreCampo6}"/>:</form:label>
						</td>
						<td class="control">
							<c:choose>
								<c:when test="${catalogoMaster.tipoCampo6 eq 'D'}">
									<form:input path="valorCampo6" maxlength="10" size="12" htmlEscape="true" onclick="showCalendar(this,6)"/>
									<img src="<s:url value='/resources/images/calendar.png'/>" onclick="showCalendar(document.getElementById('valorCampo6'),6)" style="position: relative;top: 5px;cursor: pointer;" width="21px" height="21px"/>
								</c:when>
								<c:otherwise>
									<form:input path="valorCampo6" maxlength="100" size="70" htmlEscape="true"/>	
								</c:otherwise>
							</c:choose>
							<s:hasBindErrors name="catalogoItem"><br/><form:errors path="valorCampo6" cssClass="error"/></s:hasBindErrors>
						</td>
					</tr>
				</c:if>
				
				<c:if test="${not empty catalogoMaster.nombreCampo7}">
					<tr>	
						<td class="tag">
							<form:label path="valorCampo7"><c:out value="${catalogoMaster.nombreCampo7}"/>:</form:label>
						</td>
						<td class="control">
							<c:choose>
								<c:when test="${catalogoMaster.tipoCampo7 eq 'D'}">
									<form:input path="valorCampo7" maxlength="10" size="12" htmlEscape="true" onclick="showCalendar(this,7)"/>
									<img src="<s:url value='/resources/images/calendar.png'/>" onclick="showCalendar(document.getElementById('valorCampo7'),7)" style="position: relative;top: 5px;cursor: pointer;" width="21px" height="21px"/>
								</c:when>
								<c:otherwise>
									<form:input path="valorCampo7" maxlength="100" size="70" htmlEscape="true"/>	
								</c:otherwise>
							</c:choose>
							<s:hasBindErrors name="catalogoItem"><br/><form:errors path="valorCampo7" cssClass="error"/></s:hasBindErrors>
						</td>
					</tr>
				</c:if>
				
				<tr>	
					<td class="tag">
						<form:label path="estatus">Estatus:</form:label>
					</td>
					<td class="control">
						<form:radiobutton path="estatus" value="true" label="Activo"/>
						<form:radiobutton path="estatus" value="false" label="Inactivo"/>
					</td>
				</tr>
				</tbody>
			</table>
			
			<div align="center">
				<span class="error" id="errorJS"></span>
			</div>
			
			<input type="hidden" name="command" value="${command}"/>
			<form:hidden path="clave.id"/>
		</form:form>
		
		<div>
			<br/>
			<button type="button" class="btnEnviar" onclick="cancelar()"><span><em>Cancelar</em></span></button>
			<c:choose>
				<c:when test="${command eq 'update'}">
					<button type="button" class="btnEnviar" onclick="update()"><span><em>Guardar</em></span></button>					
				</c:when>
				<c:otherwise>
					<button type="button" class="btnEnviar" onclick="save()"><span><em>Guardar</em></span></button>
				</c:otherwise>
			</c:choose>
			<br/><br/>
		</div>		
		
	</div>
	
	<form name="master" method="post">
		<input type="hidden" name="id" value="<c:out value='${catalogoItem.clave.id}'/>"/>
	</form>
 

<div id="calendarDlg" align="center">
	<div id="calFecha1"></div>
	<div id="calFecha2"></div>
	<div id="calFecha3"></div>
	<div id="calFecha4"></div>
	<div id="calFecha5"></div>
	<div id="calFecha6"></div>
	<div id="calFecha7"></div>
	<a href="javascript:cerrarDlg()">Cerrar</a>
</div>
