<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
	

	
	function refresh() {
		document.forms["filtro"].target = "_top";
		document.forms["filtro"].action = "<s:url value='/catalogo/refresh/execute'/>";
		submitForm(document.forms["filtro"]);
	}

</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

</style>


<div class="moduleTitle"><span>Cat&aacute;logos</span></div>
	<div id="filtro" style="width: 90%" align="center">
		<form name="filtro" method="post">
			<table class="ui-widget-content ui-corner-all">
				<tr >
					<td align="center"><span class="titleSeccion">Clic para recargar los cat&aacute;logos</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<p style="width: 75%">
							<s:escapeBody>
								
							</s:escapeBody>
						</p>
						<button type="button" class="btnEnviar" onclick="refresh()"><span><em>Enviar</em></span></button>
					</td>
				</tr>
			</table>
			    <c:if test="${not empty result}">
					<br/>
					Se han recargado los cat&aacute;logos;
				</c:if>
		</form>
	</div>
	
