<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script type="text/javascript">

	var paramsDlg;
	dojo.require("dijit.Dialog");
	
	dojo.ready(function(){
		
		paramsDlg = new dijit.Dialog({ title:"Par&aacute;metros", style: "width: 600px;text-align:center" }, "editDialog");
	  	paramsDlg.startup();
	  	
	  	<c:if test="${not empty appParameter}">
			paramsDlg.show();
		</c:if>
	  	
	});
		
			
	  function cancelar(){
		  paramsDlg.hide();
	  } 
      
	  function search(){
	     document.forms["form"].action = "<s:url value='/params/search'/>";
	     submitForm(document.forms["form"]);
	  }
	  
	  function edit(parametro){
		     document.forms["form"].action = "<s:url value='/params/edit'/>";
		     document.forms["form"].clave.value = parametro;
		     submitForm(document.forms["form"]);
	  }
		
  	function nuevo(){
	     document.forms["form"].action = "<s:url value='/params/alta'/>";
	     submitForm(document.forms["form"]);
	  }
		
	  function save(){
		 document.forms["editForm"].action = "<s:url value='/params/save'/>";
		 submitForm(document.forms["editForm"]);
	  }
	  
	  function update(){
		 document.forms["editForm"].action = "<s:url value='/params/update'/>";
		 submitForm(document.forms["editForm"]);
	  }
      
</script>

		<div class="moduleTitle"><span>Administraci&oacute;n de Par&aacute;metros</span></div>
		<div>
	 		<form name="form" method="POST">
	   			<table align="center" width="100%">
	   				<tr>
	   					<td align="center">
						    <table width="600px">
						    	<tr>
		        					<td align="left">&nbsp;</td>
		        					<td align="right">
							    	    <button type="button" class="btnEnviar" onclick="javascript:nuevo()"><span><em>Nuevo</em></span></button>
							    	</td>
	        				  	</tr>
							  <c:if test="${not empty params}">
						      <tr>
	   						   <td colspan="2" align="center">
	   							<div style="width:100%" class="ui-widget-content ui-corner-all">
	   								<table class="display">
	   									<thead class="ui-widget-header">
	   										<tr>
		   										<th>Num</th>
		   										<th>Clave</th>
		   										<th>Valor</th>
		   										<th>Editar</th>
	   										</tr>
	   									</thead>
	   									<tbody>
	   										<c:forEach items="${params}" var="p" varStatus="st">
		   										<c:choose>
											    	<c:when test="${st.count % 2 == 1}">
											    		<c:set var="css" value="odd"/>
											    	</c:when>
											    	<c:otherwise>
											    		<c:set var="css" value="even"/>
											    	</c:otherwise>
										    	</c:choose>
	   											<tr class="${css}">	
	   												<td><c:out value="${st.count}"/></td>
	   												<td><a href="javascript:edit('${p.clave}')">${p.clave}</a></td>
	   												<td><c:out value="${p.valor}"/></td>
	   												<td>
	   													<a href="javascript:edit('${p.clave}')">
															<img src="<s:url value="/resources/images/edit-file-icon32.png" />" alt="Modificar" height="15px" width="15px"/>
														</a>
	   												</td>	
	   											</tr>
	   										</c:forEach>
	   									</tbody>
	   								</table>
								</div>   
	   						</td>
		   					</tr>
	   						</c:if>					      
						   </table>
	   				  </td>
	   				</tr>
	   			</table>
	   			<input name="clave" type="hidden">
			</form>
	   
	</div>

			<c:if test="${not empty ACTION}">
   		    	  <div id="editDialog" title="Parámetros" style="display: none">
   		    	  	<sf:errors cssClass="error" path="*"/>
						<sf:form modelAttribute="appParameter" name="editForm" method="post">
							<table class="seccion-captura" width="100%" align="center" id="tableEdit">
							      <thead>
							            <tr>
							            	<td colspan="2" align="center"> 
								            	<c:choose>
								      		     	<c:when test="${ACTION eq 'ALTA'}">
								      		    		<span class="moduleTittle">Alta</span> 	
								      		     	</c:when>
								      		     	<c:otherwise>
														<span class="moduleTittle">Modificar</span>				      		     	
								      		     	</c:otherwise>
								      		     </c:choose>
							      			</td>
							      		<tr>
							      </thead>
							      <tbody>
							      	<tr>
							      		<td align="right">
							      			<label class="tag">Clave:</label>
							      		</td>
							      		<td align="left">
							      		     <c:choose>
							      		     	<c:when test="${ACTION eq 'ALTA'}">
							      		    		<sf:input path="clave" cssClass="inputContent"  maxlength="32"/>
							      		    		<s:hasBindErrors name="appParameter">
							      			    		<br/><br/>
						      		    				<sf:errors cssClass="error" path="clave"/>
						      		    			</s:hasBindErrors>	
							      		     	</c:when>
							      		     	<c:otherwise>
													<span class="infoContent">${appParameter.clave}</span>
							      					<sf:hidden path="clave"/>	
							      		     	</c:otherwise>
							      		     </c:choose>
							      		</td>
							      	</tr>
							      	<tr>
							      		<td align="right">
							      			<label class="tag">Valor:</label>
							      		</td>
							      		<td align="left">
							      			<sf:input path="valor" cssClass="inputContent"  size="80" maxlength="512"/>
							      			<s:hasBindErrors name="appParameter">
							      			    <br/><br/>
						      		    		<sf:errors cssClass="error" path="valor"/>
						      		    	</s:hasBindErrors>
							      		</td>
									</tr>	
									
							      	<tr>
							      		<td colspan="2" align="center">
							      		 	<c:choose>
							      		     	<c:when test="${ACTION eq 'ALTA'}">
													<button type="button" class="btnEnviar" onclick="javascript:save()"><span><em>Guardar</em></span></button>
							      		     	</c:when>
							      		     	<c:otherwise>
							      		     		<button type="button" class="btnEnviar" onclick="javascript:update()"><span><em>Guardar</em></span></button>
							      		     	</c:otherwise>
							      		     </c:choose>
							      		     <button type="button" class="btnEnviar" name="cancel" onclick="javascript:cancelar()"><span><em>Cancelar</em></span></button>
							      		</td>
							      	</tr>
							      	
							      </tbody>
							</table>
							<input name="filter" type="hidden" value="${param.filter}">
						</sf:form>
					</div>	
			</c:if>
