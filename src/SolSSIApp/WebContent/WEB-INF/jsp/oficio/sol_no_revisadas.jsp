<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<s:url value="/consulta" var="urlConsulta"/>

<script>
		 
		 function saveCaducidad(){
		
			if(document.forms["solicitudes"].solicitudes.length){
				var uch = true;
				for(var i = 0; i < document.forms["solicitudes"].solicitudes.length ; i++){
					if(document.forms["solicitudes"].solicitudes[i].checked){
						uch = false;
					}
				}
				
				if(uch){
					alert("Favor de seleccionar al menos una solicitud");
					return;
				}
			}else{
				if(!document.forms["solicitudes"].solicitudes.checked){
					alert("Favor de seleccionar la solicitud");
					return;
				}
			}
			
			document.forms["solicitudes"].action = "<s:url value='/oficio/save/caducadas'/>";
			submitForm(document.forms["solicitudes"]);
		}
		
		function selectAllSols(check){
			
			if(document.forms["solicitudes"].solicitudes.length){
				for(var i = 0; i < document.forms["solicitudes"].solicitudes.length ; i++){
					document.forms["solicitudes"].solicitudes[i].checked = check.checked;
				}
			}else{
				document.forms["solicitudes"].solicitudes.checked = check.checked;	
			}	
		}
		 
</script>

<div class="moduleTitle"><span><s:message code="module.title.oficio.generar" htmlEscape="true"/></span></div>
<div id="oficiosDiv" align="center">      
      
      <div>
      	<span>
      		<s:message code="oficio.solicitudes.norevisadas"/>
      	</span>
      </div>
      
      <c:if test="${not empty solicitudesPendientes}">
		<br/>
			<div style="position: relative;left: 243px">
				<input type="checkbox" id="chkAll" onclick="selectAllSols(this)"/><label for="chkAll"><b>Marcar todos</b></label>
			</div>			
			<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 80%;">
				<form name="solicitudes" method="post">
					<table  class="display" style="width: 100%;padding-top: 30px" align="center">
						<thead class="ui-widget-header">
							<tr>
								<th><s:message code="table.header.solicitud.num" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.folio" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.cuenta" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.rfc" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.nivelPuesto" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.modoPago" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.checkNoRevisada" htmlEscape="true"/></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
			
						<c:forEach items="${solicitudesPendientes}" var="sol" varStatus="st">
							
							<c:choose>
						    	<c:when test="${st.count % 2 == 1}">
						    		<c:set var="css" value="odd"/>
						    	</c:when>
						    	<c:otherwise>
						    		<c:set var="css" value="even"/>
						    	</c:otherwise>
						    </c:choose>
							
							<c:set var="estatus"><c:out value="${sol.cveStatus}"/></c:set>
							<c:set var="tipoTramite"><c:out value="${sol.tipoTramite}"/></c:set>
							
							<tr class="${css}">
								<td><c:out value="${st.count}"/></td>
								<td><c:out value="${sol.userFolio}"/></td>
								<td><c:out value="${sol.cuenta}"/></td>
								<td><c:out value="${sol.rfc}"/></td>
								<td><c:out value="${sol.nivelPuesto}"/></td>
								<td>
									<c:choose>
										<c:when test="${sol.modoPago eq 1}">
											TRANSFERENCIA
										</c:when>
										<c:when test="${sol.modoPago eq 2}">
											CHEQUE
										</c:when>
									</c:choose>
								</td>
								<td>
									<input type="checkbox" name="solicitudes" value="<c:out value='${sol.numFolio}'/>"/>
								</td>
							</tr>
							
						</c:forEach>
					</table>
				</form>	
			</div>
			
			<br/>
			
			<div>
				<br/>
				<button type="button" class="btnEnviar" onclick="javascript:history.back()" style="width: 150px"><span><em><s:message code="button.regresar" htmlEscape="true"/></em></span></button>
				<button type="button" class="btnEnviar" onclick="javascript:saveCaducidad()" style="width: 150px"><span><em><s:message code="button.aceptar" htmlEscape="true"/></em></span></button>
			</div>
		</c:if>
		
		<br/>
</div>


