<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

<script>
	

	function showSolicitud(numFolio) {
		document.forms["solicitud"].numFolio.value = numFolio;
		document.forms["solicitud"].submit();
	}
	
</script>

   <div style="width: 100%" align="center">
		
		<div class="moduleTitle"><span>Detalle de Oficio <c:out value="${oficio.numOficio}"/></span></div>
		
		<c:if test="${not empty solicitudes}">
				<br/>
				<div align="center" style="width:80%" class="ui-widget-content ui-corner-all">
				<table class="display" align="center">
					<tr class="ui-state-default">
						<td align="center">
						    Num Oficio
						</td>
						<td align="center">
						    Fecha<br/> Generaci&oacute;n
						</td>
						<td align="center">
						    Fecha<br/> Autorizaci&oacute;n
						</td>
						<td align="center">
						    Fecha<br/> Envio
						</td>
						<td align="center">
						    Total<br/> Solicitudes
						</td>
						<td align="center">
						    Total<br/> Satisfactorias
						</td>
						<td align="center">
						    Cifra<br/> de Control
						</td>
					</tr>
					<tr>
						<td align="center">
						    <c:out value="${oficio.numOficio}"/>
						</td>
						<td align="center">
						    <fmt:formatDate value="${oficio.fechaGeneracion}" pattern="dd-MM-yyyy HH:mm"/>
						</td>
						<td align="center">
						    <fmt:formatDate value="${oficio.fechaAceptacion}" pattern="dd-MM-yyyy HH:mm"/>
						</td>
						<td align="center">
						    <fmt:formatDate value="${oficio.fechaEnvio}" pattern="dd-MM-yyyy HH:mm"/>
						</td>
						<td align="center">
						    <c:out value="${control.totalSolicitudes}"/>
						</td>
						<td align="center">
						    <c:out value="${control.totalAceptadas}"/>
						</td>
						<td align="center">
						    <c:out value="${control.controlCode}"/>
						</td>
					</tr>
				</table>
				</div>
				<br/>
				<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 80%">
					<table  class="display" style="width: 100%;padding-top: 30px" align="center">
						<thead class="ui-widget-header">
							<tr>
								<th><s:message code="table.header.solicitud.num" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.folio" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.cuenta" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.rfc" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.tipoTramite" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.estatus" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.fechaSolicitud" htmlEscape="true"/></th>
								<th><s:message code="table.header.solicitud.desplegar" htmlEscape="true"/></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
			
						<c:forEach items="${solicitudes}" var="sol" varStatus="st">
							
							<c:choose>
						    	<c:when test="${st.count % 2 == 1}">
						    		<c:set var="css" value="odd"/>
						    	</c:when>
						    	<c:otherwise>
						    		<c:set var="css" value="even"/>
						    	</c:otherwise>
						    </c:choose>
							<c:set var="estatus"><c:out value="${sol.cveStatus}"/></c:set>
							<c:set var="tipoTramite"><c:out value="${sol.tipoTramite}"/></c:set>
							
							<tr class="${css}">
								<td><c:out value="${st.count}"/></td>
								<td><c:out value="${sol.userFolio}"/></td>
								<td><c:out value="${sol.cuenta}"/></td>
								<td><c:out value="${sol.rfc}"/></td>
								<td><c:out value="${CATALOGO.TIPO_TRAMITE[tipoTramite].descripcion}"/></td>
								<td><c:out value="${CATALOGO.ESTATUS_TRAMITE[estatus].valorCampo2}"/></td>
								<td><fmt:formatDate  value="${sol.fechaCaptura}" pattern="dd-MM-yyyy HH:mm:ss"/></td>
								<td><a href="javascript:showSolicitud(${sol.numFolio})" style="color: blue;text-decoration: underline;">
									<img src="<s:url value='/resources/images/edit-find32.png'/>" alt="Desplegar" height="17px" width="17px"/></a>
								</td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<br/><br/>
		</c:if>
		<button type="button" class="btnEnviar" onclick="history.back()"><span><em>Regresar</em></span></button>
	</div>	

	
	<form name="solicitud" action="<s:url value='/consulta/view'/>" method="post">
		<input type="hidden" name="numFolio"/>
	</form>