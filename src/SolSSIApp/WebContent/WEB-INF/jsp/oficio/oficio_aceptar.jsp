<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
	
	dojo.ready(function (){
		if(!document.forms["oficios"].oficios){
			dojo.byId("btnAceptar").style.display = "none";
		}
			
	});
	
	
	
	function buscar() {
		document.forms["filtro"].action = "<s:url value='/oficio/consulta/execute'/>";
		submitForm(document.forms["filtro"]);
	}

	function showDetalleOficio(numOficio) {
		document.forms["detalle"].oficio.value = numOficio;
		submitForm(document.forms["detalle"]);
	}
	
	function enviarOficio(){
		
		if(document.forms["oficios"].oficios.length){
			var uch = true;
			for(var i = 0; i < document.forms["oficios"].oficios.length ; i++){
				if(document.forms["oficios"].oficios[i].checked){
					uch = false;
				}
			}
			
			if(uch){
				alert("Seleccione un oficio");
				return;
			}
		}else{
			if(!document.forms["oficios"].oficios.checked){
				alert("Seleccione un oficio");
				return;
			}
		}
		
		
		document.forms["oficios"].action = "<s:url value='/oficio/aceptar/execute'/>";
		submitForm(document.forms["oficios"]);
	}
</script>

<style>

	#filtro li {
		float: left;
		margin: 0px 20px 0px 0px;
		list-style-type: none;
		height: 50px
	}

</style>
<div class="moduleTitle"><span><s:message code="module.title.oficio.aceptar" htmlEscape="true"/></span></div>

	<div id="filtro" style="width: 90%" align="center">
		<form:form modelAttribute="oficioFilter" name="filtro">
			<table class="ui-widget-content ui-corner-all" >
				<tr >
					<td align="center"><span class="titleSeccion">Filtro de B&uacute;squeda</span>
					</td>
				</tr>
				<tr>
					<td align="left">
						<ul>
							<li>
								<span>Oficio:</span> 
								<form:input path="numOficio" maxlength="10" cssStyle="width:90px" htmlEscape="true"/></li>
							<li>
								<span>Dependencia:</span> 
								<form:select path="dependencia" cssStyle="width:230px" htmlEscape="true">
									<option value="0">TODAS</option>
									<form:options items="${CATALOGO.DEPENDENCIAS}" itemLabel="descDependencia"/>
								</form:select>
							</li>
							<li>
								<span>Estatus:</span>
								<form:select path="estatus" cssStyle="width:100px" htmlEscape="true">
									<option value="0">TODOS</option>
									<form:option value="1" label="GENERADO"/>
									<form:option value="2" label="ACEPTADO"/>
									<form:option value="3" label="ENVIADO"/>
									<form:option value="4" label="PROCESADO"/>
								</form:select>
							</li>	 
							<li>
							<button type="button" class="btnEnviar" onclick="buscar()"><span><em>Buscar</em></span></button>
							</li>		
	
						</ul>
					</td>
				</tr>
			</table>
			<s:hasBindErrors name="oficioFilter">
				<br/><br/>
				<div class="ui-state-error ui-corner-all" style="width: 80%;padding-top: 10px;padding-bottom: 10px">
					<form:errors path="*"/>
				</div>
			</s:hasBindErrors>
		</form:form>
	</div>


	<c:if test="${not empty oficios}">
		<br/>
		<h4>Resultados</h4>
		
		<div align="right" style="width: 95%" class="ui-state-default ui-corner-all">
			<img src="<s:url value='/resources/images/rechazar32.png'/>" height="20px" width="20px">No enviado
			&nbsp;&nbsp;&nbsp;
			<img src="<s:url value='/resources/images/aceptar32.png'/>" height="20px" width="20px">Enviado
			&nbsp;&nbsp;&nbsp;
			<img src="<s:url value='/resources/images/edit-file-icon32.png'/>" height="20px" width="20px">Procesado
		</div>
		<br/>
		
		<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 95%">
			<form name="oficios" method="post">
				
				<table  class="display" style="width: 100%;padding-top: 30px" align="center">
					<thead class="ui-widget-header">
						<tr>
							<th>Oficio</th>
							<th>Dependencia</th>
							<th>Fecha Generaci&oacute;n</th>
							<th>Detalle</th>
							<th>Autom&aacute;tico</th>
							<th>Pr&eacute;stamo</th>
							<th>Jur&iacute;dico</th>
							<th>Cheque</th>
							<th>Pagom&aacute;tico</th>
							<th>Aceptar</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
		
					<c:forEach items="${oficios}" var="oficio" varStatus="st">
						<c:set var="cveDependencia">${oficio.dependencia}</c:set>
						<c:choose>
					    	<c:when test="${st.count % 2 == 1}">
					    		<c:set var="css" value="odd"/>
					    	</c:when>
					    	<c:otherwise>
					    		<c:set var="css" value="even"/>
					    	</c:otherwise>
					    </c:choose>
		
						<tr class="${css}">
							<td><c:out value="${oficio.numOficio}"/></td>
							<td align="left" title="${cveDependencia}"><c:out value="${CATALOGO.DEPENDENCIAS[cveDependencia].descDependencia}"/></td>
							<td><fmt:formatDate value="${oficio.fechaGeneracion}" pattern="dd-MM-yyyy HH:mm:ss"/></td>
							
							
							<td><a href="javascript:showDetalleOficio(<c:out value='${oficio.numOficio}'/>)" style="color: blue;text-decoration: underline;">
								Detalle</a></td>
							<td>
								<c:choose>
									<c:when test="${oficio.bndAutomatico eq 0}">
										<img src="<s:url value='/resources/images/rechazar32.png'/>" height="20px" width="20px">
									</c:when>
									<c:when test="${oficio.bndAutomatico eq 1}">
										<img src="<s:url value='/resources/images/aceptar32.png'/>" height="20px" width="20px">
									</c:when>
									<c:when test="${oficio.bndAutomatico eq 2}">
										<img src="<s:url value='/resources/images/edit-file-icon32.png'/>" height="20px" width="20px">
									</c:when>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${oficio.bndPrestamo eq 0}">
										<img src="<s:url value='/resources/images/rechazar32.png'/>" height="20px" width="20px">
									</c:when>
									<c:when test="${oficio.bndPrestamo eq 1}">
										<img src="<s:url value='/resources/images/aceptar32.png'/>" height="20px" width="20px">
									</c:when>
									<c:when test="${oficio.bndPrestamo eq 2}">
										<img src="<s:url value='/resources/images/edit-file-icon32.png'/>" height="20px" width="20px">
									</c:when>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${oficio.bndJuridico eq 0}">
										<img src="<s:url value='/resources/images/rechazar32.png'/>" height="20px" width="20px">
									</c:when>
									<c:when test="${oficio.bndJuridico eq 1}">
										<img src="<s:url value='/resources/images/aceptar32.png'/>" height="20px" width="20px">
									</c:when>
									<c:when test="${oficio.bndJuridico eq 2}">
										<img src="<s:url value='/resources/images/edit-file-icon32.png'/>" height="20px" width="20px">
									</c:when>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${oficio.bndCheque eq 0}">
										<img src="<s:url value='/resources/images/rechazar32.png'/>" height="20px" width="20px">
									</c:when>
									<c:when test="${oficio.bndCheque eq 1}">
										<img src="<s:url value='/resources/images/aceptar32.png'/>" height="20px" width="20px">
									</c:when>
									<c:when test="${oficio.bndCheque eq 2}">
										<img src="<s:url value='/resources/images/edit-file-icon32.png'/>" height="20px" width="20px">
									</c:when>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${oficio.bndPagomatico eq 0}">
										<img src="<s:url value='/resources/images/rechazar32.png'/>" height="20px" width="20px">
									</c:when>
									<c:when test="${oficio.bndPagomatico eq 1}">
										<img src="<s:url value='/resources/images/aceptar32.png'/>" height="20px" width="20px">
									</c:when>
									<c:when test="${oficio.bndPagomatico eq 2}">
										<img src="<s:url value='/resources/images/edit-file-icon32.png'/>" height="20px" width="20px">
									</c:when>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${oficio.estatus eq 3}">
										Enviado
									</c:when>
									<c:when test="${oficio.estatus eq 4}">
										Procesado
									</c:when>
									<c:when test="${oficio.estatus eq 2}">
										Aceptado
									</c:when>
									<c:otherwise>
										<input type="checkbox" name="oficios" value="<c:out value="${oficio.numOficio}"/>"/>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</table>
			</form>	
		</div>
		<div>
			<br/>
			<button type="button" class="btnEnviar" onclick="enviarOficio()" style="width: 140px" id="btnAceptar"><span><em>Aceptar Oficios</em></span></button>
		</div>
</c:if>


<form name="detalle" action="<s:url value='/oficio/detalle'/>" method="post">
	<input name="oficio" type="hidden"/>
</form>