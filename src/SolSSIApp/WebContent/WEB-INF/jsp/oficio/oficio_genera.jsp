<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<s:url value="/consulta" var="urlConsulta"/>

<script>
		 function generarOficio(){
		 	document.forms["oficio"].action = "<s:url value='/oficio/generar/execute'/>";
		 	submitForm(document.forms["oficio"]);
		 }
		 
		 function showSolicitud(folio){
		 	document.forms["solicitud"].numFolio.value = folio;
		 	submitForm(document.forms["solicitud"]);
		 }
		 
		 
</script>

<div class="moduleTitle"><span><s:message code="module.title.oficio.generar" htmlEscape="true"/></span></div>
<div id="oficiosDiv" align="center">      
      
      <c:if test="${not empty solicitudesOficio}">
		<br/>
			<h4>Solicitudes</h4>
			
			<div align="center" style="width:80%" class="ui-widget-content ui-corner-all">
					<table class="display" align="center">
						<tr class="ui-state-default">
							<td align="center">
							    Solicitudes Ingresadas Satisfactorias
							</td>
							<td align="center">
							    Solicitudes Ingresadas NO Satisfactorias
							</td>
							<td align="center">
							    Total de Solicitudes
							</td>
						</tr>
						<tr>
							<td align="center">
							    <c:out value="${control.totalAceptadas}"/>
							</td>
							<td align="center">
							    <c:out value="${control.totalRechazadas}"/>
							</td>
							<td align="center">
							    <c:out value="${control.totalSolicitudes}"/>
							</td>
						</tr>
					</table>
				</div>
			<br/>
			<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 80%;">
				<table  class="display" style="width: 100%;padding-top: 30px" align="center">
					<thead class="ui-widget-header">
						<tr>
							<th><s:message code="table.header.solicitud.num" htmlEscape="true"/></th>
							<th><s:message code="table.header.solicitud.folio" htmlEscape="true"/></th>
							<th><s:message code="table.header.solicitud.cuenta" htmlEscape="true"/></th>
							<th><s:message code="table.header.solicitud.rfc" htmlEscape="true"/></th>
							<th><s:message code="table.header.solicitud.tipoTramite" htmlEscape="true"/></th>
							<th><s:message code="table.header.solicitud.estatus" htmlEscape="true"/></th>
							<th><s:message code="table.header.solicitud.fechaSolicitud" htmlEscape="true"/></th>
						</tr>
					</thead>
					<tbody>
					</tbody>
		
					<c:forEach items="${solicitudesOficio}" var="sol" varStatus="st">
						
						<c:choose>
					    	<c:when test="${st.count % 2 == 1}">
					    		<c:set var="css" value="odd"/>
					    	</c:when>
					    	<c:otherwise>
					    		<c:set var="css" value="even"/>
					    	</c:otherwise>
					    </c:choose>
						
						<c:set var="estatus">${sol.cveStatus}</c:set>
						<c:set var="tipoTramite">${sol.tipoTramite}</c:set>
						
						<tr class="${css}">
							<td><c:out value="${st.count}"/></td>
							<td><c:out value="${sol.userFolio}"/></td>
							<td><c:out value="${sol.cuenta}"/></td>
							<td><c:out value="${sol.rfc}"/></td>
							<td><c:out value="${CATALOGO.TIPO_TRAMITE[tipoTramite].descripcion}"/></td>
							<td><c:out value="${CATALOGO.ESTATUS_TRAMITE[estatus].valorCampo2}"/></td>
							<td><fmt:formatDate value="${sol.fechaCaptura}" pattern="dd-MM-yyyy	"/></td>
						</tr>
						
					</c:forEach>
				</table>
			</div>
			
			<br/>
			
			<div>
				<br/>
				<button type="button" class="btnEnviar" onclick="javascript:generarOficio()" style="width: 150px"><span><em><s:message code="button.oficio.generar" htmlEscape="true"/></em></span></button>
			</div>
		</c:if>
		
		<c:if test="${empty solicitudesOficio}">
			<div><img src="<s:url value='/resources/images/warning_blue.png'/>"></div>
			<h5><s:message code='oficio.solicitudes.empty'/></h5>
			<div>
				<br/>
			</div>
		</c:if>
      
		<form name="oficio" action="<s:url value='/oficio/generar/execute'/>" method="post"></form>
		<form name="solicitud" action="<s:url value='/oficio/view/solicitud'/>" method="post">
			<input name="numFolio" type="hidden"/>
		</form>
		<br/>
</div>


