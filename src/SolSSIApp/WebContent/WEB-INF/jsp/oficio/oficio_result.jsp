<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
	var imprimir = false;
	function salir() {
		document.forms["form"].target = "_top";
		<c:choose>
			<c:when test="${command eq 'generar'}">
				document.forms["form"].action = "<s:url value='/oficio/generar'/>";

		    	<c:if test="${empty error}">
					if (imprimir == false) {
						alert("Antes de salir, por favor imprima o guarde el oficio.");
						return;
					}
				</c:if>
			</c:when>
			<c:when test="${command eq 'enviar'}">
				document.forms["form"].action = "<s:url value='/oficio/enviar'/>";
			</c:when>
			<c:when test="${command eq 'caducidad'}">
				document.forms["form"].action = "<s:url value='/oficio/generar'/>";
			</c:when>
			<c:otherwise>
				document.forms["form"].action = "<s:url value='/oficio/consulta'/>";
			</c:otherwise>
		</c:choose>
		document.forms["form"].submit();
	}

	function imprimirOficio() {
		imprimir = true;
		openWindowCenter("", "oficiopdf", 1000, 600);
		document.forms["print"].submit();

	}
</script>

<s:url value="/clientes/busqueda" var="urlConsulta" />
<br />
<div id="containerInicio" align="center"
	class="ui-widget-content ui-corner-all" style="width: 75%">
	<br />
	<div>
		<c:choose>
			<c:when test="${empty error}">
				<div>
					<img src="<s:url value='/resources/images/sucess.png'/>"
						width="50px" height="50px">
				</div>
				<c:choose>
					<c:when test="${command eq 'generar'}">
						<fmt:formatDate value="${oficio.fechaGeneracion}"
							pattern="dd 'de' MMMMMMMMMMMM 'de' yyyy" var="fechaGeneracion" />
						<span class="result"><s:message
								code="oficio.generar.result"
								arguments="${oficio.numOficio},${fechaGeneracion},${control.totalSolicitudes}"
								argumentSeparator="," htmlEscape="true" /> </span>
					</c:when>
					<c:when test="${command eq 'enviar'}">
						<span class="result"><s:message code="oficio.enviar.result"
								htmlEscape="true" /></span>
					</c:when>
					<c:when test="${command eq 'caducidad'}">
						<span class="result"><s:message
								code="oficio.guardar.caducadas.result" htmlEscape="true" /></span>

					</c:when>
					<c:otherwise>
						<span class="result">Operaci&oacute;n realizada
							exitosamente</span>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:otherwise>
				<div>
					<img src="<s:url value='/resources/images/warning_blue.png'/>">
				</div>
				<span class="result">Error al ejecutar la operaci&oacute;n</span>
				<div>
					<br /> <span class="error" style="font-size: 12px"><c:out
							value="${error}" /></span>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
	<br />

	<form name="form" method="post">
		<table style="width: 100%" align="center">
			<tr>
				<td align="center"><c:if
						test="${empty error and command eq 'generar'}">
						<button type="button" class="btnEnviar" onclick="imprimirOficio()"
							style="width: 150px">
							<span><em>Imprimir/Guardar</em></span>
						</button>
					</c:if>
					<button type="button" class="btnEnviar" onclick="salir()">
						<span><em>Regresar</em></span>
					</button></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
	</form>

</div>

<form name="print" action="<s:url value='/oficio/oficio/frames'/>"
	method="post" target="oficiopdf"></form>
