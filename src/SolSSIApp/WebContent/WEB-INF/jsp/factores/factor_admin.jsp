<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script type="text/javascript">

	  function save(){
		 document.forms["form"].action = "<s:url value='/ccfactor/save'/>";
		 submitForm(document.forms["form"]);
	  }
	  
	  function salir(){
		 document.forms["menuDirectionForm"].action = "<s:url value='/logout'/>";
		 submitForm(document.forms["menuDirectionForm"]);
	  }
	  
</script>

		<c:choose>
			<c:when test="${empty save_estatus}">
				<div class="moduleTitle"><span>Variables</span></div>
				<div>
		   			<sf:form modelAttribute="CCFactor" name="form" method="post">
						<table>
							<tr>	
								<td>Valor X:</td>
								<td>
									<sf:input path="x" maxlength="6" onkeypress="return enableNumero(event)"/>
									<sf:errors path="x" cssClass="error"/>
								</td>
							</tr>
							<tr>	
								<td>Valor Y:</td>
								<td>
									<sf:input path="y" maxlength="6" onkeypress="return enableNumero(event)"/>
									<sf:errors path="y" cssClass="error"/>
								</td>
							</tr>
						</table>			
					</sf:form>
					<div>
						<button class="btnEnviar" onclick="save()"><span><em>Guardar</em></span></button>
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<br/><br/>
				<div align="center" class="ui-widget-content ui-corner-all" style="width: 50%">
					<br/>
					<div>
						<img src="<s:url value='/resources/images/sucess.png'/>" width="50px" height="50px">
					</div>
					<div>
						<h5>Las variables se han guardado exitosamente</h5>
					</div>
					<br/>
					<div>
						<button class="btnEnviar" onclick="salir()"><span><em>Salir</em></span></button>
					</div>
					<br/>
				</div>
			</c:otherwise>	
		</c:choose>