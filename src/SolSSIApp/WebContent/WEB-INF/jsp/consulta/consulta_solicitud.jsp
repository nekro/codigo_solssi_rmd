<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

<s:url value="/consulta" var="urlConsulta"/>

<script>
		 
		dojo.ready(function(){
			  
			<c:if test="${solicitud.contribuyenteEUA}">
	 			js_solicitud.showNSS(true);
	 	   </c:if>
	 	   
	 		<c:if test="${solicitud.idPaisNac gt 0}">
	 			checkPaisNacimiento(<c:out value="${solicitud.idPaisNac}"/>);
	 		</c:if>
	 		
	 		<c:if test="${solicitud.idPais gt 0}">
	 			checkPaisDomicilio(<c:out value="${solicitud.idPais}"/>);
			</c:if>
		});
		
		 
		 function dependientes() {
			 //openWindowCenter("<s:url value='/consulta/dependientes/'/><c:out value='${solicitud.numFolio}'/>","dep",1000,600);
			 openWindowCenterScroll("","dep",1000,600);
			 document.forms["dependientes"].submit();
		 };
		 
		 
		 function checkPaisNacimiento(value){
			 if(value == 1){
				 document.getElementById("divEstadoNacExtranjero").style.display = "none";
				 document.getElementById("divEstadoNacMexico").style.display = "";
			 }else{
				 document.getElementById("divEstadoNacMexico").style.display = "none";
				 document.getElementById("divEstadoNacExtranjero").style.display = "";
			 }
		 }
		 
		 
		 function checkPaisDomicilio(value){
			 if(value == 1){
				 document.getElementById("divEstadoExtranjero").style.display = "none";
				 document.getElementById("divEstadoMexico").style.display = "";
				 document.getElementById("divMunicipioExtranjero").style.display = "none";
				 document.getElementById("divMunicipioMexico").style.display = "";
			 }else{
				 document.getElementById("divEstadoMexico").style.display = "none";
				 document.getElementById("divEstadoExtranjero").style.display = "";
				 document.getElementById("divMunicipioMexico").style.display = "none";
				 document.getElementById("divMunicipioExtranjero").style.display = "";
			 }
		 }
		 
		 function regresarConsulta(){
			 //document.forms["solicitud"].action = "<s:url value='/'/>${urlRegresar}";
			 //document.forms["solicitud"].target = "_top";
			 //document.forms["solicitud"].submit();
			 history.back();
		 }
		 
</script>

<div class="moduleTitle"><span><s:message code="module.title.solicitud.consulta" htmlEscape="true"/></span></div>

<jsp:include page="datos_solicitud.jsp"/>

<br/>
	<div>
		<button type="button" class="btnEnviar" onclick="regresarConsulta()"><span><em>Regresar</em></span></button>
	</div>
<br/>

<form name="dependientes" action="<s:url value='/consulta/dependientes/'/>" target="dep" method="post"/>
	<input type="hidden" name="folio" value="<c:out value='${solicitud.numFolio}'/>">
</form>

