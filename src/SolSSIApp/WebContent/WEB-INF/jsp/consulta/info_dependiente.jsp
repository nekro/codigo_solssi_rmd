<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<link href="<s:url value="/resources/css/solssi.css" />" rel="stylesheet"  type="text/css" />
	    <link href="<s:url value="/resources/css/tablas.css" />" rel="stylesheet"  type="text/css" />
	    <link href="<s:url value="/resources/css/ui/estilos_ui.css"/>" rel="stylesheet"  type="text/css" />

		<script type="text/javascript" src="<s:url value="/resources/js/ssi_library_1.0.0.js"/>"></script>
		<script type="text/javascript" src="<s:url value="/resources/js/dojo/dojo.xd.js"/>"></script>
		<style>
		    .subseccion {
				font-size: 12px !important;
			}
			
		    #container{
		    	background-image: none !important;
		    }
		    
		    #contenido{
		    	font-size: 12px !important;
		    }
		    
			input{
				text-transform: uppercase;
			}
			
			<s:hasBindErrors name="dependienteEconomico">
				td{
					vertical-align: top !important;
				}
				
				.error{
					position: relative;
					top: -3px; 
				}
			</s:hasBindErrors>

		</style>
	<script>
	
	require(["dojo/ready"], function(ready){
		  ready(function(){
			  
		 		<c:if test="${dependienteEconomico.idPaisNac gt 0}">
		 			checkPaisNacimiento(${dependienteEconomico.idPaisNac});
		 		</c:if>
		 		
		 		<c:if test="${dependienteEconomico.idEstado gt 0}">
		 			loadMunicipios(dojo.byId("idEstado"));
	 			</c:if>
	 			
	 			
		 		<c:if test="${dependienteEconomico.idPais gt 0}">
		 			checkPaisDomicilio(${dependienteEconomico.idPais});
				</c:if>
					
				validaIdentificacion(dojo.byId("cveTipoIdentificacion"), false);
					
		  });
		});
	

			
		function cerrarDependiente() {
			document.forms["regresar"].action = '<s:url value="/consulta/dependientes/regresar/"/>';
			document.forms["regresar"].submit();
		}
		
		
	</script>
</head>
<body style="background-image: none;">

<div id="container">
		<div id="contenido" align="center">
			
			<div class="welcomeLeft">
			 <br/>
			 <table width="95%">
			 <tr>
			  <td width="15px">&nbsp;</td>
			  <td align="left" valign="bottom" width="50%">
			    <img src="<s:url value="/resources"/>/images/logoblanco2.png"/>
			  </td>
			  <td align="right" valign="bottom">
				   &nbsp;
			  </td>
			 </tr>  
			 </table> 
			</div>
			
			<div class="moduleTitle"><span>Informaci&oacute; de Dependientes Econ&oacute;micos</span></div>
			<div id="containerSolicitud" align="center" style="width: 800px">
				
				<form:form modelAttribute="dependienteEconomico" name="dependiente">
				
					<form:hidden path="numero"/>
		            <div align="left" class="seccion">Datos Generales</div>
					<table class="ui-widget-content ui-corner-all seccion-solicitud">
						<tbody>
							   
				            
				            <!-- Subseccion datos personales -->
					        <tr>
								<td colspan="2" align="left"><span class="subseccion">Datos Personales</span></td>
								<td colspan="2">&nbsp;</td>
							</tr>
				            <tr>
				            	<td class="tag"><label>Car&aacute;cter:</label>
								</td>
								<td class="control" colspan="3">
									<c:choose>
										<c:when test="${dependienteEconomico.caracter == 1}">
											C&oacute;nyuge o Concubino
										</c:when>
										<c:when test="${dependienteEconomico.caracter == 2}">
											Dependiente Econ&oacute;mico
										</c:when>
									</c:choose>
								</td>
				            </tr>
				            
				            <!-- Homoclave  y cuenta -->
							<tr>
								<td class="tag" width="20%"><label for="rfc">RFC - Homoclave:</label></td>
							    <td class="valueField" width="20%">
							        <c:out value="${dependienteEconomico.rfc}"/> - <c:out value="${dependienteEconomico.homoclaveRfc}"/>
							    </td>
							</tr>
				           
					        
					     <!-- Apellido paterno y materno -->  
					        <tr>
								<td class="tag" width="20%">
									<label for="apePaterno">Apellido Paterno:</label>
								</td>
								<td class="valueField" width="20%">
								    <c:out value="${dependienteEconomico.apePaterno}"/>
								</td>
								<td class="tag" width="20%">
								    <label for="apeMaterno">Apellido Materno:</label>
								</td>
								<td class="valueField" width="20%">
								    <c:out value="${dependienteEconomico.apeMaterno}"/>
							     </td>
							</tr>
					       
					       <!-- Nombre y fecha de nacimiento -->
					       <tr>
								<td class="tag" width="20%">
								    <label for="nombre">Nombre(s):</label>
								</td>
								<td class="valueField" width="20%">
									<c:out value="${dependienteEconomico.nombre}"/>
								</td>
								<td class="tag" width="20%">
								    <label for="dia">Fecha de Nacimiento:</label>
								</td>
								<td colspan="3" class="valueField" width="20%">
								    <table>
								    <tbody>
								           <tr>
								           <td>
										      <c:out value="${CATALOGO.DIAS[solicitud.diaNacimiento].descripcion}"/>
				                           </td>
				                           <td>
											  <c:out value="${CATALOGO.MESES[solicitud.mesNacimiento].descripcion}"/>                            
										   </td>
				                           <td>
												<c:out value="${CATALOGO.ANIOS[solicitud.anioNacimiento].descripcion}"/>                            
											</td>
				                        </tr>
				                      </tbody>
				                    </table>
								</td>
							</tr>
					       
					       <!-- Genero y estado civil -->
					       <tr>
								<td class="tag" width="20%">
								     <label for="cveGenero">G&eacute;nero:</label>
								</td>
								<td class="valueField" width="20%">
								      <c:set var="cveGenero">${dependienteEconomico.cveGenero}</c:set>
									  <c:out value="${CATALOGO.GENEROS[cveGenero].descripcion}"/>
							    </td>
								<td class="tag" width="20%">
								      <form:label path="cveEdoCivil">Estado Civil:</form:label></td>
								<td class="valueField" width="20%">
							  	      <c:set var="estadoCivil">${dependienteEconomico.cveEdoCivil}</c:set>
									  <c:out value="${CATALOGO.ESTADO_CIVIL[estadoCivil].descripcion}"/>
								</td>
							</tr>
					       
					        <!-- Subseccion lugar de nacimiento -->
					        <tr>
								<td colspan="2" align="left"><span class="subseccion">Lugar de Nacimiento</span></td>
								<td colspan="2">&nbsp;</td>
							</tr>
					       
					       <!-- Pais y nacionalidad -->
					       <tr>
								<td class="tag" width="20%">
								    <label for="idPaisNac">Pa&iacute;s:</label>
								</td>
								<td class="valueField" width="20%">
									<c:set var="paisNac">${dependienteEconomico.idPaisNac}</c:set>
									<c:out value="${CATALOGO.PAISES[paisNac].descripcion}"/>
								</td>
								<td class="tag" width="20%">
								    <label for="idNacionalidad">Nacionalidad:</label>
								</td>
								<td class="valueField" width="20%">
								    <c:set var="nacionalidad">${dependienteEconomico.idNacionalidad}</c:set>
									<c:out value="${CATALOGO.PAISES[nacionalidad].valorCampo2}"/>
								</td>
							</tr>
					       
					       <tr>
								<td class="tag" width="20%">
								    <label for="idEdoNac">Estado:</label>
								</td>
								<td class="valueField" width="20%">
								     <div id="divEstadoNacMexico">
									     <c:set var="idEdoNac">${dependienteEconomico.idEdoNac}</c:set>
										 <c:out value="${CATALOGO.ESTADOS[idEdoNac].descripcion}"/>
							         </div>
							         <div id="divEstadoNacExtranjero" style="display:none">
								         <c:out value="${dependienteEconomico.estadoNacExtranjero}"/>
							         </div>
						        </td>
								<td class="tag" width="20%">
								     <label for="ciudadNacimiento">Ciudad/Poblaci&oacute;n:</label>
								</td>
								<td class="valueField" width="20%">
								     <c:out value="${dependienteEconomico.ciudadNacimiento}"/>
							    </td>
							</tr>
					       
					        <!-- Subseccion datos adicionales -->
					        <tr>
								<td colspan="2" align="left"><span class="subseccion">Datos Adicionales</span></td>
								<td colspan="2">&nbsp;</td>
							</tr>
					       
					       
					       
					       <!-- Curp y profesion -->
					       <tr>
								 <td class="tag" width="20%">
								    <label for="curp">CURP:</label>
								 </td>
								 <td class="valueField" width="20%">
								     <c:out value="${dependienteEconomico.curp}"/>
							     </td>
							     <td class="tag" width="20%">
							          <label for="profesion">Profesi&oacute;n/Ocupaci&oacute;n:</label>
								</td>
							     <td class="valueField" width="20%">
							          <c:set var="cveProfesion"><c:out value="${dependienteEconomico.profesion}"/></c:set>
							          <c:out value="${CATALOGO.OCUPACIONES[cveProfesion].descripcion}"/>
								</td>
								
							</tr>
					       
					       
					       <!-- Dependencia -->
					       	<tr>
					       	    <td class="tag">
					       	        <label for="centroTrabajo">Centro de Trabajo:</label>
					       	    </td>
								<td class="valueField">
									
							    </td>
				                <td class="tag">
				                	
				                </td>
				                <td class="valueField">
									
				                </td>
							</tr>
					       
					       <!-- Tipo y numero de identificacion -->
					       <tr>
								<td class="tag" width="20%">
								     <label for="cveTipoIdentificacion">Identificaci&oacute;n Oficial:</label>
								</td>
							    <td class="valueField" width="20%">
									<c:set var="cveTipoIdentificacion">${dependienteEconomico.cveTipoIdentificacion}</c:set>
								    <c:out value="${CATALOGO.IDENTIFICACIONES[cveTipoIdentificacion].descripcion}"/>
								</td>
								<td class="tag" width="20%">
								    <label for="numeroIdentificacion">N&uacute;mero de Identificaci&oacute;n:</label>
								</td>
								<td class="valueField" width="20%">
								     <c:out value="${dependienteEconomico.numeroIdentificacion}"/>
								</td>
							</tr>
					       
					      </tbody>
					      </table>
					      
					      <br/>
					      <div align="left"><span class="seccion">Datos de Contacto</span></div>
					      <table class="ui-widget-content ui-corner-all seccion-solicitud">
						     <tbody>
						     		<!-- Subseccion datos personales -->
								        <tr>
											<td colspan="2" align="left"><span class="subseccion">Domicilio Particular</span></td>
											<td colspan="2">&nbsp;</td>
										</tr>
							             
								       <tr>
											<td class="tag" width="20%">
											    <label for="calle">Calle/Avenida:</label>
											</td>
											<td class="valueField" width="20%" colspan="3">
												<c:out value="${dependienteEconomico.calle}"/>
										    </td>
							   		  </tr>
							   		  <tr>
							   		  		<td class="tag" width="20%">
											    <label for="numExt">No. Exterior:</label>
											</td>
											<td class="valueField">
												<c:out value="${dependienteEconomico.numExt}"/>
										    </td>
										    <td class="tag" width="20%">
											    <label for="numInt">No. Interior:</label>
											</td>
											<td class="valueField">
											    <c:out value="${dependienteEconomico.numInt}"/>
											</td>
							   		  </tr>
								       
								       <!-- Codigo postal y colonia -->
								       <tr>
											<td class="tag" width="20%">
											    <label for="codigoPostal">C&oacute;digo Postal:</label>
											</td>
											<td class="valueField" width="20%">
											    <c:out value="${dependienteEconomico.codigoPostal}"/>
											</td>
											<td class="tag" width="20%">
											    <label for="colonia">Colonia/Barrio:</label>
											</td>
											<td class="valueField" width="20%">
											    <c:out value="${dependienteEconomico.colonia}"/>
										    </td>
										</tr>
										
										
										<!-- Ciudad y delegacion -->
										<tr>
											<td class="tag" width="20%">
											    <label for="ciudad">Ciudad/Poblaci&oacute;n:</label>
											</td>
											<td class="valueField" width="20%">
												<c:out value="${dependienteEconomico.ciudad}"/>
											</td>
											<td class="tag" width="20%">
											     <label for="idPais">Pa&iacute;s:</label>
											</td>
											<td class="valueField" width="20%">
											    <c:set var="idPais">${dependienteEconomico.idPais}</c:set>
											    <c:out value="${CATALOGO.PAISES[idPais].descripcion}"/>
											</td>
										</tr>
										
										
										<!-- Pais y estado -->
										<tr>
											<td class="tag" width="20%">
											      <label for="idEstado">Estado:</label>
											</td>
											<td class="valueField" width="20%">
												<div id="divEstadoMexico">
												      <c:set var="idEstado">${dependienteEconomico.idEstado}</c:set>
											    	  <c:out value="${CATALOGO.ESTADOS[idEstado].descripcion}"/>
											    </div>  
											    <div id="divEstadoExtranjero" style="display: none">
												      <c:out value="${dependienteEconomico.estadoExtranjero}"/>
											    </div>
											</td>
											<td class="tag" width="20%">
											      <label for="idMunicipio">Delegaci&oacute;n/Municipio:</label>
											</td>
											<td class="valueField" width="20%">
											 	<div id="divMunicipioMexico">
													<c:set var="idMunicipio">${dependienteEconomico.idMunicipio}</c:set>
							  			    	    <c:out value="${CATALOGO.MUNICIPIOS[idMunicipio].descripcion}"/>
												</div>
												<div id="divMunicipioExtranjero" style="display: none">
												      <c:out value="${dependienteEconomico.municipioExtranjero}"/>
											    </div>
											</td>
										</tr>
								       
								        <!-- Subseccion telefonos de contacto -->
								        <tr>
											<td colspan="2" align="left"><span class="subseccion">Tel&eacute;fonos de Contacto</span></td>
											<td colspan="2">&nbsp;</td>
										</tr>
								       
								       <tr>
											<td class="tag" width="20%">
											    <label for="telefonoCasa">Tel&eacute;fono Domicilio (Con LADA):</label>
											</td>
											<td class="valueField" width="20%">
											     <c:out value="${dependienteEconomico.telefonoCasa}"/>
											     
										     </td>
											<td class="tag" width="20%">
											     <label for="telefonoCelular">Tel&eacute;fono Celular (044):</label>
											</td>
											<td class="valueField" width="20%">
												 <c:out value="${dependienteEconomico.telefonoCelular}"/>
										    </td>
										</tr>
							
										<tr>
											<td class="tag" width="20%">
											     <label for="telefonoTrabajo">Tel&eacute;fono Oficina/Trabajo:</label>
											</td>
											<td class="valueField" width="20%">
												 <c:out value="${dependienteEconomico.telefonoTrabajo}"/>
											</td>
											<td class="tag" width="20%">
											     <label for="extension">N&uacute;m. Extensi&oacute;n:</label>
											</td>
											<td class="valueField" width="20%">
												 <c:out value="${dependienteEconomico.extension}"/>
										    </td>
										</tr>
							
										 <!-- Subseccion correo electronico -->
								        <tr>
											<td colspan="2" align="left"><span class="subseccion">Correo El&eacute;ctronico</span></td>
											<td colspan="2">&nbsp;</td>
										</tr>
							
										<tr>
											<td class="tag" width="20%">
											      <label for="correoPersonal">Correo Personal:</label>
											</td>
											<td class="valueField" width="20%">
										           <c:out value="${dependienteEconomico.correoPersonal}"/>
											</td>
											<td class="tag" width="20%">
											       <label for="correoTrabajo">Correo Trabajo:</label>
											</td>
											<td class="valueField" width="20%">
												    <c:out value="${dependienteEconomico.correoTrabajo}"/>
											</td>
										</tr>
							</tbody>
						  </table>      
				</form:form>
			</div>
			<form:errors path="*"/>
			<form name="regresar" action=""></form>
		</div>
		<div align="center">
			<button type="button" class="btnEnviar" onclick="cerrarDependiente()" ><span><em>Regresar</em></span></button>
		</div>
		<br/>
		<br/>
</div>
</body>
</html>