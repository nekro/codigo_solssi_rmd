<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

<c:set var="estatus"><c:out value="${solicitud.cveStatus}"/></c:set>
	<div align="center">
		<table class="ui-widget-content ui-state-focus ui-corner-all seccion-solicitud">
				<tr>
					<td width="25%" align="right">
						Folio:						
					</td>
					<td width="25%" class="valueField">
						<c:out value="${solicitud.userFolio}"/>
					</td>
					<td width="25%" align="right">
						Estatus:						
					</td>
					<td class="valueField">
						<c:out value="${CATALOGO.ESTATUS_TRAMITE[estatus].valorCampo2}"/>
					</td>
				</tr>
				<tr>
					
				</tr>
				<c:if test="${estatus eq '5' or estatus eq '8'}">
					<tr>
						<td width="25%" align="right">
							Motivo Rechazo :						
						</td>
						<td class="valueField" colspan="3">
							<c:out value="${solicitud.motivoRechazo}"/>
						</td>
					</tr>			
				</c:if>
				<c:if test="${estatus eq '7'}">
					<tr>
						<td width="25%" align="right">
							Fecha de Pago :						
						</td>
						<td class="valueField">
							<c:out value="${solicitud.fechaPago}"/>
						</td>
					</tr>
				</c:if>
		</table>
	</div>


<div id="containerSolicitud" align="center" class="consultaSolicitud">      
      
	<form name="solicitud" id="solicitud" method="post" action="${urlProcesa}">
	     
	    <input type="hidden" name="checkClabe">
	    
		<br/>
		<div align="left"><span class="seccion">Tipo de Tr&aacute;mite</span></div>
		<table class="ui-widget-content ui-corner-all seccion-solicitud">
			<tbody>
			     <!--  Tipo de tramite -->
			     <!-- Subseccion tipo de tramite  -->
			    <tr>
					<td  align="left" colspan="2"><span class="subseccion">Rescate Parcial</span></td>
				</tr>	
				
				<!--  Porcentaje de retiro -->
				<tr>
					
					<td width="25%" align="right">
							<label for="porcentaje">Porcentaje Rescate Parcial:</label>
				    </td>
				    <td align="left" style="color: black;">
				    		<c:out value="${solicitud.porcentaje}"/>
							<span>%</span>
				    </td>
	
				</tr>
				
				<!-- Lugar de elaboracion -->
				<tr>
					<td width="25%" align="right">
					    <label for="estadoElaboracion">Lugar de Elaboraci&oacute;n:</label>
					</td>
					<td align="left" style="color: black;">
					    <table>
					       <tbody>
					          <tr>
					             <td>
					                  <label for="estadoElaboracion">Ciudad/Poblaci&oacute;n:</label>
					                  <c:out value="${solicitud.ciudadElaboracion}"/>
					             </td> 
					             <td>
								      <label for="estadoElaboracion">Estado:</label>
								      <c:set var="estadoElaboracion"><c:out value="${solicitud.estadoElaboracion}"/></c:set>
								      <c:out value="${CATALOGO.ESTADOS[estadoElaboracion].descripcion}"/>
					             </td>
					          </tr>
				           </tbody>
				          </table>	 	
					</td>
				</tr>
	        	</tbody>
		</table>
		
		<br/>
		
		<div align="left" class="seccion">Datos Generales de Asegurado</div>
		<table class="ui-widget-content ui-corner-all seccion-solicitud">
			<tbody>
				   
	            
	            <!-- Subseccion datos personales -->
		        <tr>
					<td colspan="2" align="left"><span class="subseccion">Datos Personales</span></td>
					<td colspan="2">&nbsp;</td>
				</tr>
	            
	            
	            <!-- Homoclave  y cuenta -->
				<tr>
					<td class="tag" ><label for="rfc">RFC - Homoclave:</label></td>
				    <td class="valueField" >
				        <c:out value="${solicitud.rfc}"/> - <c:out value="${solicitud.homoclaveRfc}"/>
				    </td>
				    <td class="tag" ><label for="cuenta">N&uacute;mero de Cuenta:</label>
				    </td>
				    <td class="valueField">
				        <c:out value="${solicitud.cuenta}"/>
				    </td>    
				</tr>
				
		     <!-- Apellido paterno y materno -->  
		        <tr>
					<td class="tag" >
						<label for="apePaterno">Apellido Paterno:</label>
					</td>
					<td class="valueField" >
					    <c:out value="${solicitud.apePaterno}"/>
					</td>
					<td class="tag" >
					    <label for="apeMaterno">Apellido Materno:</label>
					</td>
					<td class="valueField" >
					    <c:out value="${solicitud.apeMaterno}"/>
				     </td>
				</tr>
		       
		       <!-- Nombre y fecha de nacimiento -->
		       <tr>
					<td class="tag" >
					    <label for="nombre">Nombre(s):</label>
					</td>
					<td class="valueField" >
						<c:out value="${solicitud.nombre}"/>
					</td>
					<td class="tag" >
					    <label for="dia">Fecha de Nacimiento:</label>
					</td>
					<td colspan="3" class="valueField" >
					    <table>
					    <tbody>
					           <tr>
					           <td>
							      <c:out value="${CATALOGO.DIAS[solicitud.diaNacimiento].descripcion}"/>
	                           </td>
	                           <td>
								  <c:out value="${CATALOGO.MESES[solicitud.mesNacimiento].descripcion}"/>                            
							   </td>
	                           <td>
									<c:out value="${CATALOGO.ANIOS[solicitud.anioNacimiento].descripcion}"/>                            
								</td>
	                        </tr>
	                      </tbody>
	                    </table>
					</td>
				</tr>
		       
		       <!-- Genero y estado civil -->
		       <tr>
					<td class="tag" >
					     <label for="cveGenero">G&eacute;nero:</label>
					</td>
					<td class="valueField" >
					      <c:set var="cveGenero"><c:out value="${solicitud.cveGenero}"/></c:set>
						  <c:out value="${CATALOGO.GENEROS[cveGenero].descripcion}"/>
				    </td>
					<td class="tag" >
					      <label>Estado Civil:</label></td>
					<td class="valueField" >
				  	      <c:set var="estadoCivil"><c:out value="${solicitud.cveEdoCivil}"/></c:set>
						  <c:out value="${CATALOGO.ESTADO_CIVIL[estadoCivil].descripcion}"/>
					</td>
				</tr>
		       
		        <!-- Subseccion lugar de nacimiento -->
		        <tr>
					<td colspan="2" align="left"><span class="subseccion">Lugar de Nacimiento</span></td>
					<td colspan="2">&nbsp;</td>
				</tr>
		       
		       <!-- Pais y nacionalidad -->
		       <tr>
					<td class="tag" >
					    <label for="idPaisNac">Pa&iacute;s:</label>
					</td>
					<td class="valueField" >
						<c:set var="paisNac"><c:out value="${solicitud.idPaisNac}"/></c:set>
						<c:out value="${CATALOGO.PAISES[paisNac].descripcion}"/>
					</td>
					<td class="tag" >
					    <label for="idNacionalidad">Nacionalidad:</label>
					</td>
					<td class="valueField" >
					    <c:set var="nacionalidad"><c:out value="${solicitud.idNacionalidad}"/></c:set>
						<c:out value="${CATALOGO.PAISES[nacionalidad].valorCampo2}"/>
					</td>
				</tr>
		       
		       <tr>
					<td class="tag" >
					    <label for="idEdoNac">Estado:</label>
					</td>
					<td class="valueField" >
					     <div id="divEstadoNacMexico">
						     <c:set var="idEdoNac"><c:out value="${solicitud.idEdoNac}"/></c:set>
							 <c:out value="${CATALOGO.ESTADOS[idEdoNac].descripcion}"/>
				         </div>
				         <div id="divEstadoNacExtranjero" style="display:none">
					         <c:out value="${solicitud.estadoNacExtranjero}"/>
				         </div>
			        </td>
					<td class="tag" >
					     <label for="ciudadNacimiento">Ciudad/Poblaci&oacute;n:</label>
					</td>
					<td class="valueField" >
					     <c:out value="${solicitud.ciudadNacimiento}"/>
				    </td>
				</tr>
		       
		        <!-- Subseccion datos adicionales -->
		        <tr>
					<td colspan="2" align="left"><span class="subseccion">Datos Adicionales</span></td>
					<td colspan="2">&nbsp;</td>
				</tr>
		       
		       
		       
		       <!-- Curp y profesion -->
		       <tr>
					 <td class="tag" >
					    <label for="curp">CURP:</label>
					 </td>
					 <td class="valueField" >
					     <c:out value="${solicitud.curp}"/>
				     </td>
				     <td class="tag" >
				          <label for="profesion">Profesi&oacute;n/Ocupaci&oacute;n:</label>
					</td>
				     <td class="valueField" >
				          <c:out value="${solicitud.profesion}"/>
					</td>
					
				</tr>
		       
		       
		       <!-- Dependencia -->
		       	<tr>
		       	    <td class="tag">
		       	        <label for="centroTrabajo">Centro de Trabajo:</label>
		       	    </td>
					<td class="valueField">
						<c:set var="centroTrabajo"><c:out value="${solicitud.centroTrabajo}"/></c:set>
					    <c:out value="${CATALOGO.DEPENDENCIAS[centroTrabajo].descripcion}"/>
				    </td>
	                <td class="tag">
		       	        <label for="nivelPuesto">Nivel de Puesto:</label>
	                </td>
	                <td class="valueField">
						<c:set var="nivelPuesto"><c:out value="${solicitud.nivelPuesto}"/></c:set>
					    <c:out value="${CATALOGO.NIVEL_PUESTO[nivelPuesto].descripcion}"/>
	                </td>
				</tr>
		       
		       
		       <!-- Contribuyente en EU -->
		       
		       <tr>
					<td class="tag" >
					   <label for="contribuyenteEUA">&iquest;Es usted contribuyente de impuestos en E.U.A?</label>
					</td>
					<td class="valueField" >    
					    <c:choose>
					    	<c:when test="${solicitud.contribuyenteEUA}">
					    		Si
					    	</c:when>
					    	<c:otherwise>
					    		No
					    	</c:otherwise>
					    </c:choose>
					    
				    </td>
					<td class="tag" >
					     <div id="divNSSLabel" style="display:none">
					     <label for="numSsn">N&uacute;mero de Seguridad Social (NSS):</label>
					     </div>
					</td>
					<td class="valueField" >
					     <div id="divNSS" style="display: none">
					          <c:out value="${solicitud.numSsn}"/>
					     </div>
					</td>
	
				</tr>
		       
		       <!-- Tipo y numero de identificacion -->
		       <tr>
					<td class="tag" >
					     <label for="cveTipoIdentificacion">Identificaci&oacute;n Oficial:</label>
					</td>
				    <td class="valueField" >
						<c:set var="cveTipoIdentificacion"><c:out value="${solicitud.cveTipoIdentificacion}"/></c:set>
					    <c:out value="${CATALOGO.IDENTIFICACIONES[cveTipoIdentificacion].descripcion}"/>
					</td>
					<td class="tag" >
					    <label for="numeroIdentificacion">N&uacute;mero de Identificaci&oacute;n:</label>
					</td>
					<td class="valueField" >
					     <c:out value="${solicitud.numeroIdentificacion}"/>
					</td>
				</tr>
		       
		      </tbody>
		      </table>
		      
		      <br/>
		      <div align="left"><span class="seccion">Datos de Contacto</span></div>
		      <table class="ui-widget-content ui-corner-all seccion-solicitud">
			     <tbody>
	            
	            <!-- Subseccion datos personales -->
		        <tr>
					<td colspan="2" align="left"><span class="subseccion">Domicilio Particular</span></td>
					<td colspan="2">&nbsp;</td>
				</tr>
	             
		       <tr>
					<td class="tag" >
					    <label for="calle">Calle/Avenida:</label>
					</td>
					<td class="valueField"  colspan="3">
						<c:out value="${solicitud.calle}"/>
				    </td>
	   		  </tr>
	   		  <tr>
	   		  		<td class="tag" >
					    <label for="numExt">No. Exterior:</label>
					</td>
					<td class="valueField">
						<c:out value="${solicitud.numExt}"/>
				    </td>
				    <td class="tag" >
					    <label for="numInt">No. Interior:</label>
					</td>
					<td class="valueField">
					    <c:out value="${solicitud.numInt}"/>
					</td>
	   		  </tr>
		       
		       <!-- Codigo postal y colonia -->
		       <tr>
					<td class="tag" >
					    <label for="codigoPostal">C&oacute;digo Postal:</label>
					</td>
					<td class="valueField" >
					    <c:out value="${solicitud.codigoPostal}"/>
					</td>
					<td class="tag" >
					    <label for="colonia">Colonia/Barrio:</label>
					</td>
					<td class="valueField" >
					    <c:out value="${solicitud.colonia}"/>
				    </td>
				</tr>
				
				
				<!-- Ciudad y delegacion -->
				<tr>
					<td class="tag" >
					    <label for="ciudad">Ciudad/Poblaci&oacute;n:</label>
					</td>
					<td class="valueField" >
						<c:out value="${solicitud.ciudad}"/>
					</td>
					<td class="tag" >
					     <label for="idPais">Pa&iacute;s:</label>
					</td>
					<td class="valueField" >
					    <c:set var="idPais"><c:out value="${solicitud.idPais}"/></c:set>
					    <c:out value="${CATALOGO.PAISES[idPais].descripcion}"/>
					</td>
				</tr>
				
				
				<!-- Pais y estado -->
				<tr>
					<td class="tag" >
					      <label for="idEstado">Estado:</label>
					</td>
					<td class="valueField" >
						<div id="divEstadoMexico">
						      <c:set var="idEstado"><c:out value="${solicitud.idEstado}"/></c:set>
					    	  <c:out value="${CATALOGO.ESTADOS[idEstado].descripcion}"/>
					    </div>  
					    <div id="divEstadoExtranjero" style="display: none">
						      <c:out value="${solicitud.estadoExtranjero}"/>
					    </div>
					</td>
					<td class="tag" >
					      <label for="idMunicipio">Delegaci&oacute;n/Municipio:</label>
					</td>
					<td class="valueField" >
					 	<div id="divMunicipioMexico">
							<c:set var="idMunicipio"><c:out value="${solicitud.idMunicipio}"/></c:set>
	  			    	    <c:out value="${CATALOGO.MUNICIPIOS[idMunicipio].descripcion}"/>
						</div>
						<div id="divMunicipioExtranjero" style="display: none">
						      <c:out value="${solicitud.municipioExtranjero}"/>
					    </div>
					</td>
				</tr>
		       
		        <!-- Subseccion telefonos de contacto -->
		        <tr>
					<td colspan="2" align="left"><span class="subseccion">Tel&eacute;fonos de Contacto</span></td>
					<td colspan="2">&nbsp;</td>
				</tr>
		       
		       <tr>
					<td class="tag" >
					    <label for="telefonoCasa">Tel&eacute;fono Domicilio (Con LADA):</label>
					</td>
					<td class="valueField" >
					     <c:out value="${solicitud.telefonoCasa}"/>
					     
				     </td>
					<td class="tag" >
					     <label for="telefonoCelular">Tel&eacute;fono Celular (044):</label>
					</td>
					<td class="valueField" >
						 <c:out value="${solicitud.telefonoCelular}"/>
				    </td>
				</tr>
	
				<tr>
					<td class="tag" >
					     <label for="telefonoTrabajo">Tel&eacute;fono Oficina/Trabajo:</label>
					</td>
					<td class="valueField" >
						 <c:out value="${solicitud.telefonoTrabajo}"/>
					</td>
					<td class="tag" >
					     <label for="extension">N&uacute;m. Extensi&oacute;n:</label>
					</td>
					<td class="valueField" >
						 <c:out value="${solicitud.extension}"/>
				    </td>
				</tr>
	
				 <!-- Subseccion correo electronico -->
		        <tr>
					<td colspan="2" align="left"><span class="subseccion">Correo El&eacute;ctronico</span></td>
					<td colspan="2">&nbsp;</td>
				</tr>
	
				<tr>
					<td class="tag" >
					      <label for="correoPersonal">Correo Personal:</label>
					</td>
					<td class="valueField" >
				           <c:out value="${solicitud.correoPersonal}"/>
					</td>
					<td class="tag" >
					       <label for="correoTrabajo">Correo Trabajo:</label>
					</td>
					<td class="valueField" >
						    <c:out value="${solicitud.correoTrabajo}"/>
					</td>
				</tr>
			</tbody>
		</table>
		       
		         <br/>
		<div align="left"><span class="seccion">Politicamente Expuesto</span></div>      
		 <table class="ui-widget-content ui-corner-all seccion-solicitud">
			     <tbody>
		            <tr>
						<td colspan="4">
							<p style="text-align: justify;">
								&iquest;Desempe&ntilde;a o ha desempe&ntilde;ado usted, su conyugue o
								un familiar por consanguinidad o afinidad de hasta segundo grado,
								funciones p&uacute;blicas destacadas en territorio nacional o en el
								extranjero? Se considera Persona Politicamente Expuesta , entre
								otros, a los jefes de estado o de gobierno, l&iacute;deres
								p&oacute;liticos, funcionarios gubernamentales, judiciales o
								militares de alta jerarquia, altos ejecutivos de empresas estatales o
								funcionarios o miembros importantes de partidos p&oacute;liticos.
							</p>	
						</td>
				        
				</tr>
				<tr>
				
				<td colspan="4" align="center">
				     <c:choose>
				     	<c:when test="${solicitud.politicamenteExpuesta}">
				     		Si	
				     	</c:when>
				     	<c:otherwise>
				     	    No
				     	</c:otherwise>
				     </c:choose>
			    </td>
			    </tr>
			    <tr>
			    	<td colspan="4" align="center">
			    		<c:if test="${not empty solicitud.dependientes}">
			    			<a href="javascript:dependientes()" style="text-decoration: underline;color: blue;">Ver Dependientes Econ&oacute;micos</a>
			    		</c:if>
			    	</td>
			    </tr>
		</tbody>
		
		</table>
		<br/>	
		<div align="left"><span class="seccion">Modo de Pago</span></div>       
		<table class="ui-widget-content ui-corner-all seccion-solicitud">
			     <tbody>
				       <c:choose>
				       <c:when test="${solicitud.modoPago eq 1}">
					       <!-- Banco y cuenta clave -->
					       <tr>
						    	<td colspan="4" align="left">
						    		<span class="subseccion">Transferencia Bancaria</span>
						    	</td>
						    </tr>
					       <tr>
								<td class="tag" >
								      <label for="banco">Banco:</label>
								</td>
								<td class="valueField" >
								      <c:set var="banco"><c:out value="${solicitud.banco}"/></c:set>
					  			      <c:out value="${CATALOGO.BANCOS[banco].descripcion}"/>
								</td>
								<td class="tag" >
								      <label for="clabe">CLABE:</label>
								</td>
								<td class="valueField" >
									  <c:choose>
									  		<c:when test="${auth and not solicitud.bndUltimosDigitosClabe}">}">
								      			<c:out value="${solicitud.maskClabe}"/>
								      		</c:when>
								      		<c:otherwise>
								      			<c:out value="${solicitud.clabe}"/>
								      		</c:otherwise>
									  </c:choose>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
						    <tr>
						    	<td colspan="4" align="left">
						    		<span class="subseccion">Cheque</span>
						    	</td>
						    </tr>
						    <tr>
								<td align="right">
								      <label for="banco">Estado:</label>
								</td>
								<td  align="left" style="color: black;">
								      <c:set var="estadoPromotoria"><c:out value="${solicitud.estadoPromotoria}"/></c:set>
					  			      <c:out value="${CATALOGO.ESTADOS[estadoPromotoria].descripcion}"/>
								</td>
								<td align="right">
								      <label for="clabe">Oficina de Entrega:</label>
								</td>
								<td  align="left" style="color: black;">
									  <c:set var="clavePromotoria"><c:out value="${solicitud.clavePromotoria}"/></c:set>
								      <c:out value="${CATALOGO.OFICINAS_CHEQUE[clavePromotoria].descripcion}"/>
								</td>
							</tr>	
						</c:otherwise>
					</c:choose>
		       
		      </tbody>
		     </table> 
	</form>
</div>
