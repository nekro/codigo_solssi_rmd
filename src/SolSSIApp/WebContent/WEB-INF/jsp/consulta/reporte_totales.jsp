<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>


<script>
	var windowPDF;
	
	function buscar() {
		document.forms["filtro"].target = "_top";
		document.forms["filtro"].action = "<s:url value='/consulta/totales/execute'/>";
		submitForm(document.forms["filtro"]);
	}

</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

</style>


<div class="moduleTitle"><span>Reporte Totales</span></div>
	<div id="filtro" style="width: 90%" align="center">
		<form:form modelAttribute="consultaFilter" name="filtro">
			<table class="ui-widget-content ui-corner-all">
				<tr >
					<td align="center"><span class="titleSeccion">Filtro de B&uacute;squeda</span>
					</td>
				</tr>
				<tr>
					<td align="left">
						<ul>
							<li>
								<span>Dependencia:</span> 
								<form:select path="dependencia" cssStyle="width:230px" htmlEscape="true">
									<option value="0">TODAS</option>
									<form:options items="${CATALOGO.DEPENDENCIAS}" itemLabel="descripcion"/>
								</form:select>
							</li>
							<li>
								<button type="button" class="btnEnviar" onclick="buscar()"><span><em>Buscar</em></span></button>
							</li>		
	
						</ul>
					</td>
				</tr>
			</table>
			<s:hasBindErrors name="consultaFilter">
				<br/><br/>
				<div class="ui-state-error ui-corner-all" style="width: 80%;padding-top: 10px;padding-bottom: 10px">
					<form:errors path="*"/>
				</div>
			</s:hasBindErrors>
		</form:form>
	</div>
	
	

<c:if test="${not empty reporte}">
	
		<br/>
		<h4>RESCATES</h4>
		<div id="tablaTramites" align="center" class="ui-widget-content" style="width: 60%">
		
		<table class="display">
			<tr class="ui-state-default  ui-corner-all">
				<td align="center">TOTAL</td>
				<td align="center">Iniciadas</td>
				<td align="center">No Satisfactorias</td>
				<td align="center">Satisfactorias</td>
				<td align="center">Canceladas</td>
			</tr>
			<tr>
				<td align="center"><c:out value="${reporte.total}"/></td>
				<td align="center"><c:out value="${reporte.iniciadas}"/></td>
				<td align="center"><c:out value="${reporte.rechazadas}"/></td>
				<td align="center"><c:out value="${reporte.aceptadas}"/></td>
				<td align="center"><c:out value="${reporte.canceladas}"/></td>
			</tr>
		</table>
			
		</div>
		
</c:if>


<c:if test="${not empty reporteEsquemas}">
	
		<br/>
		<h4>ESQUEMAS</h4>
		<div id="tablaTramites" align="center" class="ui-widget-content" style="width: 60%">
		
		<table class="display">
			<tr class="ui-state-default  ui-corner-all">
				<td align="center">TOTAL</td>
				<td align="center">Esquema 1</td>
				<td align="center">Esquema 2</td>
				<td align="center">Esquema 3</td>
			</tr>
			<tr>
				<td align="center"><c:out value="${reporteEsquemas.total}"/></td>
				<td align="center"><c:out value="${reporteEsquemas.iniciadas}"/></td>
				<td align="center"><c:out value="${reporteEsquemas.aceptadas}"/></td>
				<td align="center"><c:out value="${reporteEsquemas.rechazadas}"/></td>
			</tr>
		</table>
			
		</div>
		
</c:if>
