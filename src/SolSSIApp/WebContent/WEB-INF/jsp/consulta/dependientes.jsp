<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!DOCTYPE link PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

		<link href="<s:url value="/resources/css/solssi.css" />" rel="stylesheet"  type="text/css" />
	    <link href="<s:url value="/resources/css/tablas.css" />" rel="stylesheet"  type="text/css" />
	    <link href="<s:url value="/resources/css/ui/estilos_ui.css"/>" rel="stylesheet"  type="text/css" />

		<script type="text/javascript" src="<s:url value="/resources/js/ssi_library_1.0.0.js"/>"></script>
		<script type="text/javascript" src="<s:url value="/resources/js/dojo/dojo.xd.js"/>"></script>
		
		<style>
		
		    #container{
		    	background-image: none !important;
		    }
		    
		    #contenido{
		    	font-size: 12px !important;
		    }
		    
		    p{
		        font-size: 12px !important;
		    }
		    
		    label{
		        font-size: 12px !important;
		    }
		    
		    th{
		    	font-size: 12px !important;
		    }
		</style>
	<script>
		
		function consultarDependiente(numero){
			document.forms["form"].action = '<s:url value="/consulta/info/dependiente"/>/';
			document.forms["form"].numero.value = numero;
			document.forms["form"].submit();
		}
		
	</script>
</head>
<body style="background-image: none;">	
	
	<div id="container" align="center">
	   <div class="welcomeLeft">
		 <br/>
		 <table width="95%">
		 <tr>
		  <td width="15px">&nbsp;</td>
		  <td align="left" valign="bottom" width="50%">
		    <img src="<s:url value="/resources"/>/images/logoblanco2.png"/>
		  </td>
		  <td align="right" valign="bottom">
			   &nbsp;
		  </td>
		 </tr>  
		 </table> 
		</div>
	
		<div id="contenido" align="center" class="ui-widget-content ui-corner-all" >      
	
			<div class="moduleTitle"><span>Informaci&oacute;n de Dependientes Econ&oacute;micos</span></div>
			
			<div align="center">
					<c:if test="${not empty sessionScope.solicitud.dependientes}">
						<span class="moduleTitle">Lista</span>
									<table class="ui-widget-content ui-corner-all display" style="width: 75%" align="center">
									  <thead class="ui-widget-header">
										<tr>
											<th>
												No.
											</th>
											<th>
												NOMBRE
											</th>
											<th>
												CAR&Aacute;CTER
											</th>
											<th>
												VER
											</th>
										</tr>
									</thead>	
										<c:forEach var="dep" items="${solicitud.dependientes}" varStatus="st">
										    <c:choose>
										    	<c:when test="${st.count % 2 == 1}">
										    		<c:set var="css" value="odd"/>
										    	</c:when>
										    	<c:otherwise>
										    		<c:set var="css" value="even"/>
										    	</c:otherwise>
										    </c:choose>
											<tr class="${css}">
												<td>
												    ${st.count}
												</td>
												<td>${dep.nombre} ${dep.apePaterno} ${dep.apeMaterno}</td>
												<td>
													<c:choose>
														<c:when test="${dep.caracter eq 1}">
															C&Oacute;NYUGE O CONCUBINO
														</c:when>
														<c:when test="${dep.caracter eq 2}">
															DEPENDIENTE ECON&Oacute;MICO
														</c:when>
													</c:choose>
												</td>
												<td><a href='javascript:consultarDependiente(${dep.pk.numero})'>Desplegar</a></td>
											</tr>
										</c:forEach>
									</table>
									<br/><br/><br/>
						</c:if>					
			</div>
			<div>
				<button type="button" class="btnEnviar" onclick="window.close()"><span><em>Cerrar</em></span></button>
			</div>
		</div>
		
	</div>
	<form name="form" action="" method="post">
		<input type="hidden" name="numero"/>
	</form>
</body>				
</html>