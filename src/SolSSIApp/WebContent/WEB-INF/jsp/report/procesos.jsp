<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

<link href="<s:url value='/resources/css/dijit/themes/claro/Calendar.css'/>" rel="stylesheet" type="text/css" />

<style>

#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

</style>

<script type="text/javascript">

	dojo.require("dijit.Calendar");
	dojo.require("dijit.Dialog");
	
	var calendarIniDlg = null;
	var calendarFinDlg = null;
	
	
	dojo.ready(function(){
		calendarIniDlg = new dijit.Dialog({ title:"Seleccione la Fecha"}, "calendarIniDlg");
	  	calendarIniDlg.startup();
	  	
	  	calendarFinDlg = new dijit.Dialog({ title:"Seleccione la Fecha"}, "calendarFinDlg");
	  	calendarFinDlg.startup();
	    
	});
	
	function buscar() {
		document.forms["filtro"].action = "<s:url value='/report/procesos/execute/consulta'/>";
		submitForm(document.forms["filtro"]);
	}
	
	function showCalendarIni(){
		
		calendarIniDlg.show();
		
		var calIni = new dijit.Calendar(new Date(), dojo.byId("calFechaInicio"));
	    dojo.connect(calIni, "onValueSelected", function(date){
	    	var fmt = dojo.date.locale.format(date, {datePattern: "dd/MM/yyyy", selector: "date"});
	    	dojo.byId("fechaInicio").value = fmt;
	    	calendarIniDlg.hide();
	    });
	}
	
	function showCalendarFin(){
	
		calendarFinDlg.show();
		
		var calFin = new dijit.Calendar(new Date(), dojo.byId("calFechaFin"));
	    dojo.connect(calFin, "onValueSelected", function(date){
	    	var fmt = dojo.date.locale.format(date, {datePattern: "dd/MM/yyyy", selector: "date"});
	    	dojo.byId("fechaFin").value = fmt;
	    	calendarFinDlg.hide();
	    });
	}
	
	function cerrarIni(){
		calendarIniDlg.hide();
	}
	
	function cerrarFin(){
		calendarFinDlg.hide();
	}
	
	function oficiosProcesos(id){
		document.forms["proceso"].action = "<s:url value='/report/procesos/oficios'/>";
		document.forms["proceso"].idProceso.value = id;
		submitForm(document.forms["proceso"]); 
	}
	
	function solicitudesProcesos(proceso, estatus){
		document.forms["proceso"].action = "<s:url value='/report/procesos/solicitudes'/>";
		document.forms["proceso"].idProceso.value = proceso;
		document.forms["proceso"].estatus.value = estatus;
		submitForm(document.forms["proceso"]); 
	}
	
	function solicitudesEstatus(proceso, estatus){
		document.forms["proceso"].action = "<s:url value='/report/procesos/solicitudes/estatus'/>";
		document.forms["proceso"].idProceso.value = proceso;
		document.forms["proceso"].estatus.value = estatus;
		submitForm(document.forms["proceso"]); 
	}
	
	
</script>


<div class="moduleTitle"><span>Reporte de Procesos</span></div>
	<div id="filtro" style="width: 90%" align="center">
		<form name="filtro" method="post">
			<table class="ui-widget-content ui-corner-all">
				<tr >
					<td align="center"><span class="titleSeccion">Filtro de B&uacute;squeda</span>
					</td>
				</tr>
				<tr>
					<td align="left">
						<ul>
							<li>
								<span>Fecha Inicio: de </span> 
								<input name="fechaInicio" type="text" id="fechaInicio" onclick="showCalendarIni()" size="10"/>
								a
								<input name="fechaFin" type="text" id="fechaFin" onclick="showCalendarFin()" size="10"/>
							</li>
							<li>
								<button type="button" class="btnEnviar" onclick="buscar()"><span><em>Buscar</em></span></button>
							</li>		
	
						</ul>
					</td>
				</tr>
			</table>
		</form>
	</div>
	
<c:if test="${not empty procesos}">
	
		<br/>
		<h4>Resultados <c:out value="${fechaInicio}"/> - <c:out value="${fechaFin}"/></h4>
		<div id="tablaTramites" align="center" class="ui-widget-content" style="width: 98%">
		
		<table class="display">
			<thead>
			<tr class="ui-widget-header  ui-corner-all">
				<th align="center">ID Proceso</th>
				<th align="center">Fecha Env&iacute;o</th>
				<th align="center">Fecha Recepci&oacute;n</th>
				<th align="center">Solicitudes Procesadas</th>
				<th align="center">Solicitudes Enviadas</th>
				<th align="center">Solicitudes Errores</th>
				<th align="center">Solicitudes Aceptadas</th>
				<th align="center">Solicitudes Rechazadas</th>
				<th align="center">Pendientes Respuesta</th>
				<th align="center">Solicitudes Pagadas</th>
				<th align="center">Rechazadas Por Banco</th>
			</tr>
			</thead>
			<tbody>
			<c:forEach items="${procesos}" var="proceso" varStatus="st">
				<c:choose>
					    	<c:when test="${st.count % 2 == 1}">
					    		<c:set var="css" value="odd"/>
					    	</c:when>
					    	<c:otherwise>
					    		<c:set var="css" value="even"/>
					    	</c:otherwise>
				</c:choose>
				<tr class="${css}">
					<td align="center">
						<c:out value="${proceso.id}"/>
					</td>
					<td align="center"><fmt:formatDate value="${proceso.fechaInicio}" pattern="dd/MM/yyyy HH:mm:ss"/></td>
					<td align="center"><fmt:formatDate value="${proceso.fechaFin}" pattern="dd/MM/yyyy HH:mm:ss"/></td>
					<td align="center"><c:out value="${proceso.totalSolicitudesProcesadas}"/></td>
					<td align="center">
						<a href="javascript:solicitudesProcesos(<c:out value='${proceso.id}'/>, 4)">
							<c:out value="${proceso.totalSolicitudesEnviadas}"/>
						</a>
						
					</td>
					<td align="center">
						<a href="javascript:solicitudesEstatus(<c:out value='${proceso.id}'/>, 2)">
							<c:out value="${proceso.totalSolicitudesErrores}"/>
						</a>
					</td>
					<td align="center">
						<a href="javascript:solicitudesProcesos(<c:out value='${proceso.id}'/>, 6)">
							<c:out value="${proceso.totalSolicitudesAceptadas}"/>
						</a>
					</td>
					<td align="center">
						<a href="javascript:solicitudesProcesos(<c:out value='${proceso.id}'/>, 5)">
							<c:out value="${proceso.totalSolicitudesRechazadas}"/>
						</a>
					</td>
					<td align="center">
						<c:choose>
							<c:when test="${proceso.totalSolicitudesAceptadas eq 0}">
								<c:out value="${proceso.totalSolicitudesEnviadas - (proceso.totalSolicitudesAceptadas + proceso.totalSolicitudesErrores)}"/>
							</c:when>
							<c:otherwise>
								<a href="javascript:solicitudesEstatus(<c:out value='${proceso.id}'/>, 4)">
									<c:out value="${proceso.totalSolicitudesEnviadas - (proceso.totalSolicitudesAceptadas + proceso.totalSolicitudesErrores)}"/>
								</a>
							</c:otherwise>
						</c:choose>
					</td>
					<td align="center">
						<a href="javascript:solicitudesEstatus(<c:out value='${proceso.id}'/>, 7)">
							<c:out value="${proceso.totalSolicitudesPagadas}"/>
						</a>
					</td>
					<td align="center">
						<a href="javascript:solicitudesEstatus(<c:out value='${proceso.id}'/>, 8)">
							<c:out value="${proceso.totalSolicitudesRechazadasBanco}"/>
						</a>
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
			
		</div>
		
</c:if>

<c:if test="${not empty error}">
	<br/><br/>
	<div class="ui-state-error ui-corner-all" style="width: 50%">
		<img src="<s:url value='/resources/images/rechazar32.png'/>">
		<br/>
		<span class="error"><c:out value="${error}"/></span>	
	</div>
</c:if>


<form name="proceso" method="post">
	<input type="hidden" name="idProceso"/>
	<input type="hidden" name="estatus"/>
</form>

<div id="calendarIniDlg" align="center">
	<div id="calFechaInicio"></div>
	<a href="javascript:cerrarIni()">Cerrar</a>
</div>

<div id="calendarFinDlg" align="center">
	<div id="calFechaFin"></div>
	<a href="javascript:cerrarFin()">Cerrar</a>
</div>


