<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>


	function showDetalleOficio(numOficio) {
		document.forms["detalle"].oficio.value = numOficio;
		submitForm(document.forms["detalle"]);
	}
	
</script>

<style>

	#filtro li {
		float: left;
		margin: 0px 20px 0px 0px;
		list-style-type: none;
		height: 50px
	}

</style>
<div class="moduleTitle"><span>Detalle de Oficios</span></div>

	<c:if test="${not empty oficios}">
		<br/>
		<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 80%">
			<form name="oficios" method="post">
				<table  class="display" style="width: 100%;padding-top: 30px" align="center">
					<thead class="ui-widget-header">
						<tr>
							<th>Oficio</th>
							<th>Dependencia</th>
							<th>Fecha Generaci&oacute;n</th>
							<th>Estatus</th>
							<th>Solicitudes</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
		
					<c:forEach items="${oficios}" var="oficio" varStatus="st">
						<c:set var="cveDependencia">${oficio.dependencia}</c:set>
						<c:choose>
					    	<c:when test="${st.count % 2 == 1}">
					    		<c:set var="css" value="odd"/>
					    	</c:when>
					    	<c:otherwise>
					    		<c:set var="css" value="even"/>
					    	</c:otherwise>
					    </c:choose>
		
						<tr class="${css}">
							<td><c:out value="${oficio.numOficio}"/></td>
							<td><c:out value="${CATALOGO.DEPENDENCIAS[cveDependencia].descripcion}"/></td>
							<td><fmt:formatDate value="${oficio.fechaGeneracion}" pattern="dd-MM-yyyy HH:mm:ss"/></td>
							<td>
								<c:choose>
									<c:when test="${oficio.estatus eq 3}">
										Enviado
									</c:when>
									<c:when test="${oficio.estatus eq 2}">
										Aceptado
									</c:when>
								</c:choose>
							</td>
							<td><a href="javascript:showDetalleOficio(<c:out value='${oficio.numOficio}'/>)" style="color: blue;text-decoration: underline;">
								Ver Solicitudes</a>
							</td>
						</tr>
					</c:forEach>
				</table>
			</form>	
		</div>
</c:if>

		<div>
			<br/>
			<button type="button" class="btnEnviar" onclick="history.back()" style="width: 140px"><span><em>Regresar</em></span></button>
		</div>


<form name="detalle" action="<s:url value='/oficio/detalle'/>" method="post">
	<input name="oficio" type="hidden"/>
</form>