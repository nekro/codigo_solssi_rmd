<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>


<script>
	function showSolicitud(numFolio) {
		document.forms["solicitud"].numFolio.value = numFolio;
		document.forms["solicitud"].submit();
	}
	
</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

</style>

		<h4>Solicitudes</h4>
		<div align="right" style="width: 80%">
			<a href="javascript:history.back()">Regresar</a>
		</div>
		<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 80%">
			<table  class="display" style="width: 100%;padding-top: 30px" align="center">
				<thead class="ui-widget-header">
					<tr>
						<th>Num</th>
						<th><s:message code="table.header.solicitud.folio" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.cuenta" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.rfc" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.tipoTramite" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.estatus" htmlEscape="true"/></th>
						<!--<th><s:message code="table.header.solicitud.desplegar" htmlEscape="true"/></th>-->
					</tr>
				</thead>
				<tbody>
				</tbody>
	
				<c:forEach items="${solicitudes}" var="sol" varStatus="st">
					
					<c:choose>
				    	<c:when test="${st.count % 2 == 1}">
				    		<c:set var="css" value="odd"/>
				    	</c:when>
				    	<c:otherwise>
				    		<c:set var="css" value="even"/>
				    	</c:otherwise>
				    </c:choose>
					<c:set var="estatus">${sol.cveStatus}</c:set>
					<c:set var="tipoTramite">${sol.tipoTramite}</c:set>
					
					<tr class="${css}">
						<td><c:out value="${st.count}"/></td>
						<td><c:out value="${sol.userFolio}"/></td>
						<td><c:out value="${sol.cuenta}"/></td>
						<td><c:out value="${sol.rfc}"/></td>
						<td><c:out value="${CATALOGO.TIPO_TRAMITE[tipoTramite].descripcion}"/></td>
						<td><c:out value="${CATALOGO.ESTATUS_TRAMITE[estatus].valorCampo2}"/></td>
						<!-- 
						<td><a href="javascript:showSolicitud(${sol.numFolio})" style="color: blue;text-decoration: underline;">
							<img src="<s:url value='/resources/images/edit-find32.png'/>" alt="Desplegar" height="17px" width="17px"/></a></td>-->
					</tr>
				</c:forEach>
			</table>
		</div>
		
		<br/>
		<div>
			<button class="btnEnviar" onclick="history.back()"><span><em>Regresar</em></span></button>
		</div>
		<br/>
		
		<form name="solicitud" action="<s:url value='/consulta/view'/>" method="post">
			<input type="hidden" name="numFolio"/>
		</form>

