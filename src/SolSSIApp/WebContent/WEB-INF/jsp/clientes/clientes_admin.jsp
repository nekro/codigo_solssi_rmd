<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
	
dojo.ready(function(){
	
	<c:if test="${action eq 'update' or action eq 'alta'}">
		checkMotivoNoRetiro();
		
		<c:if test="${cliente.ultimoDescuento gt 0}">
			dojo.byId("ultimoDescuento").value = formatNumber(dojo.byId("ultimoDescuento").value,'');
		</c:if>	
		
		<c:if test="${cliente.maxPorcentaje eq 0}">
			dojo.byId("maxPorcentaje").value = '';
		</c:if>
		
	</c:if>
	
});
	
	function buscar() {
		if(document.forms["cliente"].cuentaBuscar.value == ''){
			alert("Capture el n�mero de Cuenta SSI");
			return;
		}
		document.forms["filtro"].action = "<s:url value='/admin/clientes/search'/>";
		document.forms["filtro"].cuenta.value = document.forms["cliente"].cuentaBuscar.value;
		submitForm(document.forms["filtro"]);
	}
	
	
	function update() {
		document.forms["cliente"].action = "<s:url value='/admin/clientes/update'/>";
		submitForm(document.forms["cliente"]);
	}
	
	function save() {
		document.forms["cliente"].action = "<s:url value='/admin/clientes/save'/>";
		submitForm(document.forms["cliente"]);
	}

	function checkMotivoNoRetiro(){
		var v = dojo.byId("codigoRetira1");
		if(!v.checked){
			dojo.byId("trMotivoNoRetiro").style.display = '';	
		}else{
			dojo.byId("trMotivoNoRetiro").style.display = 'none';
			dojo.byId("motivoNoRetiro").value = '';
			
		}
	}
	
	function formatNumber(num,prefix){
        num = Number(num).toFixed(2);
        if(num == 'NaN'){
        	return 0;
        }
        
        prefix = prefix || '';
	    num += '';
	    var splitStr = num.split('.');
	    var splitLeft = splitStr[0];
	    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
	   
	    return prefix + splitLeft + splitRight;
	}
</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

.control{
	width: 40% !important;
}

<s:hasBindErrors name="cliente">
	#divClientes td{
		vertical-align: top;
	}
	
	.error{
		position: relative;
		top: -3px; 
	}
</s:hasBindErrors>

}


</style>


<c:choose>
	<c:when test="${action eq 'update' or filter eq 'true'}">
		<c:set var="moduleTitle">Modificaci�n de Clientes</c:set>
	</c:when>
	<c:when test="${action eq 'alta'}">
		<c:set var="moduleTitle">Alta de Clientes</c:set>
	</c:when>
</c:choose>

<div class="moduleTitle"><span><c:out value="${moduleTitle}"/></span></div>
	<form:form modelAttribute="cliente"  method="post">
		
		
		<div id="divClientes" align="center" style="width: 75%">
		
		    <c:if test="${filter eq 'true'}">
		
				<table class="ui-widget-content ui-corner-all" style="width: 100%">
					<tr>
						<td colspan="2" align="left"><span class="titleSeccion" style="padding-left:20px">Filtro de B&uacute;squeda</span></td>
					</tr>
					<tr>
						<td class="tag">Cuenta SSI:</td>
						<td class="control">
								<input type="text" name="cuentaBuscar" maxlength="10" onkeypress="return enableNumero(event)" value="<c:out value='${cliente.cuenta}'/>"/>
								<button type="button" class="btnEnviar" onclick="buscar()"><span><em>Buscar</em></span></button>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">&nbsp;
							<c:if test="${not empty error}">
								<span class="error">No se encontr&oacute; ning&uacute;n registro. </span>
							</c:if>
						</td>
					</tr>
				</table>
				<br/>
			</c:if>
		  	
			<c:if test="${action eq 'update' or action eq 'alta'}">
				<table class="ui-widget-content ui-corner-all" style="width: 100%" cellspacing = "0">
					<tbody>
							<tr>
								<td colspan="2" align="left"><span class="titleSeccion" style="padding-left:20px">Datos del Cliente</span></td>
							</tr>
							
							<c:if test="${not empty save_successful}">
								<tr class="ui-state-focus"> 
									<td align="right">
										<img src="<s:url value='/resources/images/sucess.png'/>" height="29px" width="29px"/>
									</td>
									<td align="left">
										<strong>Registro guardado exitosamente</strong>
										<br/>
										<span class="error"><c:out value="${mensaje}"/></span>
									</td>
								</tr>
							</c:if>
							
							<tr>
								<td class="tag">Cuenta SSI:</td>
								<c:choose>
									<c:when test="${action eq 'update'}">
										<td class="control">
											<span style="color:black"><c:out value="${cliente.cuenta}"/></span>
											<form:hidden path="cuenta" readonly="true"/>
										</td>
									</c:when>
									<c:when test="${action eq 'alta'}">
										<td class="control">
											<form:input path="cuenta" maxlength="10"/>
											<s:hasBindErrors name="cliente"><br/><form:errors path="cuenta" cssClass="error"/></s:hasBindErrors>
										</td>
									</c:when>
								</c:choose>

							<tr>
								<td class="tag">RFC:</td>
								<c:choose>
									<c:when test="${action eq 'update'}">
										<td class="control">
											<span style="color:black"><c:out value="${cliente.rfc}"/>
											<c:if test="${not empty cliente.homoclave}">
												- <c:out value="${cliente.homoclave}"/>
											</c:if>
											</span>
											<form:hidden path="rfc"/>
											<form:hidden path="homoclave"/>
										</td>
									</c:when>
									<c:when test="${action eq 'alta'}">
										<td class="control">
											<form:input path="rfc" maxlength="10"/> - <form:input path="homoclave" maxlength="3" size="5"/>
											<s:hasBindErrors name="cliente"><br/><form:errors path="rfc" cssClass="error"/></s:hasBindErrors>
											<s:hasBindErrors name="cliente"><form:errors path="homoclave" cssClass="error"/></s:hasBindErrors>
										</td>
									</c:when>
								</c:choose>
								
							</tr>
							
							<tr>
								<td class="tag">Retenedor:</td>
								
								<c:choose>
									<c:when test="${action eq 'update'}">
										<td class="valueField">
											<c:out value="${cliente.retenedor}"/>
											<form:hidden path="retenedor"/>
										</td>
									</c:when>
									<c:when test="${action eq 'alta'}">
										<td class="valueField">
											<form:input path="retenedor" maxlength="10"/>
											<s:hasBindErrors name="cliente"><br/><form:errors path="retenedor" cssClass="error"/></s:hasBindErrors>
										</td>
									</c:when>
								</c:choose>
								
							</tr>
							
							<tr>
								<td class="tag">Unidad de Pago:</td>
								<c:choose>
									<c:when test="${action eq 'update'}">
										<td class="valueField">
											<c:out value="${cliente.unidadPago}"/>
											<form:hidden path="unidadPago"/>
										</td>
									</c:when>
									<c:when test="${action eq 'alta'}">
										<td class="valueField">
											<form:input path="unidadPago" maxlength="10"/>
											<s:hasBindErrors name="cliente"><br/><form:errors path="unidadPago" cssClass="error"/></s:hasBindErrors>
										</td>
									</c:when>
								</c:choose>
							</tr>
							
							<tr>
								<td class="tag">Reserva($):</td>
								<td class="control">
									<form:input path="saldo" onkeypress="return enableDecimal(event)" onchange="this.value = formatNumber(this.value,'')"/>
									<s:hasBindErrors name="cliente"><br/><form:errors path="saldo" cssClass="error"/></s:hasBindErrors> 	
								</td>
							</tr>
							
							<tr>
								<td class="tag">Monto de &uacute;ltimo descuento ($):</td>
								<td class="control">
									<form:input path="ultimoDescuento" onkeypress="return enableDecimal(event)" onchange="this.value = formatNumber(this.value,'')"/>
									<s:hasBindErrors name="cliente"><br/><form:errors path="ultimoDescuento" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
							
							<tr>
								<td class="tag">4 &uacute;ltimos digitos CLABE:</td>
								<td class="control">
									<form:input path="ultimosDigitosClabe" onkeypress="return enableNumero(event)" maxlength="4"/>
									<s:hasBindErrors name="cliente"><br/><form:errors path="ultimosDigitosClabe" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
							
							<tr>
								<td class="tag">Porcentaje M&aacute;ximo de Retiro:</td>
								<td class="control">
									<form:input path="maxPorcentaje" onkeypress="return enableNumero(event)"/>
									<s:hasBindErrors name="cliente"><br/><form:errors path="maxPorcentaje" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
							
							<tr>
								<td class="tag">Nivel de Puesto:</td>
								<td class="control">
									<form:input path="nivelPuesto" maxlength="10"/>
									<s:hasBindErrors name="cliente"><br/><form:errors path="nivelPuesto" cssClass="error"/></s:hasBindErrors>
								</td>
								
							</tr>
							
							<tr>
								<td class="tag">Pr&eacute;stamo:</td>
								<td class="control">
									<form:radiobutton path="prestamo" label="Si" value="1"/>
									<form:radiobutton path="prestamo" label="No" value="0"/>
								</td>
							</tr>
							
							<tr>
								<td class="tag">Pensi&oacute;n:</td>
								<td class="control">
									<form:radiobutton path="pension" label="Si" value="1"/>
									<form:radiobutton path="pension" label="No" value="0"/>
								</td>
							</tr>
							
							<tr>
								<td class="tag">Permitir Retiro:</td>
								<td class="control">
									<form:radiobutton path="codigoRetira" label="Si" value="1" onclick="checkMotivoNoRetiro()"/>
									<form:radiobutton path="codigoRetira" label="No" value="0" onclick="checkMotivoNoRetiro()"/>
								</td>
							</tr>
							
							<tr id="trMotivoNoRetiro"> 
								<td class="tag">Motivo No Retiro:</td>
								<td align="left">
									<form:textarea path="motivoNoRetiro" cols="40" rows="3" cssStyle="border:1px solid gray"/>
									<s:hasBindErrors name="cliente"><br/><form:errors path="motivoNoRetiro" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
							
							<tr> 
								<c:choose>
									<c:when test="${action eq 'update'}">
										<td align="center" colspan="2">
											<button type="button" class="btnEnviar" onclick="update()"><span><em>Guardar</em></span></button>
											<br/>
										</td>
									</c:when>
									<c:when test="${action eq 'alta'}">
										<td align="center" colspan="2">
											<button type="button" class="btnEnviar" onclick="save()"><span><em>Guardar</em></span></button>
											<br/>
										</td>
									</c:when>
								</c:choose>
							</tr>
					</tbody>	
				</table>
				<br/>
			</c:if>
		</div>
		<form:hidden path="fueraFecha"/>
		<form:hidden path="poderJudicial"/>
	</form:form>
	
	<form name="filtro" method="post">
		<input name="cuenta" type="hidden"/>
	</form>
