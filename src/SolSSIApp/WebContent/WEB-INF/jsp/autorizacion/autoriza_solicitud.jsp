
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>



<s:url value="/autorizacion" var="urlConsulta"/>
<s:url value="/autorizacion/execute/autorizacion" var="urlAutoriza"/>
<s:url value="/autorizacion/execute/rechazo" var="urlRechazo"/>

<style>
	.buttonDlg{
		background-color: #f5f5f5;
		color: black;
		border: 1px solid gray;
	}
</style>
		
<script>
		 
		var checkListDialog;
		var confirmDialog;
		var confirmRechazoDialog;
		var clabeDialog;
		var checkItem;
		dojo.require("dijit.Dialog");
		
		dojo.ready(function(){
			  
			checkListDialog = new dijit.Dialog({ title:"<s:message code='dialog.checklist.title' htmlEscape='true'/>", style: "width: 875px;text-align:center" }, "divCheckList");
			dojo.style(dijit.byId("divCheckList").closeButtonNode,"display","none");
  			checkListDialog.startup();
  			
  			
  			confirmDialog = new dijit.Dialog({ title:"", style: "width: 700px;text-align:center" }, "divConfirm");
  			dojo.style(dijit.byId("divConfirm").closeButtonNode,"display","none");
  			confirmDialog.startup();
  			
  			clabeDialog = new dijit.Dialog({ title:"Capture la CLABE", style: "width: 500px;text-align:center" }, "divCapturaClabe");
  			dojo.style(dijit.byId("divCapturaClabe").closeButtonNode,"display","none");
  			clabeDialog.startup();
  			
  			confirmRechazoDialog = new dijit.Dialog({ title:"Mensaje", style: "width: 500px;text-align:center" }, "divRechazar");
  			dojo.style(dijit.byId("divRechazar").closeButtonNode,"display","none");
  			confirmRechazoDialog.startup();
  			
			
			<c:if test="${solicitud.contribuyenteEUA}">
	 			js_solicitud.showNSS(true);
	 	    </c:if>
	 	   
	 		<c:if test="${solicitud.idPaisNac gt 0}">
	 			checkPaisNacimiento(<c:out value="${solicitud.idPaisNac}"/>);
	 		</c:if>
	 		
	 		<c:if test="${solicitud.idPais gt 0}">
	 			checkPaisDomicilio(<c:out value="${solicitud.idPais}"/>);
			</c:if>
		});
		
		function showConfirmDialog(){
			confirmDialog.show();
		}
		
		function confirmRechazar(){
			document.forms["solicitud"].action = "${urlRechazo}";
			document.forms["solicitud"].target = "_top";
			submitForm(document.forms["solicitud"]);
		}
		
		function cancelRechazar(){
			confirmDialog.hide();
		}
		 
		 function dependientes() {
			 openWindowCenterScroll("","dep",1000,600);
			 document.forms["dependientes"].submit();
		 };
		 
		 
		 function checkPaisNacimiento(value){
			 if(value == 1){
				 document.getElementById("divEstadoNacExtranjero").style.display = "none";
				 document.getElementById("divEstadoNacMexico").style.display = "";
			 }else{
				 document.getElementById("divEstadoNacMexico").style.display = "none";
				 document.getElementById("divEstadoNacExtranjero").style.display = "";
			 }
		 }
		 
		 
		 function checkPaisDomicilio(value){
			 if(value == 1){
				 document.getElementById("divEstadoExtranjero").style.display = "none";
				 document.getElementById("divEstadoMexico").style.display = "";
				 document.getElementById("divMunicipioExtranjero").style.display = "none";
				 document.getElementById("divMunicipioMexico").style.display = "";
			 }else{
				 document.getElementById("divEstadoMexico").style.display = "none";
				 document.getElementById("divEstadoExtranjero").style.display = "";
				 document.getElementById("divMunicipioMexico").style.display = "none";
				 document.getElementById("divMunicipioExtranjero").style.display = "";
			 }
		 }
		 
		 function regresarConsulta(){
			 history.back();
		 }
		 
		 function checkListDialogOpen(){
	  	 	 for(var i = 0; i < document.forms["checkListForm"].clItem.length; i++){
		 	 	document.forms["checkListForm"].clItem[i].checked = false;
		 	 }
		 	 try{
		 	 		dojo.byId("checkListClabeError").style.display = 'none';
		 	 	}catch(e){
		 	 };
		 	 checkListDialog.show();
		 }
		 
		 function checkListDialogClose(){
		 	 checkListDialog.hide();
		 }
		 
		 function autorizar(){
		 	 
		 	 for(var i = 0; i < document.forms["checkListForm"].clItem.length; i++){
		 	 	if(!document.forms["checkListForm"].clItem[i].checked){
		 	 		showConfirmDialog();
		 	 		return;		
		 	 	}
		 	 }
			 
			 document.forms["solicitud"].action = "${urlAutoriza}";
			 document.forms["solicitud"].target = "_top";
			 submitForm(document.forms["solicitud"]);
		 }

		 function capturarClabe(){
		 	clabeDialog.show();
		 }
		 
		 function cerrarDlgClabe(){
		 	clabeDialog.hide();
		 }
		 
		 function validaCLABE_KEYUP(){
		    if(dojo.byId("inputCheckClabe").value.length == 18){
			 	if(dojo.byId("inputCheckClabe").value != '<c:out value="${solicitud.clabe}"/>'){
			 		dojo.byId("checkListClabeError").style.display = '';
			 	}else{
			 		dojo.byId("checkListClabeError").style.display = 'none';
			 	}
		 	}
		 }
		 
		 function validaCLABE(){
			 	if(dojo.byId("inputCheckClabe").value != '<c:out value="${solicitud.clabe}"/>'){
			 		dojo.byId("checkListClabeError").style.display = '';
			 		return false;
			 	}else{
			 		dojo.byId("checkListClabeError").style.display = 'none';
			 	}
			 	return true;
		 }
		 
		 function checkCLABE(check){
		 	if(check.checked){
		 		if(!validaCLABE()){
		 			check.checked = false;
		 		}
		 	}else{
		 		dojo.byId("checkListClabeError").style.display = 'none';
		 		document.forms["solicitud"].checkClabe.value = dojo.byId("inputCheckClabe").value;
		 	}
		 }
		 
</script>

<div class="moduleTitle"><span><s:message code="module.title.solicitud.autorizacion" htmlEscape="true"/></span></div>

<jsp:include page="../consulta/datos_solicitud.jsp"/>	

<br/><br/>
<div>
	<button type="button" class="btnEnviar" onclick="regresarConsulta()"><span><em><s:message code="button.cancelar" htmlEscape="true"/></em></span></button>
	<c:if test="${solicitud.cveStatus == 1}">
		<button type="button" class="btnEnviar" onclick="checkListDialogOpen()"><span><em><s:message code="button.autorizar" htmlEscape="true"/></em></span></button>
	</c:if>
</div>
<br/><br/>		

<form name="dependientes" action="<s:url value='/consulta/dependientes/'/>" target="dep" method="post"/>
	<input type="hidden" name="folio" value="<c:out value='${solicitud.numFolio}'/>">
</form>

<jsp:include page="checklist.jsp"/>
