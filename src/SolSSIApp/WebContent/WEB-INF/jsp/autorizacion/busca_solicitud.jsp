<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
	function buscar() {
		document.forms["filtro"].action = "<s:url value='/autorizacion/consulta'/>";
		document.forms["filtro"].submit();
		submitForm(document.forms["filtro"]);
	}

	function showSolicitud(numFolio) {
		document.forms["solicitud"].numFolio.value = numFolio;
		submitForm(document.forms["solicitud"]);
	}
</script>

<style>

	#filtro li {
		float: left;
		margin: 0px 20px 0px 0px;
		list-style-type: none;
		height: 50px
	}

</style>
<div class="moduleTitle"><span><s:message code="module.title.solicitud.autorizacion" htmlEscape="true"/></span></div>

	<div id="filtro" style="width: 90%" align="center">
		<form:form modelAttribute="consultaFilter" name="filtro">
			<table class="ui-widget-content ui-corner-all" >
				<tr >
					<td align="center"><span class="titleSeccion">Filtro de B&uacute;squeda</span>
					</td>
				</tr>
				<tr>
					<td align="left">
						<ul>
							<li><span>Folio Solicitud:</span> <form:input path="userFolio"
									maxlength="6" style="width:90px" htmlEscape="true"/></li>
							<li><span>RFC:</span> <form:input path="rfc" maxlength="10"
									style="width:90px" /> - <form:input path="homoclave"
									maxlength="3" style="width:30px" htmlEscape="true"/></li>
							<li><span>Cuenta:</span> <form:input path="cuenta"
									onkeypress="return enableNumero(event)" maxlength="10"
									style="width:105px" htmlEscape="true"/></li>
							<li>
							<button type="button" class="btnEnviar" onclick="buscar()"><span><em><s:message code="button.buscar" htmlEscape="true"/></em></span></button>
							</li>		
	
						</ul>
					</td>
				</tr>
			</table>
			<s:hasBindErrors name="consultaFilter">
				<br/><br/>
				<div class="ui-state-error ui-corner-all" style="width: 80%;padding-top: 10px;padding-bottom: 10px">
					<form:errors path="*"/>
				</div>
			</s:hasBindErrors>
		</form:form>
	</div>


	<c:if test="${not empty solicitudes}">
		<br/>
		<h4>Resultados</h4>
		<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 80%">
			<table  class="display" style="width: 100%;padding-top: 30px" align="center">
				<thead class="ui-widget-header">
					<tr>
						<th><s:message code="table.header.solicitud.folio" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.cuenta" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.rfc" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.tipoTramite" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.estatus" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.fechaSolicitud" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.revision" htmlEscape="true"/></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
	
				<c:forEach items="${solicitudes}" var="sol" varStatus="st">

					<c:set var="estatus">${sol.cveStatus}</c:set>
					<c:set var="tipoTramite">${sol.tipoTramite}</c:set>
					
					<c:choose>
				    	<c:when test="${st.count % 2 == 1}">
				    		<c:set var="css" value="odd"/>
				    	</c:when>
				    	<c:otherwise>
				    		<c:set var="css" value="even"/>
				    	</c:otherwise>
				    </c:choose>
	
					<tr class="${css}">
						<td><c:out value="${sol.userFolio}"/></td>
						<td><c:out value="${sol.cuenta}"/></td>
						<td><c:out value="${sol.rfc}"/></td>
						<td><c:out value="${CATALOGO.TIPO_TRAMITE[tipoTramite].descripcion}"/></td>
						<td><c:out value="${CATALOGO.ESTATUS_TRAMITE[estatus].valorCampo2}"/></td>
						<td><fmt:formatDate  value="${sol.fechaCaptura}" pattern="dd-MM-yyyy HH:mm:ss"/></td>
						<td><a href="javascript:showSolicitud(${sol.numFolio})" style="color: blue;text-decoration: underline;">
								<s:message code="table.link.solicitud.revision" htmlEscape="true"/>
							</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<form name="solicitud" action="<s:url value='/autorizacion/desplegar'/>" method="post">
			<input type="hidden" name="numFolio"/>
		</form>
</c:if>
