<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<div id="divCheckList" style="min-height: 500px;">
	<div align="center" style="width:100%">
		<div align="left" style="width: 100%">
			<form name="checkListForm">
				<table cellspacing="7" cellpadding="7">
					
					<tr>
						<td colspan="2"></td>
					</tr>
					
					<c:if test="${solicitud.prestamo}">
						<tr>
							<td colspan="2" align="justify">
								<div class="ui-state-active ui-corner-all" style="padding: 5px 5px 5px 5px">
									<div align="center"><span style="color:blue"><strong>Recordatorio:</strong></span></div>
									<span style="color:black">
										<s:message code="auth.prestamo.recordatorio" htmlEscape="true"/>
									</span>
								</div>
							</td>
						</tr>
					</c:if>
					
					<tr>
						<c:set var="st" value="1"/>
						<td align="left">
							<input type="checkbox" name="clItem" id="clItem${st}">
						</td>
						<td align="left">
							<label for="clItem${st}"><c:out value="${CATALOGO.CHECKLIST['1'].descripcionOrigen}"/></label>
						</td>
					</tr>
					<tr>
						<c:set var="st" value="${st+1}"/>
						<td align="left">
							<input type="checkbox" name="clItem" id="clItem${st}">
						</td>
						<td align="left">
							<label for="clItem${st}"><c:out value="${CATALOGO.CHECKLIST['2'].descripcionOrigen}"/></label>
						</td>
					</tr>
					
					<c:if test="${(solicitud.bndUltimosDigitosClabe) and (solicitud.modoPago eq 1)}">
						<tr>
							<c:set var="st" value="${st+1}"/>
							<td align="left">
								<input type="checkbox" name="clItem" id="clItem${st}">
							</td>
							<td align="left">
								<label for="clItem${st}"><c:out value="${CATALOGO.CHECKLIST['3'].descripcionOrigen}"/></label>
							</td>
						</tr>
						<tr>
							<c:set var="st" value="${st+1}"/>
							<td align="left">
								<input type="checkbox" name="clItem" id="clItem${st}">
							</td>
							<td align="left">
								<label for="clItem${st}"><c:out value="${CATALOGO.CHECKLIST['4'].descripcionOrigen}"/></label>
							</td>
						</tr>
							
					</c:if>
					<c:if test="${(not solicitud.bndUltimosDigitosClabe) and (solicitud.modoPago eq 1)}">
						<tr>
							<c:set var="st" value="${st+1}"/>
							<td align="left">
								<input type="checkbox" name="clItem" id="clItem${st}">
							</td>
							<td align="left">
								<label for="clItem${st}"><c:out value="${CATALOGO.CHECKLIST['7'].descripcionOrigen}"/></label>
							</td>
						</tr>
						<tr>
							<c:set var="st" value="${st+1}"/>
							<td align="left">
								<input type="checkbox" name="clItem" id="clItem${st}" onclick="checkCLABE(this)">
							</td>
							<td align="left">
								<label for="clItem${st}"><c:out value="${CATALOGO.CHECKLIST['8'].descripcion}"/></label>
								<input id="inputCheckClabe" type="text" onkeypress="return enableNumero(event);" maxlength="18" size="18" onpaste="return false" onkeyup="validaCLABE_KEYUP()">
								<span class="error" id="checkListClabeError" style="display: none"><s:message code="check.clabe.NotMatch" htmlEscape="true"/></span>
							</td>
						</tr>
							
					</c:if>
					
					<tr>
							<c:set var="st" value="${st+1}"/>
							<td align="left">
								<input type="checkbox" name="clItem" id="clItem${st}">
							</td>
							<td align="left">
								<label for="clItem${st}"><c:out value="${CATALOGO.CHECKLIST['5'].descripcionOrigen}"/></label>
							</td>
					</tr>
					<tr>
							<c:set var="st" value="${st+1}"/>
							<td align="left">
								<input type="checkbox" name="clItem" id="clItem${st}">
							</td>
							<td align="left">
								<label for="clItem${st}"><c:out value="${CATALOGO.CHECKLIST['6'].descripcionOrigen}"/></label>
							</td>
					</tr>
					
				</table>
			</form>	
			<div align="center">	
				<button type="button" class="btnEnviar" onclick="checkListDialogClose()"><span><em>Cancelar</em></span></button>
				<button type="button" class="btnEnviar" onclick="autorizar()"><span><em>Enviar</em></span></button>
			</div>
		</div>
	</div>
</div>

<div id="divConfirm">
	<div align="center" style="width:100%">
	    <table>
	    	<tr>
	    		<td valign="top">
	    			<img src="<s:url value='/resources/images/info.gif'/>"/>
	    		</td>
	    		<td>
	    			<span id="textConfirm" style="color:black;font-size:14px">
						<s:message code="documentacion.auth.incompleta" htmlEscape="true"/>
					</span>
	    		</td>
	    		
	    	</tr>
	    </table>
		
		<div>
			<br/>
			<input type="button" value="Cancelar" onclick="cancelRechazar()" class="buttonDlg" style="width: 60px"/>
			<input type="button" value="Aceptar" onclick="confirmRechazar()" class="buttonDlg" style="width: 60px"/>
			
		</div>
	</div>
</div>


<div id="divRechazar">
	<div align="center" style="width:100%">
		<div style="position: absolute;top: 5px;left: 15px">
			<img src="<s:url value='/resources/images/info.gif'/>"/>
		</div>
		<div>
			<span id="textConfirm" style="color:black;font-size:14px">
				<s:message code="confirm.rechazo.solicitud" htmlEscape="true"/>
			</span>
		</div>
		<div>
			<br/>
			<input type="button" value="No" onclick="cancelRechazar()" class="buttonDlg" style="width: 60px"/>
			<input type="button" value="Si" onclick="confirmRechazar()" class="buttonDlg" style="width: 60px"/>
		</div>
	</div>
</div>


<div id="divCapturaClabe">
	<div align="center" style="width:100%">
		<div align="center">
			<label>Clabe:</label><input name="clabe" type="text" maxlength="18" width="150px" id="inputCheckClabe"/>
		</div>
		<div style="min-height: 30px" >
			<span class="error" id="checkListClabeError" style="display: none"><s:message code="check.clabe.NotMatch" htmlEscape="true"/></span>
		</div>
		<input type="button" value="Cancelar" onclick="cerrarDlgClabe()" class="buttonDlg"/>
		<input type="button" value="Enviar" onclick="setClabe()" class="buttonDlg"/>
	</div>
</div>


