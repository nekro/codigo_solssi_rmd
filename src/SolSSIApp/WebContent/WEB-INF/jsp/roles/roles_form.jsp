<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
	
	function save() {
		document.forms["rol"].action = "<s:url value='/roles/save'/>";
		setPermisos();
		submitForm(document.forms["rol"]);
	}
	
	function setPermisos(){
		var checks = document.forms["rol"].checkPermisos;
		var permisos = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
		for(var i = 0; i < checks.length ; i++){
			if(checks[i].checked){
			     permisos[Number(checks[i].value)-1] = 1;
			}
		}
		document.forms["rol"].permisos.value = permisos.join("");
	}
	
	function update() {
		document.forms["rol"].action = "<s:url value='/roles/update'/>";
		setPermisos();
		submitForm(document.forms["rol"]);
		//document.forms["usuario"].submit();
	}
	
	function cancelar() {
		document.forms["rol"].action = "<s:url value='/roles'/>";
		submitForm(document.forms["rol"]);
	}
	
	require(["dojo/ready"], function(ready){
		  ready(function(){
			  dojo.byId("submenuUsuarios").setAttribute("class", "activo");
		  });
		});
	
	

</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

#formulario td{
	padding-top: 3px !important;
	padding-bottom: 3px !important;
}

#permisosTable td{
	padding-top: 1px !important;
	padding-bottom: 1px !important;
}

</style>

	<c:choose>
		<c:when test="${command eq 'update'}">
			<div class="moduleTitle"><span>Actualizaci&oacute;n de Rol</span></div>					
		</c:when>
		<c:otherwise>
			<div class="moduleTitle"><span>Alta de Rol</span></div>
		</c:otherwise>
	</c:choose>
	
	<div id="formulario" style="width: 75%" align="center" class="ui-widget-content ui-corner-all">
		<form:form modelAttribute="rol">
		<form:errors path="*"/>
			<form:hidden path="permisos"/>
		
			<table>
				<tr>
					<td colspan="2" align="left">
						<span class="subseccion">Capture la informaci&oacute;n</span>
					</td>
				</tr>
				<tr>	
					<td class="tag">
						<form:label path="id">Id Rol:</form:label>
					</td>
					<c:choose>
							<c:when test="${command eq 'update'}">
								<td class="valueField">
									<c:out value="${rol.id}"/>
									<form:hidden path="id"/>
								</td>
							</c:when>
							<c:otherwise>
								<td class="control">
									<form:input path="id" maxlength="10"/>
									<s:hasBindErrors name="rol"><br/><form:errors path="id" cssClass="error"/></s:hasBindErrors>
								</td>
							</c:otherwise>
						</c:choose>
					</tr>
				<tr>	
					<td class="tag">
						<form:label path="descripcion">Nombre:</form:label>
					</td>
					<td class="control">
						<form:input path="descripcion" maxlength="64" size="50"/>
						<s:hasBindErrors name="rol"><br/><form:errors path="descripcion" cssClass="error"/></s:hasBindErrors>
					</td>
				</tr>
				<tr>	
					<td class="tag">
						<form:label path="activo">Estatus:</form:label>
					</td>
					<td class="control">
						<form:radiobutton path="activo" value="true" label="Activo"/>
						<form:radiobutton path="activo" value="false" label="Inactivo"/>
					</td>
				</tr>
				<tr>	
					<td colspan="2" align="center">
						<table id="permisosTable" class="ui-widget-content  ui-corner-all" width="400px">
						    
						    <tr>
						    	<td align="left" class="ui-state-focus"><h5>Rescates</h5></td>
						    </tr>
						    
							<tr>
								<td align="left"><form:checkbox path="checkPermisos" value="1" label=" Consulta Solicitud" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="2" label=" Revisión Solicitud" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="3" label=" Genera Oficios" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="4" label=" Autoriza Oficios" htmlEscape="true"/></td>
							</tr>
							
							
							<tr>
						    	<td align="left" class="ui-state-focus"><h5>Esquemas</h5></td>
						    </tr>

							
							<tr>
								<td align="left"><form:checkbox path="checkPermisos" value="13" label=" Consulta Solicitud" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="14" label=" Revisión Solicitud" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="15" label=" Genera Oficios" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="16" label=" Autoriza Oficios" htmlEscape="true"/></td>
							</tr>
							
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="17" label=" Administración de Esquemas" htmlEscape="true"/></td>
							</tr>
							
							<tr>
						    	<td align="left" class="ui-state-focus"><h5>M&oacute;dulos Administrativos</h5></td>
						    </tr>
						    
						    <tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="5" label=" RFC Fuera de Fecha" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="6" label=" Admin Catálogos" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="7" label=" Admin Usuarios" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="8" label=" Reportes" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="9" label=" Clientes" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="10" label=" Históricos" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="11" label=" Variables" htmlEscape="true"/></td>
							</tr>
							<tr>	
								<td align="left"><form:checkbox path="checkPermisos" value="12" label=" Cifras Control" htmlEscape="true"/></td>
							</tr>
							
							
						</table>					
					</td>
				</tr>
			</table>
			
			<input type="hidden" name="command" value="${command}"/>
		</form:form>
		
		<div>
			<br/>
			<button type="button" class="btnEnviar" onclick="cancelar()"><span><em>Cancelar</em></span></button>
			<c:choose>
				<c:when test="${command eq 'update'}">
					<button type="button" class="btnEnviar" onclick="update()"><span><em>Guardar</em></span></button>					
				</c:when>
				<c:otherwise>
					<button type="button" class="btnEnviar" onclick="save()"><span><em>Guardar</em></span></button>
				</c:otherwise>
			</c:choose>
			<br/><br/>
		</div>		
		
	</div>
	
	

