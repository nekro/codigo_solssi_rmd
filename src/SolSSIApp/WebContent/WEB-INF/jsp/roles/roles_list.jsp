<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<script>
	
	require(["dojo/ready"], function(ready){
		  ready(function(){
			  dojo.byId("submenuUsuarios").setAttribute("class", "activo");
		  });
	});
	
	function nuevoRol() {
		submitForm(document.forms["nuevo"]);
		//document.forms["filtro"].submit();
	}
	
	function editarRol(clave) {
		document.forms["edit"].clave.value = clave;
		submitForm(document.forms["edit"]);
		//document.forms["edit"].submit();
	}
	
</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

#tablaRoles td{
	text-align: center;
}

#nombresRol span{
	padding-left: 10px
}

</style>

	<div class="moduleTitle"><span>Administraci&oacute;n de Roles</span></div>
		
		<br/>
		<h4>Listado de Roles</h4>
		
		<br/>
		<div id="tablaRoles" align="center" class="ui-widget-content ui-corner-all" style="width: 80%">
			<table  class="display" style="width: 100%;padding-top: 30px" align="center">
				<thead class="ui-widget-header">
					<tr>
						<td>ID</td>
						<td>NOMBRE</td>
						<td>EDITAR</td>
					</tr>
				</thead>
				<tbody>
				</tbody>
	
				<c:forEach items="${roles}" var="rol" varStatus="st">
					
					<c:choose>
				    	<c:when test="${st.count % 2 == 1}">
				    		<c:set var="css" value="odd"/>
				    	</c:when>
				    	<c:otherwise>
				    		<c:set var="css" value="even"/>
				    	</c:otherwise>
				    </c:choose>
					
					<tr class="${css}">
						<td>${rol.id}</td>
						<td><a href="javascript:editarRol(${rol.id})">${rol.descripcion}</td>
						<td><a href='javascript:editarRol(${rol.id})'><img src="<s:url value="/resources/images/edit-file-icon32.png" />" alt="Modificar" height="15px" width="15px"/></a></td>
					</tr>
					
					
				</c:forEach>
			</table>
		</div>
		
		<div>
			<br/>
			<button type="button" class="btnEnviar" onclick="nuevoRol()"><span><em>Nuevo</em></span></button>
		</div>


<form name="edit" action="<s:url value='/roles/edit'/>" method="post">
	<input type="hidden" name="clave"/>
</form>

<form name="nuevo" action="<s:url value='/roles/alta'/>" method="post">
</form>