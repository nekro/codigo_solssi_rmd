<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<c:if test="${empty opcionMenu}">
   <c:set var="opcionMenu" value="1"/>
</c:if>

<c:if test="${empty opcionSubMenu}">
   <c:set var="opcionSubMenu" value="1"/>
</c:if>
<script>

	dojo.ready(function(ready){
		  menuSelectOption(Number(<c:out value="${opcionMenu}"/>));
		  
		  <c:if test="${empty ACCESO_BY_OPERACION}">
		  		document.forms["menuDirectionForm"].action = '<s:url value="/login/expired"/>';
          		document.forms["menuDirectionForm"].submit();
		  </c:if>
	});
	
	
	 function menuSelectOption(tabNumber){
         for(var i=1; i <= 15; i++){
             tab="menuTheme" + i;
             var element = dojo.byId(tab); 
             
             if(element == null){
             	break;
             }
             
             if(tabNumber == i){
                 element.className="";
                 element.parentNode.className="On";
                 element.onmouseover = function(){return false};
                 element.onmouseout = function(){return false};
                 disableSeparator(element, i);
                
                var submenu = document.getElementById("subMenuList"+tabNumber);
				if(submenu != null){
					submenu.className="activo";
					var current = document.getElementById("subMenu"+tabNumber+"_<c:out value='${opcionSubMenu}'/>");
					 if(current != null){
					 	current.className = "current";
					 }
				}
             }
         }
    }
	
	function optionMenuOpen(opc, url){
        dojo.xhrPost({
				   url: "<s:url value='/login/opcmenu'/>",
				   content: {"opcion": opc},
				   handleAs: "json",
				   load: function (result) {
					   if(result == '1'){
					   		//window.onbeforeunload=function(){null}
					   		document.forms["menuDirectionForm"].action = url;
			                document.forms["menuDirectionForm"].submit();
			            }else{
			            	document.forms["menuDirectionForm"].action = '<s:url value="/logout"/>';
			            	
			            	<c:if test="${ACCESO_BY_OPERACION eq 'false'}">
			            		document.forms["menuDirectionForm"].action = '<s:url value="/login/expired"/>';
			            	</c:if>
			            	
			                document.forms["menuDirectionForm"].submit();
			            }
				  }
			});
    }
    
    
    function optionSubMenuOpen(opc,url){
    
    	dojo.xhrPost({
				   url: "<s:url value='/login/opcsubmenu'/>",
				   content: {"opcion": opc},
				   handleAs: "json",
				   load: function (result) {
					   if(result == '1'){
					   		document.forms["menuDirectionForm"].action = url;
			                document.forms["menuDirectionForm"].submit();
			            }else{
			            	document.forms["menuDirectionForm"].action = '<s:url value="/logout"/>';
			            	
			            	<c:if test="${ACCESO_BY_OPERACION eq 'false'}">
			            		document.forms["menuDirectionForm"].action = '<s:url value="/login/expired"/>';
			            	</c:if>
			            	
			                document.forms["menuDirectionForm"].submit();
			            }
	
				  }
			});
    }
    
</script>

<form name="menuDirectionForm" method="post"></form>

<ul class="menu interior">

	<c:set var="numeroOpcion" value="1"/>

	<c:if test="${ACCESO_BY_OPERACION eq 'false'}">
		
		<li>
			<a  class="menuSeparator"
				id="menuTheme${numeroOpcion}"
			    onMouseOver="disableSeparator(this,${numeroOpcion})"
				onMouseOut="enableSeparator(this,${numeroOpcion},false)"
				href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/clientes"/>')">
				<span style="width:100px">Bienvenida</span>
			</a>
		</li>
		
		<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
		
		<li>
			<a  class="menuSeparator"
				id="menuTheme${numeroOpcion}"
			    onMouseOver="disableSeparator(this,${numeroOpcion})"
				onMouseOut="enableSeparator(this,${numeroOpcion},false)"
				href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/clientes/captura"/>')">
				<span style="width:155px">Captura de Solicitud</span>
			</a>
		</li>
		
		<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
		<li>
				<a  
					class="menuSeparator"
					id="menuTheme${numeroOpcion}"
				    onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/clientes/saldo"/>')">
					<span style="width:155px">Consulta de Saldo</span>
				</a>
		</li>
		
		<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
		<li>
				<a  
					class="menuSeparator"
					id="menuTheme${numeroOpcion}"
				    onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/esquemas/inicio/home"/>')">
					<span style="width:155px">Selecci&oacute;n de Esquema</span>
				</a>
				
				<ul id="subMenuList${numeroOpcion}">
						<c:set var="numeroSubOpcion" value="0"/>
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/esquemas/inicio/home"/>')">Inicio</a>
						</li>
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/esquemas/inicio"/>')">Documento de Selecci&oacute;n</a>
						</li>
				</ul>
		</li>
		
		<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
		<li>
			<a class="menuSeparatorFinal" id="menuTheme${numeroOpcion}" href="#" title=""><span>&nbsp;</span></a>
		</li>
	</c:if>


	<c:if test="${ACCESO_BY_OPERACION eq 'true'}">

		
		<li>
			<a  class="menuSeparator"
				id="menuTheme${numeroOpcion}"
			    onMouseOver="disableSeparator(this,${numeroOpcion})"
				onMouseOut="enableSeparator(this,${numeroOpcion},false)"
				href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/operacion"/>')">
				<span style="width: 50px"><s:message code="menu.inicio" htmlEscape="true"/></span>
			</a>
		</li>
		
		<c:choose>
		<c:when test="${tipoMenu eq 'RESCATES'}">

		<sec:authorize access="hasAnyRole('SOLICITUD_CONSULTAS', 'HISTORICOS')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			
			<li>
			    <a  class="menuSeparator" 
			        id="menuTheme${numeroOpcion}"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/consulta"/>')">
					<span style="width: 80px"><s:message code="menu.consulta" htmlEscape="true"/></span>
				</a>
				
				<ul id="subMenuList${numeroOpcion}">
					<c:set var="numeroSubOpcion" value="0"/>
					<sec:authorize access="hasAnyRole('SOLICITUD_CONSULTAS','HISTORICOS')">
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/consulta"/>')">Solicitud</a>
						</li>
					</sec:authorize>
					<sec:authorize access="hasRole('HISTORICOS')">
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/logs"/>')">Bit&aacute;cora</a>
						</li>
						
					</sec:authorize>
				</ul>
				
			</li>
		</sec:authorize>

		<sec:authorize access="hasRole('SOLICITUD_AUTORIZACIONES')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
			    <a  class="menuSeparator"  
			    	id="menuTheme${numeroOpcion}"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/autorizacion"/>')">
					<span style="width: 110px;padding-top:3px" ><s:message code="menu.autorizacion" htmlEscape="true"/></span>
				</a>
			</li>
		</sec:authorize>
		
		<sec:authorize access="hasAnyRole('GENERA_OFICIOS','AUTORIZA_OFICIOS')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
				
				<sec:authorize access="hasRole('AUTORIZA_OFICIOS')">
				    <s:url value="/oficio/consulta" var="urlOficios"/>
				</sec:authorize>
				<sec:authorize access="hasRole('GENERA_OFICIOS')">
					<s:url value="/oficio/generar" var="urlOficios"/>
				</sec:authorize>
			    
			    <a  id="menuTheme${numeroOpcion}"
				    	class="menuSeparator"
				    	onMouseOver="disableSeparator(this,${numeroOpcion})"
						onMouseOut="enableSeparator(this,${numeroOpcion},false)"
						href="javascript:optionMenuOpen(${numeroOpcion},'${urlOficios}')">
						<span style="width: 65px"><s:message code="menu.oficios" htmlEscape="true"/></span>
				</a>
			    
				<ul id="subMenuList${numeroOpcion}">
					<c:set var="numeroSubOpcion" value="0"/>
					<sec:authorize access="hasRole('GENERA_OFICIOS')">
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/oficio/generar"/>')"><s:message code="menu.oficios.generar" htmlEscape="true"/></a>
						</li>
					</sec:authorize>
					<sec:authorize access="hasRole('AUTORIZA_OFICIOS')">
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/oficio/consulta"/>')"><s:message code="menu.oficios.aceptar" htmlEscape="true"/></a>
						</li>
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						 
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/oficio/enviar"/>')"><s:message code="menu.oficios.enviar" htmlEscape="true"/></a>
						</li>
						
					</sec:authorize>
				</ul>
			</li>
		</sec:authorize>
		
		<sec:authorize access="hasRole('REPORTE_TOTALES')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
			    <a  id="menuTheme${numeroOpcion}"
			    	class="menuSeparator"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/consulta/totales"/>')">
					<span style="width: 75px">Reportes</span>
				</a>
				<ul id="subMenuList${numeroOpcion}">
					<li id="subMenu${numeroOpcion}_1">
						<a href="javascript:optionSubMenuOpen(1,'<s:url value="/consulta/totales"/>')">Totales Solicitudes</a>
					</li>
					<li id="subMenu${numeroOpcion}_2">
						<a href="javascript:optionSubMenuOpen(2,'<s:url value="/report/procesos"/>')">Procesos</a>
					</li>
				</ul>
			</li>	
		</sec:authorize>
		
		<sec:authorize access="hasRole('CC_VARIABLES')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
			    <a  id="menuTheme${numeroOpcion}"
			    	class="menuSeparator"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/ccfactor"/>')">
					<span style="width: 85px">Variables</span>
				</a>
			</li>	
		</sec:authorize>
		
		
		<sec:authorize access="hasRole('CIFRAS_CONTROL')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
			    <a  id="menuTheme${numeroOpcion}"
			    	class="menuSeparator"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/cifrascontrol"/>')">
					<span style="width: 100px">Cifras Control</span>
				</a>
			</li>
		</sec:authorize>
		
		</c:when>
		<c:when test="${tipoMenu eq 'ESQUEMAS'}">
		
		<sec:authorize access="hasAnyRole('ESQ_SOLICITUD_CONSULTAS')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
			    <a  id="menuTheme${numeroOpcion}"
			    	class="menuSeparator"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/esquemas/consulta"/>')">
					<span style="width: 100px;padding-top:3px">Consulta Esquemas</span>
				</a>
			</li>
		</sec:authorize>
		
		
		<sec:authorize access="hasAnyRole('ESQ_SOLICITUD_REVISION')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			
			<li>
			    <a  id="menuTheme${numeroOpcion}"
			    	class="menuSeparator"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/esquemas/autorizacion"/>')">
					<span style="width: 100px;padding-top:3px">Revisi&oacute;n Esquemas</span>
				</a>
			</li>
		</sec:authorize>
		
		
		<sec:authorize access="hasAnyRole('ESQ_GENERA_OFICIOS','ESQ_AUTORIZA_OFICIOS')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
				
				<sec:authorize access="hasRole('ESQ_AUTORIZA_OFICIOS')">
				    <s:url value="/esquemas/oficio/consulta" var="urlOficios"/>
				</sec:authorize>
				<sec:authorize access="hasRole('ESQ_GENERA_OFICIOS')">
					<s:url value="/esquemas/oficio/generar" var="urlOficios"/>
				</sec:authorize>
			    
			    <a  id="menuTheme${numeroOpcion}"
				    	class="menuSeparator"
				    	onMouseOver="disableSeparator(this,${numeroOpcion})"
						onMouseOut="enableSeparator(this,${numeroOpcion},false)"
						href="javascript:optionMenuOpen(${numeroOpcion},'${urlOficios}')">
						<span style="width: 100px;padding-top:3px">Oficios Esquemas</span>
				</a>
			    
				<ul id="subMenuList${numeroOpcion}">
					<c:set var="numeroSubOpcion" value="0"/>
					<sec:authorize access="hasRole('ESQ_GENERA_OFICIOS')">
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/esquemas/oficio/generar"/>')"><s:message code="menu.oficios.generar" htmlEscape="true"/></a>
						</li>
					</sec:authorize>
					<sec:authorize access="hasRole('ESQ_AUTORIZA_OFICIOS')">
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/esquemas/oficio/consulta"/>')"><s:message code="menu.oficios.aceptar" htmlEscape="true"/></a>
						</li>
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						 
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/esquemas/oficio/enviar"/>')"><s:message code="menu.oficios.enviar" htmlEscape="true"/></a>
						</li>
						
					</sec:authorize>
				</ul>
			</li>
			
		</sec:authorize>
		
		<sec:authorize access="hasRole('ESQ_ADMINISTRACION')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
			    <a  id="menuTheme${numeroOpcion}"
			    	class="menuSeparator"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/esquemas/admin"/>')">
					<span style="width: 100px;padding-top:3px">Administraci&oacute;n Esquemas</span>
				</a>
			</li>
		</sec:authorize>
		
		</c:when>
		</c:choose>
		
		<sec:authorize access="hasRole('ADMIN_CATALOGOS')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
				<a  id="menuTheme${numeroOpcion}"
			    	class="menuSeparator"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/admin/catalogos"/>')">
					<span style="width: 80px"><s:message code="menu.catalogos" htmlEscape="true"/></span>
				</a>
				
				<ul id="subMenuList${numeroOpcion}">
					<li id="subMenu${numeroOpcion}_1">
						<a href="javascript:optionSubMenuOpen(1,'<s:url value="/admin/catalogos"/>')">Administraci&oacute;n</a>
					</li>
					<li id="subMenu${numeroOpcion}_2">
						<a href="javascript:optionSubMenuOpen(2,'<s:url value="/params"/>')">Parametros de Sistema</a>
					</li>
					<li id="subMenu${numeroOpcion}_3">
						<a href="javascript:optionSubMenuOpen(3,'<s:url value="/catalogo/refresh"/>')">Actualizar Memoria</a>
					</li>
					
				</ul>
		</sec:authorize>
		
		<sec:authorize access="hasAnyRole('FECHA_RFCS','ADMIN_CLIENTES')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
				<sec:authorize access="hasRole('FECHA_RFCS')">
				    <s:url value="/adminrfc" var="urlClientes"/>
				</sec:authorize>
				<sec:authorize access="hasRole('ADMIN_CLIENTES')">
					<s:url value="/admin/clientes/alta" var="urlClientes"/>
				</sec:authorize>
			    
			    <a  id="menuTheme${numeroOpcion}"
				    	class="menuSeparator"
				    	onMouseOver="disableSeparator(this,${numeroOpcion})"
						onMouseOut="enableSeparator(this,${numeroOpcion},false)"
						href="javascript:optionMenuOpen(${numeroOpcion},'${urlClientes}')">
						<span style="width: 75px">Clientes</span>
				</a>
				<ul id="subMenuList${numeroOpcion}">
					<c:set var="numeroSubOpcion" value="0"/>
					<sec:authorize access="hasRole('ADMIN_CLIENTES')">
						
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/admin/clientes/alta"/>')">Altas</a>
						</li>
						
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/admin/clientes"/>')">Modificaciones</a>
						</li>
						
					</sec:authorize>
					<sec:authorize access="hasRole('FECHA_RFCS')">
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
						<li id="subMenu${numeroOpcion}_${numeroSubOpcion}">
							<a href="javascript:optionSubMenuOpen(${numeroSubOpcion},'<s:url value="/adminrfc"/>')">Captura Fuera de Fecha</a>
						</li>
						<c:set var="numeroSubOpcion" value="${numeroSubOpcion+1}"/>
					</sec:authorize>
				</ul>
			</li>	
		</sec:authorize>
		
		<sec:authorize access="hasRole('ADMIN_USUARIOS')">
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
			    <a  class="menuSeparator"
			    	id="menuTheme${numeroOpcion}"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/usuarios"/>')">
					<span style="width: 80px"><s:message code="menu.usuarios" htmlEscape="true"/></span>
				</a>
				<ul id="subMenuList${numeroOpcion}">
					<li id="subMenu${numeroOpcion}_1">
						<a href="javascript:optionSubMenuOpen(1,'<s:url value="/usuarios"/>')"><s:message code="menu.usuarios.modificar" htmlEscape="true"/></a>
					</li>
					<li id="subMenu${numeroOpcion}_2">
						<a href="javascript:optionSubMenuOpen(2,'<s:url value="/usuarios/alta"/>')"><s:message code="menu.usuarios.alta" htmlEscape="true"/></a>
					</li>
				</ul>
			</li>
			<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
			<li>
			    <a  class="menuSeparator"
			    	id="menuTheme${numeroOpcion}" 
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href="javascript:optionMenuOpen(${numeroOpcion},'<s:url value="/roles"/>')">
					<span style="width: 65px"><s:message code="menu.roles" htmlEscape="true"/></span>
				</a>
				<ul id="subMenuList${numeroOpcion}">
					<li id="subMenu${numeroOpcion}_1">
						<a href="javascript:optionSubMenuOpen(1,'<s:url value="/roles"/>')"><s:message code="menu.roles.modificar" htmlEscape="true"/></a>
					</li>
					<li id="subMenu${numeroOpcion}_2">
						<a href="javascript:optionSubMenuOpen(2,'<s:url value="/roles/alta"/>')"><s:message code="menu.roles.alta" htmlEscape="true"/></a>
					</li>
				</ul>
			</li>
		</sec:authorize>
		

		<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
		<li>
			    <a  class="menuSeparator"
			    	id="menuTheme${numeroOpcion}"
			    	onMouseOver="disableSeparator(this,${numeroOpcion})"
					onMouseOut="enableSeparator(this,${numeroOpcion},false)"
					href='<s:url value="/logout"/>'>
					<span style="width: 60px"><s:message code="menu.salir" htmlEscape="true"/></span>
				</a>
		</li>
		
		
		<c:set var="numeroOpcion" value="${numeroOpcion+1}"/>
		<li>
			<a class="menuSeparatorFinal" id="menuTheme${numeroOpcion}" href="#" title=""><span>&nbsp;</span></a>
		</li>

	</c:if>


</ul>

