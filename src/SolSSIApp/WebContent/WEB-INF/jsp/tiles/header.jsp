<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>



<div class="encabezado">
	<div>
		<h1>
			<a href="#" title="MetLife | Inicio">MetLife</a>
		</h1>
		<table border="0" cellpadding="0" cellspacing="0" style="position: absolute;top:70px;left: 375px;width: 550px">
		      <tr style="height: 50px;vertical-align: center">
			        <td width="5px" class="iniciar">
			        	&nbsp;
			        </td>
			        <td style="font-size: 14px" width="150px">
			        	<span class="fecha"><c:out value="${DIA_ACTUAL}"/></span>
			        	<br/>
		    	   		<span class="fecha"><c:out value="${FECHA_ACTUAL}"/></span>
		       		</td>
		       		<td width="150px">
			        	&nbsp;
			        </td>
		       		<td align="right" style="width: auto;">
		        		<span class="usuario">Bienvenido a <em>MetLife</em></span>
		        		<div class="divusuario">
			        		<div style="padding-top: 5px">
			        			<span class="administra">
			        			<c:choose>
			        				<c:when test="${not empty usuarioLogged}">
			        					<c:out value="${usuarioLogged.nombre}"/>	
			        				</c:when>
			        				<c:otherwise>
			        					Seguro de Separaci&oacute;n Individualizado
			        				</c:otherwise>
			        			</c:choose>
			        			</span>
			        		</div>
		        		</div>
		       		</td>
		       		<td width="5px" class="iniciar">
			        	&nbsp;
			        </td>
		      </tr>
		 </table>
	</div>
	
</div>

