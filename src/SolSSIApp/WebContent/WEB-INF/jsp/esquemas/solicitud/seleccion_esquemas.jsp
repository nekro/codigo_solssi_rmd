<%@ page contentType="text/html" isELIgnored="false"%>
<%@ page buffer="40kb" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<style>
<!--
	#tablaEsquemas{
		border-spacing: 0px;
		border-collapse: collapse;
		color: black;		
	}
	
	#tablaEsquemas td{
		border-left:1px solid #79b7e7;
		border-right:1px solid #79b7e7;
		border-top: 1px solid #79b7e7;
		border-bottom: 1px solid #79b7e7;
		text-align: center;
		width: 25%
	}	
	
	#tablaEsquemas th{
		height: 25px;
	}
	
	.infoResultado{
		background-color: #eaeaea;
		font-weight: bold;
		color: black;	
	}
	
	#lastRow td{
		border-bottom:1px solid #79b7e7;
	}
	
	#thConcepto{
		border-bottom:1px solid #79b7e7;
	}
	
	.tdConcepto{
		font-weight: bold;
		text-align: left !important;
	}
	
	.seleccionado_true{
		background-color: #eaeaea;
	}
-->	
</style>

<script>
		
		function regresarSolicitud(){
			submitForm(document.forms["regresar"]);
		}
		
		function confirmarEsquema(){
			submitForm(document.forms["esquema"]);
		}
		
</script>

		<div id="divEsquema" style="width: 98%;padding: 10px 10px 10px 10px">
			<span style="font-size:13px">
				<b><c:out value="${solicitudEsquema.nombre}"/>&nbsp;<c:out value="${solicitudEsquema.apePaterno}"/>&nbsp;<c:out value="${solicitudEsquema.apeMaterno}"/></b>:
				<br/>
				Has seleccionado el esquema <c:out value="${esquemasInfo.esquemas[solicitudEsquema.esquema].nombre}"/>
			</span>
				
			<br/><br/>
	
	   <table class="seccion-solicitud">
     	    <tr>
     	    	<td>
		     		<div style="width: 850px;overflow: auto;">
		     		<table id="tablaEsquemas" style="width: ${esquemasInfo.totalEsquemasActivos * 200 + 250}px">
			     		<thead>
			      			<tr>
			      				<th id="thConcepto" width="250px" rowspan="2">
			      					&nbsp;
			      				</th>
					   			<th class="ui-state-focus" colspan="${esquemasInfo.totalEsquemasActivos}">
					      			Esquemas
		      					</th>
			      			</tr>
			      			<tr>
			      				
			      				<c:forEach items="${esquemasInfo.esquemas}" var="map">
			      				    <c:if test="${map.value.activo}">
							   			<th class="ui-state-focus">
					      					${map.value.nombre}
				      					</th>
			      					</c:if>
						     	</c:forEach>
			      			</tr>
			      		</thead>
			      		<tbody>
			      			<c:forEach items="${esquemasInfo.atributos}" var="at" varStatus="stAttr">
			      				<c:if test="${at.activo}">
				      				<tr>
				      					<td class="tdConcepto">
				      						<strong>${at.nombre}</strong>
					      				</td>
					      				<c:forEach items="${esquemasInfo.esquemas}" var="map">
								    		<c:set value="${map.value}" var="esquema"/>
								    		<c:if test="${esquema.activo}">
									    		<td class="seleccionado_${map.value.clave eq solicitudEsquema.esquema}">
									    			${esquema.atributos[stAttr.index].valor}
									    		</td>
								    		</c:if>
									    </c:forEach>
				      				</tr>
			      				</c:if>
			      			</c:forEach>
							
							<tr id="lastRow">
			      				<td>&nbsp;</td>
			      				<c:forEach items="${esquemasInfo.esquemas}" var="map">
			      					<c:if test="${map.value.activo}">
								    	<td class="seleccionado_${map.value.clave eq solicitudEsquema.esquema}">
								    		<c:if test="${map.value.clave eq solicitudEsquema.esquema}">
								    			<img src="<s:url value='/resources/images/aceptar32.png'/>" width="25px" height="25px"/>
			      								<br/>
								    			<b>Esquema Seleccionado</b>
								    		</c:if> 
								    	</td>
							    	</c:if>
								</c:forEach>
							</tr>
			      			
			      		</tbody>
			     	</table>
			     	</div>
			     </td>
			  </tr>
			  <tr><td>&nbsp;</td></tr>
	   </table>	      	
	   
	   <c:if test="${not empty esquemasInfo.esquemas[solicitudEsquema.esquema].textoSeleccion}">
	   <div class="ui-state-error ui-corner-all" style="font-weight: bold;">
	   		<br/>
	   		<c:out value="${esquemasInfo.esquemas[solicitudEsquema.esquema].textoSeleccion}"/>
	   		<br/>	
	   		<br/>
	   </div>
	   
	   <br/>
	   </c:if>
	   
			<div>
				<button class="btnEnviar" type="button" onclick="confirmarEsquema()" style="width: 200px"><span><em>Confirmar Selecci&oacute;n</em></span></button>
				<button class="btnEnviar" type="button" onclick="regresarSolicitud()" ><span><em>Regresar</em></span></button>
			</div>
			
			<form method="post" name="esquema" action="<s:url value='/esquemas/solicitud/save'/>"></form>
			<form method="post" name="regresar" action="<s:url value='/esquemas/solicitud/regresar'/>"></form>
			<br/>
		</div>
		
</div>

