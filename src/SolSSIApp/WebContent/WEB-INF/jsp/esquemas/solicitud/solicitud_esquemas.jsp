	<%@ page contentType="text/html" isELIgnored="false"%>
<%@ page buffer="40kb" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<s:url value="/esquemas/solicitud/validate" var="urlProcesa"/>

<style>
	
input{
	text-transform: uppercase;
}
	
<s:hasBindErrors name="solicitudEsquema">
	#containerSolicitud td{
		vertical-align: top;
	}
	
	.error{
		position: relative;
		top: -3px; 
	}
</s:hasBindErrors>

}

.toolTipTable{}

.toolTipTable td{
	border: 1px solid orange;
	text-align:center; 	
	height: 30px;
	width:60px;
	font-size: 12px;
}

.toolTipTable th{
	border: 1px solid orange;
	height: 25px;
	font-weight:bold;
	text-align:center;
}

.headerToolTip{
	text-align: center;
	font-weight: bold;
	color: black;
}


</style>
		
<script>
		 
		var windowDependiente = null;
		var windowErrores = null;
		var captchaDialog;
		dojo.require("dijit.form.Button");
		dojo.require("dijit.Dialog");
		dojo.require("dijit.Tooltip");
		
		dojo.ready(function(){
			
			captchaDialog = new dijit.Dialog({ title:"Verificaci�n de Usuario", style: "width: 475px;text-align:center" }, "divCaptcha");
			dojo.style(dijit.byId("divCaptcha").closeButtonNode,"display","none");
			captchaDialog.startup();
			
			new dijit.Tooltip({
			     connectId: ["tt_rendimientoBajo"],
			     position:["above"],
			     label: "<c:out value='${TT_BAJO_RENDIMIENTO}' escapeXml='false'/>"
			});
			
			new dijit.Tooltip({
				connectId: ["tt_rendimientoAlto"],
				position:["above"], 
				label: "<c:out value='${TT_ALTO_RENDIMIENTO}' escapeXml='false'/>"
			});
			
			<c:if test="${validCaptcha == 'false'}">
				showCaptcha();
			</c:if>
			
			<c:if test="${not empty solicitudEsquema.correoPersonal}">
				js_solicitud.showCorreoPersonal(dojo.byId("correoPersonal"));
			</c:if>
		
			<c:if test="${not empty solicitudEsquema.correoTrabajo}">
				js_solicitud.showCorreoTrabajo(dojo.byId("correoTrabajo"));
			</c:if>
			
			blinkVisible();
		});
		
		
		function actualizarCaptcha(){
			var d = new Date(); 
			dojo.byId("imgCaptcha").src = "<s:url value='/solicitud/captcha?d='/>"+d.getTime();
		}
		
		function seleccionarEsquema(esquema){
			
			document.forms["solicitud"].esquema.value = esquema;
			 showCaptcha();
		}
		
		function showCaptcha(){
			captchaDialog.show();
			dojo.byId("input_captcha").value = '';
			dojo.byId("input_captcha").focus() = '';
		}
		
		function regresar(){
			captchaDialog.hide();
		}
		
		function continuar(){
			
			if(dojo.byId("input_captcha").value == ''){
				return;
			}
			
			captchaDialog.hide();
			
			dojo.byId("captcha").value = dojo.byId("input_captcha").value; 
			document.forms["solicitud"].action = "${urlProcesa}";
			document.forms["solicitud"].target = "_top";
			
			
			submitForm(document.forms["solicitud"]);
		}
		
		function blinkVisible() {
			//document.getElementById('blink').style.visibility='visible';
			document.getElementById('blink').style.border='2px solid orange';
			//document.getElementById('blink').style.backgroundColor='#0080FF';
			document.getElementById('blink').style.color='darkblue';
			setTimeout('blinkHidden()',1250);
		}
		
		function blinkHidden() {
			//document.getElementById('blink').style.visibility='hidden';
			document.getElementById('blink').style.border='2px solid yellow';
			//document.getElementById('blink').style.backgroundColor='#0080FF';
			document.getElementById('blink').style.color='darkblue';
			setTimeout('blinkVisible()',500);
		}
		
		
		
</script>

<div class="moduleTitle"><span>Documento de Selecci&oacute;n de Esquema</span></div>

<div id="containerSolicitud" align="center">      
 <form:form name="solicitud" id="solicitud" modelAttribute="solicitudEsquema" method="post" action="${urlProcesa}" autocomplete="on">
     
    <input type="hidden" name="tipoTramite" value="1">

    <c:set var="tabIndex" value="1" scope="request"/>

	<!--  Table esquema 1Header&1RowX5Columns -->
	<s:hasBindErrors name="solicitudEsquema">
		<div class="ui-widget-content ui-state-error ui-corner-all seccion-solicitud" id="divErrores">
			<br/>
			<table width="100%">
				<tr>
					<td align="center">
						<span><s:message code="solicitud.errores.captura" htmlEscape="true"/></span>
					</td>
				</tr>	
			</table>
			<br/>
		</div>
		<br/>
	</s:hasBindErrors>	
	
	
	
	<table class="ui-widget-content ui-corner-all seccion-solicitud">
		<tbody>
		
			<tr>
				<td align="left" colspan="2"><span class="subseccion">Lugar de Elaboraci&oacute;n</span></td>
			</tr>
			<tr>
	             <td class="tag" id="testTool">
				     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				     <span class="required">*</span> 
				     <label for="estadoElaboracion">Estado:</label>
				 </td>
				 <td class="control">    
	                 <form:select path="estadoElaboracion" cssClass="inputContent" tabIndex="${tabIndex}">
			               <form:option value="0">Seleccione</form:option>
			               <form:options items="${CATALOGO.ESTADOS}" itemLabel="descripcion"/>
		             </form:select>
		             <s:hasBindErrors name="solicitudEsquema"><br/><form:errors path="estadoElaboracion" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
	             </td>
	             <td class="tag">
	             	  <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
	                  <span class="required">*</span>
	                  <label for="ciudadElaboracion">Ciudad/Poblaci&oacute;n:</label>
	             </td>
	             <td class="control">
	                  <form:input path="ciudadElaboracion" cssStyle="width:250px" maxlength="40" htmlEscape="true" tabIndex="${tabIndex}"/>
	                  <s:hasBindErrors name="solicitudEsquema"><br/><form:errors path="ciudadElaboracion" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
	             </td> 
	            </tr>
			
			<tr>
				<td colspan="2" align="left"><span class="subseccion">Datos Generales del Asegurado</span></td>
				<td colspan="2">&nbsp;</td>
			</tr>			
			   
            
            <!-- Homoclave  y cuenta -->
            <tr>
				<td class="tag" ><label for="poliza">P&oacute;liza :</label></td>
			    <td class="valueField" >
			        <c:out value="${solicitudEsquema.poliza}"/>
			    </td>
			</tr>
			<tr>
				<td class="tag" ><label for="rfc">RFC <c:if test="${not empty cliente.homoclave}"><c:out value="-"/></c:if>:</label></td>
			    <td class="valueField" >
			        <c:out value="${cliente.rfc}"/><c:if test="${not empty cliente.homoclave}"><c:out value="-${cliente.homoclave}"/></c:if>
			    </td>
			    <td class="tag" ><label for="cuenta">N&uacute;mero de Cuenta:</label>
			    </td>
			    <td class="valueField">
			        <c:out value="${cliente.cuenta}"/>
			    </td>    
			</tr>
			
	     <!-- Apellido paterno y materno -->  
	        <tr>
				<td class="tag">
					<span class="required">*</span>
					<label for="apePaterno">Apellido Paterno:</label>
				</td>
				<td class="control">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input path="apePaterno" maxlength="30" cssStyle="width:95%" onkeypress="return enableAlfa(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
					<s:hasBindErrors name="solicitudEsquema"><br/><form:errors path="apePaterno" cssClass="error" htmlEscape="true" /></s:hasBindErrors>	
				</td>
				<td class="tag">
				    <span class="required">*</span>
				    <label for="apeMaterno">Apellido Materno:</label>
				</td>
				<td class="control">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input path="apeMaterno" maxlength="30" cssStyle="width:95%" onkeypress="return enableAlfa(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
			   	    <s:hasBindErrors name="solicitudEsquema"><br/><form:errors path="apeMaterno" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			     </td>
			</tr>
	       
	       <!-- Nombre y fecha de nacimiento -->
	       <tr>
				<td class="tag">
				    <span class="required">*</span>
				    <label for="nombre">Nombre(s):</label>
				</td>
				<td class="control">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				    <form:input path="nombre" maxlength="30" cssStyle="width:95%" onkeypress="return enableAlfa(event)" htmlEscape="true" tabIndex="${tabIndex}"/>
					<s:hasBindErrors name="solicitudEsquema" ><br/><form:errors path="nombre" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				
			</tr>
	       
	       <!-- Tipo y numero de identificacion -->
	       <tr>
				<td class="tag">
				     <span class="required">*</span>
				     <label for="cveTipoIdentificacion">Identificaci&oacute;n Oficial:</label>
				</td>
			    <td class="control">
					<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					<form:select path="cveTipoIdentificacion" cssStyle="width:95%" onchange="validaIdentificacion(this, true)" tabIndex="${tabIndex}">
						<option value="0">Seleccione</option>
						<form:options items="${CATALOGO.IDENTIFICACIONES}" itemLabel="descripcion"/>
					</form:select>
					<s:hasBindErrors name="solicitudEsquema"><br/><form:errors path="cveTipoIdentificacion" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				<td class="tag" >
				    <span class="required">*</span>
				    <label for="numeroIdentificacion">N&uacute;mero de Identificaci&oacute;n:</label>
				</td>
				<td class="control" >
				     <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				     <form:input path="numeroIdentificacion" maxlength="20" htmlEscape="true" tabIndex="${tabIndex}"/>
				     <s:hasBindErrors name="solicitudEsquema"><br/><form:errors path="numeroIdentificacion" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
			</tr>
			
			<c:if test="${not empty cliente.nivelPuesto}">
				<tr>
	                <td class="tag">
		       	        <label for="nivelPuesto">Nivel de Puesto:</label>
	                </td> 
				    <td class="valueField">
				        <c:out value="${cliente.nivelPuesto}"/>
				    </td>  
		             <td class="tag">
	                	
	                </td> 
	                <td class="control">
	                
	                </td> 
		       </tr>
	       </c:if>
			
			<tr>
				<td colspan="2" align="left"><span class="subseccion">Correo Electr&oacute;nico: (favor de proporcionar al menos un correo de contacto)</span></td>
				<td colspan="2">&nbsp;</td>
			</tr>

			<tr>
				<td class="tag" >
				       <label for="correoTrabajo">Correo Trabajo:</label>
				</td>
				<td class="control" >
				        <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
			           <c:set var="tabIndexBis" value="${tabIndex+2}"/>
				        <form:input path="correoTrabajo" cssStyle="width:95%;text-transform: none;" onchange="js_solicitud.showCorreoTrabajo(this);dojo.byId('correoTrabajoConfirm').focus()" maxlength="50" htmlEscape="true" tabIndex="${tabIndex}"/>
					    <s:hasBindErrors name="solicitudEsquema"><br/><form:errors path="correoTrabajo" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
				<td class="tag" >
				      <label for="correoPersonal">Correo Personal:</label>
				</td>
				<td class="control" >
			           <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
			           <c:set var="tabIndexBis" value="${tabIndex+2}"/>
			           <form:input path="correoPersonal" cssStyle="width:95%;text-transform: none;" onchange="js_solicitud.showCorreoPersonal(this);dojo.byId('correoPersonalConfirm').focus()" maxlength="50" htmlEscape="true" tabIndex="${tabIndexBis}"/>
			           <s:hasBindErrors name="solicitudEsquema"><br/><form:errors path="correoPersonal" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				</td>
			</tr>
		    <tr style="height: 25px">
				<td class="tag" >
				    <div id="divCorreoTrabajoLabel" style="display:none">
				         <label for="correoTrabajoConfirm">Confirmar Correo Trabajo:</label>
                    </div>				    
				</td>
				<td class="control" >
				    <div id="divCorreoTrabajo" style="display:none">
				    <c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
			           <c:set var="tabIndexBis" value="${tabIndex+2}"/>
				        <form:input path="correoTrabajoConfirm" cssStyle="width:95%;text-transform: none;" maxlength="50" htmlEscape="true" onpaste="return false;" tabIndex="${tabIndex}"/>
				        <s:hasBindErrors name="solicitudEsquema"><br/><form:errors path="correoTrabajoConfirm" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
				    </div>
				</td>
				<td class="tag" >
				    <div id="divCorreoPersonalLabel" style="display:none">
				         <label for="correoPersonalConfirm">Confirmar Correo Personal:</label>
				    </div>
				</td>
				<td class="control" >
				    <div id="divCorreoPersonal" style="display:none">
			           <form:input path="correoPersonalConfirm" cssStyle="width:95%;text-transform: none;" maxlength="50" htmlEscape="true" onpaste="return false;" tabIndex="${tabIndexBis}"/>
			           <s:hasBindErrors name="solicitudEsquema"><br/><form:errors path="correoPersonalConfirm" cssClass="error" htmlEscape="true" /></s:hasBindErrors>
			        </div>
			    </td>
			</tr>
		    <tr style="height: 20px">
				<td class="tag">
				</td>
				<td class="control" style="position: relative;top: -5px">
				    <span id="error_correoPersonal" class="error"/>
				</td>
				<td class="tag">
				</td>
				<td class="control" style="position: relative;top: -5px">
				    <span id="error_correoTrabajo" class="error"/>    				    
				</td>
			</tr>
			<tr>
				<td colspan="4" align="center">
					<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
					<form:checkbox path="avisoPrivacidad" value="true" onchange="showMessageAvisoPrivaidad(this.value)" tabIndex="${tabIndex}" />
					<span>Acepto y autorizo que mis datos personales, patrimoniales o financieros y sensibles <br/> sean tratados conforme al </span> <a href="http://www.metlife.com.mx/wps/portal/seguros/QuienesSomos?Menu=7&tabT=1" target="_blank" style="text-decoration: underline;color: blue">aviso de privacidad</a>
					<br/><form:errors path="avisoPrivacidad" cssClass="error"/>
				</td>
			</tr>
	      </tbody>
      </table>
      
      <br/><br/>
      
      <div class="ui-state-active ui-corner-all">
      		<div style="text-align: justify;padding: 10px 10px 10px 10px;color: #1d5987;font-weight: bold;">
      			<s:escapeBody>
      				Tu Seguro de Separaci�n Individualizado (SSI) es un respaldo econ�mico que te ayudar� en caso de que te separes de tu cargo, en caso de incapacidad total o permanente, en caso de jubilaci�n o fallecimiento para que tus seres queridos queden protegidos.
				</s:escapeBody>
				<br/>
				<s:escapeBody>	
					Tu Seguro est� formado por tus aportaciones m�s las aportaciones que realiza la Dependencia en la que laboras  m�s los rendimientos que este ahorro genere.
				</s:escapeBody>
				<br/>
				<s:escapeBody>	
					Por lo tanto, es muy importante que este ahorro lo mantengas a largo plazo para que obtengas mayores rendimientos y que en caso de cualquier eventualidad puedas solicitar este ahorro para cubrir tus necesidades.
				</s:escapeBody>
      		</div>
      </div>
      <br/><br/>
      
      <div>
     	<div style="font-size: 18px;font-weight: bold;height: 35px;padding-top: 10px" class="ui-state-highlight" id="blink">
      			Ahorrar hoy, asegura tu futuro y el de tu familia
      	</div>
      </div>
      
      <br/><br/>
      
      
     <div>
     	<span>A continuaci&oacute;n te presentamos ${esquemasInfo.totalEsquemasActivos} esquemas de rescates parciales, favor de elegir una de las opciones siguientes:</span>
     </div>
     <table class="seccion-solicitud">
     	
     	<tr>
     		<td align="center">
		     		<br/><br/>
		     		<div style="width: 850px;overflow: auto;">
		     		<table id="tablaEsquemas" style="width: ${esquemasInfo.totalEsquemasActivos * 200 + 250}px">
			     		<thead>
			      			<tr>
			      				<th id="thConcepto" width="250px" rowspan="2">
			      					&nbsp;
			      				</th>
					   			<th class="ui-state-focus" colspan="${esquemasInfo.totalEsquemasActivos}">
					      			Esquemas
		      					</th>
			      			</tr>
			      			<tr>
			      				
			      				<c:forEach items="${esquemasInfo.esquemas}" var="map">
			      				    <c:if test="${map.value.activo}">
							   			<th class="ui-state-focus">
					      					${map.value.nombre}
				      					</th>
			      					</c:if>
						     	</c:forEach>
			      			</tr>
			      		</thead>
			      		<tbody>
			      			<c:forEach items="${esquemasInfo.atributos}" var="at" varStatus="stAttr">
			      				<c:if test="${at.activo}">
				      				<tr>
				      					<td class="tdConcepto">
				      						<strong>${at.nombre}</strong>
					      				</td>
					      				<c:forEach items="${esquemasInfo.esquemas}" var="map">
								    		<c:set value="${map.value}" var="esquema"/>
								    		<c:if test="${esquema.activo}">
									    		<td>
									    			${esquema.atributos[stAttr.index].valor}
									    		</td>
								    		</c:if>
									    </c:forEach>
				      				</tr>
			      				</c:if>
			      			</c:forEach>
			      			<tr id="lastRow">
			      				<td>&nbsp;</td>
			      				<c:forEach items="${esquemasInfo.esquemas}" var="map">
			      					<c:if test="${map.value.activo}">
								    	<td>
								    		<button class="btnEnviar" type="button" onclick="seleccionarEsquema(${map.value.clave})"><span><em>Seleccionar</em></span></button>
								    	</td>
							    	</c:if>
								</c:forEach>
							</tr>
			      			
			      		</tbody>
			     	</table>
			     	</div>
			     	</td>
  		     	</tr>
     	</table>
     
     	<br/><br/>
     	
     	<div class='ui-state-custom ui-corner-all'>
     	<div style="text-align: left;padding: 10px 10px 10px 10px;font-size: 16px;color:#444;font-weight: bold;width: 95%">
			Es importante consideres que la reserva m&iacute;nima (creada con 2 salarios brutos) estar&aacute; invertida igual que el SSI
				    original, tu elecci&oacute;n es para las aportaciones por encima de &eacute;sta.
			
     	</div>
     	</div>
     	
     	<div style="text-align: center;padding: 10px 10px 10px 10px;font-size: 14px;color: black;font-weight: bold;">
			El Esquema de Rescates Parciales que selecciones ser&aacute; aplicable de manera permanente y no podr&aacute; ser modificado.
     	</div>
     	
     	<br/><br/>
     	
     	<div>
			<span style="color:#000;font-weight:bold">
				Declaro que toda la informaci&oacute;n registrada en el presente Documento de Selecci&oacute;n es veraz y refleja mi voluntad de elecci&oacute;n.
			</span>
		</div>
		
		<br/><br/>
		
		<table width="100%">
										<c:forEach items="${esquemasInfo.notas}" var="nota" varStatus="nst">
										    <c:if test="${nota.activo}">
												<tr>
													<td valign="top">
														${nota.referencia}
													</td>
													<td align="left">
														${nota.texto}
													</td>
												</tr>
											</c:if>
										</c:forEach>
						</table>
	
	<form:hidden path="poliza"/>
	<form:hidden path="esquema"/>
	<form:hidden path="captcha"/>
</form:form>

</div>



<div id="divCaptcha" style="display: none">  
	  <table style="width: 100%" align="center">
		     <tbody>
	        <tr>
				<td colspan="4" align="center">
				   
				   <div class="ui-state-custom" style="color: black;font-weight: bold;">
				   		<table>
				   			<tr>
				   				<td><img src="<s:url value='/resources/images/warning_red.png'/>" width="37px" height="37px"></td>
				   				<td>El Esquema de Rescates Parciales que selecciones ser&aacute; aplicable de manera permanente y no podr&aacute; ser modificado.</td>
				   			</tr>
				   		</table>
				   </div>
				   
				   <br/><br/>
				   
				   <div>
				   		<label for="captcha"><s:message code="NotEmpty.solicitud.captcha"/></label>
				   </div>
				   
				   <div>
				        <button type="button" style="color:blue;font-size:8pt;text-decoration:underline" onclick="javascript:actualizarCaptcha()" onfocus="dojo.byId('input_captcha').focus()">Actualizar Imagen</button>
				   </div>
				   <div>
				   		<img src="<s:url value='/solicitud/captcha'/>" id="imgCaptcha"/>
				   </div>
				   <div>
				   		<c:set var="tabIndex" value="${tabIndex+1}" scope="request"/>
				   		<input name="input_captcha" id="input_captcha" maxlength="5" style="text-transform: none;width:50px" htmlEscape="true" onkeypress="return enableAlfa(event)"/>
				    	<c:if test="${validCaptcha == 'false'}">
				    		<br/>
							<span class="error"><s:message code="NotValidCaptcha.solicitud.captcha"/></span>
						</c:if>
				   </div>
				   <br/>
				   <div>
				   		<button class="btnEnviar" type="button" onclick="continuar()"><span><em>Continuar</em></span></button>
				   		<button class="btnEnviar" type="button" onclick="regresar()"><span><em>Regresar</em></span></button>
				   </div>
				   
			    </td>
			</tr>
		  </tbody>
	   </table>
</div>
