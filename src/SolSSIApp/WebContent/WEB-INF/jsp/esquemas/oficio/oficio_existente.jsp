<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>

	function imprimirOficio() {
		openWindowCenter("", "oficiopdf", 1000, 600);
		document.forms["print"].submit();
	}
	
	function showDetalleOficio(numOficio) {
		document.forms["detalle"].oficio.value = numOficio;
		submitForm(document.forms["detalle"]);
	}
	
	
</script>

<style>

	#filtro li {
		float: left;
		margin: 0px 20px 0px 0px;
		list-style-type: none;
		height: 50px
	}

</style>
<c:set var="oficio" value="${oficioEsquema}"/>
<div class="moduleTitle"><span><s:message code="module.title.oficio.generar" htmlEscape="true"/></span></div>

		<div>
			<span class="error"><s:message code="oficio.alredy.generated.dependencia"/></span>
		</div>
		
		<br/>
		
		<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 80%">
			<form name="oficios" method="post">
				<table  class="display" style="width: 100%;padding-top: 30px" align="center">
					<thead class="ui-widget-header">
						<tr>
							<th>Oficio</th>
							<th>Dependencia</th>
							<th>Fecha Generaci&oacute;n</th>
							<th>Detalle</th>
							<th>Estatus</th>
							<th>Reimprimir</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
		
						<c:set var="cveDependencia">${oficio.dependencia}</c:set>
						<c:choose>
					    	<c:when test="${st.count % 2 == 1}">
					    		<c:set var="css" value="odd"/>
					    	</c:when>
					    	<c:otherwise>
					    		<c:set var="css" value="even"/>
					    	</c:otherwise>
					    </c:choose>
		
						<tr class="${css}">
							<td><c:out value="${oficio.numOficio}"/></td>
							<td><c:out value="${CATALOGO.DEPENDENCIAS[cveDependencia].descripcion}"/></td>
							<td><fmt:formatDate value="${oficio.fechaGeneracion}" pattern="dd-MM-yyyy HH:mm:ss"/></td>
							
							
							<td><a href="javascript:showDetalleOficio(<c:out value='${oficio.numOficio}'/>)" style="color: blue;text-decoration: underline;">
								Detalle</a></td>
							
							<td>
								<c:choose>
									<c:when test="${oficio.estatus eq 1}">
										Generado
									</c:when>
									<c:when test="${oficio.estatus eq 3}">
										Enviado
									</c:when>
									<c:when test="${oficio.estatus eq 2}">
										Aceptado
									</c:when>
								</c:choose>
							</td>
							<td>
								<a href="javascript:imprimirOficio()">Imprimir</a>
							</td>
						</tr>
						
				</table>
			</form>	
		</div>
		

		<form name="detalle" action="<s:url value='/esquemas/oficio/detalle'/>" method="post">
			<input name="oficio" type="hidden"/>
		</form>
		
		<form name="print" action="<s:url value='/esquemas/oficio/reimpresion'/>"
		method="post" target="oficiopdf"></form>