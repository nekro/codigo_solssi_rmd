<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>

	function buscar() {
		document.forms["filtro"].action = "<s:url value='/esquemas/oficio/consulta/execute'/>";
		submitForm(document.forms["filtro"]);
	}

	function showDetalleOficio(numOficio) {
		document.forms["detalle"].oficio.value = numOficio;
		submitForm(document.forms["detalle"]);
	}
	
	function enviarOficio(){
		
		document.forms["oficios"].action = "<s:url value='/esquemas/oficio/enviar/execute'/>";
		submitForm(document.forms["oficios"]);
	}
	
	function selectAllOficios(check){
		
		if(document.forms["oficios"].oficios.length){
			for(var i = 0; i < document.forms["oficios"].oficios.length ; i++){
				document.forms["oficios"].oficios[i].checked = check.checked;
			}
		}else{
			document.forms["oficios"].oficios.checked = check.checked;	
		}	
	}
	
</script>

<style>

	#filtro li {
		float: left;
		margin: 0px 20px 0px 0px;
		list-style-type: none;
		height: 50px
	}

</style>
<div class="moduleTitle"><span><s:message code="module.title.oficio.enviar" htmlEscape="true"/></span></div>



	<c:if test="${not empty oficios}">
		<br/>
		<h4>Oficios Aceptados</h4>
		<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 95%">
			<form name="oficios" method="post">
				<table  class="display" style="width: 100%;padding-top: 30px" align="center">
					<thead class="ui-widget-header">
						<tr>
							<th>Oficio</th>
							<th>Dependencia</th>
							<th>Fecha Generaci&oacute;n</th>
							<th>Detalle</th>
							<th>Enviado</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
		
					<c:forEach items="${oficios}" var="oficio" varStatus="st">
						<c:set var="cveDependencia">${oficio.dependencia}</c:set>
						<c:choose>
					    	<c:when test="${st.count % 2 == 1}">
					    		<c:set var="css" value="odd"/>
					    	</c:when>
					    	<c:otherwise>
					    		<c:set var="css" value="even"/>
					    	</c:otherwise>
					    </c:choose>
		
						<tr class="${css}">
							<td><c:out value="${oficio.numOficio}"/></td>
							<td align="left" title="${cveDependencia}"><c:out value="${CATALOGO.DEPENDENCIAS[cveDependencia].descDependencia}"/></td>
							<td><fmt:formatDate value="${oficio.fechaGeneracion}" pattern="dd-MM-yyyy HH:mm:ss"/></td>
							<td><a href="javascript:showDetalleOficio(<c:out value='${oficio.numOficio}'/>)" style="color: blue;text-decoration: underline;">
								Detalle</a></td>
							<td>
								<c:choose>
									<c:when test="${oficio.bndAutomatico lt 2}">
										<c:set var="checked" value=""/>
										<c:if test="${oficio.bndAutomatico eq 1}"><c:set var="checked" value="checked"/></c:if>
										<input type="checkbox" name="automaticos" value="<c:out value="${oficio.numOficio}"/>" ${checked}/>
									</c:when>
									<c:otherwise>
										<img src="<s:url value='/resources/images/sucess.png'/>" height="20px" width="20px">
									</c:otherwise>
								</c:choose>
							</td>
													</tr>
					</c:forEach>
				</table>
			</form>	
		</div>
		<div>
			<br/>
			<button type="button" class="btnEnviar" onclick="enviarOficio()" style="width: 140px"><span><em>Enviar Oficios</em></span></button>
		</div>
	</c:if>

	<c:if test="${empty oficios}">
		<div>
			<img src="<s:url value='/resources/images/warning_blue.png'/>" height="41px" width="41px"/>
		</div>
		<div>
			Por el momento no hay Oficios Aceptados
		</div>
	</c:if>


<form name="detalle" action="<s:url value='/esquemas/oficio/detalle'/>" method="post">
	<input name="oficio" type="hidden"/>
</form>