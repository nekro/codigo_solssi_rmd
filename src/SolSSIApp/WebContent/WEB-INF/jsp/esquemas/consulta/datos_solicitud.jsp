<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

	<c:set var="estatus"><c:out value="${solicitud.estatus}"/></c:set>
	<div align="center">
		<table class="ui-widget-content ui-state-focus ui-corner-all seccion-solicitud">
				<tr>
					<td width="25%" align="right">
						Folio:						
					</td>
					<td width="25%" class="valueField">
						<c:out value="${solicitud.userFolio}"/>
					</td>
					<td width="25%" align="right">
						Estatus:						
					</td>
					<td class="valueField">
						<c:out value="${CATALOGO.ESTATUS_TRAMITE[estatus].valorCampo2}"/>
					</td>
				</tr>
				<tr>
					
				</tr>
				  
				<tr>
					<td width="25%" align="right">
						Esquema:						
					</td>
					<td class="valueField">
						<c:out value="${solicitud.esquema}"/>&nbsp;<c:out value="${esquemasInfo.esquemas[solicitud.esquema].nombre}"/>
					</td>
				</tr>
		</table>
	</div>


<div id="containerSolicitud" align="center" class="consultaSolicitud">      
      
	<form name="solicitud" id="solicitud" method="post" action="${urlProcesa}">
	     
		<br/>
		<div align="left" class="seccion">Datos Generales de Asegurado</div>
		<table class="ui-widget-content ui-corner-all seccion-solicitud">
			<tbody>
			
				<tr>
					<td class="tag">
					    <label for="estadoElaboracion">Estado de Elaboraci&oacute;n:</label>
					</td>
					<td class="valueField">
					      <c:set var="estadoElaboracion"><c:out value="${solicitud.estadoElaboracion}"/></c:set>
					      <c:out value="${CATALOGO.ESTADOS[estadoElaboracion].descripcion}"/>
	                </td>
	                <td class="tag">
					    <label for="estadoElaboracion">Ciudad de Elaboraci&oacute;n:</label>
					</td>
		             <td class="valueField">
		                  <c:out value="${solicitud.ciudadElaboracion}"/>
		             </td> 
				</tr>
				   
	            
	            <!-- Subseccion datos personales -->
		        <tr>
					<td colspan="2" align="left"><span class="subseccion">Datos Personales</span></td>
					<td colspan="2">&nbsp;</td>
				</tr>
	            
	            
	            <!-- Homoclave  y cuenta -->
				<tr>
					<td class="tag" ><label for="rfc">RFC - Homoclave:</label></td>
				    <td class="valueField" >
				        <c:out value="${solicitud.rfc}"/> <c:if test="${not empty solicitud.homoclave}">- <c:out value="${solicitud.homoclave}"/></c:if>
				    </td>
				    <td class="tag" ><label for="cuenta">N&uacute;mero de Cuenta:</label>
				    </td>
				    <td class="valueField">
				        <c:out value="${solicitud.cuenta}"/>
				    </td>    
				</tr>
				
		     <!-- Apellido paterno y materno -->  
		        <tr>
					<td class="tag" >
						<label for="apePaterno">Apellido Paterno:</label>
					</td>
					<td class="valueField" >
					    <c:out value="${solicitud.apePaterno}"/>
					</td>
					<td class="tag" >
					    <label for="apeMaterno">Apellido Materno:</label>
					</td>
					<td class="valueField" >
					    <c:out value="${solicitud.apeMaterno}"/>
				     </td>
				</tr>
		       
		       <!-- Nombre y fecha de nacimiento -->
		       <tr>
					<td class="tag" >
					    <label for="nombre">Nombre(s):</label>
					</td>
					<td class="valueField" >
						<c:out value="${solicitud.nombre}"/>
					</td>
				</tr>
		       
		       
		       <tr>
		       	    <td class="tag">
		       	        <label for="centroTrabajo">Centro de Trabajo:</label>
		       	    </td>
					<td class="valueField">
						<c:set var="centroTrabajo"><c:out value="${solicitud.centroTrabajo}"/></c:set>
					    <c:out value="${CATALOGO.DEPENDENCIAS[centroTrabajo].descripcion}"/>
				    </td>
	                <td class="tag">
		       	        <label for="nivelPuesto">Nivel de Puesto:</label>
	                </td>
	                <td class="valueField">
						<c:set var="nivelPuesto"><c:out value="${solicitud.nivelPuesto}"/></c:set>
					    <c:out value="${CATALOGO.NIVEL_PUESTO[nivelPuesto].descripcion}"/>
	                </td>
				</tr>
		       
		       
		       <!-- Tipo y numero de identificacion -->
		       <tr>
					<td class="tag" >
					     <label for="cveTipoIdentificacion">Identificaci&oacute;n Oficial:</label>
					</td>
				    <td class="valueField" >
						<c:set var="cveTipoIdentificacion"><c:out value="${solicitud.cveTipoIdentificacion}"/></c:set>
					    <c:out value="${CATALOGO.IDENTIFICACIONES[cveTipoIdentificacion].descripcion}"/>
					</td>
					<td class="tag" >
					    <label for="numeroIdentificacion">N&uacute;mero de Identificaci&oacute;n:</label>
					</td>
					<td class="valueField" >
					     <c:out value="${solicitud.numeroIdentificacion}"/>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" align="left"><span class="subseccion">Correo Electr&oacute;nico</span></td>
					<td colspan="2">&nbsp;</td>
				</tr>
	
				<tr>
					<td class="tag" >
					       <label for="correoTrabajo">Correo Trabajo:</label>
					</td>
					<td class="valueField" >
						    <c:out value="${solicitud.correoTrabajo}"/>
					</td>
					<td class="tag" >
					      <label for="correoPersonal">Correo Personal:</label>
					</td>
					<td class="valueField" >
				           <c:out value="${solicitud.correoPersonal}"/>
					</td>
				</tr>
	
		       
		      </tbody>
	      </table>
	      <input type="hidden" name="nivelPuesto"/>
	</form>
</div>
