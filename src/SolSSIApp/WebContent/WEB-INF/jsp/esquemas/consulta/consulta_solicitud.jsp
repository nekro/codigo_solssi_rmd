<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>

<s:url value="/esquemas/consulta" var="urlConsulta"/>

<script>
		 
		dojo.ready(function(){
			  
			
		});
		
		 
		 		 
		 function regresarConsulta(){
			 //document.forms["solicitud"].action = "<s:url value='/'/>${urlRegresar}";
			 //document.forms["solicitud"].target = "_top";
			 //document.forms["solicitud"].submit();
			 history.back();
		 }
		 
</script>

<div class="moduleTitle"><span><s:message code="module.title.solicitud.esquemas.consulta" htmlEscape="true"/></span></div>

<jsp:include page="datos_solicitud.jsp"/>

<br/>
	<div>
		<button type="button" class="btnEnviar" onclick="regresarConsulta()"><span><em>Regresar</em></span></button>
	</div>
<br/>
