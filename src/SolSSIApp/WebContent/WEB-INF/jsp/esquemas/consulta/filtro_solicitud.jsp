<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>


<script>
	var windowPDF;
	
	function buscar() {
		document.forms["filtro"].target = "_top";
		document.forms["filtro"].action = "<s:url value='/esquemas/consulta/execute'/>";
		submitForm(document.forms["filtro"]);
	}

	function showSolicitud(numFolio) {
		document.forms["solicitud"].numFolio.value = numFolio;
		document.forms["solicitud"].submit();
	}
	
	 function reImpresion(numFolio){
		 windowPDF = openWindowCenter("","SSI_PDF",1000,600);
		 document.forms["filtro"].target = "SSI_PDF";
		 document.forms["filtro"].action = "<s:url value='/esquemas/pdfservice/reimpresion/solicitud/'/>"+numFolio;
		 document.forms["filtro"].submit();
	 }
	 
	 function historial(numFolio){
		document.forms["historico"].numFolio.value = numFolio;
		document.forms["historico"].submit();
	 }

</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

</style>


<div class="moduleTitle"><span>Consulta de Documentos de Selecci&oacute;n de Esquema</span></div>
	<div id="filtro" style="width: 90%" align="center">
		<form:form modelAttribute="consultaFilterEsquema" name="filtro">
			<table class="ui-widget-content ui-corner-all">
				<tr >
					<td align="center"><span class="titleSeccion">Filtro de B&uacute;squeda</span>
					</td>
				</tr>
				<tr>
					<td align="left">
						<ul>
							<li><span>Folio:</span> <form:input path="userFolio"
									maxlength="10" style="width:90px" htmlEscape="true"/></li>
							<li><span>RFC:</span> <form:input path="rfc" maxlength="10"
									style="width:90px" /> - <form:input path="homoclave"
									maxlength="3" style="width:30px" htmlEscape="true"/></li>
							<li><span>Cuenta:</span> <form:input path="cuenta"
									onkeypress="return enableNumero(event)" maxlength="10"
									style="width:105px" htmlEscape="true"/></li>
							<li>
								<button type="button" class="btnEnviar" onclick="buscar()"><span><em>Buscar</em></span></button>
							</li>		
	
						</ul>
					</td>
				</tr>
			</table>
			<s:hasBindErrors name="consultaFilterEsquema">
				<br/><br/>
				<div class="ui-state-error ui-corner-all" style="width: 80%;padding-top: 10px;padding-bottom: 10px">
					<form:errors path="*"/>
				</div>
			</s:hasBindErrors>
		</form:form>
	</div>
	
	

<c:if test="${not empty solicitudes}">
	
		<br/>
		<h4>Resultados</h4>
		<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 95%">
			<table  class="display" style="width: 100%;padding-top: 30px" align="center">
				<thead class="ui-widget-header">
					<tr>
						<th width="10%"><s:message code="table.header.solicitud.folio" htmlEscape="true"/></th>
						<th width="10%"><s:message code="table.header.solicitud.cuenta" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.rfc" htmlEscape="true"/></th>
						<th><s:message code="table.header.esquema.fechaSolicitud" htmlEscape="true"/></th>
						<th><s:message code="table.header.solicitud.estatus" htmlEscape="true"/></th>
						<sec:authorize access="hasRole('ESQ_SOLICITUD_CONSULTAS')">
							<th><s:message code="table.header.esquema.desplegar" htmlEscape="true"/></th>
						</sec:authorize>
						<sec:authorize access="!hasAnyRole('ESQ_SOLICITUD_REVISION')">
							<th><s:message code="table.header.solicitud.reimprimir" htmlEscape="true"/></th>
						</sec:authorize>
						<sec:authorize access="hasRole('HISTORICOS')">
							<th>Hist&oacute;rico</th>
						</sec:authorize>
					</tr>
				</thead>
				<tbody>
				</tbody>
	
				<c:forEach items="${solicitudes}" var="sol" varStatus="st">
					
					<c:choose>
				    	<c:when test="${st.count % 2 == 1}">
				    		<c:set var="css" value="odd"/>
				    	</c:when>
				    	<c:otherwise>
				    		<c:set var="css" value="even"/>
				    	</c:otherwise>
				    </c:choose>
					<c:set var="estatus">${sol.estatus}</c:set>

					<tr class="${css}">
						<td><c:out value="${sol.userFolio}"/></td>
						<td><c:out value="${sol.cuenta}"/></td>
						<td><c:out value="${sol.rfc}"/></td>
						<td><fmt:formatDate  value="${sol.fechaCaptura}" pattern="dd-MM-yyyy HH:mm:ss"/></td>
						
						<td>
							<c:out value="${CATALOGO.ESTATUS_TRAMITE[estatus].valorCampo2}"/>
						</td>

						<sec:authorize access="hasRole('ESQ_SOLICITUD_CONSULTAS')">
							<td><a href="javascript:showSolicitud(${sol.numFolio})" style="color: blue;text-decoration: underline;">
								<img src="<s:url value='/resources/images/edit-find32.png'/>" alt="Desplegar" height="17px" width="17px"/></a>
							</td>
						</sec:authorize>
						
						<sec:authorize access="!hasAnyRole('ESQ_SOLICITUD_REVISION')">
							<td><a href="javascript:reImpresion(${sol.numFolio})" style="color: blue;text-decoration: underline;">
								<img src="<s:url value='/resources/images/pdf-icon32.png'/>" alt="Desplegar" height="17px" width="17px"/>
							</a></td>
						</sec:authorize>
						
						<sec:authorize access="hasRole('HISTORICOS')">
							<td>
								<a href="javascript:historial(${sol.numFolio})" style="color: blue;text-decoration: underline;">
									Historial
								</a>
							</td>
						</sec:authorize>
					</tr>
				</c:forEach>
			</table>
		</div>
		<form name="solicitud" action="<s:url value='/esquemas/consulta/view'/>" method="post">
			<input type="hidden" name="numFolio"/>
		</form>
		
		<sec:authorize access="hasRole('HISTORICOS')">
			<form name="historico" action="<s:url value='/logs/solicitud/esquema'/>" method="post">
				<input type="hidden" name="numFolio"/>
			</form>
		</sec:authorize>
</c:if>
