<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<style>
<!--
	#adminEsquemas a{
		color: blue !important;
		text-decoration: underline !important;
		font-size: 12px !important;
		font-weight: normal !important;
	}
	
	.atributo_false{
		background-color: #f1f1f1;
		color: #c5c5c5;
	}
	
	.atributo_false a{
		color: gray !important;
	}
	
	.atributo_true{}
	
	.esquema_true{}
	
	.esquema_false{
		background-color: #f1f1f1;
		color: #c5c5c5;
	}
	
	.nota_false{
		background-color: #f1f1f1;
		color: #c5c5c5;
	}
-->
</style>

<script type="text/javascript">

	var esquemaDlg;
	dojo.require("dijit.Dialog");
	
	dojo.ready(function(){
		
		esquemaDlg = new dijit.Dialog({ title:"Par&aacute;metros", style: "width: 600px;text-align:center" }, "editDialog");
		esquemaDlg.startup();
	  	
	  	<c:if test="${not empty appParameter}">
			paramsDlg.show();
		</c:if>
	  	
	});
		
			
	  function cancelar(){
		  paramsDlg.hide();
	  } 
      
	  function editarEsquema(esquema){
		     document.forms["form"].action = "<s:url value='/esquemas/admin/values'/>";
		     document.forms["form"].esquema.value = esquema;
		     submitForm(document.forms["form"]);
	  
	  
	  }
	  
	  function nuevoEsquema(){
		     document.forms["form"].action = "<s:url value='/esquemas/admin/add'/>";
		     submitForm(document.forms["form"]);
	  }
	  
	  
	  
	  function editarAtributo(atributo){
		     document.forms["atributo"].action = "<s:url value='/esquemas/admin/atributos'/>";
		     document.forms["atributo"].atributo.value = atributo;
		     submitForm(document.forms["atributo"]);
	  }
	  
	  function agregarAtributo(){
		     document.forms["atributo"].action = "<s:url value='/esquemas/admin/atributos/add'/>";
		     submitForm(document.forms["atributo"]);
	  }
	  
	  

	  function editarNota(nota){
		     document.forms["nota"].action = "<s:url value='/esquemas/admin/notas'/>";
		     document.forms["nota"].nota.value = nota;
		     submitForm(document.forms["nota"]);
	  }
	  
	  function agregarNota(){
		     document.forms["nota"].action = "<s:url value='/esquemas/admin/notas/add'/>";
		     submitForm(document.forms["nota"]);
	  }
		
</script>


<div id="adminEsquemas" align="center">

		<div class="moduleTitle"><span>Administraci&oacute;n de Esquemas</span></div>
		<form name="form" method="POST">
		<table class="seccion-solicitud">
			<tr><td align="center">
	 		
	 			  <div style="width: 90%;padding-bottom: 5px" align="right" >
	 			  		<a href="javascript:nuevoEsquema()">Agregar Esquema</a>	
	 			  </div>
	 			  <div style="width: 850px;overflow: auto;">
		     	  <table id="tablaEsquemas" style="width: ${esquemasInfo.totalEsquemas * 200 + 250}px">
			     		<thead>
			      			<tr>
			      				<th id="thConcepto" width="250px">
			      					&nbsp;
			      				</th>
			      				<c:forEach items="${esquemasInfo.esquemas}" var="map">
						   			<th class="ui-state-focus">
				      					<a href="javascript:editarEsquema(${map.value.clave})">${map.value.nombre}</a>
				      					
			      					</th>
						     	</c:forEach>
			      			</tr>
			      		</thead>
			      		<tbody>
			      			<c:forEach items="${atributos}" var="at" varStatus="stAttr">
			      				<tr class="atributo_${at.activo}">
			      					<td class="tdConcepto" onclick="javascript:editarAtributo(${at.id})" style="cursor: pointer;" title="click para editar">
			      						<a href="javascript:editarAtributo(${at.id})"><strong>${at.nombre}</strong></a>
				      				</td>
				      				<c:forEach items="${esquemasInfo.esquemas}" var="map">
							    		<c:set value="${map.value}" var="esquema"/>
							    		<td class="esquema_${esquema.activo}">
							    			${esquema.atributos[stAttr.index].valor}
							    		</td>
								    </c:forEach>
			      				</tr>
			      			</c:forEach>
			      			<tr id="lastRow">
			      				<td><a href="javascript:agregarAtributo()">Agregar Concepto</a></td>
			      				<c:forEach items="${esquemasInfo.esquemas}" var="map">
							    	<td>&nbsp;</td>
								</c:forEach>
							</tr>
			      		</tbody>
			     	</table>
			     	<table width="100%">
										<c:forEach items="${notas}" var="nota" varStatus="nst">
										    
											<tr>
												<td valign="top">
													<a href="javascript:editarNota(${nota.numero})">${nota.referencia}</a>
												</td>
												<td align="left" class="nota_${nota.activo}">
													${nota.texto}
												</td>
											</tr>
											
										</c:forEach>
										<tr>
												<td valign="top">
													&nbsp;
												</td>
												<td align="left">
													<a href="javascript:agregarNota()">Agregar Nota</a>
												</td>
										</tr>
						</table>
									
			     	</div>
	 			  <input type="hidden" name="esquema">		
			</td></tr>
		</table>
		</form>
		
		<form method="POST" name="atributo">
			<input type="hidden" name="atributo">
		</form>
		
		<form method="POST" name="nota">
			<input type="hidden" name="nota">
		</form>
</div>