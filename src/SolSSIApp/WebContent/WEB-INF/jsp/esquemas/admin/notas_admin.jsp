<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>



<script type="text/javascript">

	dojo.require("dijit.Editor");
	dojo.require("dijit._editor.plugins.TextColor");
	dojo.require("dijit._editor.plugins.FontChoice");
	dojo.require("dijit._editor.plugins.ViewSource");
	
	var editor;
		
	dojo.ready(function(){
	    
	        editor = new dijit.Editor({
	            plugins: ["bold","italic","underline","foreColor", "|", "justifyLeft", "justifyCenter", "justifyRight", "justifyFull","|", "fontSize","|","viewsource"],
	            height:"200px"
	        }, "editor");
	        
	        editorRef = new dijit.Editor({
	            plugins: ["bold","italic","underline","foreColor", "|", "fontSize","|","viewsource"],
	            height:"80px"
	        }, "editorRef");
	        
	})


    		
	function update(){
   		document.forms["nota"].action = "<s:url value='/esquemas/admin/notas/update'/>";
   		document.forms["nota"].texto.value = editor.attr("value");
   		document.forms["nota"].referencia.value = editorRef.attr("value");
   		submitForm(document.forms["nota"]);
	}
    
    function save(){
    	document.forms["nota"].action = "<s:url value='/esquemas/admin/notas/save'/>";
    	document.forms["nota"].texto.value = editor.attr("value");
    	document.forms["nota"].referencia.value = editorRef.attr("value");
    	submitForm(document.forms["nota"]);
	}
    
    function cancelar(){
    	document.forms["nota"].action = "<s:url value='/esquemas/admin'/>";
    	submitForm(document.forms["nota"]);
    }
	
</script>

<div class="moduleTitle"><span>Notas de Esquemas</span></div>

 <div id="editDialogEsquema" title="Esquemas" >
  	  	<sf:errors cssClass="error" path="*"/>
		<sf:form modelAttribute="nota" method="post">
			<table class="ui-widget-content ui-corner-all" width="95%" align="center" id="tableEdit">
			      <thead>
			            <tr>
			            	<td colspan="2" align="center"> 
				            	<c:choose>
				      		     	<c:when test="${ACTION eq 'alta'}">
				      		    		<span class="moduleTittle">Alta</span> 	
				      		     	</c:when>
				      		     	<c:otherwise>
										<span class="moduleTittle">Modificar</span>				      		     	
				      		     	</c:otherwise>
				      		     </c:choose>
			      			</td>
			      		<tr>
			      </thead>
			      <tbody>
					
					<tr>
			      		<td align="right">
			      			<label class="tag">Estatus:</label>
			      		</td>
			      		<td align="left">
			      			<sf:select path="activo">
			      				<sf:option value="1" label="ACTIVO"/>
			      				<sf:option value="0" label="INACTIVO"/>
			      			</sf:select>
			      		</td>
					</tr>
					
					<tr>
			      		<td align="right">
			      			<label class="tag">Referencia:</label>
			      		</td>
			      		<td align="left">
			      			<div id="editorRef" style="width: 800px">
								<c:out value="${nota.referencia}" escapeXml="false"/>
							</div>
							<s:hasBindErrors name="nota"><sf:errors path="referencia" cssClass="error"/></s:hasBindErrors>
			      		</td>
					</tr>
					
					<tr>
			      		<td align="right">
			      			<label class="tag">Texto:</label>
			      		</td>
			      		<td align="left">
			      			<div id="editor" style="width: 800px">
								<c:out value="${nota.texto}" escapeXml="false"/>
							</div>
							<s:hasBindErrors name="nota"><sf:errors path="texto" cssClass="error"/></s:hasBindErrors>
			      		</td>
					</tr>	
						
					
			      	<tr>
			      		<td colspan="2" align="center">
			      		 	<c:choose>
			      		     	<c:when test="${ACTION eq 'alta'}">
									<button type="button" class="btnEnviar" onclick="javascript:save()"><span><em>Guardar</em></span></button>
			      		     	</c:when>
			      		     	<c:otherwise>
			      		     		<button type="button" class="btnEnviar" onclick="javascript:update()"><span><em>Guardar</em></span></button>
			      		     	</c:otherwise>
			      		     </c:choose>
			      		     <button type="button" class="btnEnviar" name="cancel" onclick="javascript:cancelar()"><span><em>Cancelar</em></span></button>
			      		</td>
			      	</tr>
			      	
			      </tbody>
			</table>
			
			<c:choose>
     		     	<c:when test="${ACTION eq 'update'}">
     		    		<sf:hidden path="numero"/>
     		     	</c:when>
			</c:choose>
			<sf:hidden path="texto"/>
			<sf:hidden path="referencia"/>
		</sf:form>
		
		
	
</div>	
