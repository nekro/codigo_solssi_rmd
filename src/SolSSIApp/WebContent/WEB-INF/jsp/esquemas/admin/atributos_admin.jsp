<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>



<script type="text/javascript">

    dojo.ready(function(){
        
    })

    		
	function update(){
   		document.forms["atributo"].action = "<s:url value='/esquemas/admin/atributos/update'/>";
   		submitForm(document.forms["atributo"]);
	}
    
    function save(){
    	document.forms["atributo"].action = "<s:url value='/esquemas/admin/atributos/save'/>";
    	submitForm(document.forms["atributo"]);
	}
    
    function cancelar(){
    	document.forms["atributo"].action = "<s:url value='/esquemas/admin'/>";
    	submitForm(document.forms["atributo"]);
    }
	
</script>

<div class="moduleTitle"><span>Administraci&oacute;n de Conceptos</span></div>

 <div id="editDialogEsquema" title="Esquemas" >
  	  	<sf:errors cssClass="error" path="*"/>
		<sf:form modelAttribute="atributo" method="post">
			<table class="ui-widget-content ui-corner-all" width="60%" align="center" id="tableEdit">
			      <thead>
			            <tr>
			            	<td colspan="2" align="center"> 
				            	<c:choose>
				      		     	<c:when test="${ACTION eq 'alta'}">
				      		    		<span class="moduleTittle">Alta</span> 	
				      		     	</c:when>
				      		     	<c:otherwise>
										<span class="moduleTittle">Modificar</span>				      		     	
				      		     	</c:otherwise>
				      		     </c:choose>
			      			</td>
			      		<tr>
			      </thead>
			      <tbody>
			      	<tr>
			      		<td align="right">
			      			<label class="tag">Nombre:</label>
			      		</td>
			      		<td align="left">
			      			<sf:input path="nombre" cssClass="inputContent"  size="60" maxlength="100"/>
							<s:hasBindErrors name="atributo"><br/><sf:errors path="nombre" cssClass="error"/></s:hasBindErrors>
			      		</td>
					</tr>
					
					<tr>
			      		<td align="right">
			      			<label class="tag">Orden:</label>
			      		</td>
			      		<td align="left">
			      			<sf:select path="orden">
			      				<c:forEach begin="1" end="${TOTAL_ATTS}" var="count">
			      					<sf:option value="${count}" label="${count}"/>
			      				</c:forEach>
			      			</sf:select>
			      		</td>
					</tr>
					
					<tr>
			      		<td align="right">
			      			<label class="tag">Estatus:</label>
			      		</td>
			      		<td align="left">
			      			<sf:select path="activo">
			      				<sf:option value="1" label="ACTIVO"/>
			      				<sf:option value="0" label="INACTIVO"/>
			      			</sf:select>
			      		</td>
					</tr>	
						
					
			      	<tr>
			      		<td colspan="2" align="center">
			      		 	<c:choose>
			      		     	<c:when test="${ACTION eq 'alta'}">
									<button type="button" class="btnEnviar" onclick="javascript:save()"><span><em>Guardar</em></span></button>
			      		     	</c:when>
			      		     	<c:otherwise>
			      		     		<button type="button" class="btnEnviar" onclick="javascript:update()"><span><em>Guardar</em></span></button>
			      		     	</c:otherwise>
			      		     </c:choose>
			      		     <button type="button" class="btnEnviar" name="cancel" onclick="javascript:cancelar()"><span><em>Cancelar</em></span></button>
			      		</td>
			      	</tr>
			      	
			      </tbody>
			</table>
			
			<c:choose>
     		     	<c:when test="${ACTION eq 'update'}">
     		    		<sf:hidden path="id"/>
     		     	</c:when>
			</c:choose>
			
		</sf:form>
		
		
	
</div>	
