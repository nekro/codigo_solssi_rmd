<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<style>
<!--
	#adminEsquemas a{
		color: blue !important;
		text-decoration: underline !important;
		font-size: 12px !important;
		font-weight: normal !important;
	}
	
	textarea {
		border: 1px solid #ccc;
	}
-->
</style>

<script type="text/javascript">

	dojo.require("dijit.Editor");
	dojo.require("dijit._editor.plugins.TextColor");
	dojo.require("dijit._editor.plugins.FontChoice");
	dojo.require("dijit._editor.plugins.ViewSource");
	
	<c:forEach items="${esquema.atributos}" var="attr" varStatus="st">
    	var editor${attr.atributo.id};
    </c:forEach>	
	
    dojo.ready(function(){
        
        <c:forEach items="${esquema.atributos}" var="attr" varStatus="st">
	        editor${attr.atributo.id} = new dijit.Editor({
	            plugins: ["bold","italic","underline","foreColor", "|", "justifyLeft", "justifyCenter", "justifyRight", "justifyFull","|", "fontSize","|","viewsource"],
	            height:"80px"
	        }, "editor_${attr.atributo.id}");
		</c:forEach>
		
    })

    		
	function update(){
    	
    	<c:forEach items="${esquema.atributos}" var="attr" varStatus="st">
    		dojo.byId("valorAtributo${attr.atributo.id}").value = editor${attr.atributo.id}.attr("value");
		</c:forEach>
		

		document.forms["esquema"].action = "<s:url value='/esquemas/admin/update'/>";
		submitForm(document.forms["esquema"]);
	}
    
	function save(){
    	
    	<c:forEach items="${esquema.atributos}" var="attr" varStatus="st">
    		dojo.byId("valorAtributo${attr.atributo.id}").value = editor${attr.atributo.id}.attr("value");
		</c:forEach>
		
		document.forms["esquema"].action = "<s:url value='/esquemas/admin/save'/>";
		submitForm(document.forms["esquema"]);
	}
	
	function cancelar(){
    	document.forms["esquema"].action = "<s:url value='/esquemas/admin'/>";
    	submitForm(document.forms["esquema"]);
    }
	
	function formatNumber(num,prefix){
        num = Number(num).toFixed(2);
        if(num == 'NaN'){
        	return 0;
        }
        
        prefix = prefix || '';
	    num += '';
	    var splitStr = num.split('.');
	    var splitLeft = splitStr[0];
	    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
	   
	    return prefix + splitLeft + splitRight;
	}

	
</script>

<div class="moduleTitle"><span>Administraci&oacute;n de Esquemas</span></div>

 <div id="editDialogEsquema" title="Esquemas" style="width: 100%" align="center">
  	  	
		<sf:form modelAttribute="esquema" method="post">
			<table class="ui-widget-content ui-corner-all" width="90%" align="center" id="tableEdit">
			      <thead>
			            <tr class="ui-state-active">
			            	<td colspan="2" align="center"> 
				            	<c:choose>
				      		     	<c:when test="${ACTION eq 'alta'}">
				      		    		<span>Alta</span> 	
				      		     	</c:when>
				      		     	<c:otherwise>
										<span>Modificar</span>				      		     	
				      		     	</c:otherwise>
				      		     </c:choose>
			      			</td>
			      		<tr>
			      </thead>
			      <tbody>
			      	<tr class="ui-state-focus">
						<td colspan="2" align="center">Datos del Esquema</td>
					</tr>
			      	<tr>
			      		<td align="right">
			      			<label class="tag">N&uacute;mero de Esquema:</label>
			      		</td>
			      		<td align="left">
			      		     <c:choose>
			      		     	<c:when test="${ACTION eq 'alta'}">
			      		    		<sf:input path="clave" cssClass="inputContent"  maxlength="2" onkeypress="return enableNumero(event)" size="3"/>
			      		    		<s:hasBindErrors name="esquema"><br/><sf:errors path="clave" cssClass="error"/></s:hasBindErrors>
			      		     	</c:when>
			      		     	<c:otherwise>
									<span class="infoContent">${esquema.clave}</span>
			      					<sf:hidden path="clave"/>	
			      		     	</c:otherwise>
			      		     </c:choose>
			      		</td>
			      	</tr>
			      	<tr>
			      		<td align="right">
			      			<label class="tag">Descripci&oacute;n:</label>
			      		</td>
			      		<td align="left">
			      			<sf:input path="nombre" cssClass="inputContent"  size="80" maxlength="100"/>
							<s:hasBindErrors name="esquema"><br/><sf:errors path="nombre" cssClass="error"/></s:hasBindErrors>
			      		</td>
					</tr>
					
					<tr>
			      		<td align="right">
			      			<label class="tag">P&oacute;liza:</label>
			      		</td>
			      		<td align="left">
			      			<sf:input path="poliza" cssClass="inputContent"  size="40" maxlength="200"/>
							<s:hasBindErrors name="esquema"><br/><sf:errors path="poliza" cssClass="error"/></s:hasBindErrors>
			      		</td>
					</tr>
					
					<tr>
			      		<td align="right">
			      			<label class="tag">Orden:</label>
			      		</td>
			      		<td align="left">
			      			<sf:select path="orden">
			      				<c:forEach begin="1" end="${esquemasInfo.totalEsquemas + 1}" var="count">
			      					<sf:option value="${count}" label="${count}"/>
			      				</c:forEach>
			      			</sf:select>
			      		</td>
					</tr>
					
					
					<tr>
			      		<td align="right">
			      			<label class="tag">Estatus:</label>
			      		</td>
			      		<td align="left">
			      			<sf:select path="activo">
			      				<sf:option value="1" label="ACTIVO"/>
			      				<sf:option value="0" label="INACTIVO"/>
			      			</sf:select>
			      		</td>
					</tr>
					
					
					<tr>
			      		<td align="right">
			      			<label class="tag">Tasa t&eacute;cnica m&iacute;nima de reserva garantizada:</label>
			      		</td>
			      		<td align="left">
			      			<sf:input path="tasaTecnicaMRG" cssClass="inputContent"  size="50" maxlength="100"/>
							<s:hasBindErrors name="esquema"><br/><sf:errors path="tasaTecnicaMRG" cssClass="error"/></s:hasBindErrors>
			      		</td>
					</tr>
					
					<tr>
			      		<td align="right">
			      			<label class="tag">Tasa de rendimiento de reserva garantizada:</label>
			      		</td>
			      		<td align="left">
			      			<sf:input path="tasaRMG" cssClass="inputContent"  size="50" maxlength="100"/>
							<s:hasBindErrors name="esquema"><br/><sf:errors path="tasaRMG" cssClass="error"/></s:hasBindErrors>
			      		</td>
					</tr>
					
					
					<tr>
			      		<td align="right">
			      			<label class="tag">Tasa de rendimiento de reserva excedente:</label>
			      		</td>
			      		<td align="left">
			      			<sf:input path="tasaRME" cssClass="inputContent"  size="50" maxlength="100"/>
							<s:hasBindErrors name="esquema"><br/><sf:errors path="tasaRME" cssClass="error"/></s:hasBindErrors>
			      		</td>
					</tr>
					
					<tr><td>&nbsp;</td></tr>
					<tr>
			      		<td align="left">
			      			<label class="tag">Texto Clausula 4 del Documento de Selecci&oacute;n:</label>
			      		</td>
			      		<td>&nbsp;</td>
					</tr>
					
					<tr>
						<td colspan="2" align="center">
			      			<sf:textarea path="textoClausula4" cssClass="inputContent" cols="100" rows="10"/>
							<s:hasBindErrors name="esquema"><br/><sf:errors path="textoClausula4" cssClass="error"/></s:hasBindErrors>
						</td>
					</tr>
					
					<tr>
			      		<td align="left">
			      			<label class="tag">Texto Clausula 6 del Documento de Selecci&oacute;n:</label>
			      		</td>
			      		<td>&nbsp;</td>
					</tr>
					
					<tr>
						<td colspan="2" align="center">
							<sf:textarea path="textoClausula6" cssClass="inputContent" cols="100" rows="10"/>
							<s:hasBindErrors name="esquema"><br/><sf:errors path="textoClausula6" cssClass="error"/></s:hasBindErrors>
						</td>
					</tr>
					
					
					<tr>
			      		<td align="left">
			      			<label class="tag">Mensaje Selecci&oacute;n:</label>
			      		</td>
			      		<td align="left">
			      			&nbsp;
			      		</td>
					</tr>
					
					<tr>
						<td colspan="2" align="center">
							<sf:textarea path="textoSeleccion" cssClass="inputContent" cols="100" rows="3"/>
							<s:hasBindErrors name="esquema"><br/><sf:errors path="textoSeleccion" cssClass="error"/></s:hasBindErrors>
						</td>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
					</tr>	
					
					<tr class="ui-state-focus">
						<td colspan="2" align="center">Valores pantalla WEB (tabla de esquemas)</td>
					</tr>
					
      		    		<c:forEach items="${esquema.atributos}" var="attr" varStatus="st">
							<tr>
								<td align="left" valign="top">${attr.atributo.nombre}:</td>
								<td align="left">
									&nbsp;
								</td>
							</tr>
							<tr>
								<td	colspan="2" align="center">
									<div id="editor_${attr.atributo.id}" style="width: 600px">
										<c:out value="${attr.valor}" escapeXml="false"/>
									</div>
									<sf:hidden path="atributos[${st.index}].pk.atributo.nombre"/>
									<sf:hidden path="atributos[${st.index}].pk.atributo.id"/>
									<sf:hidden path="atributos[${st.index}].valor" id="valorAtributo${attr.atributo.id}"/>
									<s:hasBindErrors name="esquema"><sf:errors path="atributos[${st.index}]" cssClass="error"/></s:hasBindErrors>
								</td>
							</tr>
						</c:forEach>	
					
			      	<tr>
			      		<td colspan="2" align="center">
			      			<br/><br/>
			      		 	<c:choose>
			      		     	<c:when test="${ACTION eq 'alta'}">
									<button type="button" class="btnEnviar" onclick="javascript:save()"><span><em>Guardar</em></span></button>
			      		     	</c:when>
			      		     	<c:otherwise>
			      		     		<button type="button" class="btnEnviar" onclick="javascript:update()"><span><em>Guardar</em></span></button>
			      		     	</c:otherwise>
			      		     </c:choose>
			      		     <button type="button" class="btnEnviar" name="cancel" onclick="javascript:cancelar()"><span><em>Cancelar</em></span></button>
			      		</td>
			      	</tr>
			      	
			      </tbody>
			</table>
		</sf:form>
		
		
	
</div>	
