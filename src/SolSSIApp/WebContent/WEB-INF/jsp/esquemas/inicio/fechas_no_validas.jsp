<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ page isErrorPage="true" import="java.io.*" %>


<div align="center">

	<form id="form" name="form" method="post">
	</form>

  <br/><br/>
  <div class="ui-widget-content ui-corner-all" align="center" style="width: 90%">

   <table align="center">
      <tr>
         <td colspan="5" align="center">
         	<img src="<s:url value='/resources/images/warning_blue.png'/>" width="55px" height="55px" ondblclick="displayDetalle()">
         </td>
      </tr>

   </table>
   
   <table align="center" width="60%" >
      
	      <tr>
			  <td id = "error" align="center" width="60%" style="font-family:Arial; font-size:11pt;color: red" ondblclick="displayDetalle()">
			  	  <c:out value="${error}"/>
		      </td>
	      </tr>
         
       </table>
   </div>
</div>

