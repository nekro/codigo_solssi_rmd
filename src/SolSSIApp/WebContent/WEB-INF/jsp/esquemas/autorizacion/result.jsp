<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
		
		function salir(){
			document.forms["form"].action = "<s:url value='/esquemas/autorizacion'/>";
			document.forms["form"].submit();
		}
		
</script>

<br/>
<div align="center" class="ui-widget-content ui-corner-all" style="width: 75%">
		<div>
			<c:choose>
				<c:when test="${empty error and autorizado eq 'true'}">
					<div><img src="<s:url value='/resources/images/sucess.png'/>" width="60px" height="60px"></div>
					<span><s:message code="auth.result.satisfactorio.esquemas" htmlEscape="true" arguments="${folio}"/></span>
				</c:when>
				<c:when test="${empty error and rechazado eq 'true'}">
					<div><img src="<s:url value='/resources/images/rechazar32.png'/>" width="40px" height="40px"></div>
						<span><s:message code="auth.result.nosatisfactorio.esquemas" htmlEscape="true" arguments="${folio}"/></span>
					</c:when>
				<c:otherwise>
					<div><img src="<s:url value='/resources/images/warning_blue.png'/>"></div>
					<span>Error al ejecutar la operaci&oacute;n</span>
				</c:otherwise>
			</c:choose>
		</div>
			<div id="containerInicio" align="center">
				<br/>
			    <form name="form" method="post">
					<table style="width: 100%" align="center">
					    <c:if test="${not empty error}">
						<tr>
							<td align="center">
								<span class="leyenda">
								    <c:out value="${error}"/>
								</span>
							</td>
						</tr>
						</c:if>
						<tr>
							<td align="center">
								<button type="button" class="btnEnviar" onclick="salir()"><span><em>Salir</em></span></button>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
					</table>
				</form>
			</div>	
</div>
