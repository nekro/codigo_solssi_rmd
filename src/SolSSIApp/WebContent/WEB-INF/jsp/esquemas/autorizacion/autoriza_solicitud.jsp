
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>



<s:url value="/esquemas/autorizacion" var="urlConsulta"/>
<s:url value="/esquemas/autorizacion/execute/autorizacion" var="urlAutoriza"/>
<s:url value="/esquemas/autorizacion/execute/rechazo" var="urlRechazo"/>

<style>
	.buttonDlg{
		background-color: #f5f5f5;
		color: black;
		border: 1px solid gray;
	}
</style>
		
<script>
		 
		var checkListDialog;
		var confirmDialog;
		var confirmRechazoDialog;
		var checkItem;
		dojo.require("dijit.Dialog");
		
		dojo.ready(function(){
			  
			checkListDialog = new dijit.Dialog({ title:"<s:message code='dialog.checklist.title' htmlEscape='true'/>", style: "width: 875px;text-align:center" }, "divCheckList");
			dojo.style(dijit.byId("divCheckList").closeButtonNode,"display","none");
  			checkListDialog.startup();
  			
  			
  			confirmDialog = new dijit.Dialog({ title:"", style: "width: 700px;text-align:center" }, "divConfirm");
  			dojo.style(dijit.byId("divConfirm").closeButtonNode,"display","none");
  			confirmDialog.startup();
  			
  			confirmRechazoDialog = new dijit.Dialog({ title:"Mensaje", style: "width: 500px;text-align:center" }, "divRechazar");
  			dojo.style(dijit.byId("divRechazar").closeButtonNode,"display","none");
  			confirmRechazoDialog.startup();
  			
			
		});
		
		function showConfirmDialog(){
			confirmDialog.show();
		}
		
		function confirmRechazar(){
			document.forms["solicitud"].action = "${urlRechazo}";
			document.forms["solicitud"].target = "_top";
			submitForm(document.forms["solicitud"]);
		}
		
		function cancelRechazar(){
			confirmDialog.hide();
		}
		 
		 
		 function regresarConsulta(){
			 history.back();
		 }
		 
		 function checkListDialogOpen(){
	  	 	 for(var i = 0; i < document.forms["checkListForm"].clItem.length; i++){
		 	 	document.forms["checkListForm"].clItem[i].checked = false;
		 	 }
		 	 try{
		 	 		dojo.byId("checkListClabeError").style.display = 'none';
		 	 	}catch(e){
		 	 };
		 	 checkListDialog.show();
		 }
		 
		 function checkListDialogClose(){
		 	 checkListDialog.hide();
		 }
		 
		 function autorizar(){
			 
			 for(var i = 0; i < document.forms["checkListForm"].clItem.length; i++){
		 	 	if(!document.forms["checkListForm"].clItem[i].checked){
		 	 		showConfirmDialog();
		 	 		return;		
		 	 	}
		 	 }
		 	 
		 	<c:if test="${CATALOGO.PARAMETROS_NEGOCIO['VAL_NP_ESQ'].descripcion ne '1' and empty solicitud.nivelPuesto}">
			 	if(dojo.byId("selectNivelPuesto").value == ''){
			 		dojo.byId('nivelPuestoError').style.display = '';
			 		return;
			 	}
			 	document.forms["solicitud"].nivelPuesto.value = dojo.byId("selectNivelPuesto").value;
		 	 </c:if>
			 
			 document.forms["solicitud"].action = "${urlAutoriza}";
			 document.forms["solicitud"].target = "_top";
			 submitForm(document.forms["solicitud"]);
		 }

		 
		 
</script>

<div class="moduleTitle"><span><s:message code="module.title.solicitud.esquemas.autorizacion" htmlEscape="true"/></span></div>

<jsp:include page="../consulta/datos_solicitud.jsp"/>	

<br/><br/>
<div>
	<button type="button" class="btnEnviar" onclick="regresarConsulta()"><span><em><s:message code="button.cancelar" htmlEscape="true"/></em></span></button>
	<c:if test="${solicitud.estatus == 1}">
		<button type="button" class="btnEnviar" onclick="checkListDialogOpen()"><span><em><s:message code="button.autorizar" htmlEscape="true"/></em></span></button>
	</c:if>
</div>
<br/><br/>		

<jsp:include page="checklist.jsp"/>
