<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<div id="divCheckList" style="min-height: 500px;">
	<div align="center" style="width:100%">
		<div align="left" style="width: 100%">
			<form name="checkListForm">
				<table cellspacing="7" cellpadding="7">
					
					<tr>
						<td colspan="2"></td>
					</tr>
					
					<tr>
						<c:set var="st" value="1"/>
						<td align="left">
							<input type="checkbox" name="clItem" id="clItem${st}">
						</td>
						<td align="left">
							<label for="clItem${st}"><c:out value="${CATALOGO.CHECK_ESQUEMAS['1'].descripcionOrigen}"/></label>
						</td>
					</tr>
					<tr>
						<c:set var="st" value="${st+1}"/>
						<td align="left">
							<input type="checkbox" name="clItem" id="clItem${st}">
						</td>
						<td align="left">
							<label for="clItem${st}"><c:out value="${CATALOGO.CHECK_ESQUEMAS['2'].descripcionOrigen}"/></label>
						</td>
					</tr>
					
					<tr>
						<c:set var="st" value="${st+1}"/>
						<td align="left">
							<input type="checkbox" name="clItem" id="clItem${st}">
						</td>
						<td align="left">
							<label for="clItem${st}">
								<c:out value="${CATALOGO.CHECK_ESQUEMAS['3'].descripcionOrigen}"/>&nbsp;
								<c:out value="${solicitud.esquema}"/>&nbsp;
								<c:out value="${esquemasInfo.esquemas[solicitud.esquema].nombre}"/>
							</label>
						</td>
					</tr>
					
					
					<c:if test="${CATALOGO.PARAMETROS_NEGOCIO['VAL_NP_ESQ'].descripcion ne '1' and empty solicitud.nivelPuesto}">
						<tr>
							<c:set var="st" value="${st+1}"/>
							<td align="left">
								
							</td>
							<td align="left">
								<label for="clItem${st}">
									<b>El nivel de puesto del asegurado es </b> 
								</label>
								<select style="border:1px solid gray" id="selectNivelPuesto" onchange="dojo.byId('nivelPuestoError').style.display = 'none'">
										<option value="">Seleccione una opci&oacute;n</option>
										<c:forEach items="${CATALOGO.NIVEL_PUESTO}" var="cat">
											<option value="${cat.key}">${cat.key}</option>
										</c:forEach>
								</select>
								<div align="center" style="height: 30px">
									<span class="error" style="display: none" id="nivelPuestoError">Debe Seleccionar un Nivel de Puesto para continuar</span>
								</div>
							</td>
						</tr>
					</c:if>
					
											
				</table>
			</form>	
			<div align="center">	
				<button type="button" class="btnEnviar" onclick="checkListDialogClose()"><span><em>Cancelar</em></span></button>
				<button type="button" class="btnEnviar" onclick="autorizar()"><span><em>Enviar</em></span></button>
			</div>
		</div>
	</div>
</div>

<div id="divConfirm">
	<div align="center" style="width:100%">
	    <table>
	    	<tr>
	    		<td valign="top">
	    			<img src="<s:url value='/resources/images/info.gif'/>"/>
	    		</td>
	    		<td>
	    			<span id="textConfirm" style="color:black;font-size:14px">
						<s:message code="documentacion.auth.incompleta.esquemas" htmlEscape="true"/>
					</span>
	    		</td>
	    		
	    	</tr>
	    </table>
		
		<div>
			<br/>
			<input type="button" value="Cancelar" onclick="cancelRechazar()" class="buttonDlg" style="width: 60px"/>
			<input type="button" value="Aceptar" onclick="confirmRechazar()" class="buttonDlg" style="width: 60px"/>
			
		</div>
	</div>
</div>


<div id="divRechazar">
	<div align="center" style="width:100%">
		<div style="position: absolute;top: 5px;left: 15px">
			<img src="<s:url value='/resources/images/info.gif'/>"/>
		</div>
		<div>
			<span id="textConfirm" style="color:black;font-size:14px">
				<s:message code="confirm.rechazo.solicitud" htmlEscape="true"/>
			</span>
		</div>
		<div>
			<br/>
			<input type="button" value="No" onclick="cancelRechazar()" class="buttonDlg" style="width: 60px"/>
			<input type="button" value="Si" onclick="confirmRechazar()" class="buttonDlg" style="width: 60px"/>
		</div>
	</div>
</div>



