<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<style>
	#filtro li {
		float: left;
		margin: 0px 20px 0px 0px;
		list-style-type: none;
	}

	#tableReporte td{
		border: 1px solid #ccc;
		color: #333;
	}
	
	#tableReporte th{
		border: 1px solid #ccc;
	}

</style>


<script type="text/javascript">

	dojo.require("dijit.Calendar");
	dojo.require("dijit.Dialog");

	var calendarIniDlg = null;
	
	dojo.ready(function(){
		calendarIniDlg = new dijit.Dialog({ title:"Seleccione la Fecha"}, "calendarIniDlg");
	  	calendarIniDlg.startup();
	  	
	  	
	  	<c:if test="${CCFilter.criterio > 0}">
	  		selectCriterio(<c:out value='${CCFilter.criterio}'/>);
	  	</c:if>
	});

	function selectCriterio(criterio){
		
		dojo.byId("criterio1").style.display = 'none';
		dojo.byId("criterio2").style.display = 'none';
		dojo.byId("criterio3").style.display = 'none';
		dojo.byId("criterioGrupo").style.display = '';
		
		if(criterio == 2 || criterio == 3){
			dojo.byId("criterioGrupo").style.display = 'none';
		}
		
		dojo.byId("criterio"+criterio).style.display = '';
	}
	
	function showCalendarIni(){
		
		calendarIniDlg.show();
		
		var calIni = new dijit.Calendar(new Date(), dojo.byId("calFechaInicio"));
	    dojo.connect(calIni, "onValueSelected", function(date){
	    	var fmt = dojo.date.locale.format(date, {datePattern: "dd/MM/yyyy", selector: "date"});
	    	dojo.byId("fecha").value = fmt;
	    	calendarIniDlg.hide();
	    });
	}
	
	function cerrarIni(){
		calendarIniDlg.hide();
	}
	
	function consultar(){
		document.forms["filter"].action = "<s:url value='/cifrascontrol/get'/>";
		submitForm(document.forms["filter"]);
	}
	
	function download(){
		document.forms["download"].submit();
		
	}

</script>
		<c:choose>
			<c:when test="${action eq 'filter'}">
				<div><h4>Cifras de Control</h4></div>
				<div id="filtro" class="ui-state-default ui-corner-all" style="width: 95%">
		   			<sf:form modelAttribute="CCFilter" name="filter" method="post">
						<table>
							<tr>	
								<td align="left">
									<ul>
										<li style="width: 230px">
											<span>Criterio:</span>
											<sf:select path="criterio" onchange="selectCriterio(this.value)">
												<sf:option value="1" label="DEPENDENCIA"/>
												<sf:option value="2" label="PROCESO (ID WEB)"/>
												<sf:option value="3" label="FECHA"/>
												<sf:option value="4" label="GRUPO"/>
											</sf:select>
										</li>
										<li style="width: 100px" id="criterioGrupo">
											<span>Grupo:</span>
											<sf:select path="grupo">
												<sf:option value="1" label="1"/>
												<sf:option value="2" label="2"/>
												<sf:option value="3" label="3"/>
												<sf:option value="4" label="4"/>
												<sf:option value="5" label="5"/>
												<sf:option value="6" label="6"/>
											</sf:select>
										</li>
										
										
										<li id="criterio1" style="width: 300px">
											<span>Dependencia:</span> 
											<sf:select path="dependencia" cssStyle="width:200px" htmlEscape="true">
												<sf:options items="${CATALOGO.DEPENDENCIAS}" itemLabel="descDependencia"/>
											</sf:select>
										</li>
										<li id="criterio2" style="display: none;width: 300px">
											<span>ID:</span> 
											<sf:input path="proceso"/>
										</li>
										<li id="criterio3" style="display: none;width: 300px">
											<span>Fecha:</span> 
											<sf:input path="fecha" onclick="showCalendarIni()"/>
										</li>
										<li>
											<button class="btnEnviar" onclick="consultar()"><span><em>Consultar</em></span></button>
										</li>
									</ul>
								</td>
							</tr>
						</table>			
					</sf:form>
				</div>
				
				<c:if test="${not empty error}">
					<div>
						<span class="error"><c:out value="${error}"/></span>
					</div>
				</c:if>
				
				<c:if test="${not empty cifras}">
					<br/>
					<br/>
					<div align="right" style="width: 90%"><a href="javascript:download()" style="text-decoration: underline;color: blue;">Descargar Reporte</a></div>
					<div><h4>Total</h4></div>
					<table class="display" id="tableReporte" style="width:90%">
						<thead class="ui-state-default">
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>Total Tr&aacute;mites</th>
							<th>Suma de Porcentajes</th>
							<th>Suma de Folios</th>
							<th>Clabes Bancarias</th>
							<th>Suma de Monto</th>
						</thead>
						<c:forEach items="${cifras.detalle}" var="cc" varStatus="st">
						
								<c:choose>
										<c:when test="${CCFilter.criterio ne 4 or (not (CCFilter.criterio ne 4 or cc.sistema ne 1))}">
										
											<c:if test="${cc.tipo eq 3}">
												
												<tr style="background-color: #DDFFBB">
													<td style="font-weight: bold;">
														DIFERENCIA
													</td>
													<td style="font-weight: bold;">
														Pendientes
													</td>
													<td align="right">${cifras.diferenciaWEBSSI.totalAceptadas}</td>
													<td align="right">${cifras.diferenciaWEBSSI.sumaPorcentajes}</td>
													<td align="right">${cifras.diferenciaWEBSSI.sumaFolios}</td>
													<td align="right">${cifras.diferenciaWEBSSI.sumaClabes}</td>
													<td align="right">${group.diferenciaWEBSSI.fmtSumaImportes}</td>
												</tr>
														
											</c:if>
											
											<tr>
												<td style="font-weight: bold;">
													<c:choose>
														<c:when test="${cc.sistema eq 1}">SSI WEB</c:when>
														<c:when test="${cc.sistema eq 2}">SSI</c:when>
														<c:when test="${cc.sistema eq 3}">TESORERIA</c:when>
													</c:choose>
												</td>
												<td style="font-weight: bold;">
													<c:choose>
														<c:when test="${cc.tipo eq 1}">Tr&aacute;mites Satisfactorios</c:when>
														<c:when test="${cc.tipo eq 2}">Servicios Operados</c:when>
														<c:when test="${cc.tipo eq 3}">Rechazos</c:when>
													</c:choose>
												</td>
												<td align="right">${cc.totalAceptadas}</td>
												<td align="right">${cc.sumaPorcentajes}</td>
												<td align="right">${cc.sumaFolios}</td>
												<td align="right">${cc.sumaClabes}</td>
												<td align="right">${cc.fmtSumaImportes}</td>
											</tr>	
										</c:when>
								</c:choose>
								
						</c:forEach>
						
						<c:choose>
									<c:when test="${CCFilter.criterio eq 4}">
									</c:when>
									<c:otherwise>
										<tr>
											<td align="right">&nbsp;</td>
											<td align="right" style="font-weight: bold;">TOTAL</td>
											<td align="right">${cifras.totalSSI.totalAceptadas}</td>
											<td align="right">${cifras.totalSSI.sumaPorcentajes}</td>
											<td align="right">${cifras.totalSSI.sumaFolios}</td>
											<td align="right">${cifras.totalSSI.sumaClabes}</td>
											<td align="right">${cifras.totalSSI.fmtSumaImportes}</td>
										</tr>
										
										<tr class="ui-state-error">
											<td align="right">&nbsp;</td>
											<td align="right" style="font-weight: bold;">COMPROBACI&Oacute;N</td>
											<td align="right">${cifras.comprobacionSSI.totalAceptadas}</td>
											<td align="right">${cifras.comprobacionSSI.sumaPorcentajes}</td>
											<td align="right">${cifras.comprobacionSSI.sumaFolios}</td>
											<td align="right">${cifras.comprobacionSSI.sumaClabes}</td>
											<td align="right">${cifras.comprobacionSSI.fmtSumaImportes}</td>
										</tr>
									</c:otherwise>
							</c:choose>
						
					</table>
				</c:if>
			</c:when>
		</c:choose>
		
		<br/>
		<br/>		
		<c:if test="${not empty detalle && CCFilter.criterio ne 1}">
			
			<div><h4>Detalle</h4></div>
			
			<c:forEach items="${detalle}" var="group" varStatus="st">
					<c:set var="cveDependencia"><c:out value="${group.identificador}"/></c:set>
					<br/>		
					<div><span>${CATALOGO.DEPENDENCIAS[cveDependencia].descDependencia}</span></div>
					
					<table class="display" id="tableReporte" style="width:90%">
						<thead class="ui-state-default">
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>Total Tr&aacute;mites</th>
							<th>Suma de Porcentajes</th>
							<th>Suma de Folios</th>
							<th>Clabes Bancarias</th>
							<c:choose>
								<c:when test="${CCFilter.criterio eq 4}">
									<th>ID WEB</th>
								</c:when>
								<c:otherwise>
									<th>Suma de Monto</th>								
								</c:otherwise>
							</c:choose>
							
						</thead>
						
						<c:forEach items="${group.detalle}" var="cc" varStatus="st">
							<c:choose>
								<c:when test="${CCFilter.criterio ne 4 or (not (CCFilter.criterio ne 4 or cc.sistema ne 1))}">
								
										<c:if test="${cc.tipo eq 3}">
												
												<tr style="background-color: #DDFFBB">
													<td style="font-weight: bold;">
														DIFERENCIA
													</td>
													<td style="font-weight: bold;">
														Pendientes
													</td>
													<td align="right">${group.diferenciaWEBSSI.totalAceptadas}</td>
													<td align="right">${group.diferenciaWEBSSI.sumaPorcentajes}</td>
													<td align="right">${group.diferenciaWEBSSI.sumaFolios}</td>
													<td align="right">${group.diferenciaWEBSSI.sumaClabes}</td>
													<td align="right">${group.diferenciaWEBSSI.fmtSumaImportes}</td>
												</tr>
											
										</c:if>
													
										<tr>
											<td style="font-weight: bold;">
												<c:choose>
													<c:when test="${cc.sistema eq 1}">SSI WEB</c:when>
													<c:when test="${cc.sistema eq 2}">SSI</c:when>
													<c:when test="${cc.sistema eq 3}">TESORERIA</c:when>
												</c:choose>
											</td>
											<td style="font-weight: bold;">
												<c:choose>
													<c:when test="${cc.tipo eq 1}">Tr&aacute;mites Satisfactorios</c:when>
													<c:when test="${cc.tipo eq 2}">Servicios Operados</c:when>
													<c:when test="${cc.tipo eq 3}">Rechazos</c:when>
												</c:choose>
											</td>
											<td align="right">${cc.totalAceptadas}</td>
											<td align="right">${cc.sumaPorcentajes}</td>
											<td align="right">${cc.sumaFolios}</td>
											<td align="right">${cc.sumaClabes}</td>
											
											<c:choose>
												<c:when test="${CCFilter.criterio eq 4}">
													<td align="right">${cc.proceso}</td>
												</c:when>
												<c:otherwise>
													<td align="right">${cc.fmtSumaImportes}</td>								
												</c:otherwise>
											</c:choose>
										</tr>
								</c:when>	
							</c:choose>	
						</c:forEach>
						
						<c:choose>
							<c:when test="${CCFilter.criterio eq 4}">
								
							</c:when>
							<c:otherwise>
								<tr>
									<td align="right">&nbsp;</td>
									<td align="right" style="font-weight: bold;">TOTAL</td>
									<td align="right">${group.totalSSI.totalAceptadas}</td>
									<td align="right">${group.totalSSI.sumaPorcentajes}</td>
									<td align="right">${group.totalSSI.sumaFolios}</td>
									<td align="right">${group.totalSSI.sumaClabes}</td>
									<td align="right">${group.totalSSI.fmtSumaImportes}</td>
								</tr>
								
								<tr class="ui-state-error">
									<td align="right">&nbsp;</td>
									<td align="right" style="font-weight: bold;">COMPROBACI&Oacute;N</td>
									<td align="right">${group.comprobacionSSI.totalAceptadas}</td>
									<td align="right">${group.comprobacionSSI.sumaPorcentajes}</td>
									<td align="right">${group.comprobacionSSI.sumaFolios}</td>
									<td align="right">${group.comprobacionSSI.sumaClabes}</td>
									<td align="right">${group.comprobacionSSI.fmtSumaImportes}</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</table>
				</c:forEach>
				<br/><br/>
				<div align="right" style="width: 90%"><a href="javascript:download()" style="text-decoration: underline;color: blue;">Descargar Reporte</a></div>
		</c:if>
		
		<br/>
		
		<div id="calendarIniDlg" align="center">
			<div id="calFechaInicio"></div>
			<a href="javascript:cerrarIni()">Cerrar</a>
		</div>
		
		
		<form method="post" name="download" action='<s:url value="/cifrascontrol/download"/>'>
		</form>