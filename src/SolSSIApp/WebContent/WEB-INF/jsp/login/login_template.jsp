
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="es">
<!-- head: Start -->
<head>
<!-- T�tulo de la Ventana -->
<title>MetLife | Inicio</title>
<!-- Icono -->
<link rel="shortcut icon" href="<s:url value='/resources/images/favicon.ico'/>" type="image/x-icon">
<!-- Meta -->

<!-- Estilos y Scripts -->
<link href="<s:url value="/resources/css/metlife/screen.css" />" rel="stylesheet" type="text/css" />
<link href="<s:url value="/resources/css/ui/estilos_ui.css" />" rel="stylesheet" type="text/css" />
<link href="<s:url value="/resources/css/solssi.css" />" rel="stylesheet" type="text/css" />

<!--[if lt IE 7]>
   		<style type="text/css">@import url(/wps/themes/html/seguros/css/IEbugs.css);</style>
    <![endif]-->


<script type="text/javascript"	src="<s:url value="/resources/js/ssi_library_1.0.0.js"/>"></script>
<script type="text/javascript" src="<s:url value="/resources/js/dojo/dojo.xd.js"/>"></script>


</head>
<!-- Head: Fin -->
<!-- Body: Inicio -->

<body>
	<!-- Contenedor: Inicio -->
	<div id="contenedor">
		<!-- #Encabezado: Inicio -->
		<div id="encabezado">
			<!-- .Encabezado: Inicio -->
				<t:insertAttribute name="header" />
			<!-- #Encabezado: Fin -->
			
			<!-- #Contenido: Inicio -->
			<div id="contenido" class="servicioCliente">
				<!-- .Contenido: Inicio -->
				<div class="contenido">

					<div style="width: 100%;min-height: 475px" align="center" >
						<div class="ui-widget-content ui-corner-all" align="center" style="width: 98%;height: 98%">
							<t:insertAttribute name="content" />
						</div>
					</div>
					<!-- Portlets: Fin -->
				</div>
				<!-- Contenedor P�gina Inicial: Fin -->
			</div>
			<!-- .Contenido: Fin -->
		</div>
		<!-- #Contenido: Fin -->
	</div>
	<!-- Contenedor: Fin -->
	<!-- .Pie: Inicio -->
	<div class="pie">
		
		<t:insertAttribute name="footer" />
		
	</div>


	<!-- .Pie: Fin -->
</body>
<!-- Body: Fin -->
</html>
