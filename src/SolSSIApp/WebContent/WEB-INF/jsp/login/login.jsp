

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>    
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<s:url var="authUrl" value="/j_spring_security_check" />
          
<link href="<s:url value="/resources/css/login.css" />" rel="stylesheet"  type="text/css" />
    
    <div style="width: 97%;min-height: 470px" align="center">
				<br/>
				<br/>
				<br/>
				<div class="ui-widget-content ui-corner-all" style="width: 356px;height: 327px">
				<sf:form method="POST" action="${authUrl}"  modelAttribute="usuario" name="login" autocomplete="off" cssClass="login" >
						<!-- <div class="formLoginHeader">
							<span style="text-align:center">Ingresar</span>
						</div>  -->
						
						    <table>
						    	<tr>
						    	     <td colspan="6" align="center" class="formLoginHeader">
						    	     SSI (Seguro de Separaci&oacute;n Individualizado)
						    	     <br/>
						    	     </td>
						    	     
						    	</tr>
						    	<tr>
							    	<td class="tag">
							    	    <br/>
							    	    <label for="usuario"><strong>Usuario:</strong></label>
									</td>
									<td class="control">
										 <br/>
										 <input id="usuario" type="text" name="j_username" maxlength="10" style="width:90px">
									     
									</td>
									<td>
									   &nbsp;
									</td>
									<td>
									   &nbsp;
									</td>
									<td>
									   &nbsp;
									</td>
									<td>
									   &nbsp;
									</td>
							    </tr>
								<tr>
								     <td class="tag">    		
								         <label for="clave"><strong>Contrase&ntilde;a:</strong></label>
								      </td>
								      <td class="control">
									     <input id="clave" type="password" name="j_password" maxlength="10" style="width:90px">
								      </td>
								      <td>
									   &nbsp;
									</td>
									<td>
									   &nbsp;
									</td>
									<td>
									   &nbsp;
									</td>
									<td>
									   &nbsp;
									</td>
							   	</tr>
						    	<tr>
						    		<td colspan="6" align="center">
						    			<br/>
						    			<button type="submit" class="btnLogin" value="Ingresar">
											<span><em>Aceptar</em></span>
										</button>
										<button type="button" class="btnLogin" value="Ingresar" onclick="window.close()">
											<span><em>Salir</em></span>
										</button>
						    		</td>
						    	</tr>
						    	
							    <tr>
							    	<td colspan="6" align="center" style="height: 18px">
							    		<span class="error">${SPRING_SECURITY_LAST_EXCEPTION.message}</span>
							    		<c:if test="${MAX_SESSION_EXCEEDED}">
							    			<span class="error"><s:message code="session.max.exceeded"/></span>
							    		</c:if>
							    	</td>
							    </tr>
						    </table>
						    
						    <div>
						    		
								<div style="padding: 10px 5px 15px 5px;">
										Si tienes problemas con tu Contrase&ntilde;a
										<div>
											<a href="<c:out value='${URL_OLVIDO_PASSWORD}'/>" target="_blank">
												haz click aqu&iacute;.
											</a>		
										</div>
								</div>
								
								<div style="padding: 5px 5px 15px 5px;">
										Si deseas cambiar tu Contrase&ntilde;a
										<div>
											<a href="<c:out value='${URL_CAMBIO_PASSWORD}'/>" target="_blank">
												haz click aqu&iacute;.
											</a>		
										</div>
								</div>
								
								<br/>
								<br/>
						    </div>
						    
						
				</sf:form>
				</div>
		</div>
