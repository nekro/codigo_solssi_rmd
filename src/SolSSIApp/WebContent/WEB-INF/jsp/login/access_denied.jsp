<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<%@ page isErrorPage="true" import="java.io.*" %>

<div align="center">

  <br/><br/>
  <div class="ui-widget-content ui-corner-all" align="center" style="width: 90%">

   <table align="center">
      <tr>
         <td colspan="5" align="center">
         	<img src="<s:url value='/resources/images/warning_blue.png'/>"/>
         </td>
      </tr>

      <tr>
         <td colspan="5" align="center"><h3><s:message code="error.app.general"/></h3></td>
      </tr>
      
   </table>
   
   <table align="center" width="90%" >
      
      <tr>
		  <td id = "error" align="center" width="100%" ondblclick="displayDetalle()">
		  	  <h2><s:message code="access.denied"/></h2>
	      </td>
      </tr>
       </table>
   </div>
</div>

