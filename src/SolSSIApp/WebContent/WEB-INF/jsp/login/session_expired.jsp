<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<%@ page isErrorPage="true" import="java.io.*" %>

<link href="<s:url value="/resources/css/solssi.css" />" rel="stylesheet"  type="text/css" />
	<script>
		
		function iniciarSesion(){
			<c:choose>
		        	<c:when test="${concurrent eq 'true'}">
						document.location.href = '<s:url value="/operacion"/>';		        	
		        	</c:when>
		        	<c:otherwise>
		        		document.location.href = '<s:url value="/clientes"/>';
		        	</c:otherwise>
		     </c:choose>
		}
		
	</script>


<div align="center">

  <br/><br/>
  <div align="center" style="width: 90%;min-height: 400px">

   <table align="center">
      <tr>
         <td colspan="5" align="center">
         	<img src="<s:url value='/resources/images/warning_blue.png'/>">
         </td>
      </tr>

      <tr>
         <td colspan="5" align="center"><h4><s:message code="error.app.general"/></h4></td>
      </tr>
      
   </table>
   
   <table align="center" width="90%" >
      
      <tr>
		  <td id = "error" align="center" width="100%">
		  	  <c:choose>
		        	<c:when test="${concurrent eq 'true'}">
						<span class="error">Se detect&oacute; un inicio de sesi&oacute;n con su cuenta de usuario en otro sitio</span>		        	
		        	</c:when>
		        	<c:otherwise>
		        		<span style="font-size:14px">El tiempo de la sesi&oacute;n ha expirado</span>
		        	</c:otherwise>
		     </c:choose>
	      </td>
      </tr>
      <tr>
		  <td id = "error" align="center" width="100%">
				<button class="btnEnviar" onclick="javascript:iniciarSesion()" style="width: 150px"><span><em>Iniciar Sesi&oacute;n</em></span></button>		        	
	      </td>
      </tr>
       </table>
       
       
   </div>
</div>

