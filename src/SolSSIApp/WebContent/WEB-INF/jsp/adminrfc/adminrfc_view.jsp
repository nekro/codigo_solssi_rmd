<%@ page contentType="text/html" isELIgnored="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
	
dojo.ready(function(){
	
  	<c:if test="${not empty updateFueraFecha}">
	   document.getElementById("divUpdateFueraFecha").style.display = "";
	</c:if>
	
});
	
	function buscar() {
		document.forms["filtro"].target = "_top";
		document.forms["filtro"].action = "<s:url value='/adminrfc/consulta'/>";
		document.forms["filtro"].submit();
	}

	function actualizar(rfc,cuenta,fueraFecha,index) {
	    var fueraFechaSi = "fueraFechaSi_"+index;
	    var fueraFechaNo = "fueraFechaNo_"+index;
	    document.getElementById("uk_rfc").value = rfc;
	    document.getElementById("uk_cuenta").value = cuenta;	 	
	    if(fueraFecha){
		   if(document.getElementById(fueraFechaNo).checked) {
			  document.getElementById("uk_newFueraFecha").value = false;
			  document.forms["action"].target = "_top";
			  document.forms["action"].action = "<s:url value='/adminrfc/updateFueraFecha'/>";
			  document.forms["action"].submit();
	       }
		}else {
			if(document.getElementById(fueraFechaSi).checked) {
			   document.getElementById("uk_newFueraFecha").value = true;
			   document.forms["action"].target = "_top";
			   document.forms["action"].action = "<s:url value='/adminrfc/updateFueraFecha'/>";
			   document.forms["action"].submit();
			}
		}
	}
	
</script>

<style>
#filtro li {
	float: left;
	margin: 0px 20px 0px 0px;
	list-style-type: none;
	height: 50px
}

</style>


<div class="moduleTitle"><span>RFC Captura fuera de fecha</span></div>
	<div id="filtro" style="width: 90%" align="center">
		<form:form modelAttribute="consultaFilter" name="filtro">
			<table class="ui-widget-content ui-corner-all">
				<tr >
					<td align="center"><span class="titleSeccion">Filtro de B&uacute;squeda</span>
					</td>
				</tr>
				<tr>
					<td align="left">
						<ul>
							<li>
							     <span>RFC:</span>
                                 <form:input path="rfc" maxlength="10"
									         style="width:90px" /> - <form:input path="homoclave"
									         maxlength="3" style="width:30px" />
							</li>
							<li>
								<button type="button" class="btnEnviar" onclick="buscar()"><span><em>Buscar</em></span></button>
							</li>		
	
						</ul>
					</td>
				</tr>
			</table>
			    <c:if test="${not empty error}">
					<br/>
					<div class="ui-widget-content ui-state-error ui-corner-all" style="width: 80%;padding-top: 5px;padding-bottom:  5px">
						 <span><c:out value="${error}"/></span>
					</div>
				</c:if>
				<s:hasBindErrors name="consultaFilter">
				    <br/>
				    <div class="ui-widget-content ui-state-error ui-corner-all" style="width: 80%;padding-top: 5px;padding-bottom:  5px">
					     <form:errors path="*"/>
				    </div>
			    </s:hasBindErrors>
		</form:form>
	</div>
	
<c:if test="${not empty clientes}">
	
		<br/>
		<br/>
		<div id="tablaTramites" align="center" class="ui-widget-content ui-corner-all" style="width: 80%">
		  <form name="action" method="post">	
			<input type="hidden" id="uk_newFueraFecha" name="newFueraFecha" />
			<input type="hidden" id="uk_rfc" name="rfc" />
			<input type="hidden" id="uk_cuenta" name="cuenta" />
			<table class="display" style="width: 100%;padding-top: 30px">
				<thead class="ui-widget-header">
					<tr>
						<th>RFC</th>
						<th>CUENTA</th>
						<th>&iquest;PERMITIR CAPTURA FUERA DE FECHA?</th>
						<th>ACTUALIZAR</th>
					</tr>
				</thead>
				<tbody></tbody>
					<c:forEach items="${clientes}" var="cli" varStatus="st">
						<c:choose>
					    	<c:when test="${st.count % 2 == 1}">
					    		<c:set var="css" value="odd"/>
					    	</c:when>
					    	<c:otherwise>
					    		<c:set var="css" value="even"/>
					    	</c:otherwise>
					    </c:choose>
						<tr class="${css}">
							<td><c:out value="${cli.rfc}"/></td>
							<td><c:out value="${cli.cuenta}"/></td>
							<td>
							    <c:if test="${cli.fueraFecha}">
							          <input type="radio" value="1" id="fueraFechaSi_${st.count}" name="fueraFecha_${st.count}" checked="checked" />SI
							          <input type="radio" value="0" id="fueraFechaNo_${st.count}" name="fueraFecha_${st.count}" />NO
							    </c:if>
							    <c:if test="${not cli.fueraFecha}">
							          <input type="radio" value="1" id="fueraFechaSi_${st.count}" name="fueraFecha_${st.count}" />SI
							          <input type="radio" value="0" id="fueraFechaNo_${st.count}" name="fueraFecha_${st.count}" checked="checked" />NO
							    </c:if>
							</td>
							<td><a href="javascript:actualizar('<c:out value="${cli.rfc}"/>','<c:out value="${cli.cuenta}"/>',<c:out value="${cli.fueraFecha}"/>,${st.count})" style="color: blue;text-decoration: underline;"><img src="<s:url value="/resources/images/save-icon32.png" />" alt="Actualizar" height="20px" width="20px"/></a>
                            </td>
						</tr>
			        </c:forEach>
			</table>
		  </form>	
		</div>
</c:if>

<div id="divUpdateFueraFecha" style="display:none">
     <span><img src="<s:url value="/resources/images/sucess.png" />" alt="Suceso Exitoso" height="50px" width="50px"/><br/>Registro actualizado exitosamente.</span>

</div>
