<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<style>
input{
	text-transform: uppercase;
}

<s:hasBindErrors name="solicitud">
	#containerSolicitud td{
		vertical-align: top;
	}
	
	.error{
		position: relative;
		top: -3px; 
	}
</s:hasBindErrors>

}

</style>

<script>
		
		function enviarDatos(){
			var result = doValidateConsultaSaldo();
			if(result == true){
				submitForm(document.forms["saldo"]);	
		    }
		 }
		
		function actualizarCaptcha(){
			var d = new Date(); 
			dojo.byId("imgCaptcha").src = "<s:url value='/solicitud/captcha?d='/>"+d.getTime();
		}
		
</script>

<s:url value="/clientes/saldo/execute" var="urlConsulta"/>

<div class="moduleTitle"><span>Consulta de Saldo</span></div>
        
		<div class="" align="center" style="width: 675px;height: 450px">
		
			
			<br/>    
			
			<form:form name="saldo" id="saldo" modelAttribute="cliente" method="post" action="${urlConsulta} " autocomplete="on">
			
				<table cellspacing="0" class="ui-widget-content ui-corner-all" style="width: 100%;min-height: 175px" >
					<tbody>
						<tr>
						<td colspan="4" style="padding: 15px 5px 15px 5px">
				            <h5 style="padding: 5px 5px 15px 25px" align="left"><s:message code="saldo.leyenda4" htmlEscape="true"/></h5>
						</td>
						</tr>
						
						
						<tr>
							<td width="30%" align="right"><span class="required">*</span><label for="rfc">RFC-Homoclave:</label></td>
							<td width="20%" align="left">
							    <table> 
							    	<tbody>
							    		<tr>
							    			<td>
							    				<form:input title="Capture RFC" path="rfc" maxlength="10" onkeypress="return enableAlfaNumero(event);" onchange="validRFC(this);" cssStyle="width: 90px"/>
											</td>
						        			<td>
							    				<form:input path="homoclave" maxlength="3" onkeypress="return enableAlfaNumero(event)" cssStyle="width: 30px"/>
											</td>
						                </tr>
						              </tbody>
						          </table>
						    </td>
						
							<td width="25%" align="right"><span class="required">*</span><label for="cuenta">N&uacute;mero de Cuenta SSI:</label></td>
							<td width="25%" align="left"><form:input path="cuenta" maxlength="10" onkeypress="return enableNumero(event)" onchange="validCuenta(this);" cssStyle="width: 90px" />
							</td>
				
						</tr>
						
				        <!--  Fila error -->		
						<tr>
							<td colspan="2" align="center" height="15px"><span id="error_rfc" class="error"/></td>
							<td colspan="2" align="left"" height="15px"><span id="error_cuenta" class="error"/></td>
						</tr>
						<tr>
							<td align="right" colspan="3"><span class="required">*</span><form:label path="ultimoDescuento" htmlEscape="true">Importe del &uacute;ltimo descuento por concepto SSI en su tal&oacute;n de pago:</form:label></td>
							<td align="left">
								<form:input path="ultimoDescuento" maxlength="15" onkeypress="return enableDecimal(event)" cssStyle="width:90px"/>
							</td>
						</tr>
						
						<tr>
							<td colspan="4" align="center" height="30px">
								<br/>
								<span id="errores_saldo" class="error"></span>
							</td>
						</tr>
										
					     <tr>
					     	<td colspan="4" align="center">
						     		<br/><br/>
					   				<div>
					        			<button type="button" style="color:blue;font-size:8pt;text-decoration:underline" onclick="javascript:actualizarCaptcha()">Actualizar Imagen</button>
					   				</div>
						     		
						     		<div align="center">
					   					<img src="<s:url value='/solicitud/captcha'/>" id="imgCaptcha"/>
					   				</div>
					   				<div>
					   					<label for="captcha"><s:message code="NotEmpty.solicitud.captcha"/></label>
					   				</div>
					   				<div>
					   					<input name="captcha" maxlength="5" style="text-transform: none;width:50px" onkeypress="return enableAlfa(event)"/>
					   				</div>
					    			<c:if test="${validCaptcha == 'false'}">
					    				<br/>
										<span class="error"><s:message code="NotValidCaptcha.solicitud.captcha"/></span>
									</c:if>
					     	<td>
					     </tr>
					     
					     <tr>
									 <td colspan="4" align="center" style="padding-bottom: 15px">
										 <br/>
										 <button type="button" class="btnEnviar" onclick="enviarDatos()"><span><em>Consultar</em></span></button>
										 <br/><br/>
									 </td>
					     </tr>
					     
					</tbody>
				</table>
				<s:hasBindErrors name="cliente">
					<br/>
					<div class="ui-widget-content ui-state-error ui-corner-all" style="width: 100%;padding-top: 5px;padding-bottom:  5px">
							<div><form:errors path="*" /></div>
					</div>
				</s:hasBindErrors>
				
			</form:form>
		</div>