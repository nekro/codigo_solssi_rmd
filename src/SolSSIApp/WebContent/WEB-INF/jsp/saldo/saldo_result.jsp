<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<script>
		
		function salir(){
			document.forms["form"].target = "_top";
			document.forms["form"].action = "<s:url value='/solicitud/salir'/>";
			document.forms["form"].submit();
		}
		
		
</script>

<s:url value="/clientes/busqueda" var="urlConsulta"/>

<div class="moduleTitle"><span>Resultado</span></div>        

	<div id="containerInicio" align="center" style="width: 800px">
	    <form name="form" method="post">
			<table class="ui-widget-content ui-corner-all" style="width: 100%" align="center">
				<tr><td align="center"><img src="<s:url value='/resources/images/sucess.png'/>" width="50px" height="50px" /></td></tr>
							<tr>
								<td align="center">
									<span class="leyenda">
										<s:message code="saldo.result" htmlEscape="true"/>&nbsp;<c:out value="${clienteSSI.fmtSaldo}"/>
									</span>
								</td>
							</tr>
							<tr>
									<td align="center">
										<table>
											<tr style="font-size: 12px;">
												<td><s:message code="saldo.result.informacion" htmlEscape="true"/></td>
											</tr>
										</table>
									</td>
							</tr>	
				<tr>
					<td align="center">
						<button type="button" class="btnEnviar" onclick="salir()"><span><em><s:message code="button.salir" htmlEscape="true"/></em></span></button>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</form>
	</div>	
	<br/>
