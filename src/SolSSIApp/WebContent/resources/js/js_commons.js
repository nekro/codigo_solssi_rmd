function AbrirCalendario(control) {
  var fecha = control.value
  fecha = window.showModalDialog('calendario.jsp', fecha, 'scroll:no;dialogWidth:300px;dialogHeight:220px;status:off')
  control.value = fecha
}


function allTrim(valor) {
    return valor.replace(/^\s*|\s*$/g,"")
}

function EscribirIE (texto) {
  if (navigator.appName == 'Microsoft Internet Explorer')
    document.write(texto)
}

function EstablecerOptions(objeto1, objeto2, valor) {
  var option = 0
  if (objeto1.options.length)
    while (option < objeto1.options.length) {
      if (objeto1.options[option].value == valor) {
        objeto2.options[objeto2.options.length] =
          new Option(objeto1.options[option].text, objeto1.options[option].value)
        objeto1.options[option] = null
        return
      }
      option++
    }
}

function FormatoMoneda(pValor, pSigno)
{
  var sDecimales = pValor.substring(pValor.lastIndexOf('.'))
  var sEnteros = pValor.substring(0, pValor.lastIndexOf('.'))
  var sConComas = ''
  for (var i=sEnteros.length; i>3; i-=3)
    sConComas = sConComas + ',' + sEnteros.substring(i-3, i)
  sConComas = sEnteros.substring(0, i) + sConComas
  return ((pSigno) ? '$ ' : '') + sConComas + sDecimales
}

function Imprimir() {
  self.print()
}

function MostrarMensaje(falta, msg) {
  var mensaje = ''
  if (falta != '')
    mensaje += "Falta de capturar los siguientes campos obligatorios:\n\n" + falta + '\n\n'
  if (msg != "")
    mensaje += "Debe de corregir lo siguiente:\n\n" + msg
  if (mensaje != '') {
    alert(mensaje)
    return true
  }
  return false
}

function QuitarFormato(pControl) {
  return pControl.value.replace('$','').replace('%','').replace(',','')
}

function MoverOptions(objeto1, objeto2) {
  var option = 0
  if (objeto1.options.length)
    while (option < objeto1.options.length) {
      if (objeto1.options[option].selected) {
        objeto2.options[objeto2.options.length] =
          new Option(objeto1.options[option].text, objeto1.options[option].value)
        objeto1.options[option] = null
        option=-1
      }
      option++
    }
}

function ObtenerOptions(objeto) {
  var options = ''
  if (objeto.options.length)
    for (var option=0; option < objeto.options.length; option++)
      options += objeto.options[option].value + ((option < objeto.options.length - 1) ? '-' : '')
  return options
}

function Regresar(form, controlPantalla, pantalla) {
  document[form][controlPantalla].value = pantalla
  document[form].submit()
}
function ValidarFechaInicioFinEtiq(fechaInicio, fechaFin) {
  fI = new Date(fechaInicio.substring(3,5) + '/' + fechaInicio.substring(0,2) + '/' +
    ((fechaInicio.substring(6) > 50) ? '19' : '20' ) + fechaInicio.substring(6))
  fF = new Date(fechaFin.substring(3,5) + '/' + fechaFin.substring(0,2) + '/' +
    ((fechaFin.substring(6) > 50) ? '19' : '20' ) + fechaFin.substring(6))
  if (!isNaN(fI) && !isNaN(fF))
    if (fF < fI)
      return false
  return true
}

function ValidarFechaInicioFin(fechaInicio, fechaFin) {
  fI = new Date(fechaInicio.substring(3,5) + '/' + fechaInicio.substring(0,2) + '/' +
    ((fechaInicio.substring(6) > 50) ? '19' : '20' ) + fechaInicio.substring(6))
  fF = new Date(fechaFin.substring(3,5) + '/' + fechaFin.substring(0,2) + '/' +
    ((fechaFin.substring(6) > 50) ? '19' : '20' ) + fechaFin.substring(6))
  if (!isNaN(fI) && !isNaN(fF))
    if (fF <= fI)
      return false
  return true
}

function ValidarFechaCapturaHoy(fecha) {
  fechaCaptura = new Date(fecha.substring(3,5) + '/' + fecha.substring(0,2) + '/' +
    ((fecha.substring(8) > 50) ? '19' : '20' ) + fecha.substring(8))
  hoy = new Date()
  hoy = new Date((hoy.getMonth() + 1) + '/' + hoy.getDate() + '/' + hoy.getYear())
  return (fechaCaptura < hoy) ? false : true
}

function ValidarNumero(pControl, pValMin, pValMax, pValDef, pDecimales, pVacio, pForzarMaxMin, pFormato)
{
  // pFormato: c = comas, n = n�mero, m = moneda, p = porcentaje
  sValor = QuitarFormato(pControl)
  if (isNaN(parseFloat(sValor)))
  {
    if (pVacio)
    {
      pControl.value = pValDef
      return
    }
    sValor = pValDef
  }
  var iValor = (pDecimales > 0) ? parseFloat(sValor) : parseInt(sValor)
  iValor = (iValor > pValMax && pForzarMaxMin) ? pValMax : iValor
  iValor = (iValor < pValMin && pForzarMaxMin) ? pValMin : iValor
  iValor = iValor.toFixed(pDecimales)
  pControl.value = (pFormato=='m') ? FormatoMoneda(iValor, true): ((pFormato=='p') ? (iValor + '%') : ((pFormato=='c') ? FormatoMoneda(iValor, false) : iValor))
}

function VerOpcionesMenu(nomObjeto,boton) {
    objeto = document.all[nomObjeto]
    if(objeto.style.display==''){
       objeto.style.display = 'none'
       boton.src = 'imagenes/triangulo.gif'
     
    }else{
       objeto.style.display = ''
       boton.src = 'imagenes/b_bajo.gif'
    }
}

function VerOpcionesMenuPiv(nomObjeto,boton) {
    objeto = document.all[nomObjeto]
    if(objeto.style.display==''){
       objeto.style.display = 'none'
     //  boton.src = 'imagenes/triangulo.gif'
     
    }else{
       objeto.style.display = ''
      // boton.src = 'imagenes/b_bajo.gif'
    }
}


function mensaje(errorMsg, numErrors){
    if (!numErrors)
        numErrors = 0;
    switch(numErrors) {
        case 0:     break;
        case 1:     errorMsg = "Se encontr� 1 error: \r\r" + errorMsg; break;
        default:    errorMsg = "Se encontraron " + numErrors + " errores: \r\r" + errorMsg; break;
    }
    alert(errorMsg);
}

/*  eval_cadena():  Funcion para evaluar si una cadena tiene caracteres validos y longitud maxima y minima
    cadena: cadena a validar
    maximo: longitud maxima
    maximo: longitud minimo
    extras: caracteres extras que puede aceptar la cadena (p. ej. #$%&/_- )
*/
function eval_cadena(cadena,minimo, maximo, extras)
{
    var CadenaAceptada = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"+extras; // caracteres aceptados mas los extras
//    var cadena = validaN(cadena);
    cadena = cadena.toUpperCase();
    var caracterActual;
    var contador = 0;

    // -- comprobar que la cadena tiene esta dentro del rango de caracteres validos
    if ((cadena.length<minimo) || (cadena.length>maximo)) {        
        return false;
    }
    else {
        // -- ciclo para determinar si existen caracteres no permitidos
        for (var i=0; i < cadena.length; i++) {
            var caracterActual = cadena.substring(i,i+1);
            if (CadenaAceptada.indexOf(caracterActual) < 0) {
                return false;
            } // end if
        } // end for
   }
    return true;  // no hubo error (esta correcta la cadena)
}



function enableKey(inicio,final){
	if (!(window.event.keyCode >= inicio.charCodeAt(0) &&
		window.event.keyCode <= final.charCodeAt(0)))
		window.event.keyCode = 0;
}