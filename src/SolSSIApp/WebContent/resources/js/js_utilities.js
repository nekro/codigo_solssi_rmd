var bArrastrar, oControl, temp1, temp2, z
var oH = [['0'],['1'],['2'],['3'],['4'],['5'],['6'],['7'],['8'],['9'],['a'],['b'],['c'],['d'],['e'],['f']]

function IniciarRGB()
{
  document.onmousedown = Arrastrar
  document.onmouseup = new Function("bArrastrar=false")
  document.all.txtRGB.value = (window.dialogArguments == '') ? '#000000' : window.dialogArguments
  document.all.imgR.style.pixelLeft = HEX2DEC(document.all.txtRGB.value.substring(1,3)) + 12
  document.all.spnR.innerText = DEC2HEX(document.all.imgR.style.pixelLeft - 12)
  document.all.imgG.style.pixelLeft = HEX2DEC(document.all.txtRGB.value.substring(3,5)) + 12
  document.all.spnG.innerText = DEC2HEX(document.all.imgG.style.pixelLeft - 12)
  document.all.imgB.style.pixelLeft = HEX2DEC(document.all.txtRGB.value.substring(5,7)) + 12
  document.all.spnB.innerText = DEC2HEX(document.all.imgB.style.pixelLeft - 12)
  document.all.divRGB.style.backgroundColor = document.all.txtRGB.value
  document.all.spnRGB.style.color = document.all.txtRGB.value
}//IniciarRGB

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function AceptarRGB()
{
  window.returnValue = document.all.txtRGB.value
  self.close()
}//AceptarRGB

function Arrastrar()
{
  z = event.srcElement

  if (z.className == 'arrastrar')
  {
    bArrastrar = true
    temp1 = z.style.pixelLeft
    temp2 = z.style.pixelTop
    x = event.clientX
    y = event.clientY
    if (z.className == 'arrastrar') document.onmousemove = Mover
  }
}//Arrastrar

function DEC2HEX(pN)
{
  return oH[Math.floor(pN / 16)] + '' + oH[pN % 16]
}//DEC2HEX

function HEX2DEC(pN)
{
  var H1, H2
  for (H1=0; H1<16; H1++) if (oH[H1] == pN.substring(0,1)) break
  for (H2=0; H2<16; H2++) if (oH[H2] == pN.substring(1)) break
  return (H1 * 16) + H2
}//HEX2DEC

function MostrarMensaje()
{
  document.all.divMensaje.innerText = document.all.hidMsg.value
  document.all.divMensaje.style.display = (document.all.hidMsg.value == '') ? 'none' : ''
  return (Trim(document.all.hidMsg.value)=='') ? false : true
}//MostrarMensaje

function MostrarTip(pMostrar, pMensaje1, pMensaje2)
{
  SuprStat()
  document.all.divTip.style.width =  document.body.clientWidth - event.x - 10
  document.all.divTip.innerHTML =
    '<div style="COLOR:FFFFFF;BACKGROUND-COLOR:778899;FONT-SIZE:10;FONT-FAMILY:Verdana,Arial,Helvetica;PADDING:1"><b>&nbsp;' + pMensaje1 + '</b></div>' +
    '<div class="ColTd" style="FONT-SIZE:10;FONT-FAMILY:Verdana,Arial,Helvetica;PADDING:2">' + ((pMensaje2.substring(0,3)!='<ta') ? '&nbsp;' : '') + pMensaje2 + '</div>'
  document.all.divTip.style.left = event.x + 10
  document.all.divTip.style.top = event.y + 10  + document.body.scrollTop
  document.all.divTip.style.display = (pMostrar) ? '' : 'none'
}//MostrarTip

function Mover()
{
  if (event.button == 1 && bArrastrar)
  {
    z.style.pixelLeft = (z.h == 'true') ? (temp1 + event.clientX - x) : z.style.pixelLeft
    z.style.pixelTop = (z.v == 'true') ? (temp2 + event.clientY - y) : z.style.pixelTop
    if (z.id == 'imgR' || z.id == 'imgG' || z.id == 'imgB')
    {
      z.style.pixelLeft = (z.style.pixelLeft < 12) ? 12 : z.style.pixelLeft
      z.style.pixelLeft = (z.style.pixelLeft > 267) ? 267 : z.style.pixelLeft
      document.all['spn' + z.id.substring(3)].innerText = DEC2HEX(z.style.pixelLeft - 12)
      document.all.txtRGB.value = '#' + document.all.spnR.innerText + document.all.spnG.innerText + document.all.spnB.innerText
      document.all.divRGB.style.backgroundColor = document.all.txtRGB.value
      document.all.spnRGB.style.color = document.all.txtRGB.value
    }
  }
  return false
}//Mover

function SuprStat()
{
  self.status = ''
  try
  {
    parent.self.status = ''
    parent.parent.self.status = ''
    parent.parent.parent.self.status = ''
  } catch(e){}
}//SuprStat

function Trim(pCadena)
{
  return pCadena.replace(/^\s*|\s*$/g,'')
}//Trim