var cX, cY, ID__Act=1, ID__Cmt=1, ID__Cnd=1, ID__Con=1, ID__Etq=1, ID__In=1, ID__Out=1, sConf, tabY, xa, ya, z
var bArrastrar, bEnlace = false, oConf, oControl, oDia, oDoc, oModo

//Para GenerarDiagrama()
var cmds = 'addcalldebuggetdatagotoletmoveseterrorsubtractupdate'
var idE1, idE2
var INCX = 100, INCY = 40

var iteraciones = 0, numMessages = 0, numVariables = 0

function ExecuteRules() {
	alert('Hola Mundo');
}

function Iniciar() {
    tabY = 0
    oDoc = document.all
    document.onmousedown = Arrastrar
    document.onmouseup = new Function("bArrastrar=false")
    document.all.hidAccion.value = ''
    document.all.hidMsg.value = ''
    
    // Nuevo
    campos = document.all.campos.value.split('|')
    for (idx = 0; idx < campos.length - 1; idx+=2) {
        AgregarOption(document.all.listaCampos, campos[idx], campos[idx])
    }
    GenerarDiagrama()
    ActualizarRegla()
}//Iniciar

function IniciarConfAct() {
    var aConf = window.opener.document.all.argumentos.value.split('~')
    var oDoc = document.all, i
    var instruccion = aConf[1].substring(0,aConf[1].indexOf(' '))
    oDoc.descripcion.innerText = aConf[0]
    oDoc.instruccion.anterior = aConf[1]
    if (aConf[1].substring(0, 1) != '&')
        oDoc.instruccion.value = instruccion
    oDoc.variables.value = window.opener.document.all.variables.value
    oDoc.campos.value = window.opener.document.all.campos.value
    variables = oDoc.variables.value.split('|')
    campos = oDoc.campos.value.split('|')
    etiquetas = aConf[2].split('|')
    oDoc.tipoExpresion.value = aConf[3]
    var messages = aConf[4].split('|')
    for (idx=0; idx<variables.length-1; idx+=2) {
        AgregarOption(oDoc.id1, variables[idx] + '|' + variables[idx+1], variables[idx])
        AgregarOption(oDoc.id2, variables[idx] + '|' + variables[idx+1], variables[idx])
        AgregarOption(oDoc.ids, variables[idx] + '|' + variables[idx+1], variables[idx])
        if (variables[idx+1] == 'int') {
            AgregarOption(oDoc.idError, variables[idx] + '|' + variables[idx+1], variables[idx])
        }
        if (variables[idx+1] == 'int' || variables[idx+1] == 'real') {
            AgregarOption(oDoc.varNum1, variables[idx] + '|' + variables[idx+1], variables[idx])
            AgregarOption(oDoc.varNum2, variables[idx] + '|' + variables[idx+1], variables[idx])
            AgregarOption(oDoc.varNum3, variables[idx] + '|' + variables[idx+1], variables[idx])
            AgregarOption(oDoc.varNums, variables[idx] + '|' + variables[idx+1], variables[idx])
        }
    }
    for (idx=0; idx<campos.length-1; idx+=2) {
        AgregarOption(oDoc.id1, campos[idx] + '|' + campos[idx+1], campos[idx])
        AgregarOption(oDoc.id2, campos[idx] + '|' + campos[idx+1], campos[idx])
        AgregarOption(oDoc.ids, campos[idx] + '|' + campos[idx+1], campos[idx])
        AgregarOption(oDoc.campos1, campos[idx] + '|' + campos[idx+1], campos[idx])
        if (campos[idx+1] == 'int' || campos[idx+1] == 'real') {
            AgregarOption(oDoc.varNum1, campos[idx] + '|' + campos[idx+1], campos[idx])
            AgregarOption(oDoc.varNum2, campos[idx] + '|' + campos[idx+1], campos[idx])
            AgregarOption(oDoc.varNum3, campos[idx] + '|' + campos[idx+1], campos[idx])
            AgregarOption(oDoc.varNums, campos[idx] + '|' + campos[idx+1], campos[idx])
        }
    }
    for (idx=0; idx<messages.length-1; idx+=2) {
        AgregarOption(oDoc.messages, messages[idx], messages[idx])
    }
    
    if (etiquetas.length == 1) {
        AgregarOption(oDoc.etiqueta, "0", "[NO HAY ETIQUETAS]")
    } else {
        for (idx=0; idx<etiquetas.length-1; idx+=2) {
            AgregarOption(oDoc.etiqueta, etiquetas[idx + 1], etiquetas[idx])
        }
    }
    ArmarInstruccion()
    EditarInstruccion(aConf[1])
}//IniciarConfAct

function IniciarConfCnd() {
    instruccion = opener.document.all[opener.document.all.control.value].condicion
    document.all.descripcion.value = opener.document.all[opener.document.all.control.value].n
    document.all.expresion.value = (instruccion.substring(0, 1) != '&') ? instruccion : ''
    document.all.variables.value = opener.document.all.variables.value
    document.all.campos.value = opener.document.all.campos.value
}//IniciarConfCnd

function IniciarConfCmt() {
    oConf = window.dialogArguments
    document.all.drplstFuente.value = oConf.style.fontFamily
    document.all.drplstTamano.value = oConf.style.fontSize
    document.all.cbxN.checked = (oConf.style.fontWeight == 'bold') ? true : false
    document.all.cbxC.checked = (oConf.style.fontStyle == 'italic') ? true : false
    document.all.cbxS.checked = oConf.style.textDecorationUnderline
    document.all.txtColor.value = oConf.style.color
}//IniciarConfCmt

function IniciarConfCon() {
    aConf = window.dialogArguments.split('|')
    aOption = aConf[2].split('~')
    for (var i=0; i<aOption.length; i+=2)
        if (aOption[i].substring(0,6)=='simOut') AgregarOption(document.all.lstEnlace, aOption[i], aOption[i+1])
    document.all.lstEnlace.value = aConf[1]
}//IniciarConfCon

function IniciarGenExp() {
    oDoc = document.all
    //aConf = window.dialogArguments.split('-')
    variables = (opener.document.all.variables.value + opener.document.all.campos.value).split('|')
    expresion = opener.document.all.expresion.value
    for (idx=0; idx<variables.length-1; idx+=2) {
        AgregarOption(oDoc.operando1, variables[idx] + '|' + variables[idx+1], variables[idx])
        AgregarOption(oDoc.operando2, variables[idx] + '|' + variables[idx+1], variables[idx])
    }
    
    oDoc.literal1.style.display = (oDoc.operando1.value == '0') ? '' : 'none'
    oDoc.literal2.style.display = (oDoc.operando2.value == '0') ? '' : 'none'
}//IniciarGenExp

function IniciarGenFnc() {
    oDoc = document.all
    variables = (opener.document.all.variables.value + opener.document.all.campos.value).split('|')
    for (idx=0; idx<variables.length-1; idx+=2) {
        AgregarOption(oDoc.ids, variables[idx] + '|' + variables[idx+1], variables[idx])
        AgregarOption(oDoc.id1, variables[idx] + '|' + variables[idx+1], variables[idx])
    }
    //Edita la funci�n
    sentencia = Trim(opener.document.all.funcion.value)
    if (sentencia != '') {
        sentencia = sentencia.substring(sentencia.indexOf(' ') + 1)
        SeleccionarOption(oDoc.funciones, sentencia.substring(0, sentencia.indexOf(' ')))
        parametros = sentencia.substring(sentencia.indexOf(' with ') + 6, sentencia.indexOf(' giving ')).split(',')
        
        for (p=0; p<parametros.length; p++) {
            parametro = Trim(parametros[p])
            if (!isNaN(parametro) || parametro.indexOf('"') > -1) {
                tipo = 'alpha'
                if (parametro.indexOf('"') == -1)
                    if (parametro.indexOf('.') == -1)
                        tipo = 'int'
                    else
                        tipo = 'real'
                AgregarOption(oDoc.parametros, parametro + '|' + tipo, parametro)
            } else {
                SeleccionarOption(oDoc.ids, parametro)
                MoverOptions(oDoc.ids, oDoc.parametros)
            }
        }
        oDoc.literal.value = Trim(oDoc.literal.value)
        
        SeleccionarOption(oDoc.id1, sentencia.substring(sentencia.lastIndexOf(' ') + 1))
    }
}//IniciarGenFunc

function IniciarImprimir() {
    var i, sHTML = '', sURL
    oEE = document.all.tdImprimir
    oEE.innerHTML = opener.document.all.divArea.innerHTML
  
    for (i=0; i<oEE.childNodes.length; i++)
        if (oEE.children.item(i)) {
            oEE.children.item(i).style.cursor = 'crosshair'
            if (oEE.children.item(i).id == 'simEtq') {
                oEE.children.item(i).style.border = ''
                oEE.children.item(i).style.left = oEE.children.item(i).style.pixelLeft + 1
                oEE.children.item(i).style.top = oEE.children.item(i).style.pixelTop + 1
            }
            if (oEE.children.item(i).style.backgroundImage) {
                sURL = oEE.children.item(i).style.backgroundImage
                sHTML += '<img src="' + sURL.substring(4, sURL.length-1) 
                    + '" style="POSITION:absolute;LEFT:' + oEE.children.item(i).style.left
                    + ';TOP:' + oEE.children.item(i).style.top + '">'
            }
    }
    oEE.innerHTML += sHTML
    self.print()
    self.close()
}//IniciarImprimir

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function AceptarConfAct() {
    var i, oDoc = document.all
    var instruccion = '', msg = ''
    error = oDoc.idError.value.substring(0, oDoc.idError.value.indexOf('|'))
    campos = ObtenerVariablesSeleccionadas(oDoc.campos1)
    etiqueta = oDoc.etiqueta.value
    expresion = oDoc.expresion.value
    funcion = oDoc.funcion.value
    literal = oDoc.literal.value
    modoAdd = oDoc.modoAdd.value
    varNum1 = oDoc.varNum1.value.substring(0, oDoc.varNum1.value.indexOf('|'))
    varNum2 = oDoc.varNum2.value.substring(0, oDoc.varNum2.value.indexOf('|'))
    varNum3 = oDoc.varNum3.value.substring(0, oDoc.varNum3.value.indexOf('|'))
    valor1 = oDoc.id1.value.substring(0, oDoc.id1.value.indexOf('|'))
    valor2 = oDoc.id2.value.substring(0, oDoc.id2.value.indexOf('|'))
    valores = ObtenerVariablesSeleccionadas(oDoc.ids)
    varNums = ObtenerVariablesSeleccionadas(oDoc.varNums)

    tipo1 = oDoc.id1.value.substring(oDoc.id1.value.indexOf('|') + 1)
    tipo2 = oDoc.id2.value.substring(oDoc.id2.value.indexOf('|') + 1)
    tipoExpresion = oDoc.tipoExpresion.value
    
    switch (oDoc.instruccion.value) {
        case '0' : {
            instruccion = oDoc.instruccion.anterior
        } break
        case 'add' : {
            numeros = oDoc.literal.value.split(' ')
            oDoc.literal.value = ''
            for (numero=0; numero<numeros.length; numero++)
                if (!isNaN(numeros[numero]))
                    oDoc.literal.value += numeros[numero] + ' '
            literal = Trim(oDoc.literal.value)
            
            msg += (varNums == '' && (Trim(literal) == '' || isNaN(literal))) ? 'Especifique los valores a sumar\n\n' : ''
            msg += (valor2 == 0) ? 'Especifiqie la variable destino\n\n' : ''
            if (msg != '') {
                alert(msg)
                return
            }
            instruccion = 'add ' + varNums + ((Trim(literal) != '') ? (' ' + literal) : '') + ' ' + modoAdd + ' ' + valor2
            instruccion = instruccion.replace(/,/gi, '')
        } break
        case 'debug' : {
            if (valores == '')
                instruccion = oDoc.instruccion.anterior
            else
                instruccion = 'debug ' + valores
        } break
        case 'call' : {
            instruccion = (funcion != '') ? funcion : oDoc.instruccion.anterior
        } break
        case 'getdata' : {
            instruccion = (document.all.messages.value != '0')
	        ? ('getdata ' + document.all.messages.value)
                : oDoc.instruccion.anterior
        } break
        case 'goto' : {
            instruccion = (etiqueta == '0') ? oDoc.instruccion.anterior : ('goto ' + etiqueta)
        } break
        case 'let' : {
            msg += (valor2 == 0) ? 'Debe de especificar la variable destino\n\n' : ''
            msg += (Trim(expresion) == '') ? 'Debe de especificar la expresi�n\n\n' : ''
            if (msg != '') {
                alert(msg)
                return
            }
            tipoExpresion = (tipoExpresion == 'int' || tipoExpresion == 'real' ) ? 'num' : tipoExpresion
            tipo2 = (tipo2 == 'int' || tipo2 == 'real' ) ? 'num' : tipo2
            if (tipoExpresion != tipo2) {
                alert('La variable destino debe de ser tipo ' + ((tipoExpresion == 'num') ? 'num�rico' : tipoExpresion))
                return
            }
            instruccion = 'let ' + valor2 + ' = ' + expresion
        } break
        case 'move' : {
            instruccion = 'move '
            msg += (valor1 == 0 && literal == '') ? 'Debe de especificar la literal\n\n' : ''
            msg += (valor2 == 0) ? 'Debe de especificar la variable destino\n\n' : ''
            if (valor1 == 0 && literal != '') {
                if (tipo2 == 'alpha') {
                    instruccion += '"' + literal + '" to ' + valor2
                } else {
                    if (isNaN(oDoc.literal.value)) {
                        msg += 'La literal debe de ser num�rica\n\n'
                    } else {
                        instruccion += oDoc.literal.value + ' to ' + valor2
                    }
                }
            } else {
                instruccion += valor1 + ' to ' + valor2
            }
            if (msg != '') {
                alert(msg)
                return
            }
        } break
        case 'seterror' : {
            instruccion = 'seterror '
            if (oDoc.idError.value == '0') {
                if (isNaN(literal) || Trim(literal) == '') {
                    alert('La literal debe de ser num�rica')
                    return
                } else {    
                    instruccion += literal
                }
            } else {
                instruccion += error
            }
        } break
        case 'subtract' : {
            instruccion = 'subtract '
            if (document.all.varNum1.value == '0') {
                if (isNaN(literal) || Trim(literal) == '') {
                    alert('La literal debe de ser num�rica')
                    return
                } else {    
                    instruccion += literal
                }
            } else {
                instruccion += varNum1
            }
            instruccion += ' from ' + varNum2
            if (document.all.varNum3.value != '0') {
                instruccion += ' giving ' + varNum3
            }
        } break
        case 'update' : {
            instruccion = (document.all.messages.value != '0')
                ? ('update ' + document.all.messages.value)
                : oDoc.instruccion.anterior
        }
    }
    window.opener.document.all.argumentos.value = oDoc.descripcion.innerText + '|' + instruccion + '|' + oDoc.tipoExpresion.value
    ActualizarAct()
    window.opener.ActualizarRegla()
    self.close()
}//AceptarConfAct

function AceptarConfCnd() {
    descripcion = document.all.descripcion.value
    expresion = document.all.expresion.value
    iW = Math.floor(descripcion.length / 12) + 1
    opener.document.all[opener.document.all.control.value].style.width = 67 * iW
    opener.document.all[opener.document.all.control.value].style.backgroundImage = (iW > 1)
        ? ('url(imagenes/reglas/sim_cnd' + iW + '.gif)')
        : 'url(imagenes/reglas/sim_cnd.gif)'
    opener.document.all[opener.document.all.control.value].innerHTML = '<center><span id="etq" UNSELECTABLE="on"><BR>' + descripcion + '</span>'
    opener.document.all[opener.document.all.control.value].n = document.all.descripcion.value
    opener.document.all[opener.document.all.control.value].condicion = (Trim(expresion) != '') ? expresion : pControl.condicion
    self.close()
}//AceptarConfCnd

function AceptarConfCmt() {
    oConf.style.fontFamily = document.all.drplstFuente.value
    oConf.style.fontSize = document.all.drplstTamano.value
    oConf.style.fontWeight = (document.all.cbxN.checked) ? "bold" : "normal"
    oConf.style.fontStyle = (document.all.cbxC.checked) ? "italic" : "normal"
    oConf.style.textDecorationUnderline = (document.all.cbxS.checked) ? true : false
    try { oConf.style.color = document.all.txtColor.value } catch (e) { alert('Error en AceptarConfCmt()')}
    window.returnValue = oConf
    self.close()
}//AceptarConfCmt

function AceptarConfCon() {
    window.returnValue = document.all.lstEnlace.value + '|' + document.all.lstEnlace.value.substring(6)
    self.close()
}//AceptarConfCon

function AceptarGenExp() {
    if (AgregarExpresion()) {
        opener.document.all.expresion.value = document.all.expresion.value
	opener.document.all.tipoExpresion.value = document.all.tipoExpresion.value
	self.close()
    }
}//AceptarGenExp

function AceptarGenFnc() {
    oDoc = document.all
    falta = ''
    msg = ''
    if (oDoc.funciones.value == 0) falta += 'Seleccionar la funci�n\n'
    if (oDoc.id1.value == 0) falta += 'Asignar a una variable\n'
    msg += AgregarFuncion()
    if (msg != '' || falta != '') {
        alert(((falta != '') ? ('FALTA:\n\n' + falta + '\n') : '') + msg)
        return
    }
    
    opener.document.all.funcion.value = document.all.funcion.value
    self.close()
}//AceptarGenFnc

function ActualizarAct() {
    try {
        argumentos = opener.document.all.argumentos
        o = opener.document.all[opener.document.all.control.value]
        aConf = argumentos.value.split('|')
        o.innerHTML = '<center><span id="etq" UNSELECTABLE="on">' + aConf[0] + '</span>'
        o.n = aConf[0]
        o.instruccion = aConf[1]
        o.tipo = aConf[2]
    } catch (e) {
        self.close()
    }
}//ActualizarAct

function ActualizarEnlaces() {
    MostrarTip(false, '', '')
    document.all.spnMsg.innerText = 'Actualizando enlaces...'
    
    var i
    try {
        for (i=1; i<ID__Con; i++) {
            for (j=1; j<=4; j++) {
                if (document.all['con___' + i + '_' + j]) {
                    document.all['con___' + i + '_' + j].outerHTML = ''
                }
            }
        }
        ID__Con = 1
        EnlazarControl(document.all.simInicio)
        for (i=1; i<ID__Act; i++) {
            EnlazarControl(document.all['simAct' + i])
        }
        for (i=1; i<ID__Cmt; i++) {
            EnlazarControl(document.all['simCmt' + i])
        }
        for (i=1; i<ID__Cnd; i++) {
            EnlazarControl(document.all['simCnd' + i])
            EnlazarControl2(document.all['simCnd' + i])
        }
        for (i=1; i<ID__Etq; i++) {
            EnlazarControl(document.all['simEtq' + i])
        }
        for (i=1; i<ID__In; i++) {
            EnlazarControl(document.all['simIn ' + i])
        }
        for (i=1; i<ID__Out; i++) {
            EnlazarControl(document.all['simOut' + i])
        }
        
    }
    catch(e) { alert('Error en ' + z.id + ' de ActualizarEnlaces()') }
    document.all.spnMsg.innerText = ''
}//ActualizarEnlaces

function ActualizarMessages() {
    messages = ''
    i = -1
    while (++i < document.all.length) {
        if (document.all[i].id.substring(0,5)=='txtNM') {
            if (Trim(document.all[i].value)=='' || ObtenerOptions(document.all['lstCM' + document.all[i].id.substring(5)], '')=='') {
                EliminarMessage(document.all[i].id.substring(5))
                i = 0
            }
        }
    }
    
    for (i=0; i<document.all.length; i++)
        if (document.all[i].id.substring(0,5)=='txtNM' || document.all[i].id.substring(0,5)=='txtTM')
            messages += document.all[i].value + '|' + ObtenerOptions(document.all['lstCM' + document.all[i].id.substring(5)], '~') + '|'

    if (VerificarVariables(messages)) {
        document.all.messages.value = messages
        oDoc.tdMessages.innerHTML = ''
        OcultarDiv(oDoc.divCampos)
        ActualizarRegla()
    }
}//ActualizarMessages

function ActualizarRegla() {
    var F = ';<br>'
    var regla = ''
    iteraciones = 0
    rgbMsg = 'COLOR:#777777'
    // Variables
    variables = ''
    a = document.all.variables.value.split('|')
    if (document.all.variables.value != '') {
        regla += '<span style="' + rgbMsg + '">& Variables</span><BR>'
        for (idx = 0; idx < a.length - 1; idx+=2) {
            if (variables.indexOf(' ' + a[idx+1] + ' ') > -1) {
                pos = variables.indexOf(';', variables.indexOf(' ' + a[idx+1] + ' '))
                variables = variables.substring(0, pos) + ', ' + a[idx] + variables.substring(pos)
            } else {
                variables += 'declare ' + a[idx+1] + ' ' + a[idx] + F
            }
        }
        regla += variables + '<br>'
    }
    
    // Campo
    campo = document.all.campo.value
    
    if(campo!=''){
	    if (campo.indexOf('|') > -1) {
	        regla += '<span style="' + rgbMsg + '">& Campo</span>' + '<br>'
	        regla += 'message Campo(' + campo.substring(0, campo.indexOf('|')) + ')' + F + '<br>'
	    } else alert('Error en el campo')
    }
    
    // Sets de campos (messages)
    a = document.all.messages.value.split('|')
    if (document.all.messages.value.length > 0) {
        regla += '<span style="' + rgbMsg + '">& Messages</span>' + '<br>'
        for (m=0; m<a.length-1; m+=2)
            regla += 'message ' + a[m] + '(' + a[m+1].replace(/~/gi, ', ') + ')' + F
        regla += '<br>'
    }

    oEndIf = {length:20, nombre:[], nivel:[], padre:[]}
    sangria = ''
    
    if (document.all.simInicio) {
        e1 = document.all.simInicio.e1
        while (e1 != '') {
        iteraciones++
            for (idx=0; idx<oEndIf.length; idx++)
                if (document.all[e1].id == oEndIf.nombre[idx]) {
                    sangria = sangria.substring(12)
                    regla += sangria + 'endif<br>'
                    oEndIf.nombre[idx] = undefined
                    oEndIf.nivel[idx]  = undefined
                    oEndIf.padre[idx]  = undefined
                }
            switch (e1.substring(0, 6)) {
                case 'simAct' : {
                    instruccion = document.all[e1].instruccion
                    if (instruccion.substring(0,4) == 'goto') {
                        if (document.all[instruccion.substring(5)]) {
                            instruccion = 'goto ' + document.all[instruccion.substring(5)].innerText
                        } else {
                            document.all[e1].instruccion = '& Ninguna'
                            instruccion = document.all[e1].instruccion
                        }
                    }
                    regla += (document.all[e1].instruccion.substring(0, 1) == '&') 
                        ? sangria + '<span style="' + rgbMsg + '">& ' + document.all[e1].n + ' </span><br>'
                        : sangria + instruccion + F
                } break
                case 'simCnd' : {
                    regla += (document.all[e1].condicion.substring(0, 1) == '&') 
                        ? sangria + 'if <span style="' + rgbMsg + '">1 = 1</span> then<br>' 
                        : sangria + 'if ' + document.all[e1].condicion + ' then<br>'
                    idx = ObtenerIdx(oEndIf)
                    oEndIf.nombre[idx] = document.all[e1].e1
                    sangria += '&nbsp;&nbsp;'
                } break
                case 'simCmt' : {
                    regla += sangria + '<span style="' + rgbMsg + '">& ' + document.all[e1].innerText + '</span><br>'
                } break
                case 'simEtq' : {
                    regla += '<I>' + document.all[e1].innerText + ':</I><br>'
                } break
                case 'simFin' : {
                    regla += (sangria == '') ? '<br>endprogram.' : ''
                } break
                case 'simIn ' : {
                } break
                case 'simOut' : {
                } break
                default : regla += e1 + F
            }
            e1 = (e1.substring(0, 6) == 'simCnd') ? document.all[e1].e2 : document.all[e1].e1
        }
    }
    oDoc.divRegla.innerHTML = regla
}//ActualizarRegla

function ActualizarVariables() {
    variables = ''
    i = -1
    while (++i < document.all.length) {
        if (document.all[i].id.substring(0,5)=='txtNV' && Trim(document.all[i].value)=='') {
            EliminarVariable(document.all[i].id.substring(5))
            i = 0
        }
    }
    
    for (i=0; i<document.all.length; i++)
        if (document.all[i].id.substring(0,5)=='txtNV' || document.all[i].id.substring(0,5)=='txtTV')
            variables += document.all[i].value + '|'
    
    if (VerificarVariables(variables)) {
        document.all.variables.value = variables
        oDoc.tdVariables.innerHTML = ''
        OcultarDiv(oDoc.divVariables)
        ActualizarRegla()
    }
}//ActualizarVariables

function AgregarExpresion() {
    oDoc = document.all
    var msg = '', tipoOperando1 = 'alpha', tipoOperando2 = 'alpha'
    if (isNaN(oDoc.literal1.value)) {
        oDoc.literal1.value = '"' + oDoc.literal1.value + '"'
    }
    if (isNaN(oDoc.literal2.value)) {
        oDoc.literal2.value = '"' + oDoc.literal2.value + '"'
    }
    oDoc.literal1.value = oDoc.literal1.value.replace(/""/g, '"').replace(/""/g, '"')
    oDoc.literal2.value = oDoc.literal2.value.replace(/""/g, '"').replace(/""/g, '"')
    funcion1 = oDoc.funcion1.value.substring(0, oDoc.funcion1.value.indexOf('|'))
    funcion2 = oDoc.funcion2.value.substring(0, oDoc.funcion2.value.indexOf('|'))
    operador = oDoc.operador.value.substring(0, oDoc.operador.value.indexOf('|'))
    operando1 = (oDoc.operando1.value == '0')
        ? oDoc.literal1.value
        : oDoc.operando1.value.substring(0, oDoc.operando1.value.indexOf('|'))
    operando2 = (oDoc.operando2.value == '0')
        ? oDoc.literal2.value
        : oDoc.operando2.value.substring(0, oDoc.operando2.value.indexOf('|'))
    // Validaciones y definici�n de tipo - Inicio
    tipoOperador = oDoc.operador.value.substring(oDoc.operador.value.indexOf('|') + 1)

    if (oDoc.operando1.value.indexOf('|') > -1)
        tipoOperando1 = oDoc.operando1.value.substring(oDoc.operando1.value.indexOf('|') + 1)
    else
        tipoOperando1 = DeterminarTipo(operando1)
        
    if (oDoc.operando2.value.indexOf('|') > -1)
        tipoOperando2 = oDoc.operando2.value.substring(oDoc.operando2.value.indexOf('|') + 1)
    else
        tipoOperando2 = DeterminarTipo(operando2)
    
    
    msg += (operando1 == '') ? 'Debe de especificar el primer operando\n\n' : ''
    msg += (operando2 == '') ? 'Debe de especificar el segundo operando\n\n' : ''
    if (operando1 != '' && operando2 != '') {
        if (tipoOperador == 'num') {
            msg += ((tipoOperando1 == 'bool' || tipoOperando1 == 'alpha') || (tipoOperando2 == 'bool' && tipoOperando2 == 'alpha'))
                ? 'Los operandos deben de ser num�ricos\n\n' : ''
        } else {
            msg += (tipoOperando1 != tipoOperando2) ? 'Los operandos deben de ser del mismo tipo\n\n' : ''
        }
    }
    // Validaciones y definici�n de tipo - Fin
    
    oDoc.expresion.value  = ((funcion1 == '0') ? operando1 : (funcion1 + '(' + operando1 + ')'))
    oDoc.expresion.value += ' ' + operador + ' '
    oDoc.expresion.value += ((funcion2 == '0') ? operando2 : (funcion2 + '(' + operando2 + ')'))
    oDoc.expresion.value  = '(' + oDoc.expresion.value + ')'
       
    if (msg != '') {
        alert(msg)
        return false
    }
    oDoc.tipoExpresion.value = tipoOperador
    AgregarOption(oDoc.operando1, oDoc.expresion.value + '|' + tipoOperador, oDoc.expresion.value)
    AgregarOption(oDoc.operando2, oDoc.expresion.value + '|' + tipoOperador, oDoc.expresion.value)
    return true
}//AgregarExpresion

function AgregarFuncion() {
    oDoc = document.all
    msg = ''
    parametros = ''
    temp = oDoc.funciones.value.substring(oDoc.funciones.value.indexOf('|') + 1)
    numParametros = temp.substring(0, temp.indexOf('|'))
    temp = temp.substring(temp.indexOf('|') + 1)
    tipoParametro = temp.split('|')
    try {
        for (option=0; option < numParametros; option++) {
            tipoOption = oDoc.parametros.options[option].value.substring(oDoc.parametros.options[option].value.indexOf('|')+1)
            if (tipoParametro[option] != tipoOption)
                msg += 'El par�metro ' + (option + 1) + ' debe de ser de tipo ' + tipoParametro[option] + '\n\n'
            parametros +=  oDoc.parametros.options[option].text + ', '
        }
        parametros = (parametros.length > 0) ? parametros.substring(0, parametros.length - 2) : ''
    } catch (e) {
        msg += 'Debe de indicar ' + numParametros + ((numParametros>1) ? ' parametros' : ' par�metros') + '\n\n'
    }
    tipoFuncion = oDoc.funciones.value.substring(oDoc.funciones.value.lastIndexOf('|') + 1)
    tipoResultado = oDoc.id1.value.substring(oDoc.id1.value.indexOf('|')+ 1)
    if (tipoFuncion != tipoResultado && oDoc.id1.value != '0')
        msg += 'El resultado de la funci�n debe de asignarse a una variable de tipo ' + tipoFuncion + '\n\n'
    if (msg != '') return msg
    oDoc.funcion.value = 
        'call ' + oDoc.funciones.value.substring(0, oDoc.funciones.value.indexOf('|')) +
        ' with ' + parametros +
        ' giving ' + oDoc.id1.value.substring(0, oDoc.id1.value.indexOf('|'))
    return ''
}//AgregarFuncion

function AgregarMessage(nombre, campos) {
    oDoc.tdMessages.innerHTML +=
        '<table id="tblM' + numMessages + '" cellPadding="0" cellSpacing="0"><tr>' +
        '<td width="45" align="center" valign="top">' +
        '<img id="imgM' + numMessages + '" src="imagenes/eliminar.gif" border="0" onclick="EliminarMessage(' + "'" + numMessages + "'" + ')" style="CURSOR:hand"></td>' +
        '<td align="center" width="115" valign="top"><input id="txtNM' + numMessages + '" maxLength="12" type="text" width="95" value="' + ((nombre) ? nombre: '') + '"></td>' +
        '<td align="center" width="133">' +
        CrearComboCampos('lstCM' + numMessages, campos, 240) +
        '</td>' +
        '<td align="center" width="30">' +
        '<input type="button" value="&nbsp;&#060;&nbsp;" onclick="CopiarOptions(document.all.listaCampos, document.all.lstCM' + numMessages + ', true)"><br>' +
        '<input type="button" value="&nbsp;x&nbsp;" onclick="EliminarOptions(document.all.lstCM' + numMessages + ', document.all.listaCampos, true)">' +
        '</td>' +
        '</tr></table>'
    numMessages++
}//AgregarMessage

function AgregarOption(pControl, pValor, pTexto) {
    pControl.options.length += 1
    pControl.options[pControl.options.length-1].innerText = pTexto
    pControl.options[pControl.options.length-1].value = pValor
}//AgregarOption

function AgregarParametros() {
    MoverOptions(oDoc.ids, oDoc.parametros)
    literales = oDoc.literal.value.split(' ')
    for (i=0; i < literales.length; i++) {
        literal = ((isNaN(literales[i])) ? ('"' + literales[i] + '"') : literales[i])
        if (Trim(literal) != '') {
            tipo = 'alpha'
            if (literal.indexOf('"') == -1)
                if (literal.indexOf('.') == -1)
                    tipo = 'int'
                else
                    tipo = 'real'
            AgregarOption(document.all.parametros, literal + '|' + tipo, literal)
        }
    }
    document.all.literal.value = ''
}//AgregarParametros

function AgregarVariable(nombre, tipo) {
    oDoc.tdVariables.innerHTML +=
        '<table id="tblV' + numVariables + '" cellPadding="0" cellSpacing="0"><tr>' +
        '<td width="50" align="center">' +
        '<img id="imgV' + numVariables + '" src="imagenes/eliminar.gif" border="0" onclick="EliminarVariable(' + "'" + numVariables + "'" + ')" style="CURSOR:hand"></td>' +
        '<td><input id="txtNV' + numVariables + '" maxLength="12" type="text" width="95" value="' + ((nombre) ? nombre: '') + '">' +
        '<td>' + CrearComboTipo(('txtTV' + numVariables), ((tipo) ? tipo : 'int'), 108) +
        '</td></tr></table>'
    numVariables++
}//AgregarVariable

function ArmarInstruccion() {
    oDoc = document.all
    oDoc.a.style.display = 'none'
    oDoc.btn.style.display = 'none'
    oDoc.campos1.style.display = 'none'
    oDoc.cr.style.display = 'none'
    oDoc.etiqueta.style.display = 'none'
    oDoc.expresion.style.display = 'none'
    oDoc.funcion.style.display = 'none'
    oDoc.literal.style.display = 'none'
    oDoc.idError.style.display = 'none'
    oDoc.modoAdd.style.display = 'none'
    oDoc.id1.style.display = 'none'
    oDoc.id2.style.display = 'none'
    oDoc.ids.style.display = 'none'
    oDoc.sep1.style.display = 'none'
    oDoc.varNum1.style.display = 'none'
    oDoc.varNum2.style.display = 'none'
    oDoc.varNum3.style.display = 'none'
    oDoc.varNums.style.display = 'none'
    oDoc.messages.style.display = 'none'
    
    oDoc.a.style.display = 'none'
    oDoc.literal.value = ''
    oDoc.idError.value = '0'
    oDoc.id1.value = '0'
    oDoc.id2.value = '0'
    td = oDoc.tdInstruccion
    instruccion = document.all.instruccion.value
    switch (instruccion) {
        case 'update' : {
            td.innerText = 'Set de campos'
            oDoc.messages.style.display = ''
            return
        } break
        case 'let' : {
            td.innerHTML = 'Asignar<br><br>a'
            oDoc.btn.style.display = ''
            oDoc.expresion.style.display = ''
            oDoc.id2.style.display = ''
        } break
        case 'move' : {
            td.innerText = 'Asignar'
            oDoc.a.style.display = ''
            oDoc.literal.style.display = ''
            oDoc.id1.style.display = ''
            oDoc.id2.style.display = ''
        } break
        case 'goto' : {
            td.innerText = 'Ir a'
            oDoc.etiqueta.style.display = ''
        } break
        case 'call' : {
            td.innerText = 'Llamar a'
            oDoc.btn.style.display = ''
            oDoc.funcion.style.display = ''
        } break
        case 'debug' : {
            td.innerText = 'Mostrar'
            oDoc.ids.style.display = ''
        } break
        case 'getdata' : {
            td.innerText = 'Set de campos'
            oDoc.messages.style.display = ''
            return
        } break
        case 'seterror' : {
            td.innerText = 'Mostrar'
            oDoc.idError.style.display = ''
            oDoc.literal.style.display = ''
        } break
        case 'subtract' : {
            td.innerHTML = 'Restar<br><br>y asignar a'
            oDoc.a.style.display = ''
            oDoc.literal.style.display = ''
            oDoc.varNum1.style.display = ''
            oDoc.varNum2.style.display = ''
            oDoc.varNum3.style.display = ''
        } break
        case 'add' : {
            td.innerText = 'Sumar'
            oDoc.cr.style.display = ''
            oDoc.varNums.style.display = ''
            oDoc.literal.style.display = ''
            oDoc.id2.style.display = ''
            oDoc.modoAdd.style.display = ''
            oDoc.sep1.style.display = ''
        }
    }
}//ArmarInstruccion()
		
function Arrastrar() {
    z = event.srcElement
    z = (z.id == 'etq') ? z.parentElement.parentElement : z

    if (z.className=='arrastrar'||z.className=='relacion') {
        oControl = (z.id.substring(0, 6) == 'simVer' || z.id.substring(0, 6) == 'simFal')
            ? document.all['simCnd' + z.id.substring(6)]
            : z
        bArrastrar=true
        temp1 = z.style.pixelLeft
        temp2 = z.style.pixelTop
        x = event.clientX
        y = event.clientY
        xa = z.style.pixelLeft
        ya = z.style.pixelTop
        if (z.className=='arrastrar') document.onmousemove = Mover
    }
}//Arrastrar

function Borrar() {
    var i
    //Borra las l�neas de enlace
    try {
        i = oDoc.divArea.childNodes.length-1
        while (i > -1) {
            if (oDoc.divArea.children.item(i))
                if (oDoc.divArea.children.item(i).e1 && oDoc.divArea.children.item(i).e2)
                    if (oDoc.divArea.children.item(i).e1==oControl.id||oDoc.divArea.children.item(i).e2==oControl.id)
                        if (oDoc.divArea.children.item(i).className == 'relacion') {
                            BorrarIDEnlace(oDoc.divArea.children.item(i))
                            oDoc.divArea.children.item(i).outerHTML = ''
                            i = oDoc.divArea.childNodes.length
                        }
            i--
        }
    } catch(e) { alert(oDoc.divArea.children.item(oDoc.divArea.childNodes.length-1).id) }
    //Borra el control
    if (oControl.id.substring(0,3)=="sim") {
        oControl.outerHTML = ''
        if (oControl.id.substring(0,6)=="simCnd") {
            document.all['simVer' + oControl.id.substring(6)].outerHTML = ''
            document.all['simFal' + oControl.id.substring(6)].outerHTML = ''
        }
    }
    if (oControl.id.substring(0,3)=="con") {
        i = oDoc.divArea.childNodes.length-1
        while (i > -1) {
            if (oDoc.divArea.children.item(i))
                if (oDoc.divArea.children.item(i).id.substring(0, oDoc.divArea.children.item(i).id.length-2)==oControl.id.substring(0,oControl.id.length-2)) {
                    BorrarIDEnlace(oDoc.divArea.children.item(i))
                    oDoc.divArea.children.item(i).outerHTML = ''
                    i = oDoc.divArea.childNodes.length
                }
            i--
        }
    }
    ActualizarRegla()
}//Borrar

function BorrarIDEnlace(pControl) {
    try {if (document.all[pControl.e1].e1 == pControl.e1) document.all[pControl.e1].e1 = ''} catch(e){}
    try {if (document.all[pControl.e1].e1 == pControl.e2) document.all[pControl.e1].e1 = ''} catch(e){}
    try {if (document.all[pControl.e1].e2 == pControl.e1) document.all[pControl.e1].e2 = ''} catch(e){}
    try {if (document.all[pControl.e1].e2 == pControl.e2) document.all[pControl.e1].e2 = ''} catch(e){}
    try {if (document.all[pControl.e2].e1 == pControl.e1) document.all[pControl.e2].e1 = ''} catch(e){}
    try {if (document.all[pControl.e2].e1 == pControl.e2) document.all[pControl.e2].e1 = ''} catch(e){}
    try {if (document.all[pControl.e2].e2 == pControl.e1) document.all[pControl.e2].e2 = ''} catch(e){}
    try {if (document.all[pControl.e2].e2 == pControl.e2) document.all[pControl.e2].e2 = ''} catch(e){}
}//BorrarIDEnlace

function Colocar(pControl) { 
    MostrarTip(false, '', '')
    var px = z.style.pixelLeft - oDoc._divArea.style.pixelLeft
    var py = z.style.pixelTop - oDoc._divArea.style.pixelTop
    var s1 = ' e1="" e2="" class="arrastrar" style="POSITION:absolute;BACKGROUND-IMAGE:url(imagenes/reglas/sim_'
    var s2 = ');BACKGROUND-REPEAT:no-repeat;FILTER:alpha (opacity=80);LEFT:' + (px+oDoc._divArea.scrollLeft) + ';top:' + (py+oDoc._divArea.scrollTop)
    if (Dentro(px, py, (px+z.width), (py+z.style.pixelHeight), oDoc.divArea.style.pixelLeft, oDoc.divArea.style.pixelTop, (oDoc.divArea.style.pixelLeft+oDoc.divArea.style.pixelWidth), (oDoc.divArea.style.pixelTop+oDoc.divArea.style.pixelHeight))) {
        switch (pControl.id) {
            case 'imgInicio' : {
                oDoc.divArea.innerHTML += (document.all.simInicio) ? '' : '<div id="simInicio"' + s1 + 'inicio.gif' + s2 + ';WIDTH:62;HEIGHT:19" n="INICIO"></div>'
            } break
            case 'imgFin' : {
              oDoc.divArea.innerHTML += (document.all.simFin) ? '' : '<div id="simFin"' + s1 + 'fin.gif' + s2 + ';WIDTH:62;HEIGHT:19" clave=-100 n="FIN"></div>'
            } break
            case 'imgOut' : {
              oDoc.divArea.innerHTML += '<div id="simOut' + (ID__Out) + '"' + s1 + 'con.gif' + s2 + ';WIDTH:22;HEIGHT:22;PADDING-LEFT:3;PADDING-TOP:5" n="OUT ' + ID__Out + '" r="" e="1"><center><span id="etq" UNSELECTABLE="on">' + ID__Out + '</span></center></div>'
              ID__Out++
            } break
            case 'imgIn' : {
              oDoc.divArea.innerHTML += '<div id="simIn ' + (ID__In) + '"' + s1 + 'con.gif' + s2 + ';WIDTH:22;HEIGHT:22;PADDING-LEFT:3;PADDING-TOP:5" n="In ' + ID__In + '" r="" e="1"><center><span id="etq" UNSELECTABLE="on">*</span></center></div>'
              ID__In++
            } break
            case 'imgAct' : {
              oDoc.divArea.innerHTML += '<div id="simAct' + (ID__Act) + '" class="arrastrar" style="POSITION:absolute;BORDER:solid 1px #999999;' + s2 + ';WIDTH:70;HEIGHT:14" n="Sentencia ' + ID__Act + '" clave=' + ID__Act + ' instruccion="& Ninguna" e1="" e2="" k=' + ID__Act + ' tipo=""><center><span id="etq" UNSELECTABLE="on">Sentencia ' + ID__Act + '</span></center></div>'
              ID__Act++
            } break
            case 'imgCmt' : {
              oDoc.divArea.innerHTML += '<div id="simCmt' + (ID__Cmt++) + '" e1="" e2="" contentEditable class="arrastrar" style="BORDER:1px dashed #999999;POSITION:absolute;LEFT:' + px + ';top:' + (py+oDoc._divArea.scrollTop) + ';HEIGHT:14" onkeyup="ActualizarRegla()">Comentario</div>'
            } break
            case 'imgCnd' : {
              oDoc.divArea.innerHTML += '<div id="simCnd' + (ID__Cnd) + '"' + s1 + 'cnd.gif' + s2 + ';WIDTH:68;HEIGHT:36" n="Condici�n ' + ID__Cnd + '" condicion="& Ninguna" e="1"><center><span id="etq" UNSELECTABLE="on"><BR>Condici�n ' + ID__Cnd + '</span></center></div>'
              oDoc.divArea.innerHTML += '<div id="simVer' + ID__Cnd + '" class="arrastrar" style="POSITION:absolute;BACKGROUND-IMAGE:url(imagenes/reglas/v.gif);FILTER:alpha (opacity=80);LEFT:' + (px+70) + ';top:' + (py+oDoc._divArea.scrollTop+20) + ';WIDTH:7;HEIGHT:10;FONT-SIZE:1"></div>'
              oDoc.divArea.innerHTML += '<div id="simFal' + ID__Cnd + '" class="arrastrar" style="POSITION:absolute;BACKGROUND-IMAGE:url(imagenes/reglas/f.gif);FILTER:alpha (opacity=80);LEFT:' + (px+38) + ';top:' + (py+oDoc._divArea.scrollTop+40) + ';WIDTH:7;HEIGHT:10;FONT-SIZE:1"></div>'
              ID__Cnd++
            } break
            case 'imgEtq' : {
              oDoc.divArea.innerHTML += '<div id="simEtq' + (ID__Etq) + '" e1="" e2="" contentEditable class="arrastrar" style="BORDER:1px solid #999999;POSITION:absolute;LEFT:' + px + ';top:' + (py+oDoc._divArea.scrollTop) + ';BACKGROUND-IMAGE:url(imagenes/reglas/etiqueta.gif);PADDING-LEFT:14px;BACKGROUND-REPEAT:no-repeat;HEIGHT:14" onkeyup="ActualizarRegla()">Etiqueta' + (ID__Etq++) + '</div>'
            } break
        }
        ActualizarRegla()
    }
    pControl.style.left = pControl.izq
    pControl.style.top = pControl.arr
}//Colocar

function ColorRGB(pControl) {
    sConf = document.all.txtColor.value
    sConf = window.showModalDialog('zRGB.html', sConf, 'scroll:no;dialogWidth:300px;dialogHeight:220px;status:off')
    document.all.txtColor.value = (sConf != undefined) ? sConf : document.all.txtColor.value
}//ColorRGB

function CopiarOptions(origen, destino, unico) {
    for (var option=0; option<origen.options.length; option++)
        if (origen.options[option].selected && NoExisteEnSelect(origen.options[option].text, destino)) {
            destino.options[destino.options.length] = new Option(origen.options[option].text, origen.options[option].value)
            origen.options[option].selected = null
        }
}

function CrearComboCampos(nombre, campos, ancho) {
    var aCampos = campos.split('~')
    html = '<select name="' + nombre + '" size=4 style="WIDTH:' + ancho + '">'
    if (campos.length > 0) 
        for (var campo=0; campo<aCampos.length; campo++)
            html += '<option>' + aCampos[campo] + '</option>'
    html += '</select>'
    return html
}//CrearComboCampos

function CrearComboTipo(nombre, tipo, ancho) {
    html =
        '<select id="' + nombre + '" style="WIDTH:' + ancho + '">' +
        CrearOption('int','int', tipo) +
        CrearOption('boolean','boolean', tipo) +
        CrearOption('real','real', tipo) +
        CrearOption('alpha','alpha', tipo) +
        '</select>'
    return html
}//CrearComboTipo

function CrearOption(texto, valor, valorSeleccionado) {
    return '<option value="' + valor + '"' + ((valorSeleccionado==valor) ? ' selected' : '') + '>' + texto + '</option>'
}//CrearOption

function Dentro(aix, aiy, afx, afy, oix, oiy, ofx, ofy) {
    return (aix >= oix && afx <= ofx && aiy >= oiy && afy <= ofy) ? true : false
} // Dentro

function Desmarcar() {
    document.all.spnMsg.innerText = ''
    try {
        if (oDoc.divMenu.style.display != '') oControl = null
    } catch(e){}
}//Desmarcar

function DeterminarTipo(valor) {
    tipo = 'alpha'
    if (!isNaN(valor))
        tipo = (valor.indexOf('.') > -1) ? 'real' : 'int'
    return tipo
}//DeterminarTipo

function Editar(pControl) {
    OcultarMenu(0)
    document.all.control.value = pControl.id
    switch (pControl.id.substring(0, 6)) {
        case 'simAct' : {
            try {
                winCnd.close()
                winExp.close()
            } catch (e) {}
            document.all.argumentos.value = pControl.n + '~' + pControl.instruccion + '~' + ObtenerEtiquetas() + '~' + pControl.tipo + '~' + document.all.messages.value
            winConf = window.open('zConfAct.html', 'winConf', 'width=770px, height=280px')
            winConf.focus()
        } break
        case 'simCnd' : {
            try {
                winConf.close()
                winExp.close()
            } catch (e) {}
            winCnd = window.open('zConfCnd.html', 'winCnd', 'width=540px, height=140px')
            winCnd.focus()
        } break
        case 'simIn ' : {
            sConf = pControl.innerText + '|' + pControl.e1 + '|' + ObtenerEnlaces(pControl)
            sConf = window.showModalDialog('zConfCon.html', sConf, 'scroll:yes;dialogWidth:210px;dialogHeight:135px;status:off')
            if (sConf != undefined) {
                aConf = sConf.split('|')
                pControl.innerHTML = '<center><span id="etq" UNSELECTABLE="on">' + aConf[1] + '</span>'
                pControl.e1 = aConf[0]
            }
        } break
        case 'simCmt' : {
            sConf = pControl
            sConf = window.showModalDialog('zConfCmt.html', sConf, 'scroll:yes;dialogWidth:195px;dialogHeight:255px;status:off')
            if (sConf != undefined) pControl = sConf
        } break
    }
    ActualizarRegla()
}//Editar

function EditarCampos() {
    if (document.all.divCampos.style.display == '') return
        document.all.tdMessages.innerHTML = ''
        if (allTrim(document.all.messages.value) != '') {
            aCampos = document.all.messages.value.split('|')
            for (idx = 0; idx < aCampos.length - 1; idx+=2)
                AgregarMessage(aCampos[idx], aCampos[idx+1])
        }
    MostrarPropiedades(document.all.divCampos)
}//EditarCampos

function EditarExpresion() {
    winExp = window.open('zGenExp.html', 'winExp', 'width=660px, height=220px')
    winExp.focus()
}//EditarExpresion

function EditarFuncion() {
    winFnc = window.open('zGenFnc.html', 'winFnc', 'width=590px, height=260px')
    winFnc.focus()
}//EditarFuncion

function EditarInstruccion(sentencia) {
    oDoc = document.all
    var instruccion = sentencia.substring(0, sentencia.indexOf(' '))
    sentencia = sentencia.substring(sentencia.indexOf(' ') + 1)
    
    switch (instruccion) {
        case 'add' : {
            numeros = sentencia.split(' ')
            for (numero=0; numero<numeros.length; numero++)
                if (!isNaN(numeros[numero]))
                    oDoc.literal.value += numeros[numero] + ' '
            oDoc.literal.value = Trim(oDoc.literal.value)
            opciones = sentencia.substring(0, sentencia.indexOf((sentencia.indexOf(' to ') > -1) ? ' to ' : ' giving ')).split(' ')
            for (opcion=0; opcion<opciones.length; opcion++)
                SeleccionarOption(document.all.varNums, opciones[opcion])
            oDoc.modoAdd.value = (sentencia.indexOf(' to ') > -1) ? 'to' : 'giving'
            SeleccionarOption(oDoc.id2, sentencia.substring(sentencia.lastIndexOf(' ') + 1))
        } break
        case 'call' : {
            oDoc.funcion.value = 'call ' + sentencia
        } break
        case 'debug' : {
            opciones = sentencia.split(', ')
            for (opcion=0; opcion<opciones.length; opcion++)
                SeleccionarOption(document.all.ids, opciones[opcion])
        } break
        case 'getdata' : {
            oDoc.messages.value = sentencia
        }
        case 'let' : {
            SeleccionarOption(oDoc.id2, sentencia.substring(0, sentencia.indexOf(' ')))
            oDoc.expresion.value = sentencia.substring(sentencia.indexOf('=') + 2)
        } break
        case 'move' : {
            SeleccionarOption(oDoc.id1, sentencia.substring(0, sentencia.indexOf(' ')))
            SeleccionarOption(oDoc.id2, sentencia.substring(sentencia.lastIndexOf(' ') + 1))
            if (oDoc.id1.value == 0)
                if (sentencia.indexOf('"') == 0)
                    oDoc.literal.value = sentencia.substring(1, sentencia.indexOf(' ') - 1)
                else
                    oDoc.literal.value = sentencia.substring(0, sentencia.indexOf(' '))
            else
                oDoc.literal.style.display = 'none'
        } break
        case 'seterror' : {
            if (isNaN(sentencia)) {
                oDoc.literal.style.display = 'none'
                oDoc.idError.value = sentencia + '|int'
            } else {
                oDoc.literal.value = sentencia
            }
        } break
        case 'subtract' : {
            if (isNaN(sentencia.substring(0, sentencia.indexOf(' ')))) {
                oDoc.literal.style.display = 'none'
                SeleccionarOption(oDoc.varNum1, sentencia.substring(0, sentencia.indexOf(' ')))
            } else {
                oDoc.literal.value = sentencia.substring(0, sentencia.indexOf(' '))
            }
            sentencia = sentencia.substring(sentencia.indexOf('from') + 5)
            if (sentencia.indexOf(' ') > -1) {
                SeleccionarOption(oDoc.varNum2, sentencia.substring(0, sentencia.indexOf(' ')))
                sentencia = sentencia.substring(sentencia.lastIndexOf(' ') + 1)
                SeleccionarOption(oDoc.varNum3, sentencia)
            } else {
                SeleccionarOption(oDoc.varNum2, sentencia)
            }
        } break
        case 'update' : {
            oDoc.messages.value = sentencia
        }
    }
}//EditarInstruccion

function EditarVariables() {
    if (document.all.divVariables.style.display == '') return
    document.all.tdVariables.innerHTML = ''
    vars = document.all.variables.value.split('|')
    for (idx = 0; idx < vars.length - 1; idx+=2)
        AgregarVariable(vars[idx], vars[idx+1])
    MostrarPropiedades(document.all.divVariables)
}//EditarVariables

function EliminarOption(objeto, valor) {
    for (option=objeto.options.length-1; option>0; option--)
       if (objeto.options[option].text == valor)
           objeto.options[option] = null
}//EliminarOption

function EliminarOptions(objeto) {
    var option = 0
    if (objeto.options.length)
        while (option < objeto.options.length) {
            if (objeto.options[option].selected) {
                objeto.options[option] = null
                option=-1
            }
            option++
        }
}//EliminarOptions

function EliminarParametros() {
    oDoc = document.all
    for (option=oDoc.parametros.options.length-1; option>=0; option--)
        if (oDoc.parametros.options[option].selected) {
            valor = Trim(oDoc.parametros.options[option].text)
            if (valor.indexOf('"') > -1 || !isNaN(valor))
               oDoc.parametros.options[option] = null
        }
    MoverOptions(oDoc.parametros, oDoc.ids)
}//EliminarParametros

function EliminarVariable(idx) {
    document.all['tblV' + idx].outerHTML = ''
}//EliminarVariable

function EliminarMessage(idx) {
    document.all['tblM' + idx].outerHTML = ''
}//EliminarMessage

function EnArea(aix, aiy, afx, afy, oix, oiy, ofx, ofy) {
    if (Math.abs((aix + (afx-aix) / 2) - (oix + (ofx-oix) / 2)) <  ((afx-aix) + (ofx-oix)) / 2 && 
        Math.abs((aiy + (afy-aiy) / 2) - (oiy + (ofy-oiy) / 2)) <  ((afy-aiy) + (ofy-oiy)) / 2) return true
    else 
        return false
} // EnArea

function Enlazar() {
    MostrarTip(false, '', '')
    if (z.id.substring(0,6)=='simIn '||z.id=='simFin'||z.id.substring(0,6)=='simCnd') {
        MoverEnlace()
        return
    }
    var oEnlace = null
    if (bEnlace)
        if (event.srcElement) {
            if (event.srcElement.id)
                if (event.srcElement.id.length > 5)
                    if (event.srcElement.id.substring(0, 6) == 'simVer' || event.srcElement.id.substring(0, 6) == 'simFal')
                        return
            if (event.srcElement.id=='etq') {
                if (event.srcElement.parentElement.parentElement.id == z.id) return
                oEnlace = event.srcElement.parentElement.parentElement
            } else {
                if (event.srcElement.id == z.id) return
                oEnlace = (event.srcElement.id.substring(0,3)=='sim' && event.srcElement.id!=z.id)
                    ? event.srcElement : null
            }
        }
    if (oEnlace) {
        if (oEnlace.id.substring(0,6)=='simOut') {
            MoverEnlace()
            return
        }
        if (z.id.substring(0,6)=='simVer') {
            z.e = oEnlace.id
            document.all['simCnd' + z.id.substring(6)].e2 = oEnlace.id
        } else if (z.id.substring(0,6)=='simFal') {
            z.e = oEnlace.id
            document.all['simCnd' + z.id.substring(6)].e1 = oEnlace.id
        } else {
            z.e1 = oEnlace.id
        }
        ActualizarEnlaces()
        ActualizarRegla()
    }
    if (z.id.substring(0,3)=="sim") MoverEnlace()
}//Enlazar

function EnlazarControl(o) {
    if (z = o) {
        if (z.e1 != '') {
            GenerarEnlaceExpress(z, document.all[z.e1])
            MoverEnlace()
        }
    }
}//EnlazarControl

function EnlazarControl2(o) {
    if (z = o) {
        if (z.e2 != '') {
	        GenerarEnlaceExpress(z, document.all[z.e2])
	        MoverEnlace()
        }
    }
}//EnlazarControl2

function EstablecerPreferencias() {
    document.all.divArea.className = (document.all.cbxFondo.checked) ? 'area' : ''
    OcultarDiv(document.all.divPreferencias)
}//EstablecerPreferencias

function EstablecerTipo(control) {
    control.outerHTML = '<td><input id="' + control.id + '" maxLength="12" type="text" width="95" value="' + control.value + '" onmouseover="EditarTipo(this)">'
}//EstablecerTipo

function FormatearRegla() {
    document.all.divRegla.innerText = document.all.divRegla.innerText.replace(/[^0-9A-Za-z\s\n\&\*\=\+\-\/\<\>\(\)\_\:\;\.\,\"\$]/gi, '')
}//FormatearRegla

function GenerarControl(idCtrl, x, y, contenido, e1) {
    var s1 = ' e1="" e2="" class="arrastrar" style="POSITION:absolute;BACKGROUND-IMAGE:url(imagenes/reglas/sim_'
    var s2 = ');BACKGROUND-REPEAT:no-repeat;FILTER:alpha (opacity=80);LEFT:' + x + ';top:' + y
    html = ''
    resultado = ''
    switch (idCtrl) {
        case 'simAct' : {
            html = '<div id="simAct' + (ID__Act) + '" class="arrastrar" style="POSITION:absolute;BORDER:solid 1px #999999;' +
                s2 + ';WIDTH:70;HEIGHT:14" n="Sentencia ' + ID__Act + '" clave=' + ID__Act +
                " instruccion='" + contenido + "' e1='' e2='' k=" + ID__Act +
                ' tipo=""><center><span id="etq" UNSELECTABLE="on">Sentencia ' + ID__Act + '</span></center></div>'
            resultado = 'simAct' + (ID__Act++)
        } break
        case 'simCmt' : {
            html = '<div id="simCmt' + ID__Cmt +
                '" e1="" e2="" contentEditable class="arrastrar" style="BORDER:1px dashed #999999;POSITION:absolute;LEFT:' + x +
                ';top:' + y + ';HEIGHT:14" onkeyup="ActualizarRegla()">' + contenido + '</div>'
            resultado = 'simCmt' + (ID__Cmt++)
        } break
        case 'simCnd' : {
            html  = '<div id="simCnd' + ID__Cnd + '"' + s1 + 'cnd.gif' + s2 + ';WIDTH:68;HEIGHT:36" n="Condici�n ' + ID__Cnd +
                '" ' + "condicion='" + contenido + "'" + ' e="1"><center><span id="etq" UNSELECTABLE="on"><BR>Condici�n ' + ID__Cnd +
                '</span></center></div>'
            html += '<div id="simVer' + ID__Cnd +
                '" class="arrastrar" style="POSITION:absolute;BACKGROUND-IMAGE:url(imagenes/reglas/v.gif);FILTER:alpha (opacity=80);LEFT:' + (x + 70) +
                ';top:' + (y + 20) + ';WIDTH:7;HEIGHT:10;FONT-SIZE:1"></div>'
            html += '<div id="simFal' + ID__Cnd + '" class="arrastrar" style="POSITION:absolute;BACKGROUND-IMAGE:url(imagenes/reglas/f.gif);FILTER:alpha (opacity=80);LEFT:' + (x + 38) +
                ';top:' + (y + 40) + ';WIDTH:7;HEIGHT:10;FONT-SIZE:1"></div>'
            resultado = 'simCnd' + (ID__Cnd++)
        } break
        case 'simEtq' : {
            html = '<div id="simEtq' + (ID__Etq) +
                '" e1="" e2="" contentEditable class="arrastrar" style="BORDER:1px solid #999999;POSITION:absolute;LEFT:' + x +
                ';TOP:' + y + ';BACKGROUND-IMAGE:url(imagenes/reglas/etiqueta.gif);PADDING-LEFT:14px;BACKGROUND-REPEAT:no-repeat;' +
                'HEIGHT:14" onkeyup="ActualizarRegla()">' + contenido + '</div>'
            resultado = 'simEtq' + (ID__Etq++)
        } break
        case 'simFin' : {
            html = '<div id="simFin"' + s1 + 'fin.gif' + s2 + ';WIDTH:62;HEIGHT:19" clave=-100 n="FIN"></div>'
            resultado = 'simFin'
        } break
        case 'simInicio' : {
            html = '<div id="simInicio"' + s1 + 'inicio.gif' + s2 + ';WIDTH:62;HEIGHT:19" n="INICIO"></div>'
            resultado = 'simInicio'
        } break
    }
    document.all.divArea.innerHTML += html
    try {
        if (e1) {
            if (idCtrl == 'simFin') {
                fines = e1.split('|')
                for (idx=0; idx<fines.length; idx++) {
                    document.all[fines[idx]].e1 = 'simFin'
                }
            } else if (e1.indexOf('|') > - 1) {
                e1s = e1.split('|')
                for (idxE1 = 0; idxE1 < e1s.length; idxE1++) {
                    document.all[e1s[idxE1]].e1 = resultado
                }
            } else {
                if (e1.indexOf('simCnd') == 0) {
                    document.all[e1].e2 = resultado
                } else {
                    document.all[e1].e1 = resultado
                }
            }
        }
    } catch (e) {}
    return resultado
}//GenerarControl

function GenerarDiagrama() {
    document.all.spnMsg.innerText = 'Generando el diagrama. Favor de esperar'
    FormatearRegla()
    var nX = 20, nY = 20, idxEndIf = 0
    var oEndIf = {length:20, id:[]}
    oDoc = document.all
    temp = oDoc.divRegla.innerHTML
    oDoc.divRegla.innerHTML = oDoc.divRegla.innerHTML.replace(/<BR>/gi, '~')
    lineas = oDoc.divRegla.innerText.split('~')
    oDoc.divRegla.innerHTML = temp
    // Temporal. Solamente para pruebas. Inicio
    oDoc.divArea.innerHTML = ''
    oDoc.variables.value = ''
    oDoc.messages.value = ''
    // Temporal. Solamente para pruebas. Fin
    ID__Act = 1
    ID__Cmt = 1
    ID__Cnd = 1
    ID__Etq = 1
    // Coloca el inicio
    idE1 = GenerarControl('simInicio', nX + 3, nY)
    
    for (linea=0; linea<lineas.length; linea++) {
        _ = Trim(lineas[linea])
        _ = (_.indexOf(';') > -1) ? _.substring(0, _.length - 1) : _

        if (_) {
            // Recupera las variables
            if (_.indexOf('declare') == 0) {
                _ = _.substring(8)
                v = _.substring(_.indexOf(' ') + 1).split(',')
                for(idx=0; idx<v.length; idx++)
                    oDoc.variables.value += Trim(v[idx]) + '|' + _.substring(0, _.indexOf(' ')) + '|'
            } else if (_.indexOf('message') == 0 && _.indexOf(' Campo(') == -1) {
	        oDoc.messages.value += _.substring(8, _.indexOf('(')) + '|' + _.substring(_.indexOf('(') + 1, _.indexOf(')')).replace(/, /gi,'~') + '|'
            } else if (_.indexOf('endprogram') == 0) {
                // Recupera el fin
                nY += INCY
                GenerarControl('simFin', nX + 3, nY, '', idE1)
            } else if (cmds.indexOf(_.substring(0, _.indexOf(' '))) > -1 && !(_.indexOf(':') == _.length - 1) && _ != 'endif') {
                // Recupera las sentencias (actividades)
                nY += INCY
                idE1 = GenerarControl('simAct', nX, nY, _, idE1)
            } else if (_.indexOf('&') == 0 && _ != '& Variables' && _ != '& Campo' && _ != '& Messages') {
                // Recupera los comentarios
                _ = (_.indexOf('& ') == -1) ? _.replace(/&/g, '& ') : _
                nY += INCY
                idE1 = GenerarControl('simCmt', nX + 1, nY, _.substring(2), idE1)
            } else if (_.indexOf(':') == _.length - 1) {
                // Recupera las etiquetas
                nY += INCY
                idE1 = GenerarControl('simEtq', nX + 1, nY, _.substring(0, _.length - 1), idE1)
            } else if (_.indexOf('if ') == 0) {
                // Recupera los if
                nY += INCY
                idE1 = GenerarControl('simCnd', nX + 1, nY, _.substring(3, _.length - 5), idE1)
                oEndIf.id[idxEndIf] = idE1
                nX += INCX
                nY += INCY * 0.5
                idxEndIf++
            } else if (_.indexOf('endif') == 0) {
                // Recupera los endif
                try {
                    idxEndIf--
                    //document.all[oEndIf.id[idxEndIf]].e2 = idE1
                    if (idE1.indexOf('|') > -1) {
                        //document.all[oEndIf.id[idxEndIf]].e2 = idE1.substring(idE1.indexOf('|'))
                    }
                    idE1 = oEndIf.id[idxEndIf] + '|' + idE1
                    nX -= INCX
                    nY -= INCY
                } catch (e) {
                    alert('No es posible seguir generando el diagrama')
                    alert(e.description)
                    return
                }
            }
        }
    }
    // Actualiza los V y F
    for (idx=1;idx<ID__Cnd; idx++) {
        document.all['simVer' + idx].e = document.all['simCnd' + idx].e2
        document.all['simFal' + idx].e = document.all['simCnd' + idx].e1
    }
    // Actualiza los goto
    for (idx=0; idx<oDoc.divArea.childNodes.length; idx++)
        if (oDoc.divArea.children.item(idx))
            if (oDoc.divArea.children.item(idx).instruccion)
                if (oDoc.divArea.children.item(idx).instruccion.indexOf('goto') == 0)
                    for (idxEtq=0; idxEtq<oDoc.divArea.childNodes.length; idxEtq++)
                        if (oDoc.divArea.children.item(idxEtq))
                            if (oDoc.divArea.children.item(idxEtq).innerText) {
                                irA = oDoc.divArea.children.item(idx).instruccion
                                irA = irA.substring(irA.indexOf(' ') + 1, irA.length)
                                if (irA == oDoc.divArea.children.item(idxEtq).innerText)
                                    oDoc.divArea.children.item(idx).instruccion =
                                        'goto ' + oDoc.divArea.children.item(idxEtq).id
                            }
    ActualizarEnlaces()
    ActualizarRegla()
}//GenerarDiagrama
/*
function GenerarEnlace(pControl1, pControl2) {
    // Verificar que el enlace no exista
    for (i=0; i<oDoc.divArea.childNodes.length; i++) {
        if (oDoc.divArea.children.item(i))
            if (oDoc.divArea.children.item(i).className=='relacion') {
                if ((oDoc.divArea.children.item(i).e1 == pControl1.id && oDoc.divArea.children.item(i).e2 == pControl2.id) ||
                    (oDoc.divArea.children.item(i).e2 == pControl1.id && oDoc.divArea.children.item(i).e1 == pControl2.id))
                   return
                //Borra el enlace existente del conector cuando se apunta otro control
                if (pControl1.id.substring(0,6)=='simOut' && pControl1.id==oDoc.divArea.children.item(i).e1) {
                    oControl = oDoc.divArea.children.item(i)
                    Borrar()
                break
            }
        }
    }
    sIMG1 = '<img e1="' + pControl1.id+'" e2="' + pControl2.id + '" id="con___' + ID__Con
    sIMG2 = '" style="LEFT:0;px;TOP:0px;WIDTH:0px;HEIGHT:0px" class="relacion">'
    sIMG3 = '" style="LEFT:0;px;TOP:0px;WIDTH:7px;HEIGHT:7px" class="relacion">'
    oDoc.divArea.innerHTML += sIMG1 + '_1' + sIMG2 + sIMG1 + '_2' + sIMG2 + sIMG1 + '_3' + sIMG2 + sIMG1 + '_4' + sIMG3
    ID__Con++
}//GenerarEnlace*/

function GenerarEnlaceExpress(pControl1, pControl2) {
    sIMG1 = '<img e1="' + pControl1.id+'" e2="' + pControl2.id + '" id="con___' + ID__Con
    sIMG2 = '" style="LEFT:0;px;TOP:0px;WIDTH:0px;HEIGHT:0px" class="relacion">'
    sIMG3 = '" style="LEFT:0;px;TOP:0px;WIDTH:7px;HEIGHT:7px" class="relacion">'
    oDoc.divArea.innerHTML += sIMG1 + '_1' + sIMG2 + sIMG1 + '_2' + sIMG2 + sIMG1 + '_3' + sIMG2 + sIMG1 + '_4' + sIMG3
    ID__Con++
}//GenerarEnlaceExpress

function Imprimir() {
    window.open('zImprimir.html', 'win', 'resizable=yes,width=720,height=' + (screen.availHeight - 100) + ',left=' + (screen.availWidth - 720) / 2 + ',top=50')
}//Imprimir

function MostrarInfo(pObjeto) {
    if (!document.all.cbxMsg.checked) return
    o = (pObjeto.id.substring(0, 3) == 'etq') ? pObjeto.parentElement.parentElement : pObjeto
    if (o.id.substring(0, 3) == 'con') {
        e1 = (document.all[o.e1].n == undefined)
            ? (((document.all[o.e1].id.substring(0, 6) == 'simCmt') ? 'Comentario: ' : 'Etiqueta: ') + document.all[o.e1].innerText)
            : document.all[o.e1].n
        e2 = (document.all[o.e2].n == undefined)
            ? (((document.all[o.e2].id.substring(0, 6) == 'simCmt') ? 'Comentario: ' : 'Etiqueta: ') + document.all[o.e2].innerText)
            : document.all[o.e2].n
        MostrarTip(true, 'Enlaza', e1 + '<br>&nbsp;con<br>&nbsp;' + e2)
        return
    }
    switch (o.id.substring(0, 6)) {
        case 'simAct' : {
            instruccion = o.instruccion
            if (instruccion.substring(0,4) == 'goto') {
                if (document.all[instruccion.substring(5)]) { 
                    instruccion = 'goto ' + document.all[instruccion.substring(5)].innerText
                } else {
                    o.instruccion = '& Ninguna'
                    instruccion = o.instruccion
                }
            }
            MostrarTip(true, o.n, 'Instrucci�n: ' + instruccion.replace(/& /g,''))
        } break
        case 'simCnd' : {
            msg  = 'Condici�n: ' + o.condicion.replace(/& /g,'') + '<br>&nbsp;Verdadero: '
            if (document.all[o.e2]) {
                msg += (document.all[o.e2].n) ? document.all[o.e2].n : ('Comentario: ' + document.all[o.e2].innerText)
            } else {
                msg += o.e2
            }
            msg += '<br>&nbsp;Falso: '
            if (document.all[o.e1]) {
                msg += (document.all[o.e1].n) ? document.all[o.e1].n : ('Comentario: ' + document.all[o.e1].innerText)
            } else {
                msg += o.e1
            }
            MostrarTip(true, o.n,  msg) 
        } break
        case 'simFin' : MostrarTip(true, o.n, 'Fin de la regla'); break
        case 'simIni' : MostrarTip(true, o.n, 'Inicio de la regla'); break
    }
}//MostrarInfo

function MostrarMensaje() {
    document.all.divMensaje.innerText = document.all.hidMsg.value
    document.all.divMensaje.style.display = (document.all.hidMsg.value == '') ? 'none' : ''
    return (Trim(document.all.hidMsg.value)=='') ? false : true
}//MostrarMensaje

function MostrarPropiedades(divActual) {
    document.all.divHTML.style.display = 'none'
    if (divActual != divCampos) divCampos.style.display = 'none'
    if (divActual != divPreferencias) divPreferencias.style.display = 'none'
    if (divActual != divVariables) divVariables.style.display = 'none'
    divActual.style.display = ''
}//MostrarPropiedades

function MostrarTip(pMostrar, pMensaje1, pMensaje2) {
    SuprStat()
    try {
        document.all.divTip.innerHTML =
            '<div class="TblCab" style="PADDING:1;FILTER:alpha(opacity=50)"><b>&nbsp;' + pMensaje1 + '</b></div>' +
            '<div class="TblPar" style="PADDING:2;FILTER:alpha(opacity=50)">' + ((pMensaje2.substring(0,3)!='<ta') ? '&nbsp;' : '') + pMensaje2 + '</div>'
        document.all.divTip.style.left = event.x + 10
        document.all.divTip.style.top = event.y + 10  + document.body.scrollTop
        document.all.divTip.style.display = (pMostrar) ? '' : 'none'
    } catch (e) {}
}//MostrarTip
        
function Mover() {
    if (z) if (z.id) {
        cX = (z.id.substring(0,3)!='sim') ? oDoc._divArea.style.pixelLeft : 0
        cY = (z.id.substring(0,3)!='sim') ? oDoc._divArea.style.pixelTop : 0
        if (event.button == 1 && bArrastrar) {
            if ('simActsimCmtsimCndsimEtqsimFinsimIn simInisimOut'.indexOf(z.id.substring(0, 6)) > -1 || z.id.substring(0, 3) == 'img') {
                z.style.pixelLeft = temp1 + event.clientX - x
                z.style.pixelTop = (tabY == 0) ? temp2 + event.clientY - y : tabY
            }
            if (z.id.substring(0,3)=="sim") {
                if (z.style.pixelLeft < 1) z.style.pixelLeft = 0
                if (z.style.pixelTop < 1) z.style.pixelTop = 0
                if (z.style.pixelLeft > (oDoc.divArea.style.pixelWidth - z.style.pixelWidth)) z.style.pixelLeft = oDoc.divArea.style.pixelWidth - z.style.pixelWidth
                if (z.style.pixelTop > (oDoc.divArea.style.pixelHeight - z.style.pixelHeight)) z.style.pixelTop = oDoc.divArea.style.pixelHeight - z.style.pixelHeight
            }
            if (z.style.pixelLeft - cX >= 0 && z.style.pixelTop - cY >= 0) {
                oDoc.spnX.innerText = 'x: ' + (z.style.pixelLeft - cX)
                oDoc.spnY.innerText = 'y: ' + (z.style.pixelTop - cY)
            } else {
                oDoc.spnX.innerText = 'x: 0'
               oDoc.spnY.innerText = 'y: 0'
            }
            MoverTrueFalse()
            return false
        }
        if (event.button == 2 && bArrastrar) {
            bEnlace = true
        }
    }
}//Mover

function MoverEnlace() { 
    try {
        for (var i=0; i<oDoc.divArea.childNodes.length; i++)
            if (oDoc.divArea.children.item(i).e1)
                if (oDoc.divArea.children.item(i).e1==z.id||oDoc.divArea.children.item(i).e2==z.id)
                    if (oDoc.divArea.children.item(i).className == 'relacion') {
                        var x_, _x, y_, _y, w_, _w = 0, h_ = 0, _h = 0, b1_ = false, b2_ = false, _img
                        _ = oDoc.divArea.children.item(i)
                        e1 = document.all[oDoc.divArea.children.item(i).e1]
                        e2 = document.all[oDoc.divArea.children.item(i).e2]
                        x1 = e1.style.pixelLeft
                        x2 = e2.style.pixelLeft
                        y1 = e1.style.pixelTop
                        y2 = e2.style.pixelTop
                        w1 = e1.clientWidth
                        w2 = e2.clientWidth
                        h1 = e1.offsetHeight + 2
                        h2 = e2.clientHeight + 2
                        w_ = Math.abs(x1 - (x2 - w2 / 2))
           
                        if (x2 > (x1 + w1)) {
                            w_ = (x2 + w2 / 2) - (x1 + w1)
                            x_ = x1 + w1
                            _x = x_ + w_
                        } else if (x1 > (x2 + w2)) {
                            w_ = Math.abs(x1 - x2 - w2 / 2)
                            x_ = x1 - w_
                            _x = x_
                        } else {
                            x_ = x1 + w1 / 2
                            w_ = 0
                            _w = Math.abs((x1 + w1 / 2) - (x2 + w2 / 2))
                            _h = 0
                            _x = ((x1 + w1 / 2) > (x2 + w2 / 2)) ? (x_ - _w) : x_
                            b1_ = true
                        }
           
                        if (y2 > (y1 + h1)) {
                            y_ = y1 + h1 / 2
                            y_ += (w_ == 0) ? (h1 / 2) : 0
                            h_ = (y2 - (y1 + h1)) / 2
                            _y = (b1_) ? (y_ + h_) : y_
                            _h = (b1_) ? ((y2 - y_) / 2) : (y2 - y_)
                        } else if (y1 > (y2 + h2)) {
                            h_ = (y1 - (y2 + h2)) / 2
                            y_ = (w_ == 0) ? (y1 - h_) : (y1 + h1 / 2)
                            _y = (b1_) ? (y_) : (y2 + h2)
                            _h = (b1_) ? ((y_ - (y2 + h2)) / 2): (y_ - (y2 + h2))
                        } else {
                            w_ = (x1 > x2) ? ((x1 - (x2 + w2)) / 2) : ((x2 - (x1 + w1)) / 2)
                            x_ = (x1 > x2) ? (x1 - w_) : x1 + w1
                            y_ = y1 + h1 / 2
                            h_ = 0
                            _x = ((x1 + w1 / 2) > (x2 + w2 / 2)) ? x_ : (x_ + w_)
                            _y = y2 + h2 / 2
                            _h = Math.abs(y_ - _y)
                            _y = ((y1 + h1 / 2) > (y2 + h2 / 2)) ? _y : y_
                            b2_ = true
                        }
           
                        switch (_.id.substring(_.id.length-1)) {
                            case '1' : {
                            _.style.left =   x_
                            _.style.top =    y_
                            _.style.width =  w_
                            _.style.height = (w_ == 0) ? h_ : 0
                            _.style.borderLeft = (w_ == 0) ? 'solid 1px #999999' : ''
                            _.style.borderTop = (w_ != 0) ? 'solid 1px #999999' : ''
                            if (_.e1.substring(0,6)=='simCnd') {
                                simVF = (_.e2 == document.all['simVer' + _.e1.substring(6)].e)
                                    ? document.all['simVer' + _.e1.substring(6)]
                                    : document.all['simFal' + _.e1.substring(6)]
                                simVF.style.left = 
                                    (x_ < document.all[_.e1].style.pixelLeft)
                                    ? (document.all[_.e1].style.pixelLeft - 8) : (x_ + 2)
                                simVF.style.top = 
                                    (y_ < document.all[_.e1].style.pixelTop)
                                    ? (document.all[_.e1].style.pixelTop - 10) : (y_ + 2)
                            }
                        } break
                            case '2' : {
                            _.style.left =   _x
                            _.style.top =    _y
                            _.style.width =  _w
                            _.style.height = (_w == 0) ? _h : 0
                            _.style.borderLeft = (_w == 0) ? 'solid 1px #999999' : ''
                            _.style.borderTop = (_w != 0) ? 'solid 1px #999999' : ''
                            if (_w == 0) 
                                _img = (y1 < y2) ? 2 : 1
                        } break
                        case '3' : {
                            if (b1_ || b2_) { 
                                if (b1_) {
                                    if ((y1 + h1 / 2) < (y2 + h2 / 2) && (x1 + w1 / 2) < (x2 + w2 / 2)) _y = _y + _h
                                    if ((y1 + h1 / 2) < (y2 + h2 / 2) && (x1 + w1 / 2) < (x2 + w2 / 2)) _y = y_ + _h
                                    if ((y1 + h1 / 2) < (y2 + h2 / 2) && (x1 + w1 / 2) < (x2 + w2 / 2)) _x = x_ + _w
                                    if ((y1 + h1 / 2) < (y2 + h2 / 2) && (x1 + w1 / 2) > (x2 + w2 / 2)) _x = _x - w_
                                    if ((y1 + h1 / 2) > (y2 + h2 / 2) && (x1 + w1 / 2) > (x2 + w2 / 2)) _x = _x - w_
                                    if ((y1 + h1 / 2) > (y2 + h2 / 2) && (x1 + w1 / 2) < (x2 + w2 / 2)) _x = x_ + _w
                                    if ((y1 + h1 / 2) > (y2 + h2 / 2) && (x1 + w1 / 2) >= (x2 + w2 / 2)) _y = _y - h_
                                    if ((y1 + h1 / 2) > (y2 + h2 / 2) && (x1 + w1 / 2) < (x2 + w2 / 2)) _y = y_ - h_
                                } else {
                                    if ((y1 + h1 / 2) < (y2 + h2 / 2) && (x1 + w1 / 2) < (x2 + w2 / 2)) _y = _y + _h
                                    if ((y1 + h1 / 2) < (y2 + h2 / 2) && (x1 + w1 / 2) >= (x2 + w2 / 2)) _y = _y + _h
                                    if ((y1 + h1 / 2) < (y2 + h2 / 2) && (x1 + w1 / 2) > (x2 + w2 / 2)) _x = _x - w_
                                    if ((y1 + h1 / 2) > (y2 + h2 / 2) && (x1 + w1 / 2) > (x2 + w2 / 2)) _x = _x - w_
                                    if ((y1 + h1 / 2) >= (y2 + h2 / 2) && (x1 + w1 / 2) > (x2 + w2 / 2)) _x = x_ - w_
                                }
                                _.style.left =   _x
                                _.style.top =    _y
                                _.style.width =  w_
                                _.style.height = h_
                                _.style.borderLeft = (w_ == 0) ? 'solid 1px #999999' : ''
                                _.style.borderTop = (w_ != 0) ? 'solid 1px #999999' : ''
                                if (w_ != 0)
                                    _img = (x1 < x2) ? 4 : 3
                                else
                                    _img = (y1 < y2) ? 2 : 1
                            } else {
                                _.style.left = 0
                                _.style.top = 0
                                _.style.width = 0
                                _.style.height = 0
                                _.style.borderLeft = ''
                                _.style.borderTop = ''
                            }
                        } break
                            case '4' : {
                                switch (_img) {
                                    case 1 : {
                                        _.style.left = x2 + w2 / 2 - 3
                                        _.style.top = y2 + h2 + 1
                                        _.src = 'imagenes/reglas/flechaarr.gif'
                                    } break
                                    case 2 : {
                                        _.style.left = x2 + w2 / 2 - 3
                                        _.style.top = y2 - 8
                                        _.src = 'imagenes/reglas/flechaaba.gif'
                                    } break
                                    case 3 : {
                                        _.style.left = x2 + w2 + 1
                                        _.style.top = y2 + h2 / 2 - 3
                                        _.src = 'imagenes/reglas/flechaizq.gif'
                                    } break
                                    case 4 : {
                                        _.style.left = x2 - 8
                                        _.style.top = y2 + h2 / 2 - 3
                                        _.src = 'imagenes/reglas/flechader.gif'
                                    } break
                                }
                        } break
                    }
                }
    } catch(e){}
    bEnlace = false
}//MoverEnlace

function MoverOptions(objeto1, objeto2) {
    var option = 0
    if (objeto1.options.length)
        while (option < objeto1.options.length) {
            if (objeto1.options[option].selected) {
                objeto2.options[objeto2.options.length] =
                    new Option(objeto1.options[option].text, objeto1.options[option].value)
                objeto1.options[option] = null
                option=-1
            }
            option++
        }
}

function MoverTrueFalse() {
    if (z.id.substring(0,6)=='simCnd') {
        simVer = document.all['simVer' + z.id.substring(6)]
        if (simVer && z.e1 == '') {
            simVer.style.pixelLeft = z.style.pixelLeft + z.style.pixelWidth
            simVer.style.pixelTop = z.style.pixelTop + 20
        }
        simFal = document.all['simFal' + z.id.substring(6)]
        if (simFal && z.e2 == '') {
            simFal.style.pixelLeft = z.style.pixelLeft + (z.style.pixelWidth / 2) + 3
            simFal.style.pixelTop = z.style.pixelTop + 40
        }
    }
}//MoverTrueFalse

function NoExisteEnSelect(valor, objeto) {
    resultado = true
    for (var option=0; option<objeto.options.length; option++)
        if (objeto.options[option].text == valor)
            resultado = false
    return resultado
}//NoExisteEnSelect

function ObtenerEnlaces(pControl) {
    var sActs = ''
    try {
        for (var i=0; i<oDoc.divArea.childNodes.length; i++) {
            if (oDoc.divArea.children.item(i).id.substring(0,6)=='simAct')
                if (pControl.id != oDoc.divArea.children.item(i).id)
                    sActs += (Trim(oDoc.divArea.children.item(i).id) != '' && Trim(oDoc.divArea.children.item(i).innerText) != '')
                        ? (oDoc.divArea.children.item(i).id + '~' + oDoc.divArea.children.item(i).innerText + '~') : ''
            
            if (oDoc.divArea.children.item(i).id.substring(0,6)=='simOut' && pControl.id.substring(0,6)=='simIn ')
                if (pControl.id != oDoc.divArea.children.item(i).id)
                    sActs += (Trim(oDoc.divArea.children.item(i).id) != '' && Trim(oDoc.divArea.children.item(i).innerText) != '')
                        ? (oDoc.divArea.children.item(i).id + '~' + oDoc.divArea.children.item(i).innerText + '~') : ''
        }
    } catch(e) {}
    return sActs.substring(0, sActs.length-1)
}//ObtenerEnlaces

function ObtenerEtiquetas() {
    oDoc = document.all
    resultado = ''

    for (var i=0; i<ID__Etq; i++) {
       if (z = document.all['simEtq' + i]) {
           resultado += ((resultado != '') ? '|' : '') + z.innerText + '|' + z.id
       }
    }
    return resultado
}//ObtenerEtiquetas

function ObtenerIdx(objeto) {
    idx = -1
    while (++idx < objeto.length && objeto.nombre[idx] != undefined) {}
    return idx
}//ObtenerNivel

function ObtenerOptions(objeto, separador) {
    resultado = ''
    if (objeto.options) if (objeto.options.length)
        for (var option=0; option < objeto.options.length; option++)
             resultado += objeto.options[option].text + ((option < objeto.options.length-1) ? separador : '')
    return resultado
}//ObtenerOptions

function ObtenerVariablesSeleccionadas(objeto) {
    var options = ''
    if (objeto.options.length)
        for (var option=0; option < objeto.options.length; option++)
            if (objeto.options[option].selected)
                options += objeto.options[option].value.substring(0, objeto.options[option].value.indexOf('|')) +  ', '
    options = (options.indexOf(',') > -1) ? options.substring(0, options.length - 2) : options
    return options
}//ObtenerVariables

function OcultarMenu(pTipo) {
    var iMargen = 20
    try {
        iX = oDoc.divMenu.style.pixelLeft
        iY = oDoc.divMenu.style.pixelTop
        iW = oDoc.divMenu.clientWidth 
        iH = oDoc.divMenu.clientHeight
        if (pTipo == 0 || event.x < iX - iMargen || event.x > (Math.abs(iX) + Math.abs(iMargen) + Math.abs(iW)) || event.y < iY - iMargen || event.y > (Math.abs(iY) + Math.abs(iMargen) + Math.abs(iH))) {
            oDoc.divMenu.style.display = "none"
        }
    } catch (e) {}
}//OcultarMenu

function OcultarDiv(div) {
    div.style.display='none'
}//OcultarDiv

function SeleccionarOption(objeto, valor) {
    if (objeto.options.length)
        for (var option=0; option < objeto.options.length; option++)
            if (objeto.options[option].text == valor)
                objeto.options[option].selected = true
}//SeleccionarOption

function SuprStat() {
    self.status = ''
    try {
        parent.self.status = ''
        parent.parent.self.status = ''
        parent.parent.parent.self.status = ''
    } catch(e){}
}//SuprStat

function Tecla() {
    OcultarMenu(0)
    tecla = event.keyCode
    if (tecla > 116 && tecla < 124) {
        document.all.divCampos.style.display = 'none'
        document.all.divPreferencias.style.display = 'none'
        document.all.divVariables.style.display = 'none'
        if (document.all.divHTML.style.display == 'none') {
            if (tecla == 118) {
                document.all.divHTML.innerText = document.all.divRegla.innerHTML
            }
            if (tecla == 119) {
                document.all.divHTML.innerText = (oControl)
                    ? oControl.outerHTML
                    : 'Haga clic derecho sobre el control que desea ver antes de presionar [F8]'
            }
            if (tecla == 120) {
                document.all.divHTML.innerText = document.all.divArea.innerHTML
            }
            document.all.divHTML.style.display = ''
        } else {
            document.all.divHTML.style.display = 'none'
        }
    }
}

function Trim(pCadena) {
    return pCadena.replace(/^\s*|\s*$/g,'')
}//Trim

function VerificarConsecutivo(pControl, pDefault)
{
  if (document.all.hidConsecutivos.value.indexOf('[' + pControl.value + ']') > -1)
  {
    alert('Ya hay una actividad con el Consecutivo ' + pControl.value)
    pControl.value = pDefault
    pControl.focus()
  }
}//VerificarConsecutivo

function VerificarEntero(pControl, pMin, pMax, pDefault)
{
  var N = Trim(pControl.value)
  if (!isNaN(parseInt(N)))
  {
    if (N < pMin) N = pMin
    if (N > pMax) N = pMax
    pControl.value = N
    return
  }
  pControl.value = pDefault
}//VerificarEntero

function VerificarVariables(variables) {
    var a = variables.split('|')
    var msg = '', rptd = ''
    for (idx = 0; idx < a.length - 1; idx+=2) {
        if (a[idx].replace(/[a-z]|[A-Z]|[0-9]|[_]/g,'') != '' || a[idx].substring(0, 1).replace(/[a-z]|[A-Z]/g,'') != '')
            msg += a[idx] + '\n'
        for (idxR = 0; idxR < a.length; idxR+=2)
            if (idxR != idx && a[idxR] == a[idx] && ('~' + rptd).indexOf('~' + a[idx] + '~') == -1)
                rptd += a[idx] + '~'
    }
    if (msg + rptd != '') {
        if (msg != '')
            msg =
                'Los nombres de los siguientes identificadores no son correctos:\n\n' + msg + '\n' +
                'Los nombres de identificadores estaran formados de hasta 12 caracteres\n' +
                'alfanum�ricos del "0" al "9", de la "a" a la "z", de la "A" a la "Z" y "_"\n' +
                'iniciando con una letra.'
        if (rptd != '')
            rptd =
                ((msg != '') ? '\n\n' : '') +
                'Los nombres de los siguientes identificadores estan declarados m�s\n' +
                'de una vez:\n\n' + rptd.replace(/~/gi, '\n')
        alert(msg + rptd)
        return false
    }
    return true
}//VerificarVariables

function VerMenu() {
    if (oControl) if (oControl.id.substring(0,3)=='sim'||oControl.id.substring(0,3)=='con') {
        oDoc.mnu0.style.display = (oControl.id=='simInicio'||oControl.id=='simFin'||oControl.id.substring(0,3)=='con'||oControl.id.substring(0,6)=='simOut'||oControl.id.substring(0,6)=='simEtq') ? 'none' : ''
        //oDoc.mnu1.style.display = (oControl.id.substring(0,6)=='simAct') ? '' : 'none'
        oDoc.mnu1.style.display = 'none'
        oDoc.divMenu.style.left = event.x
        oDoc.divMenu.style.top = event.y + document.body.scrollTop
        oDoc.divMenu.style.display = ""
    }
}//VerMenu
//Funcion para validar el Alta de las Reglas para el control de solicitudes
function evalAddReglasSolicitudes(form){
	var errorMsg = "";
    var errorCR  = '\r';
    var contador = 0;
	
	/**/
	var modulo1 = document.forms[form].elements['modulo_1'].value;
	var modulo2 = document.forms[form].elements['modulo_2'].value;
	var modulo3 = document.forms[form].elements['modulo_3'].value;
	var tabla1 = document.forms[form].elements['tabla_1'].value;
	var tabla2 = document.forms[form].elements['tabla_2'].value;
	var tabla3 = document.forms[form].elements['tabla_3'].value;
	var variable1 = document.forms[form].elements['variable_1'].value;
	var variable2 = document.forms[form].elements['variable_2'].value;
	var variable3 = document.forms[form].elements['variable_3'].value;
	var condicion1 = document.forms[form].elements['condicion_1'].value;
	var condicion2 = document.forms[form].elements['condicion_2'].value;
	var condicion3 = document.forms[form].elements['condicion_3'].value;
	var valcat1 = document.forms[form].elements['val_cat_1'].value;
	var valcat2 = document.forms[form].elements['val_cat_2'].value; 
	var valcat3 = document.forms[form].elements['val_cat_3'].value;
	var tpovariable1 = document.forms[form].elements['tpo_variable_1'].value;
	var tpovariable2 = document.forms[form].elements['tpo_variable_2'].value;
	var tpovariable3 = document.forms[form].elements['tpo_variable_3'].value;
	var solicitud1 = document.forms[form].elements['solicitud1'].value;
	var solicitud2 = document.forms[form].elements['solicitud2'].value;
	var solicitud3 = document.forms[form].elements['solicitud3'].value;
	
	if(valcat1.indexOf("|") != -1){
		valcat1 = valcat1.substring(0,valcat1.indexOf("|"));
	}
	if(valcat2.indexOf("|") != -1){
		valcat2 = valcat2.substring(0,valcat2.indexOf("|"));
	}
	if(valcat3.indexOf("|") != -1){
		valcat3 = valcat3.substring(0,valcat3.indexOf("|"));
	}
	
	/**/
	
	if(modulo1 != null && modulo1.length > 0){
		if(tabla1 != null && tabla1.length > 0){
			if(variable1 != null && variable1.length > 0){
				if(condicion1 != null && condicion1.length > 0){
					if(valcat1 != null && valcat1.length > 0){
						if(evaluaDato(valcat1,tpovariable1)){
							if(solicitud1 != null && solicitud1.length > 0){
								
							}else{
								errorMsg += "Debe seleccionar una Solicitud." + errorCR;
						        contador++;
							}
						}else{
							errorMsg += "Error tipo de Dato." + errorCR;
				      		contador++;
						}
					}else{
						errorMsg += "Debe seleccionar un Valor 1." + errorCR;
				        contador++;
					}
				}else{
					errorMsg += "Debe seleccionar una Condicion." + errorCR;
			        contador++;
				}
			}else{
				errorMsg += "Debe seleccionar un nombre de Variable." + errorCR;
		        contador++;
			}
		}else{
			errorMsg += "Debe seleccionar un nombre de Tabla." + errorCR;
	        contador++;
		}
	}
	
	if(modulo2 != null && modulo2.length > 0){
		if(tabla2 != null && tabla2.length > 0){
			if(variable2 != null && variable2.length > 0){
				if(condicion2 != null && condicion2.length > 0){
					if(valcat2 != null && valcat2.length > 0){
						if(evaluaDato(valcat2,tpovariable2)){
							if(solicitud2 != null && solicitud2.length > 0){
								
							}else{
								errorMsg += "Debe seleccionar una Solicitud." + errorCR;
						        contador++;
							}
						}else{
							errorMsg += "Error tipo de Dato Valor 2." + errorCR;
				      		contador++;
						}
					}else{
						errorMsg += "Debe seleccionar un Valor." + errorCR;
				        contador++;
					}
				}else{
					errorMsg += "Debe seleccionar una Condicion." + errorCR;
			        contador++;
				}
			}else{
				errorMsg += "Debe seleccionar un nombre de Variable." + errorCR;
		        contador++;
			}
		}else{
			errorMsg += "Debe seleccionar un nombre de Tabla." + errorCR;
	        contador++;
		}
	}
	
	if(modulo3 != null && modulo3.length > 0){
		if(tabla3 != null && tabla3.length > 0){
			if(variable3 != null && variable3.length > 0){
				if(condicion3 != null && condicion3.length > 0){
					if(valcat3 != null && valcat3.length > 0){
						if(evaluaDato(valcat3,tpovariable3)){
							if(solicitud3 != null && solicitud3.length > 0){
								
							}else{
								errorMsg += "Debe seleccionar una Solicitud." + errorCR;
						        contador++;
							}
						}else{
							errorMsg += "Error tipo de Dato." + errorCR;
				      		contador++;
						}
					}else{
						errorMsg += "Debe seleccionar un Valor 3." + errorCR;
				        contador++;
					}
				}else{
					errorMsg += "Debe seleccionar una Condicion." + errorCR;
			        contador++;
				}
			}else{
				errorMsg += "Debe seleccionar un nombre de Variable." + errorCR;
		        contador++;
			}
		}else{
			errorMsg += "Debe seleccionar un nombre de Tabla." + errorCR;
	        contador++;
		}
	}
	
	var id_tabla1 = document.forms[form].elements['id_tabla_1'].value;
	var id_tabla2 = document.forms[form].elements['id_tabla_2'].value;
	var id_tabla3 = document.forms[form].elements['id_tabla_3'].value;
	var des_tabla1 = document.forms[form].elements['des_tabla_1'].value;
	var des_tabla2 = document.forms[form].elements['des_tabla_2'].value;
	var des_tabla3 = document.forms[form].elements['des_tabla_3'].value;
	var id_variable1 = document.forms[form].elements['id_variable_1'].value;
	var id_variable2 = document.forms[form].elements['id_variable_2'].value;
	var id_variable3 = document.forms[form].elements['id_variable_3'].value;
	
	
	
	if(id_tabla1 != null && id_tabla1.length > 0){
		if(id_tabla2 != null && id_tabla2.length > 0){
			if((id_tabla2 == id_tabla1) && (des_tabla1 == des_tabla2)){
				if(id_variable1 != null && id_variable1.length > 0){
					if(id_variable2 != null && id_variable2.length > 0){
						if(id_variable1 == id_variable2){
							if(condicion1 == condicion2){
								if(solicitud1 == solicitud2){
									errorMsg += "No puede existir la misma Condicion" + errorCR;
						        	contador++;
						        }
							}
						}
					}
				}
			}
		}
	}
	
	if(id_tabla1 != null && id_tabla1.length > 0){
		if(id_tabla3 != null && id_tabla3.length > 0){
			if(id_tabla3 == id_tabla1){
				if(id_variable1 != null && id_variable1.length > 0){
					if(id_variable3 != null && id_variable3.length > 0){
						if(id_variable1 == id_variable3){
							if(condicion1 == condicion3){
								if(solicitud1 == solicitud3){
									errorMsg += "No puede existir la misma Condicion" + errorCR;
						        	contador++;
						        }
							}
						}
					}
				}
			}
		}
	}
		
	if ( contador > 0 ){
        mensaje(errorMsg, contador);
        return false;
    }	
	
	return true;
}
function mensaje(errorMsg, numErrors)
{
    if (!numErrors)
        numErrors = 0;
    switch(numErrors) {
        case 0:     break;
        case 1:     errorMsg = "Se encontr� 1 error: \r\r" + errorMsg; break;
        default:    errorMsg = "Se encontraron " + numErrors + " errores: \r\r" + errorMsg; break;
    }
    alert(errorMsg);
}
function evaluaDato(valor,tipo){
	if(tipo != null ){
	tipo = tipo.toUpperCase();
		if(tipo == 'SMALLINT' || tipo == 'INTEGER' || tipo == 'DECIMAL'){
			if(isInteger(valor)){
				return true;
			}else{
				return false;
			}
		}else{
			if(isString(valor)){
				return true;
			}else{
				return false;
			}
		}
	}
}
function isInteger(val){
	for(var i=0;i<val.length;i++){
		if(!isDigit(val.charAt(i))){
			return false;
		}
	}
return true;
}
function isString(val){
	for(var i=0;i<val.length;i++){
		if(!isChar(val.charAt(i))){
			return false;
		}
	}
return true;
}
function isDigit(num) {
	if (num.length>1){
		return false;
	}
	var string="1234567890";
	if (string.indexOf(num)!=-1){
		return true;
	}
return false;
}
function isChar(ch) {
	if (ch.length>1){
		return false;
	}
	var string="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	if (string.indexOf(ch)!=-1){
		return true;
	}
return false;
}
//Funcion para validar campos
function evalCamposMascaras(form,pantalla){
var errorMsg = "";
var errorCR  = '\r';
var contador = 0;
var nombre = document.forms[form].elements['Nombre'].value;
var apPat = document.forms[form].elements['APaterno'].value;
var apMat = document.forms[form].elements['AMaterno'].value;
var rfc = document.forms[form].elements['RFC'].value;
var fecNac = document.forms[form].elements['FechaNacimiento'].value;
var calle = document.forms[form].elements['Calle'].value;
var colonia = document.forms[form].elements['Colonia'].value;
var CP = document.forms[form].elements['Cp'].value;



	if(nombre.length > 0){
		if(!encuentraPunto(nombre)){
			errorMsg += "El Nombre no puede Contener Punto." + errorCR;
	        contador++;
		}
		if(!encuentraDobleEspacio(nombre)){
			errorMsg += "El Nombre no puede Contener Doble Espacio." + errorCR;
	        contador++;
		}
		if(pantalla == 'GRABA'){
			if(nombre.length < 2){
				errorMsg += "El Nombre no puede ser longitud 1." + errorCR;
	        	contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(nombre.length < 2){
				errorMsg += "El Nombre no puede ser longitud 1." + errorCR;
	        	contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(!encuentraConsecutivos(nombre)){
				errorMsg += "El Nombre excede el numero de caracteres repetidos." + errorCR;
	       		contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(!esCaracterValido(nombre)){
				errorMsg += "El Nombre tiene caracteres no registrados." + errorCR;
	       		contador++;
			}
		}
	}
	if(apPat.length > 0){
		if(!encuentraPunto(apPat)){
			errorMsg += "El Apellido Paterno no puede Contener Punto." + errorCR;
	        contador++;
		}
		if(!encuentraDobleEspacio(apPat)){
			errorMsg += "El Apellido Paterno no puede Contener Doble Espacio." + errorCR;
	        contador++;
		}
		if(pantalla == 'GRABA'){
			if(apPat.length < 2){
				errorMsg += "El Apellido Paterno no puede ser longitud 1." + errorCR;
	        	contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(!encuentraConsecutivos(apPat)){
				errorMsg += "El Apellido Paterno excede el numero de caracteres repetidos." + errorCR;
	       		contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(!esCaracterValido(apPat)){
				errorMsg += "El Apellido Paterno tiene caracteres no registrados." + errorCR;
	       		contador++;
			}
		}
	}
	if(apMat.length > 0){
		if(!encuentraPunto(apMat)){
			errorMsg += "El Apellido Materno no puede Contener Punto." + errorCR;
	        contador++;
		}
		if(!encuentraDobleEspacio(apMat)){
			errorMsg += "El Apellido Materno no puede Contener Doble Espacio." + errorCR;
	        contador++;
		}
		if(pantalla == 'GRABA'){
			if(apMat.length < 2){
				errorMsg += "El Apellido Materno no puede ser longitud 1." + errorCR;
	        	contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(!encuentraConsecutivos(apMat)){
				errorMsg += "El Apellido Materno excede el numero de caracteres repetidos." + errorCR;
	       		contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(!esCaracterValido(apMat)){
				errorMsg += "El Apellido Materno tiene caracteres no registrados." + errorCR;
	       		contador++;
			}
		}
	}
	if(rfc.length > 0){
		if(pantalla == 'GRABA'){
			if(rfc.length < 8){
				errorMsg += "Capture RFC completo" + errorCR;
	       		contador++;
			}else{
	       		if(!validaRFC(rfc)){
					errorMsg += " El RFC no es correcto o \n La Fecha del RFC no es Correcta o " +
					"\n o La segunda letra del RFC debe ser Vocal"
					+ errorCR;
	       			contador++;
				}
			}
			if(fecNac.length > 7){
				fecNac = eliminaCaracter(fecNac);
				if(!validaFechas(rfc,fecNac)){
					errorMsg += "Fecha de Nacimiento no coincide con RFC" + errorCR;
		       		contador++;
				}
			}else{
				errorMsg += "Capture Fecha de Nacimiento" + errorCR;
	       		contador++;
			}
		}
	}else{
		if(pantalla == 'GRABA'){
			errorMsg += "Capture RFC" + errorCR;
		    contador++;
		}
	}
	if(calle.length > 0){
		if(pantalla == 'GRABA'){
			if(!encuentraDobleEspacio(calle)){
				errorMsg += "La calle no puede Contener Doble Espacio." + errorCR;
		        contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(calle.length < 2){
				errorMsg += "La calle no puede ser longitud 1." + errorCR;
	        	contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(!encuentraConsecutivos(calle)){
				errorMsg += "La calle excede el numero de caracteres repetidos." + errorCR;
	       		contador++;
			}else if(!encuentraConsecutivosDireccion(calle)){
				errorMsg += "La calle excede el numero de caracteres repetidos." + errorCR;
	       		contador++;
			}
		}
	}else{
		if(pantalla == 'GRABA'){
			errorMsg += "Capture Calle" + errorCR;
		    contador++;
		}
	}
	if(colonia.length > 0){
		if(pantalla == 'GRABA'){
			if(!encuentraDobleEspacio(colonia)){
				errorMsg += "La colonia no puede Contener Doble Espacio." + errorCR;
		        contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(colonia.length < 2){
				errorMsg += "La colonia no puede ser longitud 1." + errorCR;
	        	contador++;
			}
		}
		if(pantalla == 'GRABA'){
			if(!encuentraConsecutivos(colonia)){
				errorMsg += "La colonia excede el numero de caracteres repetidos." + errorCR;
	       		contador++;
			}else if(!encuentraConsecutivosDireccion(colonia)){
				errorMsg += "La colonia excede el numero de caracteres repetidos." + errorCR;
	       		contador++;
			}
		}
	}else{
		if(pantalla == 'GRABA'){
			errorMsg += "Capture Colonia" + errorCR;
		    contador++;
		}
	}
	if(CP.length < 1){
		if(pantalla == 'GRABA'){
			errorMsg += "Capture Codigo Postal" + errorCR;
		    contador++;
		}
	}
	if ( contador > 0 ){
        mensaje(errorMsg, contador);
        return false;
    }
return true;
}
function encuentraPunto(valor){
	if(valor.indexOf('.') == -1){
		return true;
	}else{
		return false;
	}
}
function encuentraDobleEspacio(valor){
	if(valor.indexOf('  ') == -1){
		return true;
	}else{
		return false;
	}
}
function encuentraConsecutivos(valor){
	contador = 0;
	for(var i=0;i<valor.length;i++){
	var letra = valor.charAt(i);
		if(letra == valor.charAt(i+1)){
			contador++;
		}else{
			contador = 0;
		}
		if(contador >= 2){
			return false;
		}
	}
return true;
}
function esCaracterValido(valor){
	for(var i=0;i<valor.length;i++){
		if(!isValidaCadena(valor.charAt(i))){
			return false;
		}
	}
return true;
}
function isValidaCadena(ch) {
	if (ch.length>1){
		return false;
	}
	var string="ABCDEFGHIJKLMN�OPQRSTUVWXYZ0123456789,#@&*-abc ";
	if (string.indexOf(ch)!=-1){
		return true;
	}
return false;
}
function validaRFC(valor){
	for(var i=0;i<4;i++){
		if(!isChar(valor.charAt(i))){
			return false;
		}
		if(i==1){
			if(!esVocal(valor.charAt(i))){
				return false;
			}
		}
	}
	for(var i=4;i<8;i++){
		if(!isDigit(valor.charAt(i))){
			return false;
		}
	}
return true;
}
function esVocal(ch){
	var string="AEIOUaeiou";
	if (string.indexOf(ch)!=-1){
		return true;
	}
return false;
}
function eliminaCaracter(fecha){
	if(fecha.indexOf('/') != -1){
		var cadena = fecha.substring(0,fecha.indexOf("/"));
		fecha = fecha.substring(fecha.indexOf("/")+1,fecha.length);
		cadena = cadena + fecha.substring(0,fecha.indexOf("/"));
		fecha = fecha.substring(fecha.indexOf("/")+1,fecha.length);
		cadena = cadena + fecha.substring(0,fecha.length);
		fecha = cadena;
	}
return fecha;
}
function validaFechas(rfcFecha,fecNac){
	if(rfcFecha.substring(4,6) == fecNac.substring(6,8)){
		if(rfcFecha.substring(6,8) == fecNac.substring(2,4)){
			if(rfcFecha.substring(8,10) == fecNac.substring(0,2)){
			var mes = rfcFecha.substring(6,8);
			var dia = rfcFecha.substring(8,10);
				if(dia > 0 && dia < 32){
					if(mes > 0 && mes < 13){
						return true;
					}
				}
			}
		}
	}
return false;
}
function encuentraConsecutivosDireccion(valor){
	contador = 0;
	for(var i=0;i<valor.length;i++){
	var letra = valor.charAt(i);
		if(letra == valor.charAt(i+1)){
			contador++;
		}else{
			contador = 0;
		}
		if(contador >= 2){
			return false;
		}
	}
return true;
}