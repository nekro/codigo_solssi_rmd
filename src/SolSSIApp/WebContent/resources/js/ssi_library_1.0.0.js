/********************************************************************
 * REGLAS DE NEGOCIO                                 Version 1.0.0  *
 * ---------------------------------------------------------------- *
 * Autor: GCIT                                                      *
 * Para : MetLife                                                   *
 * ---------------------------------------------------------------- *
 * Funciones para ejecutar reglas de negocio basicas.               *
 ********************************************************************/
var js_solicitud = {

	showNSS : function(band){
		if(band){
			document.getElementById("divNSSLabel").style.display = "";
			document.getElementById("divNSS").style.display = "";
		}else{
			document.getElementById("divNSSLabel").style.display = "none";
			document.getElementById("divNSS").style.display = "none";
		}
	},

    showExtension : function(obj){
		if(obj.value != ''){
			document.getElementById("divExtensionLabel").style.display = "";
			document.getElementById("divExtension").style.display = "";
		}else{
			document.getElementById("divExtensionLabel").style.display = "none";
			document.getElementById("divExtension").style.display = "none";
		}
   },
	
   showCorreoPersonal : function(obj){
		if(obj.value != ''){
			document.getElementById("divCorreoPersonalLabel").style.display = "";
			document.getElementById("divCorreoPersonal").style.display = "";
			//document.getElementById("correoPersonalConfirm").focus();
		}else{
			document.getElementById("divCorreoPersonalLabel").style.display = "none";
			document.getElementById("divCorreoPersonal").style.display = "none";
		}
   },
   
   showCorreoTrabajo : function(obj){
		if(obj.value != ''){
			document.getElementById("divCorreoTrabajoLabel").style.display = "";
			document.getElementById("divCorreoTrabajo").style.display = "";
			//document.getElementById("correoTrabajoConfirm").focus();
		}else{
			document.getElementById("divCorreoTrabajoLabel").style.display = "none";
			document.getElementById("divCorreoTrabajo").style.display = "none";
		}
  },
  
  checkPaisNacimiento : function (value, nacionalidad){
		 
		 if(value == 1){
			 document.getElementById("divEstadoNacExtranjero").style.display = "none";
			 document.getElementById("divEstadoNacMexico").style.display = "";
		 }else{
			 document.getElementById("divEstadoNacMexico").style.display = "none";
			 document.getElementById("divEstadoNacExtranjero").style.display = "";
		 }
		 
		 if(nacionalidad){
		 	var comboNacionalidad = document.getElementById("idNacionalidad");
		 	comboNacionalidad.value = document.getElementById("idPaisNac").value;
		 	dependientes();
		 }	
	},
	
	
  otrasFormasPago : function (radio){
		if(radio.value == 2){
			document.getElementById("divOtraFormaPago").style.display = '';
		}else{
			document.getElementById("divOtraFormaPago").style.display = 'none';
		}
	},
	
	modoPagoCheque : function(check){
		if(check.checked){
			document.getElementById("divModoPagoCheque").style.display = '';
		}else{
			document.getElementById("divModoPagoCheque").style.display = 'none';
		}
	},
	
	showMessageAvisoPrivacidad : function(check){
		 if(check){
			 document.getElementById("avisoPrivacidadRequired").innerHTML = "&nbsp;";
		 }else{
			 document.getElementById("avisoPrivacidadRequired").innerHTML = "A�n no ha aceptado el aviso de privacidad";
		 }
	 }
	 
};

function doValidate(form){
	if(form == 'formInicioTramite') {
		   //alert('doValidate: in formInicioTramite');
		   var rfc = document.getElementById('rfc');
		   var cuenta = document.getElementById('cuenta');
		   
		   var obligatorioRfc = false;
		   if(cuenta.value == ''){
			   obligatorioRfc = true;   
		   }else{
			   obligatorioRfc = false;
		   }
		   
		   var retRFC = validRFC(rfc, obligatorioRfc, 'Capture RFC y/o Cuenta');
		   var retHomoclave =validRFCHomoclave(document.getElementById('homoclave'));  
           var retCuenta =validCuenta(document.getElementById('cuenta'));
           
             
           if (retRFC && retCuenta && retHomoclave) {
        	   //document.forms["inicio"].submit(); 
               return true;
           }
           
	       return false;
	}	
	
	return true;
};


function doValidateConsultaSaldo(){
		   var rfc = document.getElementById('rfc');
		   var cuenta = document.getElementById('cuenta');
		   var monto = document.getElementById('ultimoDescuento').value;
		   
		   if(monto == '' || rfc.value == '' || cuenta.value == ''){
			   document.getElementById('errores_saldo').innerHTML='Para consultar su saldo debe proporcionar todos los datos solicitados.';
			   return false;
		   }else{
		   	   document.getElementById('errores_saldo').innerHTML='';
		   }
		   
		   obligatorioRfc = false;   

		   var retRFC = validRFC(rfc, obligatorioRfc, '');
           var retCuenta =validCuenta(document.getElementById('cuenta'));

             
           if (retRFC && retCuenta) {
               return true;
           }
           
	       return false;
};

	

/********************************************************************
 * FUNCIONES DE VALIDACION                           Version 1.0.0  *
 * ---------------------------------------------------------------- *
 * Autor: GCIT                                                      *
 * Para : Metlife                                                   *
 * ---------------------------------------------------------------- *
 * Funciones para validar los campos de captura.                    *
 ********************************************************************/

function validCorreoPersonal(obj) {
	 if (obj.value != '') {
		 var correo = document.getElementById('correoPersonal');
		 if (correo.value != obj.value) {
			 document.getElementById('error_correoPersonal').innerHTML='El Correo Personal no coincide';
			 document.getElementById('error_correoPersonal').focus();
			 return false;
	     } else {
	    	 document.getElementById('error_correoPersonal').innerHTML='';
	     }
	 } else {
		     document.getElementById('error_correoPersonal').innerHTML='';
	 } 
     
	 return true;
};

function validCorreoTrabajo(obj) {
	 if (obj.value != '') {
		 var correo = document.getElementById('correoTrabajo');
		 if (correo.value != obj.value) {
			 document.getElementById('error_correoTrabajo').innerHTML='El Correo de Trabajo no coincide';
			 document.getElementById('error_correoTrabajo').focus();
			 return false;
	     } else {
	    	 document.getElementById('error_correoTrabajo').innerHTML=''; 
	     }
	 } else {
		     document.getElementById('error_correoTrabajo').innerHTML='';
	 } 
     
	 return true;
};

function validRFC(obj, obligatorio, msg) {
  
		
		  if (obligatorio && obj.value=='') {
        	     document.getElementById('error_rfc').innerHTML=msg;
				 document.getElementById('error_rfc').focus();
			     return false;
           } 
			
   
           if (!obligatorio && obj.value=='') {
        	   
        	   document.getElementById('error_rfc').innerHTML=''; 
		       return true;
           } 
   
           if (obj.value!='') 	{
           	
           var rfc = obj.value;
		   
           var letters = rfc.substring(0,4);
		    var numbers = rfc.substr(4,10);
			p1 = /^[A-Za-z]{4}$/;
			p2 = /^[0-9]{6}$/;
			if(p1.test(letters)){
			   if(p2.test(numbers)){
				   document.getElementById('error_rfc').innerHTML=''; 
			       return true;
			   } else {
				 document.getElementById('error_rfc').innerHTML='El formato del RFC es incorrecto, favor de verificarlo.';
				 document.getElementById('error_rfc').focus();
			     return false;
			   } 
			} else {
			    document.getElementById('error_rfc').innerHTML='El formato del RFC es incorrecto, favor de verificarlo.';
				document.getElementById('error_rfc').focus();
			    return false;
			}

           }           
};

function validRFCHomoclave(obj) {
     
	
	if (obj.value!='') {
		    
			var v = validRFC(document.getElementById('rfc'), false);
			if(v){
	        	error = "error_rfc";
	        	document.getElementById('error_homoclave').innerHTML='';
	        }else{
	        	error = "error_homoclave";
	        }
	        
			var homoclave = obj.value;
	        
	        p1 = /^[A-Za-z0-9]{3}$/;
	        if(p1.test(homoclave)){
		          document.getElementById(error).innerHTML=''; 
		          return true;
	        } else {
		          document.getElementById(error).innerHTML='Longitud de homoclave debe ser 3';
		          document.getElementById(error).focus();
		          return false;
	        } 
	
	 } else {
		   document.getElementById('error_homoclave').innerHTML=''; 
           return true;
	 }
 
};

function validCuenta(obj) {
    
	 if (obj.title=='Obligatorio' && obj.value=='') {
	  	   
	  	   document.getElementById('error_cuenta').innerHTML='Capture Cuenta';
				 document.getElementById('error_cuenta').focus();
			     return false;
	  	   
	  	   
	     } 

	     if (obj.title!='Obligatorio' && obj.value=='') {
	  	   
	  	   document.getElementById('error_cuenta').innerHTML=''; 
		       return true;
	  	   
	  	   
	     } 

	      
	     if (obj.value!='') {
    var cuenta = obj.value;
    //alert('CUENTA='+cuenta);
    
    
    
	//var letters = rfc.substring(0,4);
    //var numbers = rfc.substr(4,10);
	p1 = /^[0-9]{10}$/;
	//p2 = /^[0-9]{6}$/;
	//if(p1.test(letters)){
	   if(p1.test(cuenta)){
		   document.getElementById('error_cuenta').innerHTML='';
		   return true;
	   } else {
	     //alert(mensaje);
		   document.getElementById('error_cuenta').innerHTML='El formato de la cuenta SSI es incorrecto, favor de verificarlo';
		   document.getElementById('error_cuenta').focus();
		 return false;
	   } 
	//} else {
	//   alert(mensaje);
	//   return false;
	//}
	}

    
};

function validMonto(){
	var monto = document.getElementById('ultimoDescuento').value;
	if(monto == ''){
		document.getElementById('error_monto').innerHTML='Capture el monto del �ltimo descuento';
		return false;
	}else{
		document.getElementById('error_monto').innerHTML='';
		return true;
	}
}

function validCURP() {
    var curp = document.getElementById('curp').value;
    if (curp!='') {
	var mensaje = 'El formato de [CURP] es incorrecto.';
	//var letters = rfc.substring(0,4);
    //var numbers = rfc.substr(4,10);
	p1 = /^[A-Za-z]{4}[0-9]{6}[A-Za-z]{6}[0-9]{2}$/;
	//p2 = /^[0-9]{6}$/;
	//if(p1.test(letters)){
	   if(p1.test(curp)){
	      return true;
	   } else {
	     alert(mensaje);
		 return false;
	   } 
	//} else {
	//   alert(mensaje);
	//   return false;
	//}
	}
};

function validCorreo(valor) {
    //var curp = document.getElementById('curp').value;
    if (valor!='') {
	var mensaje = 'El formato de [Correo] es incorrecto.';
	//var letters = rfc.substring(0,4);
    //var numbers = rfc.substr(4,10);
	p1 = /^[A-Za-z0-9]{1}[.\S]+[A-Za-z0-9]{1}\@{1}[A-Za-z0-9]+\.{1}[A-Za-z0-9]+$/;
	//p2 = /^[0-9]{6}$/;
	//if(p1.test(letters)){
	   if(p1.test(valor)){
	      return true;
	   } else {
	     alert(mensaje);
		 return false;
	   } 
	//} else {
	//   alert(mensaje);
	//   return false;
	//}
	}
};



function checkPaisDomicilio(value){
	 if(value == 1){
		 document.getElementById("divEstadoExtranjero").style.display = "none";
		 document.getElementById("divEstadoMexico").style.display = "";
		 document.getElementById("divMunicipioExtranjero").style.display = "none";
		 document.getElementById("divMunicipioMexico").style.display = "";
	 }else{
		 document.getElementById("divEstadoMexico").style.display = "none";
		 document.getElementById("divEstadoExtranjero").style.display = "";
		 document.getElementById("divMunicipioMexico").style.display = "none";
		 document.getElementById("divMunicipioExtranjero").style.display = "";
	 }
}



function validaIdentificacion(field, reset){
	 var numeroIdentificacion = document.getElementById("numeroIdentificacion");
	 
	 numeroIdentificacion.setAttribute("maxlength", 9);
	 if(field.value == 1){
		 numeroIdentificacion.setAttribute("maxlength", 13);
	 }
	 
	 if(field.value == 2){
		 numeroIdentificacion.setAttribute("maxlength", 7);
	 }
	 
	 if(field.value == 3){
		 numeroIdentificacion.setAttribute("maxlength", 10);
	 }
	 
	 if(field.value == 3){
		 numeroIdentificacion.setAttribute("maxlength", 9);
	 }
	 
	 if(reset){
		 numeroIdentificacion.value = '';
	 }
}

function createCURP(){
	 
	 var curp = document.getElementById("curp");
	 
	 if(curp.value != ''){
		 return;
	 }
	 
	 try{
		 var rfc = document.getElementById("rfc");
		 var generoM = document.getElementById("cveGenero1");
		 var generoF = document.getElementById("cveGenero2");
		 var estadoNac = document.getElementById("idEdoNac");
		 
		 
		 
		 var gen = '';
		 if(generoF.checked){
			 gen = 'F';
		 }
		 
		 if(generoM.checked){
			 gen = 'M';
		 }
		 
		 curp.value = rfc.value + gen + (estadoNac.value < 9 ? '0'+estadoNac.value : estadoNac.value);
		 
	 }catch(e){
		 console.log(e);
	 }
}

function loadSubCatalogo(dojo,urlAjax, idCombo, itemSelected, msgError){
	 
	 var select = dojo.byId(idCombo);
	 
	 dojo.xhrGet({
		   url: urlAjax,
		   handleAs: "json",
		   load: function (result) {
			   select.options.length = 0;
			   
			   if(result.length == 0 && msgError != null){
				   select.options[select.options.length] = new Option('Seleccione', 0);
				   alert(msgError);
				   return;
			   }
			   
			   dojo.forEach(result, function(field, i){
				   select.options[select.options.length] = new Option(field.descripcion, field.claveItem);
			   });
			   
			   if(itemSelected > 0){
				   select.value = ''+itemSelected;
			   }
		   }
		});
	 
}

 
 

function validaPorcentajeRescateParcial(field, max){
	if(Number(field.value) > max){
		//document.getElementById("msgPorcentajeRescateParcial").style.display = "block";
		document.getElementById("msgPorcentajeRescateParcial").innerHTML = "M�ximo permitido "+max+" %";
	}else{
		//document.getElementById("msgPorcentajeRescateParcial").style.display = "none";
		document.getElementById("msgPorcentajeRescateParcial").innerHTML = "&nbsp;";
	}
}

function validaPorcentajeAportaciones(field){
	if(Number(field.value) > 100){
		//document.getElementById("msgPorcentajeRescateParcial").style.display = "block";
		document.getElementById("msgPorcentajeAportaciones").innerHTML = "M�ximo permitido 100 %";
	}else{
		//document.getElementById("msgPorcentajeRescateParcial").style.display = "none";
		document.getElementById("msgPorcentajeAportaciones").innerHTML = "&nbsp;";
	}
}


function enableAlfa(push){
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    patron =/[A-Za-z������������\s]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
};

function enableNumero(push){
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    patron = /\d/; // Solo acepta n�meros
        te = String.fromCharCode(tecla);
    return patron.test(te);
};

function enableDecimal(push){
       
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    //patron = /^[0-9]{1,5}(\.[0-9]{0,2})?$/; // Solo acepta n�meros
    patron = /^([0-9])*[.]?[0-9]*$/;
        te = String.fromCharCode(tecla);
    return patron.test(te);
};

function enableAlfaNumero(push){
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    patron = /\w/; //Acepta numeros y letras
        te = String.fromCharCode(tecla);
    return patron.test(te);
};


function enableAlfaNumeroEsp(push){
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    patron = /[A-Za-z0-9������������\s]/; //Acepta numeros y letras
        te = String.fromCharCode(tecla);
    return patron.test(te);
};

function enableAlfaNumeroEsp2(push){
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    patron = /[A-Za-z0-9������������\,\.\-\s]/; //Acepta numeros y letras
        te = String.fromCharCode(tecla);
    return patron.test(te);
};

function disableNumero(push){
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    patron = /\D/; //No acepta numeros
        te = String.fromCharCode(tecla);
    return patron.test(te);
};


function submitForm(form){
	cerrarSession = false;
	showLoading();
	form.submit();
}

function showLoading(){
	document.getElementById("divLoading").style.display = "";
}

 
/********************************************************************
 * FUNCIONES DE UTILERIA                             Version 1.0.0  *
 * ---------------------------------------------------------------- *
 * Autor: GCIT                                                      *
 * Para : Metlife                                                   *
 * ---------------------------------------------------------------- *
 * Funciones comunes de proposito especifico.                       *
 ********************************************************************/

function fillZipCodeFields() {
       //alert('OK'); 
	   document.getElementById("colonia").value = 'COLONIAS';
       document.getElementById("ciudad").value = 'CIUDAD';
       document.getElementById("municipio").value = 'MUNICIPIO';
};
 
 function openWindow(url,name,posIzq,posTop,ancho,alto){
    prop = 'left='+posIzq+',top='+posTop+',width='+ancho+',height='+alto+',';
    newWindow = window.open(url,name,prop+'toolbar=no,minimize=yes,resizable=yes,status=yes,menubar=no,location=no,scrollbars=no');

    return newWindow; 
};

function openWindowCenter(url,name,ancho,alto){
    
     posIzq = (screen.width - ancho)/2;
     posTop = ((screen.height-alto)/2) - 50;
   
    prop = 'left='+posIzq+',top='+posTop+',width='+ancho+',height='+alto+',';
    newWindow = window.open(url,name,prop+'toolbar=no,minimize=yes,resizable=yes,status=yes,menubar=no,location=no,scrollbars=no');

    return newWindow; 
};


function openWindowCenterScroll(url,name,ancho,alto){
    
    posIzq = (screen.width - ancho)/2;
    posTop = ((screen.height-alto)/2) - 50;
  
   prop = 'left='+posIzq+',top='+posTop+',width='+ancho+',height='+alto+',';
   newWindow = window.open(url,name,prop+'toolbar=no,minimize=yes,resizable=yes,status=yes,menubar=no,location=no,scrollbars=yes');

   return newWindow; 
};