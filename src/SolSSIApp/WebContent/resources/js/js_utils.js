

/**enableAlfa
 *Categoria:   Validacion
 *Descripcion: Solo habilita letras y espacios.
 *@push        Evento de presionar una tecla.
 *@return      No retorno
 */
function enableAlfa(push){
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    patron =/[A-Za-z��\s]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/**enableNumero
 *Categoria:   Validacion
 *Descripcion: Solo habilita la captura de numeros
 *@push        Evento de presionar una tecla.
 *@return      No retorno
 */
function enableNumero(push){
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    patron = /\d/; // Solo acepta n�meros
        te = String.fromCharCode(tecla);
    return patron.test(te);
}

/**enableDecimal
 *Categoria:   Validacion
 *Descripcion: Solo habilita la captura de numeros
 *@push        Evento de presionar una tecla.
 *@return      No retorno
 */
function enableDecimal(push){
       
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    //patron = /^[0-9]{1,5}(\.[0-9]{0,2})?$/; // Solo acepta n�meros
    patron = /^([0-9])*[.]?[0-9]*$/;
        te = String.fromCharCode(tecla);
    return patron.test(te);
}

/**enableAlfaNumero
 *Categoria:   Validacion
 *Descripcion: Solo habilita la captura de numeros y letras
 *@push        Evento de presionar una tecla.
 *@return      No retorno
 */
function enableAlfaNumero(push){
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    patron = /\w/; //Acepta numeros y letras
        te = String.fromCharCode(tecla);
    return patron.test(te);
}

/**disableNumero
 *Categoria:   Validacion
 *Descripcion: deshabilita la captura de numeros
 *@push        Evento de presionar una tecla.
 *@return      No retorno
 */
function disableNumero(push){
    tecla = (document.all) ? push.keyCode : push.which;
    if (tecla==8 || tecla==0) return true;
    patron = /\D/; //No acepta numeros
        te = String.fromCharCode(tecla);
    return patron.test(te);
}


/**
  formatea un numero con decimales y comas, punto
*/

function formatNumber(num,prefix){
       
       
        num = Number(num).toFixed(2);
   
        prefix = prefix || '';
    num += '';
    var splitStr = num.split('.');
    var splitLeft = splitStr[0];
    var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
    var regx = /(\d+)(\d{3})/;
    while (regx.test(splitLeft)) {
        splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
    }
   
    return prefix + splitLeft + splitRight;
}

/**
        formatea un numero con decimales y comas, punto
*/

function formatFieldNumber(field,prefix){
  num = formatNumber(field.value);
  field.value = num;
}


function unformatNumber(num) {
    return num.replace(/([^0-9\.\-])/g,'')*1;
}

/**
 * quita el formato al campo
 * @param
 * @returns
 */
function unformatField(field) {
        field.value = field.value.replace(/([^0-9\.\-])/g,'')*1;
    return field.value;
}

/**
   Abre una ventana sin barra de menu
*/
function openWindow(url,name,posIzq,posTop,ancho,alto){
    prop = 'left='+posIzq+',top='+posTop+',width='+ancho+',height='+alto+',';
    newWindow = window.open(url,name,prop+'toolbar=no,minimize=yes,resizable=yes,status=yes,menubar=no,location=no,scrollbars=no');

    return newWindow; 
}

/**
   Abre una ventana sin barra de menu
*/
function openWindowCenter(url,name,ancho,alto){
    
     posIzq = (screen.width - ancho)/2;
     posTop = (screen.height-alto)/2;
   
    prop = 'left='+posIzq+',top='+posTop+',width='+ancho+',height='+alto+',';
    newWindow = window.open(url,name,prop+'toolbar=no,minimize=yes,resizable=yes,status=yes,menubar=no,location=no,scrollbars=no');

    return newWindow; 
}

/**
   Abre una ventana sin barra de menu con scrolls
*/
function openWindowCenterScroll(url,name,ancho,alto){
    
     posIzq = (screen.width - ancho)/2;
     posTop = (screen.height-alto)/2;
   
    prop = 'left='+posIzq+',top='+posTop+',width='+ancho+',height='+alto+',';
    newWindow = window.open(url,name,prop+'toolbar=no,minimize=yes,resizable=yes,status=yes,menubar=no,location=no,scrollbars=yes');

    return newWindow; 
}

/**
   Abre una ventana sin barra de menu que ocupe toda la pantalla
*/
function openWindowFull(url,name){
    
    posIzq = 0;
    posTop = 0;
    ancho = screen.width;
    alto = screen.heigh;
    prop = 'left='+posIzq+',top='+posTop+',width='+ancho+',height='+alto+',';
    newWindow = window.open(url,name,prop+'toolbar=no,minimize=yes,resizable=yes,status=yes,menubar=no,location=no,scrollbars=no');

    return newWindow; 
}

/**
   Abre una ventana que ocupe el porcentaje de la pantalla indicado
*/
function openWindowPorc(url,name,porcentaje){
   
    ancho = (screen.width*porcentaje)/100;
    alto = (screen.height*porcentaje)/100;
   
        posIzq = (screen.width - ancho)/2;
    posTop = (screen.height-alto)/2;
   
    prop = 'left='+posIzq+',top='+posTop+',width='+ancho+',height='+alto+',';
    newWindow = window.open(url,name,prop+'toolbar=no,minimize=yes,resizable=yes,status=yes,menubar=no,location=no,scrollbars=no');

    return newWindow; 
}

/**
   Abre una ventana que ocupe el porcentaje de la pantalla indicado
*/
function openWindowPorcScroll(url,name,porcentaje){
   
    ancho = (screen.width*porcentaje)/100;
    alto = (screen.height*porcentaje)/100;
   
       
        posIzq = (screen.width - ancho)/2;
    posTop = (screen.height-alto)/2;
   
    prop = 'left='+posIzq+',top='+posTop+',width='+ancho+',height='+alto+',';
    newWindow = window.open(url,name,prop+'toolbar=no,minimize=yes,resizable=yes,status=yes,menubar=no,location=no,scrollbars=yes');

    return newWindow; 
}

/**
 * abre una ventana en la esquina superior izquierda
*/
function openWindowDef(url,name,ancho,alto){
    prop = 'left=0,top=0,width='+ancho+',height='+alto+',';
    newWindow = window.open(url,name,prop+'toolbar=no,minimize=yes,resizable=yes,status=yes,menubar=no,location=no,scrollbars=no');

    return newWindow; 
}

/*
  abre una ventana con sus propiedades establecidas en yes
*/
function openWindowWithAll(url,name,posIzq,posTop,ancho,alto){
    prop = 'left='+posIzq+',top='+posTop+',width='+ancho+',height='+alto+',';
    newWindow = window.open(url,name,prop+'toolbar=yes,minimize=yes,resizable=yes,status=yes,menubar=yes,location=yes,scrollbars=yes');

    return newWindow;
}

/* Obtiene el nombre de un archivo de un campo tipo file en base al id del campo

Por ejemplo:
si el valor del campo es "c:\Documents and settings\usuario\reporte.xls"
el resultado que devuelve el m�todo ser�: "reporte.xls"
*/
function getFileName(id){
           obj = document.getElementById(id);
           path= obj.value;               
           path = path.replace(/\\/g,"\/"); 
                  
           if(path.indexOf("\/")!=-1)
                    fileName = path.substring(path.lastIndexOf("\/")+1,path.length);
           else
                    fileName=path;
                   
            return fileName;
}


/**displayCuentaCaracter
 *Categoria:   Cadenas.
 *Descripcion: Cuenta los carateres de una cadena introducida en un INPUT y pinta en pantalla
 *             la cuenta actual y por otro lado el maximo permitido.
 *             Hay que declarar una etiqueta <DIV> bajo el INPUT cuyo ID debe ser "progreso_"
 *             mas el nombre del input donde se desplegara la cuenta.
 *@textArea    TextAre o INPUT al cual se le aplica la cuenta.
 *@longitud    Longitud maxima en la cadena de entrada.
 *@return      no retorno
 */
function displayCuentaCaracter(textArea, longitud){
      var progreso = document.getElementById("progreso_" + textArea.name);
      progreso.innerHTML = '(' + (textArea.value.length) + '/' + longitud + ')';
}







