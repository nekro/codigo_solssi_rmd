package mx.com.metlife.solssi.auditoria.domain;

import java.io.Serializable;

import javax.persistence.Column;


public class ControlPK implements Serializable{

	private static final long serialVersionUID = -8412158873987684420L;
	
	@Column(name = "NUM_FOLIO")
	private Integer numFolio;
	
	@Column(name = "CVE_ESTATUS")
	private Short estatus;

	/**
	 * @return the numFolio
	 */
	public Integer getNumFolio() {
		return numFolio;
	}

	/**
	 * @param numFolio the numFolio to set
	 */
	public void setNumFolio(Integer numFolio) {
		this.numFolio = numFolio;
	}

	/**
	 * @return the estatus
	 */
	public Short getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((estatus == null) ? 0 : estatus.hashCode());
		result = prime * result
				+ ((numFolio == null) ? 0 : numFolio.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ControlPK)) {
			return false;
		}
		ControlPK other = (ControlPK) obj;
		if (estatus == null) {
			if (other.estatus != null) {
				return false;
			}
		} else if (!estatus.equals(other.estatus)) {
			return false;
		}
		if (numFolio == null) {
			if (other.numFolio != null) {
				return false;
			}
		} else if (!numFolio.equals(other.numFolio)) {
			return false;
		}
		return true;
	}
	
	
	
}
