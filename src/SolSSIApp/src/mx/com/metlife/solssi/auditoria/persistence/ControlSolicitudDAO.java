package mx.com.metlife.solssi.auditoria.persistence;

import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.ControlPK;
import mx.com.metlife.solssi.auditoria.domain.ControlSolicitud;
import mx.com.metlife.solssi.auditoria.domain.ControlSolicitudEsquema;
import mx.com.metlife.solssi.main.dao.GenericDAO;

public interface ControlSolicitudDAO extends GenericDAO<ControlSolicitud, ControlPK>{
	
	public List<ControlSolicitud> getDetalle(Integer folio);
	
	public List<ControlSolicitudEsquema> getDetalleEsquemas(Integer folio);

}
