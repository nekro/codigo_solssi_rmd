package mx.com.metlife.solssi.auditoria.domain;

import javax.persistence.Entity;
import javax.persistence.IdClass;
import javax.persistence.Table;


@Entity
@Table( name = "TW_CTRL_SOLICITUD_ESQUEMAS")
@IdClass(ControlPK.class)
public class ControlSolicitudEsquema extends ControlSolicitud{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1210787090723116603L;
	
}
