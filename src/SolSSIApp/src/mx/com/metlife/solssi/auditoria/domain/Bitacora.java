package mx.com.metlife.solssi.auditoria.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TW_BITACORA")
public class Bitacora {
	
	@Id
	@Column(name = "FECHA_MOV")
	private Date fecha;
	
	@Column(name = "CVE_MOV")
	private Short movimiento;
	
	@Column(name = "CVE_USUARIO")
	private String usuario;
	
	private String detalle;

	/**
	 * @return el fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha el fecha a establecer
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	/**
	 * @return el detalle
	 */
	public String getDetalle() {
		return detalle;
	}

	/**
	 * @param detalle el detalle a establecer
	 */
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	/**
	 * @return el movimiento
	 */
	public Short getMovimiento() {
		return movimiento;
	}

	/**
	 * @param movimiento el movimiento a establecer
	 */
	public void setMovimiento(Short movimiento) {
		this.movimiento = movimiento;
	}

	/**
	 * @return el usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario el usuario a establecer
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	
	
	
}
