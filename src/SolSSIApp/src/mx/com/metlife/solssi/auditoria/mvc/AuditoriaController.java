package mx.com.metlife.solssi.auditoria.mvc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.Bitacora;
import mx.com.metlife.solssi.auditoria.domain.ControlSolicitud;
import mx.com.metlife.solssi.auditoria.domain.ControlSolicitudEsquema;
import mx.com.metlife.solssi.auditoria.service.BitacoraService;
import mx.com.metlife.solssi.auditoria.service.ControlSolicitudService;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.esquemas.service.SolicitudEsquemaService;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.solicitud.service.SolicitudService;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.usuario.service.UsuarioService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("logs")
public class AuditoriaController {
	
	private static final int MAX_RESULTS_WARNING = 800;
	
	@Autowired
	private ControlSolicitudService controlSolicitudService;
	
	@Autowired
	private SolicitudService solicitudService;
	
	@Autowired
	private SolicitudEsquemaService solicitudEsqService;
	
	@Autowired
	private BitacoraService bitacoraService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value = "solicitud", method = RequestMethod.POST)
	public String detalleSolicitud(@RequestParam("numFolio") Integer folio, Model model){
		
		Solicitud sol = solicitudService.get(folio);
		
		List<ControlSolicitud> movs = controlSolicitudService.getDetalle(folio);
		model.addAttribute("movimientos", movs);
		model.addAttribute("solicitud", sol);
		
		return "historicos/solicitud";
	}
	
	
	@RequestMapping(value = "solicitud/esquema", method = RequestMethod.POST)
	public String detalleSolicitudEsquema(@RequestParam("numFolio") Integer folio, Model model){
		
		SolicitudEsquema sol = solicitudEsqService.get(folio);
		
		List<ControlSolicitudEsquema> movs = controlSolicitudService.getDetalleEsquemas(folio);
		model.addAttribute("movimientos", movs);
		model.addAttribute("solicitud", sol);
		
		return "historicos/solicitud";
	}
	
	
	
	@RequestMapping(method = RequestMethod.POST)
	public String showSearchPage(Model model){
		return "historicos/bitacora";
	}
	
	@RequestMapping(value = "search", method = RequestMethod.POST)
	public String searchMovimientos(@RequestParam("fechaInicio") String fechaInicio, 
									@RequestParam("fechaFin") String fechaFin, 
									@RequestParam("movimiento") Short movimiento,
									Model model){
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		try{
		
			Date ini = null;
			Date fin = null;
			
			if(StringUtils.isEmpty(fechaInicio)){
				ini = Calendar.getInstance().getTime();
			}else{
				ini = fmt.parse(fechaInicio);
			}
			
			if(StringUtils.isEmpty(fechaFin)){
				fin = ini;
			}else{
				fin = fmt.parse(fechaFin);	
			}
			
			if(fin.before(ini)){
				Date aux = ini;
				ini = fin;
				fin = aux;
			}
			
			model.addAttribute("fechaInicio", fmt.format(ini));
			model.addAttribute("fechaFin", fmt.format(fin));
			
			List<Bitacora> list = bitacoraService.consultaBitacora(ini, fin, movimiento);
			
			if(list.size() > MAX_RESULTS_WARNING){
				model.addAttribute("warning", "Demasiados resultados, favor de establecer un periodo de b�squeda mas corto.");
			}
			
			if(list.size() == 0){
				model.addAttribute("error", "No se encontraron resultados");
			}
			
			model.addAttribute("movimientos", list);
		
		}catch(Exception e){
			model.addAttribute("error", e.getMessage());
		}
		
		return "historicos/bitacora";
	}
	
	@RequestMapping("user")
	public @ResponseBody Usuario getUser(@RequestParam("user") String usuario){
		return usuarioService.getUsuario(usuario);
	}
	
	

}
