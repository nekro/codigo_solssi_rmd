package mx.com.metlife.solssi.auditoria.service;

import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.ControlSolicitud;
import mx.com.metlife.solssi.auditoria.domain.ControlSolicitudEsquema;

public interface ControlSolicitudService {
	
	public List<ControlSolicitud> getDetalle(Integer folio);
	
	public List<ControlSolicitudEsquema> getDetalleEsquemas(Integer folio);

}
