package mx.com.metlife.solssi.auditoria.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

import mx.com.metlife.solssi.util.Base26Codec;

import org.apache.commons.lang.StringUtils;

@Entity
@Table( name = "TW_CTRL_SOLICITUD")
@IdClass(ControlPK.class)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class ControlSolicitud implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2031101882432699118L;

	@Id
	@Column(name = "NUM_FOLIO")
	protected Integer numFolio;
	
	@Id
	@Column(name = "CVE_ESTATUS")
	protected Short estatus;
	
	@Column(name = "CVE_USER")
	protected String usuario;
	
	@Column(name = "IP")
	protected String ip;
	
	@Column(name = "FECHA")
	protected Date fecha;
	
	@Transient
	private String userFolio;

	/**
	 * @return the numFolio
	 */
	public Integer getNumFolio() {
		return numFolio;
	}

	/**
	 * @param numFolio the numFolio to set
	 */
	public void setNumFolio(Integer numFolio) {
		this.numFolio = numFolio;
	}

	/**
	 * @return the estatus
	 */
	public Short getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the usuario
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return el ip
	 */
	public String getIp() {
		return StringUtils.substring(ip,20);
	}

	/**
	 * @param ip el ip a establecer
	 */
	public void setIp(String ip) {
		this.ip = ip;
	}

	
	/**
	 * 
	 * @return
	 */
	public String getUserFolio() {
		if(this.userFolio == null && this.numFolio != null){
			this.userFolio = Base26Codec.encodeFolioSolicitud(this.numFolio);
		}
		return this.userFolio;
	}
	
	
}
