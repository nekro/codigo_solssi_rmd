package mx.com.metlife.solssi.auditoria.persistence;

import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.ControlPK;
import mx.com.metlife.solssi.auditoria.domain.ControlSolicitud;
import mx.com.metlife.solssi.auditoria.domain.ControlSolicitudEsquema;
import mx.com.metlife.solssi.main.dao.GenericDAOImpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class ControlSolicitudDAOImpl extends GenericDAOImpl<ControlSolicitud, ControlPK> implements ControlSolicitudDAO{
	
	@Override
	@SuppressWarnings("unchecked")
	public List<ControlSolicitud> getDetalle(Integer folio){
		Session session = getSession();
		Criteria crit = session.createCriteria(ControlSolicitud.class);
		crit.add(Restrictions.eq("numFolio", folio));
		crit.addOrder(Order.asc("fecha"));
		return crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ControlSolicitudEsquema> getDetalleEsquemas(Integer folio) {
		Session session = getSession();
		Criteria crit = session.createCriteria(ControlSolicitudEsquema.class);
		crit.add(Restrictions.eq("numFolio", folio));
		crit.addOrder(Order.asc("fecha"));
		return crit.list();
	}

}
