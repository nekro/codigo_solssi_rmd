package mx.com.metlife.solssi.auditoria.service;

import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.Bitacora;
import mx.com.metlife.solssi.auditoria.persistence.BitacoraDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BitacoraServiceImpl implements BitacoraService{
	
	@Autowired
	private BitacoraDAO bitacoraDAO;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<Bitacora> consultaBitacora(Date inicio, Date fin, Short movimiento){
		
		return bitacoraDAO.consultaBitacora(inicio, fin, movimiento);
	}

}
