package mx.com.metlife.solssi.auditoria.service;

import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.Bitacora;

public interface BitacoraService {
	public List<Bitacora> consultaBitacora(Date inicio, Date fin, Short movimiento);
}
