package mx.com.metlife.solssi.auditoria.service;

import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.ControlSolicitud;
import mx.com.metlife.solssi.auditoria.domain.ControlSolicitudEsquema;
import mx.com.metlife.solssi.auditoria.persistence.ControlSolicitudDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ControlSolicitudServiceImpl implements ControlSolicitudService{
	
	@Autowired
	private ControlSolicitudDAO controlDAO;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<ControlSolicitud> getDetalle(Integer folio){
		return controlDAO.getDetalle(folio);
	}
	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<ControlSolicitudEsquema> getDetalleEsquemas(Integer folio){
		return controlDAO.getDetalleEsquemas(folio);
	}
	

}
