package mx.com.metlife.solssi.auditoria.persistence;

import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.Bitacora;
import mx.com.metlife.solssi.main.dao.GenericDAO;

public interface BitacoraDAO extends GenericDAO<Bitacora, Date>{
	
	public List<Bitacora> consultaBitacora(Date inicio, Date fin, Short movimiento);
	
}
