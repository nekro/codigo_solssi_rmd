package mx.com.metlife.solssi.auditoria.persistence;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.Bitacora;
import mx.com.metlife.solssi.main.dao.GenericDAOImpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class BitacoraDAOImpl extends GenericDAOImpl<Bitacora, Date> implements BitacoraDAO{
	
	private int MAX_RESULTS = 1000;
	
	@SuppressWarnings("unchecked")
	public List<Bitacora> consultaBitacora(Date ini, Date fin, Short movimiento){
		
		Session session = getSession();
		
		Criteria crit = session.createCriteria(Bitacora.class);
		
		Calendar calIni = Calendar.getInstance();
		calIni.setTime(ini);
		calIni.set(Calendar.HOUR_OF_DAY, 0);
		calIni.set(Calendar.MINUTE, 0);
		calIni.set(Calendar.SECOND, 0);
		calIni.set(Calendar.MILLISECOND, 0);
		
		Calendar calFin = Calendar.getInstance();
		calFin.setTime(fin);
		calFin.set(Calendar.HOUR_OF_DAY, 23);
		calFin.set(Calendar.MINUTE, 59);
		calFin.set(Calendar.SECOND, 59);
		calFin.set(Calendar.MILLISECOND, 999);
		
		crit.add(Restrictions.between("fecha", calIni.getTime(), calFin.getTime()));
		
		if(movimiento != null && movimiento > 0){
			crit.add(Restrictions.eq("movimiento", movimiento));
		}
		
		crit.setMaxResults(MAX_RESULTS);
		
		return crit.list();
		
	}
}
