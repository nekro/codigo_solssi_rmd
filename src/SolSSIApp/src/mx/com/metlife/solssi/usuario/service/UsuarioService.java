package mx.com.metlife.solssi.usuario.service;

import java.util.List;

import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.usuario.domain.UsuarioFilter;

public interface UsuarioService {
	
	public Usuario getUsuario(String clave);
	
	public List<Usuario> getUsuarios(UsuarioFilter filter);
	
	public void save(Usuario user, String userOperacion);
	
	public void update(Usuario user, String userOperacion);
	
}
