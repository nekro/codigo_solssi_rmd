package mx.com.metlife.solssi.usuario.domain;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class UserPrincipal extends User{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4292571234388360288L;
	
	
	private Usuario usuario;

	public UserPrincipal(String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked, Collection<GrantedAuthority> authorities, Usuario usuario) {
		
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		
		this.usuario = usuario;

	}

	/**
	 * @return el usuario
	 */
	public Usuario getUsuario() {
		return usuario;
	}
	
	

}
