package mx.com.metlife.solssi.usuario.persistence;

import mx.com.metlife.solssi.main.dao.GenericDAO;
import mx.com.metlife.solssi.usuario.domain.Rol;

public interface RolDAO extends GenericDAO<Rol, Integer>{

}
