package mx.com.metlife.solssi.usuario.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name="TC_ROLES")
public class Rol implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2104215715478974851L;

	@Id
	@Column(name = "ID_ROL")
	@Min(value=1)
	private Integer id;
	
	@Column(name = "PERMISOS")
	@Pattern(regexp="[10]{5,30}")
	private String permisos;
	
	@Column(name = "DES_ROL")
	@Pattern(regexp="[A-Za-z0-9\\s]{3,30}")
	private String descripcion;
	
	@Column(name = "ESTATUS")
	private boolean activo;
	
	@Transient
	private String[] checkPermisos;
	
	public Rol(){
		this.activo = true;
	}
	
	public void fillChecks(){
		if(StringUtils.isEmpty(permisos)){
			return;
		}
		
		int index = 0;
		StringBuilder sb = new StringBuilder(20);
		for(char p : this.permisos.toCharArray()){
			if(p == '1'){
				sb.append(String.valueOf(index + 1));
				sb.append(",");
			}
			index++;
		}
		
		if(sb.length()>0){
			sb.delete(sb.length()-1,sb.length());
		}
		
		String str = sb.toString();
		this.checkPermisos = str.split(",");
	}

	/**
	 * 
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getPermisos() {
		return permisos;
	}

	/**
	 * 
	 * @param permisos
	 */
	public void setPermisos(String permisos) {
		this.permisos = permisos;
	}

	/**
	 * @return el descripcion
	 */
	public String getDescripcion() {
		return StringUtils.upperCase(StringUtils.trim(descripcion));
	}

	/**
	 * @param descripcion el descripcion a establecer
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return el estatus
	 */
	public boolean isActivo() {
		return activo;
	}

	/**
	 * @param estatus el estatus a establecer
	 */
	public void setActivo(boolean estatus) {
		this.activo = estatus;
	}

	/**
	 * @return el checkPermisos
	 */
	public String[] getCheckPermisos() {
		return checkPermisos;
	}

	/**
	 * @param checkPermisos el checkPermisos a establecer
	 */
	public void setCheckPermisos(String[] checkPermisos) {
		this.checkPermisos = checkPermisos;
	}
	
}
