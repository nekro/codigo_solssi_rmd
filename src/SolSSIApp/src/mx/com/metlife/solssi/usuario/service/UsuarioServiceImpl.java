package mx.com.metlife.solssi.usuario.service;

import java.util.Calendar;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.Bitacora;
import mx.com.metlife.solssi.auditoria.persistence.BitacoraDAO;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.usuario.domain.UsuarioFilter;
import mx.com.metlife.solssi.usuario.persistence.UsuarioDAO;
import mx.com.metlife.solssi.util.MovimientosConstants;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private UsuarioDAO dao;
	
	@Autowired
	private BitacoraDAO bitacora;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly = true)
	public Usuario getUsuario(String clave) {
		return dao.getById(StringUtils.upperCase(StringUtils.trim(clave)));
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly = true)
	public List<Usuario> getUsuarios(UsuarioFilter filter) {
		return dao.getUsuarios(filter);
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly = false)
	public void save(Usuario user, String userOperacion) {
		user.setFechaAlta(Calendar.getInstance().getTime());
		if(user.getDependencia() != null && user.getDependencia() == 0){
			user.setDependencia(null);
		}
		dao.save(user);
		String detalle = user.getClave();
		bitacora.save(this.createBitacora(MovimientosConstants.ALTA_USUARIO, userOperacion, detalle));
	}

	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly = false)
	public void update(Usuario user, String userOperacion) {

		if(user.getDependencia() != null && user.getDependencia() == 0){
			user.setDependencia(null);
		}
		dao.update(user);
		String detalle = user.getClave();
		bitacora.save(this.createBitacora(MovimientosConstants.MODIFICACION_USUARIO, userOperacion, detalle));
	}
	
	private Bitacora createBitacora(short mov, String user, String detalle){
		Bitacora bitacora = new Bitacora();
		bitacora.setFecha(Calendar.getInstance().getTime());
		bitacora.setMovimiento(mov);
		bitacora.setUsuario(user);
		bitacora.setDetalle(detalle);
		return bitacora;
	}
	
}
