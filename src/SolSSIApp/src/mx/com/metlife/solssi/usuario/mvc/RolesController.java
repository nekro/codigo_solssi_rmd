package mx.com.metlife.solssi.usuario.mvc;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import mx.com.metlife.solssi.usuario.domain.Rol;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.usuario.domain.UsuarioFilter;
import mx.com.metlife.solssi.usuario.service.RolService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("roles")
public class RolesController {
	
	@Autowired
	private RolService rolService;
	
	@RequestMapping(method=RequestMethod.POST)
	public String showRolesPage(Model model){
		List<Rol> roles = rolService.getRoles();
		model.addAttribute("roles", roles);
		return "roles/list";
	}
	
	@RequestMapping(value="alta")
	public String alta(Model model){
		model.addAttribute(new Rol());
		model.addAttribute("command","alta");
		return "roles/form";
	}
	
	@RequestMapping(value="save", method=RequestMethod.POST)
	public String save(@Valid Rol rol, BindingResult errors, @RequestParam("command") String command, Model model, Principal principal){
		
		model.addAttribute(rol);
		model.addAttribute("command", command);
		
		if(errors.hasErrors()){
			return "roles/form";
		}
		
		try{
			rolService.save(rol, principal.getName());
		}catch(DataIntegrityViolationException e){
			errors.rejectValue("id", "Duplicate");
			return "roles/form";
		}
		
		model.addAttribute("roles",rolService.getRoles());
		
		return "roles/list";
	}
	
	
	@RequestMapping(value="update", method=RequestMethod.POST)
	public String update(@Valid Rol rol, BindingResult errors, @RequestParam("command") String command, Model model, Principal principal){
		
		model.addAttribute(rol);
		model.addAttribute("command", command);
		
		if(errors.hasErrors()){
			return "roles/form";
		}
		
		rolService.update(rol, principal.getName());
		
		model.addAttribute("roles",rolService.getRoles());
		
		return "roles/list";
	}
	
	@RequestMapping(value="edit", method=RequestMethod.POST)
	public String editar(@RequestParam("clave") Integer clave,Model model){
		
		Rol rol = rolService.getRol(clave);
		model.addAttribute(rol);
		model.addAttribute("command","update");
		return "roles/form";
	}

}
