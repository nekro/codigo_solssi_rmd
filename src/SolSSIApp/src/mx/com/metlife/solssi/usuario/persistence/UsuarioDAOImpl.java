package mx.com.metlife.solssi.usuario.persistence;

import java.util.List;

import mx.com.metlife.solssi.main.dao.GenericDAOImpl;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.usuario.domain.UsuarioFilter;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class UsuarioDAOImpl extends GenericDAOImpl<Usuario, String> implements UsuarioDAO{

	public List<Usuario> getUsuarios(UsuarioFilter filtro){
		Session session = getSession();
		Criteria crit = session.createCriteria(Usuario.class);
		
		if(StringUtils.isNotEmpty(filtro.getClave())){
			crit.add(Restrictions.eq("clave", filtro.getClave()));
		}
		
		if(filtro.getRol() != null && filtro.getRol() > 0){
			crit.add(Restrictions.eq("rol.id", filtro.getRol()));
		}
		
		if(filtro.getDependencia() != null && filtro.getDependencia() > 0){
			crit.add(Restrictions.eq("dependencia", filtro.getDependencia()));
		}
		
		return crit.list();
	}
	
}
