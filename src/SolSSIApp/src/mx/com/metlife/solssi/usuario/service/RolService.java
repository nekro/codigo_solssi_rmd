package mx.com.metlife.solssi.usuario.service;

import java.util.List;

import mx.com.metlife.solssi.usuario.domain.Rol;

public interface RolService {
	
		public List<Rol> getRoles();
		
		public Rol getRol(Integer id);
		
		public void save(Rol rol, String user);
		
		public void update(Rol rol, String user);

}
