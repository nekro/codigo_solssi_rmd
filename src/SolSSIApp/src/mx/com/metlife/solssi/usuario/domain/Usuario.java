package mx.com.metlife.solssi.usuario.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name= "TC_USUARIOS")
public class Usuario implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5291586593658184153L;

	@Id
	@Column(name="CVE_USUARIO")
	@Pattern(regexp="[A-Za-z0-9]{5,10}")
	private String clave;
	
	private boolean activo;
	
	@Column(name="BND_ADMIN")
	private boolean admin;
	
	@Column(name="NOMBRE_USUARIO")
	@Pattern(regexp="[A-Za-z������������\\s]{10,64}")
	private String nombre;
	
	@Column(name = "CVE_DEPENDENCIA")
	private Short dependencia;
	
	@Column(name = "FECHA_ALTA", updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaAlta;
	
	@Column(name = "FECHA_ULTIMO_ACCESO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaUltimoAcceso;
	
	@Column(name = "IP_ULTIMO_ACCESO")
	private String IPUltimoAcceso;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ID_ROL", referencedColumnName="ID_ROL")
	private Rol rol;
	
	public Usuario(){
		this.activo = true;
		this.admin = false;
		this.rol = new Rol();
	}
	
	
	/**
	 * @return the clave
	 */
	public String getClave() {
		return StringUtils.upperCase(StringUtils.trim(clave));
	}



	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}



	/**
	 * @return the activo
	 */
	public boolean isActivo() {
		return activo;
	}



	/**
	 * @param activo the activo to set
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}



	/**
	 * @param rol the rol to set
	 */
	public void setRol(Rol rol) {
		this.rol = rol;
	}
	
	public Rol getRol() {
		return rol;
	}



	public int getIdRol(){
		return rol.getId();
	}



	/**
	 * 
	 * @return
	 */
	public String getPermisos() {
		return rol.getPermisos();
	}



	/**
	 * @return el bndAdmin
	 */
	public boolean isAdmin() {
		return admin;
	}



	/**
	 * @param bndAdmin el bndAdmin a establecer
	 */
	public void setAdmin(boolean bndAdmin) {
		this.admin = bndAdmin;
	}



	/**
	 * @return el nombre
	 */
	public String getNombre() {
		return StringUtils.upperCase(StringUtils.trim(nombre));
	}



	/**
	 * @param nombre el nombre a establecer
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	/**
	 * @return el dependencia
	 */
	public Short getDependencia() {
		return dependencia;
	}



	/**
	 * @param dependencia el dependencia a establecer
	 */
	public void setDependencia(Short dependencia) {
		this.dependencia = dependencia;
	}


	/**
	 * @return el fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}


	/**
	 * @param fechaAlta el fechaAlta a establecer
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}


	/**
	 * @return el fechaUltimoAcceso
	 */
	public Date getFechaUltimoAcceso() {
		return fechaUltimoAcceso;
	}


	/**
	 * @param fechaUltimoAcceso el fechaUltimoAcceso a establecer
	 */
	public void setFechaUltimoAcceso(Date fechaUltimoAcceso) {
		this.fechaUltimoAcceso = fechaUltimoAcceso;
	}


	/**
	 * @return el iPUltimoAcceso
	 */
	public String getIPUltimoAcceso() {
		return IPUltimoAcceso;
	}


	/**
	 * @param ultimoAcceso el iPUltimoAcceso a establecer
	 */
	public void setIPUltimoAcceso(String ultimoAcceso) {
		IPUltimoAcceso = ultimoAcceso;
	}


	
	

}
