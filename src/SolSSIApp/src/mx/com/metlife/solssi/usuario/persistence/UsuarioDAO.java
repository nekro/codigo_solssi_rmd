package mx.com.metlife.solssi.usuario.persistence;

import java.util.List;

import mx.com.metlife.solssi.main.dao.GenericDAO;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.usuario.domain.UsuarioFilter;

public interface UsuarioDAO extends GenericDAO<Usuario, String>{
	public List<Usuario> getUsuarios(UsuarioFilter filtro);
}
