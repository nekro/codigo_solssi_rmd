package mx.com.metlife.solssi.usuario.domain;

import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;


public class UsuarioFilter {
	
	@Pattern(regexp="([A-Za-z0-9]{5,10})?")
	private String clave;
	
	private Short dependencia;
	
	private Integer rol;

	/**
	 * @return el clave
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * @param clave el clave a establecer
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}

	/**
	 * @return el dependencia
	 */
	public Short getDependencia() {
		return dependencia;
	}

	/**
	 * @param dependencia el dependencia a establecer
	 */
	public void setDependencia(Short dependencia) {
		this.dependencia = dependencia;
	}

	/**
	 * @return el rol
	 */
	public Integer getRol() {
		return rol;
	}

	/**
	 * @param rol el rol a establecer
	 */
	public void setRol(Integer rol) {
		this.rol = rol;
	}
	
	public boolean isEmpty(){
		return (StringUtils.isEmpty(this.clave)) && (this.rol == null || this.rol == 0) && (this.dependencia == null || this.dependencia == 0);
	}
	

}
