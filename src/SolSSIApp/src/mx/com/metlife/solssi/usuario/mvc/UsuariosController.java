package mx.com.metlife.solssi.usuario.mvc;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.usuario.domain.UsuarioFilter;
import mx.com.metlife.solssi.usuario.service.RolService;
import mx.com.metlife.solssi.usuario.service.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("usuarios")
public class UsuariosController {
	
	@Autowired
	private UsuarioService userService;
	
	@Autowired
	private RolService rolService;
	
	@ModelAttribute
	public void roles(Model model){
		model.addAttribute("roles",rolService.getRoles());
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public String showUsers(Model model){
		model.addAttribute(new UsuarioFilter());
		return "usuarios/list";
	}
	
	@RequestMapping(value="consulta", method=RequestMethod.POST)
	public String consulta(@Valid UsuarioFilter filter,BindingResult errors , Model model){
		
		if(filter.isEmpty()){
			errors.reject("NotEmpty.usuarioFilter.object");
		}
		
		if(errors.hasErrors()){
			return "usuarios/list";
		}
		
		List<Usuario> usuarios = userService.getUsuarios(filter);
		
		if(usuarios == null || usuarios.isEmpty()){
			errors.reject("NotEmpty.usuarioFilter.result");
		}
		
		model.addAttribute("usuarios", usuarios);
		return "usuarios/list";
	}
	
	
	@RequestMapping(value="alta")
	public String alta(Model model){
		model.addAttribute(new Usuario());
		model.addAttribute("command","alta");
		return "usuarios/form";
	}
	
	@RequestMapping(value="save", method=RequestMethod.POST)
	public String save(@Valid Usuario usuario, BindingResult errors, @RequestParam("command") String command, Model model, Principal principal){
		
		model.addAttribute(usuario);
		model.addAttribute("command", command);
		
		if(errors.hasErrors()){
			return "usuarios/form";
		}
		
		try{
			userService.save(usuario, principal.getName());	
		}catch(DataIntegrityViolationException e){
			errors.rejectValue("clave","Duplicate");
			return "usuarios/form";
		}
		
		
		UsuarioFilter filtro = new UsuarioFilter();
		filtro.setClave(usuario.getClave());
		List<Usuario> list = userService.getUsuarios(filtro);
		model.addAttribute(filtro);
		model.addAttribute("usuarios",list);
		
		return "usuarios/list";
	}
	
	
	@RequestMapping(value="update", method=RequestMethod.POST)
	public String update(@Valid Usuario usuario, BindingResult errors, @RequestParam("command") String command, Model model, Principal principal){
		
		model.addAttribute(usuario);
		model.addAttribute("command", command);
		
		if(errors.hasErrors()){
			return "usuarios/form";
		}
		
		userService.update(usuario, principal.getName());
		
		UsuarioFilter filtro = new UsuarioFilter();
		filtro.setClave(usuario.getClave());
		List<Usuario> list = userService.getUsuarios(filtro);
		model.addAttribute(filtro);
		model.addAttribute("usuarios",list);
		
		return "usuarios/list";
	}
	
	@RequestMapping(value="edit", method=RequestMethod.POST)
	public String editar(@Valid UsuarioFilter filter, BindingResult errors, Model model){
		
		if(errors.hasErrors()){
			return "usuarios/list";
		}
		
		Usuario usuario = userService.getUsuario(filter.getClave());
		model.addAttribute(usuario);
		model.addAttribute("command","update");
		return "usuarios/form";
	}

}
