package mx.com.metlife.solssi.usuario.persistence;

import org.springframework.stereotype.Repository;

import mx.com.metlife.solssi.main.dao.GenericDAOImpl;
import mx.com.metlife.solssi.usuario.domain.Rol;

@Repository
public class RolDAOImpl extends GenericDAOImpl<Rol, Integer> implements RolDAO{

}
