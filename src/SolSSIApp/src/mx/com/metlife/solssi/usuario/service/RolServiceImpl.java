package mx.com.metlife.solssi.usuario.service;

import java.util.Calendar;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.Bitacora;
import mx.com.metlife.solssi.auditoria.persistence.BitacoraDAO;
import mx.com.metlife.solssi.usuario.domain.Rol;
import mx.com.metlife.solssi.usuario.persistence.RolDAO;
import mx.com.metlife.solssi.util.MovimientosConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RolServiceImpl implements RolService{

	@Autowired
	private RolDAO dao;
	
	@Autowired
	private BitacoraDAO bitacora;
	
	@PreAuthorize("hasRole('ADMIN_USUARIOS')")
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public Rol getRol(Integer id) {
		Rol rol = dao.getById(id);
		rol.fillChecks();
		return rol;
	}

	
	@PreAuthorize("hasRole('ADMIN_USUARIOS')")
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<Rol> getRoles() {
		return dao.getAll();
	}

	@PreAuthorize("hasRole('ADMIN_USUARIOS')")
	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly=false)
	public void save(Rol rol, String user) {
		dao.save(rol);
		String detalle = rol.getId()+"|"+rol.getPermisos();
		bitacora.save(this.createBitacora(MovimientosConstants.ALTA_ROL, user, detalle));
	}

	@PreAuthorize("hasRole('ADMIN_USUARIOS')")
	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly=false)
	public void update(Rol rol, String user) {
		dao.update(rol);
		String detalle = rol.getId()+"|"+rol.getPermisos();
		bitacora.save(this.createBitacora(MovimientosConstants.MODIFICACION_ROL, user, detalle));
	}
	
	private Bitacora createBitacora(short mov, String user, String detalle){
		Bitacora bitacora = new Bitacora();
		bitacora.setFecha(Calendar.getInstance().getTime());
		bitacora.setMovimiento(mov);
		bitacora.setUsuario(user);
		bitacora.setDetalle(detalle);
		return bitacora;
	}

}
