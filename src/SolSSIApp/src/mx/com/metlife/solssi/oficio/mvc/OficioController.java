package mx.com.metlife.solssi.oficio.mvc;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import mx.com.metlife.solssi.cifrascontrol.service.CifrasControlService;
import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.inicio.service.GruposCapturaService;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.oficio.domain.Oficio;
import mx.com.metlife.solssi.oficio.domain.OficioFilter;
import mx.com.metlife.solssi.oficio.service.OficioService;
import mx.com.metlife.solssi.param.service.ParametersService;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.solicitud.service.SolicitudService;
import mx.com.metlife.solssi.usuario.domain.UserPrincipal;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.BrowserUtils;
import mx.com.metlife.solssi.util.RangoCaptura;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("oficio")
@SessionAttributes({"control", "oficio"})
public class OficioController {
	
	@Autowired
	private SolicitudService solicitudService;
	
	@Autowired
	private ParametersService params;
	
	@Autowired
	private OficioService oficioService;
	
	@Autowired
	private GruposCapturaService gruposService;
	
	@Autowired
	private CifrasControlService ccService;
	
	@RequestMapping(value="generar" , method=RequestMethod.POST)
	public String showPage(HttpSession session, Model model, Authentication auth){
		
		model.addAttribute("command", "generar");
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		try {
			RangoCaptura rango = this.gruposService.checkInRangeGenerarOficios();
			Oficio oficio = oficioService.existOficioDependencia(usuario.getDependencia(), rango.getFechaInicialOficios(), rango.getFechaFinalOficios()); 
			if(oficio != null){
				session.setAttribute("oficio", oficio);
				BrowserUtils.enableHistoryBack(model);
				return "oficio/existente";
			}
		} catch (ApplicationException e) {
			model.addAttribute("error",e.getMessage());
			return "oficio/result"; 
		}
		
		ConsultaFilter filter = new ConsultaFilter();
		filter.setEstatusList(AppConstants.ESTATUS_ACEPTADO, AppConstants.ESTATUS_RECHAZADO, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD);
		filter.setDependencia(usuario.getDependencia());
		filter.setOficio(-1);
		
		
		List<Solicitud> list = solicitudService.get(filter);
		model.addAttribute("solicitudesOficio", list);
		
		ControlCode control = ccService.createCifraControl(list);
		model.addAttribute("control", control);
		
		BrowserUtils.enableHistoryBack(model);

		return "oficio/genera";
	}
	
	
	@RequestMapping(value="generar/execute" , method=RequestMethod.POST)
	public String generarOficio(HttpSession session, Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		
		
		ConsultaFilter filter = new ConsultaFilter();
		filter.setEstatus(AppConstants.ESTATUS_CAPTURA);
		filter.setDependencia(usuario.getDependencia());
		filter.setOficio(-1);
		
		List<Solicitud> list = solicitudService.get(filter);
		
		if(list != null & list.size() > 0){
			model.addAttribute("solicitudesPendientes", list);
			return "oficio/no_revisadas";
		}
		
		filter = new ConsultaFilter();
		filter.setEstatusList(AppConstants.ESTATUS_ACEPTADO, AppConstants.ESTATUS_RECHAZADO, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD);
		filter.setDependencia(usuario.getDependencia());
		filter.setOficio(-1);
		
		List<Solicitud> solicitudes = solicitudService.get(filter);
		
		if(solicitudes == null || solicitudes.isEmpty()){
			model.addAttribute("error", "No existen solicitudes para generar el oficio");
			model.addAttribute("command", "generar");
			return "oficio/result";
		}
		
		Oficio oficio;
		try {
			ControlCode control = ccService.createCifraControl(solicitudes);
			oficio = oficioService.generaOficio(solicitudes, control, usuario);
			model.addAttribute("control",control);
		} catch (ApplicationException e) {
			model.addAttribute("error", e.getMessage());
			return "oficio/result";	
		}
		
		model.addAttribute(oficio);
		
		model.addAttribute("command", "generar");
		
		return "oficio/result";
	}
	
	
	@RequestMapping(value="consulta" , method=RequestMethod.POST)
	public String showConsulta(Model model, HttpSession session){
		OficioFilter filter = new OficioFilter();
		model.addAttribute(filter);
		
		session.removeAttribute("oficios");
		
		return "oficio/aceptar";
	}
	
	
	@RequestMapping(value="consulta/execute" , method=RequestMethod.POST)
	public String showConsulta(@Valid OficioFilter filter, BindingResult errors, Model model, Authentication auth){
		
		model.addAttribute(filter);
		
		List<Oficio> oficios = oficioService.getOficios(filter);
		
		if(oficios == null || oficios.isEmpty()){
			errors.reject("NotEmpty.oficioFilter.result");
		}
		
		model.addAttribute("oficios", oficios);
		
		BrowserUtils.enableHistoryBack(model);
		
		return "oficio/aceptar";
	}
	
	
	@RequestMapping(value="detalle" , method=RequestMethod.POST)
	public String showOficio(@RequestParam("oficio") Integer numOficio, Model model){
		ConsultaFilter filtro = new ConsultaFilter();
		filtro.setOficio(numOficio);
		
		List<Solicitud> list = solicitudService.get(filtro);
		
		Oficio oficio = oficioService.getOficio(numOficio);
		
		ControlCode control = ccService.createCifraControl(list);

		model.addAttribute("solicitudes", list);
		model.addAttribute("oficio", oficio);
		model.addAttribute("control", control);
		BrowserUtils.enableHistoryBack(model);
		return "oficio/detalle";
	}
	
	@RequestMapping(value="save/caducadas" , method=RequestMethod.POST)
	public String guardarCaducadas(@RequestParam("solicitudes") Integer folios[], Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		solicitudService.saveAsCaducadas(folios, usuario.getClave());
		
		model.addAttribute("command", "caducidad");
		
		return "oficio/result";
	}
	
	
	@RequestMapping(value="aceptar/execute" , method=RequestMethod.POST)
	public String aceptarOficios(@RequestParam("oficios") Integer oficios[], Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		try {
			oficioService.aceptarOficios(oficios, usuario);
		} catch (ApplicationException e) {
			model.addAttribute("error", e.getMessage());
		}
		
		return "oficio/result";
	}
	
	
	@RequestMapping(value="enviar" , method=RequestMethod.POST)
	public String enviarOficiosList(Model model, HttpSession session){
		OficioFilter filter = new OficioFilter();
		filter.setEstatus(AppConstants.OFICIO_ESTATUS_ACEPTADO);
		
		try {
			filter.setFecha(gruposService.lastFechaInicioOficios());
		} catch (ApplicationException e) {
			model.addAttribute("error",e.getMessage());
			return "oficio/result";
		}
		
		List<Oficio> oficios = oficioService.getOficios(filter);
		
		session.setAttribute("oficios", oficios);
		
		BrowserUtils.enableHistoryBack(model);
		
		return "oficio/enviar";
	}
	
	@RequestMapping(value="enviar/execute" , method=RequestMethod.POST)
	public String enviarOficios(Model model, 
								@RequestParam(value = "automaticos", required = false) Integer automaticos[],
								@RequestParam(value = "prestamos", required = false) Integer prestamos[],										
								@RequestParam(value = "cheques", required = false) Integer cheques[],
								@RequestParam(value = "pagomaticos", required = false) Integer pagomaticos[],
								@RequestParam(value = "juridicos", required = false) Integer juridicos[],			
																				HttpSession session, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		List<Oficio> oficios = (List<Oficio>) session.getAttribute("oficios");
		
		for(Oficio oficio: oficios){
			
			if(oficio.getBndAutomatico() != AppConstants.OFICIO_BND_PROCESADO){
				oficio.setBndAutomatico(this.calculateValueBnd(oficio.getNumOficio(), automaticos));
			}
			
			if(oficio.getBndPrestamo() != AppConstants.OFICIO_BND_PROCESADO){
				oficio.setBndPrestamo(this.calculateValueBnd(oficio.getNumOficio(), prestamos));
			}
			
			if(oficio.getBndCheque() != AppConstants.OFICIO_BND_PROCESADO){
				oficio.setBndCheque(this.calculateValueBnd(oficio.getNumOficio(), cheques));
			}
			
			if(oficio.getBndJuridico() != AppConstants.OFICIO_BND_PROCESADO){
				oficio.setBndJuridico(this.calculateValueBnd(oficio.getNumOficio(), juridicos));
			}
			
			if(oficio.getBndPagomatico() != AppConstants.OFICIO_BND_PROCESADO){
				oficio.setBndPagomatico(this.calculateValueBnd(oficio.getNumOficio(), pagomaticos));
			}
			
		}
		
		oficioService.marcarOficios(oficios, usuario.getClave());
		
		model.addAttribute("command", "enviar");
		
		return "oficio/result";
	}
	
	private int calculateValueBnd(Integer oficio, Integer array[]){
		
		if(array == null){
			return AppConstants.OFICIO_BND_SIN_ENVIAR;
		}

		for(Integer of : array){
			if(oficio.intValue() == of.intValue()){
				return AppConstants.OFICIO_BND_ENVIADO;
			}
		}
		return AppConstants.OFICIO_BND_SIN_ENVIAR;
	}
	
	
	
	@RequestMapping(value = "view/solicitud", method=RequestMethod.POST)
	public String showSolicitud(@RequestParam("numFolio") Integer folio, Model model){
		
		Solicitud solicitud = solicitudService.get(folio);
		model.addAttribute("solicitud", solicitud);
		model.addAttribute("urlRegresar", "/oficio/generar");
		
		return "consulta/solicitud";
	}
	
	@RequestMapping(value = "oficio/frames", method=RequestMethod.POST)
	public String oficio(Model model){
		return "oficio/frames";
	}
	
	@RequestMapping(value = "oficio/menu")
	public String oficioMenu(Model model){
		return "oficio/menu";
	}
	
	@RequestMapping(value="reimpresion" , method=RequestMethod.POST)
	public String showConsulta(Model model, HttpSession session, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		Oficio oficio = (Oficio) session.getAttribute("oficio");
		
		ConsultaFilter filter = new ConsultaFilter();
		filter.setEstatusList(AppConstants.ESTATUS_ACEPTADO, AppConstants.ESTATUS_RECHAZADO, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD);
		filter.setDependencia(usuario.getDependencia());
		filter.setOficio(oficio.getNumOficio());
		List<Solicitud> list = solicitudService.get(filter);
		
		ControlCode control = ccService.createCifraControl(list);
		
		session.setAttribute("control", control);
		
		return "oficio/frames";
	}
	
}
