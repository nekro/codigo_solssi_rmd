package mx.com.metlife.solssi.oficio.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table (name = "TW_OFICIOS")

@NamedQueries(
	{
		@NamedQuery(name="updateEstatusOficioEnviado", 
				query = "UPDATE Oficio SET estatus = :estatus_nuevo, fechaEnvio = :fecha, usuarioEnvia = :user WHERE numOficio = :oficio and estatus = :estatus_ant"),
		@NamedQuery(name="updateEstatusOficioAceptado", 
				query = "UPDATE Oficio SET estatus = :estatus_nuevo, fechaAceptacion = :fecha, usuarioAcepta = :user WHERE numOficio = :oficio and estatus = :estatus_ant")
	})		
public class Oficio implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7227012353484267270L;

	@Id
	@Column(name = "NUM_OFICIO")
	@GeneratedValue
	protected Integer numOficio;
	
	@Column(name = "CVE_DEPENDENCIA")
	protected Short dependencia;
	
	@Column(name = "CVE_ESTATUS")
	protected Short estatus;
	
	@Column(name = "FECHA_GENERACION")
	protected Date fechaGeneracion;
	
	@Column(name = "CVE_USER_GENERA")
	protected String usuarioGenera;
	
	@Column(name = "CVE_USER_ACEPTA")
	protected String usuarioAcepta;
	
	@Column(name = "CVE_USER_ENVIA")
	protected String usuarioEnvia;
	
	@Column(name = "FECHA_ACEPTA")
	protected Date fechaAceptacion;
	
	@Column(name = "FECHA_ENVIO")
	protected Date fechaEnvio;
	
	@Column(name = "FECHA_PROCESO")
	protected Date fechaProceso;
	
	@Column(name = "ID_PROCESO")
	protected Integer idProceso;
	
	@Column(name = "TOTAL_SOLICITUDES")
	protected Integer totalSolicitudes;
	
	@Column(name = "CODIGO_CONTROL")
	protected String controlCode;
	
	@Column(name = "BND_AUTOMATICO")
	protected Integer bndAutomatico = 0;
	
	@Column(name = "BND_CHEQUE")
	protected Integer bndCheque = 0;
	
	@Column(name = "BND_JURIDICO")
	protected Integer bndJuridico = 0;
	
	@Column(name = "BND_PRESTAMO")
	protected Integer bndPrestamo = 0;
	
	@Column(name = "BND_PAGOMATICO")
	protected Integer bndPagomatico = 0;

	/**
	 * @return el numOficio
	 */
	public Integer getNumOficio() {
		return numOficio;
	}

	/**
	 * @param numOficio el numOficio a establecer
	 */
	public void setNumOficio(Integer numOficio) {
		this.numOficio = numOficio;
	}

	/**
	 * @return el estatus
	 */
	public Short getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus el estatus a establecer
	 */
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return el fechaGeneracion
	 */
	public Date getFechaGeneracion() {
		return fechaGeneracion;
	}

	/**
	 * @param fechaGeneracion el fechaGeneracion a establecer
	 */
	public void setFechaGeneracion(Date fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	/**
	 * @return el usuarioGenera
	 */
	public String getUsuarioGenera() {
		return usuarioGenera;
	}

	/**
	 * @param usuarioGenera el usuarioGenera a establecer
	 */
	public void setUsuarioGenera(String usuarioGenera) {
		this.usuarioGenera = usuarioGenera;
	}

	/**
	 * @return el usuarioAcepta
	 */
	public String getUsuarioAcepta() {
		return usuarioAcepta;
	}

	/**
	 * @param usuarioAcepta el usuarioAcepta a establecer
	 */
	public void setUsuarioAcepta(String usuarioAcepta) {
		this.usuarioAcepta = usuarioAcepta;
	}

	/**
	 * @return el fechaAceptacion
	 */
	public Date getFechaAceptacion() {
		return fechaAceptacion;
	}

	/**
	 * @param fechaAceptacion el fechaAceptacion a establecer
	 */
	public void setFechaAceptacion(Date fechaAceptacion) {
		this.fechaAceptacion = fechaAceptacion;
	}

	/**
	 * @return el dependencia
	 */
	public Short getDependencia() {
		return dependencia;
	}

	/**
	 * @param dependencia el dependencia a establecer
	 */
	public void setDependencia(Short dependencia) {
		this.dependencia = dependencia;
	}

	/**
	 * @return el fechaProceso
	 */
	public Date getFechaProceso() {
		return fechaProceso;
	}

	/**
	 * @param fechaProceso el fechaProceso a establecer
	 */
	public void setFechaProceso(Date fechaProceso) {
		this.fechaProceso = fechaProceso;
	}

	/**
	 * @return el controlCode
	 */
	public String getControlCode() {
		return controlCode;
	}

	/**
	 * @param controlCode el controlCode a establecer
	 */
	public void setControlCode(String controlCode) {
		this.controlCode = controlCode;
	}

	/**
	 * @return el usuarioEnvia
	 */
	public String getUsuarioEnvia() {
		return usuarioEnvia;
	}

	/**
	 * @param usuarioEnvia el usuarioEnvia a establecer
	 */
	public void setUsuarioEnvia(String usuarioEnvia) {
		this.usuarioEnvia = usuarioEnvia;
	}

	/**
	 * @return el fechaEnvio
	 */
	public Date getFechaEnvio() {
		return fechaEnvio;
	}

	/**
	 * @param fechaEnvio el fechaEnvio a establecer
	 */
	public void setFechaEnvio(Date fechaEnvio) {
		this.fechaEnvio = fechaEnvio;
	}

	/**
	 * @return el idProceso
	 */
	public Integer getIdProceso() {
		return idProceso;
	}

	/**
	 * @param idProceso el idProceso a establecer
	 */
	public void setIdProceso(Integer idProceso) {
		this.idProceso = idProceso;
	}

	/**
	 * @return el totalSolicitudes
	 */
	public Integer getTotalSolicitudes() {
		return totalSolicitudes;
	}

	/**
	 * @param totalSolicitudes el totalSolicitudes a establecer
	 */
	public void setTotalSolicitudes(Integer totalSolicitudes) {
		this.totalSolicitudes = totalSolicitudes;
	}

	/**
	 * @return the bndAutomatico
	 */
	public Integer getBndAutomatico() {
		return bndAutomatico;
	}

	/**
	 * @param bndAutomatico the bndAutomatico to set
	 */
	public void setBndAutomatico(Integer bndAutomatico) {
		this.bndAutomatico = bndAutomatico;
	}

	/**
	 * @return the bndCheque
	 */
	public Integer getBndCheque() {
		return bndCheque;
	}

	/**
	 * @param bndCheque the bndCheque to set
	 */
	public void setBndCheque(Integer bndCheque) {
		this.bndCheque = bndCheque;
	}

	/**
	 * @return the bndJuridico
	 */
	public Integer getBndJuridico() {
		return bndJuridico;
	}

	/**
	 * @param bndJuridico the bndJuridico to set
	 */
	public void setBndJuridico(Integer bndJuridico) {
		this.bndJuridico = bndJuridico;
	}

	/**
	 * @return the bndPrestamo
	 */
	public Integer getBndPrestamo() {
		return bndPrestamo;
	}

	/**
	 * @param bndPrestamo the bndPrestamo to set
	 */
	public void setBndPrestamo(Integer bndPrestamo) {
		this.bndPrestamo = bndPrestamo;
	}

	/**
	 * @return the bndPagomatico
	 */
	public Integer getBndPagomatico() {
		return bndPagomatico;
	}

	/**
	 * @param bndPagomatico the bndPagomatico to set
	 */
	public void setBndPagomatico(Integer bndPagomatico) {
		this.bndPagomatico = bndPagomatico;
	}
	
	
	

}
