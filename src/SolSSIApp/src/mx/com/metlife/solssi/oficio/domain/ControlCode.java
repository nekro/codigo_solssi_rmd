package mx.com.metlife.solssi.oficio.domain;

public class ControlCode {
	
	private int totalSolicitudes;
	
	private int totalPorcentajes;
	
	private int totalRechazadas;
	
	private int totalAceptadas;
	
	private long sumaClabes;
	
	private long sumaFolios;
	
	/**
	 * @return el totalSolicitudes
	 */
	public int getTotalSolicitudes() {
		return totalSolicitudes;
	}

	/**
	 * @param totalSolicitudes el totalSolicitudes a establecer
	 */
	public void setTotalSolicitudes(int totalSolicitudes) {
		this.totalSolicitudes = totalSolicitudes;
	}

	/**
	 * @return el totalPorcentajes
	 */
	public int getTotalPorcentajes() {
		return totalPorcentajes;
	}

	/**
	 * @param totalPorcentajes el totalPorcentajes a establecer
	 */
	public void setTotalPorcentajes(int totalPorcentajes) {
		this.totalPorcentajes = totalPorcentajes;
	}

	/**
	 * @return el sumaFolios
	 */
	public long getSumaFolios() {
		return sumaFolios;
	}

	/**
	 * @param sumaFolios el sumaFolios a establecer
	 */
	public void setSumaFolios(long sumaFolios) {
		this.sumaFolios = sumaFolios;
	}

	
	
	/**
	 * @return el totalRechazadas
	 */
	public int getTotalRechazadas() {
		return totalRechazadas;
	}

	/**
	 * @param totalRechazadas el totalRechazadas a establecer
	 */
	public void setTotalRechazadas(int totalRechazadas) {
		this.totalRechazadas = totalRechazadas;
	}
	
	

	public int getTotalAceptadas() {
		return totalAceptadas;
	}

	public void setTotalAceptadas(int totalAceptadas) {
		this.totalAceptadas = totalAceptadas;
	}
	
	

	/**
	 * @return el sumaClabes
	 */
	public long getSumaClabes() {
		return sumaClabes;
	}

	/**
	 * @param sumaClabes el sumaClabes a establecer
	 */
	public void setSumaClabes(long sumaClabes) {
		this.sumaClabes = sumaClabes;
	}

	/**
	 * 
	 * @return
	 */
	public String getControlCode() {
		StringBuilder str = new StringBuilder();
		str.append(this.totalSolicitudes);
		str.append("-");
		str.append(this.totalAceptadas);
		str.append("-");
		str.append(this.totalPorcentajes);
		str.append("-");
		str.append(this.sumaFolios);
		str.append("-");
		str.append(this.sumaClabes);
		return  str.toString(); 
	}
	
	public String getControlCodeEsquemas() {
		
		StringBuilder str = new StringBuilder();
		str.append(this.totalSolicitudes);
		str.append("-");
		str.append(this.totalAceptadas);
		str.append("-");
		str.append(this.sumaFolios);
		str.append("-");
		str.append(this.sumaClabes);
		return  str.toString(); 
	}
	
	
	/**
	 * 
	 * @return
	 */
	public String getControlCodeSave() {

		StringBuilder str = new StringBuilder();
		str.append(this.totalAceptadas);
		str.append("-");
		str.append(this.totalPorcentajes);
		str.append("-");
		str.append(this.sumaFolios);
		str.append("-");
		str.append(this.sumaClabes);
		
		if(str.length() > 50){
			return  str.substring(0, 50);	
		}
		
		return str.toString(); 
	}
	
	

}
