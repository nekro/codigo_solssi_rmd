package mx.com.metlife.solssi.oficio.service;

import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.oficio.domain.Oficio;
import mx.com.metlife.solssi.oficio.domain.OficioFilter;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.usuario.domain.Usuario;

public interface OficioService {
	
	public Oficio generaOficio(List<Solicitud> solicitudes, ControlCode control, Usuario userGenera) throws ApplicationException;
	
	public List<Oficio> getOficios(OficioFilter filter);
	
	public Oficio getOficio(Integer num);
	
	public void aceptarOficios(Integer oficios[], Usuario usuario) throws ApplicationException;
	
	public void enviarOficios(Integer oficios[], Usuario usuario) throws ApplicationException;
	
	public Oficio existOficioDependencia(Short dependencia, Date ini, Date fin);
	
	public void marcarOficios(List<Oficio> oficios, String user);

}


