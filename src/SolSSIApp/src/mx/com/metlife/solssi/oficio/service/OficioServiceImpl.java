package mx.com.metlife.solssi.oficio.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.Bitacora;
import mx.com.metlife.solssi.auditoria.persistence.BitacoraDAO;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.oficio.domain.Oficio;
import mx.com.metlife.solssi.oficio.domain.OficioFilter;
import mx.com.metlife.solssi.oficio.persistence.OficioDAO;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.MovimientosConstants;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OficioServiceImpl implements OficioService{
	
	@Autowired
	private OficioDAO oficioDAO;
	
	@Autowired
	private BitacoraDAO bitacoraDAO;
	
	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly= false)
	public Oficio generaOficio(List<Solicitud> solicitudes, ControlCode control, Usuario usuario) throws ApplicationException{
		
		Oficio oficio = new Oficio();
		oficio.setEstatus(AppConstants.OFICIO_ESTATUS_GENERADO);
		oficio.setFechaGeneracion(Calendar.getInstance().getTime());
		oficio.setDependencia(usuario.getDependencia());
		oficio.setUsuarioGenera(usuario.getClave());
		oficio.setControlCode(control.getControlCodeSave());
		oficio.setTotalSolicitudes(control.getTotalSolicitudes());
		
		oficioDAO.save(oficio);
		
		int updates = 0;
		for(Solicitud solicitud : solicitudes){
			updates += oficioDAO.updateNumOficioSolicitud(solicitud.getNumFolio(), oficio.getNumOficio());
		}
		
		if(updates == 0){
			throw new ApplicationException();
		}
		
		Bitacora bitacora = new Bitacora();
		bitacora.setFecha(Calendar.getInstance().getTime());
		bitacora.setMovimiento(MovimientosConstants.GENERA_OFICIO);
		bitacora.setDetalle(String.valueOf(oficio.getNumOficio()));
		bitacora.setUsuario(usuario.getClave());
		bitacoraDAO.save(bitacora);
		
		return oficio;
		
	}
	
	@PreAuthorize("hasRole('AUTORIZA_OFICIOS')")
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly= true)
	public List<Oficio> getOficios(OficioFilter filter) {
		return oficioDAO.getOficios(filter);
	}
	
	@PreAuthorize("hasRole('AUTORIZA_OFICIOS')")
	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly= false, rollbackFor=ApplicationException.class)
	public void aceptarOficios(Integer oficios[], Usuario usuario) throws ApplicationException {
		
		StringBuilder detalle = new StringBuilder();
		
		for(Integer oficio : oficios){
			
			int rowsAfected = oficioDAO.aceptarOficio(oficio, usuario.getClave());
			
			if(rowsAfected == 0){
				throw new ApplicationException(MessageUtils.getMessage("NotValid.oficio.estatus.generado",new Object[]{oficio}));
			}
			
			detalle.append(oficio);
			detalle.append("|");
		}
		
		Bitacora bitacora = new Bitacora();
		bitacora.setUsuario(usuario.getClave());
		bitacora.setFecha(Calendar.getInstance().getTime());
		bitacora.setMovimiento(MovimientosConstants.ACEPTA_OFICIO);
		bitacora.setDetalle(StringUtils.substring(detalle.toString(), 0, 100));
		bitacoraDAO.save(bitacora);
	}
	
	@PreAuthorize("hasRole('AUTORIZA_OFICIOS')")
	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly= false, rollbackFor=ApplicationException.class)
	public void enviarOficios(Integer oficios[], Usuario usuario) throws ApplicationException {
		
		StringBuilder detalle = new StringBuilder();
		for(Integer oficio : oficios){
			
			int rowsAfected = oficioDAO.enviarOficio(oficio, usuario.getClave());
			
			if(rowsAfected == 0){
				throw new ApplicationException(MessageUtils.getMessage("NotValid.oficio.estatus.generado",new Object[]{oficio}));
			}
			
			detalle.append(oficio);
			detalle.append("|");
		}
		
		Bitacora bitacora = new Bitacora();
		bitacora.setUsuario(usuario.getClave());
		bitacora.setFecha(Calendar.getInstance().getTime());
		bitacora.setMovimiento(MovimientosConstants.ENVIA_OFICIO);
		bitacora.setDetalle(StringUtils.substring(detalle.toString(), 0, 100));
		bitacoraDAO.save(bitacora);
		
	}

	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly= true)
	public Oficio getOficio(Integer num) {
		return oficioDAO.getById(num);
	} 
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly= true)
	public Oficio existOficioDependencia(Short dependencia, Date ini, Date fin){
		return oficioDAO.existOficioDependencia(dependencia, ini, fin);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly= false, rollbackFor=Exception.class)
	public void marcarOficios(List<Oficio> oficios, String user){
		oficioDAO.marcarOficios(oficios, user);
	}
	
}
