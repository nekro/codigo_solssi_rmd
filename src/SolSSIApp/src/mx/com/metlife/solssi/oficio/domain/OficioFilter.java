package mx.com.metlife.solssi.oficio.domain;

import java.io.Serializable;
import java.util.Date;

public class OficioFilter implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6565529429821927357L;

	protected Integer numOficio;
	
	protected Short dependencia;
	
	protected Short estatus;
	
	protected Integer proceso;
	
	protected Date fecha;

	/**
	 * @return el numOficio
	 */
	public Integer getNumOficio() {
		return numOficio;
	}

	/**
	 * @param numOficio el numOficio a establecer
	 */
	public void setNumOficio(Integer numOficio) {
		this.numOficio = numOficio;
	}

	/**
	 * @return el dependencia
	 */
	public Short getDependencia() {
		return dependencia;
	}

	/**
	 * @param dependencia el dependencia a establecer
	 */
	public void setDependencia(Short dependencia) {
		this.dependencia = dependencia;
	}

	/**
	 * @return el estatus
	 */
	public Short getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus el estatus a establecer
	 */
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return el proceso
	 */
	public Integer getProceso() {
		return proceso;
	}

	/**
	 * @param proceso el proceso a establecer
	 */
	public void setProceso(Integer proceso) {
		this.proceso = proceso;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
	
	

}
