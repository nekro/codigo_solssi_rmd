package mx.com.metlife.solssi.oficio.persistence;

import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.main.dao.GenericDAO;
import mx.com.metlife.solssi.oficio.domain.Oficio;
import mx.com.metlife.solssi.oficio.domain.OficioFilter;

public interface OficioDAO extends GenericDAO<Oficio, Integer>{
	
	public int updateNumOficioSolicitud(Integer numFolio, Integer numOficio);
	
	public List<Oficio> getOficios(OficioFilter filter);
	
	public int enviarOficio(Integer oficio, String user);
	
	public int aceptarOficio(Integer oficio, String user);
	
	public int marcarSolicitudesEnviadas(Integer numOficio);
	
	public Oficio existOficioDependencia(Short dependencia, Date ini, Date fin);
	
	public void marcarOficios(List<Oficio> oficios, String user);
	
}
