package mx.com.metlife.solssi.oficio.persistence;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.esquemas.domain.OficioEsquema;
import mx.com.metlife.solssi.esquemas.domain.OficioFilterEsquema;
import mx.com.metlife.solssi.main.dao.GenericDAOImpl;
import mx.com.metlife.solssi.oficio.domain.Oficio;
import mx.com.metlife.solssi.oficio.domain.OficioFilter;
import mx.com.metlife.solssi.util.AppConstants;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class OficioDAOImpl extends GenericDAOImpl<Oficio, Integer> implements OficioDAO{
	
	
	public int updateNumOficioSolicitud(Integer numFolio, Integer numOficio){
		Session session = getSession();
		Query query = session.getNamedQuery("updateOficio");
		query.setParameter("folio", numFolio);
		query.setParameter("oficio", numOficio);
		return query.executeUpdate();
	}
	
	
	public Oficio existOficioDependencia(Short dependencia, Date ini, Date fin){
		Session session = getSession();
		Query query = session.createQuery("select o from Oficio o where dependencia = :dependencia and truncate(fechaGeneracion) between :fIni and :fFin");
		
		query.setParameter("dependencia", dependencia);
		query.setParameter("fIni", ini);
		query.setParameter("fFin", fin);
		Oficio result = (Oficio) query.uniqueResult();
		
		return result;
	}
	
		
	@SuppressWarnings("unchecked")
	public List<Oficio> getOficios(OficioFilter filter){
		Session session = getSession();
		
		Criteria crit;
		
		if(filter instanceof OficioFilterEsquema){
			crit = session.createCriteria(OficioEsquema.class);
		}else{
			crit = session.createCriteria(Oficio.class);	
		}
		
		
		if(filter.getNumOficio() != null && filter.getNumOficio() > 0){
			crit.add(Restrictions.eq("numOficio", filter.getNumOficio()));
		}
		
		if(filter.getDependencia() != null && filter.getDependencia() > 0){
			crit.add(Restrictions.eq("dependencia", filter.getDependencia()));
		}
		
		if(filter.getEstatus() != null && filter.getEstatus() > 0){
			crit.add(Restrictions.eq("estatus", filter.getEstatus()));
		}
		
		if(filter.getProceso() != null && filter.getProceso() > 0){
			crit.add(Restrictions.eq("idProceso", filter.getProceso()));
		}
		
		if(filter.getFecha() != null){
			crit.add(Restrictions.ge("fechaGeneracion", filter.getFecha()));
		}
		
		return crit.list();
	}

	public int aceptarOficio(Integer numOficio, String user) {
		Session session = getSession();
		Query query = session.getNamedQuery("updateEstatusOficioAceptado");
		query.setParameter("oficio", numOficio);
		query.setParameter("estatus_nuevo", AppConstants.OFICIO_ESTATUS_ACEPTADO);
		query.setParameter("estatus_ant", AppConstants.OFICIO_ESTATUS_GENERADO);
		query.setParameter("user", user);
		query.setParameter("fecha", Calendar.getInstance().getTime());
		return query.executeUpdate();
	}
	
	public int enviarOficio(Integer numOficio, String user) {
		Session session = getSession();
		Query query = session.getNamedQuery("updateEstatusOficioEnviado");
		query.setParameter("oficio", numOficio);
		query.setParameter("estatus_nuevo", AppConstants.OFICIO_ESTATUS_ENVIADO);
		query.setParameter("estatus_ant", AppConstants.OFICIO_ESTATUS_ACEPTADO);
		query.setParameter("user", user);
		query.setParameter("fecha", Calendar.getInstance().getTime());
		return query.executeUpdate();
	}
	
	
	public void marcarOficios(List<Oficio> oficios, String user) {
		
		Session session = getSession();
		
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE Oficio SET ");
		sql.append("bndAutomatico = :automatico, ");
		sql.append("bndCheque = :cheque, ");
		sql.append("bndPrestamo = :prestamo, ");
		sql.append("bndJuridico = :juridico, ");
		sql.append("bndPagomatico = :pagomatico, ");
		sql.append("fechaEnvio = :fecha, usuarioEnvia = :user ");
		sql.append("WHERE numOficio = :oficio");
		
		Query query = session.createQuery(sql.toString());
		
		for(Oficio oficio : oficios){
			query.setInteger("automatico", oficio.getBndAutomatico());
			query.setInteger("cheque",oficio.getBndCheque());
			query.setInteger("prestamo",oficio.getBndPrestamo());
			query.setInteger("juridico",oficio.getBndJuridico());
			query.setInteger("pagomatico",oficio.getBndPagomatico());
			query.setDate("fecha",Calendar.getInstance().getTime());
			query.setString("user",user);
			query.setInteger("oficio", oficio.getNumOficio());
			query.executeUpdate();
		}
		
	}
	
		
	public int marcarSolicitudesEnviadas(Integer numOficio) {
		Session session = getSession();
		Query query = session.getNamedQuery("marcarSolicitudesEnviadas");
		query.setParameter("oficio", numOficio);
		query.setParameter("estatusAceptado", AppConstants.ESTATUS_ACEPTADO);
		query.setParameter("estatusEnviado", AppConstants.ESTATUS_EN_PROCESO);
		return query.executeUpdate();
	}
	
}
