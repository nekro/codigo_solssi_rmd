package mx.com.metlife.solssi.autorizacion.mvc;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.inicio.service.GruposCapturaService;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.solicitud.service.SolicitudService;
import mx.com.metlife.solssi.usuario.domain.UserPrincipal;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.BrowserUtils;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("autorizacion")
public class AutorizacionController {
	
	@Autowired
	private SolicitudService solicitudService;
	
	@Autowired
	private GruposCapturaService groupService;
	
	@RequestMapping(method=RequestMethod.POST)
	public String showConsulta(Model model, HttpSession session){
		
		session.removeAttribute("solicitud");
		
		model.addAttribute(new ConsultaFilter());
		return "autorizacion";
	}
	
	
	@RequestMapping(value = "consulta", method=RequestMethod.POST)
	public String executeConsulta(@Valid ConsultaFilter filtro, BindingResult errors, Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		model.addAttribute(filtro);
		
		if(usuario.getDependencia() == null || usuario.getDependencia() <= 0){
			errors.reject("NotEmpty.consultaFilter.dependencia");
		}
		
		if(filtro.isEmpty()){
			errors.reject("NotEmpty.consultaFilter.object");
			return "autorizacion";
		}
		
		if(errors.hasErrors()){
			return "autorizacion";	
		}
		
		filtro.setDependencia(usuario.getDependencia());
		filtro.setEstatus(AppConstants.ESTATUS_CAPTURA);
		List<Solicitud> list = solicitudService.get(filtro);
		
		if(list == null || list.isEmpty()){
			if(filtro.getNumFolio()!=null && filtro.getNumFolio()>0){
				errors.reject("NotEmpty.auth.result.folio");	
			}else{
				errors.reject("NotEmpty.auth.result.rfc_cuenta");
			}
		}
		
		model.addAttribute("solicitudes", list);
		
		BrowserUtils.enableHistoryBack(model);
		
		return "autorizacion";
	}
	
	@RequestMapping(value = "desplegar", method=RequestMethod.POST)
	public String showSolicitud(@RequestParam("numFolio") Integer folio, HttpSession session, Model model){
		
		Solicitud solicitud = solicitudService.get(folio);
		session.setAttribute("solicitud", solicitud);
		model.addAttribute("auth", true);
		
		return "autorizacion/solicitud";
	}
	
	
	
	@RequestMapping(value = "execute/autorizacion", method=RequestMethod.POST)
	public String executeAutorizacion(@RequestParam(value = "checkClabe", required = false) String checkClabe, Model model, HttpSession session, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		Solicitud solicitud = (Solicitud) session.getAttribute("solicitud");
		
		try {
			
			groupService.checkMaxFechaAutorization();
			
			if(StringUtils.isNotEmpty(checkClabe)){
				String clabe = solicitudService.getClabe(solicitud.getNumFolio());
				if(!StringUtils.equals(clabe, checkClabe)){
					throw new ApplicationException(MessageUtils.getMessage("check.clabe.NotMatch"));
				}
			}
			
			groupService.checkFechaMaxAutorizacion(solicitud.getRfc(), solicitud.getCuenta());
			
			solicitudService.updateEstatusAutorizacion(solicitud, AppConstants.ESTATUS_ACEPTADO, usuario.getClave());
			model.addAttribute("autorizado", "true");
		} catch (ApplicationException e) {
			model.addAttribute("error", e.getMessage());
		}
		
		model.addAttribute("folio", solicitud.getUserFolio());
		
		session.removeAttribute("solicitud");
		
		return "autorizacion/result";
	}
	
	
	
	
	@RequestMapping(value = "execute/rechazo" , method=RequestMethod.POST)
	public String executeRechazo(Model model, HttpSession session, Authentication auth){
		try {
			
			groupService.checkMaxFechaAutorization();
			
			Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
			
			Solicitud solicitud = (Solicitud) session.getAttribute("solicitud");
			model.addAttribute("folio", solicitud.getUserFolio());
			
			groupService.checkFechaMaxAutorizacion(solicitud.getRfc(), solicitud.getCuenta());
			solicitudService.updateEstatusAutorizacion(solicitud, AppConstants.ESTATUS_RECHAZADO, usuario.getClave());
			model.addAttribute("rechazado", "true");
			
		} catch (ApplicationException e) {
			model.addAttribute("error", e.getMessage());
		}
		
		session.removeAttribute("solicitud");
		
		return "autorizacion/result";
	}


}
