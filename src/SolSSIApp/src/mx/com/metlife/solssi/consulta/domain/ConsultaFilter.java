package mx.com.metlife.solssi.consulta.domain;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Pattern;

import mx.com.metlife.solssi.util.Base26Codec;

import org.apache.commons.lang.StringUtils;


public class ConsultaFilter implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7414776184774021163L;

	protected Integer numFolio;
	
	@Pattern(regexp="(^[A-Za-z]{2}[0-9]{4}$)?")
	protected String userFolio;
	
	@Pattern(regexp="(^[A-Za-z]{4}[0-9]{6}$)?")
	protected String rfc;
	
	@Pattern(regexp="([0-9]{10})?")
	protected String cuenta;
	
	protected String homoclave;
	
	protected Short estatus;
	
	protected Short dependencia;
	
	protected Integer oficio;
	
	protected Integer maxResults;
	
	protected Short estatusList[];
	
	protected Date fechaInicioCaptura;
	
	protected Date fechaFinalCaptura;
	
	/**
	 * @return the numFolio
	 */
	public Integer getNumFolio() {
		if(this.numFolio == null || this.numFolio == 0){
			if(StringUtils.isNotEmpty(userFolio)){
				this.numFolio = Base26Codec.decodeFolioSolicitud(userFolio);
			}
		}
		return this.numFolio;
	}

	/**
	 * @param numFolio the numFolio to set
	 */
	public void setNumFolio(Integer numFolio) {
		this.numFolio = numFolio;
	}

	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return StringUtils.upperCase(StringUtils.trim(rfc));
	}

	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * @return the homoclave
	 */
	public String getHomoclave() {
		return homoclave;
	}

	/**
	 * @param homoclave the homoclave to set
	 */
	public void setHomoclave(String homoclave) {
		this.homoclave = homoclave;
	}

	public String getUserFolio() {
		return StringUtils.upperCase(StringUtils.trim(userFolio));
	}

	public void setUserFolio(String userFolio) {
		this.userFolio = StringUtils.trim(StringUtils.upperCase(userFolio));
	}

	public Short getEstatus() {
		return estatus;
	}

	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}
	
	public Long getNumCuenta(){
		if(StringUtils.isNotEmpty(cuenta)){
			return Long.valueOf(cuenta);
		}
		return 0l;
	}
	
	
	public boolean isEmpty(){
		return (StringUtils.isEmpty(cuenta)) && (StringUtils.isEmpty(rfc) && StringUtils.isEmpty(userFolio));
	}

	/**
	 * @return el dependencia
	 */
	public Short getDependencia() {
		return dependencia;
	}

	/**
	 * @param dependencia el dependencia a establecer
	 */
	public void setDependencia(Short dependencia) {
		this.dependencia = dependencia;
	}

	/**
	 * @return el oficio
	 */
	public Integer getOficio() {
		return oficio;
	}

	/**
	 * @param oficio el oficio a establecer
	 */
	public void setOficio(Integer oficio) {
		this.oficio = oficio;
	}

	/**
	 * @return el maxResults
	 */
	public Integer getMaxResults() {
		return maxResults;
	}

	/**
	 * @param maxResults el maxResults a establecer
	 */
	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}
	
	/**
	 * 
	 * @param est
	 */
	public void setEstatusList(Short... est){
		this.estatusList = est;
	}
	
	/**
	 * 
	 * @return
	 */
	public Short[] getEstatusList(){
		return this.estatusList;
	}

	public Date getFechaInicioCaptura() {
		return fechaInicioCaptura;
	}

	public void setFechaInicioCaptura(Date fechaInicioCaptura) {
		this.fechaInicioCaptura = fechaInicioCaptura;
	}

	public Date getFechaFinalCaptura() {
		return fechaFinalCaptura;
	}

	public void setFechaFinalCaptura(Date fechaFinalCaptura) {
		this.fechaFinalCaptura = fechaFinalCaptura;
	}

}
