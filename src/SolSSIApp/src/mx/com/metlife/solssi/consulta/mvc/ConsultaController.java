package mx.com.metlife.solssi.consulta.mvc;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.consulta.domain.ReporteTotales;
import mx.com.metlife.solssi.esquemas.service.SolicitudEsquemaService;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.solicitud.service.SolicitudService;
import mx.com.metlife.solssi.usuario.domain.UserPrincipal;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.util.BrowserUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;




@Controller
@RequestMapping("consulta")
public class ConsultaController {
	
	@Autowired
	private SolicitudService solicitudService;
	
	@Autowired
	private SolicitudEsquemaService solicitudEsquemaService;
	
	@RequestMapping(method=RequestMethod.POST)
	public String showConsulta(Model model){
		model.addAttribute(new ConsultaFilter());
		return "consulta";
	}
	
	
	@RequestMapping(value = "execute", method=RequestMethod.POST)
	public String executeConsulta(@Valid ConsultaFilter filtro, BindingResult errors, Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		model.addAttribute(filtro);
		
		if(filtro.isEmpty()){
			errors.reject("NotEmpty.consultaFilter.object");
			return "consulta";
		}
		
		if(errors.hasErrors()){
			return "consulta";	
		}
		
		filtro.setDependencia(usuario.getDependencia());
		List<Solicitud> list = solicitudService.get(filtro, true);
		
		model.addAttribute("solicitudes", list);
		
		this.checkErroresResult(list, filtro, usuario, errors);
		
		BrowserUtils.enableHistoryBack(model);
		
		return "consulta";
	}
	
	@RequestMapping(value = "view", method=RequestMethod.POST)
	public String showSolicitud(@RequestParam("numFolio") Integer folio, Model model){
		
		Solicitud solicitud = solicitudService.get(folio);
		model.addAttribute("solicitud", solicitud);
		model.addAttribute("urlRegresar", "/consulta");
		
		return "consulta/solicitud";
	}
	
	@RequestMapping(value = "dependientes" , method=RequestMethod.POST)
	public String showDependientes(@RequestParam("folio") Integer folio, Model model, HttpSession session){
		
		Solicitud solicitud = solicitudService.get(folio);
		session.setAttribute("solicitud", solicitud);
		
		return "consulta/dependientes";
	}
	
	@RequestMapping("dependientes/regresar")
	public String backDependientes(){
		return "consulta/dependientes";
	}
	
	
	@RequestMapping(value = "info/dependiente", method=RequestMethod.POST)
	public String showDependienteInfo(@RequestParam("numero") Short numero, Model model, HttpSession session){
		
		Solicitud solicitud = (Solicitud) session.getAttribute("solicitud");
		model.addAttribute(solicitud.getDependiente(numero));
		
		return "consulta/info/dependiente";
	}
	
	@RequestMapping(value = "totales", method=RequestMethod.POST)
	public String showTotalesPage(Model model){
		model.addAttribute(new ConsultaFilter());
		return "consulta/totales";
	}
	
	@RequestMapping(value = "totales/execute", method=RequestMethod.POST)
	public String executeTotalesPage(ConsultaFilter filter, Model model){
		ReporteTotales reporte = solicitudService.getTotales(filter.getDependencia());
		ReporteTotales reporteEsquemas = solicitudEsquemaService.getTotales(filter.getDependencia());
		model.addAttribute("reporte", reporte);
		model.addAttribute("reporteEsquemas", reporteEsquemas);
		model.addAttribute(filter);
		return "consulta/totales";
	}
	
	
	/**
	 * revisa si existen errores en el resultado de la consulta
	 * @param list
	 * @param filtro
	 * @param usuario
	 * @param errors
	 */
	private void checkErroresResult(List<Solicitud> list, ConsultaFilter filtro, Usuario usuario, BindingResult errors){
	
		if(list == null || list.isEmpty()){
			if(usuario.getDependencia() != null && usuario.getDependencia() > 0){
				if(filtro.getNumFolio()!=null && filtro.getNumFolio()>0){
					errors.reject("NotEmpty.consultaFilter.dependencia.folio");	
				}else{
					if(StringUtils.isNotEmpty(filtro.getRfc())){
						errors.reject("NotEmpty.consultaFilter.dependencia.rfc");
					}else{
						if(StringUtils.isNotEmpty(filtro.getCuenta())){
							errors.reject("NotEmpty.consultaFilter.dependencia.cuenta");	
						}
					}
				}
				
			}else{
				if(filtro.getNumFolio()!=null && filtro.getNumFolio()>0){
					errors.reject("NotEmpty.consultaFilter.callcenter.folio");	
				}else{
					if(StringUtils.isNotEmpty(filtro.getRfc())){
						errors.reject("NotEmpty.consultaFilter.callcenter.rfc");
					}else{
						if(StringUtils.isNotEmpty(filtro.getCuenta())){
							errors.reject("NotEmpty.consultaFilter.callcenter.cuenta");	
						}
					}
				}
			}
		}
	}
	
}
