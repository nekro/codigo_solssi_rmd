package mx.com.metlife.solssi.consulta.domain;

import java.io.Serializable;

public class ReporteTotales implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3729245871317629233L;

	private Integer canceladas;
	
	private Integer aceptadas;
	
	private Integer rechazadas;
	
	private Integer iniciadas;
	
	private Integer enviadasRobot;
	
	private Integer aceptadasRobot;
	
	private Integer pagadas;
	
	private Integer rechazadasBanco;
	
	private Integer total;

	public Integer getCanceladas() {
		return canceladas;
	}

	public void setCanceladas(Integer canceladas) {
		this.canceladas = canceladas;
	}

	public Integer getAceptadas() {
		return aceptadas;
	}

	public void setAceptadas(Integer aceptadas) {
		this.aceptadas = aceptadas;
	}

	public Integer getRechazadas() {
		return rechazadas;
	}

	public void setRechazadas(Integer rechazadas) {
		this.rechazadas = rechazadas;
	}

	public Integer getIniciadas() {
		return iniciadas;
	}

	public void setIniciadas(Integer iniciadas) {
		this.iniciadas = iniciadas;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	/**
	 * @return el enviadasRobot
	 */
	public Integer getEnviadasRobot() {
		return enviadasRobot;
	}

	/**
	 * @param enviadasRobot el enviadasRobot a establecer
	 */
	public void setEnviadasRobot(Integer enviadasRobot) {
		this.enviadasRobot = enviadasRobot;
	}

	/**
	 * @return el aceptadasRobot
	 */
	public Integer getAceptadasRobot() {
		return aceptadasRobot;
	}

	/**
	 * @param aceptadasRobot el aceptadasRobot a establecer
	 */
	public void setAceptadasRobot(Integer aceptadasRobot) {
		this.aceptadasRobot = aceptadasRobot;
	}

	/**
	 * @return el pagadas
	 */
	public Integer getPagadas() {
		return pagadas;
	}

	/**
	 * @param pagadas el pagadas a establecer
	 */
	public void setPagadas(Integer pagadas) {
		this.pagadas = pagadas;
	}

	/**
	 * @return el rechazadasBanco
	 */
	public Integer getRechazadasBanco() {
		return rechazadasBanco;
	}

	/**
	 * @param rechazadasBanco el rechazadasBanco a establecer
	 */
	public void setRechazadasBanco(Integer rechazadasBanco) {
		this.rechazadasBanco = rechazadasBanco;
	}
	
	

}
