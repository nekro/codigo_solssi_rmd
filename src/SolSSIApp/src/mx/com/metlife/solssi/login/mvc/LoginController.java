package mx.com.metlife.solssi.login.mvc;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.com.metlife.solssi.param.service.ParametersService;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.util.BrowserUtils;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("login")
@SessionAttributes({"opcionMenu","opcionSubMenu"})
public class LoginController {
	
	
	@Autowired
	private ParametersService paramService;
	
	@ModelAttribute
	public Usuario getUsuario(){
		return new Usuario();
	}
	
	@RequestMapping
	public String showLogin(Model model, @RequestParam(value = "esquemas", required = false) String esquemas, HttpSession session){
		String urlCambio = paramService.getAppParamValue("URL_CAMBIO_PASSWORD","#");
		model.addAttribute("URL_CAMBIO_PASSWORD", urlCambio);
		String urlOlvido = paramService.getAppParamValue("URL_OLVIDO_PASSWORD","#");
		model.addAttribute("URL_OLVIDO_PASSWORD", urlOlvido);
		
		if(esquemas != null){
			session.setAttribute("tipoMenu", "ESQUEMAS");
		}
		
		BrowserUtils.setFecha(session);
		
		return "login";
	}
	
	@RequestMapping("session_max_exceeded")
	public String shorMaxExceed(HttpServletRequest request){
		request.setAttribute("MAX_SESSION_EXCEEDED", true);
		return "login";
	}
	
	@RequestMapping("expired")
	public String showExpired(HttpServletRequest request){
		return "session/expired";
	}
	
	@RequestMapping("concurrent")
	public String showExpiredConcurrent(HttpServletRequest request, Model model){
		model.addAttribute("concurrent","true");
		return "session/expired";
	}
	
	
	@RequestMapping("accessDenied")
	public void accessDenied(ModelMap model,
			HttpServletRequest request) {
			AccessDeniedException ex = (AccessDeniedException)
			request.getAttribute(AccessDeniedHandlerImpl
			.SPRING_SECURITY_ACCESS_DENIED_EXCEPTION_KEY);
			StringWriter sw = new StringWriter();
			model.addAttribute("errorDetails", MessageUtils.getMessage("access.denied"));
			ex.printStackTrace(new PrintWriter(sw));
			model.addAttribute("errorTrace", sw.toString());
	}
	
	@RequestMapping(value="opcmenu")
    public @ResponseBody String setOpcionMenu(@RequestParam("opcion") String opcion, Model model, HttpServletRequest request){
		
		if(request.isRequestedSessionIdValid() && StringUtils.isNotEmpty(opcion) && StringUtils.isNumeric(opcion)){
			model.addAttribute("opcionMenu", opcion);
			model.addAttribute("opcionSubMenu", "1");
	        return "true";
		}else{
			return "false";
		}
        
    }
	
	@RequestMapping(value="opcsubmenu")
    public @ResponseBody String setOpcionSubMenu(@RequestParam("opcion") String opcion, Model model, HttpServletRequest request){
		if(request.isRequestedSessionIdValid() && StringUtils.isNotEmpty(opcion) && StringUtils.isNumeric(opcion)){
			model.addAttribute("opcionSubMenu", opcion);
			return "true";
		}else{
			return "false";
		}
        
        
    }
	

}
