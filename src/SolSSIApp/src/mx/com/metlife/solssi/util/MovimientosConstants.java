package mx.com.metlife.solssi.util;

public class MovimientosConstants {
	
	public static final short ALTA_USUARIO = 1;
	public static final short MODIFICACION_USUARIO = 2;
	
	public static final short ALTA_ROL = 3;
	public static final short MODIFICACION_ROL = 4;
	
	public static final short GENERA_OFICIO = 5;
	public static final short ACEPTA_OFICIO = 6;
	public static final short ENVIA_OFICIO = 7;
	
	public static final short MODIFICACION_RFC_PERMISOS = 8;
	
	public static final short CANCELACION_SOLICITUD = 9;
	
	public static final short ALTA_CLIENTE = 10;
	public static final short MODIFICACION_CLIENTE = 11;
	
	
	public static final short GENERA_OFICIO_ESQUEMAS = 12;
	public static final short ACEPTA_OFICIO_ESQUEMAS = 13;
	public static final short ENVIA_OFICIO_ESQUEMAS = 14;
	
}
