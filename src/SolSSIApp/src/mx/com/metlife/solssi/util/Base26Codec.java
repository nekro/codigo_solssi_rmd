package mx.com.metlife.solssi.util;

import org.apache.commons.lang.StringUtils;

public class Base26Codec {

	    public static String encodeFolioSolicitud (int folio) {
    	   String encoded = "";
    	   String letters = "";
    	   String numbers = "";
    	   int MAX_NUMBER = 9999;
    	   int remainder = (int)(folio / MAX_NUMBER);
    	   letters=toBase26(remainder);
    	   if(letters.length() == 1) {
    		  letters = "A" + letters; 
    	   }else if(letters.length() == 2){
    		  letters = "" + letters;
    	   }else {
    		  letters = ""; 
    	   }
    	   
    	   numbers = String.valueOf(folio - (MAX_NUMBER * remainder));
    	   numbers = StringUtils.leftPad(numbers, 4, '0');
    	   
    	   encoded = letters+numbers;
    	   
    	   return encoded; 
    }
	    
	    
    public static int decodeFolioSolicitud (String folio) {
	    	   int decoded = 0;
	    	   String letters = folio.substring(0, 2);
	    	   int numbers = Integer.valueOf(folio.substring(2, 6));
	    	   int MAX_NUMBER = 9999;
	    	   int remainder=fromBase26(letters);
	    	   decoded = numbers + (MAX_NUMBER * remainder);
	    	   return decoded; 
	}
	
	    
	public static String toBase26(int number) {
    	   number = Math.abs(number);
    	   String converted = "";
    	   do {
    		   int remainder = number % 26;
    		   converted = (char)(remainder + 'A') + converted;
    		   number = (number - remainder) / 26;
    	   } while (number > 0);
    	   return converted;
    }

    public static int fromBase26(String number) {
    	   int s = 0;
    	   if (number != null && number.length() > 0) {
    		   s = (number.charAt(0) - 'A');
    		   for (int i=1; i < number.length(); i++) {
    			   s*=26;
    			   s+= (number.charAt(i) - 'A');
    	     }
    	   }
    	   return s;
    }

 
    public static String toBase26Bijective(int n) {
    	    StringBuffer ret = new StringBuffer();
    	    while(n>0) {
                  --n;
                  ret.append((char)('A' + n%26));
                  n/=26;
    	    }
    	    return ret.reverse().toString();
     }


}
