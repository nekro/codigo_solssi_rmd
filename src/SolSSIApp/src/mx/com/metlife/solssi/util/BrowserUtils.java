package mx.com.metlife.solssi.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.ui.Model;

public class BrowserUtils {

	public static void enableHistoryBack(Model model) {
		model.addAttribute("enable_history", "true");
		model.addAttribute("disabled_close_session", "true");
	}

	public static void disableCloseSession(Model model) {
		model.addAttribute("disabled_close_session", "true");
	}

	public static void setFecha(HttpSession session) {
		SimpleDateFormat format = new SimpleDateFormat("dd 'de' MMMMMMMMMMMMMMM 'del' yyyy");
		Date date = Calendar.getInstance().getTime();
		String f = format.format(date).toLowerCase();
		String fecha = f.substring(0, 6) + f.substring(6, 7).toUpperCase() + f.substring(7);
		session.setAttribute("FECHA_ACTUAL", fecha);

		SimpleDateFormat formatDia = new SimpleDateFormat("EEEEEEEEEEEE");
		f = formatDia.format(date);
		session.setAttribute("DIA_ACTUAL", StringUtils.capitalize(f));
	}

}
