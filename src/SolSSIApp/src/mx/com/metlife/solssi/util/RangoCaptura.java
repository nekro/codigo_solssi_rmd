package mx.com.metlife.solssi.util;

import java.util.Date;

public class RangoCaptura {
	
	private int first;
	
	private int last;
	
	private Date maxFechaAutorizacion;
	
	private Date fechaInicialOficios;
	
	private Date fechaFinalOficios;
	
	private Date fechaInicialCaptura;
	
	private Date fechaFinalCaptura;
	
	private Date fechaAplicacion;
	
	private String fmtFechaAplicacion;

	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getLast() {
		return last;
	}

	public void setLast(int last) {
		this.last = last;
	}

	public Date getMaxFechaAutorizacion() {
		return maxFechaAutorizacion;
	}

	public void setMaxFechaAutorizacion(Date maxFechaAutorizacion) {
		this.maxFechaAutorizacion = maxFechaAutorizacion;
	}

	public Date getFechaInicialOficios() {
		return fechaInicialOficios;
	}

	public void setFechaInicialOficios(Date fechaInicialOficios) {
		this.fechaInicialOficios = fechaInicialOficios;
	}

	public Date getFechaFinalOficios() {
		return fechaFinalOficios;
	}

	public void setFechaFinalOficios(Date fechaFinalOficios) {
		this.fechaFinalOficios = fechaFinalOficios;
	}

	public Date getFechaInicialCaptura() {
		return fechaInicialCaptura;
	}

	public void setFechaInicialCaptura(Date fechaInicialCaptura) {
		this.fechaInicialCaptura = fechaInicialCaptura;
	}

	public Date getFechaFinalCaptura() {
		return fechaFinalCaptura;
	}

	public void setFechaFinalCaptura(Date fechaFinalCaptura) {
		this.fechaFinalCaptura = fechaFinalCaptura;
	}

	/**
	 * @return the fechaAplicacion
	 */
	public Date getFechaAplicacion() {
		return fechaAplicacion;
	}

	/**
	 * @param fechaAplicacion the fechaAplicacion to set
	 */
	public void setFechaAplicacion(Date fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}

	/**
	 * @return the fmtFechaAplicacion
	 */
	public String getFmtFechaAplicacion() {
		return fmtFechaAplicacion;
	}

	/**
	 * @param fmtFechaAplicacion the fmtFechaAplicacion to set
	 */
	public void setFmtFechaAplicacion(String fmtFechaAplicacion) {
		this.fmtFechaAplicacion = fmtFechaAplicacion;
	}
	
	
	
	
}
