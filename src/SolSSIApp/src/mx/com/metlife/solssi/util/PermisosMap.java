package mx.com.metlife.solssi.util;

import java.util.HashMap;
import java.util.Map;

public class PermisosMap {
	
	private static Map<String, String> permisos;
	
	static{
		permisos = new HashMap<String, String>();
		
		permisos.put("1", "SOLICITUD_CONSULTAS");
		permisos.put("2", "SOLICITUD_AUTORIZACIONES");
		permisos.put("3", "GENERA_OFICIOS");
		permisos.put("4", "AUTORIZA_OFICIOS");
		permisos.put("5", "FECHA_RFCS");
		permisos.put("6", "ADMIN_CATALOGOS");
		permisos.put("7", "ADMIN_USUARIOS");
		permisos.put("8", "REPORTE_TOTALES");
		permisos.put("9", "ADMIN_CLIENTES");
		permisos.put("10", "HISTORICOS");
		permisos.put("11", "CC_VARIABLES");
		permisos.put("12", "CIFRAS_CONTROL");
		
		permisos.put("13", "ESQ_SOLICITUD_CONSULTAS");
		permisos.put("14", "ESQ_SOLICITUD_REVISION");
		permisos.put("15", "ESQ_GENERA_OFICIOS");
		permisos.put("16", "ESQ_AUTORIZA_OFICIOS");
		permisos.put("17", "ESQ_ADMINISTRACION");
	}
	
	public static Map<String, String>getPermisosMap(){
		return permisos;
	}

}
