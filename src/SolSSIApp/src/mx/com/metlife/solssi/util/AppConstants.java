package mx.com.metlife.solssi.util;

public class AppConstants {
	
	public static final String CVE_USER_CLIENTE = "CLIENTE";
	
	public static final short PAIS_MEXICO = 1;
	
	public static final short ESTATUS_CAPTURA = 1;
	public static final short ESTATUS_ACEPTADO = 2;
	public static final short ESTATUS_RECHAZADO = 3;
	public static final short ESTATUS_EN_PROCESO = 4;
	public static final short ESTATUS_ERROR_PROCESO = 5;
	public static final short ESTATUS_PROCESO_OK = 6;
	public static final short ESTATUS_PAGADA = 7;
	public static final short ESTATUS_RECHAZO_BANCO = 8;
	public static final short ESTATUS_CANCELADA = 9;
	public static final short ESTATUS_RECHAZADA_CADUCIDAD = 10;
	
	public static final short OFICIO_ESTATUS_GENERADO = 1;
	public static final short OFICIO_ESTATUS_ACEPTADO = 2;
	public static final short OFICIO_ESTATUS_ENVIADO = 3;
	public static final short OFICIO_ESTATUS_PROCESO = 4;
	
	public static final short TRAMITE_RESCATE_PARCIAL = 1;
	public static final short TRAMITE_RESCATE_TOTAL = 2;
	public static final short TRAMITE_APORTACIONES_VOLUNTARIAS = 3;
	public static final short TRAMITE_RESCATE_PARCIAL_MAS_APOR_VOL = 13;
	
	public static final short PERMITIR_RETIRO_VALUE = 1;
	public static final short NO_PERMITIR_RETIRO = 0;
	public static final short NO_PERMITIR_RETIRO_NO_INFO = 3;
	
	public static final short MODO_PAGO_TRANSFERENCIA = 1;
	public static final short MODO_PAGO_OTRA_FORMA = 2;
	
	//Para generacion de PDF
	public static final short PDF_RESCATE_PARCIAL = 1;
	public static final short PDF_PAGO_TOTAL = 2;
	public static final short PDF_DEPENDIENTE = 3;
	
	public static final String MEDIA_TYPE_EXCEL = "application/vnd.ms-excel";
	public static final String MEDIA_TYPE_PDF = "application/pdf";
	public static final String EXTENSION_TYPE_EXCEL = "xls";
	public static final String EXTENSION_TYPE_PDF = "pdf";
	public static final String EXTENSION_TYPE_PDF_DOWNLOAD = "pdf_download";
	
	public static final String TEMPLATE_RESCATE_PARCIAL = "Sol_Resc_Parcial.jrxml";
	public static final String TEMPLATE_RESCATE_TOTAL   = "Sol_Resc_Total.jrxml";
	public static final String TEMPLATE_DEPENDIENTE     = "Sol_Datos_Articulo_140.jrxml";

	public static final short OCUPACION_OTRO = 7;
	
	
	public static final int ERROR_CODE_CUENTA_SSI = 1;
	
	
	
	public static final int CC_TIPO_WEB = 1;
	public static final int CC_TIPO_SSI_OK = 2;
	public static final int CC_TIPO_SSI_ER = 3;
	
	public static final short OFICIO_BND_SIN_ENVIAR = 0;
	public static final short OFICIO_BND_ENVIADO = 1;
	public static final short OFICIO_BND_PROCESADO = 2;
	

}
