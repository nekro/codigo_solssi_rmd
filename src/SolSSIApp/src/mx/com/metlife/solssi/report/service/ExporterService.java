package mx.com.metlife.solssi.report.service;

import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
 

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
 

import org.springframework.stereotype.Service;

import mx.com.metlife.solssi.util.AppConstants;
 
@Service
public class ExporterService {
 
	public HttpServletResponse export(String fileName, String documentType, 
			List<JasperPrint> jplist, 
			HttpServletResponse response,
			ByteArrayOutputStream baos
			) {
		
		if (documentType.equalsIgnoreCase(AppConstants.EXTENSION_TYPE_EXCEL)) {
			// Export to output stream
			exportXls(jplist, baos);
			 
			// Set our response properties
			// Here you can declare a custom filename
			//String fileName = "UserReport.xls";
			response.setHeader("Content-Disposition", "inline; filename=" + fileName);
			
			// Set content type
			response.setContentType(AppConstants.MEDIA_TYPE_EXCEL);
			response.setContentLength(baos.size());
			
			return response;
		}
		
		if (documentType.equalsIgnoreCase(AppConstants.EXTENSION_TYPE_PDF)) {
			// Export to output stream
			exportPdf(jplist, baos);
			 
			// Set our response properties
			// Here you can declare a custom filename
			//String fileName = "UserReport.pdf";
			response.setHeader("Content-Disposition", "inline; filename="+ fileName);
			
			// Set content type
			response.setContentType(AppConstants.MEDIA_TYPE_PDF);
			response.setContentLength(baos.size());
			
			return response;
			
		} 
		
		if (documentType.equalsIgnoreCase(AppConstants.EXTENSION_TYPE_PDF_DOWNLOAD)) {
			// Export to output stream
			exportPdf(jplist, baos);
			 
			// Set our response properties
			// Here you can declare a custom filename
			//String fileName = "Nombre_De_Documento.pdf";
			response.setHeader("Content-Disposition", "attachment;filename="+ fileName);
			
			// Set content type
			response.setContentType(AppConstants.MEDIA_TYPE_PDF);
			response.setContentLength(baos.size());
			
			return response;
			
		}
		
		throw new RuntimeException("No type set for type " + documentType);
	}
	
	
	public HttpServletResponse export(String type, 
			JasperPrint jasperprint, 
			HttpServletResponse response,
			ByteArrayOutputStream baos
			) {
		
		if (type.equalsIgnoreCase(AppConstants.EXTENSION_TYPE_EXCEL)) {
			// Export to output stream
			exportXls(jasperprint, baos);
			 
			// Set our response properties
			// Here you can declare a custom filename
			String fileName = "UserReport.xls";
			response.setHeader("Content-Disposition", "inline; filename=" + fileName);
			
			// Set content type
			response.setContentType(AppConstants.MEDIA_TYPE_EXCEL);
			response.setContentLength(baos.size());
			
			return response;
		}
		
		if (type.equalsIgnoreCase(AppConstants.EXTENSION_TYPE_PDF)) {
			// Export to output stream
			exportPdf(jasperprint, baos);
			 
			// Set our response properties
			// Here you can declare a custom filename
			String fileName = "UserReport.pdf";
			response.setHeader("Content-Disposition", "inline; filename="+ fileName);
			
			// Set content type
			response.setContentType(AppConstants.MEDIA_TYPE_PDF);
			response.setContentLength(baos.size());
			
			return response;
			
		} 
		
		throw new RuntimeException("No type set for type " + type);
	}
	
	
	
	public void exportXls(JasperPrint jp, ByteArrayOutputStream baos) {
		// Create a JRXlsExporter instance
		JRXlsExporter exporter = new JRXlsExporter();
		 
		// Here we assign the parameters jp and baos to the exporter
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		 
		// Excel specific parameters
		exporter.setParameter(JRXlsAbstractExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		exporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		exporter.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		 
		try {
			exporter.exportReport();
			
		} catch (JRException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void exportXls(List<JasperPrint> jplist, ByteArrayOutputStream baos) {
		// Create a JRXlsExporter instance
		JRXlsExporter exporter = new JRXlsExporter();
		 
		// Here we assign the parameters jp and baos to the exporter
		exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jplist);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		 
		// Excel specific parameters
		exporter.setParameter(JRXlsAbstractExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
		exporter.setParameter(JRXlsAbstractExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		exporter.setParameter(JRXlsAbstractExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		 
		try {
			exporter.exportReport();
			
		} catch (JRException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void exportPdf(JasperPrint jp, ByteArrayOutputStream baos) {
		// Create a JRXlsExporter instance
		JRPdfExporter exporter = new JRPdfExporter();
		 
		// Here we assign the parameters jp and baos to the exporter
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		 
		try {
			exporter.exportReport();
			
		} catch (JRException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void exportPdf(List<JasperPrint> jplist, ByteArrayOutputStream baos) {
		// Create a JRXlsExporter instance
		JRPdfExporter exporter = new JRPdfExporter();
		 
		// Here we assign the parameters jp and baos to the exporter
		exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jplist);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
		 
		try {
			exporter.exportReport();
			
		} catch (JRException e) {
			throw new RuntimeException(e);
		}
	}
	
}