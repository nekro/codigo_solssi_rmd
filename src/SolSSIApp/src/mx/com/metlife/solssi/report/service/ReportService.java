package mx.com.metlife.solssi.report.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.service.CatalogoService;
import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.inicio.service.GruposCapturaService;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.oficio.domain.Oficio;
import mx.com.metlife.solssi.report.util.ErrorBean;
import mx.com.metlife.solssi.solicitud.domain.DependienteEconomico;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.solicitud.service.SolicitudService;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.RangoCaptura;
import mx.com.metlife.solssi.util.message.MessageUtils;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
 
@Service
public class ReportService {
 
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private CatalogoService catalogoService;

	@Autowired
	private ExporterService exporterService;
	
	@Autowired
	private SolicitudService solicitudService;
	
	@Autowired
	private GruposCapturaService gruposService;
	
	private JasperReport jrSolicitudRescateParcialH1;
	private JasperReport jrSolicitudRescateParcialH2;
	private JasperReport jrSolicitudRescateParcialDepH1;
	private JasperReport jrSolicitudRescateParcialDepH2;
	private JasperReport jrErrores;
	private JasperReport jrOficio;
	
	
	
	/**
	* Writes the report to the output stream
	*/
	public void write(String token, HttpServletResponse response, ByteArrayOutputStream baos) {
		 
		try {
			// Retrieve output stream
			ServletOutputStream outputStream = response.getOutputStream();
			// Write to output stream
			baos.writeTo(outputStream);
			// Flush the stream
			outputStream.flush();
			
			// Remove download token
			tokenService.remove(token);
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	public void fillReportOficio(String documentType, HttpServletResponse response, Cliente cliente, HttpSession session) throws IOException{
		try {
			// 1. Add report parameters
			HashMap<String, Object> params = new HashMap<String, Object>();
			// params.put("Title", "User Report");
			// params.put("datasource", new JREmptyDataSource());

			JasperPrint jasperprint = null;
			//ClassPathResource classpathresource = null;
			//InputStream inputstream = null;
			//JasperDesign jasperdesign = null;
			//JasperReport jasperreport = null;

			List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
        	//classpathresource = new ClassPathResource("reportes/Oficio.jasper");
			//inputstream = classpathresource.getInputStream();
			// 3. Convert template to JasperDesign
			//jasperdesign = JRXmlLoader.load(inputstream);
			// 4. Compile design to JasperReport
			//JasperCompileManager.compileReportToFile(jasperdesign, "C:/Users/GCIT-HP-02/Desktop/Oficio.jasper");
		    //jasperreport = JasperCompileManager.compileReport(jasperdesign);
			//jasperreport = (JasperReport) JRLoader.loadObject(inputstream); 
			// 5. Create the JasperPrint object
	        // Make sure to pass the JasperReport and report parameters
			
		    List<Solicitud> listSatisfactorias = new ArrayList<Solicitud>();
		    List<Solicitud> listNoSatisfactorias = new ArrayList<Solicitud>();
		    ConsultaFilter filtro = new ConsultaFilter();
		    Oficio oficio = (Oficio) session.getAttribute("oficio");
		    ControlCode control = (ControlCode) session.getAttribute("control");
		    
		    filtro.setOficio(oficio.getNumOficio());
		    filtro.setEstatus(AppConstants.ESTATUS_ACEPTADO);
		    
		    listSatisfactorias = solicitudService.get(filtro);	
		    filtro.setEstatus(null);
		    filtro.setEstatusList(AppConstants.ESTATUS_RECHAZADO, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD);
		    listNoSatisfactorias = solicitudService.get(filtro);    
		    
		    RangoCaptura rango = null;
		    try {
				rango = this.gruposService.checkInRangeGenerarOficios();
				
			} catch (ApplicationException e) {
				e.printStackTrace();
			}
		    
		    SimpleDateFormat fmt = new SimpleDateFormat("dd 'de' MMMMMMMMMMMMMM 'del' yyyy");
		    CatalogoItem dependencia = catalogoService.getCatalogoItem(CatalogoConstants.DEPENDENCIAS, String.valueOf(oficio.getDependencia()));
			
		    params.put("REPORT_DATA_LIST_A", listNoSatisfactorias);
		    params.put("REPORT_DATA_LIST_B", listSatisfactorias);
		    params.put("NUM_OFICIO", String.valueOf(oficio.getNumOficio()));
		    params.put("FECHA_OFICIO", fmt.format(oficio.getFechaGeneracion()));
		    params.put("NOMBRE_DIRIGE_OFICIO", catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_PARAMETROS_NEGOCIO, "NOMDIROFIC"));
		    params.put("NUM_SOL_OK", String.valueOf(control.getTotalAceptadas()));
		    params.put("NUM_SOL_NO_OK", String.valueOf(control.getTotalRechazadas()));
		    params.put("FECHA_INICIAL_SOLICITUDES", fmt.format(rango.getFechaInicialCaptura()));
		    params.put("FECHA_FINAL_SOLICITUDES", fmt.format(Calendar.getInstance().getTime()));
		    params.put("CODIGO_CONTROL", control.getControlCode());
		    params.put("POLIZA", dependencia.getValorCampo2());
		    
		    jasperprint = JasperFillManager.fillReport(this.getJrOficio(),params, new JREmptyDataSource());
			// 6. Agregamos a lista de documentos
		    jprintlist.add(jasperprint);
				    
		    // 6. Create an output byte stream where data will be written
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			// 7. Export report
			// exporterService.export(AppConstants.EXTENSION_TYPE_PDF,
			// jprintlist, response, baos);
			
			StringBuilder fileName = new StringBuilder();
			fileName.append("Oficio");
			//fileName.append(s.getfmtFechaCaptura());
			fileName.append(".pdf");
			
			exporterService.export(fileName.toString(), documentType, jprintlist,
					response, baos);

			// 8. Write to reponse stream
			this.write(tokenService.generate(), response, baos);

		} catch (JRException jre) {
			throw new RuntimeException(jre);
		}

	}
	
	public void fillReportError(String documentType, HttpServletResponse response, Cliente cliente, Solicitud solicitud, HttpSession session) throws IOException{
		try {
			// 1. Add report parameters
			HashMap<String, Object> params = new HashMap<String, Object>();
			// params.put("Title", "User Report");
			// params.put("datasource", new JREmptyDataSource());

			JasperPrint jasperprint = null;
			//ClassPathResource classpathresource = null;
			//InputStream inputstream = null;
			//JasperDesign jasperdesign = null;
			//JasperReport jasperreport = null;

			List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
        	//classpathresource = new ClassPathResource("reportes/Reporte_Errores.jasper");
			//inputstream = classpathresource.getInputStream();
			// 3. Convert template to JasperDesign
			//jasperdesign = JRXmlLoader.load(inputstream);
			// 4. Compile design to JasperReport
			//JasperCompileManager.compileReportToFile(jasperdesign, "C:/Users/GCIT-HP-02/Desktop/Reporte_Errores.jasper");
		    //jasperreport = JasperCompileManager.compileReport(jasperdesign);
			//jasperreport = (JasperReport) JRLoader.loadObject(inputstream); 
			// 5. Create the JasperPrint object
	        // Make sure to pass the JasperReport and report parameters
			
		    List<ErrorBean> errorList = new ArrayList<ErrorBean>();
		    String cuentaSSI = (String)session.getAttribute("errorCuenta");
		    if (!StringUtils.isEmpty(cuentaSSI)) {
		        errorList.add(new ErrorBean("Cuenta SSI",String.valueOf(cliente.getCuenta()),MessageUtils.getMessage("reporte.error.cuenta")));
		    }
		    String porcentaje = (String)session.getAttribute("errorPorcentaje");
			if (!StringUtils.isEmpty(porcentaje)) {
				Object[] args = new Object[] { cliente.getMaxPorcentaje() };
				errorList.add(new ErrorBean("Porcentaje de Rescate",String.valueOf(solicitud.getPorcentaje()),MessageUtils.getMessage("reporte.error.porcentaje",args)));
			}
			String clabe = (String)session.getAttribute("errorClabe");
			if (!StringUtils.isEmpty(clabe)) {
			    errorList.add(new ErrorBean("Cuenta CLABE",solicitud.getClabe(),MessageUtils.getMessage("reporte.error.clabe")));
			}	
		    	
		    JRBeanCollectionDataSource bc = new JRBeanCollectionDataSource(errorList,false);
			
		    params.put("REPORT_DATA_LIST", errorList);
		    if(StringUtils.isEmpty(cliente.getRfc())){
		    	params.put("rfc", "-");
		    }else{
		    	params.put("rfc", cliente.getRfc());	
		    }
		    
		    params.put("cuenta", cliente.getCuenta());
		    
		    jasperprint = JasperFillManager.fillReport(this.getJrErrores(),params, bc);
			// 6. Agregamos a lista de documentos
		    jprintlist.add(jasperprint);
				    
		    // 6. Create an output byte stream where data will be written
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			// 7. Export report
			// exporterService.export(AppConstants.EXTENSION_TYPE_PDF,
			// jprintlist, response, baos);
			
			StringBuilder fileName = new StringBuilder();
			fileName.append("Reporte_Errores");
			//fileName.append(s.getfmtFechaCaptura());
			fileName.append(".pdf");
			
			exporterService.export(fileName.toString(), documentType, jprintlist,
					response, baos);

			// 8. Write to reponse stream
			this.write(tokenService.generate(), response, baos);

		} catch (JRException jre) {
			throw new RuntimeException(jre);
		}

	}
	
	public void fillReport(String documentType, HttpServletResponse response, Solicitud s) throws IOException{
		try {
			// 1. Add report parameters
			HashMap<String, Object> params = new HashMap<String, Object>();
			// params.put("Title", "User Report");
			// params.put("datasource", new JREmptyDataSource());

			JasperPrint jasperprint = null;
			ClassPathResource classpathresource = null;
			InputStream inputstream = null;
			JasperDesign jasperdesign = null;
			JasperReport jasperreport = null;

			List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
            switch (s.getTipoTramite()) {
			case AppConstants.PDF_RESCATE_PARCIAL:
					// 2. Retrieve template
					// InputStream reportStream =
					// this.getClass().getResourceAsStream(TEMPLATE_RESCATE_PARCIAL);
					//classpathresource = new ClassPathResource(
					//		"Sol_Resc_Parcial.jrxml");
					//Generamos la hoja numero 1
					//classpathresource = new ClassPathResource("reportes/Sol_Resc_Parcial.jasper");
					//inputstream = classpathresource.getInputStream();
					// 3. Convert template to JasperDesign
					//jasperdesign = JRXmlLoader.load(inputstream);
					// 4. Compile design to JasperReport
					//JasperCompileManager.compileReportToFile(jasperdesign, "C:/Users/GCIT-HP-02/Desktop/Sol_Resc_Parcial.jasper");
					//jasperreport = JasperCompileManager.compileReport(jasperdesign);
					//jasperreport = (JasperReport) JRLoader.loadObject(inputstream); 
					// 5. Create the JasperPrint object
	  			    // Make sure to pass the JasperReport and report parameters
					params = this.fillPdfRescateParcial(s, params, 1);
					jasperprint = JasperFillManager.fillReport(this.getJrSolicitudRescateParcialH1(),params, new JREmptyDataSource());
					// 6. Agregamos a lista de documentos
				    jprintlist.add(jasperprint);
				    
				    // 2. Retrieve template2
					// InputStream reportStream =
					// this.getClass().getResourceAsStream(TEMPLATE_RESCATE_PARCIAL);
					//classpathresource = new ClassPathResource(
					//		"Sol_Resc_Parcial.jrxml");
					//Generamos la hoja numero 1
					//classpathresource = new ClassPathResource("reportes/Sol_Resc_Parcial_hoja_2.jasper");
					//inputstream = classpathresource.getInputStream();
					// 3. Convert template to JasperDesign
					//jasperdesign = JRXmlLoader.load(inputstream);
					// 4. Compile design to JasperReport
					//JasperCompileManager.compileReportToFile(jasperdesign, "C:/Users/GCIT-HP-02/Desktop/Sol_Resc_Parcial_hoja_2.jasper");
					//jasperreport = JasperCompileManager.compileReport(jasperdesign);
					//jasperreport = (JasperReport)JRLoader.loadObject(inputstream); 
					// 5. Create the JasperPrint object
					// Make sure to pass the JasperReport and report parameters
					params = this.fillPdfRescateParcial(s, params, 2);
					jasperprint = JasperFillManager.fillReport(this.getJrSolicitudRescateParcialH2(), params, new JREmptyDataSource());
					// 6. Agregamos a lista de documentos
				    jprintlist.add(jasperprint);
				
				    
				    
		    break;
			case AppConstants.PDF_PAGO_TOTAL:
				// 2. Retrieve template
				// InputStream reportStream =
				// this.getClass().getResourceAsStream(TEMPLATE_RESCATE_PARCIAL);
				classpathresource = new ClassPathResource(
						"reportes/Sol_Resc_Total.jasper");
				inputstream = classpathresource.getInputStream();
				// 3. Convert template to JasperDesign
				//jasperdesign = JRXmlLoader.load(inputstream);
				// 4. Compile design to JasperReport
				//JasperCompileManager.compileReportToFile(jasperdesign, "C:/Users/GCIT-HP-02/Desktop/Sol_Resc_Total.jasper");
				//jasperreport = JasperCompileManager.compileReport(jasperdesign);
				jasperreport = (JasperReport)JRLoader.loadObject(inputstream);
				// 5. Create the JasperPrint object
				// Make sure to pass the JasperReport and report parameters
				params = this.fillPdfRescateTotal(s, params, 1);
				jasperprint = JasperFillManager.fillReport(jasperreport,
						params, new JREmptyDataSource());
				// 6. Agregamos a lista de documentos
				jprintlist.add(jasperprint);
				
				// 2. Retrieve template
				// InputStream reportStream =
				// this.getClass().getResourceAsStream(TEMPLATE_RESCATE_PARCIAL);
				classpathresource = new ClassPathResource(
						"reportes/Sol_Resc_Total_hoja_2.jasper");
				inputstream = classpathresource.getInputStream();
				// 3. Convert template to JasperDesign
				//jasperdesign = JRXmlLoader.load(inputstream);
				// 4. Compile design to JasperReport
				//JasperCompileManager.compileReportToFile(jasperdesign, "C:/Users/GCIT-HP-02/Desktop/Sol_Resc_Total_hoja_2.jasper");
				//jasperreport = JasperCompileManager.compileReport(jasperdesign);
				jasperreport = (JasperReport)JRLoader.loadObject(inputstream);
				// 5. Create the JasperPrint object
				// Make sure to pass the JasperReport and report parameters
				params = this.fillPdfRescateTotal(s, params, 2);
				jasperprint = JasperFillManager.fillReport(jasperreport,
						params, new JREmptyDataSource());
				// 6. Agregamos a lista de documentos
				jprintlist.add(jasperprint);
				break;
			default:
				break;
			}

			// Generacion de PDF para dependiente
			Iterator<DependienteEconomico> it = s.getDependientes().iterator();
			
			while (it.hasNext()) {
				DependienteEconomico dependiente = it.next();
				//classpathresource = new ClassPathResource("reportes/Sol_Datos_Articulo_140.jasper");
				//inputstream = classpathresource.getInputStream();
				// 3. Convert template to JasperDesign
				//jasperdesign = JRXmlLoader.load(inputstream);
				// 4. Compile design to JasperReport
				//JasperCompileManager.compileReportToFile(jasperdesign, "C:/Users/GCIT-HP-02/Desktop/Sol_Datos_Articulo_140.jasper");
				//jasperreport = JasperCompileManager.compileReport(jasperdesign);
				//jasperreport = (JasperReport)JRLoader.loadObject(inputstream);
				// 5. Create the JasperPrint object
				// Make sure to pass the JasperReport and report parameters
				params = this.fillPdfDependiente(dependiente, params, 1);
				jasperprint = JasperFillManager.fillReport(this.getJrSolicitudRescateParcialDepH1(),	params, new JREmptyDataSource());
				// 6. Agregamos a lista de documentos
				jprintlist.add(jasperprint);
			    //Hoja 2
				//classpathresource = new ClassPathResource("reportes/Sol_Datos_Articulo_140_hoja_2.jasper");
				//inputstream = classpathresource.getInputStream();
				// 3. Convert template to JasperDesign
				//jasperdesign = JRXmlLoader.load(inputstream);
				// 4. Compile design to JasperReport
				//JasperCompileManager.compileReportToFile(jasperdesign, "C:/Users/GCIT-HP-02/Desktop/Sol_Datos_Articulo_140_hoja_2.jasper");
				//jasperreport = JasperCompileManager.compileReport(jasperdesign);
				//jasperreport = (JasperReport)JRLoader.loadObject(inputstream);
				// 5. Create the JasperPrint object
				// Make sure to pass the JasperReport and report parameters
				params = this.fillPdfDependiente(dependiente, params, 2);
				jasperprint = JasperFillManager.fillReport(this.getJrSolicitudRescateParcialDepH2(), params, new JREmptyDataSource());
				// 6. Agregamos a lista de documentos
				jprintlist.add(jasperprint); 
			    
			}// while dependiente

			// 6. Create an output byte stream where data will be written
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			// 7. Export report
			// exporterService.export(AppConstants.EXTENSION_TYPE_PDF,
			// jprintlist, response, baos);
			
			StringBuilder fileName = new StringBuilder();
			fileName.append("Rescate_Parcial_");
			fileName.append(s.getUserFolio());
			fileName.append(".pdf");
			
			exporterService.export(fileName.toString(), documentType, jprintlist,
					response, baos);

			// 8. Write to reponse stream
			this.write(tokenService.generate(), response, baos);

		} catch (JRException jre) {
			throw new RuntimeException(jre);
		}

	}
	
	
	private HashMap<String, Object> fillPdfRescateParcial(Solicitud s,
			HashMap<String, Object> parameterMap, int page) {
		String value = "";
		Object obj = null;
		
		switch(page) {
		
		case 1:
				obj = s.getfmtFechaCaptura();
				if (obj==null) { value = getEmptyStringContainer(6);} else { value = obj.toString();}
				if (value.equals("")) { value = getEmptyStringContainer(6); }
				parameterMap.put("DIA1_SOL", String.valueOf(value.charAt(0)));
				parameterMap.put("DIA2_SOL", String.valueOf(value.charAt(1)));
				parameterMap.put("MES1_SOL", String.valueOf(value.charAt(2)));
				parameterMap.put("MES2_SOL", String.valueOf(value.charAt(3)));
				parameterMap.put("AA1_SOL", String.valueOf(value.charAt(4)));
				parameterMap.put("AA2_SOL", String.valueOf(value.charAt(5)));
		
				parameterMap.put("LUGAR_ELABORA", s.getCiudadElaboracion().toUpperCase() + ", " + catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_ESTADOS,
								s.getEstadoElaboracion()));
				parameterMap.put("APATERNO", s.getApePaterno().toUpperCase());
				parameterMap.put("AMATERNO", s.getApeMaterno().toUpperCase());
				parameterMap.put("NOMBRE", s.getNombre().toUpperCase());
				parameterMap.put("NUM_CUENTA", String.valueOf(s.getCuenta()));
		
				obj = s.getRfc();
				if (obj==null) { value = getEmptyStringContainer(10);} else { value = obj.toString();}
				if (value.equals("")) { value = getEmptyStringContainer(10); }
				parameterMap.put("LETR1_RFC", String.valueOf(value.charAt(0)));
				parameterMap.put("LETR2_RFC", String.valueOf(value.charAt(1)));
				parameterMap.put("LETR3_RFC", String.valueOf(value.charAt(2)));
				parameterMap.put("LETR4_RFC", String.valueOf(value.charAt(3)));
				parameterMap.put("AA1_RFC", String.valueOf(value.charAt(4)));
				parameterMap.put("AA2_RFC", String.valueOf(value.charAt(5)));
				parameterMap.put("MES1_RFC", String.valueOf(value.charAt(6)));
				parameterMap.put("MES2_RFC", String.valueOf(value.charAt(7)));
				parameterMap.put("DIA1_RFC", String.valueOf(value.charAt(8)));
				parameterMap.put("DIA2_RFC", String.valueOf(value.charAt(9)));
		
				obj = s.getHomoclaveRfc();
				if (obj==null) { value = getEmptyStringContainer(3);} else { value = obj.toString().toUpperCase();}
				if (value.equals("")) { value = getEmptyStringContainer(3); }
				parameterMap.put("CHR1_HOMCVE", String.valueOf(value.charAt(0)));
				parameterMap.put("CHR2_HOMCVE", String.valueOf(value.charAt(1)));
				parameterMap.put("CHR3_HOMCVE", String.valueOf(value.charAt(2)));
		
				obj = s.getCurp();
				if (obj==null) { value = getEmptyStringContainer(18);} else { value = obj.toString().toUpperCase();}
				if (value.equals("")) { value = getEmptyStringContainer(18); }
				parameterMap.put("CHR1_CURP", String.valueOf(value.charAt(0)));
				parameterMap.put("CHR2_CURP", String.valueOf(value.charAt(1)));
				parameterMap.put("CHR3_CURP", String.valueOf(value.charAt(2)));
				parameterMap.put("CHR4_CURP", String.valueOf(value.charAt(3)));
				parameterMap.put("CHR5_CURP", String.valueOf(value.charAt(4)));
				parameterMap.put("CHR6_CURP", String.valueOf(value.charAt(5)));
				parameterMap.put("CHR7_CURP", String.valueOf(value.charAt(6)));
				parameterMap.put("CHR8_CURP", String.valueOf(value.charAt(7)));
				parameterMap.put("CHR9_CURP", String.valueOf(value.charAt(8)));
				parameterMap.put("CHR10_CURP", String.valueOf(value.charAt(9)));
				parameterMap.put("CHR11_CURP", String.valueOf(value.charAt(10)));
				parameterMap.put("CHR12_CURP", String.valueOf(value.charAt(11)));
				parameterMap.put("CHR13_CURP", String.valueOf(value.charAt(12)));
				parameterMap.put("CHR14_CURP", String.valueOf(value.charAt(13)));
				parameterMap.put("CHR15_CURP", String.valueOf(value.charAt(14)));
				parameterMap.put("CHR16_CURP", String.valueOf(value.charAt(15)));
				parameterMap.put("CHR17_CURP", String.valueOf(value.charAt(16)));
				parameterMap.put("CHR18_CURP", String.valueOf(value.charAt(17)));
		
				parameterMap.put("CIUDAD_NAC", s.getCiudadNacimiento().toUpperCase());
				
				if(s.getIdPaisNac() == AppConstants.PAIS_MEXICO){
					parameterMap.put("ESTADO_NAC", catalogoService
							.getCatalogoItemDescription(CatalogoConstants.CAT_ESTADOS,
									s.getIdEdoNac()));
				}else{
					parameterMap.put("ESTADO_NAC", s.getEstadoNacExtranjero());
					
				}
				
				parameterMap.put("PAIS_NAC", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_PAISES,
								s.getIdPaisNac()).toUpperCase());
				parameterMap.put("NACIONALIDAD", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_PAISES,
								s.getIdNacionalidad()).toUpperCase());
				parameterMap.put("CHECK_MASC",
						(s.getCveGenero() == CatalogoConstants.GENERO_MASCULINO) ? "X"
								: "");
				parameterMap.put("CHECK_FEM",
						(s.getCveGenero() == CatalogoConstants.GENERO_FEMENINO) ? "X"
								: "");
				parameterMap
						.put("CHECK_SOLTERO",
								(s.getCveEdoCivil() == CatalogoConstants.EDOCIVIL_SOLTERO) ? "X"
										: "");
				parameterMap.put("CHECK_CASADO",
						(s.getCveEdoCivil() == CatalogoConstants.EDOCIVIL_CASADO) ? "X"
								: "");
		
				obj = s.getAnioNacimiento().substring(2);
				if (obj==null) { value = getEmptyStringContainer(2);} else { value = obj.toString();}
				value = s.getDiaNacimiento() + s.getMesNacimiento() + value ;
				if (value.equals("")) { value = getEmptyStringContainer(6); }
				parameterMap.put("DIA1_NAC", String.valueOf(value.charAt(0)));
				parameterMap.put("DIA2_NAC", String.valueOf(value.charAt(1)));
				parameterMap.put("MES1_NAC", String.valueOf(value.charAt(2)));
				parameterMap.put("MES2_NAC", String.valueOf(value.charAt(3)));
				parameterMap.put("AA1_NAC", String.valueOf(value.charAt(4)));
				parameterMap.put("AA2_NAC", String.valueOf(value.charAt(5)));
		
				parameterMap
						.put("CHECK_IFE",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_IFE) ? "X"
										: "");
				parameterMap
						.put("CHECK_PASAPORTE",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_PASAPORTE) ? "X"
										: "");
				parameterMap
						.put("CHECK_ADULTOS",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_ADULTOS) ? "X"
										: "");
				parameterMap
						.put("CHECK_CED_PROF",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_CED_PROF) ? "X"
										: "");
				parameterMap
						.put("CHECK_MATR_CONS",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_MATR_CONS) ? "X"
										: "");
				parameterMap
						.put("CHECK_MIGRAT",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_FM2 || s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_FM3) ? "X"
										: "");
				parameterMap
						.put("CHECK_SI_CONTR",
								(s.isContribuyenteEUA()) ? "X"
										: "");
				parameterMap
						.put("CHECK_NO_CONTR",
								(!s.isContribuyenteEUA()) ? "X"
										: "");
		
				obj = s.getNumSsn();
				if (obj==null) { value = getEmptyStringContainer(11);} else { value = obj.toString().toUpperCase();}
				if (value.equals("")) { value = getEmptyStringContainer(11); }
			    parameterMap.put("NSS1", String.valueOf(value.charAt(0)));
				parameterMap.put("NSS2", String.valueOf(value.charAt(1)));
				parameterMap.put("NSS3", String.valueOf(value.charAt(2)));
				parameterMap.put("NSS4", String.valueOf(value.charAt(3)));
				parameterMap.put("NSS5", String.valueOf(value.charAt(4)));
				parameterMap.put("NSS6", String.valueOf(value.charAt(5)));
				parameterMap.put("NSS7", String.valueOf(value.charAt(6)));
				parameterMap.put("NSS8", String.valueOf(value.charAt(7)));
				parameterMap.put("NSS9", String.valueOf(value.charAt(8)));
				parameterMap.put("NSS10", String.valueOf(value.charAt(9)));
				parameterMap.put("NSS11", String.valueOf(value.charAt(10)));
				
				parameterMap.put("NUM_IDENTIFICA", s.getNumeroIdentificacion());
				parameterMap.put("PROFESION", s.getProfesion());
				parameterMap.put("CENTRO_TRABAJO", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_DEPENDENCIAS,	s.getCentroTrabajo()));
				parameterMap.put("CHECK_SI_POL_EXPUESTO",
						(s.isPoliticamenteExpuesta()) ? "X" : "");
				parameterMap.put("CHECK_NO_POL_EXPUESTO",
						(s.isPoliticamenteExpuesta()) ? "" : "X");
				parameterMap.put("CALLE_PART", s.getCalle());
				parameterMap.put("NUM_EXT_PART", s.getNumExt());
				parameterMap.put("NUM_INT_PART", s.getNumInt());
				parameterMap.put("COLONIA_PART", s.getColonia());
				parameterMap.put("CIUDAD_PART", s.getCiudad());
				
				if(s.getIdPais() == AppConstants.PAIS_MEXICO){
					parameterMap.put("MUNICIPIO_PART", catalogoService
							.getCatalogoItemDescription(CatalogoConstants.CAT_MUNICIPIOS,
									s.getIdMunicipio()));
					parameterMap.put("ESTADO_PART", catalogoService
							.getCatalogoItemDescription(CatalogoConstants.CAT_ESTADOS,
									s.getIdEstado()));	
				}else{
					parameterMap.put("MUNICIPIO_PART", s.getMunicipioExtranjero());
					parameterMap.put("ESTADO_PART", s.getEstadoExtranjero());
				}
				
				
				parameterMap.put("PAIS_PART", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_PAISES,
								s.getIdPais()).toUpperCase());
				parameterMap.put("CP_PART", s.getCodigoPostal());
				parameterMap.put("TEL_DOM", s.getTelefonoCasa());
				parameterMap.put("TEL_OFIC", s.getTelefonoTrabajo());
				parameterMap.put("TEL_CEL", s.getTelefonoCelular());
				parameterMap.put("EMAIL_PERSONAL", s.getCorreoPersonal());
				parameterMap.put("EMAIL_LABORAL", s.getCorreoTrabajo());
		
				// parameterMap.put("CODIGO_BARRAS_FECHA",s.getFechaCaptura().toString());
				obj = s.getfmtFechaCaptura();
				if (obj==null) { value = getEmptyStringContainer(9);} else { value = obj.toString();}	
				parameterMap.put("CODIGO_BARRAS_FECHA", value);
				parameterMap.put("CODIGO_BARRAS_RFC", s.getRfc());
				
				obj = s.getClabe();
				if (obj==null || (String)obj=="") {value = getEmptyStringContainer(18,"0");} else {value = obj.toString();}	
				if (value.length() < 18) { value = getEmptyStringContainer(18,"0"); }
				parameterMap.put("CODIGO_BARRAS_CLABE", value);
		
				parameterMap.put("NUM_FOLIO_SOL",String.valueOf(s.getUserFolio()));
				
				parameterMap.put("NUM_FORMA", catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_PARAMETROS_NEGOCIO,"PNUMFORMA1"));

			break;
			
		case 2:
			    obj = s.getPorcentaje();
			    if (obj==null) { value = getEmptyStringContainer(1);} else { value = obj.toString();}
			    parameterMap.put("PORCENTAJE_RETIRO", value);
			    
			    obj = s.getClabe();
			    if (obj==null) { value = getEmptyStringContainer(18);} else { value = obj.toString();}
				parameterMap.put("CLABE", value.toUpperCase());
			
				parameterMap.put("CHECK_CLABE", (AppConstants.MODO_PAGO_TRANSFERENCIA == s.getModoPago()) ? "X" : " ");
				parameterMap.put("CHECK_CHEQUE", (AppConstants.MODO_PAGO_OTRA_FORMA == s.getModoPago()) ? "X" : " ");
				
				obj = s.getClavePromotoria();
				if (obj==null || (Short)obj<1) { value = getEmptyStringContainer(1);} 
				else { value = catalogoService.getCatalogoItemDescription(CatalogoConstants.OFICINAS_CHEQUE,(Short)obj);}
			    
				if(value.length() > 57){
				   String valueAux = value;
				   value = value.substring(0, 57);
				   int lastBlankIndex = value.lastIndexOf(" ")+1;
				   value = value.substring(0, 57-(57-lastBlankIndex));
				   valueAux = valueAux.substring(57-(57-lastBlankIndex),valueAux.length());
				   parameterMap.put("PROMOT_RENG1", value);
				   parameterMap.put("PROMOT_RENG2", valueAux);
				} else {
				   parameterMap.put("PROMOT_RENG1", value);
				   parameterMap.put("PROMOT_RENG2", "");	
				}

			    
				parameterMap.put("AUTORIZA_FORMA_1", catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_PARAMETROS_NEGOCIO,"PAUTFORMA1"));
			    parameterMap.put("AUTORIZA_FORMA_2", catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_PARAMETROS_NEGOCIO,"PAUTFORMA2"));
				
			break;
		
		default:
			break;
		}//switch
				
	    return parameterMap;
	}

	private HashMap<String, Object> fillPdfRescateTotal(Solicitud s,
			HashMap<String, Object> parameterMap, int page) {
	        String value = "";
	        Object obj = null;
		
		switch(page) {
	        
		case 1:
			    obj = s.getfmtFechaCaptura();
				if (obj==null) { value = getEmptyStringContainer(6);} else { value = obj.toString();}
				if (value.equals("")) { value = getEmptyStringContainer(6); }
				parameterMap.put("DIA1_SOL", String.valueOf(value.charAt(0)));
				parameterMap.put("DIA2_SOL", String.valueOf(value.charAt(1)));
				parameterMap.put("MES1_SOL", String.valueOf(value.charAt(2)));
				parameterMap.put("MES2_SOL", String.valueOf(value.charAt(3)));
				parameterMap.put("AA1_SOL", String.valueOf(value.charAt(4)));
				parameterMap.put("AA2_SOL", String.valueOf(value.charAt(5)));
		
				parameterMap.put("LUGAR_ELABORA", s.getCiudadElaboracion() + ", " + catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_ESTADOS,
								s.getEstadoElaboracion()).toUpperCase());
				parameterMap.put("APATERNO", s.getApePaterno().toUpperCase());
				parameterMap.put("AMATERNO", s.getApeMaterno().toUpperCase());
				parameterMap.put("NOMBRE", s.getNombre().toUpperCase());
				parameterMap.put("NUM_CUENTA", String.valueOf(s.getCuenta()));
		
				obj = s.getRfc();
				if (obj==null) { value = getEmptyStringContainer(10);} else { value = obj.toString().toUpperCase();}
				if (value.equals("")) { value = getEmptyStringContainer(10); }
				parameterMap.put("LETR1_RFC", String.valueOf(value.charAt(0)));
				parameterMap.put("LETR2_RFC", String.valueOf(value.charAt(1)));
				parameterMap.put("LETR3_RFC", String.valueOf(value.charAt(2)));
				parameterMap.put("LETR4_RFC", String.valueOf(value.charAt(3)));
				parameterMap.put("DIA1_RFC", String.valueOf(value.charAt(4)));
				parameterMap.put("DIA2_RFC", String.valueOf(value.charAt(5)));
				parameterMap.put("MES1_RFC", String.valueOf(value.charAt(6)));
				parameterMap.put("MES2_RFC", String.valueOf(value.charAt(7)));
				parameterMap.put("AA1_RFC", String.valueOf(value.charAt(8)));
				parameterMap.put("AA2_RFC", String.valueOf(value.charAt(9)));
		
				obj = s.getHomoclaveRfc();
				if (obj==null) { value = getEmptyStringContainer(3);} else { value = obj.toString().toUpperCase();}
				if (value.equals("")) { value = getEmptyStringContainer(3); }
				parameterMap.put("CHR1_HOMCVE", String.valueOf(value.charAt(0)));
				parameterMap.put("CHR2_HOMCVE", String.valueOf(value.charAt(1)));
				parameterMap.put("CHR3_HOMCVE", String.valueOf(value.charAt(2)));
		
				obj = s.getCurp();
				if (obj==null) { value = getEmptyStringContainer(18);} else { value = obj.toString().toUpperCase();}
				if (value.equals("")) { value = getEmptyStringContainer(18); }
				parameterMap.put("CHR1_CURP", String.valueOf(value.charAt(0)));
				parameterMap.put("CHR2_CURP", String.valueOf(value.charAt(1)));
				parameterMap.put("CHR3_CURP", String.valueOf(value.charAt(2)));
				parameterMap.put("CHR4_CURP", String.valueOf(value.charAt(3)));
				parameterMap.put("CHR5_CURP", String.valueOf(value.charAt(4)));
				parameterMap.put("CHR6_CURP", String.valueOf(value.charAt(5)));
				parameterMap.put("CHR7_CURP", String.valueOf(value.charAt(6)));
				parameterMap.put("CHR8_CURP", String.valueOf(value.charAt(7)));
				parameterMap.put("CHR9_CURP", String.valueOf(value.charAt(8)));
				parameterMap.put("CHR10_CURP", String.valueOf(value.charAt(9)));
				parameterMap.put("CHR11_CURP", String.valueOf(value.charAt(10)));
				parameterMap.put("CHR12_CURP", String.valueOf(value.charAt(11)));
				parameterMap.put("CHR13_CURP", String.valueOf(value.charAt(12)));
				parameterMap.put("CHR14_CURP", String.valueOf(value.charAt(13)));
				parameterMap.put("CHR15_CURP", String.valueOf(value.charAt(14)));
				parameterMap.put("CHR16_CURP", String.valueOf(value.charAt(15)));
				parameterMap.put("CHR17_CURP", String.valueOf(value.charAt(16)));
				parameterMap.put("CHR18_CURP", String.valueOf(value.charAt(17)));
		
				parameterMap.put("CIUDAD_NAC", s.getCiudadNacimiento());
				parameterMap.put("ESTADO_NAC", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_ESTADOS,
								s.getIdEdoNac()));
				parameterMap.put("PAIS_NAC", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_PAISES,
								s.getIdPaisNac()));
				parameterMap.put("NACIONALIDAD", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_PAISES,
								s.getIdNacionalidad()));
				parameterMap.put("CHECK_MASC",
						(s.getCveGenero() == CatalogoConstants.GENERO_MASCULINO) ? "X"
								: "");
				parameterMap.put("CHECK_FEM",
						(s.getCveGenero() == CatalogoConstants.GENERO_FEMENINO) ? "X"
								: "");
				parameterMap
						.put("CHECK_SOLTERO",
								(s.getCveEdoCivil() == CatalogoConstants.EDOCIVIL_SOLTERO) ? "X"
										: "");
				parameterMap.put("CHECK_CASADO",
						(s.getCveEdoCivil() == CatalogoConstants.EDOCIVIL_CASADO) ? "X"
								: "");
		
				obj = s.getAnioNacimiento().substring(2);
				if (obj==null) { value = getEmptyStringContainer(2);} else { value = obj.toString();}
				value = s.getDiaNacimiento() + s.getMesNacimiento() + value ;
				if (value.equals("")) { value = getEmptyStringContainer(6); }
				parameterMap.put("DIA1_NAC", String.valueOf(value.charAt(0)));
				parameterMap.put("DIA2_NAC", String.valueOf(value.charAt(1)));
				parameterMap.put("MES1_NAC", String.valueOf(value.charAt(2)));
				parameterMap.put("MES2_NAC", String.valueOf(value.charAt(3)));
				parameterMap.put("AA1_NAC", String.valueOf(value.charAt(4)));
				parameterMap.put("AA2_NAC", String.valueOf(value.charAt(5)));
		
				parameterMap
						.put("CHECK_IFE",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_IFE) ? "X"
										: "");
				parameterMap
						.put("CHECK_PASAPORTE",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_PASAPORTE) ? "X"
										: "");
				parameterMap
						.put("CHECK_ADULTOS",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_ADULTOS) ? "X"
										: "");
				parameterMap
						.put("CHECK_CED_PROF",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_CED_PROF) ? "X"
										: "");
				parameterMap
						.put("CHECK_MATR_CONS",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_MATR_CONS) ? "X"
										: "");
				parameterMap
						.put("CHECK_MIGRAT",
								(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_FM2 || s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_FM3) ? "X"
										: "");
				parameterMap
						.put("CHECK_SI_CONTR",
								(s.isContribuyenteEUA()) ? "X"
										: "");
				parameterMap
						.put("CHECK_NO_CONTR",
								(!s.isContribuyenteEUA()) ? "X"
										: "");
		
				obj = s.getNumSsn();
				if (obj==null) { value = getEmptyStringContainer(9);} else { value = obj.toString().toUpperCase();}
				if (value.equals("")) { value = getEmptyStringContainer(9); }
				parameterMap.put("NSS1", String.valueOf(value.charAt(0)));
				parameterMap.put("NSS2", String.valueOf(value.charAt(1)));
				parameterMap.put("NSS3", String.valueOf(value.charAt(2)));
				parameterMap.put("NSS4", String.valueOf(value.charAt(3)));
				parameterMap.put("NSS5", String.valueOf(value.charAt(4)));
				parameterMap.put("NSS6", String.valueOf(value.charAt(5)));
				parameterMap.put("NSS7", String.valueOf(value.charAt(6)));
				parameterMap.put("NSS8", String.valueOf(value.charAt(7)));
				parameterMap.put("NSS9", String.valueOf(value.charAt(8)));
		
				parameterMap.put("NUM_IDENTIFICA", s.getNumeroIdentificacion().toUpperCase());
				parameterMap.put("PROFESION", s.getProfesion().toUpperCase());
				parameterMap.put("CENTRO_TRABAJO", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_CENTROS_SERVICIO,
								s.getCentroTrabajo()).toUpperCase());
				parameterMap.put("CHECK_SI_POL_EXPUESTO",
						(s.isPoliticamenteExpuesta()) ? "X" : "");
				parameterMap.put("CHECK_NO_POL_EXPUESTO",
						(s.isPoliticamenteExpuesta()) ? "" : "X");
				parameterMap.put("CALLE_PART", s.getCalle().toUpperCase());
				parameterMap.put("NUM_EXT_PART", s.getNumExt().toUpperCase());
				parameterMap.put("NUM_INT_PART", s.getNumInt().toUpperCase());
				parameterMap.put("COLONIA_PART", s.getColonia().toUpperCase());
				parameterMap.put("CIUDAD_PART", s.getCiudad().toUpperCase());
				parameterMap.put("MUNICIPIO_PART", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_MUNICIPIOS,
								s.getIdMunicipio()));
				parameterMap.put("ESTADO_PART", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_ESTADOS,
								s.getIdEstado()));
				parameterMap.put("PAIS_PART", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_PAISES,
								s.getIdPais()));
				parameterMap.put("CP_PART", s.getCodigoPostal());
				parameterMap.put("TEL_DOM", s.getTelefonoCasa());
				parameterMap.put("TEL_OFIC", s.getTelefonoTrabajo());
				parameterMap.put("TEL_CEL", s.getTelefonoCelular());
				parameterMap.put("EMAIL_PERSONAL", s.getCorreoPersonal());
				parameterMap.put("EMAIL_LABORAL", s.getCorreoTrabajo());
		
				// parameterMap.put("CODIGO_BARRAS_FECHA",s.getFechaCaptura().toString());
				obj = s.getfmtFechaCaptura();
				if (obj==null) { value = getEmptyStringContainer(6);} else { value = obj.toString();}
				parameterMap.put("CODIGO_BARRAS_FECHA", value);
				parameterMap.put("CODIGO_BARRAS_RFC", s.getRfc());
				parameterMap.put("CODIGO_BARRAS_CLABE", s.getClabe());
		
				// parameterMap.put("NUM_FOLIO_SOL",s.getNumFolio().toString());
				parameterMap.put("NUM_FOLIO_SOL", String.valueOf(s.getUserFolio()));
		
				// Parametros adicionales
				parameterMap.put("CHECH_RETIRO_TOTAL", "X");
				parameterMap.put("CHECK_APORT_VOLUNT", "");
				parameterMap.put("PORCENTAJE_RETIRO", s.getPorcentaje().toString());
				
			break;
		case 2:
			
			    parameterMap.put("CLABE", s.getClabe().toUpperCase());
			    
			break;
			
		default:
			break;
		}
			
		return parameterMap;
	}

	private HashMap<String, Object> fillPdfDependiente(
			DependienteEconomico dependiente,
			HashMap<String, Object> parameterMap, int page) {
		    String value = "";
		    Object obj = null;
		
		
		switch(page) {
				
		case 1:
		        parameterMap.put("APATERNO", dependiente.getApeMaterno().toUpperCase());
				parameterMap.put("AMATERNO", dependiente.getApePaterno().toUpperCase());
				parameterMap.put("NOMBRE", dependiente.getNombre().toUpperCase());
		
				obj = dependiente.getRfc();
				if (obj==null) { value = getEmptyStringContainer(10);} else { value = obj.toString().toUpperCase();}
				if (value.equals("")) { value = getEmptyStringContainer(10); }
				parameterMap.put("LETR1_RFC", String.valueOf(value.charAt(0)));
				parameterMap.put("LETR2_RFC", String.valueOf(value.charAt(1)));
				parameterMap.put("LETR3_RFC", String.valueOf(value.charAt(2)));
				parameterMap.put("LETR4_RFC", String.valueOf(value.charAt(3)));
				parameterMap.put("AA1_RFC", String.valueOf(value.charAt(4)));
				parameterMap.put("AA2_RFC", String.valueOf(value.charAt(5)));
				parameterMap.put("MES1_RFC", String.valueOf(value.charAt(6)));
				parameterMap.put("MES2_RFC", String.valueOf(value.charAt(7)));
				parameterMap.put("DIA1_RFC", String.valueOf(value.charAt(8)));
				parameterMap.put("DIA2_RFC", String.valueOf(value.charAt(9)));
				
		
				obj = dependiente.getHomoclaveRfc();
				if (obj==null) { value = getEmptyStringContainer(3);} else { value = obj.toString().toUpperCase();}
				if (value.equals("")) { value = getEmptyStringContainer(3); }
				parameterMap.put("CHR1_HOMCVE", String.valueOf(value.charAt(0)));
				parameterMap.put("CHR2_HOMCVE", String.valueOf(value.charAt(1)));
				parameterMap.put("CHR3_HOMCVE", String.valueOf(value.charAt(2)));
		
				obj = dependiente.getCurp();
				if (obj==null) { value = getEmptyStringContainer(18);} else { value = obj.toString().toUpperCase();}
				if (value.equals("")) { value = getEmptyStringContainer(18); }
				parameterMap.put("CHR1_CURP", String.valueOf(value.charAt(0)));
				parameterMap.put("CHR2_CURP", String.valueOf(value.charAt(1)));
				parameterMap.put("CHR3_CURP", String.valueOf(value.charAt(2)));
				parameterMap.put("CHR4_CURP", String.valueOf(value.charAt(3)));
				parameterMap.put("CHR5_CURP", String.valueOf(value.charAt(4)));
				parameterMap.put("CHR6_CURP", String.valueOf(value.charAt(5)));
				parameterMap.put("CHR7_CURP", String.valueOf(value.charAt(6)));
				parameterMap.put("CHR8_CURP", String.valueOf(value.charAt(7)));
				parameterMap.put("CHR9_CURP", String.valueOf(value.charAt(8)));
				parameterMap.put("CHR10_CURP", String.valueOf(value.charAt(9)));
				parameterMap.put("CHR11_CURP", String.valueOf(value.charAt(10)));
				parameterMap.put("CHR12_CURP", String.valueOf(value.charAt(11)));
				parameterMap.put("CHR13_CURP", String.valueOf(value.charAt(12)));
				parameterMap.put("CHR14_CURP", String.valueOf(value.charAt(13)));
				parameterMap.put("CHR15_CURP", String.valueOf(value.charAt(14)));
				parameterMap.put("CHR16_CURP", String.valueOf(value.charAt(15)));
				parameterMap.put("CHR17_CURP", String.valueOf(value.charAt(16)));
				parameterMap.put("CHR18_CURP", String.valueOf(value.charAt(17)));
		
				parameterMap.put("CIUDAD_NAC", dependiente.getCiudadNacimiento());
				
				if(dependiente.getIdPaisNac() == AppConstants.PAIS_MEXICO){
					parameterMap.put("ESTADO_NAC", catalogoService
							.getCatalogoItemDescription(CatalogoConstants.CAT_ESTADOS,
									dependiente.getIdEstado()));	
				}else{
					parameterMap.put("ESTADO_NAC", dependiente.getEstadoNacExtranjero());
				}
				
				
				parameterMap.put("PAIS_NAC", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_PAISES,
								dependiente.getIdPaisNac()));
				parameterMap.put("NACIONALIDAD", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_PAISES,
								dependiente.getIdNacionalidad()));
		
				// value.= de.getFechaNacimiento().toString();
				obj = dependiente.getAnioNacimiento().substring(2);
				if (obj==null) { value = getEmptyStringContainer(2);} else { value = obj.toString();}
				value = dependiente.getDiaNacimiento() + dependiente.getMesNacimiento() + value ;
				if (value.equals("")) { value = getEmptyStringContainer(6); }
				parameterMap.put("DIA1_NAC", String.valueOf(value.charAt(0)));
				parameterMap.put("DIA2_NAC", String.valueOf(value.charAt(1)));
				parameterMap.put("MES1_NAC", String.valueOf(value.charAt(2)));
				parameterMap.put("MES2_NAC", String.valueOf(value.charAt(3)));
				parameterMap.put("AA1_NAC", String.valueOf(value.charAt(4)));
				parameterMap.put("AA2_NAC", String.valueOf(value.charAt(5)));
		
				parameterMap
						.put("CHECK_IFE",
								(dependiente.getCveTipoIdentificacion() == CatalogoConstants.IDENT_IFE) ? "X"
										: "");
				parameterMap
						.put("CHECK_PASAPORTE",
								(dependiente.getCveTipoIdentificacion() == CatalogoConstants.IDENT_PASAPORTE) ? "X"
										: "");
				parameterMap
						.put("CHECK_ADULTOS",
								(dependiente.getCveTipoIdentificacion() == CatalogoConstants.IDENT_ADULTOS) ? "X"
										: "");
				parameterMap
						.put("CHECK_CED_PROF",
								(dependiente.getCveTipoIdentificacion() == CatalogoConstants.IDENT_CED_PROF) ? "X"
										: "");
				parameterMap
						.put("CHECK_MATR_CONS",
								(dependiente.getCveTipoIdentificacion() == CatalogoConstants.IDENT_MATR_CONS) ? "X"
										: "");
				parameterMap
						.put("CHECK_MIGRAT",
								(dependiente.getCveTipoIdentificacion() == CatalogoConstants.IDENT_FM2 || dependiente.getCveTipoIdentificacion() == CatalogoConstants.IDENT_FM3) ? "X"
										: "");
		
				parameterMap.put("NUM_IDENTIFICA",
						dependiente.getNumeroIdentificacion().toUpperCase());
				parameterMap.put("CALLE_PART", dependiente.getCalle().toUpperCase());
				parameterMap.put("NUM_EXT_PART", dependiente.getNumExt().toUpperCase());
				parameterMap.put("NUM_INT_PART", dependiente.getNumInt().toUpperCase());
				parameterMap.put("COLONIA_PART", dependiente.getColonia().toUpperCase());
				parameterMap.put("CIUDAD_PART", dependiente.getCiudad().toUpperCase());
				
				if(dependiente.getIdPais() == AppConstants.PAIS_MEXICO){
					parameterMap.put("MUNICIPIO_PART", catalogoService
							.getCatalogoItemDescription(CatalogoConstants.CAT_MUNICIPIOS,
									dependiente.getIdMunicipio()));
					parameterMap.put("ESTADO_PART", catalogoService
							.getCatalogoItemDescription(CatalogoConstants.CAT_ESTADOS,
									dependiente.getIdEstado()));	
				}else{
					parameterMap.put("MUNICIPIO_PART", dependiente.getMunicipioExtranjero());
					parameterMap.put("ESTADO_PART", dependiente.getEstadoExtranjero());
				}
		
				
				parameterMap.put("PAIS_PART", catalogoService
						.getCatalogoItemDescription(CatalogoConstants.CAT_PAISES,
								dependiente.getIdPais()));
		
				parameterMap.put("CP_PART", dependiente.getCodigoPostal());
				parameterMap.put("TEL_DOM", dependiente.getTelefonoCasa());
				parameterMap.put("TEL_OFIC", dependiente.getTelefonoTrabajo());
				parameterMap.put("TEL_CEL", dependiente.getTelefonoCelular());
				parameterMap.put("EMAIL_PERSONAL", dependiente.getCorreoPersonal());
				parameterMap.put("EMAIL_LABORAL", dependiente.getCorreoTrabajo());
				parameterMap.put("NUM_FOLIO_SOL", String.valueOf(dependiente.getPk().getSolicitud().getUserFolio()));
				//parameterMap.put("NUM_FOLIO_SOL", String.valueOf(s.getNumFolio()));
		
				// Falta los siguentes campos en bean
				parameterMap.put("CHECK_CONYUGE", (dependiente.getCaracter()==1)? "X": "");
				parameterMap.put("CHECK_DEPEND", (dependiente.getCaracter()==2)? "X": "");
		
				parameterMap.put("NUMERO_POLIZA", "");
				
				parameterMap.put("CHECK_EMPLEADO", (dependiente.getProfesion()==1)?"X":"");
				parameterMap.put("CHECK_PROFESIONAL", (dependiente.getProfesion()==2)?"X":"");
				parameterMap.put("CHECK_COMERCIANTE", (dependiente.getProfesion()==3)?"X":"");
				parameterMap.put("CHECH_JUBILADO", (dependiente.getProfesion()==4)?"X":"");
				parameterMap.put("CHECH_AMA_CASA", (dependiente.getProfesion()==5)?"X":"");
				parameterMap.put("CHECK_ESTUDIANTE", (dependiente.getProfesion()==6)?"X":"");
				parameterMap.put("CHECK_OTRO", (dependiente.getProfesion()==7)?"X":"");
				
				
				parameterMap.put("EMPRESA_LABORA", dependiente.getCentroTrabajo().toUpperCase());
				obj = dependiente.getDetalleActividad();
				if (obj==null) { value = getEmptyStringContainer(2);} else { value = obj.toString().toUpperCase();}
				parameterMap.put("DETALLE_ACTIVIDAD", value);
			
			break;
		
		case 2:
			    parameterMap.put("LUGAR Y FECHA","");
			
			break;
		
		
		default:
			break;

		}//switch
			
			
		return parameterMap;
	}

	private String getEmptyStringContainer(int size) {
		    String container = "";
		    for (int i=0; i<size; i++) {
		    	 container += " ";
		    }
		    return container;
	}
	
	private String getEmptyStringContainer(int size, String filler) {
	    String container = "";
	    for (int i=0; i<size; i++) {
	    	 container += filler;
	    }
	    return container;
	}
	
	
	
   
   
	@PostConstruct
	private void loadReports(){
		
		this.jrSolicitudRescateParcialH1 = this.loadReport("reportes/Sol_Resc_Parcial.jasper");
		this.jrSolicitudRescateParcialH2 = this.loadReport("reportes/Sol_Resc_Parcial_hoja_2.jasper");
		this.jrSolicitudRescateParcialDepH1 = this.loadReport("reportes/Sol_Datos_Articulo_140.jasper");
		this.jrSolicitudRescateParcialDepH2 = this.loadReport("reportes/Sol_Datos_Articulo_140_hoja_2.jasper");
		this.jrErrores = this.loadReport("reportes/Reporte_Errores.jasper");
		this.jrOficio = this.loadReport("reportes/Oficio.jasper");
		
   }
   
   
   
	private JasperReport loadReport(String name){
	    try{
	    	ClassPathResource classpathresource = null;
	    	InputStream inputstream = null;
	    	classpathresource = new ClassPathResource(name);
	    	inputstream = classpathresource.getInputStream();
	    	JasperReport jr = (JasperReport) JRLoader.loadObject(inputstream);
	    	return jr;
	    }catch(Exception e){
	    	e.printStackTrace();
	    	return null;
	    }
   }
	
	
	
	
	
	
	public JasperReport getJrSolicitudRescateParcialH1() {
		if(this.jrSolicitudRescateParcialH1 == null){
			this.jrSolicitudRescateParcialH1 = this.loadReport("reportes/Sol_Resc_Parcial.jasper");	
		}
		return jrSolicitudRescateParcialH1;
	}


	public JasperReport getJrSolicitudRescateParcialH2() {
		if(this.jrSolicitudRescateParcialH2 == null){
			this.jrSolicitudRescateParcialH2 = this.loadReport("reportes/Sol_Resc_Parcial_hoja_2.jasper");			
		}
		return jrSolicitudRescateParcialH2;
	}


	public JasperReport getJrSolicitudRescateParcialDepH1() {
		if(this.jrSolicitudRescateParcialDepH1 == null){
			this.jrSolicitudRescateParcialDepH1 = this.loadReport("reportes/Sol_Datos_Articulo_140.jasper");
		}
		return jrSolicitudRescateParcialDepH1;
	}


	public JasperReport getJrSolicitudRescateParcialDepH2() {
		
		if(this.jrSolicitudRescateParcialDepH2 == null){
			this.jrSolicitudRescateParcialDepH2 = this.loadReport("reportes/Sol_Datos_Articulo_140_hoja_2.jasper");
		}
		
		return jrSolicitudRescateParcialDepH2;
	}
	
	
	public JasperReport getJrErrores() {
		if(this.jrErrores == null){
			this.jrErrores = this.loadReport("reportes/Reporte_Errores.jasper");
		}
		return jrErrores;
	}


	public JasperReport getJrOficio() {
		if(this.jrOficio == null){
			this.jrOficio = this.loadReport("reportes/Oficio.jasper");
		}
		return jrOficio;
	}
	

	
	//ok

}