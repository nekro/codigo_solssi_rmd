package mx.com.metlife.solssi.report.util;

public class ErrorBean {

	private String campo;
	private String valor;
	private String mensaje;

	public ErrorBean(String campo, String valor, String mensaje) {
		this.campo = campo;
		this.valor = valor;
		this.mensaje = mensaje;
	}

	public String getCampo() {
		return campo;
	}

	public void setCampo(String campo) {
		this.campo = campo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}
