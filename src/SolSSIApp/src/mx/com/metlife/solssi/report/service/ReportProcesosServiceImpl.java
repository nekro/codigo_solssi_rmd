package mx.com.metlife.solssi.report.service;

import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.report.domain.ProcesoInfoBean;
import mx.com.metlife.solssi.report.persistence.ReportDAO;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.util.AppConstants;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
public class ReportProcesosServiceImpl implements ReportProcesosService{

	@Autowired
	private ReportDAO dao;
	
	@Override
	public List<ProcesoInfoBean> getProcesos(Date ini, Date fin) {
		List<ProcesoInfoBean> list = dao.getProcesos(ini, fin);
		for(ProcesoInfoBean bean : list){
			if(bean.getTotalSolicitudesAceptadas() > 0){
				bean.setTotalSolicitudesPagadas(dao.getCountSolicitudes(bean.getId(), AppConstants.ESTATUS_PAGADA));
				bean.setTotalSolicitudesRechazadasBanco(dao.getCountSolicitudes(bean.getId(), AppConstants.ESTATUS_RECHAZO_BANCO));
			}	
		}
		return list;
	}

	@Override
	public List<Solicitud> getSolicitudes(Integer proceso, Short estatus) {
		return dao.getSolicitudes(proceso, estatus);
	}
	
	@Override
	public List<Solicitud> getSolicitudesHistorico(Integer proceso, Short estatus) {
		return dao.getSolicitudesHistorico(proceso, estatus);
	}

}
