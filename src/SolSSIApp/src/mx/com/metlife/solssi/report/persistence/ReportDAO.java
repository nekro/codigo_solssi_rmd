package mx.com.metlife.solssi.report.persistence;

import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.report.domain.ProcesoInfoBean;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;

public interface ReportDAO {

	public List<ProcesoInfoBean> getProcesos(Date ini, Date fin);
	
	public List<Solicitud> getSolicitudesHistorico(Integer proceso, Short estatus);
	
	public List<Solicitud> getSolicitudes(Integer proceso, Short estatus);
	
	public int getCountSolicitudes(Integer proceso, Short estatus);
	
}
