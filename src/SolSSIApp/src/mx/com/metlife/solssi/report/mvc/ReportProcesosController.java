package mx.com.metlife.solssi.report.mvc;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.oficio.domain.Oficio;
import mx.com.metlife.solssi.oficio.domain.OficioFilter;
import mx.com.metlife.solssi.oficio.service.OficioService;
import mx.com.metlife.solssi.report.domain.ProcesoInfoBean;
import mx.com.metlife.solssi.report.service.ReportProcesosService;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.BrowserUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("report/procesos")
public class ReportProcesosController {
	
	@Autowired
	private ReportProcesosService service;
	
	@Autowired
	private OficioService oficioService;
	
	@RequestMapping(method = RequestMethod.POST)
	public String showReportProcesosPage(){
		return "report/procesos";
	}
	
	@RequestMapping(value = "execute/consulta", method = RequestMethod.POST)
	public String executeConsultaProcesos(@RequestParam("fechaInicio") String fechaInicio, @RequestParam("fechaFin") String fechaFin, Model model){
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		try{
		
			
			Date ini = null;
			Date fin = null;
			
			if(StringUtils.isEmpty(fechaInicio)){
				ini = Calendar.getInstance().getTime();
			}else{
				ini = fmt.parse(fechaInicio);
			}
			
			if(StringUtils.isEmpty(fechaFin)){
				fin = ini;
			}else{
				fin = fmt.parse(fechaFin);	
			}
			
			if(fin.before(ini)){
				Date aux = ini;
				ini = fin;
				fin = aux;
			}
			
			model.addAttribute("fechaInicio", fmt.format(ini));
			model.addAttribute("fechaFin", fmt.format(fin));
			
			List<ProcesoInfoBean> list = service.getProcesos(ini, fin);
			
			if(list.size() == 0){
				model.addAttribute("error", "No se encontraron resultados");
			}
			
			model.addAttribute("procesos", list);
		
		}catch(Exception e){
			model.addAttribute("error", e.getMessage());	
		}
		
		BrowserUtils.enableHistoryBack(model);
		
		return "report/procesos";
	}
	
	
	@RequestMapping(value = "oficios", method = RequestMethod.POST)
	public String executeConsultaOficios(@RequestParam("idProceso") Integer idProceso, Model model){
		
		OficioFilter filter = new OficioFilter();
		filter.setProceso(idProceso);
		List<Oficio> oficios = oficioService.getOficios(filter);
		model.addAttribute("oficios", oficios);
		
		BrowserUtils.enableHistoryBack(model);
		return "report/procesos/oficios";	
	}
	
	
	@RequestMapping(value = "solicitudes", method = RequestMethod.POST)
	public String detalleSolicitud(@RequestParam("idProceso") Integer idProceso, 
								   @RequestParam("estatus") Short estatus, Model model){
		
		List<Solicitud> solicitudes = service.getSolicitudesHistorico(idProceso, estatus);
		model.addAttribute("solicitudes", solicitudes);
		
		BrowserUtils.enableHistoryBack(model);
		
		return "report/procesos/solicitudes";
	}
	
	
	@RequestMapping(value = "solicitudes/estatus", method = RequestMethod.POST)
	public String detalleSolicitudPendienteRespuesta(@RequestParam("idProceso") Integer idProceso, 
													 @RequestParam("estatus") Short estatus, Model model){
		
		List<Solicitud> solicitudes = service.getSolicitudes(idProceso, estatus);
		model.addAttribute("solicitudes", solicitudes);
		
		BrowserUtils.enableHistoryBack(model);
		
		return "report/procesos/solicitudes";
	}

}
