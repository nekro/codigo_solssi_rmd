package mx.com.metlife.solssi.report.domain;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table( name = "TW_CTRL_ARCHIVO" )
public class ProcesoInfoBean {
	
	@Id
	@Column(name = "ID_PROCESO")
	private Integer id;
	
	@Basic
	private String nombre;
	
	@Basic
	private Integer clave;
	
	@Column( name = "FECHA_INICIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaInicio;
	
	@Column( name = "FECHA_FIN")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaFin;
	
	@Column( name = "TOTAL_SOLICITUDES_PROCESADAS")
	private Integer totalSolicitudesProcesadas;
	
	@Column( name = "TOTAL_SOLICITUDES_ENVIADAS")
	private Integer totalSolicitudesEnviadas;
	
	@Column( name = "TOTAL_SOLICITUDES_RECHAZADAS")
	private Integer totalSolicitudesRechazadas;
	
	@Column( name = "TOTAL_SOLICITUDES_ACEPTADAS")
	private Integer totalSolicitudesAceptadas;
	
	@Column( name = "TOTAL_SOLICITUDES_ERRORES")
	private Integer totalSolicitudesErrores;
	
	@Transient
	private Integer totalSolicitudesPagadas;
	
	@Transient
	private Integer totalSolicitudesRechazadasBanco;
	
	@Basic
	private Short estatus;

	/**
	 * @return el id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id el id a establecer
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return el nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre el nombre a establecer
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return el clave
	 */
	public Integer getClave() {
		return clave;
	}

	/**
	 * @param clave el clave a establecer
	 */
	public void setClave(Integer clave) {
		this.clave = clave;
	}

	/**
	 * @return el fechaInicio
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * @param fechaInicio el fechaInicio a establecer
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * @return el fechaFin
	 */
	public Date getFechaFin() {
		return fechaFin;
	}

	/**
	 * @param fechaFin el fechaFin a establecer
	 */
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	/**
	 * @return el totalSolicitudesProcesadas
	 */
	public Integer getTotalSolicitudesProcesadas() {
		return totalSolicitudesProcesadas;
	}

	/**
	 * @param totalSolicitudesProcesadas el totalSolicitudesProcesadas a establecer
	 */
	public void setTotalSolicitudesProcesadas(Integer totalSolicitudesProcesadas) {
		this.totalSolicitudesProcesadas = totalSolicitudesProcesadas;
	}

	/**
	 * @return el totalSolicitudesEnviadas
	 */
	public Integer getTotalSolicitudesEnviadas() {
		return totalSolicitudesEnviadas;
	}

	/**
	 * @param totalSolicitudesEnviadas el totalSolicitudesEnviadas a establecer
	 */
	public void setTotalSolicitudesEnviadas(Integer totalSolicitudesEnviadas) {
		this.totalSolicitudesEnviadas = totalSolicitudesEnviadas;
	}

	/**
	 * @return el totalSolicitudesRechazadas
	 */
	public Integer getTotalSolicitudesRechazadas() {
		return totalSolicitudesRechazadas;
	}

	/**
	 * @param totalSolicitudesRechazadas el totalSolicitudesRechazadas a establecer
	 */
	public void setTotalSolicitudesRechazadas(Integer totalSolicitudesRechazadas) {
		this.totalSolicitudesRechazadas = totalSolicitudesRechazadas;
	}

	/**
	 * @return el totalSolicitudesAceptadas
	 */
	public Integer getTotalSolicitudesAceptadas() {
		return totalSolicitudesAceptadas;
	}

	/**
	 * @param totalSolicitudesAceptadas el totalSolicitudesAceptadas a establecer
	 */
	public void setTotalSolicitudesAceptadas(Integer totalSolicitudesAceptadas) {
		this.totalSolicitudesAceptadas = totalSolicitudesAceptadas;
	}

	/**
	 * @return el totalSolicitudesErrores
	 */
	public Integer getTotalSolicitudesErrores() {
		return totalSolicitudesErrores;
	}

	/**
	 * @param totalSolicitudesErrores el totalSolicitudesErrores a establecer
	 */
	public void setTotalSolicitudesErrores(Integer totalSolicitudesErrores) {
		this.totalSolicitudesErrores = totalSolicitudesErrores;
	}

	/**
	 * @return el estatus
	 */
	public Short getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus el estatus a establecer
	 */
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return el totalSolicitudesPagadas
	 */
	public Integer getTotalSolicitudesPagadas() {
		return totalSolicitudesPagadas;
	}

	/**
	 * @param totalSolicitudesPagadas el totalSolicitudesPagadas a establecer
	 */
	public void setTotalSolicitudesPagadas(Integer totalSolicitudesPagadas) {
		this.totalSolicitudesPagadas = totalSolicitudesPagadas;
	}

	/**
	 * @return el totalSolicitudesRechazadasBanco
	 */
	public Integer getTotalSolicitudesRechazadasBanco() {
		return totalSolicitudesRechazadasBanco;
	}

	/**
	 * @param totalSolicitudesRechazadasBanco el totalSolicitudesRechazadasBanco a establecer
	 */
	public void setTotalSolicitudesRechazadasBanco(
			Integer totalSolicitudesRechazadasBanco) {
		this.totalSolicitudesRechazadasBanco = totalSolicitudesRechazadasBanco;
	}
	
	
	

}
