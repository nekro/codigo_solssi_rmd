package mx.com.metlife.solssi.report.persistence;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.metlife.solssi.report.domain.ProcesoInfoBean;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ReportDAOImpl implements ReportDAO{

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<ProcesoInfoBean> getProcesos(Date ini, Date fin) {
		Session session = sessionFactory.getCurrentSession();
		
		Calendar calIni = Calendar.getInstance();
		calIni.setTime(ini);
		calIni.set(Calendar.HOUR_OF_DAY, 0);
		calIni.set(Calendar.MINUTE, 0);
		calIni.set(Calendar.SECOND, 0);
		calIni.set(Calendar.MILLISECOND, 0);
		
		Calendar calFin = Calendar.getInstance();
		calFin.setTime(fin);
		calFin.set(Calendar.HOUR_OF_DAY, 23);
		calFin.set(Calendar.MINUTE, 59);
		calFin.set(Calendar.SECOND, 59);
		calFin.set(Calendar.MILLISECOND, 999);
		
		Criteria crit = session.createCriteria(ProcesoInfoBean.class);
		crit.add(Restrictions.between("fechaInicio", calIni.getTime(), calFin.getTime()));
		
		return crit.list();
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Solicitud> getSolicitudesHistorico(Integer proceso, Short estatus){
		
		Session session = sessionFactory.getCurrentSession();
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("select sol.numFolio, sol.rfc, sol.oficio, sol.tipoTramite, sol.cuenta, sol.cveStatus ");
        sql.append("from Solicitud sol, ControlSolicitud ctrl ");
        sql.append("where sol.numFolio = ctrl.numFolio ");
        sql.append("and sol.idProceso = :proceso ");
        sql.append("and ctrl.estatus = :estatus");
		
		
		Query query = session.createQuery(sql.toString());
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		query.setInteger("proceso", proceso);
		query.setShort("estatus", estatus);
		
		List<Map> list = query.list();
		List<Solicitud> solicitudes = new ArrayList<Solicitud>();
		
		for(Map m : list){
			Solicitud sol = new Solicitud();
			sol.setNumFolio((Integer)m.get("0"));
			sol.setRfc((String)m.get("1"));
			sol.setOficio((Integer)m.get("2"));
			sol.setTipoTramite((Short)m.get("3"));
			sol.setCuenta((Long)m.get("4"));
			sol.setCveStatus((Short)m.get("5"));
			solicitudes.add(sol);
		}
		
		return solicitudes;
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Solicitud> getSolicitudes(Integer proceso, Short estatus){
		
		Session session = sessionFactory.getCurrentSession();
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("select sol.numFolio, sol.rfc, sol.oficio, sol.tipoTramite, sol.cuenta, sol.cveStatus ");
        sql.append("from Solicitud sol ");
        sql.append("where sol.idProceso = :proceso ");
        sql.append("and sol.cveStatus = :estatus");
		
		
		Query query = session.createQuery(sql.toString());
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		query.setInteger("proceso", proceso);
		query.setShort("estatus", estatus);
		
		List<Map> list = query.list();
		List<Solicitud> solicitudes = new ArrayList<Solicitud>();
		
		for(Map m : list){
			Solicitud sol = new Solicitud();
			sol.setNumFolio((Integer)m.get("0"));
			sol.setRfc((String)m.get("1"));
			sol.setOficio((Integer)m.get("2"));
			sol.setTipoTramite((Short)m.get("3"));
			sol.setCuenta((Long)m.get("4"));
			sol.setCveStatus((Short)m.get("5"));
			solicitudes.add(sol);
		}
		
		return solicitudes;
	}
	
	
	public int getCountSolicitudes(Integer proceso, Short estatus){
		
		Session session = sessionFactory.getCurrentSession();
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("select count(sol.numFolio) ");
        sql.append("from Solicitud sol ");
        sql.append("where sol.idProceso = :proceso ");
        sql.append("and sol.cveStatus = :estatus");
		
		
		Query query = session.createQuery(sql.toString());
		
		query.setInteger("proceso", proceso);
		query.setShort("estatus", estatus);
		
		Long result = (Long) query.uniqueResult();
		
		return result.intValue();
	}
	
	
}
