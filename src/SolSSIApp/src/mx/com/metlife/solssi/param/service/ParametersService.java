package mx.com.metlife.solssi.param.service;

import java.util.List;

import mx.com.metlife.solssi.param.domain.AppParameter;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
public interface ParametersService {
	
	public String getParamStringValue(String key);
	public Integer getParamIntegerValue(String key);
	public Long getParamLongValue(String key);
	public Integer getParamIntegerValue(String key, Integer def);
	public Long getParamLongValue(String key, Long def);
	
	public List<AppParameter> getAppParameters();
	
	public List<AppParameter> getAppParameters(String filter);
	
	public AppParameter getAppParameter(String key);
	
	public String getAppParamValue(String key, String def);
	
	@Transactional(propagation=Propagation.REQUIRED,readOnly=false)
	public void save(AppParameter param);
	
	@Transactional(propagation=Propagation.REQUIRED,readOnly=false)
	public void update(AppParameter param);
	
}
