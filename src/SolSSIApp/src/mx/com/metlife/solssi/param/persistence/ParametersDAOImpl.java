package mx.com.metlife.solssi.param.persistence;

import java.util.List;

import mx.com.metlife.solssi.main.dao.GenericDAOImpl;
import mx.com.metlife.solssi.param.domain.AppParameter;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class ParametersDAOImpl extends GenericDAOImpl<AppParameter, String> implements ParametersDAO{
	
	
	public String getAppParamValue(String key) {
		
		Session session = getSession();
		AppParameter param =  (AppParameter) session.get(AppParameter.class, key);
		if(param == null){
			return StringUtils.EMPTY;
		}
		return param.getValor();
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<AppParameter> getAppParameters(String filter) {
		Session session = getSession();
		
		Criteria crit = session.createCriteria(AppParameter.class);
		
		if(StringUtils.isNotEmpty(filter)){
			crit.add(Restrictions.ilike("clave", filter, MatchMode.ANYWHERE));
		}
				      
		crit.addOrder(Order.asc("clave"));
						
		return crit.list();
	}
	
	public void save(AppParameter param) {
		Session session = getSession();
		session.save(param);
	}
	
	public void update(AppParameter param) {
		Session session = getSession();
		session.update(param);
	}


	public AppParameter getAppParameter(String key) {
		Session session = getSession();
		return (AppParameter) session.get(AppParameter.class, key);
	}

}
