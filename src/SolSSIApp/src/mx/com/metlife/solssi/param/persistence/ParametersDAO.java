package mx.com.metlife.solssi.param.persistence;

import java.util.List;

import mx.com.metlife.solssi.main.dao.GenericDAO;
import mx.com.metlife.solssi.param.domain.AppParameter;

import com.googlecode.ehcache.annotations.Cacheable;
import com.googlecode.ehcache.annotations.TriggersRemove;

public interface ParametersDAO extends GenericDAO<AppParameter, String>{
	
	@Cacheable(cacheName = "parametrosCache")
	public String getAppParamValue(String key);
	
	public AppParameter getAppParameter(String key);
	
	public List<AppParameter> getAppParameters(String filter);
	
	@TriggersRemove(cacheName={"parametrosCache"}, removeAll=true)
	public void save(AppParameter param);
	
	@TriggersRemove(cacheName={"parametrosCache"}, removeAll=true)
	public void update(AppParameter param);

}
