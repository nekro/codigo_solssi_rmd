package mx.com.metlife.solssi.param.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="TC_PARAMETROS_SISTEMA")
public class AppParameter implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2890353660752470624L;

	@Id
	@Column(name="cve_parametro")
	@Pattern(regexp="^[\\S]*$")
	private String clave;
	
	@Column(name="valor")
	@NotEmpty(message="Capture un valor")
	private String valor;
	
	
	/**
	 * 
	 * @return
	 */
	public String getClave() {
		clave = StringUtils.trim(StringUtils.upperCase(clave));
		return clave;
	}
	
	/**
	 * 
	 * @param cveParametro
	 */
	public void setClave(String cveParametro) {
		this.clave = StringUtils.trim(StringUtils.upperCase(cveParametro));
	}
	
	/**
	 * 
	 * @return
	 */
	public String getValor() {
		return StringUtils.trim(valor);
	}
	
	
	/**
	 * 
	 * @param valor
	 */
	public void setValor(String valor) {
		this.valor = StringUtils.trim(valor);
	}
}
