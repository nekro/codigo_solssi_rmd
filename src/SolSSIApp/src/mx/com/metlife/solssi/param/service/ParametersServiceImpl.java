package mx.com.metlife.solssi.param.service;

import java.util.List;

import mx.com.metlife.solssi.param.domain.AppParameter;
import mx.com.metlife.solssi.param.persistence.ParametersDAO;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ParametersServiceImpl implements ParametersService{
	
	@Autowired
	ParametersDAO paramDAO;

	public String getAppParamValue(String key, String def) {
		String val = paramDAO.getAppParamValue(key);
		
		if(StringUtils.isEmpty(val)){
			return def;
		}
		
		return val;
	}

	public List<AppParameter> getAppParameters() {
		return paramDAO.getAppParameters(null);
	}
	
	public List<AppParameter> getAppParameters(String filter) {
		return paramDAO.getAppParameters(filter);
	}

	public void save(AppParameter param) {
		paramDAO.save(param);
	}

	public void update(AppParameter param) {
		paramDAO.update(param);
	}

	public AppParameter getAppParameter(String key) {
		return paramDAO.getAppParameter(key);
	}

	public Integer getParamIntegerValue(String key) {
		try{
			String p = paramDAO.getAppParamValue(key);
			return Integer.valueOf(p);
		}catch(Exception e){
			return 0;
		}
	}

	public Long getParamLongValue(String key) {
		try{
			String p = paramDAO.getAppParamValue(key);
			return Long.valueOf(p);
		}catch(Exception e){
			return 0l;
		}
	}

	public Integer getParamIntegerValue(String key, Integer defaultValue) {
		try{
			String p = paramDAO.getAppParamValue(key);
			return Integer.valueOf(p);
		}catch(Exception e){
			return defaultValue;
		}
	}

	public Long getParamLongValue(String key, Long defaultValue) {
		try{
			String p = paramDAO.getAppParamValue(key);
			return Long.valueOf(p);
		}catch(Exception e){
			return defaultValue;
		}
	}

	public String getParamStringValue(String key) {
		return paramDAO.getAppParamValue(key);
	}

	
}
