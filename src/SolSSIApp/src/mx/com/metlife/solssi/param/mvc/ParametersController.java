package mx.com.metlife.solssi.param.mvc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import mx.com.metlife.solssi.param.domain.AppParameter;
import mx.com.metlife.solssi.param.service.ParametersService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("params")
public class ParametersController {
	
	@Autowired 
	ParametersService paramsService;
	
	@RequestMapping(method = RequestMethod.POST)
	public String showParamPage(Model model){
		return "param/init";
	}
	
	@ModelAttribute
	public void putParams(Model model){
		List<AppParameter> params = paramsService.getAppParameters();
		model.addAttribute("params", params);		
	}
	
	@RequestMapping(value="search")
	public String search(@RequestParam("filter") String filter, Model model){
		List<AppParameter> params = paramsService.getAppParameters(filter);
		model.addAttribute("params",params);
		return "param/init";
	}
	
	@RequestMapping(value="edit")
	public String showEdit(@RequestParam("clave") String clave, Model model){
		model.addAttribute("ACTION", "EDIT");
		AppParameter param = paramsService.getAppParameter(clave);
		model.addAttribute(param);
		
		return "param/init";
	}
	
	@RequestMapping(value="alta")
	public String showAlta(Model model){
		
		model.addAttribute(new AppParameter());
		model.addAttribute("ACTION", "ALTA");
		
		return "param/init";
	}

	
	@RequestMapping(value="save")
	public String save(@RequestParam("filter") String filter, @Valid AppParameter param, BindingResult bindingResult, Model model) throws Exception{
		
		if(bindingResult.hasErrors()){
			model.addAttribute(param);
			model.addAttribute("ACTION","ALTA");
			return "param/init"; 
		}
		
		paramsService.save(param);
		
		List<AppParameter> params = paramsService.getAppParameters(filter);
		model.addAttribute("params", params);
		model.addAttribute(new AppParameter());
		
		return "param/init";
	}
	
	@RequestMapping(value="update")
	public String update(@RequestParam("filter") String filter, @Valid AppParameter param, BindingResult bindingResult, Model model) throws Exception{
		
		model.addAttribute(new AppParameter());
		
		if(bindingResult.hasErrors()){
			model.addAttribute(param);
			model.addAttribute("ACTION","EDIT");
			return "param/init"; 
		}
		
		paramsService.update(param);
		
		List<AppParameter> params = paramsService.getAppParameters(filter);
		model.addAttribute("params", params);
				
		return "param/init";
	}
	
}
