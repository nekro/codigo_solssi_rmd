package mx.com.metlife.solssi.cifrascontrol.persistence;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFactor;


public interface CCFactorDAO {
	
	public void save(CCFactor factor);
	
	public CCFactor get(Integer idProceso);
	
}
