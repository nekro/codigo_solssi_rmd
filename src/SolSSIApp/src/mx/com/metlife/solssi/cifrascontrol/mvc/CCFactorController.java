package mx.com.metlife.solssi.cifrascontrol.mvc;

import javax.validation.Valid;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFactor;
import mx.com.metlife.solssi.cifrascontrol.service.CCFactorService;
import mx.com.metlife.solssi.util.Base26Codec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("ccfactor")
public class CCFactorController {
	
	@Autowired
	private CCFactorService factorService;
	
	@RequestMapping(method = RequestMethod.POST)
	public String showPage(Model model){
		
		model.addAttribute(new CCFactor());
		return "ccfactor";
	}
	
	@RequestMapping(value = "save" , method = RequestMethod.POST)
	public String save(@Valid CCFactor factor, BindingResult errors, Model model){
		
		if(errors.hasErrors()){
			model.addAttribute(factor);
			return "ccfactor";
		}
		
		
		
		model.addAttribute("save_estatus","ok");
		model.addAttribute(new CCFactor());
		
		factorService.save(factor);
		return "ccfactor";
	}

}
