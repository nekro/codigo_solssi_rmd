package mx.com.metlife.solssi.cifrascontrol.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table( name = "TC_FACTORES_CC")
public class CCFactor implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4624456191353505057L;

	@Id
	@Column(name = "ID_VERSION")
	private int version;
	
	@Column(name = "VALUE_X")
	private int x;
	
	@Column(name = "VALUE_Y")
	private int y;

	/**
	 * @return the version
	 */
	public int getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(int version) {
		this.version = version;
	}
	
	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}
}
