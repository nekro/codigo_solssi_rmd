package mx.com.metlife.solssi.cifrascontrol.persistence;

import java.util.List;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFilter;
import mx.com.metlife.solssi.cifrascontrol.domain.CifraControl;
import mx.com.metlife.solssi.cifrascontrol.domain.CifraControlTotal;

public interface CifrasControlDAO{
	
	public CifraControlTotal getTotal(CCFilter filter);
	
	public List<CifraControlTotal> getDetalle(CCFilter filter);

}
