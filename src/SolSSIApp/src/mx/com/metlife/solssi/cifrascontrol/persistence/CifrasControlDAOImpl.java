package mx.com.metlife.solssi.cifrascontrol.persistence;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFilter;
import mx.com.metlife.solssi.cifrascontrol.domain.CifraControl;
import mx.com.metlife.solssi.cifrascontrol.domain.CifraControlTotal;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CifrasControlDAOImpl implements CifrasControlDAO{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public CifraControlTotal getTotal(CCFilter filter) {
		Session session = this.sessionFactory.getCurrentSession();
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select cve_sistema, tipo_registro, min(fecha) as fecha, sum(total_solicitudes) as total_solicitudes, sum(total_aceptadas) as total_aceptadas,"); 
		sql.append(" sum(suma_folios) as suma_folios, sum(suma_porcentajes) as suma_porcentajes, sum(suma_clabes) as suma_clabes, ");
		sql.append(" sum(suma_importes) as suma_importes, sum(suma_saldos) as suma_saldos");
		sql.append(" from tw_cifras_control ");
		
		if(filter.getCriterio() == 1){//por dependencia
			sql.append(" where cve_dependencia = :dependencia and grupo = :grupo ");
		}
		
		if(filter.getCriterio() == 2){//por id proceso
			sql.append(" where id_proceso = :proceso ");
		}
		
		if(filter.getCriterio() == 3){//por fecha
			sql.append(" where id_proceso in (select id_proceso from tw_ctrl_archivo where truncate(fecha_inicio) = :fecha) ");
		}
		
		if(filter.getCriterio() == 4 ){//por grupo
			sql.append(" where grupo = :grupo ");
		}
		
		sql.append(" group by cve_sistema, tipo_registro ");
		
		Query query = session.createSQLQuery(sql.toString())
						.addScalar("cve_sistema", Hibernate.SHORT)		
						.addScalar("tipo_registro", Hibernate.SHORT)
						.addScalar("total_solicitudes", Hibernate.INTEGER)
						.addScalar("total_aceptadas", Hibernate.INTEGER)
						.addScalar("suma_folios", Hibernate.LONG)
						.addScalar("suma_porcentajes", Hibernate.INTEGER)
						.addScalar("suma_clabes", Hibernate.LONG)
						.addScalar("suma_importes", Hibernate.DOUBLE)
						.addScalar("suma_saldos", Hibernate.DOUBLE)
						.addScalar("fecha", Hibernate.DATE);
						
		if(filter.getCriterio() == 1){//por dependencia
			query.setInteger("dependencia", filter.getDependencia());
			query.setInteger("grupo", filter.getGrupo());
		}
		
		if(filter.getCriterio() == 2){//por id proceso
			query.setInteger("proceso", filter.getProceso());
		}
		
		if(filter.getCriterio() == 3){//por fecha
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			Date date;
			try {
				date = fmt.parse(filter.getFecha());
				query.setDate("fecha", date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
		}
		
		if(filter.getCriterio() == 4 ){
			query.setInteger("grupo", filter.getGrupo());
		}
		
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		List<Map> list = query.list();
		
		List<CifraControl> cifras = new ArrayList<CifraControl>();
		
		for(Map map : list){
			
			CifraControl cifra = new CifraControl();
			
			cifra.setFecha((Date)map.get("fecha"));
			cifra.setSistema((Short)map.get("cve_sistema"));
			cifra.setTipo((Short)map.get("tipo_registro"));
			cifra.setSumaFolios((Long)map.get("suma_folios"));
			cifra.setSumaPorcentajes((Integer) map.get("suma_porcentajes"));
			cifra.setSumaClabes((Long) map.get("suma_clabes"));
			cifra.setTotalSolicitudes((Integer) map.get("total_solicitudes"));
			cifra.setTotalAceptadas((Integer) map.get("total_aceptadas"));
			cifra.setSumaImportes((Double)map.get("suma_importes"));
			cifra.setSumaSaldos((Double)map.get("suma_saldos"));
			
			cifras.add(cifra);
		}
		
		CifraControlTotal total = new CifraControlTotal();
		
		total.setDetalle(cifras);
		
		return total;
	}
	
	
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<CifraControlTotal> getDetalle(CCFilter filter) {
		Session session = this.sessionFactory.getCurrentSession();
		
		StringBuffer sql = new StringBuffer();
		sql.append(" select cve_dependencia, cve_sistema, tipo_registro, min(fecha) as fecha, sum(total_solicitudes) as total_solicitudes, sum(total_aceptadas) as total_aceptadas,"); 
		sql.append(" sum(suma_folios) as suma_folios, sum(suma_porcentajes) as suma_porcentajes, sum(suma_clabes) as suma_clabes, ");
		sql.append(" sum(suma_importes) as suma_importes, sum(suma_saldos) as suma_saldos");
		sql.append(" from tw_cifras_control ");
		
		if(filter.getCriterio() == 2){//por id proceso
			sql.append(" where id_proceso = :proceso ");
		}
		
		if(filter.getCriterio() == 3){//por fecha
			sql.append(" where id_proceso in (select id_proceso from tw_ctrl_archivo where truncate(fecha_inicio) = :fecha) ");
		}
		
		if(filter.getCriterio() == 4){//por grupo (id -web)
			sql = new StringBuffer();
			sql.append(" select cve_dependencia, id_proceso, cve_sistema, tipo_registro, min(fecha) as fecha, sum(total_solicitudes) as total_solicitudes, sum(total_aceptadas) as total_aceptadas,"); 
			sql.append(" sum(suma_folios) as suma_folios, sum(suma_porcentajes) as suma_porcentajes, sum(suma_clabes) as suma_clabes, ");
			sql.append(" sum(suma_importes) as suma_importes, sum(suma_saldos) as suma_saldos");
			sql.append(" from tw_cifras_control ");
			sql.append(" where grupo = :grupo ");
			sql.append(" group by cve_dependencia, id_proceso, cve_sistema, tipo_registro ");
		}else{
			sql.append(" group by cve_dependencia, cve_sistema, tipo_registro ");
		}
		
		
		
		SQLQuery query = session.createSQLQuery(sql.toString())
						.addScalar("cve_dependencia", Hibernate.INTEGER)		
						.addScalar("cve_sistema", Hibernate.SHORT)		
						.addScalar("tipo_registro", Hibernate.SHORT)
						.addScalar("total_solicitudes", Hibernate.INTEGER)
						.addScalar("total_aceptadas", Hibernate.INTEGER)
						.addScalar("suma_folios", Hibernate.LONG)
						.addScalar("suma_porcentajes", Hibernate.INTEGER)
						.addScalar("suma_clabes", Hibernate.LONG)
						.addScalar("suma_importes", Hibernate.DOUBLE)
						.addScalar("suma_saldos", Hibernate.DOUBLE)
						.addScalar("fecha", Hibernate.DATE);
		
		
						
		if(filter.getCriterio() == 2){//por id proceso
			query.setInteger("proceso", filter.getProceso());
		}
		
		if(filter.getCriterio() == 3){//por fecha
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			Date date;
			try {
				date = fmt.parse(filter.getFecha());
				query.setDate("fecha", date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
		}
		
		if(filter.getCriterio() == 4){//por grupo
			query.addScalar("id_proceso", Hibernate.INTEGER);
			query.setInteger("grupo", filter.getGrupo());
		}
		
		
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		List<Map> list = query.list();
		
		List<CifraControlTotal> totales = new ArrayList<CifraControlTotal>();
		
		List<CifraControl> cifras = new ArrayList<CifraControl>();
		
		CifraControlTotal ct = null;
		
		for(Map map : list){
			
			Integer dependencia = (Integer )map.get("cve_dependencia");

			ct = new CifraControlTotal();
			ct.setIdentificador(dependencia);
			
			CifraControl cifra = new CifraControl();
			
			if(filter.getCriterio() == 4){//por grupo
				cifra.setProceso((Integer)map.get("id_proceso"));
			}
			
			cifra.setFecha((Date)map.get("fecha"));
			cifra.setSistema((Short)map.get("cve_sistema"));
			cifra.setTipo((Short)map.get("tipo_registro"));
			cifra.setSumaFolios((Long)map.get("suma_folios"));
			cifra.setSumaPorcentajes((Integer) map.get("suma_porcentajes"));
			cifra.setSumaClabes((Long) map.get("suma_clabes"));
			cifra.setTotalSolicitudes((Integer) map.get("total_solicitudes"));
			cifra.setTotalAceptadas((Integer) map.get("total_aceptadas"));
			cifra.setSumaImportes((Double)map.get("suma_importes"));
			cifra.setSumaSaldos((Double)map.get("suma_saldos"));
			
			if(totales.indexOf(ct) == -1){
				cifras = new ArrayList<CifraControl>();
				totales.add(ct);
			}else{
				ct = totales.get(totales.indexOf(ct));
			}
			
			
			cifras.add(cifra);
			ct.setDetalle(cifras);

		}
		
		return totales;
	}

}
