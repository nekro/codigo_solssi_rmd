package mx.com.metlife.solssi.cifrascontrol.mvc;

import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.service.CatalogoService;
import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.cifrascontrol.domain.CCFilter;
import mx.com.metlife.solssi.cifrascontrol.domain.CifraControl;
import mx.com.metlife.solssi.cifrascontrol.domain.CifraControlTotal;
import mx.com.metlife.solssi.util.AppConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CSVCifrasDownloadController {
	
	@Autowired
	private CatalogoService catalogoservice; 
	
	@SuppressWarnings("unchecked")
	 @RequestMapping(value = "/cifrascontrol/download")
	 public void downloadCSV(HttpServletResponse response, HttpSession session) throws IOException {
		
		try{

		 
		 	CifraControlTotal total = (CifraControlTotal) session.getAttribute("cifras"); 
		 	List<CifraControlTotal> detalle = (List<CifraControlTotal>) session.getAttribute("detalle");
		 	CCFilter filter = (CCFilter) session.getAttribute("CCFilter");
		 	
		 	if(total == null){
		 		return;
		 	}
		 
	        String csvFileName = "cifras_control.csv";
	 
	        response.setContentType("text/csv");
	 
	        // creates mock data
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",csvFileName);
	        
	        response.setHeader(headerKey, headerValue);
	        
	        ServletOutputStream os = response.getOutputStream();
	        PrintStream writer = new PrintStream(os); 
	        
	        switch(filter.getCriterio()){
	        	case 1: 
	        			writer.println("CIFRAS CONTROL DE LA DEPENDENCIA");	        			
	        			break;
	        	case 2:
	        			writer.println("CIFRAS CONTROL DEL PROCESO WEB "+filter.getProceso());	        			
	        			break;
	        	case 3:
	        			writer.println("CIFRAS CONTROL DE LA FECHA "+filter.getFecha());	        			
	        			break;
	        	case 4:
	        			writer.println("CIFRAS CONTROL DEL GRUPO "+filter.getGrupo());	        			
	        			break;
	        }
	        
	        writer.println("TOTAL");
	        
	        this.writeTableCC(writer, filter, total);
	        
	        if(detalle != null && filter.getCriterio() != 1){
	        	
	        	writer.println();
		        writer.print("DETALLE");
		        writer.println();
		        
		        for(CifraControlTotal tot : detalle){
		        	
		        	CatalogoItem item = catalogoservice.getCatalogoItem(CatalogoConstants.DEPENDENCIAS, String.valueOf(tot.getIdentificador()));
		        	writer.println("DEPENDENCIA " + item.getDescDependencia());
		        	this.writeTableCC(writer, filter, tot);
		        	
		        }
	        }
	        
	        writer.flush();
	        writer.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	        
	 }
	 
	 private String getNameSistema(Short cveSistema){
		 switch (cveSistema) {
			case 1:return "SSI WEB";
			case 2:return "SSI";
			case 3:return "TESORERIA";
		}
		 return "";
	 }
	 
	 private String getTipoRegistro(Short tipo){
		 switch (tipo) {
			case 1:return "TRÁMITES SATISFACTORIOS";
			case 2:return "SERVICIOS OPERADOS";
			case 3:return "RECHAZOS";
		}
		 return "";
	 }
	 
	 private void writeCifraControl(PrintStream writer, CifraControl cc) throws IOException{
			writer.print(cc.getTotalAceptadas());
			writer.print("|");
			writer.print(cc.getSumaPorcentajes());
			writer.print("|");
			writer.print(cc.getSumaFolios());
			writer.print("|");
			writer.print(cc.getSumaClabes());
			writer.print("|");
			writer.println(cc.getFmtSumaImportes());
	 }
	 
	 
	 private void writeTableCC(PrintStream writer, CCFilter filter, CifraControlTotal total) throws IOException{
		 
		 writer.println("||TOTAL TRÁMITES|SUMA DE PORCENTAJES|SUMA DE FOLIOS|CLABES BANCARIAS|SUMA DE MONTO");
	        
	        for(CifraControl cc : total.getDetalle()){
	        	if(filter.getCriterio() != 4 || (!(filter.getCriterio() != 4 || cc.getSistema() != 1))){
	        		if(cc.getTipo() == AppConstants.CC_TIPO_SSI_ER){
	        			writer.print("DIFERENCIA|PENDIENTES|");
	        			this.writeCifraControl(writer, total.getDiferenciaWEBSSI());
	        		}
	        		
	        		writer.print(this.getNameSistema(cc.getSistema()));
	        		writer.print("|");
	        		writer.print(this.getTipoRegistro(cc.getTipo()));
	        		writer.print("|");
	        		this.writeCifraControl(writer, cc);
	        		
	        	}
	        }
	        
	        if(filter.getCriterio()  != 4){
	     		writer.print("|TOTAL|");
	     		this.writeCifraControl(writer, total.getTotalSSI());
	     		
	     		writer.print("|COMPROBACIÓN|");
	     		this.writeCifraControl(writer, total.getComprobacionSSI());
	        }
	        
	        
	        writer.println();
		 
	 }

}
