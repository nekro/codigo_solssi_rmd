package mx.com.metlife.solssi.cifrascontrol.domain;

import java.io.Serializable;
import java.util.List;

import mx.com.metlife.solssi.util.AppConstants;

public class CifraControlTotal implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8882947755929017755L;

	private Integer identificador;
	
	private List<CifraControl> detalle;
	
	private CifraControl totalSSI;
	
	private CifraControl comprobacionSSI;
	
	private CifraControl diferenciaWEBSSI;


	/**
	 * @return the total
	 */
	public CifraControl getTotalSSI() {
		
		if(totalSSI != null){
			return totalSSI;
		}
		
		CifraControl ccOK = new CifraControl();
		CifraControl ccER = new CifraControl();
		
		for(CifraControl cc : this.detalle){
			if(cc.getTipo() == AppConstants.CC_TIPO_SSI_OK){
				ccOK.setSumaClabes(ccOK.getSumaClabes() + cc.getSumaClabes());
				ccOK.setSumaFolios(ccOK.getSumaFolios() + cc.getSumaFolios());
				ccOK.setSumaPorcentajes(ccOK.getSumaPorcentajes() + cc.getSumaPorcentajes());
				ccOK.setTotalAceptadas(ccOK.getTotalAceptadas() + cc.getTotalAceptadas());
				ccOK.setTotalSolicitudes(ccOK.getTotalSolicitudes() + cc.getTotalSolicitudes());
			}else{
				if(cc.getTipo() == AppConstants.CC_TIPO_SSI_ER){
					ccER.setSumaClabes(ccER.getSumaClabes() + cc.getSumaClabes());
					ccER.setSumaFolios(ccER.getSumaFolios() + cc.getSumaFolios());
					ccER.setSumaPorcentajes(ccER.getSumaPorcentajes() + cc.getSumaPorcentajes());
					ccER.setTotalAceptadas(ccER.getTotalAceptadas() + cc.getTotalAceptadas());
					ccER.setTotalSolicitudes(ccER.getTotalSolicitudes() + cc.getTotalSolicitudes());
				}	
			}
		}
		
		totalSSI = new CifraControl();
		
		totalSSI.setSumaClabes(ccOK.getSumaClabes() + ccER.getSumaClabes());
		totalSSI.setSumaFolios(ccOK.getSumaFolios() + ccER.getSumaFolios());
		totalSSI.setSumaPorcentajes(ccOK.getSumaPorcentajes() + ccER.getSumaPorcentajes());
		totalSSI.setTotalAceptadas(ccOK.getTotalAceptadas() + ccER.getTotalAceptadas());
		totalSSI.setTotalSolicitudes(ccOK.getTotalSolicitudes() + ccER.getTotalSolicitudes());
		
		return totalSSI;
	}
	
	public CifraControl getDiferenciaWEBSSI(){
		
		if(this.diferenciaWEBSSI != null){
			return diferenciaWEBSSI;
		}
		
		CifraControl web = new CifraControl();
		CifraControl ssi = new CifraControl();
		
		for(CifraControl cc : this.detalle){
			if(cc.getTipo() == AppConstants.CC_TIPO_WEB){
				web.setSumaClabes(web.getSumaClabes() + cc.getSumaClabes());
				web.setSumaFolios(web.getSumaFolios() + cc.getSumaFolios());
				web.setSumaPorcentajes(web.getSumaPorcentajes() + cc.getSumaPorcentajes());
				web.setTotalAceptadas(web.getTotalAceptadas() + cc.getTotalAceptadas());
				web.setTotalSolicitudes(web.getTotalSolicitudes() + cc.getTotalSolicitudes());
			}else{
				if(cc.getTipo() == AppConstants.CC_TIPO_SSI_OK){
					ssi.setSumaClabes(ssi.getSumaClabes() + cc.getSumaClabes());
					ssi.setSumaFolios(ssi.getSumaFolios() + cc.getSumaFolios());
					ssi.setSumaPorcentajes(ssi.getSumaPorcentajes() + cc.getSumaPorcentajes());
					ssi.setTotalAceptadas(ssi.getTotalAceptadas() + cc.getTotalAceptadas());
					ssi.setTotalSolicitudes(ssi.getTotalSolicitudes() + cc.getTotalSolicitudes());
				}	
			}
		}
		
		diferenciaWEBSSI = new CifraControl();
		diferenciaWEBSSI.setSumaClabes(web.getSumaClabes() - ssi.getSumaClabes());
		diferenciaWEBSSI.setSumaFolios(web.getSumaFolios() - ssi.getSumaFolios());
		diferenciaWEBSSI.setSumaPorcentajes(web.getSumaPorcentajes() - ssi.getSumaPorcentajes());
		diferenciaWEBSSI.setTotalAceptadas(web.getTotalAceptadas() - ssi.getTotalAceptadas());
		diferenciaWEBSSI.setTotalSolicitudes(web.getTotalSolicitudes() - ssi.getTotalSolicitudes());
		
		return this.diferenciaWEBSSI;
		
	}
	
	
	/**
	 * 
	 * @return
	 */
	public CifraControl getComprobacionSSI() {
		
		if(comprobacionSSI != null){
			return comprobacionSSI;
		}
		
		CifraControl total = this.getTotalSSI();
		CifraControl web = new CifraControl();
		
		for(CifraControl cc : this.detalle){
			if(cc.getTipo() == AppConstants.CC_TIPO_WEB){
				web.setSumaClabes(web.getSumaClabes() + cc.getSumaClabes());
				web.setSumaFolios(web.getSumaFolios() + cc.getSumaFolios());
				web.setSumaPorcentajes(web.getSumaPorcentajes() + cc.getSumaPorcentajes());
				web.setTotalAceptadas(web.getTotalAceptadas() + cc.getTotalAceptadas());
				web.setTotalSolicitudes(web.getTotalSolicitudes() + cc.getTotalSolicitudes());
			}
		}
		
		if(web != null){
			comprobacionSSI = new CifraControl();
			comprobacionSSI.setSumaClabes(web.getSumaClabes() - total.getSumaClabes());
			comprobacionSSI.setSumaFolios(web.getSumaFolios() - total.getSumaFolios());
			comprobacionSSI.setSumaPorcentajes(web.getSumaPorcentajes()  - total.getSumaPorcentajes());
			comprobacionSSI.setTotalAceptadas(web.getTotalAceptadas() - total.getTotalAceptadas());
			comprobacionSSI.setTotalSolicitudes(web.getTotalSolicitudes() - total.getTotalSolicitudes());
		}
		
		return comprobacionSSI;
		
	}

		/**
	 * @return the detalle
	 */
	public List<CifraControl> getDetalle() {
		return detalle;
	}

	/**
	 * @param detalle the detalle to set
	 */
	public void setDetalle(List<CifraControl> detalle) {
		this.detalle = detalle;
	}

	/**
	 * @return the identificador
	 */
	public Integer getIdentificador() {
		return identificador;
	}

	/**
	 * @param identificador the identificador to set
	 */
	public void setIdentificador(Integer identificador) {
		this.identificador = identificador;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((identificador == null) ? 0 : identificador.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CifraControlTotal))
			return false;
		CifraControlTotal other = (CifraControlTotal) obj;
		if (identificador == null) {
			if (other.identificador != null)
				return false;
		} else if (!identificador.equals(other.identificador))
			return false;
		return true;
	}
	
	
	
	
	
}
