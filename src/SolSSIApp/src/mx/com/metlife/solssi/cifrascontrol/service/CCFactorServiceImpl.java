package mx.com.metlife.solssi.cifrascontrol.service;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFactor;
import mx.com.metlife.solssi.cifrascontrol.persistence.CCFactorDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CCFactorServiceImpl implements CCFactorService{

	@Autowired
	private CCFactorDAO dao; 
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly = false)
	public void save(CCFactor factor) {
		dao.save(factor);
	}

	@Override
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly = true)	
	public CCFactor get(Integer proceso) {
		
		return dao.get(proceso);

	}

}
