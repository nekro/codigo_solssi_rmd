package mx.com.metlife.solssi.cifrascontrol.domain;

import java.io.Serializable;

public class CCFilter implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8237856874955544441L;

	private int criterio;
	
	private Integer proceso;
	
	private Integer dependencia;
	
	private Integer grupo;
	
	private String fecha;

	/**
	 * @return the criterio
	 */
	public int getCriterio() {
		return criterio;
	}

	/**
	 * @param criterio the criterio to set
	 */
	public void setCriterio(int criterio) {
		this.criterio = criterio;
	}

	/**
	 * @return the proceso
	 */
	public Integer getProceso() {
		return proceso;
	}

	/**
	 * @param proceso the proceso to set
	 */
	public void setProceso(Integer proceso) {
		this.proceso = proceso;
	}

	/**
	 * @return the dependencia
	 */
	public Integer getDependencia() {
		return dependencia;
	}

	/**
	 * @param dependencia the dependencia to set
	 */
	public void setDependencia(Integer dependencia) {
		this.dependencia = dependencia;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the grupo
	 */
	public Integer getGrupo() {
		return grupo;
	}

	/**
	 * @param grupo the grupo to set
	 */
	public void setGrupo(Integer grupo) {
		this.grupo = grupo;
	}
	
	

}
