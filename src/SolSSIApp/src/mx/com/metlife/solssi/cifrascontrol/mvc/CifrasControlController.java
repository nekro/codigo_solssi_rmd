package mx.com.metlife.solssi.cifrascontrol.mvc;

import java.util.List;

import javax.servlet.http.HttpSession;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFilter;
import mx.com.metlife.solssi.cifrascontrol.domain.CifraControlTotal;
import mx.com.metlife.solssi.cifrascontrol.service.CifrasControlService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("cifrascontrol")
@SessionAttributes({"CCFilter"})
public class CifrasControlController {
	
	@Autowired
	private CifrasControlService cifrasControlService;
	
	@RequestMapping(method=RequestMethod.POST)
	public String showPage(Model model, HttpSession session){
		model.addAttribute(new CCFilter());
		model.addAttribute("action","filter");
		
		session.removeAttribute("cifras");
		session.removeAttribute("detalle");
		
		return "cifrascontrol/filter";
	}
	
	
	@RequestMapping(value="get", method=RequestMethod.POST)
	public String get(CCFilter filter, Model model, HttpSession session){
		
		CifraControlTotal cifras = this.cifrasControlService.getTotal(filter);
		
		session.removeAttribute("detalle");
		
		if(filter.getCriterio() != 1 ){
			List<CifraControlTotal> detalle =this.cifrasControlService.getDetalle(filter);
			session.setAttribute("detalle", detalle);
		}
		
		session.setAttribute("cifras", cifras);
		
		model.addAttribute("action","filter");
		
		return "cifrascontrol/filter";
	}
	
	
	
	
	

}
