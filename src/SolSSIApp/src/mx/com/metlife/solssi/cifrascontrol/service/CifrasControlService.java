package mx.com.metlife.solssi.cifrascontrol.service;

import java.util.List;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFilter;
import mx.com.metlife.solssi.cifrascontrol.domain.CifraControlTotal;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;

public interface CifrasControlService {
	
	public ControlCode createCifraControl(List<Solicitud> solicitudes);
	
	public ControlCode createCifraControlEsquemas(List<SolicitudEsquema> solicitudes);
	
	public CifraControlTotal getTotal(CCFilter filter);
	
	public List<CifraControlTotal> getDetalle(CCFilter filter);

}
