package mx.com.metlife.solssi.cifrascontrol.domain;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TW_CIFRAS_CONTROL")
public class CifraControl implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 914526797171930259L;

	@Id
	@Column(name = "FECHA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	@Column(name = "ID_PROCESO")
	private Integer proceso;
	
	@Column(name = "CVE_DEPENDENCIA")
	private Short dependencia;
	
	@Column(name = "CVE_SISTEMA")
	private Short sistema;
	
	@Column(name = "GRUPO")
	private Short grupo;
	
	@Column(name = "TIPO_REGISTRO")
	private Short tipo;
	
	@Column(name = "TOTAL_SOLICITUDES")
	private Integer totalSolicitudes = 0;
	
	@Column(name = "TOTAL_ACEPTADAS")
	private Integer totalAceptadas = 0;
	
	@Column(name = "SUMA_FOLIOS")
	private Long sumaFolios = 0l;
	
	@Column(name = "SUMA_PORCENTAJES")
	private Integer sumaPorcentajes = 0;
	
	@Column(name = "SUMA_CLABES")
	private Long sumaClabes = 0l;
	
	@Column(name = "SUMA_SALDOS")
	private Double sumaSaldos = 0d;
	
	@Column(name = "SUMA_IMPORTES")
	private Double sumaImportes = 0d;

	/**
	 * @return the fecha
	 * 
	 * 
	 * 
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the proceso
	 */
	public Integer getProceso() {
		return proceso;
	}

	/**
	 * @param proceso the proceso to set
	 */
	public void setProceso(Integer proceso) {
		this.proceso = proceso;
	}

	/**
	 * @return the dependencia
	 */
	public Short getDependencia() {
		return dependencia;
	}

	/**
	 * @param dependencia the dependencia to set
	 */
	public void setDependencia(Short dependencia) {
		this.dependencia = dependencia;
	}

	/**
	 * @return the sistema
	 */
	public Short getSistema() {
		return sistema;
	}

	/**
	 * @param sistema the sistema to set
	 */
	public void setSistema(Short sistema) {
		this.sistema = sistema;
	}

	/**
	 * @return the grupo
	 */
	public Short getGrupo() {
		return grupo;
	}

	/**
	 * @param grupo the grupo to set
	 */
	public void setGrupo(Short grupo) {
		this.grupo = grupo;
	}

	/**
	 * @return the tipo
	 */
	public Short getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(Short tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the totalSolicitudes
	 */
	public Integer getTotalSolicitudes() {
		return totalSolicitudes;
	}

	/**
	 * @param totalSolicitudes the totalSolicitudes to set
	 */
	public void setTotalSolicitudes(Integer totalSolicitudes) {
		this.totalSolicitudes = totalSolicitudes;
	}

	/**
	 * @return the totalAceptadas
	 */
	public Integer getTotalAceptadas() {
		return totalAceptadas;
	}

	/**
	 * @param totalAceptadas the totalAceptadas to set
	 */
	public void setTotalAceptadas(Integer totalAceptadas) {
		this.totalAceptadas = totalAceptadas;
	}

	/**
	 * @return the sumaFolios
	 */
	public Long getSumaFolios() {
		return sumaFolios;
	}

	/**
	 * @param sumaFolios the sumaFolios to set
	 */
	public void setSumaFolios(Long sumaFolios) {
		this.sumaFolios = sumaFolios;
	}

	/**
	 * @return the sumaPorcentajes
	 */
	public Integer getSumaPorcentajes() {
		return sumaPorcentajes;
	}

	/**
	 * @param sumaPorcentajes the sumaPorcentajes to set
	 */
	public void setSumaPorcentajes(Integer sumaPorcentajes) {
		this.sumaPorcentajes = sumaPorcentajes;
	}

	/**
	 * @return the sumaClabes
	 */
	public Long getSumaClabes() {
		return sumaClabes;
	}

	/**
	 * @param sumaClabes the sumaClabes to set
	 */
	public void setSumaClabes(Long sumaClabes) {
		this.sumaClabes = sumaClabes;
	}

	/**
	 * @return the sumaSaldos
	 */
	public Double getSumaSaldos() {
		return sumaSaldos;
	}

	/**
	 * @param sumaSaldos the sumaSaldos to set
	 */
	public void setSumaSaldos(Double sumaSaldos) {
		this.sumaSaldos = sumaSaldos;
	}

	/**
	 * @return the sumaImportes
	 */
	public Double getSumaImportes() {
		return sumaImportes;
	}

	/**
	 * @param sumaImportes the sumaImportes to set
	 */
	public void setSumaImportes(Double sumaImportes) {
		this.sumaImportes = sumaImportes;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getFmtSumaImportes(){
		if(this.sumaImportes != null){
			DecimalFormat df = new DecimalFormat("#,###,##0.00");
			return "$ "+df.format(this.sumaImportes.doubleValue());
		}else{
			return "$ 0.00";
		}
	}
	
	public String getFmtSumaSaldos(){
		if(this.sumaSaldos != null){
			DecimalFormat df = new DecimalFormat("#,###,##0.00");
			return "$ "+df.format(this.sumaSaldos.doubleValue());
		}else{
			return "$ 0.00";
		}
	}
	

}
