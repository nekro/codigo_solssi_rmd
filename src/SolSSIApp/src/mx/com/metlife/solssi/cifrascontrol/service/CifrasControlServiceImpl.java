package mx.com.metlife.solssi.cifrascontrol.service;

import java.util.List;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFactor;
import mx.com.metlife.solssi.cifrascontrol.domain.CCFilter;
import mx.com.metlife.solssi.cifrascontrol.domain.CifraControlTotal;
import mx.com.metlife.solssi.cifrascontrol.persistence.CifrasControlDAO;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.util.AppConstants;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CifrasControlServiceImpl implements CifrasControlService {
	
	@Autowired
	private CifrasControlDAO cifrasDAO;
	
	@Autowired
	private CCFactorService factorService;
	
	/**
	 * 
	 * @param solicitudes
	 * @return
	 */
	@Override
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public ControlCode createCifraControl(List<Solicitud> solicitudes){
		
		ControlCode control = new ControlCode();
		
		int totalRechazadas = 0;
		int totalAceptadas = 0;
		
		for(Solicitud sol : solicitudes){
			if(sol.getCveStatus() == AppConstants.ESTATUS_RECHAZADO || sol.getCveStatus() == AppConstants.ESTATUS_RECHAZADA_CADUCIDAD){
				totalRechazadas++;
			}else{
				if(sol.getCveStatus() != AppConstants.ESTATUS_CANCELADA){
					control.setTotalPorcentajes(control.getTotalPorcentajes()+sol.getPorcentaje());
					control.setSumaFolios(control.getSumaFolios()+Integer.valueOf(sol.getUserFolio().substring(2)));
					control.setSumaClabes(control.getSumaClabes() +  this.getSumDigits(sol.getClabe(), factorService.get(sol.getIdProceso())));
					totalAceptadas++;
				}
			}
		}
		
		control.setTotalSolicitudes(solicitudes.size());
		control.setTotalRechazadas(totalRechazadas);
		control.setTotalAceptadas(totalAceptadas);
		
		return control;
		
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public ControlCode createCifraControlEsquemas(List<SolicitudEsquema> solicitudes){	
		
		ControlCode control = new ControlCode();
		
		int totalRechazadas = 0;
		int totalAceptadas = 0;
		
		for(SolicitudEsquema sol : solicitudes){
			if(sol.getEstatus() == AppConstants.ESTATUS_RECHAZADO || sol.getEstatus() == AppConstants.ESTATUS_RECHAZADA_CADUCIDAD){
				totalRechazadas++;
			}else{
				if(sol.getEstatus() != AppConstants.ESTATUS_CANCELADA){
					control.setSumaFolios(control.getSumaFolios()+Integer.valueOf(sol.getUserFolio().substring(6)));
					control.setSumaClabes(control.getSumaClabes() +  sol.getEsquema());
					totalAceptadas++;
				}
			}
		}
		
		control.setTotalSolicitudes(solicitudes.size());
		control.setTotalRechazadas(totalRechazadas);
		control.setTotalAceptadas(totalAceptadas);
		
		
		
		return control;
		
	}
	
	
	
	private int getSumDigits(String clabe, CCFactor factor){
		
		
		if(StringUtils.isEmpty(clabe)){
			return 0;
		}
		
		int g1 = this.sumaDigitos(StringUtils.substring(clabe, 0, 3)) * factor.getX();
		int g2 = this.sumaDigitos(StringUtils.substring(clabe, 3, 6)) * factor.getX();
		int g3 = this.sumaDigitos(StringUtils.substring(clabe, 6, 9)) * factor.getX();
		int g4 = this.sumaDigitos(StringUtils.substring(clabe, 9, 12)) * factor.getX();
		int g5 = this.sumaDigitos(StringUtils.substring(clabe, 12, 15)) * factor.getX();
		int g6 = this.sumaDigitos(StringUtils.substring(clabe, 15, 18)) * factor.getX();
		
		int suma = g1 + g2 +g3 +g4 +g5 + g6; 
		
		return suma * factor.getY();
	}
	
	
	
	private int sumaDigitos(String number){
		
		int sum = 0;
		for(char a : number.toCharArray()){
			int num = Byte.valueOf(String.valueOf(a));
			sum += num;
		}
		
		return sum;
	}



	@Override
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public CifraControlTotal getTotal(CCFilter filter)  {

		return cifrasDAO.getTotal(filter);
	}



	@Override
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<CifraControlTotal> getDetalle(CCFilter filter) {
		
		return cifrasDAO.getDetalle(filter);
	}

}
