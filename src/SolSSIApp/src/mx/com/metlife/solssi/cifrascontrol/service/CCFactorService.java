package mx.com.metlife.solssi.cifrascontrol.service;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFactor;

public interface CCFactorService {
	
	public void save(CCFactor factor);
	
	public CCFactor get(Integer proceso);

}
