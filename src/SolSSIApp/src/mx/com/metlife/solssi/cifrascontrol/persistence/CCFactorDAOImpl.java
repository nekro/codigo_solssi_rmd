package mx.com.metlife.solssi.cifrascontrol.persistence;

import java.util.Map;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFactor;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.googlecode.ehcache.annotations.Cacheable;
import com.googlecode.ehcache.annotations.TriggersRemove;


@Repository
public class CCFactorDAOImpl implements CCFactorDAO{
	
	
	private static String key = "METLIFE_SSI_WEB_APP";
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	@TriggersRemove(cacheName = "procesosCache", removeAll = true)
	public void save(CCFactor factor) {
		
		Session session = this.sessionFactory.getCurrentSession();
		
		String sql = "insert into tc_factores_cc(value_x, value_y) values(encrypt(:x,:key),encrypt(:y,:key))";
		
		Query query = session.createSQLQuery(sql);
		query.setInteger("x", factor.getX());
		query.setInteger("y", factor.getY());
		query.setString("key", key);
		
		query.executeUpdate();
	}

	@Override
	@Cacheable(cacheName = "procesosCache")
	public CCFactor get(Integer proceso) {

		Session session = this.sessionFactory.getCurrentSession();
		
		String sql = "select id_version as version, decrypt_char(value_x,:key) as x, decrypt_char(value_y,:key) as y from tc_factores_cc where id_version = "
				+ "(select max(id_version) from tc_factores_cc)";
		
		if(proceso != null && proceso > 0){
			sql = "select id_version as version, decrypt_char(value_x,:key) as x, decrypt_char(value_y,:key) as y "
					+ "from tc_factores_cc where id_version = ("
					+ "select version_factores from tw_ctrl_archivo where id_proceso = :proceso)";
		}
		
		Query query = session.createSQLQuery(sql);
		
		query.setString("key", key);
		
		if(proceso != null && proceso > 0){
			query.setInteger("proceso", proceso);
		}
		
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		CCFactor factor = new CCFactor(); 
		Map map = (Map) query.uniqueResult();
		
		if(map == null){
			factor.setVersion(0);
			factor.setX(1);
			factor.setY(1);
			return factor;
		}
		
		factor.setVersion((Integer)map.get("VERSION"));
		factor.setX(Integer.valueOf((String)map.get("X")));
		factor.setY(Integer.valueOf((String)map.get("Y")));
		
		return factor;
	}
		

}
