package mx.com.metlife.solssi.security.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import mx.com.metlife.solssi.usuario.domain.UserPrincipal;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.util.PermisosMap;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("assembler")
public class Assembler {
	
		  
		@Transactional(readOnly = true)
		public User buildUserFromUserEntity(Usuario userEntity) {
			  
				String username = userEntity.getClave();
			    String password = userEntity.getClave();
			    boolean enabled = userEntity.isActivo();
			    boolean accountNonExpired = userEntity.isActivo();
			    boolean credentialsNonExpired = userEntity.isActivo();
			    boolean accountNonLocked = userEntity.isActivo();
			
			    Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			    
			    String permisos = userEntity.getPermisos();
			    Map<String,String> mapPermisos = PermisosMap.getPermisosMap();
			    int permiso = 1;
			    for(char a : permisos.toCharArray()){
			    	if(a == '1'){
			    		authorities.add(new GrantedAuthorityImpl(mapPermisos.get(String.valueOf(permiso))));		
			    	}
			    	permiso++;
			    }
			    
			    User user = new UserPrincipal(username, password, enabled,accountNonExpired, credentialsNonExpired, accountNonLocked, authorities, userEntity);
			    
			    return user;
		  }
}
