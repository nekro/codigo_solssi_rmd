package mx.com.metlife.solssi.security.provider;

import mx.com.iw.ldap.SUIDException;
import mx.com.metlife.solssi.param.service.ParametersService;
import mx.com.metlife.solssi.security.service.LdapAuthenticatorService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.authentication.dao.SaltSource;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class SolssiAuthorityProvider extends
		AbstractUserDetailsAuthenticationProvider {

	@Autowired
	private LdapAuthenticatorService ldapAuthenticatorService;
	
	@Autowired
	ParametersService parametersService;

	private PasswordEncoder passwordEncoder = new PlaintextPasswordEncoder();
	private SaltSource saltSource;
	private UserDetailsService userDetailsService;
	
	

	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {

		boolean bLDAP = false;

		try {
			bLDAP = ldapAuthenticatorService.validateUser(authentication.getName(),	(String) authentication.getCredentials());
		}catch(SUIDException side){
			throw new AuthenticationServiceException(this.getMessage(side));
		} catch (Exception e) {
			throw new AuthenticationServiceException(e.getMessage());
		}

		if (bLDAP) {
			try {
				UserDetails loadedUser = userDetailsService.loadUserByUsername(authentication.getName());
				return new UsernamePasswordAuthenticationToken(
						loadedUser,
						authentication.getCredentials(),
						loadedUser.getAuthorities());
			} catch (UsernameNotFoundException une) {
				throw new AuthenticationServiceException(une.getMessage());
			} catch (Exception e) {
				throw new AuthenticationServiceException("ERROR DE SISTEMA:" + e.getMessage());
			}
		} else {
			return null;
		}

	}

	public boolean supports(Class<? extends Object> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);

	}

	
	protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		Object salt = null;

		if (saltSource != null) {
			salt = saltSource.getSalt(userDetails);
		}

		if (!passwordEncoder.isPasswordValid(userDetails.getPassword(),	authentication.getCredentials().toString(), salt)) {
			throw new BadCredentialsException(messages.getMessage(
					"AbstractUserDetailsAuthenticationProvider.badCredentials",
					"Bad credentials"), userDetails);
		}
	}

	
	
	protected final UserDetails retrieveUser(String username,
			UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		UserDetails loadedUser = null;

		try {
			loadedUser = userDetailsService.loadUserByUsername(username);
		} catch (DataAccessException repositoryProblem) {
			throw new AuthenticationServiceException(
					repositoryProblem.getMessage(), repositoryProblem);
		}

		if (loadedUser == null) {
			throw new AuthenticationServiceException(
					"AuthenticationDao returned null, which is an interface contract violation");
		}

		return loadedUser;
	}
	
	
	
	/**
	 * obtiene el mensaje 
	 * @param e
	 * @return
	 */
	private String getMessage(SUIDException e){
		
		String code = e.getFaultCode();
		
		try{
		
			if(StringUtils.equals(code, "3000")){
				return "Error al conectarse al Servidor LDAP.";
			}
			
			if(StringUtils.equals(code, "3001")){
				return "El Servidor LDAP no se encontr�.";
			}
			
			if(StringUtils.equals(code, "3002")){
				return "ID de Usuario no v�lido.";
			}
			
			if(StringUtils.equals(code, "3003")){
				return "Contrase�a no v�lida.";
			}
			
			if(StringUtils.equals(code, "3005")){
				return "No se encontr� el Servidor LDAP.";
			}
			
			if(StringUtils.equals(code, "3011")){
				return "El Sistema encontr� un Error.";
			}
			
			if(StringUtils.equals(code, "3012")){
				return "La Contrase�a ha expirado.";
			}
			
			if(StringUtils.equals(code, "3013")){
				return "La Contrase�a nueva no puede ser la misma que alguna de las �ltimas 12 contrase�as que utiliz�.";
			}
			
			if(StringUtils.equals(code, "3014")){
				return "No cuenta con privilegios de Administrador.";
			}
			
			if(StringUtils.equals(code, "3015")){
				return "Criterios de b�squeda no v�lidos.";
			}
			
			if(StringUtils.equals(code, "3019")){
				return "La Contrase�a no puede ser igual al Nombre o Apellido";
			}
			
			if(StringUtils.equals(code, "3020")){
				return "El ID de Usuario se encuentra bloqueado.";
			}
			
			if(StringUtils.equals(code, "3031")){
				return "Ha introducido la contrase�a incorrecta 4 veces. Si captura la contrase�a mal nuevamente, el ID de Usuario se bloquear�.";
			}
			
			if(StringUtils.equals(code, "3033")){
				return "DN no v�lido";
			}
			
			if(StringUtils.equals(code, "3034")){
				return "Error al obtener los valores de los atributos de un usuario";
			}
			
		}catch(Exception ex){
			return e.getMessage();
		}
		
		return e.getMessage();
		
	}
	
}