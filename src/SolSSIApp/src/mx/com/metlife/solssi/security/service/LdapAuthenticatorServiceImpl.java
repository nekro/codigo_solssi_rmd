package mx.com.metlife.solssi.security.service;

import javax.naming.directory.DirContext;

import mx.com.iw.ldap.LDAPManager;
import mx.com.iw.ldap.Person;
import mx.com.iw.ldap.SUIDException;
import mx.com.metlife.solssi.param.service.ParametersService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LdapAuthenticatorServiceImpl implements LdapAuthenticatorService {

	private static Logger logger = Logger
			.getLogger(LdapAuthenticatorServiceImpl.class);

	@Autowired
	private ParametersService parametersService;

	public boolean validateUser(String user, String password)
			throws SUIDException {
		
		try {
			
			//if(true){
				//return true;
			//}
			
			if (StringUtils.isEmpty(user) || StringUtils.isEmpty(password)) {
				throw new SUIDException("0", "Capture Usuario y Password");
			} 
			
			String url = parametersService.getParamStringValue("LDAP_HOST");
			String hierarchy = parametersService.getParamStringValue("LDAP_DN");
			//String url = "ldap://10.170.47.54:389";
			//String hierarchy = "ou.people.o.affiliates.c.mexico.o.metlife.dc.metlife.dc.com";
			
			LDAPManager ldapmgr = new LDAPManager();
			Person p = new Person();
			p.setUserID(user);
			p.setPassword(password);
			p.setHierarchy(hierarchy);
			DirContext context = ldapmgr.authenticate(url, p);
			
			context.close();
			return true;

		} catch (Exception e) {
			if (e instanceof SUIDException) {
				SUIDException suie = ((SUIDException) e);
				logger.debug("fault code: "	+ suie.getFaultCode());
				throw (SUIDException) e;
			}
			return false;
		}
	}
}
