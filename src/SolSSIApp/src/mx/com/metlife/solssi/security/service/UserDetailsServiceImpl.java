package mx.com.metlife.solssi.security.service;

import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.usuario.service.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired 
	private UsuarioService usuarioService;
	
	@Autowired
	private Assembler assembler;

	@Transactional
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {

		
		Usuario userEntity = usuarioService.getUsuario(username);
		
		if (userEntity == null) {
			throw new UsernameNotFoundException(
					"El usuario no existe en el sistema");
		}

		if (!userEntity.isActivo()) {
			throw new UsernameNotFoundException("Cuenta de Usuario Inactiva");
		}

		return assembler.buildUserFromUserEntity(userEntity);
	}
	

}
