package mx.com.metlife.solssi.security.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class SessionValidFilter implements Filter {
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		
		if (request instanceof HttpServletRequest) {
			 
			  HttpServletRequest req = ((HttpServletRequest)request);
			 if(!req.isRequestedSessionIdValid()){
				 String url = req.getRequestURL().toString();
				 if( !(StringUtils.indexOf(url, "/clientes") > 0) && !(StringUtils.indexOf(url, "/operacion") >0) ){
					 request.getRequestDispatcher("/WEB-INF/jsp/login/session_expired.jsp").forward(request, response);
				 }	 
			 }
			 
			 
		}
		
		chain.doFilter(request, response);
	}
}
