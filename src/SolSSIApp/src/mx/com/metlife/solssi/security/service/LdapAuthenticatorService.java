package mx.com.metlife.solssi.security.service;

import mx.com.iw.ldap.SUIDException;

public interface LdapAuthenticatorService {

	public boolean validateUser(String user, String password) throws SUIDException;
	
}
