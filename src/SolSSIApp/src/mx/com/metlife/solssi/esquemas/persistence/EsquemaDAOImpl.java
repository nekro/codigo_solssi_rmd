package mx.com.metlife.solssi.esquemas.persistence;

import java.util.List;

import mx.com.metlife.solssi.esquemas.domain.AtributoEsquema;
import mx.com.metlife.solssi.esquemas.domain.Esquema;
import mx.com.metlife.solssi.esquemas.domain.Nota;
import mx.com.metlife.solssi.main.dao.GenericDAOImpl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class EsquemaDAOImpl extends GenericDAOImpl<Esquema, Integer> implements EsquemaDAO{

	@Override
	public void updateAtributosValue(Integer esquema, Integer atributo, String valor) {
		Session session = this.getSession();
		
		Query query = session.createSQLQuery("update tr_esquemas_atributos set valor = :valor where num_esquema = :esquema and id_atributo = :atributo");
		query.setInteger("esquema", esquema);
		query.setInteger("atributo", atributo);
		query.setString("valor", valor);
		query.executeUpdate();
		
	}
	
	
	@Override
	public void saveAtributosValue(Integer esquema, Integer atributo, String valor) {
		Session session = this.getSession();
		
		Query query = session.createSQLQuery("insert into tr_esquemas_atributos(num_esquema, id_atributo, valor) values (:esquema, :atributo, :valor)");
		query.setInteger("esquema", esquema);
		query.setInteger("atributo", atributo);
		query.setString("valor", valor);
		query.executeUpdate();
		
	}
	
	
	@Override
	public void updateEsquema(Esquema esquema) {
		Session session = this.getSession();
		
		Query query = session.createSQLQuery("update tc_esquemas set nombre = :nombre, cve_estatus = :estatus where num_esquema = :esquema");
		query.setInteger("esquema", esquema.getClave());
		query.setString("nombre", esquema.getNombre());
		query.setBoolean("estatus", esquema.isActivo());
		query.executeUpdate();
		
	}

	@Override
	public void saveNota(Nota nota) {
		Session session = this.getSession();
		session.save(nota);
		
	}

	@Override
	public void updateNota(Nota nota) {
		Session session = this.getSession();
		session.update(nota);
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Nota> getNotas() {
		Session session = this.getSession();
		Criteria crit = session.createCriteria(Nota.class);
		return crit.list();
	}

	@Override
	public Nota getNota(Integer num) {
		Session session = this.getSession();
		return (Nota) session.get(Nota.class, num);
	}
	
	public List<AtributoEsquema> getAtributosValues(Integer esquema){
		Session session = this.getSession();
		Criteria crit = session.createCriteria(AtributoEsquema.class);
		crit.add(Restrictions.eq("pk.numEsquema", esquema));
		return crit.list();
	}
	
	
}
