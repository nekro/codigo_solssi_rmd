package mx.com.metlife.solssi.esquemas.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import edu.emory.mathcs.backport.java.util.Collections;

@Entity
@Table(name="TC_ESQUEMAS")
public class Esquema implements Comparable<Esquema>, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3349008325512734127L;

	@Id
	@Column(name="NUM_ESQUEMA")
	@NotNull(message = "Capture el n�mero de esquema")
	private Integer clave;
	
	@Column(name="DESCRIPCION")
	@NotEmpty(message = "Capture un valor")
	private String nombre;
	
	@Column(name="TASA_TECNICA_MRG")
	@NotNull(message = "Capture un valor")
	private String tasaTecnicaMRG;
	
	@Column(name="TASA_RMG")
	@NotNull(message = "Capture un valor")
	private String tasaRMG;
	
	@Column(name="TASA_RME")
	@NotNull(message = "Capture un valor")
	private String tasaRME;
	
	@Column(name="TEXTO_DOC_CLAUSULA6", length = 1000)
	@NotEmpty(message = "Capture un valor")
	@Length(max=1000, message="Longitud de cadena no permitida")
	private String textoClausula6;
	
	@Column(name="TEXTO_DOC_CLAUSULA4", length = 1000)
	@Length(max=1000, message="Longitud de cadena no permitida")
	private String textoClausula4;
	
	@Column(name="TEXTO_SELECCION", length = 600)
	@Length(max=600, message="Longitud de cadena no permitida")
	private String textoSeleccion;
	
	@Column(name="NUM_POLIZA")
	@NotEmpty(message = "Capture un valor")
	private String poliza;
	
	@Column(name="ORDEN")
	private Integer orden;
	
	
	@Column(name="CVE_ESTATUS")
	private boolean activo;
	
	@Transient
	private List<AtributoEsquema> atributos;
	
	public Esquema(){
		this.atributos = new ArrayList<AtributoEsquema>();
	}

	/**
	 * @return the clave
	 */
	public Integer getClave() {
		return clave;
	}

	/**
	 * @param clave the clave to set
	 */
	public void setClave(Integer clave) {
		this.clave = clave;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return StringUtils.trim(nombre);
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the activo
	 */
	public boolean isActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	
	

	/**
	 * @return the atributos
	 */
	public List<AtributoEsquema> getAtributos() {
		Collections.sort(atributos);	
		return atributos;
	}
	
	
	/**
	 * @param atributos the atributos to set
	 */
	public void setAtributos(List<AtributoEsquema> atributos) {
		this.atributos = atributos;
	}
	
	
	/**
	 * @return the orden
	 */
	public Integer getOrden() {
		return orden;
	}

	/**
	 * @param orden the orden to set
	 */
	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Esquema [clave=");
		builder.append(clave);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", activo=");
		builder.append(activo);
		builder.append(", atributos=");
		builder.append(atributos);
		builder.append("]");
		return builder.toString();
	}
	
	

	
	/**
	 * @return the textoClausula6
	 */
	public String getTextoClausula6() {
		return textoClausula6;
	}

	/**
	 * @param textoClausula6 the textoClausula6 to set
	 */
	public void setTextoClausula6(String textoClausula6) {
		this.textoClausula6 = textoClausula6;
	}
	
	

	/**
	 * @return the poliza
	 */
	public String getPoliza() {
		return poliza;
	}

	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}

	@Override
	public int compareTo(Esquema o) {
		
		if(o == null){
			return 0;
		}
		
		if(o.getOrden() == null || this.orden == null){
			return 0;
		}
		
		return this.orden.compareTo(o.getOrden());
	}

	/**
	 * @return the tasaTecnica
	 */
	public String getTasaTecnicaMRG() {
		return StringUtils.trim(tasaTecnicaMRG);
	}

	/**
	 * @param tasaTecnica the tasaTecnica to set
	 */
	public void setTasaTecnicaMRG(String tasaTecnica) {
		this.tasaTecnicaMRG = tasaTecnica;
	}

	/**
	 * @return the tasaRMG
	 */
	public String getTasaRMG() {
		return StringUtils.trim(tasaRMG);
	}

	/**
	 * @param tasaRMG the tasaRMG to set
	 */
	public void setTasaRMG(String tasaRMG) {
		this.tasaRMG = tasaRMG;
	}

	/**
	 * @return the tasaRME
	 */
	public String getTasaRME() {
		return StringUtils.trim(tasaRME);
	}

	/**
	 * @param tasaRME the tasaRME to set
	 */
	public void setTasaRME(String tasaRME) {
		this.tasaRME = tasaRME;
	}

	/**
	 * @return the textoSeleccion
	 */
	public String getTextoSeleccion() {
		return StringUtils.trim(textoSeleccion);
	}

	/**
	 * @param textoSeleccion the textoSeleccion to set
	 */
	public void setTextoSeleccion(String textoSeleccion) {
		this.textoSeleccion = textoSeleccion;
	}

	/**
	 * @return the textoClausula4
	 */
	public String getTextoClausula4() {
		return textoClausula4;
	}

	/**
	 * @param textoClausula4 the textoClausula4 to set
	 */
	public void setTextoClausula4(String textoClausula4) {
		this.textoClausula4 = textoClausula4;
	}
	
	
	

}
