package mx.com.metlife.solssi.esquemas.mvc;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import mx.com.metlife.solssi.cifrascontrol.service.CifrasControlService;
import mx.com.metlife.solssi.esquemas.domain.ConsultaFilterEsquema;
import mx.com.metlife.solssi.esquemas.domain.OficioEsquema;
import mx.com.metlife.solssi.esquemas.domain.OficioFilterEsquema;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.esquemas.service.OficioEsquemaService;
import mx.com.metlife.solssi.esquemas.service.SolicitudEsquemaService;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.inicio.service.GruposCapturaService;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.param.service.ParametersService;
import mx.com.metlife.solssi.usuario.domain.UserPrincipal;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.BrowserUtils;
import mx.com.metlife.solssi.util.RangoCaptura;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("esquemas/oficio")
@SessionAttributes({"control", "oficioEsquema"})
public class OficioEsquemasController {
	
	@Autowired
	private SolicitudEsquemaService solicitudService;
	
	@Autowired
	private ParametersService params;
	
	@Autowired
	private OficioEsquemaService oficioService;
	
	@Autowired
	private GruposCapturaService gruposService;
	
	@Autowired
	private CifrasControlService ccService;
	
	@RequestMapping(value="generar" , method=RequestMethod.POST)
	public String showPage(HttpSession session, Model model, Authentication auth){
		
		model.addAttribute("command", "generar");
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		try {
			RangoCaptura rango = this.gruposService.checkInRangeGenerarOficiosEsquemas();
			OficioEsquema oficio = oficioService.existOficioDependencia(usuario.getDependencia(), rango.getFechaInicialOficios(), rango.getFechaFinalOficios()); 
			if(oficio != null){
				session.setAttribute("oficioEsquema", oficio);
				BrowserUtils.enableHistoryBack(model);
				return "esquemas/oficio/existente";
			}
		} catch (ApplicationException e) {
			model.addAttribute("error",e.getMessage());
			return "esquemas/oficio/result"; 
		}
		
		ConsultaFilterEsquema filter = new ConsultaFilterEsquema();
		filter.setEstatusList(AppConstants.ESTATUS_ACEPTADO, AppConstants.ESTATUS_RECHAZADO, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD);
		filter.setDependencia(usuario.getDependencia());
		filter.setOficio(-1);
		
		
		List<SolicitudEsquema> list = solicitudService.get(filter);
		model.addAttribute("solicitudesOficio", list);
		
		ControlCode control = ccService.createCifraControlEsquemas(list);
		model.addAttribute("control", control);
		
		BrowserUtils.enableHistoryBack(model);

		return "esquemas/oficio/genera";
	}
	
	
	@RequestMapping(value="generar/execute" , method=RequestMethod.POST)
	public String generarOficio(HttpSession session, Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		
		
		ConsultaFilterEsquema filter = new ConsultaFilterEsquema();
		filter.setEstatus(AppConstants.ESTATUS_CAPTURA);
		filter.setDependencia(usuario.getDependencia());
		filter.setOficio(-1);
		
		List<SolicitudEsquema> list = solicitudService.get(filter);
		
		if(list != null & list.size() > 0){
			model.addAttribute("solicitudesPendientes", list);
			return "esquemas/oficio/no_revisadas";
		}
		
		filter = new ConsultaFilterEsquema();
		filter.setEstatusList(AppConstants.ESTATUS_ACEPTADO, AppConstants.ESTATUS_RECHAZADO, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD);
		filter.setDependencia(usuario.getDependencia());
		filter.setOficio(-1);
		
		List<SolicitudEsquema> solicitudes = solicitudService.get(filter);
		
		if(solicitudes == null || solicitudes.isEmpty()){
			model.addAttribute("error", "No existen Documentos de Selecci�n para generar el oficio");
			model.addAttribute("command", "generar");
			return "esquemas/oficio/result";
		}
		
		OficioEsquema oficio;
		try {
			ControlCode control = ccService.createCifraControlEsquemas(solicitudes);
			oficio = oficioService.generaOficioEsquemas(solicitudes, control, usuario);
			
			model.addAttribute("control",control);
		} catch (ApplicationException e) {
			model.addAttribute("error", e.getMessage());
			return "esquemas/oficio/result";	
		}
		
		model.addAttribute(oficio);
		
		model.addAttribute("command", "generar");
		
		return "esquemas/oficio/result";
	}
	
	
	@RequestMapping(value="consulta" , method=RequestMethod.POST)
	public String showConsulta(Model model, HttpSession session){
		OficioFilterEsquema filter = new OficioFilterEsquema();
		model.addAttribute(filter);
		
		session.removeAttribute("oficios");
		
		return "esquemas/oficio/aceptar";
	}
	
	
	@RequestMapping(value="consulta/execute" , method=RequestMethod.POST)
	public String showConsulta(@Valid OficioFilterEsquema filter, BindingResult errors, Model model, Authentication auth){
		
		model.addAttribute(filter);
		
		List<OficioEsquema> oficios = oficioService.getOficios(filter);
		
		if(oficios == null || oficios.isEmpty()){
			errors.reject("NotEmpty.oficioFilter.result");
		}
		
		model.addAttribute("oficios", oficios);
		
		BrowserUtils.enableHistoryBack(model);
		
		return "esquemas/oficio/aceptar";
	}
	
	
	@RequestMapping(value="detalle" , method=RequestMethod.POST)
	public String showOficio(@RequestParam("oficio") Integer numOficio, Model model){
		ConsultaFilterEsquema filtro = new ConsultaFilterEsquema();
		filtro.setOficio(numOficio);
		
		List<SolicitudEsquema> list = solicitudService.get(filtro);
		
		OficioEsquema oficio = oficioService.getOficio(numOficio);
		
		ControlCode control = ccService.createCifraControlEsquemas(list);

		model.addAttribute("solicitudes", list);
		model.addAttribute("oficio", oficio);
		model.addAttribute("control", control);
		BrowserUtils.enableHistoryBack(model);
		return "esquemas/oficio/detalle";
	}
	
	@RequestMapping(value="save/caducadas" , method=RequestMethod.POST)
	public String guardarCaducadas(@RequestParam("solicitudes") Integer folios[], Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		solicitudService.saveAsCaducadas(folios, usuario.getClave());
		
		model.addAttribute("command", "caducidad");
		
		return "esquemas/oficio/result";
	}
	
	
	@RequestMapping(value="aceptar/execute" , method=RequestMethod.POST)
	public String aceptarOficios(@RequestParam("oficios") Integer oficios[], Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		try {
			oficioService.aceptarOficios(oficios, usuario);
		} catch (ApplicationException e) {
			model.addAttribute("error", e.getMessage());
		}
		
		return "esquemas/oficio/result";
	}
	
	
	@RequestMapping(value="enviar" , method=RequestMethod.POST)
	public String enviarOficiosList(Model model, HttpSession session){
		OficioFilterEsquema filter = new OficioFilterEsquema();
		filter.setEstatus(AppConstants.OFICIO_ESTATUS_ACEPTADO);
		
		RangoCaptura rango = gruposService.getRangoFechaCapturaEsquemas();
		filter.setFecha(rango.getFechaInicialOficios());
		
		List<OficioEsquema> oficios = oficioService.getOficios(filter);
		
		session.setAttribute("oficios", oficios);
		
		BrowserUtils.enableHistoryBack(model);
		
		return "esquemas/oficio/enviar";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="enviar/execute" , method=RequestMethod.POST)
	public String enviarOficios(Model model, 
								@RequestParam(value = "automaticos", required = false) Integer automaticos[],
								@RequestParam(value = "prestamos", required = false) Integer prestamos[],										
								@RequestParam(value = "cheques", required = false) Integer cheques[],
								@RequestParam(value = "pagomaticos", required = false) Integer pagomaticos[],
								@RequestParam(value = "juridicos", required = false) Integer juridicos[],			
																				HttpSession session, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		List<OficioEsquema> oficios = (List<OficioEsquema>) session.getAttribute("oficios");
	
		
		for(OficioEsquema oficio: oficios){
			
			if(oficio.getBndAutomatico() != AppConstants.OFICIO_BND_PROCESADO){
				oficio.setBndAutomatico(this.calculateValueBnd(oficio.getNumOficio(), automaticos));
			}
			
		}
		
		oficioService.marcarOficios(oficios, usuario.getClave());
		
		model.addAttribute("command", "enviar");
		
		return "esquemas/oficio/result";
	}
	
	private int calculateValueBnd(Integer oficio, Integer array[]){
		
		if(array == null){
			return AppConstants.OFICIO_BND_SIN_ENVIAR;
		}

		for(Integer of : array){
			if(oficio.intValue() == of.intValue()){
				return AppConstants.OFICIO_BND_ENVIADO;
			}
		}
		return AppConstants.OFICIO_BND_SIN_ENVIAR;
	}
	
	
	
	@RequestMapping(value = "view/solicitud", method=RequestMethod.POST)
	public String showSolicitud(@RequestParam("numFolio") Integer folio, Model model){
		
		SolicitudEsquema solicitud = solicitudService.get(folio);
		model.addAttribute("solicitud", solicitud);
		model.addAttribute("urlRegresar", "/oficio/generar");
		
		return "esquemas/consulta/solicitud";
	}
	
	@RequestMapping(value = "oficio/frames", method=RequestMethod.POST)
	public String oficio(Model model){
		return "esquemas/oficio/frames";
	}
	
	@RequestMapping(value = "oficio/menu")
	public String oficioMenu(Model model){
		return "esquemas/oficio/menu";
	}
	
	@RequestMapping(value="reimpresion" , method=RequestMethod.POST)
	public String showConsulta(Model model, HttpSession session, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		OficioEsquema oficio = (OficioEsquema) session.getAttribute("oficioEsquema");
		
		ConsultaFilterEsquema filter = new ConsultaFilterEsquema();
		filter.setEstatusList(AppConstants.ESTATUS_ACEPTADO, AppConstants.ESTATUS_RECHAZADO, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD);
		filter.setDependencia(usuario.getDependencia());
		filter.setOficio(oficio.getNumOficio());
		List<SolicitudEsquema> list = solicitudService.get(filter);
		
		ControlCode control = ccService.createCifraControlEsquemas(list);
		
		session.setAttribute("control", control);
		
		return "esquemas/oficio/frames";
	}
	
}
