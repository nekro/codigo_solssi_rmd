package mx.com.metlife.solssi.esquemas.service;

import java.util.Calendar;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.ControlSolicitud;
import mx.com.metlife.solssi.auditoria.domain.ControlSolicitudEsquema;
import mx.com.metlife.solssi.auditoria.persistence.ControlSolicitudDAO;
import mx.com.metlife.solssi.consulta.domain.ReporteTotales;
import mx.com.metlife.solssi.esquemas.domain.ConsultaFilterEsquema;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.esquemas.persistence.SolicitudEsquemaDAO;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
public class SolicitudEsquemaServiceImpl implements SolicitudEsquemaService{

	@Autowired
	private SolicitudEsquemaDAO solicitudDAO;
	
	@Autowired
	private ControlSolicitudDAO controlDAO;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public void save(SolicitudEsquema solicitud) {
		
		solicitud.setFechaCaptura(Calendar.getInstance().getTime());
		solicitud.setEstatus(AppConstants.ESTATUS_CAPTURA);
		solicitud.setAvisoPrivacidad(true);
		
		ConsultaFilterEsquema filter = new ConsultaFilterEsquema();
		filter.setRfc(solicitud.getRfc());
		filter.setCuenta(String.valueOf(solicitud.getCuenta()));
		filter.setEstatus(AppConstants.ESTATUS_CAPTURA);
		List<SolicitudEsquema> solicitudes = solicitudDAO.get(filter);
		
		for(SolicitudEsquema sol : solicitudes){
		    solicitudDAO.updateEstatus(sol.getNumFolio(),AppConstants.ESTATUS_CANCELADA, AppConstants.ESTATUS_CAPTURA, solicitud.getRfc());
		    ControlSolicitudEsquema ctrl = this.createControlRegister(sol.getNumFolio(), AppConstants.ESTATUS_CANCELADA);
		    controlDAO.save(ctrl);
		}
		
		solicitudDAO.save(solicitud);
		
		ControlSolicitudEsquema control = this.createControlRegister(solicitud.getNumFolio(), AppConstants.ESTATUS_CAPTURA);
		controlDAO.save(control);
	}
	
	
	@Override
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<SolicitudEsquema> get(ConsultaFilterEsquema filtro){
		return solicitudDAO.get(filtro);
	}


	@Override
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public SolicitudEsquema get(Integer numFolio) {
		SolicitudEsquema s =  solicitudDAO.getById(numFolio);
		s.fillTrasientFields();
		return s;
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public void updateEstatusAutorizacion(SolicitudEsquema solicitud,
			Short estatus, String userAutorizador) throws ApplicationException {
		
		if(estatus == AppConstants.ESTATUS_ACEPTADO){
			ConsultaFilterEsquema filter = new ConsultaFilterEsquema();
			filter.setCuenta(String.valueOf(solicitud.getCuenta()));
			filter.setRfc(solicitud.getRfc());
			filter.setEstatus(AppConstants.ESTATUS_ACEPTADO);
			List list = this.get(filter);
			
			if(list.size()>0){
				throw new ApplicationException(MessageUtils.getMessage("duplicate.numero.autorizaciones"));
			}	
		}
		
		
		int results = solicitudDAO.updateEstatus(solicitud.getNumFolio(), estatus, AppConstants.ESTATUS_CAPTURA, userAutorizador);
		
		if(results == 0){
			throw new ApplicationException(MessageUtils.getMessage("autorizacion.not.rows.affected"));
		}
		
		ControlSolicitudEsquema control = new ControlSolicitudEsquema();
		control.setNumFolio(solicitud.getNumFolio());
		control.setEstatus(estatus);
		control.setUsuario(userAutorizador);
		control.setFecha(Calendar.getInstance().getTime());
		
		controlDAO.save(control);
		
	}
	
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public void saveAsCaducadas(Integer[] numFolios, String user){
		for(Integer folio : numFolios){
			solicitudDAO.updateEstatus(folio, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD, AppConstants.ESTATUS_CAPTURA, user);
			
			ControlSolicitudEsquema control = new ControlSolicitudEsquema();
			control.setNumFolio(folio);
			control.setEstatus(AppConstants.ESTATUS_RECHAZADA_CADUCIDAD);
			control.setUsuario(user);
			control.setFecha(Calendar.getInstance().getTime());
			
			controlDAO.save(control);
			
		}
	}
	
	private ControlSolicitudEsquema createControlRegister(Integer folio, Short estatus){
		ControlSolicitudEsquema control = new ControlSolicitudEsquema();
		control.setNumFolio(folio);
		control.setEstatus(estatus);
		control.setUsuario("CLIENTE");
		
		control.setFecha(Calendar.getInstance().getTime());
		return control;
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public void updateNivelPuesto(Integer folio, String nivelPuesto) {
		solicitudDAO.updateNivelPuesto(folio, nivelPuesto);
	}
	
	
	@Override
	@PreAuthorize("hasRole('REPORTE_TOTALES')")
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public ReporteTotales getTotales(Short dependencia) {
		return solicitudDAO.getTotales(dependencia);
	}


}
