package mx.com.metlife.solssi.esquemas.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

@Embeddable
public class AtributoEsquemaPK implements Serializable{

	private static final long serialVersionUID = -1639005477535730228L;
	
	private int numEsquema;
	
	private int numAtributo;
	
	private Esquema esquema;
	
	private Atributo atributo;

	/**
	 * @return the esquema
	 */
	@Transient
	public Esquema getEsquema() {
		return esquema;
	}

	/**
	 * @param esquema the esquema to set
	 */
	public void setEsquema(Esquema esquema) {
		this.esquema = esquema;
	}

	/**
	 * @return the atributo
	 */
	@Transient
	public Atributo getAtributo() {
		return atributo;
	}

	/**
	 * @param atributo the atributo to set
	 */
	public void setAtributo(Atributo atributo) {
		this.atributo = atributo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((atributo == null) ? 0 : atributo.hashCode());
		result = prime * result + ((esquema == null) ? 0 : esquema.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AtributoEsquemaPK))
			return false;
		AtributoEsquemaPK other = (AtributoEsquemaPK) obj;
		if (atributo == null) {
			if (other.atributo != null)
				return false;
		} else if (!atributo.equals(other.atributo))
			return false;
		if (esquema == null) {
			if (other.esquema != null)
				return false;
		} else if (!esquema.equals(other.esquema))
			return false;
		return true;
	}

	/**
	 * @return the numEsquema
	 */
	@Column( name = "NUM_ESQUEMA")
	public int getNumEsquema() {
		return numEsquema;
	}

	/**
	 * @param numEsquema the numEsquema to set
	 */
	public void setNumEsquema(int numEsquema) {
		this.numEsquema = numEsquema;
	}

	/**
	 * @return the numAtributo
	 */
	@Column( name = "ID_ATRIBUTO")
	public int getNumAtributo() {
		return numAtributo;
	}

	/**
	 * @param numAtributo the numAtributo to set
	 */
	public void setNumAtributo(int numAtributo) {
		this.numAtributo = numAtributo;
	}
	
	
 
	
			
}
