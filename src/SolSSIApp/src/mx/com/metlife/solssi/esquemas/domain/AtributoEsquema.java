package mx.com.metlife.solssi.esquemas.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.util.StringUtils;

@Entity
@Table(name="TR_ESQUEMAS_ATRIBUTOS")
public class AtributoEsquema implements Comparable<AtributoEsquema>, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8481907959692928496L;

	private AtributoEsquemaPK pk = new AtributoEsquemaPK();
	
	private String valor;

	
	@Transient
	public Atributo getAtributo(){
		return pk.getAtributo();
	}
	
	@Transient
	public String getNombre(){
		return pk.getAtributo().getNombre();
	}
	
	@Transient
	public Esquema getEsquema(){
		return pk.getEsquema();
	}
	
	public void setEsquema(Esquema esquema){
		this.getPk().setEsquema(esquema);
	}
	
	public void setAtributo(Atributo atributo){
		this.getPk().setAtributo(atributo);
	}
	
	/**
	 * @return the getValor
	 */
	@Column(name = "VALOR")
	public String getValor() {
		return StringUtils.trimWhitespace(valor);
	}

	/**
	 * @param getValor the getValor to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof AtributoEsquema))
			return false;
		AtributoEsquema other = (AtributoEsquema) obj;
		if (pk == null) {
			if (other.pk != null)
				return false;
		} else if (!pk.equals(other.pk))
			return false;
		return true;
	}

	/**
	 * @return the pk
	 */
	@EmbeddedId
	public AtributoEsquemaPK getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(AtributoEsquemaPK pk) {
		this.pk = pk;
	}
	
	public int compareTo(AtributoEsquema o) {
		if(this.getPk() == null || o.getPk() == null){
			return 0;
		}
		
		if(this.getPk().getAtributo() == null || o.getPk().getAtributo() == null){
			return 0;
		}
		
		if(this.getPk().getAtributo().getOrden() == null || o.getPk().getAtributo().getOrden() == null){
			return 0;
		}
		
		return this.getPk().getAtributo().getOrden().compareTo(o.getPk().getAtributo().getOrden());
	}

	
}
