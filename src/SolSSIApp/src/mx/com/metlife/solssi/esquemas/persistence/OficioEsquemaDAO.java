package mx.com.metlife.solssi.esquemas.persistence;

import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.esquemas.domain.OficioEsquema;
import mx.com.metlife.solssi.esquemas.domain.OficioFilterEsquema;
import mx.com.metlife.solssi.main.dao.GenericDAO;

public interface OficioEsquemaDAO extends GenericDAO<OficioEsquema, Integer>{
	
	public int updateNumOficioSolicitud(Integer numFolio, Integer numOficio);
	
	public List<OficioEsquema> getOficios(OficioFilterEsquema filter);
	
	public int enviarOficio(Integer oficio, String user);
	
	public int aceptarOficio(Integer oficio, String user);
	
	public OficioEsquema existOficioDependencia(Short dependencia, Date ini, Date fin);
	
	public void marcarOficios(List<OficioEsquema> oficios, String user);
	
}
