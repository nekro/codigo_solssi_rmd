package mx.com.metlife.solssi.esquemas.persistence;

import java.util.List;

import mx.com.metlife.solssi.esquemas.domain.Atributo;
import mx.com.metlife.solssi.esquemas.domain.Esquema;
import mx.com.metlife.solssi.main.dao.GenericDAO;

public interface AtributoDAO extends GenericDAO<Atributo, Integer>{

	public void save(Atributo atributo, List<Esquema> esquemas);
	
	public List<Atributo> getAll();
	
}
