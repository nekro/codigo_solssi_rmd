package mx.com.metlife.solssi.esquemas.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.service.CatalogoService;
import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.esquemas.domain.ConsultaFilterEsquema;
import mx.com.metlife.solssi.esquemas.domain.Esquema;
import mx.com.metlife.solssi.esquemas.domain.OficioEsquema;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.inicio.service.GruposCapturaService;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.oficio.domain.Oficio;
import mx.com.metlife.solssi.report.service.ExporterService;
import mx.com.metlife.solssi.report.service.TokenService;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.RangoCaptura;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

@Service
public class PDFEsquemasService {
	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private SolicitudEsquemaService solicitudService;
	
	@Autowired
	private CatalogoService catalogoService;

	@Autowired
	private ExporterService exporterService;
	
	@Autowired
	private EsquemaService esquemaService;
	
	@Autowired
	private GruposCapturaService gruposService;
	
	private JasperReport jrSolicitudEsquema;
	private JasperReport jrOficioEsquema;
	
	public JasperReport getJrOficioEsquema() {
		if(this.jrOficioEsquema == null){
			this.jrOficioEsquema = this.loadReport("reportes/Oficio_Esquema.jasper");
		}
		return jrOficioEsquema;
	}

	public JasperReport getJrSolicitudEsquema() {
		if(this.jrSolicitudEsquema == null){
			this.jrSolicitudEsquema = this.loadReport("reportes/Solicitud_Esquema.jasper");
		}
		return jrSolicitudEsquema;
	}
	
	@PostConstruct
	private void loadReports(){
		
		this.jrSolicitudEsquema = this.loadReport("reportes/Solicitud_Esquema.jasper");
		this.jrOficioEsquema = this.loadReport("reportes/Oficio_Esquema.jasper");
		
   }
	
	private JasperReport loadReport(String name){
	    try{
	    	ClassPathResource classpathresource = null;
	    	InputStream inputstream = null;
	    	classpathresource = new ClassPathResource(name);
	    	inputstream = classpathresource.getInputStream();
	    	JasperReport jr = (JasperReport) JRLoader.loadObject(inputstream);
	    	return jr;
	    }catch(Exception e){
	    	e.printStackTrace();
	    	return null;
	    }
   }
	
	
	
	/**
	* Writes the report to the output stream
	*/
	public void write(String token, HttpServletResponse response, ByteArrayOutputStream baos) {
		 
		try {
			// Retrieve output stream
			ServletOutputStream outputStream = response.getOutputStream();
			// Write to output stream
			baos.writeTo(outputStream);
			// Flush the stream
			outputStream.flush();
			
			// Remove download token
			tokenService.remove(token);
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	
	public void fillReport(String documentType, HttpServletResponse response, SolicitudEsquema s) throws IOException{
		try {
			// 1. Add report parameters
			HashMap<String, Object> params = new HashMap<String, Object>();
			// params.put("Title", "User Report");
			// params.put("datasource", new JREmptyDataSource());

			JasperPrint jasperprint = null;

			List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
			params = this.fillPdfSolicitudEsquema(s, params, 1);
			jasperprint = JasperFillManager.fillReport(this.getJrSolicitudEsquema(),params, new JREmptyDataSource());
			jprintlist.add(jasperprint);
				    
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			StringBuilder fileName = new StringBuilder();
			fileName.append("Solicitud_Esquema");
			fileName.append(s.getUserFolio());
			fileName.append(".pdf");
			
			exporterService.export(fileName.toString(), documentType, jprintlist,response, baos);

			this.write(tokenService.generate(), response, baos);

		} catch (JRException jre) {
			throw new RuntimeException(jre);
		}

	}

	
	private HashMap<String, Object> fillPdfSolicitudEsquema(SolicitudEsquema s,
			HashMap<String, Object> parameterMap, int page) {
		String value = "";
		Object obj = null;
		
			obj = s.getfmtFechaCaptura();
			if (obj==null) { value = getEmptyStringContainer(8);} else { value = obj.toString();}
			if (value.equals("")) { value = getEmptyStringContainer(6); }
			parameterMap.put("DIA1_SOL", String.valueOf(value.charAt(0)));
			parameterMap.put("DIA2_SOL", String.valueOf(value.charAt(1)));
			parameterMap.put("MES1_SOL", String.valueOf(value.charAt(2)));
			parameterMap.put("MES2_SOL", String.valueOf(value.charAt(3)));
			parameterMap.put("AA1_SOL", String.valueOf(value.charAt(4)));
			parameterMap.put("AA2_SOL", String.valueOf(value.charAt(5)));
			parameterMap.put("AA3_SOL", String.valueOf(value.charAt(6)));
			parameterMap.put("AA4_SOL", String.valueOf(value.charAt(7)));
	
			parameterMap.put("APATERNO", s.getApePaterno().toUpperCase());
			parameterMap.put("AMATERNO", s.getApeMaterno().toUpperCase());
			parameterMap.put("NOMBRE", s.getNombre().toUpperCase());
			parameterMap.put("NUM_CUENTA", String.valueOf(s.getCuenta()));
			
			parameterMap.put("LUGAR_ELABORA", s.getCiudadElaboracion() + ", " + catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_ESTADOS, s.getEstadoElaboracion()));
	
			obj = s.getRfc();
			if (obj==null) { value = getEmptyStringContainer(10);} else { value = obj.toString();}
			if (value.equals("")) { value = getEmptyStringContainer(10); }
			parameterMap.put("LETR1_RFC", String.valueOf(value.charAt(0)));
			parameterMap.put("LETR2_RFC", String.valueOf(value.charAt(1)));
			parameterMap.put("LETR3_RFC", String.valueOf(value.charAt(2)));
			parameterMap.put("LETR4_RFC", String.valueOf(value.charAt(3)));
			parameterMap.put("AA1_RFC", String.valueOf(value.charAt(4)));
			parameterMap.put("AA2_RFC", String.valueOf(value.charAt(5)));
			parameterMap.put("MES1_RFC", String.valueOf(value.charAt(6)));
			parameterMap.put("MES2_RFC", String.valueOf(value.charAt(7)));
			parameterMap.put("DIA1_RFC", String.valueOf(value.charAt(8)));
			parameterMap.put("DIA2_RFC", String.valueOf(value.charAt(9)));
	
			obj = s.getHomoclave();
			if (obj==null) { value = getEmptyStringContainer(3);} else { value = obj.toString().toUpperCase();}
			if (value.equals("")) { value = getEmptyStringContainer(3); }
			parameterMap.put("CHR1_HOMCVE", String.valueOf(value.charAt(0)));
			parameterMap.put("CHR2_HOMCVE", String.valueOf(value.charAt(1)));
			parameterMap.put("CHR3_HOMCVE", String.valueOf(value.charAt(2)));
	
			obj = s.getAnioNacimiento().substring(2);
			if (obj==null) { value = getEmptyStringContainer(2);} else { value = obj.toString();}
			value = s.getDiaNacimiento() + s.getMesNacimiento() + value ;
			if (value.equals("")) { value = getEmptyStringContainer(6); }
			parameterMap.put("DIA1_NAC", String.valueOf(value.charAt(0)));
			parameterMap.put("DIA2_NAC", String.valueOf(value.charAt(1)));
			parameterMap.put("MES1_NAC", String.valueOf(value.charAt(2)));
			parameterMap.put("MES2_NAC", String.valueOf(value.charAt(3)));
			parameterMap.put("AA1_NAC", String.valueOf(value.charAt(4)));
			parameterMap.put("AA2_NAC", String.valueOf(value.charAt(5)));
			
			parameterMap.put("NUM_IDENTIFICA", s.getNumeroIdentificacion());
	
			parameterMap.put("CHECK_IFE", (s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_IFE) ? "X" : "");
			parameterMap.put("CHECK_PASAPORTE",	(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_PASAPORTE) ? "X": "");
			parameterMap.put("CHECK_ADULTOS",(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_ADULTOS) ? "X": "");
			parameterMap.put("CHECK_CED_PROF",(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_CED_PROF) ? "X": "");
			parameterMap.put("CHECK_MATR_CONS",	(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_MATR_CONS) ? "X": "");
			parameterMap.put("CHECK_MIGRAT",(s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_FM2 || s.getCveTipoIdentificacion() == CatalogoConstants.IDENT_FM3) ? "X": "");
			parameterMap.put("CORREO_PERSONAL", s.getCorreoPersonal());
			parameterMap.put("CORREO_TRABAJO", s.getCorreoTrabajo());
	
			parameterMap.put("CODIGO_CONTROL1", s.getRfc());
			parameterMap.put("CODIGO_CONTROL2", s.getUserFolio());
			
			Esquema esquema = esquemaService.getEsquemaFromMemory(s.getEsquema());
			
			parameterMap.put("ESQUEMA", esquema.getNombre());
			parameterMap.put("TASA_RME", esquema.getTasaRME());
			parameterMap.put("TASA_RMG", esquema.getTasaRMG());
			parameterMap.put("TASA_TECNICA", esquema.getTasaTecnicaMRG());
			parameterMap.put("CLAUSULA_CUARTA", esquema.getTextoClausula4());
			parameterMap.put("CLAUSULA_SEXTA", esquema.getTextoClausula6());
			
			parameterMap.put("FECHA_ESQUEMA", gruposService.getRangoFechaCapturaEsquemas().getFmtFechaAplicacion()); 
			
			parameterMap.put("NUM_POLIZA", s.getPoliza());
			parameterMap.put("NUM_FOLIO_SOL",String.valueOf(s.getUserFolio()));
			
			parameterMap.put("NUM_FORMA", catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_PARAMETROS_NEGOCIO,"PNUMFORMA2"));
			
			parameterMap.put("DOC_REG1", catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_PARAMETROS_NEGOCIO,"DOC_REG1"));
			//parameterMap.put("DOC_REG2", catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_PARAMETROS_NEGOCIO,"DOC_REG2"));
			parameterMap.put("DOC_FEC1", catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_PARAMETROS_NEGOCIO,"DOC_FEC1"));
			//parameterMap.put("DOC_FEC2", catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_PARAMETROS_NEGOCIO,"DOC_FEC2"));
			
			
			return parameterMap;
	}
	
	
	public void fillReportOficio(String documentType, HttpServletResponse response, Cliente cliente, HttpSession session) throws IOException{
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			JasperPrint jasperprint = null;
			List<JasperPrint> jprintlist = new ArrayList<JasperPrint>();
		    List<SolicitudEsquema> listSatisfactorias = new ArrayList<SolicitudEsquema>();
		    List<SolicitudEsquema> listNoSatisfactorias = new ArrayList<SolicitudEsquema>();
		    ConsultaFilterEsquema filtro = new ConsultaFilterEsquema();
		    OficioEsquema oficio = (OficioEsquema) session.getAttribute("oficioEsquema");
		    ControlCode control = (ControlCode) session.getAttribute("control");
		    
		    filtro.setOficio(oficio.getNumOficio());
		    filtro.setEstatus(AppConstants.ESTATUS_ACEPTADO);
		    
		    listSatisfactorias = solicitudService.get(filtro);	
		    
		    for(SolicitudEsquema se : listSatisfactorias){
		    	se.setNombreEsquema(esquemaService.getEsquemaFromMemory(se.getEsquema()).getNombre());
		    	//System.out.println("###"+se.getNombreEsquema());
		    }
		    
		    
		    filtro.setEstatus(null);
		    filtro.setEstatusList(AppConstants.ESTATUS_RECHAZADO, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD);
		    listNoSatisfactorias = solicitudService.get(filtro);    
		    
		    RangoCaptura rango = null;
		    try {
				rango = this.gruposService.checkInRangeGenerarOficiosEsquemas();
			} catch (ApplicationException e) {
				e.printStackTrace();
			}
		    
		    SimpleDateFormat fmt = new SimpleDateFormat("dd 'de' MMMMMMMMMMMMMM 'del' yyyy");
		    CatalogoItem dependencia = catalogoService.getCatalogoItem(CatalogoConstants.DEPENDENCIAS, String.valueOf(oficio.getDependencia()));
		    
		    params.put("REPORT_DATA_LIST_A", listNoSatisfactorias);
		    params.put("REPORT_DATA_LIST_B", listSatisfactorias);
		    params.put("NUM_OFICIO", String.valueOf(oficio.getNumOficio()));
		    params.put("FECHA_OFICIO", fmt.format(oficio.getFechaGeneracion()));
		    params.put("NOMBRE_DIRIGE_OFICIO", catalogoService.getCatalogoItemDescription(CatalogoConstants.CAT_PARAMETROS_NEGOCIO, "NOMDIROFIC"));
		    params.put("NUM_SOL_OK", String.valueOf(control.getTotalAceptadas()));
		    params.put("NUM_SOL_NO_OK", String.valueOf(control.getTotalRechazadas()));
		    params.put("FECHA_INICIAL_SOLICITUDES", fmt.format(rango.getFechaInicialCaptura()));
		    params.put("FECHA_FINAL_SOLICITUDES", fmt.format(Calendar.getInstance().getTime()));
		    params.put("CODIGO_CONTROL", control.getControlCodeEsquemas());
		    params.put("POLIZA", dependencia.getValorCampo2());
		    
		    jasperprint = JasperFillManager.fillReport(this.getJrOficioEsquema(),params, new JREmptyDataSource());
			// 6. Agregamos a lista de documentos
		    jprintlist.add(jasperprint);
				    
		    // 6. Create an output byte stream where data will be written
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			// 7. Export report
			// exporterService.export(AppConstants.EXTENSION_TYPE_PDF,
			// jprintlist, response, baos);
			
			StringBuilder fileName = new StringBuilder();
			fileName.append("Oficio");
			//fileName.append(s.getfmtFechaCaptura());
			fileName.append(".pdf");
			
			exporterService.export(fileName.toString(), documentType, jprintlist,
					response, baos);

			// 8. Write to reponse stream
			this.write(tokenService.generate(), response, baos);

		} catch (JRException jre) {
			throw new RuntimeException(jre);
		}

	}
	
	
	private String getEmptyStringContainer(int size) {
	    String container = "";
	    for (int i=0; i<size; i++) {
	    	 container += " ";
	    }
	    return container;
	}

	private String getEmptyStringContainer(int size, String filler) {
	    String container = "";
	    for (int i=0; i<size; i++) {
	    	 container += filler;
	    }
	    return container;
	}


}
