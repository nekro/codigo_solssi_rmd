package mx.com.metlife.solssi.esquemas.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.StringUtils;




@Entity
@Table( name = "TC_NOTAS_ESQUEMAS")
public class Nota implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4182340992966100364L;

	@Id
	@Column( name = "NUM_NOTA")
	@GeneratedValue
	private Integer numero;
	
	@Column( name = "REFERENCIA")
	@NotNull(message = "Capture un valor")
	@NotEmpty(message = "Capture un valor")
	@Length(max = 100, message="Longitud m�xima excedida") 
	private String referencia;
	
	@Column( name = "TEXTO")
	@NotNull(message = "Capture un valor")
	@NotEmpty(message = "Capture un valor")
	@Length(max = 1000, message="Longitud m�xima excedida")
	private String texto;
	
	@Column( name = "CVE_ESTATUS")
	private boolean activo;

	/**
	 * @return the numero
	 */
	public Integer getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	/**
	 * @return the texto
	 */
	public String getTexto() {
		return StringUtils.trimWhitespace(texto);
	}

	/**
	 * @param texto the texto to set
	 */
	public void setTexto(String texto) {
		this.texto = StringUtils.trimWhitespace(texto);
	}

	/**
	 * @return the activo
	 */
	public boolean isActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return StringUtils.trimWhitespace(referencia);
	}

	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = StringUtils.trimWhitespace(referencia);
	}
	
	

}
