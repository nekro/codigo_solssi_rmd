package mx.com.metlife.solssi.esquemas.mvc;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.esquemas.service.SolicitudEsquemaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("esquemas/solicitud")
@SessionAttributes("solicitudEsquema")
public class SolicitudEsquemasController {
	
	@Autowired
	private SolicitudEsquemasValidator validator;
	
	@Autowired
	private SolicitudEsquemaService solicitudService;
	
	
	@RequestMapping("validate")
	public String showSeleccion(@Valid SolicitudEsquema solicitud, BindingResult errors, Model model, HttpSession session){

		if(!validator.validaCaptcha(solicitud, session)){
			model.addAttribute("validCaptcha", "false");
			model.addAttribute(solicitud);
			return "esquemas/solicitud";
		}
		
		validator.validate(solicitud, errors);
		
		if(errors.hasErrors()){
			model.addAttribute(solicitud);
			return "esquemas/solicitud";	
		}
		
		return "esquemas/seleccion";
	}
	
	
	@RequestMapping("regresar")
	public String showSeleccion(){
		return "esquemas/solicitud";
	}

	
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public String save(Model model, HttpSession session){

		Boolean saved = (Boolean) session.getAttribute("SolicitudSaved");
		
		if(saved != null && saved){
			return "esquemas/solicitud/result";
		}
		
		Cliente cliente = (Cliente) session.getAttribute("cliente");
		SolicitudEsquema solicitud = (SolicitudEsquema) session.getAttribute("solicitudEsquema");
		
		solicitud.setCuenta(cliente.getCuenta());
		solicitud.setRfc(cliente.getRfc());
		solicitud.setHomoclave(cliente.getHomoclave());
		solicitud.setCentroTrabajo(cliente.getClaveDependencia().shortValue());
		solicitud.setNivelPuesto(cliente.getNivelPuesto());
		
		solicitudService.save(solicitud);
		
		session.setAttribute("SolicitudSaved", true);
		
		return "esquemas/solicitud/result";
	
	}

}
