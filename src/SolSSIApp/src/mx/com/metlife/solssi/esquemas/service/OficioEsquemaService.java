package mx.com.metlife.solssi.esquemas.service;

import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.esquemas.domain.OficioEsquema;
import mx.com.metlife.solssi.esquemas.domain.OficioFilterEsquema;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.usuario.domain.Usuario;

public interface OficioEsquemaService {
	
	public OficioEsquema generaOficioEsquemas(List<SolicitudEsquema> solicitudes, ControlCode control, Usuario userGenera) throws ApplicationException;
	
	public List<OficioEsquema> getOficios(OficioFilterEsquema filter);
	
	public OficioEsquema getOficio(Integer num);
	
	public void aceptarOficios(Integer oficios[], Usuario usuario) throws ApplicationException;
	
	public void enviarOficios(Integer oficios[], Usuario usuario) throws ApplicationException;
	
	public OficioEsquema existOficioDependencia(Short dependencia, Date ini, Date fin);
	
	public void marcarOficios(List<OficioEsquema> oficios, String user);

}


