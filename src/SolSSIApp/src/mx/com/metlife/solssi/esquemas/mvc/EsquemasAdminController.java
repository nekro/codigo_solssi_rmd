package mx.com.metlife.solssi.esquemas.mvc;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import mx.com.metlife.solssi.esquemas.domain.Atributo;
import mx.com.metlife.solssi.esquemas.domain.AtributoEsquema;
import mx.com.metlife.solssi.esquemas.domain.AtributoEsquemaPK;
import mx.com.metlife.solssi.esquemas.domain.Esquema;
import mx.com.metlife.solssi.esquemas.domain.Nota;
import mx.com.metlife.solssi.esquemas.service.EsquemaService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("esquemas/admin")
public class EsquemasAdminController {
	
	@Autowired
	private EsquemaService esquemaService;
	
	@RequestMapping(method = RequestMethod.POST)
	public String showPageAdmin(Model model){
		List<Atributo> atributos = esquemaService.getAtributos();
		model.addAttribute("atributos", atributos);
		
		List<Nota> notas = esquemaService.getNotas();
		model.addAttribute("notas", notas);
		
		esquemaService.getEsquemasInfo();
		return "esquemas/admin";
	}
	
	@RequestMapping(value = "values" , method = RequestMethod.POST)
	public String showPageValues(@RequestParam("esquema") Integer clave, Model model){
		
		Esquema esquema = esquemaService.getEsquema(clave);
		model.addAttribute(esquema);
		
		model.addAttribute("ACTION", "update");
		return "esquemas/admin/values";
	}
	
	@RequestMapping(value = "add" , method = RequestMethod.POST)
	public String add(Model model){
		
		Esquema esquema = new Esquema();
		esquema.setActivo(true);
		
		List<Atributo> atributos = esquemaService.getAtributos();
		
		List<AtributoEsquema> list = new ArrayList<AtributoEsquema>();
		for(Atributo atributo : atributos){
			AtributoEsquema ae = new AtributoEsquema();
			AtributoEsquemaPK pk = new AtributoEsquemaPK();
			
			pk.setAtributo(atributo);
			pk.setEsquema(esquema);
			
			ae.setAtributo(atributo);
			ae.setEsquema(esquema);
			
			ae.setPk(pk);
			
			list.add(ae);
		}
		
		esquema.setAtributos(list);
		
		model.addAttribute(esquema);
		
		model.addAttribute("ACTION", "alta");
		
		return "esquemas/admin/values";
	}
	
	
	@RequestMapping(value = "update" , method = RequestMethod.POST)
	public String update(@Valid Esquema esquema, BindingResult br, Model model){
		
		this.validateValores(esquema, br);	
		
		if(br.hasErrors()){
			model.addAttribute(esquema);
			model.addAttribute("ACTION", "update");
			return "esquemas/admin/values";
		}
		
		esquemaService.updateAtributosValues(esquema);
		
		return this.showPageAdmin(model);
	}
	
	
	@RequestMapping(value = "save" , method = RequestMethod.POST)
	public String save(@Valid Esquema esquema, BindingResult br, Model model){
		
		if(esquema.getClave() != null){
			Esquema oth = esquemaService.getEsquema(esquema.getClave());
			if(oth != null){
				br.rejectValue("clave", "", "El esquema ya existe");	
			}
		}
		
		this.validateValores(esquema, br);
		
		
		if(br.hasErrors()){
			model.addAttribute(esquema);
			model.addAttribute("ACTION", "alta");
			return "esquemas/admin/values";
		}
		
		
		
		esquemaService.saveEsquema(esquema);
		return this.showPageAdmin(model);
	}
	
	
	@RequestMapping(value = "/atributos/add", method = RequestMethod.POST)
	public String showPageAtributos(Model model){
		model.addAttribute("ACTION", "alta");
		Atributo atributo = new Atributo(); 
		
		List<Atributo> atributos = esquemaService.getAtributos();
		model.addAttribute("TOTAL_ATTS",atributos.size()+1);
		atributo.setOrden(atributos.size()+1);
		
		model.addAttribute(atributo);
		
		return "esquemas/admin/atributos";
	}
	
	
	@RequestMapping(value = "/atributos", method = RequestMethod.POST)
	public String showPageAtributos(@RequestParam("atributo") Integer atributo, Model model){
		model.addAttribute("ACTION", "update");
		List<Atributo> atributos = esquemaService.getAtributos();
		model.addAttribute("TOTAL_ATTS",atributos.size());
		model.addAttribute(esquemaService.getAtributo(atributo));
		return "esquemas/admin/atributos";
	}
	
	@RequestMapping(value = "/atributos/save", method = RequestMethod.POST)
	public String saveAtributo(@Valid Atributo atributo, BindingResult br, Model model){
		
		if(br.hasErrors()){
			model.addAttribute(atributo);
			model.addAttribute("ACTION", "alta");
			return "esquemas/admin/atributos";
		}
		
		esquemaService.saveAtributo(atributo);
		esquemaService.getEsquemasInfo();
		return this.showPageAdmin(model);
	}
	
	@RequestMapping(value = "/atributos/update", method = RequestMethod.POST)
	public String updateAtributo(@Valid Atributo atributo, BindingResult br, Model model){
		
		if(br.hasErrors()){
			model.addAttribute(atributo);
			model.addAttribute("ACTION", "update");
			return "esquemas/admin/atributos";
		}
		
		esquemaService.updateAtributo(atributo);
		esquemaService.getEsquemasInfo();
		
		return this.showPageAdmin(model);
	}
	
	
	
	
	
	
	@RequestMapping(value = "/notas", method = RequestMethod.POST)
	public String showPageNotas(@RequestParam("nota") Integer nota, Model model){
		model.addAttribute("ACTION", "update");
		model.addAttribute(esquemaService.getNota(nota));
		return "esquemas/admin/notas";
	}
	
	
	@RequestMapping(value = "/notas/add", method = RequestMethod.POST)
	public String showPageNotas(Model model){
		model.addAttribute("ACTION", "alta");
		Nota nota = new Nota();
		nota.setActivo(true);
		model.addAttribute(nota);
		return "esquemas/admin/notas";
	}
	
	
	@RequestMapping(value = "/notas/save", method = RequestMethod.POST)
	public String saveNota(@Valid Nota nota, BindingResult br, Model model){
		
		if(br.hasErrors()){
			model.addAttribute(nota);
			model.addAttribute("ACTION", "alta");
			return "esquemas/admin/notas";
		}
		
		esquemaService.saveNota(nota);
		esquemaService.getEsquemasInfo();
		return this.showPageAdmin(model);
	}
	
	
	@RequestMapping(value = "/notas/update", method = RequestMethod.POST)
	public String updateNota(@Valid Nota nota, BindingResult br, Model model){
		
		if(br.hasErrors()){
			model.addAttribute(nota);
			model.addAttribute("ACTION", "alta");
			return "esquemas/admin/notas";
		}
		
		esquemaService.updateNota(nota);
		esquemaService.getEsquemasInfo();
		return this.showPageAdmin(model);
	}
	
	
	private void validateValores(Esquema esquema, BindingResult br){
		if(esquema.getAtributos() != null){
			int i = 0;
			for(AtributoEsquema ae : esquema.getAtributos()){
				if(StringUtils.isEmpty(ae.getValor())){
					br.rejectValue("atributos["+i+"]", "", "Capture un valor para "+ae.getNombre());
				}else{
					if(ae.getValor().length() > 300){
						br.rejectValue("atributos["+i+"]", "", "Longitud m�xima es de 300 caracteres");
					}
				}
				i++;
			}
		}
	}
	
}
