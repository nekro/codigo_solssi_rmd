package mx.com.metlife.solssi.esquemas.mvc;

import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.service.CatalogoService;
import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.cliente.service.ClienteService;
import mx.com.metlife.solssi.esquemas.domain.EsquemasInfo;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.esquemas.service.EsquemaService;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.inicio.service.GruposCapturaService;
import mx.com.metlife.solssi.param.service.ParametersService;
import mx.com.metlife.solssi.util.BrowserUtils;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("esquemas/inicio")
@SessionAttributes({"solicitudEsquema","cliente"})
public class InicioEsquemasController {
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private CatalogoService catalogoService;
	
	@Autowired
	private GruposCapturaService gruposService;
	
	@Autowired
	private ParametersService paramService;
	
	@Autowired 
	public void init(ServletContext context, EsquemaService esquemaService){
		Locale.setDefault(new Locale("es","MX"));
		EsquemasInfo info = esquemaService.getEsquemasInfo();
		context.setAttribute("esquemasInfo", info);
		
		context.setAttribute("TT_ALTO_RENDIMIENTO", paramService.getAppParamValue("TT_ALTO_RENDIMIENTO", ""));
		context.setAttribute("TT_BAJO_RENDIMIENTO", paramService.getAppParamValue("TT_BAJO_RENDIMIENTO", ""));
	}
	
	@RequestMapping
	public String showInicioEsquemas(Model model) throws ApplicationException{
		
		try{
			gruposService.validateFechasCapturaEsquemas();
		}catch(Exception e){
			model.addAttribute("error", e.getMessage());
			return "esquemas/error_fechas";
		}
		
		model.addAttribute(new Cliente());
		return "esquemas/inicio";
	}
	
	@RequestMapping(value = "home")
	public String showHomeEsquemas() throws ApplicationException{
		return "esquemas/home";
	}
	
	
	
	
	@RequestMapping(value = "buscar" , method = RequestMethod.POST)
	public String searchCliente(@Valid Cliente clienteForm,BindingResult errors, Model model, HttpSession session){
		try {
		
			
			if(StringUtils.isEmpty(clienteForm.getRfc()) || clienteForm.getCuenta() == null){
				errors.reject("", MessageUtils.getMessage("esquemas.inicio.rfc_y_cuenta.NotEmpty"));
			}
				
			if(errors.hasErrors()){
				model.addAttribute(clienteForm);
				return "esquemas/inicio";
			}
			
			Cliente clienteSSI = clienteService.searchSSIEsquemas(clienteForm.getRfc(), clienteForm.getCuenta());
			
			gruposService.checkInFechaCapturaEsquemas(clienteSSI.isReCaptura());
			
			CatalogoItem item = catalogoService.getCatalogoItem(CatalogoConstants.PARAMETROS_NEGOCIO, "VAL_NP_ESQ");
			String val = item.getDescripcion();
			
			if(val.equals("1") && StringUtils.isEmpty(clienteSSI.getNivelPuesto())){
				errors.reject("", MessageUtils.getMessage("NotEmpty.solicitudEsquema.nivelPuesto"));
				return "esquemas/inicio";
			}
			
			CatalogoItem dependencia = catalogoService.getDependenciaFromCatalog(clienteSSI.getRetenedor(), clienteSSI.getUnidadPago());
			
			CatalogoItem polizas = catalogoService.getCatalogoItem(CatalogoConstants.PARAMETROS_NEGOCIO, "POLIZAS");
			
			if(polizas != null){
			
				String strPolizas[] = polizas.getDescripcion().split(",");
				boolean existPoliza = false;
				for(String strPoliza : strPolizas){
					if(StringUtils.equals(StringUtils.trim(strPoliza), dependencia.getValorCampo2())){
						existPoliza = true;
						break;
					}
				}
				if(!existPoliza){
					errors.reject("", MessageUtils.getMessage("esquemas.numPoliza.noCaptura",new Object[]{dependencia.getValorCampo2()}));
					return "esquemas/inicio";
				}
			}else{
				errors.reject("", MessageUtils.getMessage("esquemas.parametro.polizas"));
				return "esquemas/inicio";
			}
			
			

			
			model.addAttribute(clienteSSI);
			
			if(clienteSSI.isReCaptura()){
				model.addAttribute(clienteSSI);
				return "esquemas/inicio";
			}
			
			SolicitudEsquema solicitud = new SolicitudEsquema(); 
			solicitud.setRfc(clienteSSI.getRfc());
			solicitud.setCuenta(clienteSSI.getCuenta());
			solicitud.setFechaNacimientoRFC();
			solicitud.setPoliza(dependencia.getValorCampo2());
			
			model.addAttribute(solicitud);
			
		} catch (ApplicationException e) {
			errors.reject("", e.getMessage());
			return "esquemas/inicio";
		}
		
		session.setAttribute("SolicitudSaved", false);
		
		return "esquemas/solicitud";
		
	}
	
	
	@RequestMapping(value = "siModificar", method=RequestMethod.POST)
	public String confirmarModificacion(Model model, HttpSession session){
		
		Cliente clienteSSI = (Cliente) session.getAttribute("cliente");
		
		SolicitudEsquema solicitud = new SolicitudEsquema();
		solicitud.setRfc(clienteSSI.getRfc());
		solicitud.setCuenta(clienteSSI.getCuenta());
		solicitud.setFechaNacimientoRFC();
		
		CatalogoItem dependencia;
		try {
			dependencia = catalogoService.getDependenciaFromCatalog(clienteSSI.getRetenedor(), clienteSSI.getUnidadPago());
		} catch (ApplicationException e) {
			return "esquemas/inicio";
		}
		
		solicitud.setPoliza(dependencia.getValorCampo2());
		
		model.addAttribute(solicitud);
		this.deleteObjectsSession(session);
		return "esquemas/solicitud";
	}
	
	@RequestMapping(value = "noModificar", method=RequestMethod.POST)
	public String cancelarModificacion(Model model, HttpSession session){
		session.removeAttribute("solicitudEsquema");
		model.addAttribute(new Cliente());
		BrowserUtils.disableCloseSession(model);
		return "esquemas/inicio";
	}
	
	
	private void deleteObjectsSession(HttpSession session){
		session.removeAttribute("SolicitudSaved");
		session.removeAttribute("PRINT_PDF_COUNT");
		session.removeAttribute("DOWNLOAD_PDF_COUNT");
	}
	
}
