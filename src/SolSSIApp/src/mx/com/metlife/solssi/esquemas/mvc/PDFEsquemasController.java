package mx.com.metlife.solssi.esquemas.mvc;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.esquemas.service.PDFEsquemasService;
import mx.com.metlife.solssi.esquemas.service.SolicitudEsquemaService;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.param.service.ParametersService;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("esquemas/pdfservice")
public class PDFEsquemasController {


	@Autowired
	private PDFEsquemasService pdfService;
	
	@Autowired
	private ParametersService paramService;
	
	@Autowired
	private SolicitudEsquemaService solicitudService;
	
	@RequestMapping(value= "/imprimir/solicitud", method=RequestMethod.POST)
	public String imprimirSolicitud() {
		return "esquemas/solicitud/pdf";
	}
	
	@RequestMapping(value= "/imprimir/solicitud/menu")
	public String imprimirSolicitudMenu(){
		return "esquemas/solicitud/pdf/menu";
	}
	
	
	@RequestMapping(value= "/reimpresion/solicitud/{folio}", method=RequestMethod.POST)
	public void executeReportService(@PathVariable Integer folio, HttpServletResponse response) throws IOException {
		
		SolicitudEsquema s = solicitudService.get(folio);
		pdfService.fillReport(AppConstants.EXTENSION_TYPE_PDF, response, s);
	}
	
	@RequestMapping(value= "solicitud/generate")
	public void executeReportService(HttpServletResponse response,	HttpSession session) throws IOException, ApplicationException {

		if(this.checkNumVecesImpresion(session)){
			SolicitudEsquema s = (SolicitudEsquema) session.getAttribute("solicitudEsquema");
			if(s == null){
				throw new ApplicationException("El tiempo de la sesi�n ha expirado");
			}
			pdfService.fillReport(AppConstants.EXTENSION_TYPE_PDF, response, s);
		}else{
			throw new ApplicationException(MessageUtils.getMessage("number.max.print.exceeded"));
		}
	}//
	
	@RequestMapping(value= "/solicitud/download", method = RequestMethod.POST)
	public void downloadReportService(HttpServletResponse response,	HttpSession session, HttpServletRequest request) throws IOException, ApplicationException, ServletException {

		if(this.checkNumVecesDescarga(session)){
			SolicitudEsquema s = (SolicitudEsquema) session.getAttribute("solicitudEsquema");
			if(s == null){
				throw new ApplicationException("El tiempo de la sesi�n ha expirado");
			}
			pdfService.fillReport(AppConstants.EXTENSION_TYPE_PDF_DOWNLOAD, response, s);
		}else{
			request.setAttribute("errorDownload", MessageUtils.getMessage("number.max.download.exceeded"));
			request.getRequestDispatcher("/WEB-INF/jsp/esquemas/solicitud/pdf/pdf_menu.jsp").forward(request, response);
		}

	}//

	@RequestMapping("/pdf")
	public ModelAndView generatePdfReport(ModelAndView modelAndView, Solicitud s) {

		String pdfReportType = "pdfReportType";
		HashMap<String, Object> parameterMap = new HashMap<String, Object>();

		// pdfReport bean has ben declared in the jasper-views.xml file
		modelAndView = new ModelAndView(pdfReportType, parameterMap);

		return modelAndView;

	}// generatePdfReport
	
	
	
	
	@RequestMapping(value= "/oficio")
	public void executeReportOficio(HttpServletResponse response,
		   HttpSession session) throws IOException, ApplicationException {
		   Cliente cliente = (Cliente) session.getAttribute("cliente");
		   pdfService.fillReportOficio(AppConstants.EXTENSION_TYPE_PDF, response, cliente, session);
	}//
	
	
	@RequestMapping(value= "/download/oficio", method = RequestMethod.POST)
	public void downloadReportServiceOficio(HttpServletResponse response,	HttpSession session, HttpServletRequest request) throws IOException, ApplicationException, ServletException {

		Cliente cliente = (Cliente) session.getAttribute("cliente");
		pdfService.fillReportOficio(AppConstants.EXTENSION_TYPE_PDF_DOWNLOAD, response, cliente, session);

	}//

	
	
	private boolean checkNumVecesImpresion(HttpSession session){
		
		Integer count = 0;
		if(session.getAttribute("PRINT_PDF_COUNT") != null){
			count = (Integer) session.getAttribute("PRINT_PDF_COUNT");
		}
		
		
		Integer max = paramService.getParamIntegerValue("MAX_PRINT_PDF",10);
		
		boolean result = true;
		
		if(count >= max){
			result = false;
		}
		count++;
		session.setAttribute("PRINT_PDF_COUNT", count);
		
		return result;
	}
	
	private boolean checkNumVecesDescarga(HttpSession session){
		
		Integer count = 0;
		
		if(session.getAttribute("DOWNLOAD_PDF_COUNT") != null){
			count = (Integer) session.getAttribute("DOWNLOAD_PDF_COUNT");	
		}
		
		
		Integer max = paramService.getParamIntegerValue("MAX_DOWNLOAD_PDF",10);
		
		boolean result = true;
		
		if(count >= max){
			result = false;
		}
		
		count++;
		session.setAttribute("DOWNLOAD_PDF_COUNT", count);
		
		return result;
	}


}