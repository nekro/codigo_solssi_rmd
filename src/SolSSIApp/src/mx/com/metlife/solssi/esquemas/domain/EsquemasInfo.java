package mx.com.metlife.solssi.esquemas.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class EsquemasInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4435876792911747804L;

	private Integer totalEsquemas;
	
	private Integer totalEsquemasActivos;
	
	private Integer totalAtributos;
	
	private Integer totalAtributosActivos;
	
	private Map<Integer, Esquema> esquemas = new LinkedHashMap<Integer, Esquema>();
	
	private List<Atributo> atributos = new ArrayList<Atributo>();
	
	private List<Nota> notas = new ArrayList<Nota>();

	/**
	 * @return the totalEsquemas
	 */
	public Integer getTotalEsquemas() {
		return totalEsquemas;
	}

	/**
	 * @param totalEsquemas the totalEsquemas to set
	 */
	public void setTotalEsquemas(Integer totalEsquemas) {
		this.totalEsquemas = totalEsquemas;
	}

	/**
	 * @return the totalEsquemasActivos
	 */
	public Integer getTotalEsquemasActivos() {
		return totalEsquemasActivos;
	}

	/**
	 * @param totalEsquemasActivos the totalEsquemasActivos to set
	 */
	public void setTotalEsquemasActivos(Integer totalEsquemasActivos) {
		this.totalEsquemasActivos = totalEsquemasActivos;
	}

	/**
	 * @return the esquemas
	 */
	public Map<Integer, Esquema> getEsquemas() {
		return esquemas;
	}

	/**
	 * @param esquemas the esquemas to set
	 */
	public void setEsquemas(Map<Integer, Esquema> esquemas) {
		this.esquemas = esquemas;
	}

	/**
	 * @return the atributos
	 */
	public List<Atributo> getAtributos() {
		return atributos;
	}

	/**
	 * @param atributos the atributos to set
	 */
	public void setAtributos(List<Atributo> atributos) {
		this.atributos.clear();
		this.atributos.addAll(atributos);
	}

	/**
	 * @return the totalAtributos
	 */
	public Integer getTotalAtributos() {
		return totalAtributos;
	}

	/**
	 * @param totalAtributos the totalAtributos to set
	 */
	public void setTotalAtributos(Integer totalAtributos) {
		this.totalAtributos = totalAtributos;
	}

	/**
	 * @return the totalAtributosActivos
	 */
	public Integer getTotalAtributosActivos() {
		return totalAtributosActivos;
	}

	/**
	 * @param totalAtributosActivos the totalAtributosActivos to set
	 */
	public void setTotalAtributosActivos(Integer totalAtributosActivos) {
		this.totalAtributosActivos = totalAtributosActivos;
	}

	/**
	 * @return the notas
	 */
	public List<Nota> getNotas() {
		return notas;
	}

	/**
	 * @param notas the notas to set
	 */
	public void setNotas(List<Nota> notas) {
		this.notas = notas;
	}
	
	
	/**
	 * 
	 * @param esquema
	 * @return
	 */
	public Esquema getEsquema(Integer esquema){
		return this.esquemas.get(esquema);
	}
	
	

}
