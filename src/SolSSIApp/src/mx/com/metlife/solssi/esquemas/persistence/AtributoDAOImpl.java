package mx.com.metlife.solssi.esquemas.persistence;

import java.util.List;

import mx.com.metlife.solssi.esquemas.domain.Atributo;
import mx.com.metlife.solssi.esquemas.domain.Esquema;
import mx.com.metlife.solssi.main.dao.GenericDAOImpl;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

@Repository
public class AtributoDAOImpl extends GenericDAOImpl<Atributo, Integer> implements AtributoDAO{
	
	public List<Atributo> getAll(){
		Session session = this.getSession();
		Criteria crit = session.createCriteria(Atributo.class);
		crit.addOrder(Order.asc("orden"));
		return crit.list();
	}

	@Override
	public void save(Atributo atributo, List<Esquema> esquemas) {
		Session session = this.getSession();
		session.save(atributo);
		
		Query query = session.createSQLQuery("insert into tr_esquemas_atributos(num_esquema, id_atributo, valor) values(?,?,?)");
		
		for(Esquema esquema : esquemas){
			query.setInteger(0, esquema.getClave());
			query.setInteger(1, atributo.getId());
			query.setString(2, "");
			query.executeUpdate();
		}
		
	}

}
