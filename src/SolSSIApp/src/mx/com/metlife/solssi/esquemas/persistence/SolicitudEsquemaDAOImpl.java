package mx.com.metlife.solssi.esquemas.persistence;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.metlife.solssi.consulta.domain.ReporteTotales;
import mx.com.metlife.solssi.esquemas.domain.ConsultaFilterEsquema;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.main.dao.GenericDAOImpl;
import mx.com.metlife.solssi.util.AppConstants;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

@Repository
public class SolicitudEsquemaDAOImpl extends GenericDAOImpl<SolicitudEsquema, Integer> implements SolicitudEsquemaDAO{
	
	public List<SolicitudEsquema> get(ConsultaFilterEsquema filtro) {

		Session session = getSession();
		Criteria criteria = session.createCriteria(SolicitudEsquema.class,"s");
		
		ProjectionList proList = Projections.projectionList();
		proList.add(Projections.property("numFolio"),"s.numFolio");
		proList.add(Projections.property("cuenta"),"s.cuenta");
		proList.add(Projections.property("rfc"),"s.rfc");
		proList.add(Projections.property("oficio"),"s.oficio");
		proList.add(Projections.property("fechaCaptura"),"s.fechaCaptura");
		proList.add(Projections.property("estatus"),"s.estatus");
		proList.add(Projections.property("nombre"),"s.nombre");
		proList.add(Projections.property("apePaterno"),"s.apePaterno");
		proList.add(Projections.property("apeMaterno"),"s.apeMaterno");
		proList.add(Projections.property("proceso"),"s.proceso");
		proList.add(Projections.property("esquema"),"s.esquema");
		criteria.setProjection(proList);
		criteria.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		if (filtro.getNumFolio() != null && filtro.getNumFolio() > 0) {
			criteria.add(Restrictions.eq("numFolio", filtro.getNumFolio()));
			if (filtro.getEstatus() != null && filtro.getEstatus() > 0) {
				criteria.add(Restrictions.eq("estatus", filtro.getEstatus()));
			}
			
			Integer esquema = filtro.getEsquemaFromFolio();
			if(esquema != null){
				criteria.add(Restrictions.eq("esquema", esquema));
			}
			
		} else {

			if (StringUtils.isNotEmpty(filtro.getRfc())) {
				criteria.add(Restrictions.eq("rfc", filtro.getRfc()));
			}

			if (StringUtils.isNotEmpty(filtro.getHomoclave())) {
				criteria.add(Restrictions.eq("homoclaveRfc", filtro.getHomoclave()));
			}

			if (StringUtils.isNotEmpty(filtro.getCuenta())) {
				criteria.add(Restrictions.eq("cuenta", Long.valueOf(filtro.getNumCuenta())));
			}
			
			if (filtro.getOficio() != null) {
				if(filtro.getOficio() == -1){
					criteria.add(Restrictions.isNull("oficio"));
				}else{
					criteria.add(Restrictions.eq("oficio", filtro.getOficio()));
				}	
			}
			
			if (filtro.getEstatus() != null && filtro.getEstatus() > 0 ) {
				criteria.add(Restrictions.eq("estatus", filtro.getEstatus()));
			}else{
				if(filtro.getEstatusList() != null && filtro.getEstatusList().length > 0){
					criteria.add(Restrictions.in("estatus", filtro.getEstatusList()));
				}
			}
			
		}
		
		if(filtro.getDependencia() != null){
			criteria.add(Restrictions.eq("centroTrabajo", filtro.getDependencia()));	
		}
		
		if(filtro.getMaxResults() != null && filtro.getMaxResults() > 0){
			criteria.setMaxResults(filtro.getMaxResults());
		}
		
		criteria.addOrder(Order.asc("numFolio"));
		
		List<SolicitudEsquema> solicitudes = new ArrayList<SolicitudEsquema>();
		List<Map> list = criteria.list();
		
		for(Map m : list){
			SolicitudEsquema sol = new SolicitudEsquema();
			sol.setNumFolio((Integer)m.get("s.numFolio"));
			sol.setCuenta((Long)m.get("s.cuenta"));
			sol.setRfc(String.valueOf(m.get("s.rfc")));
			sol.setOficio((Integer)m.get("s.oficio"));
			sol.setEsquema((Integer)m.get("s.esquema"));
			sol.setFechaCaptura((Date) m.get("s.fechaCaptura"));
			sol.setEstatus((Short)m.get("s.estatus"));
			sol.setNombre(String.valueOf(m.get("s.nombre")));
			sol.setApePaterno(String.valueOf(m.get("s.apePaterno")));
			sol.setApeMaterno(String.valueOf(m.get("s.apeMaterno")));
			sol.setProceso((Integer)m.get("s.proceso"));
			
			solicitudes.add(sol);
			
		}
		
		return solicitudes;

	}
	
	

	public int updateEstatus(Integer numFolio, Short estatus, String userAutorizador){
		
		String sql = "update SolicitudEsquema set estatus = :estatus, userAutoriza = :user, fechaModificacion = :fecha where numFolio = :folio";
		Session session = getSession();
		Query query = session.createQuery(sql);
		query.setShort("estatus", estatus);
		query.setString("user", userAutorizador);
		query.setDate("fecha", Calendar.getInstance().getTime());
		query.setInteger("folio", numFolio);
		
		int results = query.executeUpdate();
		
		return results;
	}
	
	public int updateEstatus(Integer numFolio, Short estatus, Short estatusAnt, String userAutorizador){
		
		String sql = "update SolicitudEsquema set estatus = :estatus, userAutoriza = :user, fechaModificacion = :fecha where numFolio = :folio and estatus = :estatusAnt";
		Session session = getSession();
		Query query = session.createQuery(sql);
		query.setShort("estatus", estatus);
		query.setShort("estatusAnt", estatusAnt);
		query.setString("user", userAutorizador);
		query.setDate("fecha", Calendar.getInstance().getTime());
		query.setInteger("folio", numFolio);
		
		int results = query.executeUpdate();
		return results;
	}
	
	public int cancelaCapturadas(SolicitudEsquema solicitud){
		Session session = getSession();
		
		Query query = session.getNamedQuery("cancelaSolicitudesEsquemaCapturadas");
		query.setParameter("estatusCancelada", AppConstants.ESTATUS_CANCELADA);
		query.setParameter("rfc", solicitud.getRfc());
		query.setParameter("cuenta", solicitud.getCuenta());
		query.setParameter("estatusCapturada", AppConstants.ESTATUS_CAPTURA);
		query.setParameter("cveUserAutoriza", AppConstants.CVE_USER_CLIENTE);
		query.setParameter("fecha", Calendar.getInstance().getTime());
		
		int result = query.executeUpdate();
		return result;
		
	}



	@Override
	public int updateNivelPuesto(Integer numFolio, String nivelPuesto) {
		Session session = getSession();
		Query query = session.createQuery("update SolicitudEsquema set nivelPuesto = ? where numFolio = ?");
		query.setParameter(0, nivelPuesto);
		query.setParameter(1, numFolio);
		return query.executeUpdate();
	}
	
	
	@Override
	public ReporteTotales getTotales(Short dependencia){
		
		Session session = getSession();
		
		ReporteTotales report = new ReporteTotales();
		
		StringBuffer sql = new StringBuffer();
		sql.append("select sum(case when (num_esquema=1 and cve_estatus <> 9) then 1 else 0 end) as iniciadas,");
				sql.append("sum(case when (num_esquema=2 and cve_estatus <> 9)then 1 else 0 end) as aceptadas,");
				sql.append("sum(case when (num_esquema=3 and cve_estatus <> 9) then 1 else 0 end) as rechazadas,");
				sql.append("0 as canceladas, ");
				sql.append("sum(case when (cve_estatus <> 9) then 1 else 0 end) as total ");
				
				sql.append("from tw_solicitud_esquemas");
				
		if(dependencia != null && dependencia > 0){
			sql.append(" where centro_trabajo = :dependencia");
		}
		
		Query query = session.createSQLQuery(sql.toString());
		
		if(dependencia != null && dependencia > 0){
			query.setShort("dependencia", dependencia);
		}
		
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		Map result = (Map) query.uniqueResult();
		
		report.setAceptadas((Integer)result.get("ACEPTADAS"));
		report.setCanceladas((Integer)result.get("CANCELADAS"));
		report.setIniciadas((Integer)result.get("INICIADAS"));
		report.setRechazadas((Integer)result.get("RECHAZADAS"));
		report.setTotal((Integer)result.get("TOTAL"));
		
		return report;
		
	}


}
