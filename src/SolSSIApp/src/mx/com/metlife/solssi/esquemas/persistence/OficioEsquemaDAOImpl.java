package mx.com.metlife.solssi.esquemas.persistence;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.esquemas.domain.OficioEsquema;
import mx.com.metlife.solssi.esquemas.domain.OficioFilterEsquema;
import mx.com.metlife.solssi.main.dao.GenericDAOImpl;
import mx.com.metlife.solssi.oficio.domain.Oficio;
import mx.com.metlife.solssi.oficio.domain.OficioFilter;
import mx.com.metlife.solssi.util.AppConstants;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class OficioEsquemaDAOImpl extends GenericDAOImpl<OficioEsquema, Integer> implements OficioEsquemaDAO{
	
	
	public int updateNumOficioSolicitud(Integer numFolio, Integer numOficio){
		Session session = getSession();
		Query query = session.getNamedQuery("updateOficioEsquema");
		query.setParameter("folio", numFolio);
		query.setParameter("oficio", numOficio);
		return query.executeUpdate();
	}
	
	
	public OficioEsquema existOficioDependencia(Short dependencia, Date ini, Date fin){
		Session session = getSession();
		Query query = session.createQuery("select o from OficioEsquema o where dependencia = :dependencia and truncate(fechaGeneracion) between :fIni and :fFin");
		
		query.setParameter("dependencia", dependencia);
		query.setParameter("fIni", ini);
		query.setParameter("fFin", fin);
		OficioEsquema result = (OficioEsquema) query.uniqueResult();
		
		return result;
	}
	
		
	@SuppressWarnings("unchecked")
	public List<OficioEsquema> getOficios(OficioFilterEsquema filter){
		Session session = getSession();
		
		Criteria crit;
		
		crit = session.createCriteria(OficioEsquema.class);
		
		if(filter.getNumOficio() != null && filter.getNumOficio() > 0){
			crit.add(Restrictions.eq("numOficio", filter.getNumOficio()));
		}
		
		if(filter.getDependencia() != null && filter.getDependencia() > 0){
			crit.add(Restrictions.eq("dependencia", filter.getDependencia()));
		}
		
		if(filter.getEstatus() != null && filter.getEstatus() > 0){
			crit.add(Restrictions.eq("estatus", filter.getEstatus()));
		}
		
		if(filter.getProceso() != null && filter.getProceso() > 0){
			crit.add(Restrictions.eq("idProceso", filter.getProceso()));
		}
		
		if(filter.getFecha() != null){
			crit.add(Restrictions.ge("fechaGeneracion", filter.getFecha()));
		}
		
		return crit.list();
	}

	public int aceptarOficio(Integer numOficio, String user) {
		Session session = getSession();
		Query query = session.getNamedQuery("updateEstatusOficioEsquemaAceptado");
		query.setParameter("oficio", numOficio);
		query.setParameter("estatus_nuevo", AppConstants.OFICIO_ESTATUS_ACEPTADO);
		query.setParameter("estatus_ant", AppConstants.OFICIO_ESTATUS_GENERADO);
		query.setParameter("user", user);
		query.setParameter("fecha", Calendar.getInstance().getTime());
		return query.executeUpdate();
	}
	
	public int enviarOficio(Integer numOficio, String user) {
		Session session = getSession();
		Query query = session.getNamedQuery("updateEstatusOficioEsquemaEnviado");
		query.setParameter("oficio", numOficio);
		query.setParameter("estatus_nuevo", AppConstants.OFICIO_ESTATUS_ENVIADO);
		query.setParameter("estatus_ant", AppConstants.OFICIO_ESTATUS_ACEPTADO);
		query.setParameter("user", user);
		query.setParameter("fecha", Calendar.getInstance().getTime());
		return query.executeUpdate();
	}
	
	
	public void marcarOficios(List<OficioEsquema> oficios, String user) {
		
		Session session = getSession();
		
		StringBuffer sql = new StringBuffer();
		sql.append("UPDATE OficioEsquema SET ");
		sql.append("bndAutomatico = :automatico, ");
		sql.append("fechaEnvio = :fecha, usuarioEnvia = :user ");
		sql.append("WHERE numOficio = :oficio");
		
		Query query = session.createQuery(sql.toString());
		
		for(OficioEsquema oficio : oficios){
			query.setInteger("automatico", oficio.getBndAutomatico());
			query.setDate("fecha",Calendar.getInstance().getTime());
			query.setString("user",user);
			query.setInteger("oficio", oficio.getNumOficio());
			query.executeUpdate();
		}
		
	}
	
		
}
