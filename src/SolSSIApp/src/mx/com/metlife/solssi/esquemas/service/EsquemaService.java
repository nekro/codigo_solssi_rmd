package mx.com.metlife.solssi.esquemas.service;

import java.util.List;

import mx.com.metlife.solssi.esquemas.domain.Atributo;
import mx.com.metlife.solssi.esquemas.domain.Esquema;
import mx.com.metlife.solssi.esquemas.domain.EsquemasInfo;
import mx.com.metlife.solssi.esquemas.domain.Nota;

public interface EsquemaService {
	
	public Esquema getEsquema(Integer clave);
	
	public EsquemasInfo getEsquemasInfo();
	
	public List<Atributo> getAtributos();
	
	public void updateAtributosValues(Esquema esquema);
	
	public void saveEsquema(Esquema esquema);
	
	public Atributo getAtributo(Integer id);
	
	public void saveAtributo(Atributo atributo);
	
	public void updateAtributo(Atributo atributo);
	
	public Esquema getEsquemaFromMemory(Integer esquema);
	
	
	
	public void saveNota(Nota nota);
	
	public void updateNota(Nota nota);
	
	public List<Nota> getNotas();
	
	public Nota getNota(Integer num);

}
