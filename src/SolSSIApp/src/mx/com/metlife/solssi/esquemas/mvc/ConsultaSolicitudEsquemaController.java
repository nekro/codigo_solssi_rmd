package mx.com.metlife.solssi.esquemas.mvc;

import java.util.List;

import javax.validation.Valid;

import mx.com.metlife.solssi.esquemas.domain.ConsultaFilterEsquema;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.esquemas.service.SolicitudEsquemaService;
import mx.com.metlife.solssi.usuario.domain.UserPrincipal;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.util.BrowserUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("esquemas/consulta")
public class ConsultaSolicitudEsquemaController {
	
	@Autowired
	private SolicitudEsquemaService solicitudService;
	
	@RequestMapping(method=RequestMethod.POST)
	public String showConsulta(Model model){
		model.addAttribute(new ConsultaFilterEsquema());
		return "esquemas/consulta";
	}
	
	
	@RequestMapping(value = "execute", method=RequestMethod.POST)
	public String executeConsulta(@Valid ConsultaFilterEsquema filtro, BindingResult errors, Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		model.addAttribute(filtro);
		
		if(filtro.isEmpty()){
			errors.reject("NotEmpty.consultaFilter.object");
			return "esquemas/consulta";
		}
		
		if(errors.hasErrors()){
			return "esquemas/consulta";	
		}
		
		filtro.setDependencia(usuario.getDependencia());
		List<SolicitudEsquema> list = solicitudService.get(filtro);
		
		model.addAttribute("solicitudes", list);
		
		this.checkErroresResult(list, filtro, usuario, errors);
		
		BrowserUtils.enableHistoryBack(model);
		
		return "esquemas/consulta";
	}
	
	@RequestMapping(value = "view", method=RequestMethod.POST)
	public String showSolicitud(@RequestParam("numFolio") Integer folio, Model model){
		
		SolicitudEsquema solicitud = solicitudService.get(folio);
		model.addAttribute("solicitud", solicitud);
		model.addAttribute("urlRegresar", "/consulta");
		
		return "esquemas/consulta/solicitud";
	}
	
	/**
	 * revisa si existen errores en el resultado de la consulta
	 * @param list
	 * @param filtro
	 * @param usuario
	 * @param errors
	 */
	private void checkErroresResult(List<SolicitudEsquema> list, ConsultaFilterEsquema filtro, Usuario usuario, BindingResult errors){
	
		if(list == null || list.isEmpty()){
			if(usuario.getDependencia() != null && usuario.getDependencia() > 0){
				if(filtro.getNumFolio()!=null && filtro.getNumFolio()>0){
					errors.reject("NotEmpty.consultaFilter.dependencia.folio");	
				}else{
					if(StringUtils.isNotEmpty(filtro.getRfc())){
						errors.reject("NotEmpty.consultaFilter.dependencia.rfc");
					}else{
						if(StringUtils.isNotEmpty(filtro.getCuenta())){
							errors.reject("NotEmpty.consultaFilter.dependencia.cuenta");	
						}
					}
				}
				
			}else{
				if(filtro.getNumFolio()!=null && filtro.getNumFolio()>0){
					errors.reject("NotEmpty.consultaFilter.callcenter.folio");	
				}else{
					if(StringUtils.isNotEmpty(filtro.getRfc())){
						errors.reject("NotEmpty.consultaFilter.callcenter.rfc");
					}else{
						if(StringUtils.isNotEmpty(filtro.getCuenta())){
							errors.reject("NotEmpty.consultaFilter.callcenter.cuenta");	
						}
					}
				}
			}
		}
	}


}
