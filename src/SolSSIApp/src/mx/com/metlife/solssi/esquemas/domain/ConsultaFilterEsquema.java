package mx.com.metlife.solssi.esquemas.domain;

import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;

import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.util.Base26Codec;

public class ConsultaFilterEsquema extends ConsultaFilter{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9134680065392869900L;

	@Pattern(regexp="(^[E|e]{1}[0-9]{2}-[A-Za-z]{2}[0-9]{4}$)?")
	protected String userFolio;
	
	private Integer esquema;
	
	/**
	 * @return the userFolio
	 */
	public String getUserFolio() {
		return this.userFolio;
	}

	/**
	 * @param userFolio the userFolio to set
	 */
	public void setUserFolio(String userFolio) {
		this.userFolio = StringUtils.upperCase(userFolio);
		
		
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getEsquemaFromFolio(){
		if(StringUtils.isNotEmpty(this.userFolio)){
			try{
				String str = this.userFolio.substring(1,3);
				this.esquema = Integer.parseInt(str);
				return esquema;
			}catch(Exception e){
				return null;
			}
		}
		return null;
		
	}

	/**
	 * @return the esquema
	 */
	public Integer getEsquema() {
		return esquema;
	}
	
	/**
	 * @return the numFolio
	 */
	public Integer getNumFolio() {
		if(this.numFolio == null || this.numFolio == 0){
			if(StringUtils.isNotEmpty(userFolio)){
				this.numFolio = Base26Codec.decodeFolioSolicitud(userFolio.substring(4));
			}
		}
		return this.numFolio;
	}

	/**
	 * @param esquema the esquema to set
	 */
	public void setEsquema(Integer esquema) {
		this.esquema = esquema;
	}
	
	public boolean isEmpty(){
		return (StringUtils.isEmpty(cuenta)) && (StringUtils.isEmpty(rfc) && StringUtils.isEmpty(userFolio));
	}


}
