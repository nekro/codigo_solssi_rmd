package mx.com.metlife.solssi.esquemas.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table( name = "TC_ATRIBUTOS_ESQUEMAS")

public class Atributo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7783950984242681381L;

	@Id
	@Column( name = "ID_ATRIBUTO")
	@GeneratedValue
	private Integer id;
	
	@Column( name = "ORDEN")
	private Integer orden;
	
	@Column( name = "NOMBRE")
	@NotEmpty(message = "Capture un valor")
	private String nombre;
	
	@Column( name = "CVE_ESTATUS")
	private boolean activo = true;

	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the orden
	 */
	public Integer getOrden() {
		return orden;
	}

	/**
	 * @param orden the orden to set
	 */
	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	/**
	 * @return the activo
	 */
	public boolean isActivo() {
		return activo;
	}

	/**
	 * @param activo the activo to set
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return StringUtils.trim(nombre);
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AtributoEsquema [id=");
		builder.append(id);
		builder.append(", orden=");
		builder.append(orden);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", activo=");
		builder.append(activo);
		builder.append("]");
		return builder.toString();
	}
	
	

}
