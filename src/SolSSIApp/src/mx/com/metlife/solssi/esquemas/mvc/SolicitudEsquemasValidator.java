package mx.com.metlife.solssi.esquemas.mvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

@Service
public class SolicitudEsquemasValidator {
	
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public void validate(Object target, Errors errors) {

		SolicitudEsquema solicitud = (SolicitudEsquema) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ciudadElaboracion","NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apePaterno", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apeMaterno","NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numeroIdentificacion", "NotEmpty");
		
		if (solicitud.getEstadoElaboracion() == 0) {
			errors.rejectValue("estadoElaboracion", "NotEmpty");
		}
		
		if (solicitud.getCveTipoIdentificacion() == 0) {
			errors.rejectValue("cveTipoIdentificacion", "NotEmpty");
		} else {
			switch (solicitud.getCveTipoIdentificacion()) {
			case CatalogoConstants.IDENT_IFE:// IFE
				if (!StringUtils.isNumeric(solicitud.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() != 13) {
					errors.rejectValue("numeroIdentificacion", "IFEInvalid");
				}

				break;
			case CatalogoConstants.IDENT_CED_PROF:// cedula
				if (!StringUtils.isNumeric(solicitud.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() != 7) {
					errors.rejectValue("numeroIdentificacion", "CedulaInvalid");
				}
				break;
			case CatalogoConstants.IDENT_PASAPORTE:// pasaporte
				if (!StringUtils.isAlphanumeric(solicitud
						.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() < 9
						|| solicitud.getNumeroIdentificacion().length() > 10) {
					errors.rejectValue("numeroIdentificacion",
							"PasaporteInvalid");
				}
				break;
			case CatalogoConstants.IDENT_MATR_CONS:// Certificado consular
				if (!StringUtils.isNumeric(solicitud.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() < 7
						|| solicitud.getNumeroIdentificacion().length() > 9) {
					errors.rejectValue("numeroIdentificacion",
							"CertificadoInvalid");
				}
			case CatalogoConstants.IDENT_FM2:
				if (!StringUtils.isNumeric(solicitud.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() != 13) {
					errors.rejectValue("numeroIdentificacion", "fm2Invalid");
				}
				break;
			case CatalogoConstants.IDENT_FM3:
				if (!StringUtils.isNumeric(solicitud.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() != 13) {
					errors.rejectValue("numeroIdentificacion", "fm3Invalid");
				}
				break;
			}
		}
		
		
		if (StringUtils.isNotEmpty(solicitud.getCorreoPersonal())) {
			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
			Matcher matcher = pattern.matcher(solicitud.getCorreoPersonal());
			if (!matcher.matches()) {
				errors.rejectValue("correoPersonal", "NotFormat");
			}

		}

		if (StringUtils.isNotEmpty(solicitud.getCorreoTrabajo())) {
			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
			Matcher matcher = pattern.matcher(solicitud.getCorreoTrabajo());
			if (!matcher.matches()) {
				errors.rejectValue("correoTrabajo", "NotFormat");
			}

		}

		
		if (StringUtils.isEmpty(solicitud.getCorreoPersonal())
				&& StringUtils.isEmpty(solicitud.getCorreoTrabajo())) {
			errors.rejectValue("correoTrabajo", "NotEmpty");
		} else {
			if (StringUtils.isNotEmpty(solicitud.getCorreoPersonal())) {
				if (StringUtils.isEmpty(solicitud.getCorreoPersonalConfirm())) {
					errors.rejectValue("correoPersonalConfirm", "NotEmpty");
				} else {
					if (!StringUtils.equals(solicitud.getCorreoPersonal(),
							solicitud.getCorreoPersonalConfirm())) {
						errors.rejectValue("correoPersonalConfirm", "NotMatch");
					}
				}
			}

		if (StringUtils.isNotEmpty(solicitud.getCorreoTrabajo())) {
				if (StringUtils.isEmpty(solicitud.getCorreoTrabajoConfirm())) {
					errors.rejectValue("correoTrabajoConfirm", "NotEmpty");
				} else {
					if (!StringUtils.equals(solicitud.getCorreoTrabajo(),
							solicitud.getCorreoTrabajoConfirm())) {
						errors.rejectValue("correoTrabajoConfirm", "NotMatch");
					}
				}
			}

		}
		
		try {
			solicitud.setFechaNacimientoRFC();
			SimpleDateFormat format = new SimpleDateFormat("yyyyddMM");
			String strDate = solicitud.getAnioNacimiento()
					+ solicitud.getDiaNacimiento()
					+ solicitud.getMesNacimiento();
			Date date = format.parse(strDate);
			String nuevaFecha = format.format(date);
			if (!StringUtils.equals(strDate, nuevaFecha)) {
				errors.rejectValue("fechaNacimiento", "NotValid");
			} else {
				if (date.after(Calendar.getInstance().getTime())) {
					errors.rejectValue("fechaNacimiento", "NotValid");
				} else {
					solicitud.setFechaNacimiento(date);
				}
			}
		} catch (ParseException e) {
			errors.rejectValue("fechaNacimiento", "NotValid");
		}
		
		if(!solicitud.isAvisoPrivacidad()){
			errors.rejectValue("avisoPrivacidad", "NotValid");
		}


	}	
	
	
	public boolean validaCaptcha(SolicitudEsquema solicitud, HttpSession session) {
		
		if (session != null) {
			String captcha = (String) session
					.getAttribute("sessionCaptchaCode");
			
			if (StringUtils.isNotEmpty(captcha)) {
				if (!StringUtils.equals(captcha, solicitud.getCaptcha())) {
					return false;
				}
			}else{
				//return false;
			}
			return true;
		} else {
			return false;
		}
	}
}
