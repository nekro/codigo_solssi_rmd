package mx.com.metlife.solssi.esquemas.persistence;

import java.util.List;

import mx.com.metlife.solssi.esquemas.domain.AtributoEsquema;
import mx.com.metlife.solssi.esquemas.domain.Esquema;
import mx.com.metlife.solssi.esquemas.domain.Nota;
import mx.com.metlife.solssi.main.dao.GenericDAO;


public interface EsquemaDAO extends GenericDAO<Esquema, Integer>{
	
	public void updateAtributosValue(Integer esquema, Integer atributo, String valor);
	
	public void saveAtributosValue(Integer esquema, Integer atributo, String valor);
	
	public void updateEsquema(Esquema esquema);
	
	public List<AtributoEsquema> getAtributosValues(Integer esquema);
	
	
	
	public void saveNota(Nota nota);
	
	public void updateNota(Nota nota);
	
	public List<Nota> getNotas();
	
	public Nota getNota(Integer num);
	
}
