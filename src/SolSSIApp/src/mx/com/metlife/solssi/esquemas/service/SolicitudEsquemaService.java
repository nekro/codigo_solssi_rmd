package mx.com.metlife.solssi.esquemas.service;

import java.util.List;

import mx.com.metlife.solssi.consulta.domain.ReporteTotales;
import mx.com.metlife.solssi.esquemas.domain.ConsultaFilterEsquema;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.exception.ApplicationException;

public interface SolicitudEsquemaService {
	
	public void save(SolicitudEsquema solicitud);
	
	public void saveAsCaducadas(Integer[] numFolios, String user);
	
	public SolicitudEsquema get(Integer numFolio);

	public List<SolicitudEsquema> get(ConsultaFilterEsquema filtro);
	
	public void updateEstatusAutorizacion(SolicitudEsquema solicitud, Short estatus, String userAutorizador) throws ApplicationException;
	
	public void updateNivelPuesto(Integer folio, String nivelPuesto);
	
	public ReporteTotales getTotales(Short dependencia);

}
