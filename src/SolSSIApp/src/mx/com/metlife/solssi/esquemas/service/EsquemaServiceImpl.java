package mx.com.metlife.solssi.esquemas.service;

import java.util.List;
import java.util.Map;

import mx.com.metlife.solssi.esquemas.domain.Atributo;
import mx.com.metlife.solssi.esquemas.domain.AtributoEsquema;
import mx.com.metlife.solssi.esquemas.domain.AtributoEsquemaPK;
import mx.com.metlife.solssi.esquemas.domain.Esquema;
import mx.com.metlife.solssi.esquemas.domain.EsquemasInfo;
import mx.com.metlife.solssi.esquemas.domain.Nota;
import mx.com.metlife.solssi.esquemas.persistence.AtributoDAO;
import mx.com.metlife.solssi.esquemas.persistence.EsquemaDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import edu.emory.mathcs.backport.java.util.Collections;

@Service
@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
public class EsquemaServiceImpl implements EsquemaService{

	@Autowired
	private EsquemaDAO esquemaDAO;
	
	@Autowired
	private AtributoDAO atributoDAO;
	
	private EsquemasInfo esquemasInfo = new EsquemasInfo();
	
	private List<Esquema> getEsquemas() {
		List<Esquema> esquemas = esquemaDAO.getAll();
		//this.putAtributosValues(esquemas);
		Collections.sort(esquemas);
		return esquemas;
	}
	
	
	
	@Override
	public EsquemasInfo getEsquemasInfo() {

		List<Esquema> list = esquemaDAO.getAll();
		this.putAtributosValues(list);
		
		Collections.sort(list);
		
		Map<Integer, Esquema> map = esquemasInfo.getEsquemas();
		map.clear();
		
		int activos = 0;
		for(Esquema esquema : list ){
			map.put(esquema.getClave(), esquema);
			if(esquema.isActivo()){
				activos++;
			}
			
		}
		
		esquemasInfo.setTotalEsquemas(list.size());
		esquemasInfo.setTotalEsquemasActivos(activos);
		
		List<Atributo> atributos = this.getAtributos();
		
		activos = 0;
		for(Atributo at : atributos){
			if(at.isActivo()){
				activos ++;
			}
		}
		
		esquemasInfo.setAtributos(atributos);
		esquemasInfo.setTotalAtributos(atributos.size());
		esquemasInfo.setTotalAtributosActivos(activos);
		
		esquemasInfo.setNotas(this.getNotas());
		
		
		return esquemasInfo;
		
	}

	@Override
	public List<Atributo> getAtributos() {
		
		return atributoDAO.getAll();
	}

	@Override
	public Esquema getEsquema(Integer clave) {
		List<Esquema> list = esquemaDAO.getAll();
		this.putAtributosValues(list);
		for(Esquema esquema : list ){
			if(esquema.getClave().equals(clave)){
				return esquema;
			}
		}
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public void updateAtributosValues(Esquema esquema) {
		for(AtributoEsquema ae : esquema.getAtributos()){
			esquemaDAO.updateAtributosValue(esquema.getClave(), ae.getAtributo().getId(), ae.getValor());
		}
		
		esquemaDAO.update(esquema);
		
		this.getEsquemasInfo();
	}



	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public Atributo getAtributo(Integer id) {
		return atributoDAO.getById(id);
	}



	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public void saveAtributo(Atributo atributo) {
		atributoDAO.save(atributo, this.getEsquemas());
	}



	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public void updateAtributo(Atributo atributo) {
		atributoDAO.update(atributo);
	}



	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public void saveEsquema(Esquema esquema) {
		
		esquemaDAO.save(esquema);
		
		for(AtributoEsquema ae : esquema.getAtributos()){
			esquemaDAO.saveAtributosValue(esquema.getClave(), ae.getAtributo().getId(), ae.getValor());
		}
		
		this.getEsquemasInfo();
		
	}



	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public void saveNota(Nota nota) {
		esquemaDAO.saveNota(nota);
		
	}



	@Override
	@Transactional(propagation = Propagation.REQUIRED , readOnly = false)
	public void updateNota(Nota nota) {
		esquemaDAO.updateNota(nota);
	}



	@Override
	public List<Nota> getNotas() {
		return esquemaDAO.getNotas();
	}



	@Override
	public Nota getNota(Integer num) {
		return esquemaDAO.getNota(num);
	}
	
	
	/**
	 * 
	 * @param esquema
	 * @return
	 */
	public Esquema getEsquemaFromMemory(Integer esquema){
		
		if(this.esquemasInfo == null){
			this.getEsquemasInfo();
		}
		
		return this.esquemasInfo.getEsquema(esquema);
		
	}
	
	
	private void putAtributosValues(List<Esquema> esquemas){
		for(Esquema esquema : esquemas){
			
			List<AtributoEsquema> values = esquemaDAO.getAtributosValues(esquema.getClave());
			for(AtributoEsquema attr :values){
				AtributoEsquemaPK pk = attr.getPk();
				pk.setAtributo(atributoDAO.getById(pk.getNumAtributo()));
				pk.setEsquema(esquema);
			}
			esquema.setAtributos(values);
		}
	}

	

}
