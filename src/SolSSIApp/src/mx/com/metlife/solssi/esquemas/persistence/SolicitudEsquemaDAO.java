package mx.com.metlife.solssi.esquemas.persistence;

import java.util.List;

import mx.com.metlife.solssi.consulta.domain.ReporteTotales;
import mx.com.metlife.solssi.esquemas.domain.ConsultaFilterEsquema;
import mx.com.metlife.solssi.esquemas.domain.SolicitudEsquema;
import mx.com.metlife.solssi.main.dao.GenericDAO;

public interface SolicitudEsquemaDAO extends GenericDAO<SolicitudEsquema, Integer>{
	
	public int updateEstatus(Integer numFolio, Short estatus, String userAutorizador);
	
	public int updateEstatus(Integer numFolio, Short estatus, Short estatusAnt, String userAutorizador);
	
	public int updateNivelPuesto(Integer numFolio, String nivelPuesto);
	
	public int cancelaCapturadas(SolicitudEsquema solicitud);
	
	public List<SolicitudEsquema> get(ConsultaFilterEsquema filtro);
	
	public ReporteTotales getTotales(Short dependencia);

}
