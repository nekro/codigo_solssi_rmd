package mx.com.metlife.solssi.esquemas.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.Base26Codec;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name = "TW_SOLICITUD_ESQUEMAS")
@NamedQueries(
		{
			@NamedQuery(name="getSolicitudEsquemaCapturada", 
					query = "SELECT count(s.cuenta) FROM SolicitudEsquema s WHERE s.rfc = :rfc and s.cuenta = :cuenta and s.estatus in(:listEstatus)"),
			
			@NamedQuery(name="existeSolicitudEsquemaEnTramite", 
					query = "SELECT count(s.cuenta) FROM SolicitudEsquema s WHERE s.rfc = :rfc and s.cuenta = :cuenta and (s.estatus not in(:listEstatus) or s.oficio is not null)"),
					
			@NamedQuery(name="updateOficioEsquema", 
							query = "UPDATE SolicitudEsquema s SET s.oficio = :oficio WHERE s.numFolio = :folio and oficio is null"),
							
			@NamedQuery(name="cancelaSolicitudesEsquemasCapturadas", 
							query = "UPDATE SolicitudEsquema SET estatus = :estatusCancelada, userAutoriza =:user, fechaModificacion =:fecha  WHERE rfc = :rfc and cuenta = :cuenta and estatus = :estatusCapturada")		
			
		})		

public class SolicitudEsquema implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1213762360253960369L;

	@Id
	@Column(name = "NUM_FOLIO")
	@GeneratedValue
	private Integer numFolio;
	
	@Transient
	private String userFolio;
	
	@Column(name = "CUENTA")
	private Long cuenta;
	
	@Column(name = "RFC")
	private String rfc;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "APE_PATERNO")
	private String apePaterno;
	
	@Column(name = "APE_MATERNO")
	private String apeMaterno;
	
	@Column(name = "HOMOCLAVE")
	private String homoclave;
	
	@Column(name = "CVE_TIPO_IDENTIFICACION")
	private Short cveTipoIdentificacion;
	
	@Column(name = "NUMERO_IDENTIFICACION")
	private String numeroIdentificacion;
	
	@Column(name = "NUM_ESQUEMA")
	private Integer esquema;
	
	@Column(name = "CVE_ESTATUS")
	private Short estatus;
	
	@Column(name = "FECHA_NACIMIENTO")
	@Temporal(TemporalType.DATE)
	private Date fechaNacimiento;
	
	@Column(name = "BND_AVISO_PRIVACIDAD")
	private boolean avisoPrivacidad;
	
	@Column(name = "FECHA_CAPTURA", updatable = false)
	private Date fechaCaptura;
	
	@Column(name = "FECHA_MODIFICACION", insertable = false)
	private Date fechaModificacion;
	
	@Column(name = "CVE_USER_AUTORIZA", insertable = false)
	private String userAutoriza;
	
	@Column(name = "NUM_OFICIO", insertable = false)
	private Integer oficio;
	
	@Column(name = "ID_PROCESO", insertable = false)
	private Integer proceso;
	
	@Column(name = "CENTRO_TRABAJO")
	private Short centroTrabajo;
	
	@Column(name = "CORREO_PERSONAL")
	private String correoPersonal;
	
	@Column(name = "CORREO_TRABAJO")
	private String correoTrabajo;
	
	@Column(name = "CIUDAD_ELABORACION")
	private String ciudadElaboracion;
	
	@Column(name = "ID_ESTADO_ELABORACION")
	private Short estadoElaboracion;
	
	@Column(name = "NIVEL_PUESTO", length = 10)
	private String nivelPuesto;
	
	@Column(name = "NUM_POLIZA")
	private String poliza;
	
	@Transient 
	private String nombreEsquema;
	
	@Transient
	private boolean reCaptura;
	
	@Transient
	private String captcha;
	
	@Transient 
	private String correoPersonalConfirm;
	
	@Transient 
	private String correoTrabajoConfirm;
	
	@Transient 
	private String diaNacimiento;
	
	@Transient 
	private String mesNacimiento;
	
	@Transient 
	private String anioNacimiento;

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return StringUtils.trim(StringUtils.upperCase(nombre));
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = StringUtils.trim(StringUtils.upperCase(nombre));
	}

	/**
	 * @return the apePaterno
	 */
	public String getApePaterno() {
		return StringUtils.trim(StringUtils.upperCase(apePaterno));
	}

	/**
	 * @param apePaterno the apePaterno to set
	 */
	public void setApePaterno(String apePaterno) {
		this.apePaterno = StringUtils.trim(StringUtils.upperCase(apePaterno));
	}

	/**
	 * @return the apeMaterno
	 */
	public String getApeMaterno() {
		return StringUtils.trim(StringUtils.upperCase(apeMaterno));
	}

	/**
	 * @param apeMaterno the apeMaterno to set
	 */
	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = StringUtils.trim(StringUtils.upperCase(apeMaterno));
	}

	/**
	 * @return the avisoPrivacidad
	 */
	public boolean isAvisoPrivacidad() {
		return avisoPrivacidad;
	}

	/**
	 * @param avisoPrivacidad the avisoPrivacidad to set
	 */
	public void setAvisoPrivacidad(boolean avisoPrivacidad) {
		this.avisoPrivacidad = avisoPrivacidad;
	}

	/**
	 * @return the cuenta
	 */
	public Long getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(Long cuenta) {
		this.cuenta = cuenta;
	}

	/**
	 * @return the homoclave
	 */
	public String getHomoclave() {
		return StringUtils.trim(StringUtils.upperCase(homoclave));
	}

	/**
	 * @param homoclave the homoclave to set
	 */
	public void setHomoclave(String homoclave) {
		this.homoclave = StringUtils.trim(StringUtils.upperCase(homoclave));
	}

	/**
	 * @return the cveTipoIdentificacion
	 */
	public Short getCveTipoIdentificacion() {
		return cveTipoIdentificacion;
	}

	/**
	 * @param cveTipoIdentificacion the cveTipoIdentificacion to set
	 */
	public void setCveTipoIdentificacion(Short cveTipoIdentificacion) {
		this.cveTipoIdentificacion = cveTipoIdentificacion;
	}

	/**
	 * @return the numeroIdentificacion
	 */
	public String getNumeroIdentificacion() {
		return StringUtils.trim(StringUtils.upperCase(numeroIdentificacion));
	}

	/**
	 * @param numeroIdentificacion the numeroIdentificacion to set
	 */
	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = StringUtils.trim(StringUtils.upperCase(numeroIdentificacion));
	}

	/**
	 * @return the esquema
	 */
	public Integer getEsquema() {
		return esquema;
	}

	/**
	 * @param esquema the esquema to set
	 */
	public void setEsquema(Integer esquema) {
		this.esquema = esquema;
	}

	/**
	 * @return the captcha
	 */
	public String getCaptcha() {
		return captcha;
	}

	/**
	 * @param captcha the captcha to set
	 */
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	/**
	 * @return the numFolio
	 */
	public Integer getNumFolio() {
		return numFolio;
	}

	/**
	 * @param numFolio the numFolio to set
	 */
	public void setNumFolio(Integer numFolio) {
		this.numFolio = numFolio;
	}

	/**
	 * @return the rfc
	 */
	public String getRfc() {
		return rfc;
	}

	/**
	 * @param rfc the rfc to set
	 */
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	/**
	 * @return the estatus
	 */
	public Short getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Short estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the fechaCaptura
	 */
	public Date getFechaCaptura() {
		return fechaCaptura;
	}

	/**
	 * @param fechaCaptura the fechaCaptura to set
	 */
	public void setFechaCaptura(Date fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}

	/**
	 * @return the fechaAutorizacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaAutorizacion the fechaAutorizacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the userAutoriza
	 */
	public String getUserAutoriza() {
		return userAutoriza;
	}

	/**
	 * @param userAutoriza the userAutoriza to set
	 */
	public void setUserAutoriza(String userAutoriza) {
		this.userAutoriza = userAutoriza;
	}

	/**
	 * @return the userFolio
	 */
	public String getUserFolio() {
		
		StringBuffer buf = new StringBuffer(10);
		
		if(this.esquema < 10){
			buf.append("E0");	
		}else{
			buf.append("E");	
		}
		
		buf.append(this.esquema).append("-").append(Base26Codec.encodeFolioSolicitud(this.numFolio));
		
		return buf.toString();
		 
	}

	/**
	 * @return the oficio
	 */
	public Integer getOficio() {
		return oficio;
	}

	/**
	 * @param oficio the oficio to set
	 */
	public void setOficio(Integer oficio) {
		this.oficio = oficio;
	}

	/**
	 * @return the proceso
	 */
	public Integer getProceso() {
		return proceso;
	}

	/**
	 * @param proceso the proceso to set
	 */
	public void setProceso(Integer proceso) {
		this.proceso = proceso;
	}

	/**
	 * @return the centroTrabajo
	 */
	public Short getCentroTrabajo() {
		return centroTrabajo;
	}

	/**
	 * @param centroTrabajo the centroTrabajo to set
	 */
	public void setCentroTrabajo(Short centroTrabajo) {
		this.centroTrabajo = centroTrabajo;
	}

	/**
	 * @return the reCaptura
	 */
	public boolean isReCaptura() {
		return reCaptura;
	}

	/**
	 * @param reCaptura the reCaptura to set
	 */
	public void setReCaptura(boolean reCaptura) {
		this.reCaptura = reCaptura;
	}
	
	

	
	/**
	 * @return the fechaNacimiento
	 */
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	/**
	 * @param fechaNacimiento the fechaNacimiento to set
	 */
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	/**
	 * @return the correoPersonal
	 */
	public String getCorreoPersonal() {
		return correoPersonal;
	}

	/**
	 * @param correoPersonal the correoPersonal to set
	 */
	public void setCorreoPersonal(String correoPersonal) {
		this.correoPersonal = correoPersonal;
	}

	/**
	 * @return the correoTrabajo
	 */
	public String getCorreoTrabajo() {
		return correoTrabajo;
	}

	/**
	 * @param correoTrabajo the correoTrabajo to set
	 */
	public void setCorreoTrabajo(String correoTrabajo) {
		this.correoTrabajo = correoTrabajo;
	}

	/**
	 * @return the correoPersonalConfirm
	 */
	public String getCorreoPersonalConfirm() {
		return correoPersonalConfirm;
	}

	/**
	 * @param correoPersonalConfirm the correoPersonalConfirm to set
	 */
	public void setCorreoPersonalConfirm(String correoPersonalConfirm) {
		this.correoPersonalConfirm = correoPersonalConfirm;
	}

	/**
	 * @return the correoTrabajoConfirm
	 */
	public String getCorreoTrabajoConfirm() {
		return correoTrabajoConfirm;
	}

	/**
	 * @param correoTrabajoConfirm the correoTrabajoConfirm to set
	 */
	public void setCorreoTrabajoConfirm(String correoTrabajoConfirm) {
		this.correoTrabajoConfirm = correoTrabajoConfirm;
	}

	/**
	 * @return the diaNacimiento
	 */
	public String getDiaNacimiento() {
		return diaNacimiento;
	}

	/**
	 * @param diaNacimiento the diaNacimiento to set
	 */
	public void setDiaNacimiento(String diaNacimiento) {
		this.diaNacimiento = diaNacimiento;
	}

	/**
	 * @return the mesNacimiento
	 */
	public String getMesNacimiento() {
		return mesNacimiento;
	}

	/**
	 * @param mesNacimiento the mesNacimiento to set
	 */
	public void setMesNacimiento(String mesNacimiento) {
		this.mesNacimiento = mesNacimiento;
	}

	/**
	 * @return the anioNacimiento
	 */
	public String getAnioNacimiento() {
		return anioNacimiento;
	}

	/**
	 * @param anioNacimiento the anioNacimiento to set
	 */
	public void setAnioNacimiento(String anioNacimiento) {
		this.anioNacimiento = anioNacimiento;
	}

	public void fillTrasientFields(){
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
		
		if(this.fechaNacimiento != null){
			String fecha = format.format(this.fechaNacimiento);
			this.diaNacimiento = fecha.substring(0,2);
			this.mesNacimiento = fecha.substring(2,4);
			this.anioNacimiento = fecha.substring(4);
		}
		
	}
	
	
	/**
	 * Establece la fecha de nacimiento a partir del RFC
	 */
	public void setFechaNacimientoRFC(){
		if(StringUtils.isNotEmpty(rfc) && this.fechaNacimiento == null){
			try{
				this.anioNacimiento = "19" + rfc.substring(4,6);
				this.mesNacimiento = rfc.substring(6,8);
				this.diaNacimiento = rfc.substring(8);
			}catch(Exception e){}
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public String getfmtFechaCaptura(){
		if(this.fechaCaptura != null){
			SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
			return format.format(this.fechaCaptura);
		}
		return "";
	}

	/**
	 * @return the ciudadElaboracion
	 */
	public String getCiudadElaboracion() {
		return StringUtils.upperCase(StringUtils.trim(ciudadElaboracion));
	}

	/**
	 * @param ciudadElaboracion the ciudadElaboracion to set
	 */
	public void setCiudadElaboracion(String ciudadElaboracion) {
		this.ciudadElaboracion = ciudadElaboracion;
	}

	/**
	 * @return the estadoElaboracion
	 */
	public Short getEstadoElaboracion() {
		return estadoElaboracion;
	}

	/**
	 * @param estadoElaboracion the estadoElaboracion to set
	 */
	public void setEstadoElaboracion(Short estadoElaboracion) {
		this.estadoElaboracion = estadoElaboracion;
	}

	/**
	 * @return the nivelPuesto
	 */
	public String getNivelPuesto() {
		return StringUtils.trim(nivelPuesto);
	}

	/**
	 * @param nivelPuesto the nivelPuesto to set
	 */
	public void setNivelPuesto(String nivelPuesto) {
		this.nivelPuesto = nivelPuesto;
	}

	/**
	 * @return the poliza
	 */
	public String getPoliza() {
		return StringUtils.trim(poliza);
	}

	/**
	 * @param poliza the poliza to set
	 */
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	
	public String getMotivoNoSatisfactorio(){
		if(this.estatus == AppConstants.ESTATUS_RECHAZADO){
			return "Revisi�n documental";
		}
		
		if(this.estatus == AppConstants.ESTATUS_RECHAZADA_CADUCIDAD){
			return "Caducidad del folio";
		}
		
		return StringUtils.EMPTY;
	}

	/**
	 * @return the nombreEsquema
	 */
	public String getNombreEsquema() {
		return this.esquema + " " + nombreEsquema;
	}

	/**
	 * @param nombreEsquema the nombreEsquema to set
	 */
	public void setNombreEsquema(String nombreEsquema) {
		this.nombreEsquema = nombreEsquema;
	}

	
	
	
}
