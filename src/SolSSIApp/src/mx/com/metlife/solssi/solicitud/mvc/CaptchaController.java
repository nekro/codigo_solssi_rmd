package mx.com.metlife.solssi.solicitud.mvc;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.captcha.FrontOutput;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("solicitud/captcha")
public class CaptchaController {
	
	@RequestMapping
	public void generaCaptcha(HttpServletResponse response, HttpSession session) throws IOException{
		
		String captchaText = FrontOutput.getMainCaptcha(response.getOutputStream());

		if(session != null ){
			session.setAttribute("sessionCaptchaCode", captchaText);
		}
	}
	
	
	@RequestMapping("/saldo")
	public void generaCaptchaSaldo(HttpServletResponse response, HttpSession session) throws IOException{
		
		String captchaText = FrontOutput.getMainCaptcha(response.getOutputStream());

		if(session != null ){
			session.setAttribute("sessionCaptchaCodeSaldo", captchaText);
		}
	}

}
