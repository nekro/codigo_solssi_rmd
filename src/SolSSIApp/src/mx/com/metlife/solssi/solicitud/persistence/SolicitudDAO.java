package mx.com.metlife.solssi.solicitud.persistence;

import java.util.List;

import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.consulta.domain.ReporteTotales;
import mx.com.metlife.solssi.main.dao.GenericDAO;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;

public interface SolicitudDAO extends GenericDAO<Solicitud, Integer>{
	
	public List<Solicitud> get(ConsultaFilter filtro);
	
	public List<Solicitud> get(ConsultaFilter filtro, boolean infoRechazo);
	
	public int updateEstatus(Integer numFolio, Short estatus, Short estatusAnt, String userAutorizador);
	
	public int updateEstatus(Integer numFolio, Short estatus, String userAutorizador);
	
	public boolean checkUpdate(Integer numFolio, Short estatus);
	
	public String getClabe(Integer numFolio);
	
	public int cancelaCapturadas(Solicitud solicitud);
	
	public ReporteTotales getTotales(Short dependencia);
	
	public String getDescripcionRechazo(Integer numFolio);

}
