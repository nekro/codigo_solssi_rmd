package mx.com.metlife.solssi.solicitud.domain;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class DependientePK implements Serializable {
	
	@ManyToOne
	@JoinColumn(name="NUM_FOLIO")
	private Solicitud solicitud;

	@Column(name="NUM_DEPENDIENTE")
	private short numero;

	private static final long serialVersionUID = 1L;

	public DependientePK() {
		super();
	}

	
	public short getNumDependiente() {
		return this.numero;
	}

	public void setNumDependiente(short numDependiente) {
		this.numero = numDependiente;
	}


	/**
	 * @return the solicitud
	 */
	public Solicitud getSolicitud() {
		return solicitud;
	}


	/**
	 * @param solicitud the solicitud to set
	 */
	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}


	/**
	 * @return the numero
	 */
	public short getNumero() {
		return numero;
	}


	/**
	 * @param numero the numero to set
	 */
	public void setNumero(short numero) {
		this.numero = numero;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numero;
		result = prime * result
				+ ((solicitud == null) ? 0 : solicitud.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DependientePK)) {
			return false;
		}
		DependientePK other = (DependientePK) obj;
		if (numero != other.numero) {
			return false;
		}
		if (solicitud == null) {
			if (other.solicitud != null) {
				return false;
			}
		} else if (!solicitud.equals(other.solicitud)) {
			return false;
		}
		return true;
	}

	
	
}
