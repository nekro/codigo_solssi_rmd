package mx.com.metlife.solssi.solicitud.mvc;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import mx.com.metlife.solssi.solicitud.domain.DependienteEconomico;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("solicitud/dependientes")
public class DependientesController{
	
	@Autowired
	DependienteValidator dependienteValidator;
	
	@RequestMapping(value = "list", method=RequestMethod.POST)
	public String show(){
		return "dependientes/list";
	}
	
	
	@RequestMapping(value="form", method=RequestMethod.POST)
	public String showForm(Model model, HttpSession session){
		
		DependienteEconomico dependiente = new DependienteEconomico();

		//dependiente.fillForTest();
		
		model.addAttribute(dependiente);
		
		return "dependientes/form";
	}
	
	
	@RequestMapping(value = "edit/{numero}" , method=RequestMethod.POST)
	public String edit(@PathVariable Short numero, Model model, HttpSession session){
		
		Solicitud solicitud = (Solicitud) session.getAttribute("solicitud");
		DependienteEconomico dependiente = solicitud.getDependiente(numero);
		model.addAttribute(dependiente);
		
		return "dependientes/form";
	}
	
	@RequestMapping(value = "remove/{numero}", method=RequestMethod.POST)
	public String remove(@PathVariable Short numero, Model model, HttpSession session){
		
		Solicitud solicitud = (Solicitud) session.getAttribute("solicitud");
		solicitud.removeDependiente(numero);
		
		return "dependientes/list";
	}
	
	@RequestMapping(value = "insert", method=RequestMethod.POST)
	public String insert(@Valid DependienteEconomico dependiente, BindingResult errors, Model model, HttpSession session){
		
		dependienteValidator.validate(dependiente, errors);
		if(errors.hasErrors()){
			model.addAttribute(dependiente);
			return "dependientes/form";
		}
		
		
		Solicitud solicitud = (Solicitud) session.getAttribute("solicitud");
		if(solicitud.getDependientes().size()<20){
			solicitud.addDependiente(dependiente);
		}
		
		return "dependientes/list";
	}
	
	
	@RequestMapping("nodependientes")
	public String declareNoDependientes(Model model, HttpSession session){
		
		Solicitud solicitud = (Solicitud) session.getAttribute("solicitud");
		solicitud.setDeclaracionNoDependientes(true);
		
		model.addAttribute("DECLARACION_NO_DEPENDIENTES", "true");
		
		
		return "dependientes/list";
	}
	
}
