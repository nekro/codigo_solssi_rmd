package mx.com.metlife.solssi.solicitud.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TW_MOTIVOS_SOLICITUD")
public class MotivoSolicitud implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1295502868723033161L;

	@Id
	@Column(name = "NUM_FOLIO")
	private Integer folio;
	
	@Column(name="TIPO_MOTIVO")
	private Short tipoMotivo;
	
	@Column(name = "CVE_MOTIVO")
	private Short cveMotivo;
	
	@Column(name = "NUM_SINIESTRO")
	private String siniestro;
	
	@Column(name = "CVE_AFECTADO")
	private Short afectado;

	/**
	 * @return el folio
	 */
	public Integer getFolio() {
		return folio;
	}

	/**
	 * @param folio el folio a establecer
	 */
	public void setFolio(Integer folio) {
		this.folio = folio;
	}

	/**
	 * @return el tipoMotivo
	 */
	public Short getTipoMotivo() {
		return tipoMotivo;
	}

	/**
	 * @param tipoMotivo el tipoMotivo a establecer
	 */
	public void setTipoMotivo(Short tipoMotivo) {
		this.tipoMotivo = tipoMotivo;
	}

	/**
	 * @return el cveMotivo
	 */
	public Short getCveMotivo() {
		return cveMotivo;
	}

	/**
	 * @param cveMotivo el cveMotivo a establecer
	 */
	public void setCveMotivo(Short cveMotivo) {
		this.cveMotivo = cveMotivo;
	}

	/**
	 * @return el siniestro
	 */
	public String getSiniestro() {
		return siniestro;
	}

	/**
	 * @param siniestro el siniestro a establecer
	 */
	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}

	/**
	 * @return el afectado
	 */
	public Short getAfectado() {
		return afectado;
	}

	/**
	 * @param afectado el afectado a establecer
	 */
	public void setAfectado(Short afectado) {
		this.afectado = afectado;
	}

	/**
	 * @return el serialVersionUID
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
		
	

}
