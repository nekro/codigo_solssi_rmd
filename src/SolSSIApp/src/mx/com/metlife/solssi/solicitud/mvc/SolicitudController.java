package mx.com.metlife.solssi.solicitud.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.solicitud.domain.DependienteEconomico;
import mx.com.metlife.solssi.solicitud.domain.DependientePK;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.solicitud.service.SolicitudService;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;


@Controller(value = "solicitudController")
@RequestMapping("solicitud")
@SessionAttributes(value={"solicitud"})
public class SolicitudController {

	@Autowired
	SolicitudValidator solicitudValidator;
	
	@Autowired	
	private SolicitudService solicitudService;
	
	@RequestMapping("avisoprivacidad")
	public String showAvisoPrivacidad(){
		return "aviso/privacidad";
	}
	
	
	@RequestMapping(value = "/procesa", method=RequestMethod.POST)
	public String procesaSolicitud(@Valid Solicitud solicitud, BindingResult errors, Model model, HttpSession session, HttpServletRequest request) {
		
		this.deleteObjectsSession(session);
		Boolean saved = (Boolean) session.getAttribute("SolicitudSaved");
		
		if(saved != null && saved){
			return "solicitud/result";
		}
		
		if(!solicitudValidator.validaCaptcha(solicitud, session)){
			model.addAttribute("validCaptcha", "false");
			return "solicitud";
		}
		
		Cliente cliente = (Cliente) session.getAttribute("cliente");
		
		
		
		solicitud.setCliente(cliente);
		
		model.addAttribute(solicitud);
		
		solicitudValidator.validate(solicitud, errors, session);
		
		if(errors.hasErrors()){
			return "solicitud";
		}
		
		if(solicitud.isAplicaFormato140()){
			Solicitud solSession = (Solicitud) session.getAttribute("solicitud");
			int i=1;
			for(DependienteEconomico d : solSession.getDependientes()){
				DependientePK pk = new DependientePK();
				pk.setNumero((short)i++);
				pk.setSolicitud(solicitud);
				d.setPk(pk);
			}
			solicitud.setDependientes(solSession.getDependientes());
		}
		
		solicitud.setCuenta(cliente.getCuenta());
		solicitud.setRfc(cliente.getRfc());
		solicitud.setHomoclaveRfc(cliente.getHomoclave());
		solicitud.setPrestamo(cliente.isPrestamo());
		solicitud.setPensionado(cliente.isPension());
		solicitud.setNivelPuesto(cliente.getNivelPuesto());
		solicitud.setCentroTrabajo(cliente.getClaveDependencia().shortValue());
		solicitud.setBndUltimosDigitosClabe(StringUtils.isNotEmpty(cliente.getUltimosDigitosClabe()));
		solicitud.setReCaptura(cliente.isReCaptura());
		solicitud.setMontoSolicitado(this.calculaMonto(cliente, solicitud));
		
		String ipAddress = request.getHeader("X-FORWARDED-FOR");  
		if (ipAddress == null) {  
		   ipAddress = request.getRemoteAddr();  
		}
		
		solicitudService.save(solicitud, ipAddress);
		session.setAttribute("SolicitudSaved", true);
		this.deleteObjectsSession(session);
		
		return "solicitud/result";
	}
	
	@RequestMapping(value = "/salir", method=RequestMethod.POST)
	public String salir(Model model, HttpSession session){
		session.invalidate();
		return "solicitud/salir";
	}
	
	@RequestMapping(value = "/salir/ajax", method=RequestMethod.POST)
	public @ResponseBody String salirAjax(Model model, HttpSession session){
		session.invalidate();
		return "true";
	}
	
	
	@RequestMapping(value = "/refresh", method=RequestMethod.POST)
	public String refresh(Solicitud solicitud, Model model){
		model.addAttribute(solicitud);
		model.addAttribute("refresh","true");
		return "solicitud";
	}
	
	@RequestMapping(value = "imprimir/errores")
	public String imprimirErrores(HttpSession session){
		
		return "impresion/errores";
	}
	
	@RequestMapping(value="dependientes", method=RequestMethod.POST)
	public String showList(Solicitud solicitud, Model model, HttpSession session){
		
		if(solicitud.isAplicaFormato140()){
			Solicitud solSession = (Solicitud) session.getAttribute("solicitud");
			solicitud.setDependientes(solSession.getDependientes());
			model.addAttribute(solicitud);
		}
		
		return "dependientes/list";
	}
	
	@RequestMapping(value = "/pdf", method=RequestMethod.POST)
	public String pdf(Solicitud solicitud, Model model){
		model.addAttribute(solicitud);
		//model.addAttribute("refresh","true");
		return "solicitud/pdf";
	}
	
	@RequestMapping(value = "/pdf/menu")
	public String pdfMenu(Solicitud solicitud, Model model){
		model.addAttribute(solicitud);
		//model.addAttribute("refresh","true");
		return "solicitud/pdf/menu";
	}
	
	@RequestMapping(value = "/error", method=RequestMethod.POST)
	public String error(Model model){
		return "solicitud/error";
	}
	
	@RequestMapping(value = "/error/menu")
	public String errorMenu(Model model){
		return "solicitud/error/menu";
	}

	
	private void deleteObjectsSession(HttpSession session){
		session.removeAttribute("errorClabe");
		session.removeAttribute("errorPorcentaje");
		session.removeAttribute("errorList");
	}
	
	
	
	/**
	 * calcula el monto solicitado
	 * @param cliente
	 * @param solicitud
	 * @return
	 */
	private double calculaMonto(Cliente cliente, Solicitud solicitud){
		
		try{
			
			if(cliente.getSaldo() == null || cliente.getSaldo().doubleValue() == 0){
				return 0;
			}
			
			Double monto = cliente.getSaldo().doubleValue() * (solicitud.getPorcentaje().doubleValue()/100);
			
			return monto;
		}catch(Exception e){
			//e.printStackTrace();
		}
		return 0;
	}

	
}
