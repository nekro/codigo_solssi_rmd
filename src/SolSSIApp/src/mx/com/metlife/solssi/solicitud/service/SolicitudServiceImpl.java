package mx.com.metlife.solssi.solicitud.service;

import java.util.Calendar;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.ControlSolicitud;
import mx.com.metlife.solssi.auditoria.persistence.ControlSolicitudDAO;
import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.consulta.domain.ReporteTotales;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.solicitud.persistence.SolicitudDAO;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation=Propagation.SUPPORTS, isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
public class SolicitudServiceImpl implements SolicitudService{

	@Autowired
	private SolicitudDAO solicitudDAO;
	
	@Autowired
	private ControlSolicitudDAO controlDAO;
	
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public void save(Solicitud solicitud, String dirIP) {
		
		
		solicitud.setCveStatus(AppConstants.ESTATUS_CAPTURA);
		solicitud.setCveUserCaptura(AppConstants.CVE_USER_CLIENTE);
		solicitud.setFechaCaptura(Calendar.getInstance().getTime());
		solicitud.setFechaModificacion(Calendar.getInstance().getTime());
		solicitud.setOficio(null);
		
		ConsultaFilter filter = new ConsultaFilter();
		filter.setRfc(solicitud.getRfc());
		filter.setCuenta(String.valueOf(solicitud.getCuenta()));
		filter.setEstatus(AppConstants.ESTATUS_CAPTURA);
		List<Solicitud> solicitudes = solicitudDAO.get(filter);
		for(Solicitud sol : solicitudes){
		    solicitudDAO.updateEstatus(sol.getNumFolio(),AppConstants.ESTATUS_CANCELADA, AppConstants.ESTATUS_CAPTURA, solicitud.getRfc());
		    ControlSolicitud ctrl = this.createControlRegister(sol.getNumFolio(), AppConstants.ESTATUS_CANCELADA, dirIP);
		    controlDAO.save(ctrl);
		}
		
		solicitudDAO.save(solicitud);
		ControlSolicitud control = this.createControlRegister(solicitud.getNumFolio(), AppConstants.ESTATUS_CAPTURA , dirIP);
		controlDAO.save(control);
		
	}
	
	
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public void update(Solicitud solicitud) {
		solicitudDAO.update(solicitud);
	}
	
	@PreAuthorize("hasAnyRole('SOLICITUD_CONSULTAS', 'SOLICITUD_AUTORIZACIONES', 'AUTORIZA_OFICIOS', 'GENERA_OFICIOS', 'HISTORICOS')")
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<Solicitud> get(ConsultaFilter filtro){
		
		return solicitudDAO.get(filtro);
	}
	
	@PreAuthorize("hasAnyRole('SOLICITUD_CONSULTAS', 'SOLICITUD_AUTORIZACIONES', 'AUTORIZA_OFICIOS', 'GENERA_OFICIOS', 'HISTORICOS')")
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public List<Solicitud> get(ConsultaFilter filtro, boolean infoRechazo){
		return solicitudDAO.get(filtro, infoRechazo);
	}
	

	
	@PreAuthorize("hasAnyRole('GENERA_OFICIOS')")
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public void saveAsCaducadas(Integer[] numFolios, String user){
		for(Integer folio : numFolios){
			solicitudDAO.updateEstatus(folio, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD, AppConstants.ESTATUS_CAPTURA, user);
			
			ControlSolicitud control = new ControlSolicitud();
			control.setNumFolio(folio);
			control.setEstatus(AppConstants.ESTATUS_RECHAZADA_CADUCIDAD);
			control.setUsuario(user);
			control.setFecha(Calendar.getInstance().getTime());
			
			controlDAO.save(control);
			
		}
	}
	
	
	private ControlSolicitud createControlRegister(Integer folio, Short estatus, String ip){
		ControlSolicitud control = new ControlSolicitud();
		control.setNumFolio(folio);
		control.setEstatus(estatus);
		control.setUsuario("CLIENTE");
		
		if(StringUtils.isNotEmpty(ip)){
			if(ip.length()>20){
				control.setIp(ip.substring(0, 20));
			}else{
				control.setIp(ip);	
			}
		}
		
		
		control.setFecha(Calendar.getInstance().getTime());
		return control;
	}

	
	@PreAuthorize("hasAnyRole('SOLICITUD_CONSULTAS', 'SOLICITUD_AUTORIZACIONES', 'AUTORIZA_OFICIOS', 'GENERA_OFICIOS', 'HISTORICOS')")
	public Solicitud get(Integer numFolio) {
		Solicitud  solicitud = solicitudDAO.getById(numFolio);
		if(solicitud != null){
			solicitud.getDependientes().size();
		}
		
		solicitud.fillTrasientFields();
		
		if(solicitud.getCveStatus() == AppConstants.ESTATUS_ERROR_PROCESO || solicitud.getCveStatus() == AppConstants.ESTATUS_RECHAZO_BANCO){
			solicitud.setMotivoRechazo(solicitudDAO.getDescripcionRechazo(solicitud.getNumFolio()));
		}
		
		return solicitud;
	}
	
	
	
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public void updateEstatus(Integer numFolio, Short estatus, String userAutorizador){
		
		if(estatus == AppConstants.ESTATUS_ACEPTADO && estatus == AppConstants.ESTATUS_CANCELADA){
			return;
		}
		
		solicitudDAO.updateEstatus(numFolio, estatus, userAutorizador);
		
		ControlSolicitud control = new ControlSolicitud();
		control.setNumFolio(numFolio);
		control.setEstatus(estatus);
		control.setUsuario(userAutorizador);
		control.setFecha(Calendar.getInstance().getTime());
		
		controlDAO.save(control);
	}
	
	
	@PreAuthorize("hasRole('SOLICITUD_AUTORIZACIONES')")
	@Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	public void updateEstatusAutorizacion(Solicitud solicitud, Short estatus, String userAutorizador) throws ApplicationException{
		
		if(estatus == AppConstants.ESTATUS_ACEPTADO){
			ConsultaFilter filter = new ConsultaFilter();
			filter.setCuenta(String.valueOf(solicitud.getCuenta()));
			filter.setRfc(solicitud.getRfc());
			filter.setEstatus(AppConstants.ESTATUS_ACEPTADO);
			List list = this.get(filter);
			
			if(list.size()>0){
				throw new ApplicationException(MessageUtils.getMessage("duplicate.numero.autorizaciones"));
			}	
		}
		
		
		int results = solicitudDAO.updateEstatus(solicitud.getNumFolio(), estatus, AppConstants.ESTATUS_CAPTURA, userAutorizador);
		
		if(results == 0){
			throw new ApplicationException(MessageUtils.getMessage("autorizacion.not.rows.affected"));
		}
		
		ControlSolicitud control = new ControlSolicitud();
		control.setNumFolio(solicitud.getNumFolio());
		control.setEstatus(estatus);
		control.setUsuario(userAutorizador);
		control.setFecha(Calendar.getInstance().getTime());
		
		controlDAO.save(control);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public String getClabe(Integer numFolio){
		return solicitudDAO.getClabe(numFolio);
	}


	@Override
	@PreAuthorize("hasRole('REPORTE_TOTALES')")
	@Transactional(propagation=Propagation.NOT_SUPPORTED, readOnly=true)
	public ReporteTotales getTotales(Short dependencia) {
		return solicitudDAO.getTotales(dependencia);
	}
	
	
}
