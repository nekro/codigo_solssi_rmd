package mx.com.metlife.solssi.solicitud.persistence;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.metlife.solssi.cifrascontrol.domain.CCFactor;
import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.consulta.domain.ReporteTotales;
import mx.com.metlife.solssi.main.dao.GenericDAOImpl;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.util.AppConstants;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

@Repository
public class SolicitudDAOImpl extends GenericDAOImpl<Solicitud, Integer>
		implements SolicitudDAO {

	private static Logger logger = Logger.getLogger(SolicitudDAOImpl.class);
	
	public void save(Solicitud solicitud) {

		logger.info("guardando solicitud");

		Session session = getSession();
		session.save(solicitud);
	}

	public List<Solicitud> get(ConsultaFilter filtro){
		return this.get(filtro, false);
	}
	
	@SuppressWarnings("unchecked")
	public List<Solicitud> get(ConsultaFilter filtro, boolean infoRechazo) {

		logger.info("consultando solicitudes");
		
		CCFactor factor = null;
		

		Session session = getSession();
		Criteria criteria = session.createCriteria(Solicitud.class,"s");
		
		ProjectionList proList = Projections.projectionList();
		proList.add(Projections.property("numFolio"),"s.numFolio");
		proList.add(Projections.property("cuenta"),"s.cuenta");
		proList.add(Projections.property("rfc"),"s.rfc");
		proList.add(Projections.property("oficio"),"s.oficio");
		proList.add(Projections.property("fechaCaptura"),"s.fechaCaptura");
		proList.add(Projections.property("fechaPago"),"s.fechaPago");
		proList.add(Projections.property("cveStatus"),"s.cveStatus");
		proList.add(Projections.property("tipoTramite"),"s.tipoTramite");
		proList.add(Projections.property("porcentaje"),"s.porcentaje");
		proList.add(Projections.property("nivelPuesto"),"s.nivelPuesto");
		proList.add(Projections.property("modoPago"),"s.modoPago");
		proList.add(Projections.property("nombre"),"s.nombre");
		proList.add(Projections.property("apePaterno"),"s.apePaterno");
		proList.add(Projections.property("apeMaterno"),"s.apeMaterno");
		proList.add(Projections.property("clabe"),"s.clabe");
		proList.add(Projections.property("idProceso"),"s.idProceso");
		criteria.setProjection(proList);
		criteria.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		if (filtro.getNumFolio() != null && filtro.getNumFolio() > 0) {
			criteria.add(Restrictions.eq("numFolio", filtro.getNumFolio()));
			if (filtro.getEstatus() != null && filtro.getEstatus() > 0) {
				criteria.add(Restrictions.eq("cveStatus", filtro.getEstatus()));
			}
		} else {

			if (StringUtils.isNotEmpty(filtro.getRfc())) {
				criteria.add(Restrictions.eq("rfc", filtro.getRfc()));
			}

			if (StringUtils.isNotEmpty(filtro.getHomoclave())) {
				criteria.add(Restrictions.eq("homoclaveRfc", filtro.getHomoclave()));
			}

			if (StringUtils.isNotEmpty(filtro.getCuenta())) {
				criteria.add(Restrictions.eq("cuenta", Long.valueOf(filtro.getNumCuenta())));
			}
			
			if (filtro.getOficio() != null) {
				if(filtro.getOficio() == -1){
					criteria.add(Restrictions.isNull("oficio"));
				}else{
					criteria.add(Restrictions.eq("oficio", filtro.getOficio()));
				}	
			}
			
			if (filtro.getEstatus() != null && filtro.getEstatus() > 0 ) {
				criteria.add(Restrictions.eq("cveStatus", filtro.getEstatus()));
			}else{
				if(filtro.getEstatusList() != null && filtro.getEstatusList().length > 0){
					criteria.add(Restrictions.in("cveStatus", filtro.getEstatusList()));
				}
			}
			
		}
		
		if(filtro.getDependencia() != null){
			criteria.add(Restrictions.eq("centroTrabajo", filtro.getDependencia()));	
		}
		
		if(filtro.getMaxResults() != null && filtro.getMaxResults() > 0){
			criteria.setMaxResults(filtro.getMaxResults());
		}
		
		criteria.addOrder(Order.asc("numFolio"));
		
		List<Solicitud> solicitudes = new ArrayList<Solicitud>();
		List<Map> list = criteria.list();
		
		for(Map m : list){
			Solicitud sol = new Solicitud();
			sol.setNumFolio((Integer)m.get("s.numFolio"));
			sol.setCuenta((Long)m.get("s.cuenta"));
			sol.setRfc(String.valueOf(m.get("s.rfc")));
			sol.setOficio((Integer)m.get("s.oficio"));
			sol.setFechaCaptura((Date) m.get("s.fechaCaptura"));
			sol.setFechaPago((Date) m.get("s.fechaPago"));
			sol.setCveStatus((Short)m.get("s.cveStatus"));
			sol.setTipoTramite((Short)(m.get("s.tipoTramite")));
			sol.setPorcentaje((Short)(m.get("s.porcentaje")));
			sol.setNivelPuesto(String.valueOf(m.get("s.nivelPuesto")));
			sol.setModoPago((Short)(m.get("s.modoPago")));
			sol.setNombre(String.valueOf(m.get("s.nombre")));
			sol.setApePaterno(String.valueOf(m.get("s.apePaterno")));
			sol.setApeMaterno(String.valueOf(m.get("s.apeMaterno")));
			sol.setClabe(String.valueOf(m.get("s.clabe")));
			sol.setIdProceso((Integer)m.get("s.idProceso"));
			
			solicitudes.add(sol);
			
			if(infoRechazo){
				if(sol.getCveStatus() == AppConstants.ESTATUS_ERROR_PROCESO || sol.getCveStatus() == AppConstants.ESTATUS_RECHAZO_BANCO){
					sol.setMotivoRechazo(this.getDescripcionRechazo(sol.getNumFolio()));
				}
			}
			
		}
		
		return solicitudes;

	}
	
	
	public int updateEstatus(Integer numFolio, Short estatus, Short estatusAnt, String userAutorizador){
		
		String sql = "update Solicitud set cveStatus = :estatus, cveUserAutoriza = :user, fechaModificacion = :fecha where numFolio = :folio and cveStatus = :estatusAnt";
		Session session = getSession();
		Query query = session.createQuery(sql);
		query.setShort("estatus", estatus);
		query.setShort("estatusAnt", estatusAnt);
		query.setString("user", userAutorizador);
		query.setDate("fecha", Calendar.getInstance().getTime());
		query.setInteger("folio", numFolio);
		
		int results = query.executeUpdate();
		return results;
	}
	
	
	
	
	
	public int updateEstatus(Integer numFolio, Short estatus, String userAutorizador){
		
		String sql = "update Solicitud set cveStatus = :estatus, cveUserAutoriza = :user, fechaModificacion = :fecha where numFolio = :folio";
		Session session = getSession();
		Query query = session.createQuery(sql);
		query.setShort("estatus", estatus);
		query.setString("user", userAutorizador);
		query.setDate("fecha", Calendar.getInstance().getTime());
		query.setInteger("folio", numFolio);
		
		int results = query.executeUpdate();
		
		return results;
	}
	
	public int cancelaCapturadas(Solicitud solicitud){
		Session session = getSession();
		
		Query query = session.getNamedQuery("cancelaSolicitudesCapturadas");
		query.setParameter("estatusCancelada", AppConstants.ESTATUS_CANCELADA);
		query.setParameter("rfc", solicitud.getRfc());
		query.setParameter("cuenta", solicitud.getCuenta());
		query.setParameter("estatusCapturada", AppConstants.ESTATUS_CAPTURA);
		query.setParameter("cveUserAutoriza", AppConstants.CVE_USER_CLIENTE);
		query.setParameter("fecha", Calendar.getInstance().getTime());
		
		int result = query.executeUpdate();
		return result;
		
	}
	
	
	public boolean checkUpdate(Integer numFolio, Short estatus){
		
		String sql = "select count(numFolio) from Solicitud where numFolio = :folio and cveStatus = :estatus";
		Session session = getSession();
		Query query = session.createQuery(sql);
		query.setShort("estatus", estatus);
		query.setInteger("folio", numFolio);
		
		Long result = (Long) query.uniqueResult();
		
		if(result == null || result == 0){
			return false;
		}
		
		return true;
		
	}
	
	public String getClabe(Integer numFolio){
		Session session = getSession();
		Query query = session.getNamedQuery("selectCLABE");
		query.setParameter("folio", numFolio);
		String result = (String) query.uniqueResult();
		return result;
	}
	
	
	@SuppressWarnings("rawtypes")
	public ReporteTotales getTotales(Short dependencia){
		
		Session session = getSession();
		
		ReporteTotales report = new ReporteTotales();
		
		StringBuffer sql = new StringBuffer();
		sql.append("select sum(case when cve_estatus=1 then 1 else 0 end) as iniciadas,");
				sql.append("sum(case when (cve_estatus=3 or cve_estatus=10) then 1 else 0 end) as rechazadas,");
				sql.append("sum(case when cve_estatus=9 then 1 else 0 end) as canceladas,");
				sql.append("sum(case when cve_estatus=2 then 1 else 0 end) as aceptadas, ");
				sql.append("count(cve_estatus) total ");
				
				sql.append("from tw_solicitud");
				
		if(dependencia != null && dependencia > 0){
			sql.append(" where centro_trabajo = :dependencia");
		}
		
		Query query = session.createSQLQuery(sql.toString());
		
		if(dependencia != null && dependencia > 0){
			query.setShort("dependencia", dependencia);
		}
		
		query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		Map result = (Map) query.uniqueResult();
		
		report.setAceptadas((Integer)result.get("ACEPTADAS"));
		report.setCanceladas((Integer)result.get("CANCELADAS"));
		report.setIniciadas((Integer)result.get("INICIADAS"));
		report.setRechazadas((Integer)result.get("RECHAZADAS"));
		report.setTotal((Integer)result.get("TOTAL"));
		
		return report;
		
	}
	
	
	
	public String getDescripcionRechazo(Integer numFolio){
		
		Session session = getSession();
		
		StringBuffer sql = new StringBuffer();
		sql.append("select comentario from tw_motivos_rechazo where num_folio = :folio order by fecha_rechazo desc fetch first 1 rows only");
		
		Query query = session.createSQLQuery(sql.toString());
		query.setInteger("folio", numFolio);
		String desc = (String) query.uniqueResult();
		
		return desc;
		
	}
	
}