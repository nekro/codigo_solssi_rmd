package mx.com.metlife.solssi.solicitud.mvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.solicitud.domain.DependienteEconomico;
import mx.com.metlife.solssi.util.AppConstants;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Service
public class DependienteValidator implements Validator {
	
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String RFC_PATTERN = "^[A-Za-z]{4}[0-9]{6}$";
	private static final String CURP_PATTERN = "[A-Z]{4}[0-9]{6}[H,M][A-Z]{5}[0-9]{2}";
	
	public void validate(Object target, Errors errors) {
		
		DependienteEconomico dependiente = (DependienteEconomico)target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apePaterno", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apeMaterno", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "codigoPostal", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ciudadNacimiento", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "calle", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numExt", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ciudad", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "profesion", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rfc", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "colonia", "NotEmpty");
		
		
		if(dependiente.getCaracter() == null || dependiente.getCaracter() == 0){
			errors.rejectValue("caracter", "NotEmpty");
		}
		
		if(StringUtils.isNotEmpty(dependiente.getCurp())){
			if(!dependiente.getCurp().matches(CURP_PATTERN)){
				errors.rejectValue("curp", "NotValid");
			}
			
		}
		
		if(StringUtils.isNotEmpty(dependiente.getCodigoPostal())){
			if(dependiente.getIdPais() == AppConstants.PAIS_MEXICO){
				if(!StringUtils.isNumeric(dependiente.getCodigoPostal())){
					errors.rejectValue("codigoPostal", "NotValid");
				}else{
					Integer num = Integer.valueOf(dependiente.getCodigoPostal());
					if( num < 1000 || num > 99999){
						errors.rejectValue("codigoPostal", "NotValid");
					}
				}
			}
			
		}
		
		if(StringUtils.isNotEmpty(dependiente.getRfc())){
			if(dependiente.getRfc().length() != 10){
				errors.rejectValue("rfc", "NotValid");
			}else{
				Pattern pattern = Pattern.compile(RFC_PATTERN);
	            Matcher matcher = pattern.matcher(dependiente.getRfc());
	            if (!matcher.matches()) {
	                errors.rejectValue("rfc", "NotValid");
	            }
			}
			
		}
		
		if(dependiente.getIdPais() == 0){
			errors.rejectValue("idPais", "NotEmpty");
		}else{
			
			if(dependiente.getIdPais() != AppConstants.PAIS_MEXICO){
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "estadoExtranjero", "NotEmpty");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "municipioExtranjero", "NotEmpty");
			}else{
				if(dependiente.getIdEstado()==0){
					errors.rejectValue("idEstado", "NotEmpty");
				}
				
				if(dependiente.getIdMunicipio() == 0){
					errors.rejectValue("idMunicipio", "NotEmpty");
				}
			}
		}
		
		
		if(dependiente.getIdNacionalidad() == 0){
			errors.rejectValue("idNacionalidad", "NotEmpty");
		}
		
		if(dependiente.getIdPaisNac() == 0){
			errors.rejectValue("idPaisNac", "NotEmpty");
		}else{
			if(dependiente.getIdPaisNac() != AppConstants.PAIS_MEXICO){
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "estadoNacExtranjero", "NotEmpty");
			}else{
				if(dependiente.getIdEdoNac()==0){
					errors.rejectValue("idEdoNac", "NotEmpty");
				}
			}
			
		}
		
		if(dependiente.getCveEdoCivil() == 0){
			errors.rejectValue("cveEdoCivil", "NotEmpty");
		}
		
		if(dependiente.getCveGenero() == null){
			errors.rejectValue("cveGenero", "NotEmpty");
		}
		
		if(dependiente.getCveTipoIdentificacion() != 0){
			switch(dependiente.getCveTipoIdentificacion()){
				case CatalogoConstants.IDENT_IFE://IFE
					 if(!StringUtils.isNumeric(dependiente.getNumeroIdentificacion()) || dependiente.getNumeroIdentificacion().length() != 13){
						 errors.rejectValue("numeroIdentificacion", "IFEInvalid");
					 }
					 
					 break;
				case CatalogoConstants.IDENT_CED_PROF://cedula
					if(!StringUtils.isNumeric(dependiente.getNumeroIdentificacion()) || dependiente.getNumeroIdentificacion().length() != 7){
						 errors.rejectValue("numeroIdentificacion", "CedulaInvalid");
					 }
					 break;
				case CatalogoConstants.IDENT_PASAPORTE://pasaporte
					if(!StringUtils.isAlphanumeric(dependiente.getNumeroIdentificacion()) || 
							dependiente.getNumeroIdentificacion().length() < 9 || dependiente.getNumeroIdentificacion().length() > 10){
						 errors.rejectValue("numeroIdentificacion", "PasaporteInvalid");
					 }
					 break;
				case CatalogoConstants.IDENT_MATR_CONS://Certificado consular
					 if(!StringUtils.isNumeric(dependiente.getNumeroIdentificacion()) ||
					     dependiente.getNumeroIdentificacion().length() < 7 || dependiente.getNumeroIdentificacion().length() > 9){
						 errors.rejectValue("numeroIdentificacion", "CertificadoInvalid");
					 }
					 break;
				case CatalogoConstants.IDENT_FM2:
					if(!StringUtils.isNumeric(dependiente.getNumeroIdentificacion()) ||
							dependiente.getNumeroIdentificacion().length() != 13){
							 errors.rejectValue("numeroIdentificacion", "fm2Invalid");
					}	 
					break;
				case CatalogoConstants.IDENT_FM3:
					 if(!StringUtils.isNumeric(dependiente.getNumeroIdentificacion()) ||
						 dependiente.getNumeroIdentificacion().length() != 13){
						 errors.rejectValue("numeroIdentificacion", "fm3Invalid");
					 }	 
					 break;	 
				
			}
		}
		
		if(StringUtils.isEmpty(dependiente.getTelefonoCasa()) 
				&& StringUtils.isEmpty(dependiente.getTelefonoCelular()) 
				&& StringUtils.isEmpty(dependiente.getTelefonoTrabajo())){
			errors.rejectValue("telefonoCasa", "NotEmpty");
		}
		
		if(StringUtils.isNotEmpty(dependiente.getCorreoPersonal())){
			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(dependiente.getCorreoPersonal());
            if (!matcher.matches()) {
                errors.rejectValue("correoPersonal", "NotFormat");
            }else {
			    if(StringUtils.isEmpty(dependiente.getCorreoPersonalConfirm())){
			  	   errors.rejectValue("correoPersonalConfirm", "NotEmpty");
			    }else{
				    if(!StringUtils.equals(dependiente.getCorreoPersonal(), dependiente.getCorreoPersonalConfirm())){
					    errors.rejectValue("correoPersonalConfirm", "NotMatch");
			 	    }
			    }
			}
		}
		
		if(StringUtils.isNotEmpty(dependiente.getCorreoTrabajo())){
			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(dependiente.getCorreoPersonal());
            if (!matcher.matches()) {
                errors.rejectValue("correoTrabajo", "NotFormat");
            }else {
			    if(StringUtils.isEmpty(dependiente.getCorreoTrabajoConfirm())){
				   errors.rejectValue("correoTrabajoConfirm", "NotEmpty");
			    }else{
				    if(!StringUtils.equals(dependiente.getCorreoTrabajo(), dependiente.getCorreoTrabajoConfirm())){
					   errors.rejectValue("correoTrabajoConfirm", "NotMatch");
				    }
			    }
			}
		}		
		
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyddMM");
			String strDate = dependiente.getAnioNacimiento() + dependiente.getDiaNacimiento() + dependiente.getMesNacimiento();
			Date date = format.parse(strDate);
			String nuevaFecha = format.format(date);
			if(!StringUtils.equals(strDate, nuevaFecha)){
				errors.rejectValue("fechaNacimiento", "NotValid");
			}else{
				if(date.after(Calendar.getInstance().getTime())){
					errors.rejectValue("fechaNacimiento", "NotValid");
				}else{
					dependiente.setFechaNacimiento(date);	
				}
			}
		} catch (ParseException e) {
			errors.rejectValue("fechaNacimiento", "NotValid");
		}
		
		
		if(StringUtils.isNotEmpty(dependiente.getTelefonoCasa())){
			if(dependiente.getTelefonoCasa().length() != 10){
				errors.rejectValue("telefonoCasa", "NotValid");	
			}
		}

		if(StringUtils.isNotEmpty(dependiente.getTelefonoTrabajo())){
			if(dependiente.getTelefonoTrabajo().length() != 10){
				errors.rejectValue("telefonoTrabajo", "NotValid");	
			}
		}
		
		if(StringUtils.isNotEmpty(dependiente.getTelefonoCelular())){
			if(dependiente.getTelefonoCelular().length() != 10){
				errors.rejectValue("telefonoCelular", "NotValid");	
			}
		}
		
		if(dependiente.getProfesion() == AppConstants.OCUPACION_OTRO){
			if(StringUtils.isEmpty(dependiente.getDetalleActividad())){
				errors.rejectValue("detalleActividad", "NotEmpty");
			}
		}
		
	}

	public boolean supports(Class klass) {
		return DependienteEconomico.class.isAssignableFrom(klass);
	}

	
	
	
	
	
	
	
}
