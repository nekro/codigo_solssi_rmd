package mx.com.metlife.solssi.solicitud.mvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.report.util.ErrorBean;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

@Service
public class SolicitudValidator {

	private static final String factores = "37137137137137137";

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	private static final String CURP_PATTERN = "[A-Z]{4}[0-9]{6}[H,M][A-Z]{5}[0-9]{2}";
	
	private static final String CLABE_PATTER = "[0-9]{18}";

	public void validate(Object target, Errors errors, HttpSession session) {

		Solicitud solicitud = (Solicitud) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apePaterno",
				"NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apeMaterno",
				"NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "codigoPostal",
				"NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ciudadNacimiento",
				"NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "calle", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,
				"numeroIdentificacion", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "numExt", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ciudad", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ciudadElaboracion",
				"NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "profesion",
				"NotEmpty");
		ValidationUtils
				.rejectIfEmptyOrWhitespace(errors, "colonia", "NotEmpty");
		ValidationUtils
				.rejectIfEmptyOrWhitespace(errors, "captcha", "NotEmpty");

		if (StringUtils.isNotEmpty(solicitud.getCodigoPostal())) {
			if (solicitud.getIdPais() == AppConstants.PAIS_MEXICO) {
				if (!StringUtils.isNumeric(solicitud.getCodigoPostal())) {
					errors.rejectValue("codigoPostal", "NotValid");
				} else {
					Integer num = Integer.valueOf(solicitud.getCodigoPostal());
					if (num < 1000 || num > 99999) {
						errors.rejectValue("codigoPostal", "NotValid");
					}
				}
			}

		}

		// if(StringUtils.isEmpty(solicitud.getNivelPuesto())){
		// errors.rejectValue("nivelPuesto", "NotEmpty");
		// }else{
		// if(!StringUtils.equals(solicitud.getNivelPuesto(),
		// solicitud.getCliente().getNivelPuesto())){
		// errors.rejectValue("nivelPuesto", "NotValid");
		// }
		// }

		if (StringUtils.isNotEmpty(solicitud.getCurp())) {
			if (!solicitud.getCurp().matches(CURP_PATTERN)) {
				errors.rejectValue("curp", "NotValid");
			}

		}

		if (solicitud.getIdPais() == 0) {
			errors.rejectValue("idPais", "NotEmpty");
		} else {

			if (solicitud.getIdPais() != AppConstants.PAIS_MEXICO) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors,
						"estadoExtranjero", "NotEmpty");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors,
						"municipioExtranjero", "NotEmpty");
			} else {
				if (solicitud.getIdEstado() == 0) {
					errors.rejectValue("idEstado", "NotEmpty");
				}

				if (solicitud.getIdMunicipio() == 0) {
					errors.rejectValue("idMunicipio", "NotEmpty");
				}
			}
		}

		if (solicitud.getIdNacionalidad() == 0) {
			errors.rejectValue("idNacionalidad", "NotEmpty");
		}

		if (solicitud.getIdPaisNac() == 0) {
			errors.rejectValue("idPaisNac", "NotEmpty");
		} else {
			if (solicitud.getIdPaisNac() != AppConstants.PAIS_MEXICO) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors,
						"estadoNacExtranjero", "NotEmpty");
			} else {
				if (solicitud.getIdEdoNac() == 0) {
					errors.rejectValue("idEdoNac", "NotEmpty");
				}
			}

		}

		if (solicitud.getEstadoElaboracion() == 0) {
			errors.rejectValue("estadoElaboracion", "NotEmpty");
		}

		if (solicitud.getCveEdoCivil() == 0) {
			errors.rejectValue("cveEdoCivil", "NotEmpty");
		}

		if (solicitud.getCveGenero() == null) {
			errors.rejectValue("cveGenero", "NotEmpty");
		}

		if (solicitud.getCveTipoIdentificacion() == 0) {
			errors.rejectValue("cveTipoIdentificacion", "NotEmpty");
		} else {
			switch (solicitud.getCveTipoIdentificacion()) {
			case CatalogoConstants.IDENT_IFE:// IFE
				if (!StringUtils.isNumeric(solicitud.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() != 13) {
					errors.rejectValue("numeroIdentificacion", "IFEInvalid");
				}

				break;
			case CatalogoConstants.IDENT_CED_PROF:// cedula
				if (!StringUtils.isNumeric(solicitud.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() != 7) {
					errors.rejectValue("numeroIdentificacion", "CedulaInvalid");
				}
				break;
			case CatalogoConstants.IDENT_PASAPORTE:// pasaporte
				if (!StringUtils.isAlphanumeric(solicitud
						.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() < 9
						|| solicitud.getNumeroIdentificacion().length() > 10) {
					errors.rejectValue("numeroIdentificacion",
							"PasaporteInvalid");
				}
				break;
			case CatalogoConstants.IDENT_MATR_CONS:// Certificado consular
				if (!StringUtils.isNumeric(solicitud.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() < 7
						|| solicitud.getNumeroIdentificacion().length() > 9) {
					errors.rejectValue("numeroIdentificacion",
							"CertificadoInvalid");
				}
			case CatalogoConstants.IDENT_FM2:
				if (!StringUtils.isNumeric(solicitud.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() != 13) {
					errors.rejectValue("numeroIdentificacion", "fm2Invalid");
				}
				break;
			case CatalogoConstants.IDENT_FM3:
				if (!StringUtils.isNumeric(solicitud.getNumeroIdentificacion())
						|| solicitud.getNumeroIdentificacion().length() != 13) {
					errors.rejectValue("numeroIdentificacion", "fm3Invalid");
				}
				break;
			}
		}

		if (StringUtils.isEmpty(solicitud.getTelefonoCasa())
				&& StringUtils.isEmpty(solicitud.getTelefonoCelular())
				&& StringUtils.isEmpty(solicitud.getTelefonoTrabajo())) {
			errors.rejectValue("telefonoCasa", "NotEmpty");
		}

		if (StringUtils.isEmpty(solicitud.getCorreoPersonal())
				&& StringUtils.isEmpty(solicitud.getCorreoTrabajo())) {
			errors.rejectValue("correoPersonal", "NotEmpty");
		} else {
			if (StringUtils.isNotEmpty(solicitud.getCorreoPersonal())) {
				if (StringUtils.isEmpty(solicitud.getCorreoPersonalConfirm())) {
					errors.rejectValue("correoPersonalConfirm", "NotEmpty");
				} else {
					if (!StringUtils.equals(solicitud.getCorreoPersonal(),
							solicitud.getCorreoPersonalConfirm())) {
						errors.rejectValue("correoPersonalConfirm", "NotMatch");
					}
				}
			}

			if (StringUtils.isNotEmpty(solicitud.getCorreoTrabajo())) {
				if (StringUtils.isEmpty(solicitud.getCorreoTrabajoConfirm())) {
					errors.rejectValue("correoTrabajoConfirm", "NotEmpty");
				} else {
					if (!StringUtils.equals(solicitud.getCorreoTrabajo(),
							solicitud.getCorreoTrabajoConfirm())) {
						errors.rejectValue("correoTrabajoConfirm", "NotMatch");
					}
				}
			}

		}

		if (solicitud.isContribuyenteEUA()) {
			if (StringUtils.isEmpty(solicitud.getNumSsn())) {
				errors.rejectValue("numSsn", "NotEmpty");
			} else {
				if (solicitud.getNumSsn().length() != 11) {
					errors.rejectValue("numSsn", "NotFormat");
				} else {
					if (!StringUtils.isNumeric(solicitud.getNumSsn())) {
						errors.rejectValue("numSsn", "NotFormat");
					}
				}
			}
		}

		if (solicitud.getTipoTramite() == null) {
			errors.rejectValue("tipoTramite", "NotEmpty");
		} else {

			if (solicitud.getTipoTramite() == AppConstants.TRAMITE_RESCATE_PARCIAL) {
				if (solicitud.getPorcentaje() == null
						|| solicitud.getPorcentaje() <= 0) {
					errors.rejectValue("porcentaje", "NotEmpty");
				} else {
					int maxPorc = solicitud.getCliente().getMaxPorcentaje();
					if (solicitud.getPorcentaje() > maxPorc) {
						Object[] arg = new Object[] { maxPorc };
						errors.rejectValue("porcentaje", "Overflow", arg, "");
						session.setAttribute("errorPorcentaje", "true");
					}
				}
			}

			if (solicitud.getTipoTramite() == AppConstants.TRAMITE_APORTACIONES_VOLUNTARIAS) {
				if (solicitud.getPorcentajeAportVol() == null
						|| solicitud.getPorcentajeAportVol() <= 0) {
					errors.rejectValue("porcentajeAportVol", "NotEmpty");
				} else {
					if (solicitud.getPorcentajeAportVol() > 100) {
						errors.rejectValue("porcentajeAportVol", "Overflow");
					}
				}
			}

			if (solicitud.getTipoTramite() == AppConstants.TRAMITE_RESCATE_TOTAL) {
				solicitud.setPorcentaje((short) 100);
			}

			if (solicitud.getTipoTramite() == AppConstants.TRAMITE_RESCATE_PARCIAL_MAS_APOR_VOL) {
				if (solicitud.getPorcentaje() == null
						|| solicitud.getPorcentaje() <= 0) {
					errors.rejectValue("porcentaje", "NotEmpty");
				} else {
					if (solicitud.getPorcentaje() > 50) {
						errors.rejectValue("porcentaje", "Overflow");
					}
				}

				if (solicitud.getPorcentajeAportVol() == null
						|| solicitud.getPorcentajeAportVol() <= 0) {
					errors.rejectValue("porcentajeAportVol", "NotEmpty");
				} else {
					if (solicitud.getPorcentajeAportVol() > 100) {
						errors.rejectValue("porcentajeAportVol", "Overflow");
					}
				}
			}
		}

		if (StringUtils.isNotEmpty(solicitud.getCorreoPersonal())) {
			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
			Matcher matcher = pattern.matcher(solicitud.getCorreoPersonal());
			if (!matcher.matches()) {
				errors.rejectValue("correoPersonal", "NotFormat");
			}

		}

		if (StringUtils.isNotEmpty(solicitud.getCorreoTrabajo())) {
			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
			Matcher matcher = pattern.matcher(solicitud.getCorreoTrabajo());
			if (!matcher.matches()) {
				errors.rejectValue("correoTrabajo", "NotFormat");
			}

		}

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyyddMM");
			String strDate = solicitud.getAnioNacimiento()
					+ solicitud.getDiaNacimiento()
					+ solicitud.getMesNacimiento();
			Date date = format.parse(strDate);
			String nuevaFecha = format.format(date);
			if (!StringUtils.equals(strDate, nuevaFecha)) {
				errors.rejectValue("fechaNacimiento", "NotValid");
			} else {
				if (date.after(Calendar.getInstance().getTime())) {
					errors.rejectValue("fechaNacimiento", "NotValid");
				} else {
					solicitud.setFechaNacimiento(date);
				}
			}
		} catch (ParseException e) {
			errors.rejectValue("fechaNacimiento", "NotValid");
		}

		if (StringUtils.isNotEmpty(solicitud.getTelefonoCasa())) {
			if (solicitud.getTelefonoCasa().length() != 10) {
				errors.rejectValue("telefonoCasa", "NotValid");
			}
		}

		if (StringUtils.isNotEmpty(solicitud.getTelefonoTrabajo())) {
			if (solicitud.getTelefonoTrabajo().length() != 10) {
				errors.rejectValue("telefonoTrabajo", "NotValid");
			}
		}

		if (StringUtils.isNotEmpty(solicitud.getTelefonoCelular())) {
			if (solicitud.getTelefonoCelular().length() != 10) {
				errors.rejectValue("telefonoCelular", "NotValid");
			}
		}

		// if(solicitud.getCentroTrabajo() == 0){
		// errors.rejectValue("centroTrabajo", "NotEmpty");
		// }

		if (solicitud.getModoPago() == AppConstants.MODO_PAGO_TRANSFERENCIA) {

			if (solicitud.getBanco() == 0) {
				errors.rejectValue("banco", "NotEmpty");
			}

			if (solicitud.getClabe() == null) {
				errors.rejectValue("clabe", "NotEmpty");
			} else {
				if (solicitud.getClabe().matches(CLABE_PATTER)) {

					String banco = StringUtils.leftPad(
							String.valueOf(solicitud.getBanco()), 3, '0');
					String firstDigits = solicitud.getClabe().substring(0, 3);
					if (!StringUtils.equals(banco, firstDigits)) {
						errors.rejectValue("clabe", "NotBanco");
					} else {
						if (!this.validaDigitoVerificadorCLABE(solicitud
								.getClabe())) {
							errors.rejectValue("clabe", "NotDigitoVerificador");
						} else {
							// si en la base estan los ultimos digitos, se
							// validan
							if (StringUtils.isNotEmpty(solicitud.getCliente()
									.getUltimosDigitosClabe())) {
								String ult = solicitud.getClabe().substring(14);
								String bd = StringUtils.leftPad(String
										.valueOf(solicitud.getCliente()
												.getUltimosDigitosClabe()), 4,
										'0');
								if (!StringUtils.equals(ult, bd)) {
									errors.rejectValue("clabe", "NotValid");
									session.setAttribute("errorClabe","true");
								}
							}
						}
					}
				} else {
					errors.rejectValue("clabe", "NotLength");
				}

			}
		} else {

			solicitud.setClabe(StringUtils.EMPTY);
			solicitud.setBanco(null);

			if (!solicitud.isCheque()) {
				errors.rejectValue("cheque", "NotEmpty");
			} else {
				if (solicitud.getEstadoPromotoria() == 0) {
					errors.rejectValue("estadoPromotoria", "NotEmpty");
				} else {
					if (solicitud.getClavePromotoria() == null
							|| solicitud.getClavePromotoria() == 0) {
						errors.rejectValue("clavePromotoria", "NotEmpty");
					}
				}
			}
		}

	}

	public boolean validaDigitoVerificadorCLABE(String clabe) {

		StringBuffer productos = new StringBuffer(17);
		for (int i = 0; i < 17; i++) {
			int digito = Integer.valueOf(clabe.substring(i, i + 1));
			int factor = Integer.valueOf(factores.substring(i, i + 1));
			int producto = (digito * factor) % 10;
			productos.append(producto);
		}

		int suma = 0;
		String prod = productos.toString();
		for (int i = 0; i < 17; i++) {
			int digito = Integer.valueOf(prod.substring(i, i + 1));
			suma += digito;
		}

		suma = suma % 10;

		suma = 10 - suma;

		int verificador = suma % 10;

		int ultimo = Integer.valueOf(clabe.substring(17, 18));

		return (ultimo == verificador);
	}

	public boolean validaCaptcha(Solicitud solicitud, HttpSession session) {
		if (session != null) {
			String captcha = (String) session
					.getAttribute("sessionCaptchaCode");
			if (StringUtils.isNotEmpty(captcha)) {
				if (!StringUtils.equals(captcha, solicitud.getCaptcha())) {
					return false;
				}
			}else{
				return false;
			}
			return true;
		} else {
			return false;
		}
	}

}
