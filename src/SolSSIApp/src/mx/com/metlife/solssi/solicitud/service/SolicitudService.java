package mx.com.metlife.solssi.solicitud.service;

import java.util.List;

import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.consulta.domain.ReporteTotales;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.oficio.domain.ControlCode;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;

public interface SolicitudService{

    public void save(Solicitud solicitud, String dirIP);
    public void update(Solicitud solicitud);
    public List<Solicitud> get(ConsultaFilter filtro);
    public List<Solicitud> get(ConsultaFilter filtro, boolean infoRechazo);
    public Solicitud get(Integer numFolio);
    public void updateEstatus(Integer numFolio, Short estatus, String userAutorizador);
    public void saveAsCaducadas(Integer[] numFolios, String user);
    public void updateEstatusAutorizacion(Solicitud solicitud, Short estatus, String userAutorizador) throws ApplicationException;
    public String getClabe(Integer numFolio);
    public ReporteTotales getTotales(Short dependencia);
}
