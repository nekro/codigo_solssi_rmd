package mx.com.metlife.solssi.solicitud.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name = "TW_SOLICITUD_DEPENDIENTES")

public class DependienteEconomico implements java.io.Serializable{
	
	@EmbeddedId
	private DependientePK pk;
	
	
	@Transient
	private Solicitud solicitud;
	
	@Column(name="NUM_DEPENDIENTE", insertable=false, updatable=false)
	private Short numero;
	
	
	@Column(name="CVE_CARACTER")
	private Short caracter;
	
	private String rfc;

	@Column(name="HOMOCLAVE_RFC")
	private String homoclaveRfc;
	
	private String nombre;

	@Column(name="APE_PATERNO")
	private String apePaterno;

	@Column(name="APE_MATERNO")
	private String apeMaterno;

	@Column(name="FECHA_NACIMIENTO")
	private Date fechaNacimiento;
	
	@Column(name="CIUDAD_NACIMIENTO")
	private String ciudadNacimiento;

	@Column(name="ID_EDO_NAC")
	private Short idEdoNac;

	@Column(name="ID_PAIS_NAC")
	private Short idPaisNac;

	@Column(name="ID_NACIONALIDAD")
	private Short idNacionalidad;

	@Column(name="CVE_GENERO")
	private Short cveGenero;

	@Column(name="CVE_EDO_CIVIL")
	private Short cveEdoCivil;

	@Column(name="CVE_TIPO_IDENTIFICACION")
	private Short cveTipoIdentificacion;

	@Column(name="NUMERO_IDENTIFICACION")
	private String numeroIdentificacion;

	@Column(name="CVE_PROFESION")
	private Short profesion;

	@Column(name="CENTRO_TRABAJO")
	private String centroTrabajo;
	
	@Column(name="DETALLE_ACTIVIDAD")
	private String detalleActividad;

	private String calle;

	@Column(name="NUM_EXT")
	private String numExt;

	@Column(name="NUM_INT")
	private String numInt;

	@Column(name="COLONIA")
	private String colonia;

	@Column(name="CIUDAD")
	private String ciudad;

	@Column(name="ID_MUNICIPIO")
	private Short idMunicipio;

	@Column(name="ID_ESTADO")
	private Short idEstado;

	@Column(name="ID_PAIS")
	private Short idPais;

	@Column(name="CODIGO_POSTAL")
	private String codigoPostal;

	@Column(name="TELEFONO_CASA")
	private String telefonoCasa;

	@Column(name="TELEFONO_TRABAJO")
	private String telefonoTrabajo;

	@Column(name="TEL_TRABAJO_EXT")
	private String extension;
	
	@Column(name="TELEFONO_CELULAR")
	private String telefonoCelular;

	@Column(name="CORREO_PERSONAL")
	private String correoPersonal;

	@Column(name="CORREO_TRABAJO")
	private String correoTrabajo;

	@Column(name="CURP")
	private String curp;
	
	//@Column(name="EDO_NAC_EXT")
	@Transient
	private String estadoNacExtranjero;
	
	@Transient
	//@Column(name="ESTADO_EXT")
	private String estadoExtranjero;
	
	@Transient
	//@Column(name="MUNICIPIO_EXT")
	private String municipioExtranjero;
	
	@Transient private String diaNacimiento;
	@Transient private String mesNacimiento;
	@Transient private String anioNacimiento;
	@Transient private String correoPersonalConfirm;
	@Transient private String correoTrabajoConfirm;
	
	
	private static final long serialVersionUID = 1L;

	public DependienteEconomico(){
		this.numero = 0;
	}
	
	
	public Short getNumero() {
		return this.numero;
	}
	
	public void setNumero(Short numero) {
		this.numero = numero;
	}

	public Short getCaracter() {
		return caracter;
	}

	public void setCaracter(Short caracter) {
		this.caracter = caracter;
	}

	public String getRfc() {
		return StringUtils.upperCase(StringUtils.trim(rfc));
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getHomoclaveRfc() {
		return StringUtils.upperCase(StringUtils.trim(homoclaveRfc));
	}

	public void setHomoclaveRfc(String homoclaveRfc) {
		this.homoclaveRfc = homoclaveRfc;
	}

	
	public String getNombre() {
		return StringUtils.upperCase(StringUtils.trim(nombre));
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApePaterno() {
		return StringUtils.upperCase(StringUtils.trim((apePaterno)));
	}

	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	public String getApeMaterno() {
		return StringUtils.upperCase(StringUtils.trim((apeMaterno)));
	}

	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getCiudadNacimiento() {
		return StringUtils.upperCase(StringUtils.trim((ciudadNacimiento)));
	}

	public void setCiudadNacimiento(String ciudadNacimiento) {
		this.ciudadNacimiento = ciudadNacimiento;
	}

	public Short getIdEdoNac() {
		return idEdoNac;
	}

	public void setIdEdoNac(Short idEdoNac) {
		this.idEdoNac = idEdoNac;
	}

	public Short getIdPaisNac() {
		return idPaisNac;
	}

	public void setIdPaisNac(Short idPaisNac) {
		this.idPaisNac = idPaisNac;
	}

	public Short getIdNacionalidad() {
		return idNacionalidad;
	}

	public void setIdNacionalidad(Short idNacionalidad) {
		this.idNacionalidad = idNacionalidad;
	}

	public Short getCveGenero() {
		return cveGenero;
	}

	public void setCveGenero(Short cveGenero) {
		this.cveGenero = cveGenero;
	}

	public Short getCveEdoCivil() {
		return cveEdoCivil;
	}

	public void setCveEdoCivil(Short cveEdoCivil) {
		this.cveEdoCivil = cveEdoCivil;
	}

	public Short getCveTipoIdentificacion() {
		return cveTipoIdentificacion;
	}

	public void setCveTipoIdentificacion(Short cveTipoIdentificacion) {
		this.cveTipoIdentificacion = cveTipoIdentificacion;
	}

	public String getNumeroIdentificacion() {
		return StringUtils.upperCase(StringUtils.trim((numeroIdentificacion)));
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public Short getProfesion() {
		return profesion;
	}

	public void setProfesion(Short profesion) {
		this.profesion = profesion;
	}

	public String getCentroTrabajo() {
		return StringUtils.upperCase(StringUtils.trim((centroTrabajo)));
	}

	public void setCentroTrabajo(String centroTrabajo) {
		this.centroTrabajo = centroTrabajo;
	}

	public String getCalle() {
		return StringUtils.upperCase(StringUtils.trim((calle)));
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumExt() {
		return StringUtils.upperCase(StringUtils.trim((numExt)));
	}

	public void setNumExt(String numExt) {
		this.numExt = numExt;
	}

	public String getNumInt() {
		return StringUtils.upperCase(StringUtils.trim((numInt)));
	}

	public void setNumInt(String numInt) {
		this.numInt = numInt;
	}

	
	public String getCiudad() {
		return StringUtils.upperCase(StringUtils.trim((ciudad)));
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Short getIdMunicipio() {
		if(this.idMunicipio == null){
			this.idMunicipio = 0;
		}
		return idMunicipio;
	}

	public void setIdMunicipio(Short idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public Short getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Short idEstado) {
		this.idEstado = idEstado;
	}

	public Short getIdPais() {
		return idPais;
	}

	public void setIdPais(Short idPais) {
		this.idPais = idPais;
	}

	public String getCodigoPostal() {
		return StringUtils.upperCase(StringUtils.trim((codigoPostal)));
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getTelefonoCasa() {
		return StringUtils.trim(telefonoCasa);
	}

	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}

	public String getTelefonoTrabajo() {
		return StringUtils.trim(telefonoTrabajo);
	}

	public void setTelefonoTrabajo(String telefonoTrabajo) {
		this.telefonoTrabajo = telefonoTrabajo;
	}

	public String getExtension() {
		return StringUtils.trim(extension);
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getTelefonoCelular() {
		return telefonoCelular;
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}

	public String getCorreoPersonal() {
		return StringUtils.trim(correoPersonal);
	}

	public void setCorreoPersonal(String correoPersonal) {
		this.correoPersonal = correoPersonal;
	}

	public String getCorreoTrabajo() {
		return StringUtils.trim(correoTrabajo);
	}

	public void setCorreoTrabajo(String correoTrabajo) {
		this.correoTrabajo = correoTrabajo;
	}


	public String getCurp() {
		return StringUtils.upperCase(StringUtils.trim(curp));
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	
	
	public void fillForTest(){
		
	    this.caracter = 1;
	    this.mesNacimiento = "09";
	    this.anioNacimiento = "1995";
	    this.diaNacimiento = "21";
		this.nombre = "RODRIGO";
		this.apePaterno ="NERI";
		this.apeMaterno = "SALDIVAR";
		this.rfc = "NESR800101";
		this.homoclaveRfc = "2K5";
		this.calle = "ZAPATA";
		this.centroTrabajo ="GCIT";
		this.ciudadNacimiento="YAUTEPEC";
		this.codigoPostal="62736";
		this.correoPersonal = "roddxns@gmail.com";
		this.correoPersonalConfirm = "roddxns@gmail.com";
		this.cveEdoCivil = new Short((short)1);
		this.cveGenero = new Short((short)1);
		this.profesion = 1;
		this.idEstado = new Short((short)1);
		this.idMunicipio = new Short((short)1);
		this.idEdoNac = new Short((short)1);
		this.idPais = new Short((short)1);
		this.idPaisNac = new Short((short)1);
		this.numeroIdentificacion = "1234561";
		this.numExt = "1A";
		this.numInt = "1INT";
		this.telefonoCasa = "7351035558";
		this.extension = "123";
		this.cveTipoIdentificacion = 2;
		this.ciudad = "MEXICO";
		this.colonia = "juarez";
		this.fechaNacimiento = new Date();
		this.idNacionalidad = 1;
	}

	/**
	 * @return the solicitud
	 */
	public Solicitud getSolicitud() {
		return solicitud;
	}

	/**
	 * @param solicitud the solicitud to set
	 */
	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}
	
	/**
	 * @return the pk
	 */
	public DependientePK getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(DependientePK pk) {
		this.pk = pk;
	}

	/**
	 * @return the colonia
	 */
	public String getColonia() {
		return colonia;
	}

	/**
	 * @param colonia the colonia to set
	 */
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}


	public String getDiaNacimiento() {
		return diaNacimiento;
	}


	public void setDiaNacimiento(String diaNacimiento) {
		this.diaNacimiento = diaNacimiento;
	}


	public String getMesNacimiento() {
		return mesNacimiento;
	}


	public void setMesNacimiento(String mesNacimiento) {
		this.mesNacimiento = mesNacimiento;
	}


	public String getAnioNacimiento() {
		return anioNacimiento;
	}


	public void setAnioNacimiento(String anioNacimiento) {
		this.anioNacimiento = anioNacimiento;
	}


	public String getEstadoNacExtranjero() {
		return StringUtils.upperCase(StringUtils.trim((estadoNacExtranjero)));
	}


	public void setEstadoNacExtranjero(String estadoNacExtranjero) {
		this.estadoNacExtranjero = estadoNacExtranjero;
	}


	public String getEstadoExtranjero() {
		return StringUtils.upperCase(StringUtils.trim((estadoExtranjero)));
	}


	public void setEstadoExtranjero(String estadoExtranjero) {
		this.estadoExtranjero = estadoExtranjero;
	}


	public String getMunicipioExtranjero() {
		return StringUtils.upperCase(StringUtils.trim((municipioExtranjero)));
	}


	public void setMunicipioExtranjero(String municipioExtranjero) {
		this.municipioExtranjero = municipioExtranjero;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof DependienteEconomico)) {
			return false;
		}
		DependienteEconomico other = (DependienteEconomico) obj;
		if (numero == null) {
			if (other.numero != null) {
				return false;
			}
		} else if (!numero.equals(other.numero)) {
			return false;
		}
		return true;
	}
	
	

	/**
	 * 
	 * @return
	 */
	public String getDetalleActividad() {
		return detalleActividad;
	}


	/**
	 * 
	 * @param detalleActividad
	 */
	public void setDetalleActividad(String detalleActividad) {
		this.detalleActividad = detalleActividad;
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DependienteEconomico [numero=");
		builder.append(numero);
		builder.append(", caracter=");
		builder.append(caracter);
		builder.append(", rfc=");
		builder.append(rfc);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", apePaterno=");
		builder.append(apePaterno);
		builder.append(", apeMaterno=");
		builder.append(apeMaterno);
		builder.append(", fechaNacimiento=");
		builder.append(fechaNacimiento);
		builder.append("]");
		return builder.toString();
	}


	/**
	 * @return el correoPersonalConfirm
	 */
	public String getCorreoPersonalConfirm() {
		return correoPersonalConfirm;
	}


	/**
	 * @param correoPersonalConfirm el correoPersonalConfirm a establecer
	 */
	public void setCorreoPersonalConfirm(String correoPersonalConfirm) {
		this.correoPersonalConfirm = correoPersonalConfirm;
	}


	/**
	 * @return el correoTrabajoConfirm
	 */
	public String getCorreoTrabajoConfirm() {
		return correoTrabajoConfirm;
	}


	/**
	 * @param correoTrabajoConfirm el correoTrabajoConfirm a establecer
	 */
	public void setCorreoTrabajoConfirm(String correoTrabajoConfirm) {
		this.correoTrabajoConfirm = correoTrabajoConfirm;
	}
	
	
	
	
}
