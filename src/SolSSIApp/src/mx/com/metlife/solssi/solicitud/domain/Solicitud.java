package mx.com.metlife.solssi.solicitud.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.Base26Codec;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name="TW_SOLICITUD")
@NamedQueries(
	{
		@NamedQuery(name="getSolicitudCapturada", 
				query = "SELECT count(s.cuenta) FROM Solicitud s WHERE s.rfc = :rfc and s.cuenta = :cuenta and s.cveStatus in(:listEstatus)"),
		
		@NamedQuery(name="existeSolicitudEnTramite", 
				query = "SELECT count(s.cuenta) FROM Solicitud s WHERE s.rfc = :rfc and s.cuenta = :cuenta and (s.cveStatus not in(:listEstatus) or s.oficio is not null)"),
				
		@NamedQuery(name="existeSolicitudEnTramiteFF", 
				query = "SELECT count(s.cuenta) FROM Solicitud s WHERE s.rfc = :rfc and s.cuenta = :cuenta and s.cveStatus not in(:listEstatus)"),		
		
		@NamedQuery(name="updateOficio", 
						query = "UPDATE Solicitud s SET s.oficio = :oficio WHERE s.numFolio = :folio and oficio is null"),
						
		@NamedQuery(name="marcarSolicitudesEnviadas", 
				query = "UPDATE Solicitud s SET s.cveStatus = :estatusEnviado WHERE oficio = :oficio and cveStatus = :estatusAceptado"),
				
		@NamedQuery(name="cancelaSolicitudesCapturadas", 
						query = "UPDATE Solicitud SET cveStatus = :estatusCancelada, cveUserAutoriza =:user, fechaModificacion =:fecha  WHERE rfc = :rfc and cuenta = :cuenta and cveStatus = :estatusCapturada"),		
		
		@NamedQuery(name="selectCLABE", 
						query = "SELECT clabe FROM Solicitud WHERE numFolio = :folio")		
	})		
public class Solicitud implements Serializable {
	
	private static final long serialVersionUID = 2181997926462319029L;

	@Id
	@Column(name="NUM_FOLIO")
	@GeneratedValue
	private Integer numFolio;

	private Long cuenta;

	private String rfc;

	@Column(name="HOMOCLAVE_RFC")
	private String homoclaveRfc;
	
	@Column(name="ID_ESTADO_ELABORACION")
	private Short estadoElaboracion;
	
	@Column(name="CIUDAD_ELABORACION")
	private String ciudadElaboracion;

	@Column(name="TIPO_TRAMITE")
	private Short tipoTramite;

	@Column(name="MONTO_SOLICITADO")
	private Double montoSolicitado;
	
	@Column(name="PORCENTAJE")
	private Short porcentaje;
	
	@Column(name="PORCENTAJE_APORT_VOL")
	private Short porcentajeAportVol;

	private String nombre;

	@Column(name="APE_PATERNO")
	private String apePaterno;

	@Column(name="APE_MATERNO")
	private String apeMaterno;

	@Column(name="FECHA_NACIMIENTO")
	private Date fechaNacimiento;
	
	@Column(name="CIUDAD_NACIMIENTO")
	private String ciudadNacimiento;

	@Column(name="ID_EDO_NAC")
	private Short idEdoNac;

	@Column(name="EDO_NAC_EXT")
	private String estadoNacExtranjero; 
	
	@Column(name="ID_PAIS_NAC")
	private Short idPaisNac;

	@Column(name="ID_NACIONALIDAD")
	private Short idNacionalidad;

	@Column(name="CVE_GENERO")
	private Short cveGenero;

	@Column(name="CVE_EDO_CIVIL")
	private Short cveEdoCivil;

	@Column(name="CVE_TIPO_IDENTIFICACION")
	private Short cveTipoIdentificacion;

	@Column(name="NUMERO_IDENTIFICACION")
	private String numeroIdentificacion;

	@Column(name="BND_CONTRIBUY_EUA")
	private boolean contribuyenteEUA;

	@Column(name="NUM_SSN")
	private String numSsn;

	@Column(name="PROFESION")
	private String profesion;

	@Column(name="CENTRO_TRABAJO")
	private Short centroTrabajo;

	private String calle;

	@Column(name="NUM_EXT")
	private String numExt;

	@Column(name="NUM_INT")
	private String numInt;

	@Column(name="COLONIA")
	private String colonia;

	@Column(name="CIUDAD")
	private String ciudad;

	@Column(name="ID_MUNICIPIO")
	private Short idMunicipio;

	@Column(name="ID_ESTADO")
	private Short idEstado;

	@Column(name="ID_PAIS")
	private Short idPais;
	
	@Column(name="ESTADO_EXT")
	private String estadoExtranjero;
	
	@Column(name="MUNICIPIO_EXT")
	private String municipioExtranjero;

	@Column(name="CODIGO_POSTAL")
	private String codigoPostal;

	@Column(name="TELEFONO_CASA")
	private String telefonoCasa;

	@Column(name="TELEFONO_TRABAJO")
	private String telefonoTrabajo;

	@Column(name="TEL_TRABAJO_EXT")
	private String extension;
	
	@Column(name="TELEFONO_CELULAR")
	private String telefonoCelular;

	@Column(name="CORREO_PERSONAL")
	private String correoPersonal;
	
	@Column(name="NIVEL_PUESTO")
	private String nivelPuesto;

	@Column(name="CORREO_TRABAJO")
	private String correoTrabajo;

	@Column(name="BND_PPE")
	private boolean politicamenteExpuesta;

	@Column(name="CVE_BANCO")
	private Short banco;
	
	@Column(name="CLABE")
	private String clabe;
	
	@Column(name="CVE_CENTRO_SERVICIO")
	private Short caja;
	
	@Column(name="CVE_PROMOTORIA")
	private Short clavePromotoria;
	
	@Column(name="ID_ESTADO_PROMOTORIA")
	private Short estadoPromotoria;
	
	@Column(name="BND_PENSIONADO")
	private boolean pensionado;

	@Column(name="BND_PRESTAMO")
	private boolean prestamo;

	@Column(name="FECHA_CAPTURA")
	private Date fechaCaptura;

	@Column(name="FECHA_MODIFICACION")
	private Date fechaModificacion;

	@Column(name="CVE_ESTATUS")
	private Short cveStatus;

	@Column(name="CVE_USER_CAPTURA")
	private String cveUserCaptura;

	@Column(name="CVE_USER_AUTORIZA")
	private String cveUserAutoriza;
	
	@Column(name="MODO_PAGO")
	private Short modoPago;

	@Column(name="CURP")
	private String curp;
	
	@Column(name="NUM_OFICIO")
	private Integer oficio;
	
	@Column(name="ID_PROCESO")
	private Integer idProceso;
	
	@Column(name="FECHA_PAGO")
	@Temporal(TemporalType.DATE)
	private Date fechaPago;
	
	@Column(name="BND_ULT_DIGITOS_SSI")
	private boolean bndUltimosDigitosClabe;
	
	@Transient private String userFolio;
	@Transient private String captcha;
	@Transient private String diaNacimiento;
	@Transient private String mesNacimiento;
	@Transient private String anioNacimiento;
	@Transient private boolean avisoPrivacidad;
	@Transient private boolean reCaptura;
	@Transient private boolean cheque;
	@Transient private Cliente cliente;
	@Transient private String correoPersonalConfirm;
	@Transient private String correoTrabajoConfirm;
	
	@Transient private String motivoRechazo;
	
	@OneToMany(mappedBy="pk.solicitud", cascade=CascadeType.ALL)
	private List<DependienteEconomico> dependientes;
	
	@Transient
	private boolean declaracionNoDependientes;
	
	public Solicitud() {
		this.modoPago = AppConstants.MODO_PAGO_TRANSFERENCIA;
	}

	public Integer getNumFolio() {
		return this.numFolio;
	}

	public void setNumFolio(Integer numFolio) {
		this.numFolio = numFolio;
	}

	public Long getCuenta() {
		return this.cuenta;
	}

	public void setCuenta(Long cuenta) {
		this.cuenta = cuenta;
	}

	public String getRfc() {
		return StringUtils.upperCase(StringUtils.upperCase(this.rfc));
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getHomoclaveRfc() {
		return this.homoclaveRfc;
	}

	public void setHomoclaveRfc(String homoclaveRfc) {
		this.homoclaveRfc = homoclaveRfc;
	}

	public Short getTipoTramite() {
		return this.tipoTramite;
	}

	public void setTipoTramite(Short tipoTramite) {
		this.tipoTramite = tipoTramite;
	}

	public Double getMontoSolicitado() {
		return this.montoSolicitado;
	}

	public void setMontoSolicitado(Double montoSolicitado) {
		this.montoSolicitado = montoSolicitado;
	}

	public String getNombre() {
		return StringUtils.upperCase(StringUtils.trim(this.nombre));
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApePaterno() {
		return StringUtils.upperCase(StringUtils.trim(this.apePaterno));
	}

	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	public String getApeMaterno() {
		return StringUtils.upperCase(StringUtils.upperCase(StringUtils.trim(this.apeMaterno)));
	}

	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	public String getCiudadNacimiento() {
		return StringUtils.upperCase(StringUtils.trim(this.ciudadNacimiento));
	}

	public void setCiudadNacimiento(String ciudadNacimiento) {
		this.ciudadNacimiento = ciudadNacimiento;
	}

	public Short getIdEdoNac() {
		return this.idEdoNac;
	}

	public void setIdEdoNac(Short idEdoNac) {
		this.idEdoNac = idEdoNac;
	}

	public Short getIdPaisNac() {
		return this.idPaisNac;
	}

	public void setIdPaisNac(Short idPaisNac) {
		this.idPaisNac = idPaisNac;
	}

	public Short getIdNacionalidad() {
		return this.idNacionalidad;
	}

	public void setIdNacionalidad(Short idNacionalidad) {
		this.idNacionalidad = idNacionalidad;
	}

	public Short getCveGenero() {
		return this.cveGenero;
	}

	public void setCveGenero(Short cveGenero) {
		this.cveGenero = cveGenero;
	}

	public Short getCveEdoCivil() {
		return this.cveEdoCivil;
	}

	public void setCveEdoCivil(Short cveEdoCivil) {
		this.cveEdoCivil = cveEdoCivil;
	}

	public Short getCveTipoIdentificacion() {
		return this.cveTipoIdentificacion;
	}

	public void setCveTipoIdentificacion(Short cveTipoIdentificacion) {
		this.cveTipoIdentificacion = cveTipoIdentificacion;
	}

	public String getNumeroIdentificacion() {
		return StringUtils.upperCase(StringUtils.trim(this.numeroIdentificacion));
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public String getNumSsn() {
		return StringUtils.upperCase(StringUtils.trim(this.numSsn));
	}

	public void setNumSsn(String numSsn) {
		this.numSsn = numSsn;
	}

	public String getProfesion() {
		return StringUtils.upperCase(StringUtils.trim(this.profesion));
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public Short getCentroTrabajo() {
		return this.centroTrabajo;
	}

	public void setCentroTrabajo(Short centroTrabajo) {
		this.centroTrabajo = centroTrabajo;
	}

	public String getCalle() {
		return StringUtils.upperCase(StringUtils.trim(this.calle));
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumExt() {
		return StringUtils.upperCase(StringUtils.trim(this.numExt));
	}

	public void setNumExt(String numExt) {
		this.numExt = numExt;
	}

	public String getNumInt() {
		return StringUtils.upperCase(StringUtils.trim(this.numInt));
	}

	public void setNumInt(String numInt) {
		this.numInt = numInt;
	}

	public String getColonia() {
		return StringUtils.upperCase(StringUtils.trim(this.colonia));
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCiudad() {
		return StringUtils.upperCase(StringUtils.trim(this.ciudad));
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Short getIdMunicipio() {
		if(this.idMunicipio == null){
			this.idMunicipio = 0;
		}
		return this.idMunicipio;
	}

	public void setIdMunicipio(Short idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public Short getIdEstado() {
		return this.idEstado;
	}

	public void setIdEstado(Short idEstado) {
		this.idEstado = idEstado;
	}

	public Short getIdPais() {
		return this.idPais;
	}

	public void setIdPais(Short idPais) {
		this.idPais = idPais;
	}

	public String getCodigoPostal() {
		return StringUtils.upperCase(StringUtils.trim(this.codigoPostal));
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getTelefonoCasa() {
		return StringUtils.upperCase(StringUtils.trim(this.telefonoCasa));
	}

	public void setTelefonoCasa(String telefonoCasa) {
		this.telefonoCasa = telefonoCasa;
	}

	public String getTelefonoTrabajo() {
		return StringUtils.trim(this.telefonoTrabajo);
	}

	public void setTelefonoTrabajo(String telefonoTrabajo) {
		this.telefonoTrabajo = StringUtils.trim(telefonoTrabajo);
	}

	public String getTelefonoCelular() {
		return StringUtils.trim(this.telefonoCelular);
	}

	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = StringUtils.trim(telefonoCelular);
	}

	public String getCorreoPersonal() {
		return StringUtils.trim(this.correoPersonal);
	}

	public void setCorreoPersonal(String correoPersonal) {
		this.correoPersonal = correoPersonal;
	}

	public String getCorreoTrabajo() {
		return StringUtils.trim(this.correoTrabajo);
	}

	public void setCorreoTrabajo(String correoTrabajo) {
		this.correoTrabajo = correoTrabajo;
	}

	public Date getFechaCaptura() {
		return this.fechaCaptura;
	}

	public void setFechaCaptura(Date fechaCaptura) {
		this.fechaCaptura = fechaCaptura;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Short getCveStatus() {
		return this.cveStatus;
	}

	public void setCveStatus(Short cveStatus) {
		this.cveStatus = cveStatus;
	}

	public String getCveUserCaptura() {
		return StringUtils.trim(this.cveUserCaptura);
	}

	public void setCveUserCaptura(String cveUserCaptura) {
		this.cveUserCaptura = cveUserCaptura;
	}

	public String getCveUserAutoriza() {
		return StringUtils.trim(this.cveUserAutoriza);
	}

	public void setCveUserAutoriza(String cveUserAutoriza) {
		this.cveUserAutoriza = cveUserAutoriza;
	}

	public boolean isContribuyenteEUA() {
		return contribuyenteEUA;
	}

	public void setContribuyenteEUA(boolean contribuyenteEUA) {
		this.contribuyenteEUA = contribuyenteEUA;
	}

	public boolean isPoliticamenteExpuesta() {
		return politicamenteExpuesta;
	}

	public void setPoliticamenteExpuesta(boolean politicamenteExpuesta) {
		this.politicamenteExpuesta = politicamenteExpuesta;
	}

	public boolean isPensionado() {
		return pensionado;
	}

	public void setPensionado(boolean pensionado) {
		this.pensionado = pensionado;
	}

	public boolean isPrestamo() {
		return prestamo;
	}

	public void setPrestamo(boolean prestamo) {
		this.prestamo = prestamo;
	}

	public String getCiudadElaboracion() {
		return StringUtils.upperCase(StringUtils.trim(ciudadElaboracion));
	}

	public void setCiudadElaboracion(String ciudadElaboracion) {
		this.ciudadElaboracion = ciudadElaboracion;
	}
	
	public Short getEstadoElaboracion() {
		return estadoElaboracion;
	}

	public void setEstadoElaboracion(Short estadoElaboracion) {
		this.estadoElaboracion = estadoElaboracion;
	}

	/**
	 * @return el fechaNacimiento
	 */
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	/**
	 * @param fechaNacimiento el fechaNacimiento a establecer
	 */
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public void fillTrasientFields(){
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
		
		if(this.fechaNacimiento != null){
			String fecha = format.format(this.fechaNacimiento);
			this.diaNacimiento = fecha.substring(0,2);
			this.mesNacimiento = fecha.substring(2,4);
			this.anioNacimiento = fecha.substring(4);
		}
		
		if(dependientes != null && !dependientes.isEmpty()){
			for(DependienteEconomico de : this.dependientes){
				String fecha = format.format(de.getFechaNacimiento());
				de.setDiaNacimiento(fecha.substring(0,2));
				de.setMesNacimiento(fecha.substring(2,4));
				de.setAnioNacimiento(fecha.substring(4));
			}
			
		}
		
	}

	/**
	 * @return el curp
	 */
	public String getCurp() {
		return StringUtils.upperCase(StringUtils.trim(curp));
	}

	/**
	 * @param curp el curp a establecer
	 */
	public void setCurp(String curp) {
		this.curp = curp;
	}

	/**
	 * @return el banco
	 */
	public Short getBanco() {
		return banco;
	}

	/**
	 * @param banco el banco a establecer
	 */
	public void setBanco(Short banco) {
		this.banco = banco;
	}

	/**
	 * @return el clabe
	 */
	public String getClabe() {
		return StringUtils.trim(clabe);
	}

	/**
	 * @param clabe el clabe a establecer
	 */
	public void setClabe(String clabe) {
		this.clabe = clabe;
	}

	/**
	 * @return el caja
	 */
	public Short getCaja() {
		return caja;
	}

	/**
	 * @param caja el caja a establecer
	 */
	public void setCaja(Short caja) {
		this.caja = caja;
	}

	/**
	 * @return el zona
	 */
	public Short getClavePromotoria() {
		return clavePromotoria;
	}

	/**
	 * @param zona el zona a establecer
	 */
	public void setClavePromotoria(Short clavePromotoria) {
		this.clavePromotoria = clavePromotoria;
	}

	/**
	 * @return el extension
	 */
	public String getExtension() {
		return StringUtils.upperCase(StringUtils.trim(extension));
	}

	/**
	 * @param extension el extension a establecer
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	public List<DependienteEconomico> getDependientes() {
		if(dependientes == null){
			this.dependientes = new ArrayList<DependienteEconomico>();
		}
		return dependientes;
	}

	public void setDependientes(List<DependienteEconomico> dependientes) {
		this.dependientes = dependientes;
	}

	public void addDependiente(DependienteEconomico dep){
		
		if(this.dependientes == null){
			this.dependientes = new ArrayList<DependienteEconomico>();
		}
		
		
		
		if(dep.getNumero() == 0){
			int numero = this.dependientes.size() + 1;
			dep.setNumero((short)numero);
			dependientes.add(dep);
		}else{
			this.dependientes.remove(dep);
			dependientes.add(dep);	
		}
		
	}

	public boolean isDeclaracionNoDependientes() {
		return declaracionNoDependientes;
	}

	public void setDeclaracionNoDependientes(boolean declaracionNoDependientes) {
		this.declaracionNoDependientes = declaracionNoDependientes;
	}
	
	public void fillForTest(){
		
		this.estadoElaboracion = 1;
		
		this.nombre = "ANA";
		this.apePaterno ="MARTINEZ";
		this.apeMaterno = "CISNEROS";
		this.fechaCaptura = new Date();
		this.fechaModificacion = new Date();
		this.fechaNacimiento = new Date();
		this.calle = "ZAPATA";
		this.banco = new Short((short)2);
		this.centroTrabajo = 1;
		this.ciudadElaboracion="CUERNAVACA";
		this.ciudadNacimiento="YAUTEPEC";
		this.codigoPostal="62736";
		this.contribuyenteEUA =true;
		this.numSsn = "12341234121";
		this.correoPersonal = "correo@gmail.com";
		this.correoPersonalConfirm = "correo@gmail.com";
		this.cveEdoCivil = new Short((short)1);
		this.cveGenero = new Short((short)1);
		this.profesion = "ING";
		this.cveStatus = new Short((short)1);
		this.idEstado = new Short((short)1);
		this.idMunicipio = new Short((short)1);
		this.idEdoNac = new Short((short)2);
		this.idPais = new Short((short)1);
		this.idPaisNac = new Short((short)1);
		this.montoSolicitado = 50d;
		this.numeroIdentificacion = "1234567";
		this.numExt = "1A";
		this.numInt = "1INT";
		this.tipoTramite = 1;
		this.telefonoCasa = "7351035558";
		this.clabe = "123456789123456789";
		this.extension = "123";
		this.cveUserCaptura = "us1";
		this.cveTipoIdentificacion = 2;
		this.ciudad = "MEXICO";
		this.politicamenteExpuesta = true;
		this.modoPago = 1;
		this.colonia = "JUAREZ";
		this.clabe = "002180024269503190";
		this.porcentaje = 25;
		this.idNacionalidad = new Short((short)1);
	}

	public Short getModoPago() {
		return modoPago;
	}

	public void setModoPago(Short modoPago) {
		this.modoPago = modoPago;
	}
	
	
	public DependienteEconomico getDependiente(Short num){
		DependienteEconomico llave = new DependienteEconomico();
		llave.setNumero(num);
		int index = dependientes.indexOf(llave);
		return dependientes.get(index);
	}
	
	
	public void removeDependiente(Short num){
		DependienteEconomico llave = new DependienteEconomico();
		llave.setNumero(num);
		this.dependientes.remove(llave);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((numFolio == null) ? 0 : numFolio.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Solicitud)) {
			return false;
		}
		Solicitud other = (Solicitud) obj;
		if (numFolio == null) {
			if (other.numFolio != null) {
				return false;
			}
		} else if (!numFolio.equals(other.numFolio)) {
			return false;
		}
		return true;
	}

	/**
	 * @return the captcha
	 */
	public String getCaptcha() {
		return captcha;
	}

	/**
	 * @param captcha the captcha to set
	 */
	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	/**
	 * @return the porcentaje
	 */
	public Short getPorcentaje() {
		return porcentaje;
	}

	/**
	 * @param porcentaje the porcentaje to set
	 */
	public void setPorcentaje(Short porcentaje) {
		this.porcentaje = porcentaje;
	}

	/**
	 * @return the porcentajeAportVol
	 */
	public Short getPorcentajeAportVol() {
		return porcentajeAportVol;
	}

	/**
	 * @param porcentajeAportVol the porcentajeAportVol to set
	 */
	public void setPorcentajeAportVol(Short porcentajeAportVol) {
		this.porcentajeAportVol = porcentajeAportVol;
	}

	/**
	 * @return the estadoNacExtranjero
	 */
	public String getEstadoNacExtranjero() {
		return StringUtils.upperCase(StringUtils.trim(estadoNacExtranjero));
	}

	/**
	 * @param estadoNacExtranjero the estadoNacExtranjero to set
	 */
	public void setEstadoNacExtranjero(String estadoNacExtranjero) {
		this.estadoNacExtranjero = estadoNacExtranjero;
	}

	/**
	 * @return the estadoExtranjero
	 */
	public String getEstadoExtranjero() {
		return StringUtils.upperCase(StringUtils.trim(estadoExtranjero));
	}

	/**
	 * @param estadoExtranjero the estadoExtranjero to set
	 */
	public void setEstadoExtranjero(String estadoExtranjero) {
		this.estadoExtranjero = estadoExtranjero;
	}

	/**
	 * @return the municipioExtranjero
	 */
	public String getMunicipioExtranjero() {
		return StringUtils.upperCase(StringUtils.trim(municipioExtranjero));
	}

	/**
	 * @param municipioExtranjero the municipioExtranjero to set
	 */
	public void setMunicipioExtranjero(String municipioExtranjero) {
		this.municipioExtranjero = municipioExtranjero;
	}

	
	public String getDiaNacimiento() {
		return diaNacimiento;
	}

	public void setDiaNacimiento(String diaNacimiento) {
		this.diaNacimiento = diaNacimiento;
	}

	public String getMesNacimiento() {
		return mesNacimiento;
	}

	public void setMesNacimiento(String mesNacimiento) {
		this.mesNacimiento = mesNacimiento;
	}

	public String getAnioNacimiento() {
		return anioNacimiento;
	}

	public void setAnioNacimiento(String anioNacimiento) {
		this.anioNacimiento = anioNacimiento;
	}

	public boolean isAvisoPrivacidad() {
		return avisoPrivacidad;
	}

	public void setAvisoPrivacidad(boolean avisoPrivacidad) {
		this.avisoPrivacidad = avisoPrivacidad;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getUserFolio() {
		if(this.userFolio == null && this.numFolio != null){
			this.userFolio = Base26Codec.encodeFolioSolicitud(this.numFolio);
		}
		return this.userFolio;
	}

	public String getNivelPuesto() {
		return nivelPuesto;
	}

	public void setNivelPuesto(String nivelPuesto) {
		this.nivelPuesto = nivelPuesto;
	}
	
	public String getfmtFechaCaptura(){
		if(this.fechaCaptura != null){
			SimpleDateFormat format = new SimpleDateFormat("ddMMyy");
			return format.format(this.fechaCaptura);
		}
		return "";
	}

	public boolean isReCaptura() {
		return reCaptura;
	}

	public void setReCaptura(boolean reCaptura) {
		this.reCaptura = reCaptura;
	}
	
	/**
	 * Establece la fecha de nacimiento a partir del RFC
	 */
	public void setFechaNacimientoRFC(){
		if(StringUtils.isNotEmpty(rfc) && this.fechaNacimiento == null){
			try{
				this.anioNacimiento = "19" + rfc.substring(4,6);
				this.mesNacimiento = rfc.substring(6,8);
				this.diaNacimiento = rfc.substring(8);
			}catch(Exception e){}
		}
	}

	/**
	 * @return el oficio
	 */
	public Integer getOficio() {
		return oficio;
	}

	/**
	 * @param oficio el oficio a establecer
	 */
	public void setOficio(Integer oficio) {
		this.oficio = oficio;
	}

	/**
	 * @return el correoPersonalConfirm
	 */
	public String getCorreoPersonalConfirm() {
		return correoPersonalConfirm;
	}

	/**
	 * @param correoPersonalConfirm el correoPersonalConfirm a establecer
	 */
	public void setCorreoPersonalConfirm(String correoPersonalConfirm) {
		this.correoPersonalConfirm = correoPersonalConfirm;
	}

	/**
	 * @return el correoTrabajoConfirm
	 */
	public String getCorreoTrabajoConfirm() {
		return correoTrabajoConfirm;
	}

	/**
	 * @param correoTrabajoConfirm el correoTrabajoConfirm a establecer
	 */
	public void setCorreoTrabajoConfirm(String correoTrabajoConfirm) {
		this.correoTrabajoConfirm = correoTrabajoConfirm;
	}
	
	
	
	/**
	 * @return el estadoPromotoria
	 */
	public Short getEstadoPromotoria() {
		return estadoPromotoria;
	}

	/**
	 * @param estadoPromotoria el estadoPromotoria a establecer
	 */
	public void setEstadoPromotoria(Short estadoPromotoria) {
		this.estadoPromotoria = estadoPromotoria;
	}

	public boolean isAplicaFormato140(){
		
		if(this.idNacionalidad == null) {
		   return false;
		}
		
		return (this.idNacionalidad != AppConstants.PAIS_MEXICO && this.politicamenteExpuesta);
	}

	/**
	 * @return el cheque
	 */
	public boolean isCheque() {
		return cheque;
	}

	/**
	 * @param cheque el cheque a establecer
	 */
	public void setCheque(boolean cheque) {
		this.cheque = cheque;
	}

	/**
	 * @return el bndUltimosDigitosClabe
	 */
	public boolean isBndUltimosDigitosClabe() {
		return bndUltimosDigitosClabe;
	}

	/**
	 * @param bndUltimosDigitosClabe el bndUltimosDigitosClabe a establecer
	 */
	public void setBndUltimosDigitosClabe(boolean bndUltimosDigitosClabe) {
		this.bndUltimosDigitosClabe = bndUltimosDigitosClabe;
	}
	
	public String getMaskClabe(){
		if(StringUtils.isNotEmpty(clabe)){
			return "**************"+this.getClabe().substring(14);	
		}
		return StringUtils.EMPTY;
	}
	
	
	public String getDescripcionModoPago(){
		
		if(this.modoPago == AppConstants.MODO_PAGO_TRANSFERENCIA){
			try{
				return this.getClabe().substring(0,3) + "***********" + this.getClabe().substring(14);
			}catch(Exception e){
				return "**** **** ****";
			}
		}else{
			return "CHEQUE";
		}
		
	}
	
	public String getMotivoNoSatisfactorio(){
		if(this.cveStatus == AppConstants.ESTATUS_RECHAZADO){
			return "Revisi�n documental";
		}
		
		if(this.cveStatus == AppConstants.ESTATUS_RECHAZADA_CADUCIDAD){
			return "Caducidad del folio";
		}
		
		return StringUtils.EMPTY;
	}

	/**
	 * @return el idProceso
	 */
	public Integer getIdProceso() {
		return idProceso;
	}

	/**
	 * @param idProceso el idProceso a establecer
	 */
	public void setIdProceso(Integer idProceso) {
		this.idProceso = idProceso;
	}

	/**
	 * @return el fechaPago
	 */
	public Date getFechaPago() {
		return fechaPago;
	}

	/**
	 * @param fechaPago el fechaPago a establecer
	 */
	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	/**
	 * @return el motivoRechazo
	 */
	public String getMotivoRechazo() {
		return motivoRechazo;
	}

	/**
	 * @param motivoRechazo el motivoRechazo a establecer
	 */
	public void setMotivoRechazo(String motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}
	
}
