package mx.com.metlife.solssi.main.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T, ID extends Serializable> {

    T getById(ID id);

    List<T> getAll();

    void save(T entity);
    
    void update(T entity);
    
}
