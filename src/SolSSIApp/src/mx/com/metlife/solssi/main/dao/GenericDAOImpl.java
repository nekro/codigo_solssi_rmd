package mx.com.metlife.solssi.main.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class GenericDAOImpl<T,ID extends Serializable> implements GenericDAO<T, ID>{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Class<T> entityBeanType;

	@SuppressWarnings("unchecked")
	public GenericDAOImpl(){
		this.entityBeanType = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	protected Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}
	
	
	@SuppressWarnings("unchecked")
	public T getById(ID id) {
		Session session = this.getSession();
		T entity = (T) session.get(this.entityBeanType, id);
		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		Session session = this.getSession();
		return session.createQuery("from " + getEntityBeanType().getName() ).list();
	}

	public void save(T entity) {
		Session session = this.getSession();
		session.save(entity);
	}

	public void update(T entity) {
		Session session = this.getSession();
		session.update(entity);
	}
	
	
	public Class<T> getEntityBeanType() {
        return entityBeanType;
    }
	
}
