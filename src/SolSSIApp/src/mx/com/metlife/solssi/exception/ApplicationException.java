package mx.com.metlife.solssi.exception;

public class ApplicationException extends Exception{
	
	
	private int code; 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -261399302952372196L;

	public ApplicationException(){
		super();
	}
	
	public ApplicationException(String msg){
		super(msg);
	}
	
	public ApplicationException(int code, String msg){
		super(msg);
		this.code = code;
	}
	
	public int getErrorCode(){
		return code;
	}
	
	

}
