package mx.com.metlife.solssi.catalogo.persistence;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.domain.CatalogoMaster;
import mx.com.metlife.solssi.catalogo.domain.CatalogoPK;
import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.main.dao.GenericDAOImpl;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.googlecode.ehcache.annotations.Cacheable;
import com.googlecode.ehcache.annotations.TriggersRemove;

@Repository
public class CatalogoDAOImpl extends GenericDAOImpl<CatalogoItem, CatalogoPK> implements CatalogoDAO{

	
	Logger logger = Logger.getLogger(CatalogoDAOImpl.class);
	
	
	@SuppressWarnings("unchecked")
	@Cacheable(cacheName="catalogosCache")
	public Map<String, CatalogoItem> getItems(Integer idCatalogo) {
		
		logger.debug("cargando catalogo id "+idCatalogo);
		
		Session session = getSession();
		String sql = "SELECT c FROM CatalogoItem c where id_catalogo = :idCatalogo and cve_estatus = 1";
		
		Query query = session.createQuery(sql)
						.setInteger("idCatalogo", idCatalogo);
		
		List<CatalogoItem> catalogos = query.list();
		
		Map<String,CatalogoItem> map = new HashMap<String,CatalogoItem>();
		

		for(CatalogoItem item : catalogos){
			map.put(StringUtils.trim(item.getClaveItem()), item);
		}
		
		return map;
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Cacheable(cacheName="catalogosCache")
	public Map<String, CatalogoItem> getItems(Integer idCatalogo, String orderField) {
		
		Session session = getSession();
		
		String sql = "SELECT c FROM CatalogoItem c where id_catalogo = :idCatalogo and cve_estatus = 1 order by c."+orderField;
		
		Query query = session.createQuery(sql)
						.setInteger("idCatalogo", idCatalogo);
		
		List<CatalogoItem> catalogos = query.list();
		
		Map<String,CatalogoItem> map = new LinkedHashMap<String,CatalogoItem>();
		

		for(CatalogoItem item : catalogos){
			map.put(StringUtils.trim(item.getClaveItem()), item);
		}
		
		return map;
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Cacheable(cacheName="catalogosCache")
	public List<CatalogoItem> getItems(Integer idCatalogo, String clavePadre, String orderField) {
		
		Session session = getSession();
		
		String sql = "SELECT c FROM CatalogoItem c where id_catalogo = :idCatalogo and cve_estatus = 1 and c.valorCampo2 = :cvePadre order by c."+orderField;
		
		Query query = session.createQuery(sql)
						.setInteger("idCatalogo", idCatalogo)
						.setString("cvePadre", StringUtils.trim(clavePadre));
		
		List<CatalogoItem> catalogos = query.list();
		
		return catalogos;
		
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public List<CatalogoItem> getAllItems(Integer idCatalogo, String orderField) {
		
		Session session = getSession();
		
		String sql = "SELECT c FROM CatalogoItem c where id_catalogo = :idCatalogo order by c."+orderField;
		
		Query query = session.createQuery(sql)
						.setInteger("idCatalogo", idCatalogo);
		
		List<CatalogoItem> catalogos = query.list();
		
		return catalogos;
	}
	
	
	@SuppressWarnings("unchecked")
	@Cacheable(cacheName="catalogosCache")
	public Integer getIdDependenciaFromCatalog(Integer retenedor, Integer unidadPago) throws ApplicationException {
		
		Session session = getSession();
		String sql = "SELECT c FROM CatalogoItem c where id_catalogo = :idCatalogo and cve_estatus = 1 and c.valorCampo3 = :retenedor and c.valorCampo4 = :unidadPago";
		
		Query query = session.createQuery(sql)
						.setInteger("idCatalogo", CatalogoConstants.DEPENDENCIAS)
						.setString("retenedor", String.valueOf(retenedor))
						.setString("unidadPago", String.valueOf(unidadPago));
						
		List<CatalogoItem> c = query.list();
		
		if( c == null || c.isEmpty()){
			throw new ApplicationException(MessageUtils.getMessage("cliente.dependencia.notfound"));
		}
		
		CatalogoItem item = c.get(0);
		
		try{
			return Integer.valueOf(item.getClaveItem());	
		}catch(Exception e){
			throw new ApplicationException(e.getMessage());
		}
		
		
		
	}


	@Override
	@TriggersRemove(cacheName="catalogosCache", removeAll=true)
	public void refreshCache() {
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoItem> getItemsWithoutCache(Integer idCatalogo, String orderField) {
		
		Session session = getSession();
		String sql = "SELECT c FROM CatalogoItem c where id_catalogo = :idCatalogo and cve_estatus = 1 order by c."+orderField;
		Query query = session.createQuery(sql)
						.setInteger("idCatalogo", idCatalogo);
		
		List<CatalogoItem> catalogos = query.list();
		
		return catalogos;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoMaster> getCatalogos(){
		Session session = getSession();
		Criteria crit = session.createCriteria(CatalogoMaster.class);
		return crit.list();
	}


	@Override
	public CatalogoMaster getCatalogoMaster(Integer id) {
		Session session = getSession();
		CatalogoMaster master = (CatalogoMaster) session.get(CatalogoMaster.class, id);
		return master;
	}


	@Override
	public Integer getNextClaveItem(Integer idCat) {
		
		Session session = getSession();
		
		Query q = session.createQuery("SELECT max(cast(c.clave.clave as int)) FROM CatalogoItem c where c.clave.id = :idCat");
		
		q.setInteger("idCat", idCat);
		Integer result = (Integer) q.uniqueResult();
		
		if(result == null){
			return 1;
		}
		
		Integer res = Integer.valueOf(result);
		return res + 1;
	}


	@Override
	@Cacheable(cacheName="catalogosCache")
	public CatalogoItem getDependenciaFromCatalog(Integer retenedor, Integer unidadPago) throws ApplicationException {
		
		Session session = getSession();
		
		String sql = "SELECT c FROM CatalogoItem c where id_catalogo = :idCatalogo and cve_estatus = 1 and c.valorCampo3 = :retenedor and c.valorCampo4 = :unidadPago";
		
		Query query = session.createQuery(sql)
						.setInteger("idCatalogo", CatalogoConstants.DEPENDENCIAS)
						.setString("retenedor", String.valueOf(retenedor))
						.setString("unidadPago", String.valueOf(unidadPago));
						
		List<CatalogoItem> c = query.list();
		
		if( c == null || c.isEmpty()){
			throw new ApplicationException(MessageUtils.getMessage("cliente.dependencia.notfound"));
		}
		
		CatalogoItem item = c.get(0);
		
		return item;
	}
	
	
}
