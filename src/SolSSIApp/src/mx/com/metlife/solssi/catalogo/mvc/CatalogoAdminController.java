package mx.com.metlife.solssi.catalogo.mvc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.domain.CatalogoMaster;
import mx.com.metlife.solssi.catalogo.domain.CatalogoPK;
import mx.com.metlife.solssi.catalogo.service.CatalogoService;
import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.inicio.service.GruposCapturaService;
import mx.com.metlife.solssi.util.BrowserUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("admin/catalogos")
public class CatalogoAdminController {
	
	@Autowired
	private CatalogoService catalogoService;
	
	@Autowired
	private GruposCapturaService gruposService;
	
	@Autowired
	private CatalogoValidator catalogoValidator;
	
	@RequestMapping(method = RequestMethod.POST)
	public String showAdminPage(Model model){
		List<CatalogoMaster> catalogos = catalogoService.getCatalogosMaster();
		model.addAttribute("catalogos", catalogos);
		model.addAttribute(new CatalogoMaster());
		BrowserUtils.enableHistoryBack(model);
		return "admin/catalogos";
	}
	
	@RequestMapping(value="items", method = RequestMethod.POST)
	public String getItems(CatalogoMaster m, Model model){
		
		CatalogoMaster master = catalogoService.getCatalogoMaster(m.getId());
		
		this.executeConsulta(master, model);
		
		BrowserUtils.enableHistoryBack(model);
		
		return "admin/catalogos";
	}
	
	@RequestMapping(value="alta/form", method = RequestMethod.POST)
	public String formAlta(@RequestParam("id") Integer id, Model model){
		CatalogoMaster master = catalogoService.getCatalogoMaster(id);
		model.addAttribute(master);
		
		CatalogoPK pk = new CatalogoPK();
		pk.setId(id);
		CatalogoItem item = new CatalogoItem();
		item.setEstatus(true);
		item.setClave(pk);
		
		if(master.getTipoClave() == 'N'){
			pk.setClave(String.valueOf(catalogoService.getNextClaveItem(id)));
		}
		
		model.addAttribute(item);
		model.addAttribute("command","alta");
		return "catalogos/form";
	}
	
	@RequestMapping(value="edit/form", method = RequestMethod.POST)
	public String formEdit(@RequestParam("id") Integer id, @RequestParam("clave") String clave, Model model){
		CatalogoMaster master = catalogoService.getCatalogoMaster(id);
		model.addAttribute(master);
		
		CatalogoItem item = catalogoService.getCatalogoItem(id, clave);
		
		model.addAttribute(item);
		model.addAttribute("command","update");
		return "catalogos/form";
	}
	
	
	@RequestMapping(value="save", method = RequestMethod.POST)
	public String save(@Valid CatalogoItem item, BindingResult errors, Model model, HttpServletRequest request){
		
		CatalogoMaster master = catalogoService.getCatalogoMaster(item.getIdCatalogo());
		
		catalogoValidator.validate(master, item, errors, false);
		
		if(errors.hasErrors()){
			model.addAttribute("command","alta");
			model.addAttribute(master);
			model.addAttribute(item);
			return "catalogos/form";
		}
		
		catalogoService.save(item);
		
		this.executeConsulta(master, model);
		
		if(item.getIdCatalogo() == CatalogoConstants.MESES_CAPTUTRA || item.getIdCatalogo() == CatalogoConstants.FECHAS_ESQUEMAS){
			catalogoService.refreshCache();
			gruposService.clear();	
		}
		
		return "admin/catalogos";
	}
	
	
	@RequestMapping(value="update", method = RequestMethod.POST)
	public String update(@Valid CatalogoItem item, BindingResult errors, Model model, HttpServletRequest request){
		
		CatalogoMaster master = catalogoService.getCatalogoMaster(item.getIdCatalogo());
		
		catalogoValidator.validate(master, item, errors, true);
		
		if(errors.hasErrors()){
			model.addAttribute("command","update");
			model.addAttribute(master);
			model.addAttribute(item);
			return "catalogos/form";
		}
		
		catalogoService.update(item);
		
		this.executeConsulta(master, model);
		
		if(item.getIdCatalogo() == CatalogoConstants.MESES_CAPTUTRA || item.getIdCatalogo() == CatalogoConstants.FECHAS_ESQUEMAS){
			catalogoService.refreshCache();
			gruposService.clear();	
		}
		
		return "admin/catalogos";
		
	}
	
	
	private void executeConsulta(CatalogoMaster master, Model model){
		
		model.addAttribute(master);
		
		String orderField = "descripcion";
		
		if(master.getTipoClave() == 'A'){
			orderField = "clave.clave";
		}
		
		if(master.getId() == CatalogoConstants.MUNICIPIOS){
			orderField = "valorCampo2";
		}
		
		List<CatalogoItem> items = catalogoService.getAllItems(master.getId(),orderField);
		
		List<CatalogoMaster> catalogos = catalogoService.getCatalogosMaster();
		model.addAttribute("catalogos", catalogos);
		
		model.addAttribute("items", items);
		model.addAttribute(master);
		
	}
	
}
