package mx.com.metlife.solssi.catalogo.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.apache.commons.lang.StringUtils;

@Embeddable
public class CatalogoPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3183059103495517024L;
	
	@Column(name="ID_CATALOGO")
	private Integer id;
	
	@Column(name="CLAVE")
	private String clave;
	
	

	/**
	 * @return el id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id el id a establecer
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return el clave
	 */
	public String getClave() {
		return StringUtils.trim(clave);
	}

	/**
	 * @param clave el clave a establecer
	 */
	public void setClave(String clave) {
		
		this.clave = StringUtils.trim(clave);
	}

	/* (sin Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (sin Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CatalogoPK))
			return false;
		CatalogoPK other = (CatalogoPK) obj;
		if (clave == null) {
			if (other.clave != null)
				return false;
		} else if (!clave.equals(other.clave))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
	
	

}
