package mx.com.metlife.solssi.catalogo.domain;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name="TC_DETALLE_CATALOGO")
public class CatalogoItem implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId	
	private CatalogoPK clave;
	
	@Column(name="CVE_ESTATUS")
	private Boolean estatus;

	@Column(name="VALOR_CAMPO1")
	private String descripcion;

	@Column(name="VALOR_CAMPO2")
	private String valorCampo2;
	
	@Column(name="VALOR_CAMPO3")
	private String valorCampo3;
	
	@Column(name="VALOR_CAMPO4")
	private String valorCampo4;
	
	@Column(name="VALOR_CAMPO5")
	private String valorCampo5;
	
	@Column(name="VALOR_CAMPO6")
	private String valorCampo6;
	
	@Column(name="VALOR_CAMPO7")
	private String valorCampo7;
	
	@Transient
	private String descDependencia;

	public CatalogoItem() {
		super();
	}

	
	public Integer getIdCatalogo(){
		return this.clave.getId();
	}
	
	public String getClaveItem(){
		return this.clave.getClave();
	}
	
	public CatalogoPK getClave() {
		return this.clave;
	}

	public void setClave(CatalogoPK clave) {
		this.clave = clave;
	}

	/**
	 * @return el estatus
	 */
	public Boolean getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus el estatus a establecer
	 */
	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return el descripcion
	 */
	public String getDescripcion() {
		return StringUtils.upperCase(StringUtils.trim(descripcion));
	}
	
	public String getDescripcionOrigen() {
		return StringUtils.trim(descripcion);
	}

	/**
	 * @param descripcion el descripcion a establecer
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return el valorCampo2
	 */
	public String getValorCampo2() {
		return StringUtils.upperCase(StringUtils.trim(valorCampo2));
	}

	/**
	 * @param valorCampo2 el valorCampo2 a establecer
	 */
	public void setValorCampo2(String valorCampo2) {
		this.valorCampo2 = valorCampo2;
	}


	/**
	 * @return the valorCampo3
	 */
	public String getValorCampo3() {
		return StringUtils.trim(valorCampo3);
	}


	/**
	 * @param valorCampo3 the valorCampo3 to set
	 */
	public void setValorCampo3(String valorCampo3) {
		this.valorCampo3 = valorCampo3;
	}


	public String getValorCampo4() {
		return valorCampo4;
	}


	public void setValorCampo4(String valorCampo4) {
		this.valorCampo4 = valorCampo4;
	}


	public String getValorCampo5() {
		return valorCampo5;
	}


	public void setValorCampo5(String valorCampo5) {
		this.valorCampo5 = valorCampo5;
	}
	
	

	/**
	 * @return the valorCampo6
	 */
	public String getValorCampo6() {
		return valorCampo6;
	}


	/**
	 * @param valorCampo6 the valorCampo6 to set
	 */
	public void setValorCampo6(String valorCampo6) {
		this.valorCampo6 = valorCampo6;
	}


	/**
	 * @return the valorCampo7
	 */
	public String getValorCampo7() {
		return valorCampo7;
	}


	/**
	 * @param valorCampo7 the valorCampo7 to set
	 */
	public void setValorCampo7(String valorCampo7) {
		this.valorCampo7 = valorCampo7;
	}


	public String getDescDependencia(){
		if(this.descDependencia == null){
			StringBuffer buf = new StringBuffer();
			buf.append("( ").append(StringUtils.trim(valorCampo3)).append(" - ").append(StringUtils.trim(valorCampo4)).append(" )  ");
			buf.append(StringUtils.trim(descripcion));
			this.descDependencia =  buf.toString();
		}
		return descDependencia;
	}


}
