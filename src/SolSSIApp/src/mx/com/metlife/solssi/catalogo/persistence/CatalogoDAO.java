package mx.com.metlife.solssi.catalogo.persistence;

import java.util.List;
import java.util.Map;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.domain.CatalogoMaster;
import mx.com.metlife.solssi.catalogo.domain.CatalogoPK;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.main.dao.GenericDAO;

public interface CatalogoDAO extends GenericDAO<CatalogoItem, CatalogoPK>{
	
	public Map<String, CatalogoItem> getItems(Integer idCatalogo);
	public Map<String, CatalogoItem> getItems(Integer idCatalogo, String orderField);
	public List<CatalogoItem> getItemsWithoutCache(Integer idCatalogo, String orderField);
	public List<CatalogoItem> getItems(Integer idCatalogo, String clavePadre, String orderField) ;
	public List<CatalogoItem> getAllItems(Integer idCatalogo, String orderField) ;
	public Integer getIdDependenciaFromCatalog(Integer retenedor, Integer unidadPago) throws ApplicationException;
	public CatalogoItem getDependenciaFromCatalog(Integer retenedor, Integer unidadPago) throws ApplicationException;
	
	public List<CatalogoMaster> getCatalogos();
	public CatalogoMaster getCatalogoMaster(Integer id);
	
	public Integer getNextClaveItem(Integer id);
	
	public void refreshCache();
}
