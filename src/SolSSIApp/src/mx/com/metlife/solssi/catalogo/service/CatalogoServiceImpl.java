package mx.com.metlife.solssi.catalogo.service;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.domain.CatalogoMaster;
import mx.com.metlife.solssi.catalogo.domain.CatalogoPK;
import mx.com.metlife.solssi.catalogo.persistence.CatalogoDAO;
import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.exception.ApplicationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation=Propagation.NOT_SUPPORTED, isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
public class CatalogoServiceImpl implements CatalogoService{

	private Map<String, Map<String, CatalogoItem>> catalogos = new HashMap<String, Map<String, CatalogoItem>>();
	
	@Autowired
	private CatalogoDAO catalogoDAO;
	
	
	public void load(){
		
		catalogos.clear();
		
		this.putCatalogo(CatalogoConstants.CAT_PAISES, CatalogoConstants.PAISES, "descripcion");
		this.putCatalogo(CatalogoConstants.CAT_ESTADOS, CatalogoConstants.ESTADOS, "descripcion");
		this.putCatalogo(CatalogoConstants.CAT_MUNICIPIOS,CatalogoConstants.MUNICIPIOS, "descripcion");
		this.putCatalogo(CatalogoConstants.CAT_ESTADO_CIVIL,CatalogoConstants.ESTADO_CIVIL);
		this.putCatalogo(CatalogoConstants.CAT_ESTATUS_TRAMITE,CatalogoConstants.ESTATUS_TRAMITE);
		this.putCatalogo(CatalogoConstants.CAT_IDENTIFICACIONES,CatalogoConstants.IDENTIFICACIONES, "valorCampo4");
		this.putCatalogo(CatalogoConstants.CAT_CENTROS_SERVICIO,CatalogoConstants.CENTROS_SERVICIO, "descripcion");
		this.putCatalogo(CatalogoConstants.CAT_MOTIVOS_RECHAZO,CatalogoConstants.MOTIVOS_RECHAZO);
		this.putCatalogo(CatalogoConstants.CAT_PARAMETROS_NEGOCIO,CatalogoConstants.PARAMETROS_NEGOCIO);
		this.putCatalogo(CatalogoConstants.CAT_AFECTADOS,CatalogoConstants.AFECTADOS);
		this.putCatalogo(CatalogoConstants.CAT_OFICINAS_CHEQUE,CatalogoConstants.OFICINAS_CHEQUE, "descripcion");
		this.putCatalogo(CatalogoConstants.CAT_DEPENDENCIAS,CatalogoConstants.DEPENDENCIAS, "valorCampo3");
		this.putCatalogo(CatalogoConstants.CAT_TIPO_TRAMITE,CatalogoConstants.TIPO_TRAMITE);
		this.putCatalogo(CatalogoConstants.CAT_BANCOS,CatalogoConstants.BANCOS, "descripcion");
		this.putCatalogo(CatalogoConstants.CAT_OCUPACIONES,CatalogoConstants.OCUPACIONES,"clave.clave");
		this.putCatalogo(CatalogoConstants.CAT_NIVEL_PUESTO,CatalogoConstants.NIVEL_PUESTO, "descripcion");
		this.putCatalogo(CatalogoConstants.CAT_GENEROS,CatalogoConstants.GENEROS);
		this.putCatalogo(CatalogoConstants.CAT_CHECKLIST,CatalogoConstants.CHECKLIST, "clave.clave");
		this.putCatalogo(CatalogoConstants.CAT_MOVIMIENTOS,CatalogoConstants.MOVIMIENTOS, "descripcion");
		this.putCatalogo(CatalogoConstants.CAT_FECHAS_ESQUEMAS,CatalogoConstants.FECHAS_ESQUEMAS);
		this.putCatalogo(CatalogoConstants.CAT_CHECK_ESQUEMAS,CatalogoConstants.CHECK_ESQUEMAS);
		this.createCalendarCatalogo(catalogos);
	}
	
	@Override
	public List<CatalogoItem> getAllItems(Integer idCatalogo, String orderField) {
		return catalogoDAO.getAllItems(idCatalogo, orderField);
	}
	
	@Override
	public Map<String, CatalogoItem> getItems(Integer idCatalogo) {
		return catalogoDAO.getItems(idCatalogo);
	}
	public Map<String, CatalogoItem> getItems(Integer idCatalogo, String orderField) {
		return catalogoDAO.getItems(idCatalogo, orderField);
	}
	
	private void putCatalogo(String name, Integer id){
		try{
			catalogos.put(name, this.getItems(id));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	private void putCatalogo(String name, Integer id, String orderField){
		try{
			catalogos.put(name, this.getItems(id, orderField));
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
	public Map<String, Map<String, CatalogoItem>> getCatalogos() {
		return catalogos;
	}
	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
	public List<CatalogoItem> getCatalogoMunicipios(String claveEstado) {
		return catalogoDAO.getItems(CatalogoConstants.MUNICIPIOS, claveEstado, "descripcion");
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
	public List<CatalogoItem> getCatalogoPromotorias(String claveEstado) {
		return catalogoDAO.getItems(CatalogoConstants.OFICINAS_CHEQUE, claveEstado, "descripcion");
	}
	

	@Transactional(propagation=Propagation.NOT_SUPPORTED, isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
	public Integer getIdDependenciaFromCatalog(Integer retenedor, Integer unidadPago) throws ApplicationException{
		return catalogoDAO.getIdDependenciaFromCatalog(retenedor, unidadPago);
	}
	
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED, isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
	public CatalogoItem getDependenciaFromCatalog(Integer retenedor, Integer unidadPago) throws ApplicationException{
		return catalogoDAO.getDependenciaFromCatalog(retenedor, unidadPago);
	}
	
	
	private void createCalendarCatalogo(Map<String, Map<String, CatalogoItem>> cat){
		
		Map<String,CatalogoItem> meses = new LinkedHashMap<String,CatalogoItem>();  
		
		DateFormatSymbols sym = new DateFormatSymbols();
		for (int i = 1; i <= 12; i++) {
			CatalogoItem item = new CatalogoItem();
			CatalogoPK pk = new CatalogoPK();
			pk.setId(CatalogoConstants.MESES);
			pk.setClave(String.valueOf(i < 10 ? "0" + i : i));
			item.setClave(pk);
			item.setDescripcion(sym.getMonths()[i - 1].toUpperCase());
			meses.put(item.getClaveItem(), item);
	    }
		
		Map<String,CatalogoItem> dias = new LinkedHashMap<String,CatalogoItem>();
		for (int i = 1; i <= 31; i++) {
			CatalogoItem item = new CatalogoItem();
			CatalogoPK pk = new CatalogoPK();
			pk.setId(CatalogoConstants.DIAS);
			pk.setClave(String.valueOf(i < 10 ? "0" + i : i));
			item.setClave(pk);
			item.setDescripcion(String.valueOf(i < 10 ? "0" + i : i));
			dias.put(item.getClaveItem(), item);
	    }
		
		
		Map<String,CatalogoItem> anios = new LinkedHashMap<String,CatalogoItem>();
		Calendar fecha = Calendar.getInstance();
        int anio = fecha.get(Calendar.YEAR);
        for (int i = anio; i > anio -90; i--) {
        	CatalogoItem item = new CatalogoItem();
			CatalogoPK pk = new CatalogoPK();
			pk.setId(CatalogoConstants.ANIOS);
			pk.setClave(String.valueOf(i));
			item.setClave(pk);
			item.setDescripcion(String.valueOf(i));
			anios.put(item.getClaveItem(), item);
        }
        
        cat.put("DIAS", dias);
        cat.put("MESES", meses);
        cat.put("ANIOS", anios);
	}
	
	
	@Override
	public String getCatalogoItemDescription(Integer idCatalogo,Short idElemento) {
		
		Map<String, CatalogoItem> catalogo = this.getItems(idCatalogo);
		CatalogoItem item = catalogo.get(String.valueOf(idElemento));
		return item.getDescripcion();
		
		
	}
	
	@Override
	public List<CatalogoItem> getItemsWithoutCache(Integer idCatalogo, String orderField){
		return catalogoDAO.getItemsWithoutCache(idCatalogo, orderField);
	}
	
	@Override
	public String getCatalogoItemDescription(String catalogo,Short idElemento) {
		
		Map<String, CatalogoItem> catalogoMap = catalogos.get(catalogo);
		CatalogoItem item = catalogoMap.get(String.valueOf(idElemento));
		
		if(item == null){
			item = this.getAndPutInCache(catalogo, String.valueOf(idElemento));
		}
		
		if(item == null){
			return "";
		}
		
		
		return item.getDescripcion();
	}

	@Override
	@PreAuthorize("hasRole('ADMIN_CATALOGOS')")
	public void refreshCache() {
		catalogoDAO.refreshCache();
		
	}
	
	public String getCatalogoItemDescription(String catalogo,String idElemento) {
		Map<String, CatalogoItem> catalogoMap = catalogos.get(catalogo);
		CatalogoItem item = catalogoMap.get(idElemento);
		
		if(item == null){
			item = this.getAndPutInCache(catalogo, idElemento);
		}
		
		if(item == null){
			return "";
		}
		
		return item.getDescripcionOrigen();
	}
	
	
	private CatalogoItem getAndPutInCache(String nemoCatalogo, String idElemento){
		
		Map<String, CatalogoItem> catalogoMap = catalogos.get(nemoCatalogo);
		
		CatalogoPK pk = new CatalogoPK();
		pk.setClave(idElemento);
		pk.setId(this.getIdFromNemo(nemoCatalogo));
		
		CatalogoItem item = catalogoDAO.getById(pk);
		
		if(item.getEstatus()){
			catalogoMap.put(item.getClaveItem(), item);	
		}
		
		return item;
		
	}
	
	
	
	@Override
	public List<CatalogoMaster> getCatalogosMaster() {
		return catalogoDAO.getCatalogos();
	}
	
	@Override
	public CatalogoMaster getCatalogoMaster(Integer id){
		return catalogoDAO.getCatalogoMaster(id);
	}
	
	
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly=false)
	public void save(CatalogoItem item){
		
		catalogoDAO.save(item);
		
		String nemo = getNemonico(item.getIdCatalogo());
		if(nemo!=null){
			Map<String, CatalogoItem> catalogoMap = catalogos.get(nemo);
			if(catalogoMap != null){
				catalogoMap.put(item.getClaveItem(), item);
			}
		}
		
	}
	
	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW, readOnly=false)
	public void update(CatalogoItem item){
		
		catalogoDAO.update(item);
		
		String nemo = getNemonico(item.getIdCatalogo());
		if(nemo!=null){
			Map<String, CatalogoItem> catalogoMap = catalogos.get(nemo);
			if(catalogoMap != null){
				catalogoMap.put(item.getClaveItem(), item);
			}
		}
	}
	
	@Override
	public CatalogoItem getCatalogoItem(Integer idCatalogo, String clave){
		CatalogoPK pk = new CatalogoPK();
		pk.setId(idCatalogo);
		pk.setClave(clave);
		return catalogoDAO.getById(pk);
	}
	
	
	
	/**
	 * 
	 */
	public String getNemonico(Integer id){
		String nemoCat = null;
		switch(id){
			case CatalogoConstants.AFECTADOS : nemoCat = CatalogoConstants.CAT_AFECTADOS;break; 
			case CatalogoConstants.BANCOS : nemoCat = CatalogoConstants.CAT_BANCOS;break;
			case CatalogoConstants.CENTROS_SERVICIO : nemoCat = CatalogoConstants.CAT_CENTROS_SERVICIO;break;
			case CatalogoConstants.CHECKLIST : nemoCat = CatalogoConstants.CAT_CHECKLIST;break;
			case CatalogoConstants.DEPENDENCIAS : nemoCat = CatalogoConstants.CAT_DEPENDENCIAS;break;
			case CatalogoConstants.ESTATUS_TRAMITE : nemoCat = CatalogoConstants.CAT_ESTATUS_TRAMITE;break;
			case CatalogoConstants.ESTADO_CIVIL : nemoCat = CatalogoConstants.CAT_ESTADO_CIVIL;break;
			case CatalogoConstants.ESTADOS : nemoCat = CatalogoConstants.CAT_ESTADOS;break;
			case CatalogoConstants.GENEROS : nemoCat = CatalogoConstants.CAT_GENEROS;break;
			case CatalogoConstants.IDENTIFICACIONES : nemoCat = CatalogoConstants.CAT_IDENTIFICACIONES;break;
			case CatalogoConstants.MOTIVOS_RECHAZO : nemoCat = CatalogoConstants.CAT_MOTIVOS_RECHAZO;break;
			case CatalogoConstants.MUNICIPIOS : nemoCat = CatalogoConstants.CAT_MUNICIPIOS;break;
			case CatalogoConstants.NIVEL_PUESTO : nemoCat = CatalogoConstants.CAT_MUNICIPIOS;break;
			case CatalogoConstants.OCUPACIONES : nemoCat = CatalogoConstants.CAT_OCUPACIONES;break;
			case CatalogoConstants.OFICINAS_CHEQUE : nemoCat = CatalogoConstants.CAT_OFICINAS_CHEQUE;break;
			case CatalogoConstants.PAISES : nemoCat = CatalogoConstants.CAT_PAISES;break;
			case CatalogoConstants.PARAMETROS_NEGOCIO : nemoCat = CatalogoConstants.CAT_PARAMETROS_NEGOCIO;break;
			case CatalogoConstants.FECHAS_ESQUEMAS : nemoCat = CatalogoConstants.CAT_FECHAS_ESQUEMAS;break;
			case CatalogoConstants.CHECK_ESQUEMAS : nemoCat = CatalogoConstants.CAT_CHECK_ESQUEMAS;break;
		}
		
		return nemoCat;
	}
	
	
	/**
	 * 
	 * @param nemo
	 * @return
	 */
	public Integer getIdFromNemo(String nemo){
		
		if(CatalogoConstants.CAT_AFECTADOS.equals(nemo)){return CatalogoConstants.AFECTADOS;}
		if(CatalogoConstants.CAT_BANCOS.equals(nemo)){return CatalogoConstants.BANCOS;}
		if(CatalogoConstants.CAT_CENTROS_SERVICIO.equals(nemo)){return CatalogoConstants.CENTROS_SERVICIO;}
		if(CatalogoConstants.CAT_CHECKLIST.equals(nemo)){return CatalogoConstants.CHECKLIST;}
		if(CatalogoConstants.CAT_DEPENDENCIAS.equals(nemo)){return CatalogoConstants.DEPENDENCIAS;}
		if(CatalogoConstants.CAT_ESTATUS_TRAMITE.equals(nemo)){return CatalogoConstants.ESTATUS_TRAMITE;}
		if(CatalogoConstants.CAT_ESTADO_CIVIL.equals(nemo)){return CatalogoConstants.ESTADO_CIVIL;}
		if(CatalogoConstants.CAT_ESTADOS.equals(nemo)){return CatalogoConstants.ESTADOS;}
		if(CatalogoConstants.CAT_GENEROS.equals(nemo)){return CatalogoConstants.GENEROS;}
		if(CatalogoConstants.CAT_IDENTIFICACIONES.equals(nemo)){return CatalogoConstants.IDENTIFICACIONES;}
		if(CatalogoConstants.CAT_MOTIVOS_RECHAZO.equals(nemo)){return CatalogoConstants.MOTIVOS_RECHAZO;}
		if(CatalogoConstants.CAT_MUNICIPIOS.equals(nemo)){return CatalogoConstants.MUNICIPIOS;}
		if(CatalogoConstants.CAT_OCUPACIONES .equals(nemo)){return CatalogoConstants.OCUPACIONES;}
		if(CatalogoConstants.CAT_OFICINAS_CHEQUE.equals(nemo)){return CatalogoConstants.OFICINAS_CHEQUE;}
		if(CatalogoConstants.CAT_PAISES.equals(nemo)){return CatalogoConstants.PAISES;}
		if(CatalogoConstants.CAT_PARAMETROS_NEGOCIO.equals(nemo)){return CatalogoConstants.PARAMETROS_NEGOCIO;}
		if(CatalogoConstants.CAT_FECHAS_ESQUEMAS.equals(nemo)){return CatalogoConstants.FECHAS_ESQUEMAS;}
		if(CatalogoConstants.CAT_CHECK_ESQUEMAS.equals(nemo)){return CatalogoConstants.CHECK_ESQUEMAS;}
		
		return null;
		
	}

	@Override
	public Integer getNextClaveItem(Integer id) {
		return catalogoDAO.getNextClaveItem(id);
	}

}

