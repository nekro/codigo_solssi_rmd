package mx.com.metlife.solssi.catalogo.util;

public class CatalogoConstants {
	
	public static final int PAISES = 1;
	public static final int ESTADOS = 2;
	public static final int MUNICIPIOS = 3;
	public static final int ESTADO_CIVIL = 4;
	public static final int ESTATUS_TRAMITE = 5;
	public static final int IDENTIFICACIONES = 6;
	public static final int CENTROS_SERVICIO = 7;
	public static final int BANCOS = 8;
	public static final int MOTIVOS_RECHAZO = 9;
	public static final int PARAMETROS_NEGOCIO = 10;
	public static final int AFECTADOS = 11;
	public static final int OFICINAS_CHEQUE = 12;
	//public static final int PROMOTORIAS = 3;
	public static final int DEPENDENCIAS = 13;
	public static final int MESES_CAPTUTRA = 14;
	public static final int TIPO_TRAMITE = 15;
	public static final int OCUPACIONES = 16;
	public static final int NIVEL_PUESTO = 17;
	public static final int GENEROS = 18;
	public static final int CHECKLIST = 19;
	public static final int MOVIMIENTOS = 20;
	public static final int FECHAS_ESQUEMAS = 21;
	public static final int CHECK_ESQUEMAS = 22;
	
	public static final int DIAS = 1001;
	public static final int MESES = 1002;
	public static final int ANIOS = 1003;
	
	
	public static final int GENERO_MASCULINO = 1;
	public static final int GENERO_FEMENINO = 2;
	
	public static final int EDOCIVIL_SOLTERO = 1;
	public static final int EDOCIVIL_CASADO = 2;
	
	public static final int IDENT_IFE = 1;
	public static final int IDENT_CED_PROF = 2;
	public static final int IDENT_PASAPORTE = 3;
	public static final int IDENT_MATR_CONS = 4;
	public static final int IDENT_ADULTOS = 5;
	public static final int IDENT_FM2 = 6;
	public static final int IDENT_FM3 = 7;
	
	
	
	public static final String CAT_PAISES = "PAISES";
	public static final String CAT_ESTADOS = "ESTADOS";
	public static final String CAT_MUNICIPIOS = "MUNICIPIOS";
	public static final String CAT_ESTADO_CIVIL = "ESTADO_CIVIL";
	public static final String CAT_ESTATUS_TRAMITE = "ESTATUS_TRAMITE";
	public static final String CAT_IDENTIFICACIONES = "IDENTIFICACIONES";
	public static final String CAT_CENTROS_SERVICIO = "CENTROS_SERVICIO";
	public static final String CAT_MOTIVOS_RECHAZO = "MOTIVOS_RECHAZO";
	public static final String CAT_PARAMETROS_NEGOCIO = "PARAMETROS_NEGOCIO";
	public static final String CAT_AFECTADOS = "AFECTADOS";
	public static final String CAT_OFICINAS_CHEQUE = "OFICINAS_CHEQUE";
	public static final String CAT_DEPENDENCIAS = "DEPENDENCIAS";
	public static final String CAT_TIPO_TRAMITE = "TIPO_TRAMITE";
	public static final String CAT_BANCOS = "BANCOS";
	public static final String CAT_OCUPACIONES = "OCUPACIONES";
	public static final String CAT_NIVEL_PUESTO = "NIVEL_PUESTO";
	public static final String CAT_GENEROS = "GENEROS";
	public static final String CAT_CHECKLIST = "CHECKLIST";
	public static final String CAT_MOVIMIENTOS = "MOVIMIENTOS";
	public static final String CAT_FECHAS_ESQUEMAS = "FECHAS_ESQUEMAS";
	public static final String CAT_CHECK_ESQUEMAS = "CHECK_ESQUEMAS";
	
}
