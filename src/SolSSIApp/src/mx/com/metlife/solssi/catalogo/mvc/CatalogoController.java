package mx.com.metlife.solssi.catalogo.mvc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.service.CatalogoService;
import mx.com.metlife.solssi.inicio.service.GruposCapturaService;
import mx.com.metlife.solssi.param.service.ParametersService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("catalogo")
public class CatalogoController {
	
	@Autowired	
	private CatalogoService catalogoService;
	
	@Autowired	
	private ParametersService paramService;
	
	@Autowired
	private GruposCapturaService gruposService;
	
	@RequestMapping(value="municipios/{estado}")
	public @ResponseBody List<CatalogoItem> loadMunicipios(@PathVariable String estado){
		
		return catalogoService.getCatalogoMunicipios(estado);
		
	}
	
	
	@RequestMapping(value="promotorias/{estado}")
	public @ResponseBody List<CatalogoItem> loadPromotorias(@PathVariable String estado){
		
		return catalogoService.getCatalogoPromotorias(estado);
		
	}
	
	@RequestMapping(value="refresh/execute", method=RequestMethod.POST)
	public String refresh(Model model, HttpSession sess){
		
		catalogoService.refreshCache();
		gruposService.clear();
		catalogoService.load();
		model.addAttribute("result", "ok");
		
		sess.getServletContext().setAttribute("TT_ALTO_RENDIMIENTO", paramService.getAppParamValue("TT_ALTO_RENDIMIENTO", ""));
		sess.getServletContext().setAttribute("TT_BAJO_RENDIMIENTO", paramService.getAppParamValue("TT_BAJO_RENDIMIENTO", ""));
		
		return "refresh";
	}
	
	@RequestMapping(value="refresh")
	public String refreshPage(Model model){
		return "refresh";
	}

}
