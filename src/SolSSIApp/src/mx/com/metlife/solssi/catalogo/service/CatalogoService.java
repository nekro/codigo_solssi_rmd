package mx.com.metlife.solssi.catalogo.service;

import java.util.List;
import java.util.Map;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.domain.CatalogoMaster;
import mx.com.metlife.solssi.exception.ApplicationException;

public interface CatalogoService {
	
	public void load();
	
	public Map<String, CatalogoItem> getItems(Integer idCatalogo);
	
	public Map<String, Map<String, CatalogoItem>> getCatalogos();
	
	public List<CatalogoItem> getAllItems(Integer idCatalogo, String orderField);
	
	public List<CatalogoItem> getCatalogoMunicipios(String claveEstado);
	
	public List<CatalogoItem> getCatalogoPromotorias(String claveEstado);
	
	public String getCatalogoItemDescription(Integer idCatalogo, Short idElemento);
	
	public String getCatalogoItemDescription(String catalogo, Short idElemento);
	
	public void refreshCache();
	
	public String getCatalogoItemDescription(String catalogo,String idElemento);
	
	public List<CatalogoMaster> getCatalogosMaster();
	
	public CatalogoMaster getCatalogoMaster(Integer id);
	
	public CatalogoItem getCatalogoItem(Integer idCatalogo, String clave);

	public void save(CatalogoItem item);
	
	public void update(CatalogoItem item);
	
	public String getNemonico(Integer id);
	
	public List<CatalogoItem> getItemsWithoutCache(Integer idCatalogo, String orderField);
	
	public Integer getNextClaveItem(Integer id);
	
	public Integer getIdDependenciaFromCatalog(Integer retenedor, Integer unidadPago) throws ApplicationException;
	
	public CatalogoItem getDependenciaFromCatalog(Integer retenedor, Integer unidadPago) throws ApplicationException;
}
