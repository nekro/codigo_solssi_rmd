package mx.com.metlife.solssi.catalogo.mvc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.domain.CatalogoMaster;
import mx.com.metlife.solssi.catalogo.service.CatalogoService;
import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;

@Service
public class CatalogoValidator {
	
	@Autowired
	private CatalogoService catalogoService;
	
	public void validate(CatalogoMaster master, CatalogoItem item, BindingResult errors, boolean update){
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "clave.clave", "NotEmpty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descripcion", "NotEmpty");
		
		if(!update && StringUtils.isNotEmpty(item.getClaveItem())){
			if (catalogoService.getCatalogoItem(item.getIdCatalogo(), item.getClaveItem()) != null){
				errors.rejectValue("clave.clave", null,null, "Elemento ya existe en el cat�logo");
			}
		}
		
		if(StringUtils.isNotEmpty(master.getNombreCampo2())){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "valorCampo2", "NotEmpty");
		}
		
		if(StringUtils.isNotEmpty(master.getNombreCampo3())){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "valorCampo3", "NotEmpty");
		}
		
		if(StringUtils.isNotEmpty(master.getNombreCampo4())){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "valorCampo4", "NotEmpty");
		}
		
		if(StringUtils.isNotEmpty(master.getNombreCampo5())){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "valorCampo5", "NotEmpty");
		}
		
		this.validateCampos("clave.clave", item.getClaveItem(), master.getTipoClave(), errors);
		this.validateCampos("descripcion", item.getDescripcion(), master.getTipoCampo1(), errors);
		this.validateCampos("valorCampo2", item.getValorCampo2(), master.getTipoCampo2(), errors);
		this.validateCampos("valorCampo3", item.getValorCampo3(), master.getTipoCampo3(), errors);
		this.validateCampos("valorCampo4", item.getValorCampo4(), master.getTipoCampo4(), errors);
		this.validateCampos("valorCampo5", item.getValorCampo5(), master.getTipoCampo5(), errors);
		
		if(master.getId() == CatalogoConstants.MESES_CAPTUTRA){
			  	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			  	try {
					
			  		String pattern = "([A-Z]{2}\\-[A-Z]{2})?";
			  		if(!item.getClaveItem().matches(pattern)){
			  			errors.rejectValue("clave.clave", null,null, "El formato del rango de letras debe ser AA-AA");
			  		}
			  		
			  		Date capturaIni = fmt.parse(item.getDescripcion());
					Date capturaFin = fmt.parse(item.getValorCampo2());
					Date maxAut = fmt.parse(item.getValorCampo3());
					Date oficiosIni = fmt.parse(item.getValorCampo4());
					Date oficiosFin = fmt.parse(item.getValorCampo5());
					
					if(capturaFin.before(capturaIni)){
						errors.rejectValue("valorCampo2", null,null, "La fecha final de captura debe ser menor a la fecha inicial");
					}
					
					if(maxAut.before(capturaIni)){
						errors.rejectValue("valorCampo3", null,null, "La fecha m�xima de autorizaci�n debe ser mayor a la fecha inicial de captura");
					}
					
					if(oficiosIni.before(capturaIni)){
						errors.rejectValue("valorCampo4", null,null, "La fecha inicial de oficios debe ser mayor a la fecha inicial de captura");
					}
					
					if(oficiosFin.before(oficiosIni)){
						errors.rejectValue("valorCampo5", null,null, "La fecha final de oficios debe ser mayor a la fecha inicial de oficios");
					}
					
					if(maxAut.after(oficiosFin)){
						errors.rejectValue("valorCampo3", null,null, "La fecha m�xima de autorizaci�n debe ser menor a la fecha final de oficios");
					}
					
					List<CatalogoItem> items = catalogoService.getItemsWithoutCache(CatalogoConstants.MESES_CAPTUTRA, "descripcion");
					for(CatalogoItem cat : items){
						
						if(cat.getClaveItem().equals(item.getClaveItem())){
							continue;
						}

						Date capturaIni2 = fmt.parse(cat.getDescripcion());
						Date capturaFin2 = fmt.parse(cat.getValorCampo2());
						Date oficiosIni2 = fmt.parse(cat.getValorCampo4());
						Date oficiosFin2 = fmt.parse(cat.getValorCampo5());
						
						if(capturaIni.equals(capturaIni2) || (capturaIni.after(capturaIni2) && capturaIni.before(capturaFin2))){
							errors.rejectValue("descripcion", null,null, "Ya existe un grupo de captura abierto para la fecha seleccionada :"+cat.getClaveItem());
						}
						
						if(oficiosIni.equals(oficiosIni2) || (oficiosIni.after(oficiosIni2) && oficiosIni.before(oficiosFin2))){
							errors.rejectValue("valorCampo4", null,null, "Ya existe un grupo de recepcion de oficios abierto para la fecha seleccionada: "+cat.getClaveItem());
						}
						
					}
					
				} catch (ParseException e) {
					e.printStackTrace();
				}
		}
		
		
		if(master.getId() == CatalogoConstants.FECHAS_ESQUEMAS){
		  	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		  	try {
				
		  		Date capturaIni = fmt.parse(item.getDescripcion());
				Date capturaFin = fmt.parse(item.getValorCampo2());
				Date maxAut = fmt.parse(item.getValorCampo3());
				Date oficiosIni = fmt.parse(item.getValorCampo4());
				Date oficiosFin = fmt.parse(item.getValorCampo5());
				Date aplicacionInicio = fmt.parse(item.getValorCampo6());
				
				
				if(capturaFin.before(capturaIni)){
					errors.rejectValue("valorCampo2", null,null, "La fecha final de captura debe ser menor a la fecha inicial");
				}
				
				if(maxAut.before(capturaIni)){
					errors.rejectValue("valorCampo3", null,null, "La fecha m�xima de autorizaci�n debe ser mayor a la fecha inicial de captura");
				}
				
				if(oficiosIni.before(capturaIni)){
					errors.rejectValue("valorCampo4", null,null, "La fecha inicial de oficios debe ser mayor a la fecha inicial de captura");
				}
				
				if(oficiosFin.before(oficiosIni)){
					errors.rejectValue("valorCampo5", null,null, "La fecha final de oficios debe ser mayor a la fecha inicial de oficios");
				}
				
				if(maxAut.after(oficiosFin)){
					errors.rejectValue("valorCampo3", null,null, "La fecha m�xima de autorizaci�n debe ser menor a la fecha final de oficios");
				}
				
				if(!aplicacionInicio.after(capturaFin)){
					errors.rejectValue("valorCampo6", null,null, "La fecha de inicio de aplicaci�n de esquema debe ser mayor a la fecha final de selecci�n");
				}
				
			} catch (ParseException e) {
				e.printStackTrace();
			}
	}
		
	}
	
	
	private void validateCampos(String campo, String valor, Character tipoCampo, BindingResult errors){
		
		if(StringUtils.isNotEmpty(valor)){
			switch(tipoCampo){
				case 'A':break;
				
				case 'N':
						if(!StringUtils.isNumeric(valor)){
							errors.rejectValue(campo, "NotNumeric");
						}
						break;
				case 'D':
					try {
						
						if(valor.length() != 10){
							errors.rejectValue(campo, "NotDate");
						}else{
							SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
							Date date = format.parse(valor);
							String nueva = format.format(date);
							if(!StringUtils.equals(valor, nueva)){
								errors.rejectValue(campo, "NotDate");
							}
						}
					} catch (Exception e) {
						errors.rejectValue(campo, "NotDate");
					}
						
						
			}
		}
	}

}
