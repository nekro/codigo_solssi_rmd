package mx.com.metlife.solssi.catalogo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TC_CATALOGO_MAESTRO")
public class CatalogoMaster {
	
	@Id
	@Column(name = "ID_CATALOGO")
	private Integer id;
	
	@Column(name = "NOMBRE_CATALOGO")
	private String nombreCatalogo;
	
	@Column(name = "TIPO_CLAVE")
	private Character tipoClave;
	
	@Column(name = "LONG_CLAVE")
	private Short longitudClave;
	
	@Column(name = "NOMBRE_CLAVE")
	private String nombreClave;
	
	@Column(name = "TIPO_CAMPO1")
	private Character tipoCampo1;
	
	@Column(name = "TIPO_CAMPO2")
	private Character tipoCampo2;
	
	@Column(name = "TIPO_CAMPO3")
	private Character tipoCampo3;

	@Column(name = "TIPO_CAMPO4")
	private Character tipoCampo4;
	
	@Column(name = "TIPO_CAMPO5")
	private Character tipoCampo5;
	
	@Column(name = "TIPO_CAMPO6")
	private Character tipoCampo6;
	
	@Column(name = "TIPO_CAMPO7")
	private Character tipoCampo7;
	
	@Column(name = "NOMBRE_CAMPO1")
	private String nombreCampo1;
	
	@Column(name = "NOMBRE_CAMPO2")
	private String nombreCampo2;
	
	@Column(name = "NOMBRE_CAMPO3")
	private String nombreCampo3;
	
	@Column(name = "NOMBRE_CAMPO4")
	private String nombreCampo4;
	
	@Column(name = "NOMBRE_CAMPO5")
	private String nombreCampo5;
	
	@Column(name = "NOMBRE_CAMPO6")
	private String nombreCampo6;
	
	@Column(name = "NOMBRE_CAMPO7")
	private String nombreCampo7;
	
	@Column(name = "CVE_ESTATUS")
	private boolean activo;

	/**
	 * @return el id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id el id a establecer
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return el nombreCatalogo
	 */
	public String getNombreCatalogo() {
		return nombreCatalogo;
	}

	/**
	 * @param nombreCatalogo el nombreCatalogo a establecer
	 */
	public void setNombreCatalogo(String nombreCatalogo) {
		this.nombreCatalogo = nombreCatalogo;
	}

	/**
	 * @return el tipoClave
	 */
	public Character getTipoClave() {
		return tipoClave;
	}

	/**
	 * @param tipoClave el tipoClave a establecer
	 */
	public void setTipoClave(Character tipoClave) {
		this.tipoClave = tipoClave;
	}

	/**
	 * @return el longitudClave
	 */
	public Short getLongitudClave() {
		return longitudClave;
	}

	/**
	 * @param longitudClave el longitudClave a establecer
	 */
	public void setLongitudClave(Short longitudClave) {
		this.longitudClave = longitudClave;
	}

	/**
	 * @return el nombreClave
	 */
	public String getNombreClave() {
		return nombreClave;
	}

	/**
	 * @param nombreClave el nombreClave a establecer
	 */
	public void setNombreClave(String nombreClave) {
		this.nombreClave = nombreClave;
	}

	/**
	 * @return el tipoCampo1
	 */
	public Character getTipoCampo1() {
		return tipoCampo1;
	}

	/**
	 * @param tipoCampo1 el tipoCampo1 a establecer
	 */
	public void setTipoCampo1(Character tipoCampo1) {
		this.tipoCampo1 = tipoCampo1;
	}

	/**
	 * @return el tipoCampo2
	 */
	public Character getTipoCampo2() {
		return tipoCampo2;
	}

	/**
	 * @param tipoCampo2 el tipoCampo2 a establecer
	 */
	public void setTipoCampo2(Character tipoCampo2) {
		this.tipoCampo2 = tipoCampo2;
	}

	/**
	 * @return el tipoCampo3
	 */
	public Character getTipoCampo3() {
		return tipoCampo3;
	}

	/**
	 * @param tipoCampo3 el tipoCampo3 a establecer
	 */
	public void setTipoCampo3(Character tipoCampo3) {
		this.tipoCampo3 = tipoCampo3;
	}

	/**
	 * @return el tipoCampo4
	 */
	public Character getTipoCampo4() {
		return tipoCampo4;
	}

	/**
	 * @param tipoCampo4 el tipoCampo4 a establecer
	 */
	public void setTipoCampo4(Character tipoCampo4) {
		this.tipoCampo4 = tipoCampo4;
	}

	/**
	 * @return el tipoCampo5
	 */
	public Character getTipoCampo5() {
		return tipoCampo5;
	}

	/**
	 * @param tipoCampo5 el tipoCampo5 a establecer
	 */
	public void setTipoCampo5(Character tipoCampo5) {
		this.tipoCampo5 = tipoCampo5;
	}

	/**
	 * @return el nombreCampo1
	 */
	public String getNombreCampo1() {
		return nombreCampo1;
	}

	/**
	 * @param nombreCampo1 el nombreCampo1 a establecer
	 */
	public void setNombreCampo1(String nombreCampo1) {
		this.nombreCampo1 = nombreCampo1;
	}

	/**
	 * @return el nombreCampo2
	 */
	public String getNombreCampo2() {
		return nombreCampo2;
	}

	/**
	 * @param nombreCampo2 el nombreCampo2 a establecer
	 */
	public void setNombreCampo2(String nombreCampo2) {
		this.nombreCampo2 = nombreCampo2;
	}

	/**
	 * @return el nombreCampo3
	 */
	public String getNombreCampo3() {
		return nombreCampo3;
	}

	/**
	 * @param nombreCampo3 el nombreCampo3 a establecer
	 */
	public void setNombreCampo3(String nombreCampo3) {
		this.nombreCampo3 = nombreCampo3;
	}

	/**
	 * @return el nombreCampo4
	 */
	public String getNombreCampo4() {
		return nombreCampo4;
	}

	/**
	 * @param nombreCampo4 el nombreCampo4 a establecer
	 */
	public void setNombreCampo4(String nombreCampo4) {
		this.nombreCampo4 = nombreCampo4;
	}

	/**
	 * @return el nombreCampo5
	 */
	public String getNombreCampo5() {
		return nombreCampo5;
	}

	/**
	 * @param nombreCampo5 el nombreCampo5 a establecer
	 */
	public void setNombreCampo5(String nombreCampo5) {
		this.nombreCampo5 = nombreCampo5;
	}

	/**
	 * @return el activo
	 */
	public boolean isActivo() {
		return activo;
	}

	/**
	 * @param activo el activo a establecer
	 */
	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	/**
	 * @return the tipoCampo6
	 */
	public Character getTipoCampo6() {
		return tipoCampo6;
	}

	/**
	 * @param tipoCampo6 the tipoCampo6 to set
	 */
	public void setTipoCampo6(Character tipoCampo6) {
		this.tipoCampo6 = tipoCampo6;
	}

	/**
	 * @return the tipoCampo7
	 */
	public Character getTipoCampo7() {
		return tipoCampo7;
	}

	/**
	 * @param tipoCampo7 the tipoCampo7 to set
	 */
	public void setTipoCampo7(Character tipoCampo7) {
		this.tipoCampo7 = tipoCampo7;
	}

	/**
	 * @return the nombreCampo6
	 */
	public String getNombreCampo6() {
		return nombreCampo6;
	}

	/**
	 * @param nombreCampo6 the nombreCampo6 to set
	 */
	public void setNombreCampo6(String nombreCampo6) {
		this.nombreCampo6 = nombreCampo6;
	}

	/**
	 * @return the nombreCampo7
	 */
	public String getNombreCampo7() {
		return nombreCampo7;
	}

	/**
	 * @param nombreCampo7 the nombreCampo7 to set
	 */
	public void setNombreCampo7(String nombreCampo7) {
		this.nombreCampo7 = nombreCampo7;
	}
	
	
	

}
