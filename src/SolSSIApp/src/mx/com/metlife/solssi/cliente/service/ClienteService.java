package mx.com.metlife.solssi.cliente.service;

import java.util.List;

import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.exception.ApplicationException;

public interface ClienteService {
	
	public Cliente searchSSI(String rfc, String homoclave, Long cuenta) throws ApplicationException;
	public Cliente searchSSIEsquemas(String rfc, Long cuenta)	throws ApplicationException;
	
	public Cliente consultaSaldo(String rfc, Long cuenta, Double ultimoDescuento) throws ApplicationException;
	public Cliente get(Long cuenta);
	public List<Cliente> searchSSIAdmin(String rfc, String homoclave, Long cuenta) throws ApplicationException;
	public void updateFueraFecha(String rfc, Long cuenta, boolean newFueraFecha, String usuario);
	
	public void update(Cliente cliente, String usuario);
	
	public void save(Cliente cliente, String usuario);

}
