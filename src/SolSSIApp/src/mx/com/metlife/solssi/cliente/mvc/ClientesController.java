package mx.com.metlife.solssi.cliente.mvc;

import java.util.List;

import javax.validation.Valid;

import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.cliente.service.ClienteService;
import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.solicitud.service.SolicitudService;
import mx.com.metlife.solssi.usuario.domain.UserPrincipal;
import mx.com.metlife.solssi.usuario.domain.Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@RequestMapping("admin/clientes")
@SessionAttributes("filter")
public class ClientesController {
	
	@Autowired
	private ClienteService clientesService;
	
	@Autowired
	private ClientesValidator validator;
	
	@Autowired
	private SolicitudService solicitudService;
	
	@RequestMapping(method=RequestMethod.POST)
	public String showAdminPage(Model model){
		
		model.addAttribute("filter","true");
		model.addAttribute(new Cliente());
		
		return "clientes/admin";
	}
	
	
	@RequestMapping(value="search", method=RequestMethod.POST)
	public String search(Cliente clienteFilter, Model model){
		try {
			
			List<Cliente> clientes = clientesService.searchSSIAdmin(null, null, clienteFilter.getCuenta());
			
			model.addAttribute("cliente", clientes.get(0));
			
			model.addAttribute("action", "update");
			
		} catch (ApplicationException e) {
			model.addAttribute(clienteFilter);
			model.addAttribute("error",e.getMessage());
		}
		return "clientes/admin";
	}
	
	@RequestMapping(value="alta", method=RequestMethod.POST)
	public String search(Model model){

		Cliente cliente = new Cliente();
		cliente.setCodigoRetira((short)1);
		model.addAttribute("cliente", cliente);
		model.addAttribute("filter","false");
		model.addAttribute("action", "alta");
			
		return "clientes/admin";
	}
	
	
	@RequestMapping(value="update", method=RequestMethod.POST)
	public String update(@Valid Cliente cliente, BindingResult errors, Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		model.addAttribute("cliente", cliente);
		model.addAttribute("action", "update");
		
		validator.validate(cliente, errors);
		
		if(errors.hasErrors()){
			return "clientes/admin";
		}
		
		ConsultaFilter filter = new ConsultaFilter();
		filter.setCuenta(String.valueOf(cliente.getCuenta()));
		List<Solicitud> list = solicitudService.get(filter);
		
		if(list  != null && list.size()>0){
			
			Cliente clienteBase = clientesService.get(cliente.getCuenta());
			clienteBase.setSaldo(cliente.getSaldo());
			clienteBase.setUltimoDescuento(cliente.getUltimoDescuento());
			clientesService.update(clienteBase, usuario.getClave());
			model.addAttribute("cliente", clienteBase);
			model.addAttribute("mensaje", "existe una solicitud capturada, solo se actualizaron los campos Reserva y Monto de �ltimo Descuento");
		}else{
			Cliente clienteBase = clientesService.get(cliente.getCuenta());
			cliente.setFechaAlta(clienteBase.getFechaAlta());
			clientesService.update(cliente, usuario.getClave());	
		}
		
		model.addAttribute("save_successful", "true");
			
		return "clientes/admin";
	}
	
	@RequestMapping(value="save", method=RequestMethod.POST)
	public String save(@Valid Cliente cliente, BindingResult errors, Model model, Authentication auth){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		
		model.addAttribute("cliente", cliente);
		model.addAttribute("action", "alta");
		
		validator.validate(cliente, errors);
		
		
		if(!errors.hasFieldErrors("cuenta") && clientesService.get(cliente.getCuenta()) != null){
			errors.rejectValue("cuenta", "Duplicate");
		}
		
		if(errors.hasErrors()){
			return "clientes/admin";
		}
		
		clientesService.save(cliente, usuario.getClave());	
				
		model.addAttribute("save_successful", "true");
		
		cliente = new Cliente();
		cliente.setCodigoRetira((short)1);
		model.addAttribute("cliente", cliente);
		
			
		return "clientes/admin";
	}


}
