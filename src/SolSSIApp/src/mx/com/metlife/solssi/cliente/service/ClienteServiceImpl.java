package mx.com.metlife.solssi.cliente.service;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import mx.com.metlife.solssi.auditoria.domain.Bitacora;
import mx.com.metlife.solssi.auditoria.persistence.BitacoraDAO;
import mx.com.metlife.solssi.catalogo.persistence.CatalogoDAO;
import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.cliente.persistence.ClienteDAO;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.solicitud.persistence.SolicitudDAO;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.MovimientosConstants;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


 @Service
 @Transactional(propagation=Propagation.NOT_SUPPORTED, isolation=Isolation.READ_UNCOMMITTED, readOnly=true)
 public class ClienteServiceImpl implements ClienteService{

	Logger logger = Logger.getLogger(ClienteServiceImpl.class);
	 
	@Autowired
	private ClienteDAO clienteDAO;
	
	@Autowired
	private CatalogoDAO catalogoDAO;
	
	@Autowired
	private SolicitudDAO solicitudDAO;

	
	@Autowired
	private BitacoraDAO bitacora;
	
	public Cliente searchSSI(String rfc, String homoclave, Long cuenta)	throws ApplicationException {
		
		logger.info("buscando cliente en la base de datos");
		 
		Cliente clienteEncontrado = null; 

		List<Cliente> result = null; 
		
		if(StringUtils.isNotEmpty(rfc) && cuenta != null ){
			result = clienteDAO.getClienteByRFCCuenta(rfc, cuenta);
			clienteEncontrado = this.validaResultCuentaRFC(result);
		}else{
			if(StringUtils.isNotEmpty(rfc)){
				result = clienteDAO.getClienteByRFC(rfc);
				if(null == result || result.isEmpty()){
					throw new ApplicationException(MessageUtils.getMessage("rfc.no.existe"));
					
				}else{
				    if(result.size()>1){
				    	if(StringUtils.isNotEmpty(homoclave)){
				    		int n=0;
				    		Cliente clienteConHomo = null;
				    		for(Cliente cliente : result){
				    			if(homoclave.equals(cliente.getHomoclave())){
				    				clienteConHomo = cliente;
				    				n++;
				    			}
				    		}
				    		if(n==1){
				    			clienteEncontrado = clienteConHomo;
				    		}
				    	}
				    	throw new ApplicationException(MessageUtils.getMessage("cuenta.requerida"));
				    }else{
				    	clienteEncontrado = result.get(0);
				    }
				}
				
			}else{
				result = clienteDAO.getClienteByCuenta(cuenta);
				if(null == result || result.isEmpty()){
					throw new ApplicationException(AppConstants.ERROR_CODE_CUENTA_SSI, MessageUtils.getMessage("cuenta.no.existe"));
				}else{
				    if(result.size() > 1){
				    	throw new ApplicationException(MessageUtils.getMessage("varios.registros"));
				    }else{
				    	clienteEncontrado = result.get(0);
				    }
				}
			}
		}
		
		if(clienteEncontrado == null){
			throw new ApplicationException(MessageUtils.getMessage("cliente.no.encontrado"));
		}
		
		Integer dependencia = catalogoDAO.getIdDependenciaFromCatalog(clienteEncontrado.getRetenedor(), clienteEncontrado.getUnidadPago());
		
		clienteEncontrado.setClaveDependencia(dependencia);
		
		if(clienteDAO.solicitudEnTramite(clienteEncontrado)){
			throw new ApplicationException(MessageUtils.getMessage("cliente.tiene.solicitud.en.tramite"));
		}
		
		boolean hasSolicitudesCapturadas = clienteDAO.hasSolicitudCapturada(clienteEncontrado.getRfc(), clienteEncontrado.getCuenta());
		
		clienteEncontrado.setReCaptura(hasSolicitudesCapturadas);
		
		return clienteEncontrado;
	}
	
	
	
	
	
	/**
	 * 
	 * @param rfc
	 * @param cuenta
	 * @return
	 * @throws ApplicationException
	 */
	public Cliente consultaSaldo(String rfc, Long cuenta, Double ultimoDescuento)	throws ApplicationException {
		
		List<Cliente> list = clienteDAO.getClienteByRFCCuenta(rfc, cuenta);
		
		if(list == null || list.size() == 0){
			throw new ApplicationException(MessageUtils.getMessage("cliente.saldo.datosError"));
		}
		
		Cliente cliente = list.get(0);
		
		if(cliente.getUltimoDescuento() == null){
			throw new ApplicationException(MessageUtils.getMessage("cliente.saldo.montoDescuentoNull"));
		}
		
		DecimalFormat fmt = new DecimalFormat("#######0.00");
		
		if(!StringUtils.equals(fmt.format(ultimoDescuento),fmt.format(cliente.getUltimoDescuento()))){
			throw new ApplicationException(MessageUtils.getMessage("cliente.saldo.datosError"));
		}
		
		return cliente; 
	}
	 
	 
	/**
	 * 
	 * @param result
	 * @return
	 * @throws ApplicationException
	 */
	 private Cliente validaResultCuentaRFC(List<Cliente> result) throws ApplicationException{
		if (null == result || result.isEmpty()) {
			throw new ApplicationException(MessageUtils.getMessage("rfc.y.cuenta.no.encontrado"));
		} else {
			if (result.size() > 1) {
				throw new ApplicationException(MessageUtils.getMessage("varios.registros"));
			} else {
				return result.get(0);
			}

		}
		 
	 }
	 
	 public List<Cliente> searchSSIAdmin(String rfc, String homoclave, Long cuenta) throws ApplicationException {
			
			logger.info("searchSSIAdmin: buscando cliente en la base de datos");
			 
			List<Cliente> result = null; 
			
			if(StringUtils.isNotEmpty(rfc) && cuenta != null ){
				result = clienteDAO.getClienteByRFCCuenta(rfc, cuenta);
			}else{
				if(StringUtils.isNotEmpty(rfc)){
					result = clienteDAO.getClienteByRFC(rfc);
					if(null == result || result.isEmpty()){
						throw new ApplicationException(MessageUtils.getMessage("rfc.no.existe"));
						
					}
				}else{
					result = clienteDAO.getClienteByCuenta(cuenta);
					if(null == result || result.isEmpty()){
						throw new ApplicationException(MessageUtils.getMessage("cuenta.no.existe"));
					}
				}
			}
			
			return result;
	}
	 
	
	public Cliente get(Long cuenta){
		return clienteDAO.getById(cuenta);
	}
	
	 
	 @Transactional(propagation=Propagation.REQUIRED, readOnly=false,rollbackFor=Exception.class)
	 public void updateFueraFecha(String rfc, Long cuenta, boolean newFueraFecha, String usuario){		 
		    
		 	clienteDAO.updateFueraFecha(rfc, cuenta, newFueraFecha);
		    String detalle = "rfc="+rfc+"-cuenta="+cuenta;
		    bitacora.save(this.createBitacora(MovimientosConstants.MODIFICACION_RFC_PERMISOS, usuario, detalle));
	 }
	 
	 @PreAuthorize("hasRole('ADMIN_CLIENTES')")
	 @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	 public void update(Cliente cliente, String usuario){
		 	
		 	cliente.setFechaModificacion(Calendar.getInstance().getTime());
		 	
		 	clienteDAO.insertHistorico(cliente.getCuenta());
		 	clienteDAO.update(cliente);
		 	
		    String detalle = "rfc="+cliente.getRfc()+"-cuenta="+cliente.getCuenta();
		    bitacora.save(this.createBitacora(MovimientosConstants.MODIFICACION_CLIENTE, usuario, detalle));
	 }
	 
	 
	 @PreAuthorize("hasRole('ADMIN_CLIENTES')")
	 @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
	 public void save(Cliente cliente, String usuario){
		 	
		 	cliente.setFechaAlta(Calendar.getInstance().getTime());
		 	
		 	clienteDAO.save(cliente);
		 	
		    String detalle = "rfc="+cliente.getRfc()+"-cuenta="+cliente.getCuenta();
		    bitacora.save(this.createBitacora(MovimientosConstants.ALTA_CLIENTE, usuario, detalle));
	 }
	 
	 
	/**
	 * 
	 * @param rfc
	 * @param cuenta
	 * @return
	 * @throws ApplicationException
	 */
	 public Cliente searchSSIEsquemas(String rfc, Long cuenta)	throws ApplicationException {
		
			List<Cliente> list = clienteDAO.getClienteByRFCCuenta(rfc, cuenta);
			
			if(list == null || list.size() == 0){
				throw new ApplicationException(MessageUtils.getMessage("cliente.no.encontrado"));
			}
			
			Cliente clienteEncontrado = list.get(0);
			
			Integer dependencia = catalogoDAO.getIdDependenciaFromCatalog(clienteEncontrado.getRetenedor(), clienteEncontrado.getUnidadPago());
			
			clienteEncontrado.setClaveDependencia(dependencia);
			
			if(clienteDAO.solicitudEsquemaEnTramite(clienteEncontrado)){
				throw new ApplicationException(MessageUtils.getMessage("esquemas.cliente.tiene.solicitud.en.tramite"));
			}
			
			boolean hasSolicitudesCapturadas = clienteDAO.hasSolicitudEsquemaCapturada(clienteEncontrado.getRfc(), clienteEncontrado.getCuenta());
			
			clienteEncontrado.setReCaptura(hasSolicitudesCapturadas);
			
			return clienteEncontrado; 
	}
	 
	 
	 private Bitacora createBitacora(short mov, String user, String detalle){
			Bitacora bitacora = new Bitacora();
			bitacora.setFecha(Calendar.getInstance().getTime());
			bitacora.setMovimiento(mov);
			bitacora.setUsuario(user);
			bitacora.setDetalle(detalle);
			return bitacora;
     }
	 
	 
}
