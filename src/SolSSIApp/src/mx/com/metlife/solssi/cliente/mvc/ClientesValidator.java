package mx.com.metlife.solssi.cliente.mvc;

import mx.com.metlife.solssi.catalogo.service.CatalogoService;
import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.util.AppConstants;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ClientesValidator implements Validator{
	
	@Autowired
	private CatalogoService catalogoService;
	
	private static final String CUENTA_PATTERN = "[0-9]{10}";
	
	@Override
	public boolean supports(Class<?> arg0) {
		return false;
	}

	@Override
	public void validate(Object obj, Errors errors) {
		Cliente cliente = (Cliente) obj;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rfc", "NotEmpty");
		
		
		
		if(cliente.getCuenta() == null || cliente.getCuenta() == 0){
			errors.rejectValue("cuenta", "NotValid");
		}else{
			String strc = cliente.getCuenta().toString();
			if(!strc.matches(CUENTA_PATTERN)){
				errors.rejectValue("cuenta", "NotValid");
			}
		}
		
		if(cliente.getRetenedor() == null){
			errors.rejectValue("retenedor", "NotValid");
		}
		
		if(cliente.getUnidadPago() == null){
			errors.rejectValue("unidadPago", "NotValid");
		}
		
		if(cliente.getRetenedor() != null && cliente.getUnidadPago() != null){
			Integer id = null;
			try {
				id = catalogoService.getIdDependenciaFromCatalog(cliente.getRetenedor(), cliente.getUnidadPago());
			} catch (ApplicationException e) {}
			
			if(id == null || id == 0){
				errors.rejectValue("retenedor", "NotDependencia");
				errors.rejectValue("unidadPago", "NotDependencia");
			}
		}
		
		
		if(cliente.getCodigoRetira() != AppConstants.PERMITIR_RETIRO_VALUE){
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "motivoNoRetiro", "NotEmpty");
			
			if(StringUtils.isNotEmpty(cliente.getMotivoNoRetiro()) && cliente.getMotivoNoRetiro().length()>100){
				errors.rejectValue("motivoNoRetiro", "MaxLength");
			}
		}else{
			if(cliente.getMaxPorcentaje() == null){
				errors.rejectValue("maxPorcentaje", "NotValid");
			}else{
				
				if(cliente.getMaxPorcentaje() > 50 || cliente.getMaxPorcentaje() <= 0 && cliente.getCodigoRetira() == 1){
					errors.rejectValue("maxPorcentaje", "NotValid");
				}
			}
			
			if(StringUtils.isNotEmpty(cliente.getUltimosDigitosClabe())){
				if(cliente.getUltimosDigitosClabe().length() != 4 || !StringUtils.isNumeric(cliente.getUltimosDigitosClabe())){
					errors.rejectValue("ultimosDigitosClabe", "NotValid");
				}
			}
			
		}
		
		if(StringUtils.isEmpty(cliente.getNivelPuesto())){
			errors.rejectValue("nivelPuesto", "NotValid");
		}
		
		if(cliente.getSaldo() == null || cliente.getSaldo().doubleValue() <= 0){
			errors.rejectValue("saldo", "NotValid");
		}
		
		if(cliente.getUltimoDescuento() == null || cliente.getUltimoDescuento() <= 0){
			errors.rejectValue("ultimoDescuento", "NotValid");
		}
		
	}

}
