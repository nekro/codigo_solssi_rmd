package mx.com.metlife.solssi.cliente.persistence;

import java.util.List;

import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.main.dao.GenericDAO;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;

public interface ClienteDAO extends GenericDAO<Cliente, Long>{ 

	public List<Cliente> getClienteByRFC(String rfc);
	public List<Cliente> getClienteByCuenta(Long cuenta);
	public List<Cliente> getClienteByRFCCuenta(String rfc, Long cuenta);
	public boolean hasSolicitudCapturada(String rfc, Long cuenta);
	public boolean solicitudEnTramite(Cliente cliente);
	public void updateFueraFecha(String rfc, Long Cuenta, boolean newFueraFecha);
	public void insertHistorico(Long cuenta);
	
	public boolean hasSolicitudEsquemaCapturada(String rfc, Long cuenta);
	public boolean solicitudEsquemaEnTramite(Cliente cliente);

}
