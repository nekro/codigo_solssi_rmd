package mx.com.metlife.solssi.cliente.persistence;

import java.util.Calendar;
import java.util.List;

import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.main.dao.GenericDAOImpl;
import mx.com.metlife.solssi.util.AppConstants;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
public class ClienteDAOImpl extends GenericDAOImpl<Cliente, Long> implements ClienteDAO{

private static Short estatusTramite[] = new Short[]{AppConstants.ESTATUS_CAPTURA,AppConstants.ESTATUS_RECHAZADO, AppConstants.ESTATUS_RECHAZADA_CADUCIDAD, AppConstants.ESTATUS_CANCELADA, AppConstants.ESTATUS_ERROR_PROCESO};
	
	private static Short estatusCapturada[] = new Short[]{AppConstants.ESTATUS_CAPTURA};
	
	@SuppressWarnings("unchecked")
	public List<Cliente> getClienteByCuenta(Long cuenta) {
			Session session = getSession();
			Query query = session.getNamedQuery("getClienteByCuenta")
							.setLong("cuenta", cuenta);
			return query.list();	
		
	}

	@SuppressWarnings("unchecked")
	public List<Cliente> getClienteByRFC(String rfc){
		
			Session session = getSession();
			Query query = session.getNamedQuery("getClienteByRFC")
							.setString("rfc", rfc);
			return query.list();	
	}

	@SuppressWarnings("unchecked")
	public List<Cliente> getClienteByRFCCuenta(String rfc, Long cuenta){
		
			Session session = getSession();
			Query query = session.getNamedQuery("getClienteByRFCCuenta")
							.setString("rfc", rfc)
							.setLong("cuenta", cuenta);
			return query.list();	
	}
	
	
	public boolean hasSolicitudCapturada(String rfc, Long cuenta){
			
			Session session = getSession();

			Query query = session.getNamedQuery("getSolicitudCapturada")
					.setString("rfc", rfc)
					.setLong("cuenta", cuenta)
					.setParameterList("listEstatus", estatusCapturada);
			
			
			Long result = (Long) query.uniqueResult();
			
			if (result != null && result > 0) {
				return true;
			}
			
			return false;
	}
	
	
	public boolean solicitudEnTramite(Cliente cliente){
		
		Session session = getSession();
		
		Query query;
		
		if(cliente.isFueraFecha()){
			query = session.getNamedQuery("existeSolicitudEnTramiteFF");
		}else{
			query = session.getNamedQuery("existeSolicitudEnTramite");	
		}

		
		  query.setString("rfc", cliente.getRfc())
				.setLong("cuenta", cliente.getCuenta())
				.setParameterList("listEstatus", estatusTramite);
		
		
		Long result = (Long) query.uniqueResult();
		
		if (result!=null && result > 0) {
			return true;
		}
		
		return false;
	}
	
	
	
	public boolean hasSolicitudEsquemaCapturada(String rfc, Long cuenta){
		
		Session session = getSession();

		Query query = session.getNamedQuery("getSolicitudEsquemaCapturada")
				.setString("rfc", rfc)
				.setLong("cuenta", cuenta)
				.setParameterList("listEstatus", estatusCapturada);
		
		
		Long result = (Long) query.uniqueResult();
		
		if (result != null && result > 0) {
			return true;
		}
		
		return false;
	}


	public boolean solicitudEsquemaEnTramite(Cliente cliente){
		
		Session session = getSession();
		
		Query query = session.getNamedQuery("existeSolicitudEsquemaEnTramite");	
	
		  query.setString("rfc", cliente.getRfc())
				.setLong("cuenta", cliente.getCuenta())
				.setParameterList("listEstatus", estatusTramite);
		
		
		Long result = (Long) query.uniqueResult();
		
		if (result!=null && result > 0) {
			return true;
		}
		
		return false;
	}

	
	public void updateFueraFecha(String rfc, Long cuenta, boolean newFueraFecha){
		  
		Session session = getSession();
		Query query = session.createQuery("update Cliente set fueraFecha =:newFueraFecha, fechaModificacion =:fecha where rfc =:rfc and cuenta =:cuenta");
        query.setParameter("rfc", rfc);
        query.setParameter("cuenta", cuenta);
        query.setParameter("newFueraFecha", newFueraFecha);
        query.setParameter("fecha", Calendar.getInstance().getTime());
        query.executeUpdate();
			
	}
	
	
	
	/**
	 * 
	 */
	public void insertHistorico(Long cuenta){
		  
		Session session = getSession();
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("insert into th_ssi_clientes(rfc, num_cuenta, bnd_prestamo, bnd_pension, bnd_retira, bnd_poder_judicial, cve_retenedor,");
		sql.append("cve_unidad_pago, ultimos_digitos_clabe, permitir_fuera_fecha, max_porcentaje_retiro, homoclave_rfc,");
		sql.append("motivo_no_retiro, nivel_puesto, saldo, monto_ult_descuento, fecha_alta, fecha_modificacion) "); 
			
		sql.append("select rfc, num_cuenta, bnd_prestamo, bnd_pension, bnd_retira, bnd_poder_judicial, cve_retenedor, ");
			sql.append("cve_unidad_pago, ultimos_digitos_clabe, permitir_fuera_fecha, max_porcentaje_retiro, homoclave_rfc, "); 
			sql.append("motivo_no_retiro, nivel_puesto, saldo, monto_ult_descuento, fecha_alta, fecha_modificacion from tw_ssi_clientes ");
			sql.append("where num_cuenta = :cuenta");
			
		
		Query query = session.createSQLQuery(sql.toString());
        query.setParameter("cuenta", cuenta);
        query.executeUpdate();
			
	}
	
	//ok

}
