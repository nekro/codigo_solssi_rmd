package mx.com.metlife.solssi.cliente.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

import mx.com.metlife.solssi.solicitud.domain.Solicitud;

import org.apache.commons.lang.StringUtils;

@Entity
@Table(name="TW_SSI_CLIENTES")
@NamedQueries({
	@NamedQuery(name="getClienteByCuenta", query = "SELECT c FROM Cliente c WHERE c.cuenta = :cuenta"),
	@NamedQuery(name="getClienteByRFC", query = "SELECT c FROM Cliente c WHERE c.rfc = :rfc"),
	@NamedQuery(name="getClienteByRFCCuenta", query = "SELECT c FROM Cliente c WHERE c.rfc = :rfc and c.cuenta = :cuenta")
})
	
public class Cliente implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1295502868723033160L;

	@Column(name = "RFC")
	@Pattern(regexp="(^[A-Za-z]{4}[0-9]{6}$)?")
	private String rfc;
	
	@Column(name="HOMOCLAVE_RFC")
	@Pattern(regexp="([A-Za-z0-9]{3})?")
	private String homoclave;
	
	@Id
	@Column(name = "NUM_CUENTA")
	@Max(value=9999999999L)
	@Min(value=1L)
	private Long cuenta;
	
	@Transient
	private String nombre;
	
	@Column(name = "BND_PRESTAMO")
	private boolean prestamo;
	
	@Column(name = "BND_PODER_JUDICIAL")
	private boolean poderJudicial;
	
	@Column(name = "BND_PENSION")
	private boolean pension;
	
	@Column(name = "BND_RETIRA")
	private short codigoRetira;
	
	@Column(name = "MOTIVO_NO_RETIRO")
	private String motivoNoRetiro;
	
	@Column(name = "PERMITIR_FUERA_FECHA")
	private boolean fueraFecha;
	
	@Column(name = "ULTIMOS_DIGITOS_CLABE")
	private String ultimosDigitosClabe;
	
	@Column(name = "MAX_PORCENTAJE_RETIRO")
	private Short maxPorcentaje;
	
	@Column(name = "NIVEL_PUESTO")
	private String nivelPuesto;
	
	@Column(name = "CVE_RETENEDOR")
	private Integer retenedor;
	
	@Column(name = "CVE_UNIDAD_PAGO")
	private Integer unidadPago;
	
	@Column(name = "SALDO")
	private BigDecimal saldo;
	
	@Column(name = "MONTO_ULT_DESCUENTO")
	private Double ultimoDescuento;
	
	@Column(name = "FECHA_ALTA")
	private Date fechaAlta;
	
	@Column(name = "FECHA_MODIFICACION")
	private Date fechaModificacion;
	
	@Transient
	private Solicitud solicitud;
	
	@Transient
	private Integer claveDependencia;
	
	@Transient
	private boolean reCaptura;

	public String getRfc() {
		return StringUtils.upperCase(StringUtils.trim(rfc));
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getHomoclave() {
		return StringUtils.upperCase(StringUtils.trim(homoclave));
	}

	public void setHomoclave(String homoclave) {
		this.homoclave = homoclave;
	}

	public Long getCuenta() {
		return cuenta;
	}

	public void setCuenta(Long cuenta) {
		this.cuenta = cuenta;
	}
	
	public void setCuenta(String cuenta) {
		this.cuenta = new Long(cuenta);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isPrestamo() {
		return prestamo;
	}

	public void setPrestamo(boolean prestamo) {
		this.prestamo = prestamo;
	}

	public boolean isPoderJudicial() {
		return poderJudicial;
	}

	public void setPoderJudicial(boolean poderJudicial) {
		this.poderJudicial = poderJudicial;
	}

	public boolean isPension() {
		return pension;
	}

	public void setPension(boolean pension) {
		this.pension = pension;
	}
	
	
	
	public Solicitud getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Solicitud solicitud) {
		this.solicitud = solicitud;
	}
	
	public boolean isFueraFecha() {
		return fueraFecha;
	}

	public void setFueraFecha(boolean fueraFecha) {
		this.fueraFecha = fueraFecha;
	}

	
	/**
	 * 
	 * @return
	 */
	public String getUltimosDigitosClabe() {
		return StringUtils.trim(ultimosDigitosClabe);
	}

	
	/**
	 * 
	 * @param ultimosDigitosClabe
	 */
	public void setUltimosDigitosClabe(String ultimosDigitosClabe) {
		this.ultimosDigitosClabe = ultimosDigitosClabe;
	}
	
	

	/**
	 * 
	 * @return
	 */
	public Short getMaxPorcentaje() {
		if(this.maxPorcentaje == null){
			return 0;
		}
		return maxPorcentaje;
	}

	/**
	 * 
	 * @param maxPorcentaje
	 */
	public void setMaxPorcentaje(Short maxPorcentaje) {
		this.maxPorcentaje = maxPorcentaje;
	}
	
	

	/**
	 * 
	 * @return
	 */
	public String getNivelPuesto() {
		return StringUtils.upperCase(StringUtils.trim(nivelPuesto));
	}

	
	/**
	 * 
	 * @param nivelPuesto
	 */
	public void setNivelPuesto(String nivelPuesto) {
		this.nivelPuesto = nivelPuesto;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public String getMotivoNoRetiro() {
		return StringUtils.trim(motivoNoRetiro);
	}

	/**
	 * 
	 * @param motivoNoRetiro
	 */
	public void setMotivoNoRetiro(String motivoNoRetiro) {
		this.motivoNoRetiro = motivoNoRetiro;
	}
	
	/**
	 * @return el retenedor
	 */
	public Integer getRetenedor() {
		return retenedor;
	}

	/**
	 * @param retenedor el retenedor a establecer
	 */
	public void setRetenedor(Integer retenedor) {
		this.retenedor = retenedor;
	}

	/**
	 * @return el unidadPago
	 */
	public Integer getUnidadPago() {
		return unidadPago;
	}

	/**
	 * @param unidadPago el unidadPago a establecer
	 */
	public void setUnidadPago(Integer unidadPago) {
		this.unidadPago = unidadPago;
	}
	
	

	/**
	 * @return el claveDependencia
	 */
	public Integer getClaveDependencia() {
		return claveDependencia;
	}

	/**
	 * @param claveDependencia el claveDependencia a establecer
	 */
	public void setClaveDependencia(Integer claveDependencia) {
		this.claveDependencia = claveDependencia;
	}
	
	

	/**
	 * @return el reCaptura
	 */
	public boolean isReCaptura() {
		return reCaptura;
	}

	/**
	 * @param reCaptura el reCaptura a establecer
	 */
	public void setReCaptura(boolean reCaptura) {
		this.reCaptura = reCaptura;
	}
	
	/**
	 * @return el codigoRetira
	 */
	public short getCodigoRetira() {
		return codigoRetira;
	}

	/**
	 * @param codigoRetira el codigoRetira a establecer
	 */
	public void setCodigoRetira(short codigoRetira) {
		this.codigoRetira = codigoRetira;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public BigDecimal getSaldo() {
		return saldo;
	}

	/**
	 * 
	 * @param saldo
	 */
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}
	
	public String getFmtSaldo(){
		if(this.saldo != null){
			DecimalFormat df = new DecimalFormat("#,###,##0.00");
			return "$ "+df.format(this.saldo.doubleValue());
		}else{
			return "0.00";
		}
	}
	
	
	/**
	 * 
	 * @return
	 */
	public Double getUltimoDescuento() {
		return ultimoDescuento;
	}
	
	

	/**
	 * @return el fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion el fechaModificacion a establecer
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * 
	 * @param ultimoDescuento
	 */
	public void setUltimoDescuento(Double ultimoDescuento) {
		this.ultimoDescuento = ultimoDescuento;
	}
	
	

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("{RFC:");
		builder.append(rfc);
		builder.append(",");
		builder.append("Homoclave:");
		builder.append(homoclave);
		builder.append(",");
		builder.append("Cuenta:");
		builder.append(cuenta);
		builder.append("}");
		return builder.toString();
	}
	
	

}
