package mx.com.metlife.solssi.adminrfc.mvc;

import java.util.List;

import javax.validation.Valid;

import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.cliente.service.ClienteService;
import mx.com.metlife.solssi.consulta.domain.ConsultaFilter;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.usuario.domain.UserPrincipal;
import mx.com.metlife.solssi.usuario.domain.Usuario;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "adminrfc", method = RequestMethod.POST)
public class AdminRFCController {
	
	@Autowired
	ClienteService clienteService;
	
	@RequestMapping(method = RequestMethod.POST)
	public String showConsulta(Model model){
		model.addAttribute(new ConsultaFilter());
		return "adminrfc";
	}
	
	
	@RequestMapping(value = "consulta", method = RequestMethod.POST)
	public String executeConsulta(@Valid ConsultaFilter filtro, BindingResult errors, Model model, Authentication auth) throws ApplicationException{
		
		model.addAttribute(filtro);
		
		if(StringUtils.isEmpty(filtro.getRfc())){
			errors.reject("NotEmpty.consultaFilter.object");
			return "adminrfc";
		}
		
		if(errors.hasErrors()){
			return "adminrfc";	
		}
		
		try {
				List<Cliente> clientes = clienteService.searchSSIAdmin(filtro.getRfc(), "", null);
				
				if(clientes == null){
					errors.reject("NotEmpty.consultaFilter.result");
				}
				
				model.addAttribute("clientes", clientes);
		
		} catch (Exception e) {
			model.addAttribute("error",e.getMessage());
		}
		
		return "adminrfc";
	}
	
	@RequestMapping(value="updateFueraFecha", method=RequestMethod.POST)
	public String updateFueraFecha(Model model, Authentication auth, @RequestParam("newFueraFecha") boolean newFueraFecha, @RequestParam("rfc") String rfc, @RequestParam("cuenta") Long cuenta){
		
		Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		ConsultaFilter consultaFilter = new ConsultaFilter();
		consultaFilter.setRfc(rfc);
		consultaFilter.setCuenta(cuenta.toString());
		model.addAttribute(consultaFilter);
		clienteService.updateFueraFecha(rfc, cuenta, newFueraFecha, usuario.getClave()); 
		model.addAttribute("updateFueraFecha", "OK");
		return "forward:/adminrfc/consulta";
	
	}


}
