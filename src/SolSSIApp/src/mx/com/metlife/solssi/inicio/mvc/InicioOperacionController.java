package mx.com.metlife.solssi.inicio.mvc;

import javax.servlet.http.HttpSession;

import mx.com.metlife.solssi.usuario.domain.UserPrincipal;
import mx.com.metlife.solssi.usuario.domain.Usuario;
import mx.com.metlife.solssi.util.BrowserUtils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("operacion")
public class InicioOperacionController {
	
	@RequestMapping
	public String inicioOperacion(Model model, HttpSession session, Authentication auth){
		  
		  
		  session.setAttribute("ACCESO_BY_OPERACION", "true");
		  
		  BrowserUtils.setFecha(session);
		  
		  Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		  
		  if(session.getAttribute("tipoMenu") == null){
			  session.setAttribute("tipoMenu", "RESCATES");  
		  }
		  
		  session.setAttribute("usuarioLogged", usuario);
		  session.setAttribute("opcionMenu", 1);
		  
		  return "inicio/operacion"; 
	}
	
	
	@RequestMapping("rescates")
	public String inicioOperacionRescates(Model model, HttpSession session, Authentication auth){
		  
		  
		  session.setAttribute("ACCESO_BY_OPERACION", "true");
		  
		  BrowserUtils.setFecha(session);
		  
		  Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		  
		  session.setAttribute("usuarioLogged", usuario);
		  session.setAttribute("tipoMenu", "RESCATES");
		  session.setAttribute("opcionMenu", 1);
		  
		  return "inicio/operacion"; 
	}
	
	
	@RequestMapping("esquemas")
	public String inicioOperacionEsquemas(Model model, HttpSession session, Authentication auth){
		  
		  
		  session.setAttribute("ACCESO_BY_OPERACION", "true");
		  
		  BrowserUtils.setFecha(session);
		  
		  Usuario usuario = ((UserPrincipal)auth.getPrincipal()).getUsuario();
		  
		  session.setAttribute("usuarioLogged", usuario);
		  session.setAttribute("tipoMenu", "ESQUEMAS");
		  session.setAttribute("opcionMenu", 1);
		  
		  return "inicio/operacion"; 
	}

}
