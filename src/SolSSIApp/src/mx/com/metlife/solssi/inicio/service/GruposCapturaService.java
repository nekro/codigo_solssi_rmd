package mx.com.metlife.solssi.inicio.service;

import java.util.Date;

import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.util.RangoCaptura;



public interface GruposCapturaService {
	
	public RangoCaptura calculateRangoCaptura(String rfc) throws ApplicationException;
	
	public void checkInicialInGroup(Cliente cliente) throws ApplicationException;
	
	public void checkFechaMaxAutorizacion(String rfc, Long cuenta) throws ApplicationException;
	
	public RangoCaptura checkMaxFechaAutorization() throws ApplicationException;
	
	public RangoCaptura checkInRangeGenerarOficios() throws ApplicationException;
	
	public Date lastFechaInicioOficios() throws ApplicationException;
	
	public void clear();
	
	
	/*
	 * esquemas
	 */
	
	
	public void checkInFechaCapturaEsquemas(boolean recaptura) throws ApplicationException;;
	
	public void checkFechaMaxAutorizacionEsquemas() throws ApplicationException;
	
	public void validateFechasCapturaEsquemas() throws ApplicationException;
	
	public RangoCaptura getRangoFechaCapturaEsquemas();
	
	public RangoCaptura checkInRangeGenerarOficiosEsquemas() throws ApplicationException;
	
}
