package mx.com.metlife.solssi.inicio.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import mx.com.metlife.solssi.catalogo.domain.CatalogoItem;
import mx.com.metlife.solssi.catalogo.service.CatalogoService;
import mx.com.metlife.solssi.catalogo.util.CatalogoConstants;
import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.cliente.service.ClienteService;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.util.Base26Codec;
import mx.com.metlife.solssi.util.RangoCaptura;
import mx.com.metlife.solssi.util.message.MessageUtils;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

@Service
public class GruposCapturaServiceImpl implements GruposCapturaService {

	private static Logger logger = Logger
			.getLogger(GruposCapturaServiceImpl.class);

	@Autowired
	private CatalogoService catalogoService;
	
	@Autowired
	private ClienteService clienteService;
	
	private List<RangoCaptura> rangosCaptura;
	
	private RangoCaptura rangoCapturaEsquemas;
	
	public void buildRangosCaptura() throws ApplicationException {

		Map<String, CatalogoItem> mesesCaptura = catalogoService
				.getItems(CatalogoConstants.MESES_CAPTUTRA);
		
		rangosCaptura = new ArrayList<RangoCaptura>();

		try {
			for (CatalogoItem item : mesesCaptura.values()) {

				String strFechaInicial = item.getDescripcion();
				String strFechaFinal = item.getValorCampo2();
				String strFechaAutorizacion = item.getValorCampo3();
				String strFechaInicialOficio = item.getValorCampo4();
				String strFechaFinalOficio = item.getValorCampo5();
				
				String letras = item.getClaveItem();
				String letrasIniciales = letras.substring(0,2);
				String letrasFinales = letras.substring(3,5);

				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				
				Date fechaInicial = this.parseDate(format, strFechaInicial);
				Date fechaFinal = this.parseDate(format, strFechaFinal);
				Date fechaMaxAutorizacion = this.parseDate(format, strFechaAutorizacion);
				Date fechaInicialOficio = this.parseDate(format, strFechaInicialOficio);
				Date fechaFinalOficio = this.parseDate(format, strFechaFinalOficio);

				int rangoInicial = Base26Codec.fromBase26(letrasIniciales);
				int rangoFinal = Base26Codec.fromBase26(letrasFinales);
				
				RangoCaptura rango = new RangoCaptura();
				rango.setFirst(rangoInicial);
				rango.setLast(rangoFinal);
				rango.setFechaInicialCaptura(fechaInicial);
				rango.setFechaFinalCaptura(fechaFinal);
				rango.setMaxFechaAutorizacion(fechaMaxAutorizacion);
				rango.setFechaInicialOficios(fechaInicialOficio);
				rango.setFechaFinalOficios(fechaFinalOficio);
				
				rangosCaptura.add(rango);

			}
		} catch (Exception e) {
			logger.error(e);
		}
		
	}
	
	
    public RangoCaptura calculateRangoCaptura(String rfc) throws ApplicationException {

		if(rangosCaptura == null){
			this.buildRangosCaptura();
		}

		for(RangoCaptura rango : this.rangosCaptura){
			String iniciales = rfc.substring(0,2);
			int valueIniciales = Base26Codec.fromBase26(iniciales);
			if(valueIniciales >= rango.getFirst() && valueIniciales <= rango.getLast()){
				return rango;
			}		
		}
	
		throw new ApplicationException(MessageUtils.getMessage("grupos.service.no.informacion"));
	}

	public void checkInicialInGroup(Cliente cliente) throws ApplicationException {
		
		String rfc = cliente.getRfc();
		
		if(cliente.isFueraFecha()){
			return;
		}
		
		RangoCaptura rangoCaptura = this.calculateRangoCaptura(rfc);
		
		Date fechaActual = this.getFechaActual();
		
		if(fechaActual.before(rangoCaptura.getFechaInicialCaptura())){
			SimpleDateFormat fmt = new SimpleDateFormat("dd 'de' MMMMMMMMMMMMMMM 'de' yyyy");
			String fi = fmt.format(rangoCaptura.getFechaInicialCaptura());
			String ff = fmt.format(rangoCaptura.getFechaFinalCaptura());
			throw new ApplicationException(MessageUtils.getMessage("periodo.captura.posterior", new Object[]{fi, ff}));	
		}else{
			if(fechaActual.after(rangoCaptura.getFechaFinalCaptura())){
				throw new ApplicationException(MessageUtils.getMessage("periodo.captura.anterior"));
			}
		}
	}
	
	
	public void checkFechaMaxAutorizacion(String rfc, Long cuenta) throws ApplicationException {
		
		Date fechaActual = this.getFechaActual();
		
		RangoCaptura rango = this.calculateRangoCaptura(rfc);
		Date fechaMaxima = rango.getMaxFechaAutorizacion();
		
		if(fechaActual.after(fechaMaxima)){
			Cliente cliente = clienteService.get(cuenta);
			if(!cliente.isFueraFecha()){
				throw new ApplicationException(MessageUtils.getMessage("fecha.autorizacion.excedida"));
			}
		}	
		
	}
	
	
	public RangoCaptura checkInRangeGenerarOficios() throws ApplicationException {
		
		Date fechaActual = this.getFechaActual();
		
		if(rangosCaptura == null){
			this.buildRangosCaptura();
		}
		
		for(RangoCaptura rango : this.rangosCaptura){

			if(!fechaActual.before(rango.getFechaInicialOficios()) && !fechaActual.after(rango.getFechaFinalOficios())){
				return rango;
			}
			
		}
		
		throw new ApplicationException(MessageUtils.getMessage("oficio.periodo.inicial.error"));
	}
	

	public RangoCaptura checkMaxFechaAutorization() throws ApplicationException {
		
		Date fechaActual = this.getFechaActual();
		
		if(rangosCaptura == null){
			this.buildRangosCaptura();
		}
		
		for(RangoCaptura rango : this.rangosCaptura){

			if(!fechaActual.before(rango.getFechaInicialCaptura()) && !fechaActual.after(rango.getMaxFechaAutorizacion())){
				Date fechaMaxima = rango.getMaxFechaAutorizacion();
				if(fechaActual.after(fechaMaxima)){
					throw new ApplicationException(MessageUtils.getMessage("fecha.autorizacion.excedida"));
				}else{
					return rango;
				}

			}
			
		}
		
		throw new ApplicationException(MessageUtils.getMessage("fecha.autorizacion.excedida"));
	}
	
	
	private Date parseDate(SimpleDateFormat format, String str){
		try{
			return format.parse(str);
		}catch(Exception e){
			return null;
		}
	}
	
	@PreAuthorize("hasRole('ADMIN_CATALOGOS')")
	public void clear(){
		this.rangosCaptura = null;
		this.rangoCapturaEsquemas = null;
	}
	
	@Override
	public Date lastFechaInicioOficios() throws ApplicationException {
		
		Date fechaActual = this.getFechaActual();
		
		if(rangosCaptura == null){
			this.buildRangosCaptura();
		}
		
		Date lastFechaOficios = null;
		
		for(RangoCaptura rango : this.rangosCaptura){
			
			if(lastFechaOficios == null && !rango.getFechaInicialOficios().after(fechaActual)){
				lastFechaOficios = rango.getFechaInicialOficios();
			}
			
			if(lastFechaOficios != null && rango.getFechaInicialOficios().after(lastFechaOficios) && !rango.getFechaInicialOficios().after(fechaActual)){
				lastFechaOficios = rango.getFechaInicialOficios();
			}
		}
		
		if(lastFechaOficios == null){
			throw new ApplicationException(MessageUtils.getMessage("No se encontraron fechas v�lidas para consultar oficios"));	
		}
		
		return lastFechaOficios;
	}
	
	
	
	/*
	 * FECHAS ESQUEMAS
	 */
	
	
	
	
	public void buildRangosCapturaEsquemas() throws ApplicationException {

		Map<String, CatalogoItem> mesesCaptura = catalogoService
				.getItems(CatalogoConstants.FECHAS_ESQUEMAS);
		
		
		try {
			for (CatalogoItem item : mesesCaptura.values()) {

				String strFechaInicial = item.getDescripcion();
				String strFechaFinal = item.getValorCampo2();
				String strFechaAutorizacion = item.getValorCampo3();
				String strFechaInicialOficio = item.getValorCampo4();
				String strFechaFinalOficio = item.getValorCampo5();
				String strFechaAplicacion = item.getValorCampo6();
				
				SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
				
				Date fechaInicial = this.parseDate(format, strFechaInicial);
				Date fechaFinal = this.parseDate(format, strFechaFinal);
				Date fechaMaxAutorizacion = this.parseDate(format, strFechaAutorizacion);
				Date fechaInicialOficio = this.parseDate(format, strFechaInicialOficio);
				Date fechaFinalOficio = this.parseDate(format, strFechaFinalOficio);
				Date fechaAplicacion = this.parseDate(format, strFechaAplicacion);

				this.rangoCapturaEsquemas = new RangoCaptura();

				rangoCapturaEsquemas.setFechaInicialCaptura(fechaInicial);
				rangoCapturaEsquemas.setFechaFinalCaptura(fechaFinal);
				rangoCapturaEsquemas.setMaxFechaAutorizacion(fechaMaxAutorizacion);
				rangoCapturaEsquemas.setFechaInicialOficios(fechaInicialOficio);
				rangoCapturaEsquemas.setFechaFinalOficios(fechaFinalOficio);
				rangoCapturaEsquemas.setFechaAplicacion(fechaAplicacion);
				
				SimpleDateFormat fmt = new SimpleDateFormat("dd 'de' MMMMMMMMMMMMMMM 'del' yyyy");
				rangoCapturaEsquemas.setFmtFechaAplicacion(fmt.format(fechaAplicacion));
				
			}
		} catch (Exception e) {
			logger.error(e);
		}
		
	}

	
	public void checkInFechaCapturaEsquemas(boolean recaptura) throws ApplicationException{
		
		if(this.rangoCapturaEsquemas == null){
			this.buildRangosCapturaEsquemas();
		}
		
		Date fechaActual = this.getFechaActual();
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd 'de' MMMMMMMMMMMMMMM 'de' yyyy");
		String fi = fmt.format(rangoCapturaEsquemas.getFechaInicialCaptura());
		String ff = fmt.format(rangoCapturaEsquemas.getFechaFinalCaptura());
		
		if(fechaActual.before(rangoCapturaEsquemas.getFechaInicialCaptura())){
			throw new ApplicationException(MessageUtils.getMessage("periodo.esquemas.captura.posterior", new Object[]{fi, ff}));	
		}else{
			if(recaptura){
				if(fechaActual.after(rangoCapturaEsquemas.getMaxFechaAutorizacion())){
					throw new ApplicationException(MessageUtils.getMessage("periodo.esquemas.captura.anterior", new Object[]{fi, ff}));
				}	
			}else{
				if(fechaActual.after(rangoCapturaEsquemas.getFechaFinalCaptura())){
					throw new ApplicationException(MessageUtils.getMessage("periodo.esquemas.captura.anterior", new Object[]{fi, ff}));
				}
			}
			
		}
	}
	
	public void validateFechasCapturaEsquemas() throws ApplicationException{
		
		if(this.rangoCapturaEsquemas == null){
			this.buildRangosCapturaEsquemas();
		}
		
		Date fechaActual = this.getFechaActual();
		
		SimpleDateFormat fmt = new SimpleDateFormat("dd 'de' MMMMMMMMMMMMMMM 'de' yyyy");
		String fi = fmt.format(rangoCapturaEsquemas.getFechaInicialCaptura());
		String ff = fmt.format(rangoCapturaEsquemas.getFechaFinalCaptura());
		
		if(fechaActual.before(rangoCapturaEsquemas.getFechaInicialCaptura())){
			throw new ApplicationException(MessageUtils.getMessage("periodo.esquemas.captura.posterior", new Object[]{fi, ff}));	
		}else{
			if(fechaActual.after(rangoCapturaEsquemas.getMaxFechaAutorizacion())){
				throw new ApplicationException(MessageUtils.getMessage("periodo.esquemas.captura.anterior", new Object[]{fi, ff}));
			}
		}
	}
	
	
	
	public void checkFechaMaxAutorizacionEsquemas() throws ApplicationException {
		
		Date fechaActual = this.getFechaActual();
		
		if(this.rangoCapturaEsquemas == null){
			this.buildRangosCapturaEsquemas();
		}
		
		
		Date fechaMaxima = rangoCapturaEsquemas.getMaxFechaAutorizacion();
		
		if(fechaActual.after(fechaMaxima)){
			throw new ApplicationException(MessageUtils.getMessage("fecha.autorizacion.excedida"));
		}	
		
	}
	
	
	private Date getFechaActual(){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
		
	}


	@Override
	public RangoCaptura checkInRangeGenerarOficiosEsquemas()
			throws ApplicationException {
		
		Date fechaActual = this.getFechaActual();
		
		if(this.rangoCapturaEsquemas == null){
			this.buildRangosCapturaEsquemas();
		}
		

		if(!fechaActual.before(rangoCapturaEsquemas.getFechaInicialOficios()) && !fechaActual.after(rangoCapturaEsquemas.getFechaFinalOficios())){
			return rangoCapturaEsquemas;
		}
			
		
		
		throw new ApplicationException(MessageUtils.getMessage("esquemas.oficio.periodo.inicial.error"));
	}


	@Override
	public RangoCaptura getRangoFechaCapturaEsquemas() {
		if(this.rangoCapturaEsquemas == null){
			try {
				this.buildRangosCapturaEsquemas();
			} catch (ApplicationException e) {
				e.printStackTrace();
			}
		}
		
		return this.rangoCapturaEsquemas;
	}


}
