package mx.com.metlife.solssi.inicio.mvc;

//import javax.portlet.RenderResponse;

import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import mx.com.metlife.solssi.catalogo.service.CatalogoService;
import mx.com.metlife.solssi.cliente.domain.Cliente;
import mx.com.metlife.solssi.cliente.service.ClienteService;
import mx.com.metlife.solssi.exception.ApplicationException;
import mx.com.metlife.solssi.inicio.service.GruposCapturaService;
import mx.com.metlife.solssi.solicitud.domain.Solicitud;
import mx.com.metlife.solssi.util.AppConstants;
import mx.com.metlife.solssi.util.BrowserUtils;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;


@Controller
@RequestMapping("clientes")
@SessionAttributes(value={"solicitud","cliente"})
public class InicioClientesController {
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	private GruposCapturaService gruposService;
	
	
	@Autowired 
	public void init(ServletContext context, CatalogoService catalogo){
		Locale.setDefault(new Locale("es","MX"));
		catalogo.load();
		context.setAttribute("CATALOGO", catalogo.getCatalogos());
	}

	
	
	@RequestMapping
	public String inicio(Model model, HttpSession session) {
		
		Locale.setDefault(new Locale("es","MX"));
		
		session.setAttribute("ACCESO_BY_OPERACION", "false");
		session.removeAttribute("solicitud");
		BrowserUtils.disableCloseSession(model);
		
		BrowserUtils.setFecha(session);
		
		return "inicio/clientes";
	}
	
	@RequestMapping(value="captura")
	public String inicioCaptura(Model model){
		BrowserUtils.disableCloseSession(model);
		model.addAttribute(new Cliente());
		return "inicio/captura";
	}
	
	@RequestMapping(value="busqueda", method=RequestMethod.POST)
    public String consulta(@Valid Cliente cliente,BindingResult errors, Model model, HttpSession session) {
		
		session.removeAttribute("errorCuenta");
		model.addAttribute(cliente);
		
		if(errors.hasErrors()){
			
			return "inicio/captura";
		}
		
		try {
			Cliente clienteSSI = clienteService.searchSSI(cliente.getRfc(), cliente.getHomoclave(),cliente.getCuenta());
			Solicitud solicitud;
			
			solicitud = new Solicitud();
			solicitud.setRfc(clienteSSI.getRfc());
			solicitud.setCuenta(clienteSSI.getCuenta());
			solicitud.setHomoclaveRfc(clienteSSI.getHomoclave());
			solicitud.setNivelPuesto(clienteSSI.getNivelPuesto());
			solicitud.setBndUltimosDigitosClabe(StringUtils.isNotEmpty(clienteSSI.getUltimosDigitosClabe()));
			
			solicitud.setFechaNacimientoRFC();
			
			if(clienteSSI.getCodigoRetira() == AppConstants.NO_PERMITIR_RETIRO_NO_INFO){
				throw new ApplicationException(clienteSSI.getMotivoNoRetiro());
			}
			
			if(clienteSSI.isReCaptura()){
				
				gruposService.checkFechaMaxAutorizacion(clienteSSI.getRfc(), clienteSSI.getCuenta());
					
				solicitud.setReCaptura(true);
				model.addAttribute(solicitud);
				model.addAttribute(clienteSSI);
				//solicitud.fillForTest();
				return "inicio/captura";
			}else{
				gruposService.checkInicialInGroup(clienteSSI);
				//solicitud.fillForTest();
			}
			
			if(clienteSSI.getCodigoRetira() == AppConstants.NO_PERMITIR_RETIRO){
				throw new ApplicationException(clienteSSI.getMotivoNoRetiro());
			}
			
			model.addAttribute(clienteSSI);
			model.addAttribute(solicitud);
			
			this.deleteObjectsSession(session);
			
			return "solicitud";
		}catch (ApplicationException ae){
			if(ae.getErrorCode() == AppConstants.ERROR_CODE_CUENTA_SSI){
				session.setAttribute("errorCuenta","true");
			}
			errors.reject("", ae.getMessage());
		} 
		
		return "inicio/captura";
	}
	
	
	
	@RequestMapping(value="saldo", method=RequestMethod.POST)
    public String consultaSaldoForm(Model model) {
		model.addAttribute(new Cliente());
		return "saldo/form";
	}
	
	
	@RequestMapping(value="saldo/execute", method=RequestMethod.POST)
    public String consultaSaldo(@Valid Cliente cliente,BindingResult errors, @RequestParam("captcha") String captcha,Model model, HttpSession session) {
		
		if(!StringUtils.equals(captcha, (String)session.getAttribute("sessionCaptchaCode"))){
			model.addAttribute("validCaptcha","false");
			return "saldo/form";
		}
		
		model.addAttribute(cliente);
		
		if(StringUtils.isEmpty(cliente.getRfc()) || cliente.getCuenta() == null || cliente.getCuenta() == 0){
			errors.reject("NotEmpty.saldo.rfc_cuenta");
		}
		
		if(errors.hasErrors()){
			return "saldo/form";
		}
		
		try {
			Cliente clienteSSI = clienteService.consultaSaldo(cliente.getRfc(), cliente.getCuenta(), cliente.getUltimoDescuento());
			
			model.addAttribute("clienteSSI",clienteSSI);
			session.removeAttribute("cliente");
			return "saldo/result";
		} catch (ApplicationException e) {
			errors.reject("", e.getMessage());
		}
		return "saldo/form";
	}
	
	@RequestMapping(value = "siModificar", method=RequestMethod.POST)
	public String confirmarModificacion(Model model, HttpSession session){
		Solicitud solicitud = (Solicitud) session.getAttribute("solicitud");
		model.addAttribute(solicitud);
		this.deleteObjectsSession(session);
		return "solicitud";
	}
	
	@RequestMapping(value = "noModificar", method=RequestMethod.POST)
	public String cancelarModificacion(Model model, HttpSession session){
		session.removeAttribute("solicitud");
		model.addAttribute(new Cliente());
		BrowserUtils.disableCloseSession(model);
		return "inicio/captura";
	}
	
	
	private void deleteObjectsSession(HttpSession session){
		session.removeAttribute("errorClabe");
		session.removeAttribute("errorPorcentaje");
		session.removeAttribute("errorCuenta");
		session.removeAttribute("errorList");
		session.removeAttribute("SolicitudSaved");
		session.removeAttribute("PRINT_PDF_COUNT");
		session.removeAttribute("DOWNLOAD_PDF_COUNT");
	}
	
	
}
